//
//  CityListPresenter.swift
//  PartTime
//
//  Created by passant on 1/6/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//
import ObjectMapper
import Moya
class CityListPresenter {
    
    
    var cityListView: CityListView?
    
    
    
    func attachView(view: CityListView) {
        self.cityListView = view
    }
    
    func detachView() {
        self.cityListView = nil
    }
    
    
    func doGetCityList() {
        
        
        if let path = Bundle.main.path(forResource: "Cities", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let jsonArray = try JSONSerialization
                    .jsonObject(with: data, options: []) as? [String: AnyObject] {
                    /* perform your ObjectMapper's mapping operation here */
                    print("city list = \(jsonArray)")
                    let cityListResponse = Mapper<CityListResponse>().map(JSON:try jsonArray)
                    if let cityListArr = cityListResponse?.data {
                        self.cityListView?.onSucessLoadingCityList(cities: cityListArr)
                    }
                } else {
                    
                }
            }
            catch let error as NSError {
                print(error)
                
            }
        }
        
        
    }
}

