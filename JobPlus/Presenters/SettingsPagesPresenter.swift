//
//  SettingsPagesPresenter.swift
//  Najeez
//
//  Created by passant on 6/3/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class SettingsPagesPresenter {
    
    
    var settingsPagesView: SettingsPagesView?
    
    
    
    func attachView(view: SettingsPagesView) {
        self.settingsPagesView = view
    }
    
    func detachView() {
        self.settingsPagesView = nil
    }
    
    
    func doGetSettingsPagesData(slug:String) {
        let provider = MoyaProvider<SettingsApi>()
        
        self.settingsPagesView?.showLoadingPageIndicator()
        provider.request(.getPagesData(slug: slug), completion: { result in
            self.settingsPagesView?.hideLoadingPageIndicator()
            switch result {
            case let .success(getSettingsPagesViewDataResponse):
                do {
                    print("getSettingsPagesViewDataResponse:")
                    print(try getSettingsPagesViewDataResponse.mapJSON() as! [String : Any])
                    if getSettingsPagesViewDataResponse.statusCode == 500 {
                        //unkonwon or server error
                        self.settingsPagesView?.hideLoadingPageIndicator()
                        self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                    }else {
                        let getSettingsPagesViewDataResponse = Mapper<PagesResponse>().map(JSON:try getSettingsPagesViewDataResponse.mapJSON() as! [String : Any])
                        
                        if(getSettingsPagesViewDataResponse?.page != nil) {
                            //request page done
                            self.settingsPagesView?.onSucessPage(pageObj: (getSettingsPagesViewDataResponse?.page!)!)
                            
                        }
                        
                    }
                } catch {
                    print(error)
                    //unkonwon or server error
                    self.settingsPagesView?.hideLoadingPageIndicator()
                    self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.settingsPagesView?.hideLoadingPageIndicator()
                self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getContactInfo() {
        let provider = MoyaProvider<SettingsApi>()
        
        self.settingsPagesView?.showLoadingPageIndicator()
        provider.request(.getContact, completion: { result in
            self.settingsPagesView?.hideLoadingPageIndicator()
            switch result {
            case let .success(getSettingsPagesViewDataResponse):
                do {
                    print("getSettingsPagesViewDataResponse:")
                    print(try getSettingsPagesViewDataResponse.mapJSON() as! [String : Any])
                    if getSettingsPagesViewDataResponse.statusCode == 500 {
                        //unkonwon or server error
                        self.settingsPagesView?.hideLoadingPageIndicator()
                        self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                    }else {
                        let getSettingsPagesViewDataResponse = Mapper<ContactUsRes>().map(JSON:try getSettingsPagesViewDataResponse.mapJSON() as! [String : Any])
                        if let Response = getSettingsPagesViewDataResponse{
                            if let status = Response.status {
                                if status {
                                    self.settingsPagesView?.onSuccessContactInfo(contactInfo: Response)
                                }
                                else{
                                    self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                                }
                            }
                            else{
                                self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                            }
                        }
                        else{
                            self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                        }
                    }
                } catch {
                    print(error)
                    //unkonwon or server error
                    self.settingsPagesView?.hideLoadingPageIndicator()
                    self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.settingsPagesView?.hideLoadingPageIndicator()
                self.settingsPagesView?.onErrorPage(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
}
