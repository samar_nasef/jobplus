//
//  InvoicesPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 19/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class InvoicesPresenter {
    var invoiceView: InvoicesView?
    
    func attachView(view: InvoicesView) {
        self.invoiceView = view
    }
    
    func detachView() {
        self.invoiceView = nil
    }
    
    func getInvoicesList() {
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        
        self.invoiceView?.showLoadingIndicator()
        provider.request(.Invoices, completion: { result in
            
            switch result {
            case let .success(ApiResponse):
                do {
                    self.invoiceView?.hideLoadingIndicator()
                    if ApiResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    let Response = try JSONDecoder().decode(InvoicesModel.self, from: ApiResponse.data)
                    
                    if let status = Response.status {
                        if status {
                            if let InvoicesList = Response.Envoices {
                                self.invoiceView?.onSucess(Response: InvoicesList)
                            }
                            else{
                                self.invoiceView?.onError(message:"Connection error, please try again later".localized())
                            }
                        }
                        else{
                            self.invoiceView?.onError(message:"Connection error, please try again later".localized())
                        }
                    }
                    else{
                        self.invoiceView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    self.invoiceView?.hideLoadingIndicator()
                    print(error)
                    self.invoiceView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.invoiceView?.hideLoadingIndicator()
                self.invoiceView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
