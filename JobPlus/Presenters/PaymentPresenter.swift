//
//  PaymentPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 2/27/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//
//


import UIKit
import Moya
import ObjectMapper
class PaymentPresenter {
    
    
    var PaymentView: PaymentInfoView?
    
    func attachView(view: PaymentInfoView) {
        self.PaymentView = view
    }
    
    func detachView() {
        self.PaymentView = nil
    }
    
    func createPayPage(paymentInfo:PaymentObj) {
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            self.PaymentView?.showLoadingIndicator()
        }
        
        self.PaymentView?.showLoadingIndicator()
        provider.request(.createPayPage(paymentInfo: paymentInfo) , completion: { result in
            switch result {
            case let .success(jobsApiResponse):
                do {
                    if jobsApiResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    let jobsResponse = try JSONDecoder().decode(paymentRes.self, from: jobsApiResponse.data)
                    self.PaymentView?.onSucess(paymentResponse: jobsResponse)
                    
                    
                } catch {
                    print(error)
                    self.PaymentView?.hideLoadingIndicator()
                    self.PaymentView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.PaymentView?.hideLoadingIndicator()
                self.PaymentView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    
}

