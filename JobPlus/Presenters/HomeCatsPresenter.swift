//
//  HomeCatsPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

class HomeCatsPresenter {
    var homeCatsView: HomeCatsView?
    
    func attachView(homeCatsView: HomeCatsView){
        self.homeCatsView = homeCatsView
    }
    
    func getHomeCats(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.homeCatsView?.showLoading()
        provider.request(.getHomeCategories, completion: { result in
            self.homeCatsView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.homeCatsView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(HomeCategories.self, from: ApiResponse.data)
                    if let showOffers = Response.showOffers {
                        UserManager.sharedInstance.showOffers = showOffers
                    }
                    if let showNewsArticles = Response.showNewsArticles {
                        UserManager.sharedInstance.showNewsArticles = showNewsArticles
                    }
                       if let subscribed = Response.subscribedUser {
                        if subscribed {
                            UserManager.sharedInstance.subscribedUser = true
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                        }
                        else{
                            if let suMsg = Response.NotSubscribedMeassage{
                                UserManager.sharedInstance.NotSubscribedMeassage = suMsg
                                print("su msg from home :\(suMsg)")
                            }
                           
                            UserManager.sharedInstance.subscribedUser = false
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                            
                            
                        }
                    }
                   
                    if let data = Response.data {
                        self.homeCatsView?.onSucessGetHomeCats(homeCats: data)
                    }
                    else{
                        self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.homeCatsView?.hideLoading()
                    self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func addVisit(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
       
        provider.request(.addVisit, completion: { result in
            switch result {
            case let .success(ApiResponse):
                do {
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = Mapper<AddVisitModel>().map(JSON:try ApiResponse.mapJSON() as! [String : Any])
                    if Response != nil {
                        if let status = Response?.status  {
                            if status {
                                let timestamp = NSDate().timeIntervalSince1970
                                UserDefaults.standard.set(timestamp, forKey: "previousAddVisit")
                            }
                        }
                    }
                    
                } catch {
                    print(error)
                    
                }
                
                break
            case .failure(_):
                break
            }
            
        })
        
    }
    func addCatToFave(catID: Int, index: Int){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.homeCatsView?.showLoading()
        provider.request(.addCategoryToFav(catId: catID), completion: { result in
            self.homeCatsView?.hideLoading()
            switch result {
            case let .success(preferedCategoriesResponse):
                do {
                    self.homeCatsView?.hideLoading()
                    if preferedCategoriesResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let preferedCategoriesResponse = Mapper<LoginResponse>().map(JSON:try preferedCategoriesResponse.mapJSON() as! [String : Any])
                    if(preferedCategoriesResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(preferedCategoriesResponse?.user)!)
                        self.homeCatsView?.onSuccessAddFav(index: index)
                    }
                    else{
                        self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.homeCatsView?.hideLoading()
                    self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    func removeCatToFave(catID: Int, index: Int){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.homeCatsView?.showLoading()
        provider.request(.removeCategoryToFav(catId: catID), completion: { result in
            self.homeCatsView?.hideLoading()
            switch result {
            case let .success(preferedCategoriesResponse):
                do {
                    self.homeCatsView?.hideLoading()
                    if preferedCategoriesResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let preferedCategoriesResponse = Mapper<LoginResponse>().map(JSON:try preferedCategoriesResponse.mapJSON() as! [String : Any])
                    if(preferedCategoriesResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(preferedCategoriesResponse?.user)!)
                        self.homeCatsView?.onSuccessRemoveFav(index: index)
                    }
                    else{
                        self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.homeCatsView?.hideLoading()
                    self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.homeCatsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
