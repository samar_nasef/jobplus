//
//  ChangePasswordPresenter.swift
//  Najeez
//
//  Created by passant on 5/17/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class ChangePasswordPresenter {
    var changePasswordView: ChangePasswordView?
    
    func attachView(view: ChangePasswordView) {
        self.changePasswordView = view
    }
    
    func detachView() {
        self.changePasswordView = nil
    }
    
    func dochangePassword(oldPassword:String,password:String) {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
            self.changePasswordView?.showLoadingIndicator()
        }
        provider.request(.changePasswordrequest(oldPassword: oldPassword, newPassword: password) , completion: { result in
            switch result {
            case let .success(changePasswordResponse):
                do {
                    if changePasswordResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try changePasswordResponse.mapJSON() as! [String : Any])
                    let verificationCodeResponse = Mapper<LoginResponse>().map(JSON:try changePasswordResponse.mapJSON() as! [String : Any])
                    if(verificationCodeResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(verificationCodeResponse?.user)!)
                        self.changePasswordView?.onSucess()
                    }else if verificationCodeResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (verificationCodeResponse?.errors)!)
                        self.changePasswordView?.onError(message: errorMsg)
                        self.changePasswordView?.hideLoadingIndicator()
                        
                    }else {
                        //some thing went wrong
                        self.changePasswordView?.hideLoadingIndicator()
                        if let msg = verificationCodeResponse?.message {
                        self.changePasswordView?.onError(message:msg)
                        }
                        else{
                             self.changePasswordView?.onError(message: "Connection error, please try again later".localized())
                        }
                    }
                    
                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                self.changePasswordView?.hideLoadingIndicator()
                self.changePasswordView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getErrorMessage(errorObj:ErrorResponse) -> String{
        var errorMessage = ""
        if let password = errorObj.password {
            errorMessage += "\n" + password
        }
        if let oldPassword = errorObj.oldPassword {
            errorMessage += "\n" + oldPassword
        }
        return errorMessage
    }
}
