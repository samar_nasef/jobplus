//
//  CountryListPresenter.swift
//  PartTime
//
//  Created by passant on 2/5/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
import ObjectMapper
import Moya
class CountryListPresenter {
    
    
    var countryListView: CountryListView?
    
    
    
    func attachView(view: CountryListView) {
        self.countryListView = view
    }
    
    func detachView() {
        self.countryListView = nil
    }
    
    
    func doGetCountryList() {
        
        
        if let path = Bundle.main.path(forResource: "Country", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let jsonArray = try JSONSerialization
                    .jsonObject(with: data, options: []) as? [String: AnyObject] {
                    /* perform your ObjectMapper's mapping operation here */
                    print("country list = \(jsonArray)")
                    let countryListResponse = Mapper<CountryListResponse>().map(JSON:try jsonArray)
                    print("countryListResponse = \(countryListResponse?.data)")
                    if let cityListArr = countryListResponse?.data {
                        self.countryListView?.onSucessLoadingCountryList(countries: cityListArr)
                    }
                } else {
                    
                }
            }
            catch let error as NSError {
                print(error)
            }
        }
        
        
    }
}

