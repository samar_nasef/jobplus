//
//  NotificationPresenter.swift
//  Najeez
//
//  Created by passant on 6/19/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class NotificationPresenter {
    
    var notificationView: NotificationView?
    func attachView(view: NotificationView) {
        self.notificationView = view
    }
    
    func detachView() {
        self.notificationView = nil
    }
    
    func doGetNotificationList(page:Int)
    {
        var provider = MoyaProvider<NotificationsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
           let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
           provider = MoyaProvider<NotificationsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
       }
        
        self.notificationView?.showLoadingIndicator()
        provider.request( .getNotifications(page: page) , completion: { result in
            switch result {
            case let .success(getnotificationsResponse):
                do {
                    print("get notifications Response")
                    if getnotificationsResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try getnotificationsResponse.mapJSON() as! [String : Any])
                   
                    let getnotificationsResponse = try JSONDecoder().decode(NotificaationsListResponse.self, from: getnotificationsResponse.data)
                    
                    if(getnotificationsResponse.data != nil) {
                        self.notificationView?.hideLoadingIndicator()
                        self.notificationView?.onSucess(notificationObject: getnotificationsResponse)
                    }else {
                        //errors found
                        self.notificationView?.hideLoadingIndicator()
                        self.notificationView?.onError(message: "Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.notificationView?.hideLoadingIndicator()
                    self.notificationView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.notificationView?.hideLoadingIndicator()
                self.notificationView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
        }
    
    func doGetGeneralNotificationList(page:Int)
    {
        var provider = MoyaProvider<NotificationsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
           let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
           provider = MoyaProvider<NotificationsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
       }
        
        self.notificationView?.showLoadingIndicator()
        provider.request( .getGeneralNotifications(page: page) , completion: { result in
            switch result {
            case let .success(getnotificationsResponse):
                do {
                    print("get notifications Response")
                    if getnotificationsResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try getnotificationsResponse.mapJSON() as! [String : Any])
                   
                    let getnotificationsResponse = try JSONDecoder().decode(NotificaationsListResponse.self, from: getnotificationsResponse.data)
                    if(getnotificationsResponse.data != nil) {
                        self.notificationView?.hideLoadingIndicator()
                        self.notificationView?.onSucess(notificationObject: getnotificationsResponse)
                    }else {
                        //errors found
                        self.notificationView?.hideLoadingIndicator()
                        self.notificationView?.onError(message: "Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.notificationView?.hideLoadingIndicator()
                    self.notificationView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.notificationView?.hideLoadingIndicator()
                self.notificationView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
        }
    
    func doGetJobsNotificationList(page:Int)
    {
        var provider = MoyaProvider<NotificationsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
           let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
           provider = MoyaProvider<NotificationsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
       }
        
        self.notificationView?.showLoadingIndicator()
        provider.request( .getJobsNotifications(page: page) , completion: { result in
            switch result {
            case let .success(getnotificationsResponse):
                do {
                    print("get notifications Response")
                    if getnotificationsResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try getnotificationsResponse.mapJSON() as! [String : Any])
                   
                    let getnotificationsResponse = try JSONDecoder().decode(NotificaationsListResponse.self, from: getnotificationsResponse.data)
                    if(getnotificationsResponse.data != nil) {
                        self.notificationView?.hideLoadingIndicator()
                        self.notificationView?.onSucess(notificationObject: getnotificationsResponse)
                    }else {
                        //errors found
                        self.notificationView?.hideLoadingIndicator()
                        self.notificationView?.onError(message: "Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.notificationView?.hideLoadingIndicator()
                    self.notificationView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.notificationView?.hideLoadingIndicator()
                self.notificationView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
        }
    
    func doreadNotificationList()
    {
        var provider = MoyaProvider<NotificationsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
           let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
           provider = MoyaProvider<NotificationsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
       }
        
        //self.notificationView?.showLoadingIndicator()
        provider.request( .readNotications , completion: { result in
            switch result {
            case let .success(DataRes):
                if DataRes.statusCode == 401 {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                        return
                                    }
                UserManager.sharedInstance.notificationCount = 0
//                do {
//                    print("readNotications")
//                   // print(try getnotificationsResponse.mapJSON() as! [String : Any])
//                } catch {
//                    print(error)
//                }
                
                break
            case .failure(_):
                //self.notificationView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
        }
        
    
    func getNotificationCount()
    {
        var provider = MoyaProvider<NotificationsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
           let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
           provider = MoyaProvider<NotificationsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
       }
        
        //self.notificationView?.showLoadingIndicator()
        provider.request( .getNotificationsCount , completion: { result in
            switch result {
            case let .success(getNotificationsCountResponse):
                do {
                    print("getNotificationsCount")
                    if getNotificationsCountResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try getNotificationsCountResponse.mapJSON() as! [String : Any])
                    let getnotificationsCountResponse = try JSONDecoder().decode(NotificationsCountRes.self, from: getNotificationsCountResponse.data)
                    if let showOffers = getnotificationsCountResponse.showOffers {
                        UserManager.sharedInstance.showOffers = showOffers
                    }
                    if let showNewsArticles = getnotificationsCountResponse.showNewsArticles {
                        UserManager.sharedInstance.showNewsArticles = showNewsArticles
                    }
                    self.notificationView?.onCountSuccess(notificationCount: getnotificationsCountResponse.notifications_count, notificationStatus: getnotificationsCountResponse.NotificationStatus)
                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                break
            }
            
        })
        
        }
    func changeNotificationStatus(status: Int)
    {
        var provider = MoyaProvider<NotificationsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
           let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
           provider = MoyaProvider<NotificationsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
       }
        
        self.notificationView?.showLoadingIndicator()
        provider.request( .NotificationStatus(status: status) , completion: { result in
            switch result {
            case let .success(getNotificationsStatusResponse):
                do {
                    print("getNotificationsCount")
                    if getNotificationsStatusResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    self.notificationView?.hideLoadingIndicator()
                    print(try getNotificationsStatusResponse.mapJSON() as! [String : Any])
                    let Response = try JSONDecoder().decode(NotificationStatusRes.self, from: getNotificationsStatusResponse.data)
                    if let statusRes = Response.status {
                        if statusRes {
                            UserManager.sharedInstance.notificationStatus = status
                            self.notificationView?.onCountSuccess(notificationCount: UserManager.sharedInstance.notificationCount, notificationStatus: status)
                        }
                    }
                    else{
                        self.notificationView?.onError(message: "changeLang")
                    }
                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                break
            }
            
        })
        
        }
}
