//
//  searchPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 12/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
import Moya

class SearchPresenter {
    var searchView: SearchView?
    
    func attachView(view: SearchView){
        self.searchView = view
    }
    
    func getCompanies(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getCompaniesData, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(companiesRes.self, from: ApiResponse.data)
                    if let data = Response.companies {
                        self.searchView?.onSuccessGetCompanies(companies: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getWorkSectors(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getWorkSectors, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(workSectorsRes.self, from: ApiResponse.data)
                    if let data = Response.workSectors {
                        self.searchView?.onSuccessGetWorkSectors(sectors: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getWorkingTimes(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getWorkTimes, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(workTimeRes.self, from: ApiResponse.data)
                    if let data = Response.worktimes {
                        self.searchView?.onSuccessGetWorkingTimes(worktimes: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getCountries(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getCountries, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(countriesRes.self, from: ApiResponse.data)
                    if let data = Response.data {
                        self.searchView?.onSuccessGetCities(countries: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getSpecialities(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getSpecialities, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(specialitiesRes.self, from: ApiResponse.data)
                    if let data = Response.specialities {
                        self.searchView?.onSuccessGetSpecialities(specialities: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getExperiences(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getExperoences, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(experiencesRes.self, from: ApiResponse.data)
                    if let data = Response.experiences {
                        self.searchView?.onSuccessGetExperiences(experience: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getDisabilitTypes(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.searchView?.showLoading()
        provider.request(.getDisabilitities, completion: { result in
            self.searchView?.hideLoading()
            switch result {
            case let .success(ApiResponse):
                do {
                    self.searchView?.hideLoading()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(disabilityRes.self, from: ApiResponse.data)
                    if let data = Response.disabilitytype {
                        self.searchView?.onSuccessGetDiabilities(disabilities: data)
                    }
                    else{
                        self.searchView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.searchView?.hideLoading()
                    self.searchView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.searchView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
