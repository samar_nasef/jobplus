//
//  CatgoriesPresenter.swift
//  PartTime
//
//  Created by mohamed shaat on 7/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class CatgoriesPresenter {
    
    
    var catgoriesView: CatgoriesView?
    
    func attachView(view: CatgoriesView) {
        self.catgoriesView = view
    }
    
    func detachView() {
        self.catgoriesView = nil
    }
    func getCatgories() {
        let provider = MoyaProvider<JobsApi>()
        self.catgoriesView?.showLoadingIndicator()
        provider.request(.catgories, completion: { result in
            self.catgoriesView?.hideLoadingIndicator()
            switch result {
            case let .success(catgoriesResponse):
                do {
                    print(try catgoriesResponse.mapJSON() as! [String : Any])
                    let response = Mapper<FavoritesRes>().map(JSON:try catgoriesResponse.mapJSON() as! [String : Any])
                    if let res = response, let status = res.status {
                        if status {
                            self.catgoriesView?.onSucess(catgoriesResponse: res)
                        }
                        else{
                            self.catgoriesView?.onError(message:"Connection error, please try again later".localized())
                        }
                    }
                    else{
                        self.catgoriesView?.onError(message:"Connection error, please try again later".localized())
                    }                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                self.catgoriesView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
}

