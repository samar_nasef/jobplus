//
//  AnswerQuestionsPresenter.swift
//  PartTime
//
//  Created by passant on 8/28/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class AnswerQuestionsPresenter: NSObject {
    
    var answerQuestionsView: AnswerQuestionsView?
    
    func attachView(view: AnswerQuestionsView) {
        self.answerQuestionsView = view
    }
    
    func detachView() {
        self.answerQuestionsView = nil
    }
    func getQuestionsList(jobId:Int) {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        let provider = MoyaProvider<AnswerQuestionsApi>(plugins: [authPlugin])
        self.answerQuestionsView?.showLoadingIndicator()
        provider.request(.questionsList(jobId: jobId), completion: { result in
            self.answerQuestionsView?.hideLoadingIndicator()
            switch result {
            case let .success(questionsListResponse):
                do {
                    if questionsListResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try questionsListResponse.mapJSON() as! [String : Any])
                    let questionsListResponse = Mapper<QuestionListResponse>().map(JSON:try questionsListResponse.mapJSON() as! [String : Any])
                    if let questionsListResponse = questionsListResponse {
                        self.answerQuestionsView?.onSucess(questionsListResponse: questionsListResponse)
                    }else {
                        self.answerQuestionsView?.onError(message: "Something went wrong".localized())
                    }
                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                self.answerQuestionsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
