//
//  arabClicksPresenter.swift
//  JobPlus
//
//  Created by samar nasef on 02/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class arabClicksPresenter: NSObject {
  
    var arabClicksView: arabClicksView?
    
    func attachView(view: arabClicksView) {
        self.arabClicksView = view
    }
    
    func detachView() {
        self.arabClicksView = nil
    }
    
    
    func doGetCategoriesAndMarkets() {
        
        var provider = MoyaProvider<arabsClickApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<arabsClickApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
//        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.arabClicksView?.showLoadingIndicator()
        provider.request(.getCategoriesAndMarkets , completion: { result in
            self.arabClicksView?.hideLoadingIndicator()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(categoriesAndMarketsRes.self, from: degreeTimesResponseData.data)

                        self.arabClicksView?.onSucessGetCategoriesAndMarkets(data: degreessTimesRes)
                        
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })

        
    }
    
    
    func doGetarabClicksOffers(countryID:Int,categoryID:Int) {
        
        var provider = MoyaProvider<arabsClickApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<arabsClickApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
//        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.arabClicksView?.showLoadingIndicator()
        provider.request(.getArabClicksOffers(countryID:countryID,categoryID:categoryID) , completion: { result in
            self.arabClicksView?.hideLoadingIndicator()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(arabClicksOffersRes.self, from: degreeTimesResponseData.data)

                        self.arabClicksView?.onSucessGetarabClicksOffers(data: degreessTimesRes)
                        
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })

        
    }
    
//    func doGetMessageDetails(id:Int) {
//
//        var provider = MoyaProvider<messagesApi>()
//        if UserManager.sharedInstance.getUser()?.token != nil {
//            let authPlugin = AccessTokenPlugin { () -> String in
//                (UserManager.sharedInstance.getUser()?.token!)!
//            }
//            provider = MoyaProvider<messagesApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
//        }
////        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
//        self.messagesView?.showLoadingIndicator()
//        provider.request(.MessageDetails(id:id) , completion: { result in
//            self.messagesView?.hideLoadingIndicator()
//                   switch result {
//                   case let .success(degreeTimesResponseData):
//                       do {
//                        let degreessTimesRes = try JSONDecoder().decode(allMessagesApiRes.self, from: degreeTimesResponseData.data)
////                        self.messagesView?.onSucessLoadingNewCeritficateList(degreeWorkingTimes: degreessTimesRes)
//                        self.messagesView?.onSucessMessageDetails(msg: "")
//
//                       } catch {
//                           print(error)
//
//                       }
//
//                       break
//                   case .failure(_):
//
//                       break
//                   }
//
//               })
//
//
//    }
//
//
//    func doAddNewMessageOrReplay (messageText:String,RecevierID:Int,ParentID:Int) {
//
//        var provider = MoyaProvider<messagesApi>()
//        if UserManager.sharedInstance.getUser()?.token != nil {
//            let authPlugin = AccessTokenPlugin { () -> String in
//                (UserManager.sharedInstance.getUser()?.token!)!
//            }
//            provider = MoyaProvider<messagesApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
//        }
////        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
//        self.messagesView?.showLoadingIndicator()
//        provider.request(.AddNewMessageOrReplay(messageText: messageText, RecevierID: RecevierID, ParentID: ParentID) , completion: { result in
//            self.messagesView?.hideLoadingIndicator()
//                   switch result {
//                   case let .success(degreeTimesResponseData):
//                       do {
//                        let degreessTimesRes = try JSONDecoder().decode(allMessagesApiRes.self, from: degreeTimesResponseData.data)
////                        self.messagesView?.onSucessLoadingNewCeritficateList(degreeWorkingTimes: degreessTimesRes)
//                        self.messagesView?.onSucessAddNewMessageOrReplay(msg: "")
//
//                       } catch {
//                           print(error)
//
//                       }
//
//                       break
//                   case .failure(_):
//
//                       break
//                   }
//
//               })
//
//
//    }
//    
  
}
