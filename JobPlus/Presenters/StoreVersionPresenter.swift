//
//  StoreVersionPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 29/01/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation
import Moya

class StoreVersionPresenter {
    var storeVersionView: StoreVersionView?
    func attachView(view: StoreVersionView){
        self.storeVersionView = view
    }
    func getStoreVersion(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        provider.request(.storeVersion, completion: { result in
            switch result {
            case let .success(ApiResponse):
                do {
                    
                    let Response = try JSONDecoder().decode(StoreVersionModel.self, from: ApiResponse.data)
                    if let storeVersion = Response.StoreIOSVersion {
                        self.storeVersionView?.storeVersionSuccessCallback(storeVersion: storeVersion)
                    }
                    
                       
                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                break
            }
            
        })
    }
}
