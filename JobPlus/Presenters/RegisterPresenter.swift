//
//  RegisterPresenter.swift
//  NajeezAgent
//
//  Created by passant on 5/9/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class RegisterPresenter {
    
    //get news data in NewsPresenter
    
    var registerView: RegisterView?
    
    
    
    func attachView(view: RegisterView) {
        self.registerView = view
    }
    
    func detachView() {
        self.registerView = nil
    }
    func doEditProfile( mobileApplicationLocale:String) {
        
       
        
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
            self.registerView?.showLoadingIndicator()
        }
        
        provider.request(.editProfileLanguage( mobileApplicationLocale : mobileApplicationLocale) , completion: { result in
            switch result {
            case let .success(registerResponse):
                do {
                    if registerResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try registerResponse.mapJSON() as! [String : Any])
                    let registerResponse = Mapper<LoginResponse>().map(JSON:try registerResponse.mapJSON() as! [String : Any])
                    if(registerResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(registerResponse?.user)!)
                        
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onSucess()
                        
                    }else if registerResponse?.errors != nil {
                        //errors found
                        //let errorMsg = Helper.getErrorMessage(errorObj: (registerResponse?.errors)!)
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onError(message: "changeLang")
                    }else {
                        //some thing went wrong
                        self.registerView?.hideLoadingIndicator()
                        //if let msg = registerResponse?.message {
                        self.registerView?.onError(message: "changeLang")
                        //}
                        //else{
                        //    self.registerView?.onError(message: "changeLang".localized())
                        //}
                    }
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                self.registerView?.hideLoadingIndicator()
                
                self.registerView?.onError(message:"changeLang")
                break
            }
            
        })
        
    }
    func getCaptchaInfo(uuid: String){
        let provider = MoyaProvider<AuthorizationApi>(plugins: [ NetworkLoggerPlugin(verbose: true)])
        provider.request(.getCaptchCode(uuid: uuid)) { result in
            switch result {
            case let .success(Response):
                do {
                    let captchaApiResponse = try JSONDecoder().decode(captchaInfo.self, from: Response.data)
                    if let status = captchaApiResponse.status {
                        if status {
                            self.registerView?.onSuccessGetCaptch(captchaCode: captchaApiResponse.Code ?? "")
                        }
                        else{
                            self.registerView?.onError(message: "Connection error, please try again later".localized())
                        }
                    }
                    else{
                        self.registerView?.onError(message: "Connection error, please try again later".localized())
                    }
                }
                catch {
                    self.registerView?.onError(message: "Connection error, please try again later".localized())
                }
            case .failure(_):
                self.registerView?.onError(message: "Connection error, please try again later".localized())
            }
        }
        
    }
    func doEditProfileImage(image:String?){
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            self.registerView?.showLoadingIndicator()
        }
        provider.request(.editProfileimg(image: image ?? ""), completion: { result in
                switch result {
                case let .success(registerResponse):
                    do {
                        if registerResponse.statusCode == 401 {
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                                return
                                            }
                        print(try registerResponse.mapJSON() as! [String : Any])
                        let registerResponse = Mapper<editProfileImageResponse>().map(JSON:try registerResponse.mapJSON() as! [String : Any])
                        if(registerResponse?.userData != nil) {
                            UserManager.sharedInstance.saveUser(user:(registerResponse?.userData)!)
                            
                            self.registerView?.hideLoadingIndicator()
                            self.registerView?.onSucess()
                            
                        }else if registerResponse?.errors != nil {
                            //errors found
                            let errorMsg = Helper.getErrorMessage(errorObj: (registerResponse?.errors)!)
                            self.registerView?.hideLoadingIndicator()
                            self.registerView?.onError(message: errorMsg)
                        }else {
                            //some thing went wrong
                            self.registerView?.hideLoadingIndicator()
                            if let msg = registerResponse?.message {
                            self.registerView?.onError(message:msg)
                            }
                            else{
                                self.registerView?.onError(message: "Connection error, please try again later".localized())
                            }
                            
                        }
                    } catch {
                        print(error)
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                    break
                case .failure(_):
                    self.registerView?.hideLoadingIndicator()
                    self.registerView?.onError(message:"Connection error, please try again later".localized())
                    break
                }
                
            })
            
        
    }
    func doEditProfile(name:String , email:String , mobile:String , gender:String? , birthDate:Int?, mobileApplicationLocale:String,image:String? , userCountryId:Int , educationCountryId:Int , educationName:String , educationCity:String , educationDegreeKey:String , educationOtherDegree:String , educationMajor:String ,experienceObject:ExperienceObjectToPost,cv_extension: String, cv: String, certificate_id : Int, worktime_id: Int,City_Residence: String) {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            self.registerView?.showLoadingIndicator()
        }
        provider.request(.editProfileRequest(name: name,
                                             email : email,
                                             mobile : mobile,
                                             gender : gender,
                                             birthDate : birthDate ,
                                             mobileApplicationLocale : mobileApplicationLocale,
                                             image: image,
                                             userCountryId: userCountryId ,
                                             educationCountryId :educationCountryId ,
                                             educationName:educationName,
                                             educationCity:educationCity,
                                             educationDegreeKey:educationDegreeKey,
                                             educationOtherDegree:educationOtherDegree,
                                             educationMajor:educationMajor ,
                                             experienceObject:experienceObject,
                                             cv_extension: cv_extension,
                                             cv: cv, certificate_id: certificate_id , worktime_id: worktime_id,  City_Residence: City_Residence) , completion: { result in
            switch result {
            case let .success(registerResponse):
                do {
                    if registerResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    print(try registerResponse.mapJSON() as! [String : Any])
                    let registerResponse = Mapper<LoginResponse>().map(JSON:try registerResponse.mapJSON() as! [String : Any])
                    if(registerResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(registerResponse?.user)!)
                        
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onSucess()
                        
                    }else if registerResponse?.errors != nil {
                        self.registerView?.onError(message:"Connection error, please try again later".localized())
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (registerResponse?.errors)!)
                        self.registerView?.onError(message:errorMsg)
                        self.registerView?.hideLoadingIndicator()
                        //self.registerView?.onError(message: errorMsg)
                    }else {
                        //some thing went wrong
                        self.registerView?.hideLoadingIndicator()
                        //if let msg = registerResponse?.message {
                        //self.registerView?.onError(message:msg)
                        //}
                       // else{
                            //self.registerView?.onError(message: "Connection error, please try again later".localized())
                        //}
                        
                    }
                } catch {
                    print(error)
                    self.registerView?.hideLoadingIndicator()
                    self.registerView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.registerView?.hideLoadingIndicator()
                self.registerView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
   
//    name:String , email:String , mobile:String , gender:String? , birthDate:Int?, mobileApplicationLocale:String,image:String? , userCountryId:Int , educationCountryId:Int , educationName:String , educationCity:String , educationDegreeKey:String , educationOtherDegree:String , educationMajor:String ,experienceObject:ExperienceObjectToPost,cv_extension: String, cv: String, certificate_id : Int, worktime_id: Int,City_Residence: String
    func doEditNewProfile( data:User) {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            self.registerView?.showLoadingIndicator()
        }
        provider.request(.editnewProfileRequest(data:data) , completion: { result in
            switch result {
            case let .success(registerResponse):
                do {
                    if registerResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    print(try registerResponse.mapJSON() as! [String : Any])
                    let registerResponse = Mapper<LoginResponse>().map(JSON:try registerResponse.mapJSON() as! [String : Any])
                    if(registerResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(registerResponse?.user)!)
                        
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onSucess()
                        
                    }else if registerResponse?.errors != nil {
                        
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (registerResponse?.errors)!)
                        self.registerView?.onError(message:errorMsg)
                        self.registerView?.hideLoadingIndicator()
                        //self.registerView?.onError(message: errorMsg)
                    }else {
                        //some thing went wrong
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onError(message:"Connection error, please try again later".localized())
                        //if let msg = registerResponse?.message {
                        //self.registerView?.onError(message:msg)
                        //}
                       // else{
                            //self.registerView?.onError(message: "Connection error, please try again later".localized())
                        //}
                        
                    }
                } catch {
                    print(error)
                    self.registerView?.hideLoadingIndicator()
                    self.registerView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.registerView?.hideLoadingIndicator()
                self.registerView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }

    
    func doRegister(name:String, LastName: String , email:String , mobile:String , password:String , gender:String? , birthDate:Int?, mobileApplicationLocale:String,image:String?, DeviceGuid: String, CaptchaCode: String) {
        
        let provider = MoyaProvider<AuthorizationApi>(plugins: [ NetworkLoggerPlugin(verbose: true)])
        
        self.registerView?.showLoadingIndicator()
        provider.request(.registerRequest(name: name,LastName: LastName, email : email, mobile : mobile, password : password, gender : gender, birthDate : birthDate , mobileApplicationLocale : mobileApplicationLocale, image: image, DeviceGuid: DeviceGuid, CaptchaCode: CaptchaCode) , completion: { result in
            switch result {
            case let .success(registerResponse):
                do {
                    
                    print(try registerResponse.mapJSON() as! [String : Any])
                    let registerResponse = Mapper<LoginResponse>().map(JSON:try registerResponse.mapJSON() as! [String : Any])
                    if(registerResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(registerResponse?.user)!)
                        if(UserDefaults.standard.string(forKey: "FIREBASE_TOKEN") != nil)
                        {
                            self.addFirebaseToken()
                        }
                        
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onSucess()
                        
                    }else if registerResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (registerResponse?.errors)!)
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onError(message: errorMsg)
                    }else if registerResponse?.error != nil {
                        //errors found
                        self.registerView?.hideLoadingIndicator()
                        self.registerView?.onError(message: registerResponse?.error ?? "")
                    }
                    else {
                        //some thing went wrong
                        self.registerView?.hideLoadingIndicator()
                        if let msg = registerResponse?.message {
                        self.registerView?.onError(message:msg)
                        }
                        else{
                            self.registerView?.onError(message: "Connection error, please try again later".localized())
                        }
                    }
                } catch {
                    print(error)
                    self.registerView?.hideLoadingIndicator()
                    self.registerView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.registerView?.hideLoadingIndicator()
                self.registerView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    

    
    func addFirebaseToken() {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        let provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
        provider.request(.addFireBaseToken(deviceFirebaseToken:UserDefaults.standard.string(forKey: "FIREBASE_TOKEN")!), completion: { result in
            switch result {
            case let .success(editFirebaseResponse):
                do {
                    if editFirebaseResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print("editFirebaseResponse = \(editFirebaseResponse)")
                    print(try editFirebaseResponse.mapJSON() as! [String : Any])
                    
                } catch {
                    print(error)
                }
                break
            case .failure(_):
                break
            }
            
        })
    }
    
    
}




