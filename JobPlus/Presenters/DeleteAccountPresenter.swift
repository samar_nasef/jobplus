//
//  DeleteAccountPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 23/10/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import Foundation

import Foundation
import Moya
import ObjectMapper
class DeleteAccountPresenter {
    var deleteAccountView: DeleteAccountView?
    
    func attachView(view: DeleteAccountView) {
        self.deleteAccountView = view
    }
    
    func detachView() {
        self.deleteAccountView = nil
    }
    
    func deleteAccount() {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.deleteAccountView?.showLoadingIndicator()
        
        
        provider.request(.deleteAccount, completion: { result in
            self.deleteAccountView?.hideLoadingIndicator()
            switch result {
            case let .success(Response):
                do {
                    if Response.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    print("Response = \(Response)")
                    print(try Response.mapJSON() as! [String : Any])
                    let deleteResponse = Mapper<DeleteAccountRes>().map(JSON:try Response.mapJSON() as! [String : Any])
                    print(deleteResponse ?? "")
                    self.deleteAccountView?.hideLoadingIndicator()
                    if let status = deleteResponse?.status {
                        if status {
                            self.deleteAccountView?.onSucessDeleteAccount(message: "")
                        }else {
                            self.deleteAccountView?.onErrorDeleteAccount(message:"Connection error, please try again later".localized())
                        }
                    }
                    else{
                        self.deleteAccountView?.onErrorDeleteAccount(message:"Connection error, please try again later".localized())
                    }
                    
                    
                } catch {
                    print(error)
                    self.deleteAccountView?.hideLoadingIndicator()
                    self.deleteAccountView?.onErrorDeleteAccount(message:"Connection error, please try again later".localized())
                }
                
                
                break
            case .failure(_):
                self.deleteAccountView?.onErrorDeleteAccount(message:"Connection error, please try again later".localized())
                break
            }
            
        })
    }
}
