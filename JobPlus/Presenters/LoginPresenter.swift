//
//  LoginPresenter.swift
//  NajeezAgent
//
//  Created by passant on 5/8/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class LoginPresenter {
    
    //get news data in NewsPresenter
    
    var loginView: LoginView?
    

    
    func attachView(view: LoginView) {
        self.loginView = view
    }
    
    func detachView() {
        self.loginView = nil
    }
    
    
    func doLogin(mobile:String , password:String) {
        let provider = MoyaProvider<AuthorizationApi>()
        
        self.loginView?.showLoadingIndicator()
        provider.request(.loginRequest(mobile:mobile , password:password) , completion: { result in
            switch result {
            case let .success(loginResponse):
                do {
                    
                    print(try loginResponse.mapJSON() as! [String : Any])
                    let loginResponse = Mapper<LoginResponse>().map(JSON:try loginResponse.mapJSON() as! [String : Any])
                    if(loginResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(loginResponse?.user)!)
                        if(UserDefaults.standard.string(forKey: "FIREBASE_TOKEN") != nil)
                        {
                            self.addFirebaseToken()
                        }
                        self.loginView?.onSucess()
                    }
                    else if loginResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (loginResponse?.errors)!)
                        self.loginView?.hideLoadingIndicator()
                        self.loginView?.onError(message: errorMsg)
                    }else {
                        self.loginView?.hideLoadingIndicator()
                        if let error = loginResponse?.error {
                        self.loginView?.onError(message:error)
                        }
                    }
                } catch {
                    print(error)
                    self.loginView?.hideLoadingIndicator()
                    self.loginView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.loginView?.hideLoadingIndicator()
                self.loginView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }


    
    func addFirebaseToken() {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        let provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
        provider.request(.addFireBaseToken(deviceFirebaseToken:UserDefaults.standard.string(forKey: "FIREBASE_TOKEN")!), completion: { result in
            switch result {
            case let .success(editFirebaseResponse):
                do {
                    if editFirebaseResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print("editFirebaseResponse = \(editFirebaseResponse)")
                    print(try editFirebaseResponse.mapJSON() as! [String : Any])
            
                } catch {
                    print(error)
                }
                break
            case .failure(_):
                break
            }
            
        })
    }
    
}
