//
//  JobsPresenter.swift
//  PartTime
//
//  Created by mohamed shaat on 8/6/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class JobsPresenter: NSObject {
    var jobsView: JobsView?
    var noOfTries = 0
    func attachView(view: JobsView) {
        self.jobsView = view
    }
    
    func detachView() {
        self.jobsView = nil
    }
    func getJobs(link:String) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin])
        }
     //   self.jobsView?.showLoadingIndicator()
        provider.request(.jobsNext(nextLink: link),completion: { result in
            self.jobsView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    if jobsApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(jobsApiResponse.statusCode)
                    let jobsResponse = try JSONDecoder().decode(JobListResponse.self, from: jobsApiResponse.data)
                    self.jobsView?.onSuccessNextLinkCall(jobsResponse: jobsResponse)
                    
                } catch {
                    print(error)
                    self.jobsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.jobsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getJobs(title:String?,jobTypeID:Int?,page:Int?  , jobCityID:Int? , order:String, worktime_id: Int?, WorkSectorID: Int?, companyID: Int?, disabilityID: Int?, experienceID: Int?, specialityID: Int?, genderID: Int?, ageFrom: Int?, ageTo: Int?) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        if title == "" {
            self.jobsView?.showLoadingIndicator()
        }
        provider.request(.jobs(title:title,jobTypeID:jobTypeID,page:page, jobCityID: jobCityID, order: order, worktime_id: worktime_id, WorkSectorID: WorkSectorID, companyID : companyID, disabilityID: disabilityID, experienceID: experienceID, specialityID: specialityID, genderID: genderID, ageFrom: ageFrom, ageTo: ageTo ),completion: { result in
            self.jobsView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    if jobsApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(jobsApiResponse.statusCode)
                    let theJSONText = String(data: jobsApiResponse.data, encoding: .utf8)
                    print(theJSONText ?? "")
                    let jobsResponse = try JSONDecoder().decode(JobListResponse.self, from: jobsApiResponse.data)
                    if page == 1 {
                    self.jobsView?.onSucess(jobsResponse: jobsResponse)
                    }
                    else{
                        self.jobsView?.onSuccessNextLinkCall(jobsResponse: jobsResponse)
                    }
                    
                } catch {
                    print(error)
                     self.jobsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.jobsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    func getRecommendedJobs(page: Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
            self.jobsView?.showLoadingIndicator()
        
        provider.request(.getRecommendedJobs(page: page),completion: { result in
            self.jobsView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    print(jobsApiResponse.statusCode)
                    if jobsApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let jobsResponse = try JSONDecoder().decode(JobListResponse.self, from: jobsApiResponse.data)
                    if page == 1 {
                    self.jobsView?.onSucess(jobsResponse: jobsResponse)
                    }
                    else{
                        self.jobsView?.onSuccessNextLinkCall(jobsResponse: jobsResponse)
                    }
                    
                } catch {
                    print(error)
                     self.jobsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.jobsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    func getJobsBySectorID(ID: Int,page: Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
            self.jobsView?.showLoadingIndicator()
        
        provider.request(.getJobsBySectorID(ID: ID, page: page),completion: { result in
            self.jobsView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    print(jobsApiResponse.statusCode)
                    if jobsApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let jobsResponse = try JSONDecoder().decode(JobListResponse.self, from: jobsApiResponse.data)
                    self.jobsView?.onSucess(jobsResponse: jobsResponse)
                    
                } catch {
                    print(error)
                     self.jobsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.jobsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    func getNewestJobs(page: Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
            self.jobsView?.showLoadingIndicator()
    
        provider.request(.getNewestJobs(page: page),completion: { result in
            self.jobsView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    self.noOfTries = 0
                    if jobsApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                    }
                    print(jobsApiResponse.statusCode)
                    let jobsResponse = try JSONDecoder().decode(JobListResponse.self, from: jobsApiResponse.data)
                    if page == 1 {
                    self.jobsView?.onSucess(jobsResponse: jobsResponse)
                    }
                    else{
                        self.jobsView?.onSuccessNextLinkCall(jobsResponse: jobsResponse)
                    }
                    
                } catch {
                    self.jobsView?.onError(message:"Connection error, please try again later".localized())
                    print(error)
                }
                
                break
            case .failure(_):
                if self.noOfTries == 0 {
                    self.getNewestJobs(page: page)
                    self.noOfTries = 1
                }
                else{
                    self.jobsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            }
            
        })
        
    }
}
