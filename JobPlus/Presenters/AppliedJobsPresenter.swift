//
//  AppliedJobsPresenter.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import UIKit
import Moya
import ObjectMapper
class AppliedJobsPresenter: NSObject {
    var appliedJobsView: AppliedJobsView?
    
    func attachView(view: AppliedJobsView) {
        self.appliedJobsView = view
    }
    
    func detachView() {
        self.appliedJobsView = nil
    }
    func getAppliedJobs(page:Int) {
        var provider = MoyaProvider<AppliedJobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AppliedJobsApi>(plugins: [authPlugin])
        }
        self.appliedJobsView?.showLoadingIndicator()
        provider.request(.getAppliedJobs(page: page),completion: { result in
            self.appliedJobsView?.hideLoadingIndicator()
            switch result {
            case let .success(appliedJobsResponse):
                do {
                    if appliedJobsResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try appliedJobsResponse.mapJSON() as! [String : Any])
                    let jobsResponse =  try JSONDecoder().decode(JobListResponse.self, from: appliedJobsResponse.data)
                        self.appliedJobsView?.onSucess(appliedJobsListResponse: jobsResponse)
                    
                } catch {
                    print(error)
                    self.appliedJobsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.appliedJobsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
