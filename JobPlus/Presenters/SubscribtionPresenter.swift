//
//  SubscribtionPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/3/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class SubscribtionPresenter {
    
    
    var subscritpionView: subscriptionsView?
    
    func attachView(view: subscriptionsView) {
        self.subscritpionView = view
    }
    
    func detachView() {
        self.subscritpionView = nil
    }
    func addIAPTransaction(transaction_id: String, packageID: Int, paymentBrand: Int){
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        
        self.subscritpionView?.iapShowLoading()
        provider.request(.addTransactionIAP(transaction_id: transaction_id, packageID: packageID, paymentBrand: paymentBrand), completion: { result in
            self.subscritpionView?.iapHideLoading()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    if jobsApiResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    let Response = try JSONDecoder().decode(iapSubscribtionRes.self, from: jobsApiResponse.data)
                    if let requestStatus = Response.status {
                        if requestStatus {
                            self.subscritpionView?.onSuccessAddTransaction()
                        }
                        else{
                            self.subscritpionView?.onAddTransactionError()
                        }
                    }
                    
                } catch {
                    print(error)
                    self.subscritpionView?.onAddTransactionError()
                }
                
                break
                
            case .failure(_):
                self.subscritpionView?.onAddTransactionError()
                break
            }
            
        })
    }
    func getHyperPayURL(package_id: Int, amount: String, payment_brand: Int){
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        
        self.subscritpionView?.iapShowLoading()
        provider.request(.getHyperPayURL(package_id: package_id, amount: amount, payment_brand: payment_brand), completion: { result in
            self.subscritpionView?.iapHideLoading()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    if jobsApiResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    let Response = try JSONDecoder().decode(hyperPayRes.self, from: jobsApiResponse.data)
                    if let requestStatus = Response.status {
                        if requestStatus {
                            self.subscritpionView?.onSuccessHyperPay(URLString: Response.message ?? "")
                        }
                        else{
                            self.subscritpionView?.onAddTransactionError()
                        }
                    }
                    
                } catch {
                    print(error)
                    self.subscritpionView?.onAddTransactionError()
                }
                
                break
                
            case .failure(_):
                self.subscritpionView?.onAddTransactionError()
                break
            }
            
        })
    }
    func getPackages() {
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            self.subscritpionView?.showLoadingIndicator()
        }
        
        self.subscritpionView?.showLoadingIndicator()
        provider.request(.getPackagesData, completion: { result in
            self.subscritpionView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsApiResponse):
                do {
                    if jobsApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let jobsResponse = try JSONDecoder().decode(packagesRes.self, from: jobsApiResponse.data)
                    self.subscritpionView?.onSucess(subscriptionResponse: jobsResponse)
                    
                } catch {
                    print(error)
                    self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
                
            //            case let .success(SubscriptionResponse):
            //                do {
            //                    print(try SubscriptionResponse.mapJSON() as! [String : Any])
            //                    let catogriesResponse = Mapper<SubscriptionResponse>().map(JSON:try SubscriptionResponse.mapJSON() as! [String : Any])
            //                    if let catgories = catogriesResponse {
            //                        self.subscritpionView?.onSucess(subscriptionResponse:catgories)
            //                    }
            //
            //                } catch {
            //                    print(error)
            //                }
            
            //                break
            case .failure(_):
                self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getNewPackages() {
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        
        self.subscritpionView?.showLoadingIndicator()
        provider.request(.UserNewPackage, completion: { result in
            
            switch result {
            case let .success(ApiResponse):
                do {
                    self.subscritpionView?.hideLoadingIndicator()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(newPacakagesRes.self, from: ApiResponse.data)
                    self.subscritpionView?.onSuccessNewPackages(Res: Response)
                    
                } catch {
                    self.subscritpionView?.hideLoadingIndicator()
                    print(error)
                    self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.subscritpionView?.hideLoadingIndicator()
                self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func cancelSubscribtion() {
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        
        self.subscritpionView?.showLoadingIndicator()
        provider.request(.CancelAutoSubscribe, completion: { result in
            switch result {
            case let .success(ApiResponse):
                do {
                    self.subscritpionView?.hideLoadingIndicator()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(newPacakagesRes.self, from: ApiResponse.data)
                    self.subscritpionView?.onSuccessNewPackages(Res: Response)
                    
                } catch {
                    print(error)
                    self.subscritpionView?.hideLoadingIndicator()
                    self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.subscritpionView?.hideLoadingIndicator()
                self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func autoSubscribtion() {
        var provider = MoyaProvider<subscriptionsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<subscriptionsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            
        }
        self.subscritpionView?.showLoadingIndicator()
        provider.request(.ActivateAutoSubscribe, completion: { result in
            switch result {
            case let .success(ApiResponse):
                do {
                    self.subscritpionView?.hideLoadingIndicator()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(newPacakagesRes.self, from: ApiResponse.data)
                    self.subscritpionView?.onSuccessNewPackages(Res: Response)
                    
                } catch {
                    self.subscritpionView?.hideLoadingIndicator()
                    print(error)
                    self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.subscritpionView?.hideLoadingIndicator()
                self.subscritpionView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}

