//
//  PreferencesPresenter.swift
//  PartTime
//
//  Created by passant on 1/6/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class PreferencesPresenter {
    
    
    var preferencesView: PreferencesView?
    
    
    
    func attachView(view: PreferencesView) {
        self.preferencesView = view
    }
    
    func detachView() {
        self.preferencesView = nil
    }
    
    
    func addPreferedCategories(categoriesIds:[Int], jobTypes: [Int], sectors: [Int], times: [Int], companies: [Int], cities: [Int] ) {
        var provider = MoyaProvider<SettingsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<SettingsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        
        self.preferencesView?.showLoadingPreferencesIndicator()
        provider.request(.addPreferedCategoires(categoiresIds: categoriesIds,  jobTypes: jobTypes, sectors: sectors, times: times, companies: companies, cities: cities) , completion: { result in
            switch result {
            case let .success(preferedCategoriesResponse):
                do {
                    if preferedCategoriesResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try preferedCategoriesResponse.mapJSON() as! [String : Any])
                    let preferedCategoriesResponse = Mapper<LoginResponse>().map(JSON:try preferedCategoriesResponse.mapJSON() as! [String : Any])
                    if(preferedCategoriesResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(preferedCategoriesResponse?.user)!)
                        self.preferencesView?.hideLoadingPreferencesIndicator()
                        self.preferencesView?.onSucess()
                    }
                    else if preferedCategoriesResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (preferedCategoriesResponse?.errors)!)
                        self.preferencesView?.hideLoadingPreferencesIndicator()
                        self.preferencesView?.onError(message: errorMsg)
                    }else {
                        self.preferencesView?.hideLoadingPreferencesIndicator()
                        if let error = preferedCategoriesResponse?.error {
                            self.preferencesView?.onError(message:error)
                        }
                    }
                } catch {
                    print(error)
                    self.preferencesView?.hideLoadingPreferencesIndicator()
                    self.preferencesView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.preferencesView?.hideLoadingPreferencesIndicator()
                self.preferencesView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
