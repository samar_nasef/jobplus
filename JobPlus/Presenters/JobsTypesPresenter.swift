//
//  JobsTypesPresenter.swift
//  PartTime
//
//  Created by mohamed shaat on 8/2/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class JobsTypesPresenter: NSObject {
  
    var jobsView: JobsTypesView?
    
    func attachView(view: JobsTypesView) {
        self.jobsView = view
    }
    
    func detachView() {
        self.jobsView = nil
    }
    func getJobsTypes() {
        let provider = MoyaProvider<JobsApi>()
        self.jobsView?.showLoadingIndicator()
        provider.request(.types, completion: { result in
            self.jobsView?.hideLoadingIndicator()
            switch result {
            case let .success(jobsTypesResponse):
                do {
                    print(try jobsTypesResponse.mapJSON() as! [String : Any])
                    let jobsResponse = Mapper<JobsTypesResponse>().map(JSON:try jobsTypesResponse.mapJSON() as! [String : Any])
                    if let jobsResponse = jobsResponse {
                        self.jobsView?.onSucess(jobsResponse: jobsResponse)
                    }
                    
                } catch {
                    self.jobsView?.onError(message:"Connection error, please try again later".localized())
                    print(error)
                }
                
                break
            case .failure(_):
                self.jobsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
