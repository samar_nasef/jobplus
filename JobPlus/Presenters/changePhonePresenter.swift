//
//  changePhonePresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/18/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

class ChangePhonePresenter{
    var changePhoneView: CheckPhoneForChangeView?
    
    func attachView(view: CheckPhoneForChangeView) {
        self.changePhoneView = view
    }
    
    func detachView() {
        self.changePhoneView = nil
    }
    func doCheckPhone(mobile:String) {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
            self.changePhoneView?.showLoadingIndicator()
        }
        provider.request(.checkChangePhone(mobile: mobile) , completion: { result in
            switch result {
            case let .success(checkPhoneResponse):
                do {
                    print(try checkPhoneResponse.mapJSON() as! [String : Any])
                    if checkPhoneResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let checkMobileForChangeResponse = try JSONDecoder().decode(checkPhoneForChangeRes.self, from: checkPhoneResponse.data)
                     self.changePhoneView?.hideLoadingIndicator()
                    if let validPhone = checkMobileForChangeResponse.status {
                        if validPhone {
                            self.changePhoneView?.onSucess()
                        }
                        else{
                            self.changePhoneView?.hideLoadingIndicator()
                            if let msg = checkMobileForChangeResponse.message {
                            self.changePhoneView?.onError(message: msg)
                            }
                            else{
                                self.changePhoneView?.onError(message: "Connection error, please try again later".localized())
                            }
                        }
                    }
                    else if let errMsg = checkMobileForChangeResponse.errors {
                        if let msg = errMsg.mobile {
                             self.changePhoneView?.onError(message: msg)
                        }
                    }
                    else{
                         self.changePhoneView?.onError(message: "Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.changePhoneView?.hideLoadingIndicator()
                    self.changePhoneView?.onError(message: "Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.changePhoneView?.hideLoadingIndicator()
                self.changePhoneView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func getErrorMessage(errorObj:ErrorResponse) -> String{
        var errorMessage = ""
        if let password = errorObj.password {
            errorMessage += "\n" + password
        }
        if let oldPassword = errorObj.oldPassword {
            errorMessage += "\n" + oldPassword
        }
        return errorMessage
    }
}
