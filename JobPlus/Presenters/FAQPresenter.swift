//
//  FAQPresenter.swift
//  PartTime
//
//  Created by passant on 1/7/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//


import Foundation
import Moya
import ObjectMapper
class FAQPresenter {
    
    
    var fAQView: FAQView?
    
    
    
    func attachView(view: FAQView) {
        self.fAQView = view
    }
    
    func detachView() {
        self.fAQView = nil
    }
    
    
    func getFAQData(page:Int) {
        let provider = MoyaProvider<SettingsApi>()
        
        self.fAQView?.showLoadingIndicator()
        provider.request(.getFAQData(page: page) , completion: { result in
            switch result {
            case let .success(faqResponseData):
                do {
                    
                    print(try faqResponseData.mapJSON() as! [String : Any])
                    let faqResponse = Mapper<FAQResponse>().map(JSON:try faqResponseData.mapJSON() as! [String : Any])
                    
                    if faqResponseData.statusCode == 500 {
                        //unkonwon or server error
                        self.fAQView?.hideLoadingIndicator()
                        self.fAQView?.onError(message:"Internal Server Error".localized())
                    }else if(faqResponse?.data != nil) {
                       
                        self.fAQView?.hideLoadingIndicator()
                        self.fAQView?.onSucessLoading(faqResponse: faqResponse!)
                    }
                    else if faqResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (faqResponse?.errors)!)
                        self.fAQView?.hideLoadingIndicator()
                        self.fAQView?.onError(message: errorMsg)
                    }else {
                        self.fAQView?.hideLoadingIndicator()
                        self.fAQView?.onError(message:"Internal Server Error".localized())
                    }
                } catch {
                    print(error)
                    self.fAQView?.hideLoadingIndicator()
                    self.fAQView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.fAQView?.hideLoadingIndicator()
                self.fAQView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
