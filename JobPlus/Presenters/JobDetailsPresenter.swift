//
//  JobDetailsPresenter.swift
//  PartTime
//
//  Created by passant on 1/3/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class JobDetailsPresenter {
    
    //get news data in NewsPresenter
    
    var jobDetailsView: JobDetailsView?
    var noOfRetries = 0
    
    
    func attachView(view: JobDetailsView) {
        self.jobDetailsView = view
    }
    
    func detachView() {
        self.jobDetailsView = nil
    }
    
    
    func doGetJobDetails(jobId:Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
            self.jobDetailsView?.showLoadingIndicator()
        }
        
        
        provider.request(.getJobDetails(jobId: jobId) , completion: { result in
            
            switch result {
            case let .success(jobInfoResponse):
                do {
                    self.noOfRetries = 0
                    if jobInfoResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                   let theJSONText = String(data: jobInfoResponse.data, encoding: .utf8)
                   print(theJSONText ?? "")
                    let jobInfoResponse = try JSONDecoder().decode(jobDetailRes.self, from: jobInfoResponse.data)
                    if let jobsInfoRes = jobInfoResponse.job {
                        if(jobsInfoRes.count > 0) {
                            self.jobDetailsView?.hideLoadingIndicator()
                            self.jobDetailsView?.onSucessLoadingJob(job: jobInfoResponse.job![0])
                            //self.jobDetailsView?.onSucessLoadingJob(job: jobInfoResponse.job![0],numberOfAppliedJobsThisMonth:jobInfoResponse.numberOfAppliedJobsThisMonth!,FullScreenAdAfter:jobInfoResponse.FullScreenAdAfter!)
                        }
                        else {
                            //some thing went wrong
                            self.jobDetailsView?.hideLoadingIndicator()
                            self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                        }
                    }
                    else {
                        //some thing went wrong
                        self.jobDetailsView?.hideLoadingIndicator()
                        self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                    }
                } catch {
                    self.jobDetailsView?.hideLoadingIndicator()
                    self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                    print(error)
                }
                break
            case .failure(_):
                if self.noOfRetries == 0 {
                    self.jobDetailsView?.hideLoadingIndicator()
                    self.doGetJobDetails(jobId: jobId)
                    self.noOfRetries = 1
                }
                else{
                    self.jobDetailsView?.hideLoadingIndicator()
                    self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                }
                break
            }
            
        })
        
    }
    
    func cancelApplyToJob(jobId:Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
        self.jobDetailsView?.showLoadingIndicator()
        provider.request(.cancelApply(jobId: jobId),completion: { result in
            self.jobDetailsView?.hideLoadingIndicator()
            switch result {
            case let .success(cancelApplyToJobResponse):
                do {
                    if cancelApplyToJobResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    let jobInfoResponse = try JSONDecoder().decode(cancelApplyRes.self, from: cancelApplyToJobResponse.data)
                    if let jobsInfoRes = jobInfoResponse.jobData {
                        if(jobsInfoRes.count > 0) {
                            self.jobDetailsView?.hideLoadingIndicator()
                            UserManager.sharedInstance.jobAppliedOrCanceled = 2
                            self.jobDetailsView?.onSucessLoadingJob(job: jobInfoResponse.jobData![0])
                        }
                        else {
                            //some thing went wrong
                            self.jobDetailsView?.hideLoadingIndicator()
                            self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                        }
                    }
                    else {
                        //some thing went wrong
                        self.jobDetailsView?.hideLoadingIndicator()
                        self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.jobDetailsView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func increaseClicks(jobId:Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
        provider.request(.increaseClicks(jobId: jobId),completion: { result in
            switch result {
            case let .success(increaseClicks):
                do {
                    if increaseClicks.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                    let increaseClicksResponse = try JSONDecoder().decode(increaseClicksRes.self, from: increaseClicks.data)
                    if let status = increaseClicksResponse.status {
                        print(status)
                    }
                    else {
                        //some thing went wrong
                        print("error happened")
                    }
                    
                } catch {
                    print(error)
                    
                }
                
                break
            case .failure(_):
                
                break
            }
            
        })
        
    }
}
