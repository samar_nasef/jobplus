//
//  ForgetPasswordPresenter.swift
//  Najeez
//
//  Created by passant on 5/20/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class ForgetPasswordPresenter {
    
    
    var forgetPasswordView: ForgetPasswordView?
    
    func attachView(view: ForgetPasswordView) {
        self.forgetPasswordView = view
    }
    
    func detachView() {
        self.forgetPasswordView = nil
    }
    func doForgetPasswordTokenRequest(phone:String,password:String,forgotPasswordToken:String) {
        let provider = MoyaProvider<AuthorizationApi>()
        self.forgetPasswordView?.showLoadingIndicator()
        provider.request(.forgetPasswordTokenRequest(phone:phone,password:password,forgotPasswordToken:forgotPasswordToken), completion: { result in
            switch result {
            case let .success(forgetPasswordResponse):
                do {
                    print(try forgetPasswordResponse.mapJSON() as! [String : Any])
                    let forgetPassResponse = Mapper<GeneralResponse>().map(JSON:try forgetPasswordResponse.mapJSON() as! [String : Any])
                    if let errors = forgetPassResponse?.errors {
                        self.forgetPasswordView?.hideLoadingIndicator()
                        self.forgetPasswordView?.onError(message:Helper.getErrorMessage(errorObj: errors))
                        
                    }else {
                        self.forgetPasswordView?.hideLoadingIndicator()
                        self.forgetPasswordView?.onSucess()
                    }
                    
                } catch {
                    print(error)
                    self.forgetPasswordView?.hideLoadingIndicator()
                    self.forgetPasswordView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.forgetPasswordView?.hideLoadingIndicator()
                self.forgetPasswordView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    func doForgetPassword(mobile:String) {
        let provider = MoyaProvider<AuthorizationApi>()
        self.forgetPasswordView?.showLoadingIndicator()
        provider.request(.forgetPasswordRequest(mobile:mobile) , completion: { result in
            switch result {
            case let .success(forgetPasswordResponse):
                do {
                    print(try forgetPasswordResponse.mapJSON() as! [String : Any])
                    let forgetPassResponse = Mapper<GeneralResponse>().map(JSON:try forgetPasswordResponse.mapJSON() as! [String : Any])
                    if let errors = forgetPassResponse?.errors {
                        self.forgetPasswordView?.hideLoadingIndicator()
                        self.forgetPasswordView?.onError(message:Helper.getErrorMessage(errorObj: errors))
                  
                    }else {
                        self.forgetPasswordView?.hideLoadingIndicator()
                        self.forgetPasswordView?.onSucess()
                    }
                 
                } catch {
                    print(error)
                    self.forgetPasswordView?.hideLoadingIndicator()
                    self.forgetPasswordView?.onError(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.forgetPasswordView?.hideLoadingIndicator()
                self.forgetPasswordView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
