//
//  VerificationCodePresenter.swift
//  Najeez
//
//  Created by passant on 5/22/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class VerificationCodePresenter {
    
    
    var verificationCodeView: VerficationCodeView?
    
    func attachView(view: VerficationCodeView) {
        self.verificationCodeView = view
    }
    
    func detachView() {
        self.verificationCodeView = nil
    }
    
    func doSendVerificationCode(verificationCode:Int) {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        print(UserManager.sharedInstance.getUser()!)
        let provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        self.verificationCodeView?.showVerficationLoadingIndicator()
        provider.request(.verficationCodeRequest(verificationCode: verificationCode) , completion: { result in
            switch result {
            case let .success(verificationCodeResponse):
                do {
                    if verificationCodeResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try verificationCodeResponse.mapJSON() as! [String : Any])
                    let verificationCodeResponse = Mapper<LoginResponse>().map(JSON:try verificationCodeResponse.mapJSON() as! [String : Any])
                    if(verificationCodeResponse?.user != nil) {
                        UserManager.sharedInstance.saveUser(user:(verificationCodeResponse?.user)!)
                        if(UserDefaults.standard.string(forKey: "FIREBASE_TOKEN") != nil)
                        {
                            self.addFirebaseToken()
                        }
                       self.verificationCodeView?.onSucessVerfication()
                    }else if verificationCodeResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (verificationCodeResponse?.errors)!)
                         self.verificationCodeView?.onErrorVerfication(message: errorMsg)
                        self.verificationCodeView?.hideVerficationLoadingIndicator()
                       
                    }else {
                        //some thing went wrong
                        self.verificationCodeView?.hideVerficationLoadingIndicator()
                        //self.verificationCodeView?.onErrorVerfication(message:(verificationCodeResponse?.message!)!)
                        if let msg = verificationCodeResponse?.message {
                                              self.verificationCodeView?.onErrorVerfication(message: msg)
                                               }
                                               else{
                                                   self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                                               }
                    }
                    
          
                    
                    
                } catch {
                    print(error)
                    self.verificationCodeView?.hideResendLoadingIndicator()
                    self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.verificationCodeView?.hideVerficationLoadingIndicator()
                self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    func doSendVerificationCodeToChangePhone(verificationCode:Int, mobile: String) {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        print(UserManager.sharedInstance.getUser()!)
        let provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
        self.verificationCodeView?.showVerficationLoadingIndicator()
        provider.request(.verifyMobileAfterChange(mobile: mobile, mobileVerificationCode: verificationCode), completion: { result in
            switch result {
            case let .success(verificationCodeResponse):
                do {
                    if verificationCodeResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print(try verificationCodeResponse.mapJSON() as! [String : Any])
                    
                    let checkMobileForChangeResponse = try JSONDecoder().decode(checkPhoneForChangeRes.self, from: verificationCodeResponse.data)
                         self.verificationCodeView?.hideVerficationLoadingIndicator()
                        if let validPhone = checkMobileForChangeResponse.status {
                            if validPhone {
                                 if let msg = checkMobileForChangeResponse.message {
                                    UserManager.sharedInstance.getUser()?.mobile = mobile
                                    self.verificationCodeView?.successChangePhone(message: "Phone number has been change successfully".localized())
                                }
                                else{
                                self.verificationCodeView?.onSucessVerfication()
                                }
                            }
                            else{
                                if let msg = checkMobileForChangeResponse.message {
                                self.verificationCodeView?.onErrorVerfication(message: msg)
                                }
                                else{
                                    self.verificationCodeView?.onErrorVerfication(message: "Connection error, please try again later".localized())
                                }
                            }
                        }
                        else if let errMsg = checkMobileForChangeResponse.errors {
                            if let msg = errMsg.mobile {
                                 self.verificationCodeView?.onErrorVerfication(message: msg)
                            }
                            else if let msg = errMsg.mobileVerificationCode {
                                 self.verificationCodeView?.onErrorVerfication(message: msg)
                            }
                        }
                        else{
                             self.verificationCodeView?.onErrorVerfication(message: "Connection error, please try again later".localized())
                        }
                        
                    }
                 catch {
                    print(error)
                    self.verificationCodeView?.hideResendLoadingIndicator()
                    self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.verificationCodeView?.hideVerficationLoadingIndicator()
                self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
    
    func doResendSendVerificationCode(secondPhone: String) {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        let provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
        
        self.verificationCodeView?.showResendLoadingIndicator()
        provider.request(.resendVerificationCodeRequest(secondPhone: secondPhone) , completion: { result in
            switch result {
            case let .success(verificationCodeResponse):
                do {
                    if verificationCodeResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print("resend verificationCodeResponse")
                    print(try verificationCodeResponse.mapJSON() as! [String : Any])
                    let verificationCodeResponse = Mapper<GeneralResponse>().map(JSON:try verificationCodeResponse.mapJSON() as! [String : Any])
                     if verificationCodeResponse?.errors != nil {
                        //errors found
                        let errorMsg = Helper.getErrorMessage(errorObj: (verificationCodeResponse?.errors)!)
                        self.verificationCodeView?.hideResendLoadingIndicator()
                        self.verificationCodeView?.onErrorVerfication(message: errorMsg)
                    }else {
                        //some thing went wrong
                        self.verificationCodeView?.hideResendLoadingIndicator()
                        //self.verificationCodeView?.onSucessResend(message: (verificationCodeResponse?.message)!)
                        if let msg = verificationCodeResponse?.message {
                        self.verificationCodeView?.onSucessResend(message: msg)
                         }
                         else{
                             self.verificationCodeView?.onSucessResend(message:"Connection error, please try again later".localized())
                         }
                    }
                    
                    
                    
                    
                } catch {
                    print(error)
                      self.verificationCodeView?.hideResendLoadingIndicator()
                      self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.verificationCodeView?.hideVerficationLoadingIndicator()
                self.verificationCodeView?.onErrorVerfication(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    

    func addFirebaseToken() {
        let authPlugin = AccessTokenPlugin { () -> String in
            (UserManager.sharedInstance.getUser()?.token!)!
        }
        let provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin])
        provider.request(.addFireBaseToken(deviceFirebaseToken:UserDefaults.standard.string(forKey: "FIREBASE_TOKEN")!), completion: { result in
            switch result {
            case let .success(editFirebaseResponse):
                do {
                    if editFirebaseResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print("editFirebaseResponse = \(editFirebaseResponse)")
                    print(try editFirebaseResponse.mapJSON() as! [String : Any])
                    
                } catch {
                    print(error)
                    
                }
                break
            case .failure(_):
                break
            }
            
        })
    }
    
}
