//
//  MessagesPresenter.swift
//  JobPlus
//
//  Created by samar nasef on 13/07/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class MessagesPresenter: NSObject {
  
    var messagesView: messagesView?
    
    func attachView(view: messagesView) {
        self.messagesView = view
    }
    
    func detachView() {
        self.messagesView = nil
    }
    
    
    func doGetAllMessages() {
        
        var provider = MoyaProvider<messagesApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<messagesApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
//        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.messagesView?.showLoadingIndicator()
        provider.request(.allMessages , completion: { result in
            self.messagesView?.hideLoadingIndicator()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(allMessagesApiRes.self, from: degreeTimesResponseData.data)
//                        self.messagesView?.onSucessLoadingNewCeritficateList(degreeWorkingTimes: degreessTimesRes)
                        self.messagesView?.onSucessGetAllMessages(data: degreessTimesRes)
                        
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })

        
    }
    
    
    func doGetMessageDetails(id:Int) {
        
        var provider = MoyaProvider<messagesApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<messagesApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
//        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.messagesView?.showLoadingIndicator()
        provider.request(.MessageDetails(id:id) , completion: { result in
            self.messagesView?.hideLoadingIndicator()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(allMessagesApiRes.self, from: degreeTimesResponseData.data)
//                        self.messagesView?.onSucessLoadingNewCeritficateList(degreeWorkingTimes: degreessTimesRes)
                        self.messagesView?.onSucessMessageDetails(msg: "")
                        
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })

        
    }
    
   
    func doAddNewMessageOrReplay (messageText:String,RecevierID:Int,ParentID:Int) {
        
        var provider = MoyaProvider<messagesApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<messagesApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
//        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.messagesView?.showLoadingIndicator()
        provider.request(.AddNewMessageOrReplay(messageText: messageText, RecevierID: RecevierID, ParentID: ParentID) , completion: { result in
            self.messagesView?.hideLoadingIndicator()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(allMessagesApiRes.self, from: degreeTimesResponseData.data)
//                        self.messagesView?.onSucessLoadingNewCeritficateList(degreeWorkingTimes: degreessTimesRes)
                        self.messagesView?.onSucessAddNewMessageOrReplay(msg: "")
                        
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })

        
    }
    
  
}
