//
//  UserInfoPresenter.swift
//  NajeezAgent
//
//  Created by passant on 7/9/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class UserInfoPresenter {
    
    //get news data in NewsPresenter
    
    var userInfoView: UserInfoView?
    
    
    
    func attachView(view: UserInfoView) {
        self.userInfoView = view
    }
    
    func detachView() {
        self.userInfoView = nil
    }
    
    
    func doGetUserInfo() {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
            self.userInfoView?.showLoadingUserInfoIndicator()
        }
        
        
        provider.request(.getUserInfo , completion: { result in
            switch result {
            case let .success(userInfoResponse):
                do {
                    if userInfoResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    self.userInfoView?.hideLoadingUserInfoIndicator()
                    print(try userInfoResponse.mapJSON() as! [String : Any])
                    
                    let userInfoResponse = Mapper<LoginResponse>().map(JSON:try userInfoResponse.mapJSON() as! [String : Any])
                    
                    if(userInfoResponse?.user != nil) {
                        print("saveObject : \(userInfoResponse?.user?.userLanguages2)")
                        UserManager.sharedInstance.saveUser(user:(userInfoResponse?.user)!)
                      
                        self.userInfoView?.onSucessUserInfo()
                    }else if let status = userInfoResponse?.status  {
                        //errors found
                        if !status {
                        
                        self.userInfoView?.hideLoadingUserInfoIndicator()
                            if let msg = userInfoResponse?.message {
                            self.userInfoView?.onErrorUserInfo(message: msg)
                            }
                            else{
                                 self.userInfoView?.onErrorUserInfo(message: "Connection error, please try again later".localized())
                            }
                        }
                    }
                        
                   
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                self.userInfoView?.hideLoadingUserInfoIndicator()
                self.userInfoView?.onErrorUserInfo(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
  
    func deleteUserProfileImg() {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
            self.userInfoView?.showLoadingUserInfoIndicator()
        }
        
        
        provider.request(.deleteUserProfileImg , completion: { result in
            switch result {
            case let .success(userInfoResponse):
                do {
                    if userInfoResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    self.userInfoView?.hideLoadingUserInfoIndicator()
                    print(try userInfoResponse.mapJSON() as! [String : Any])
                    
                    let userInfoResponse = Mapper<editProfileImageResponse>().map(JSON:try userInfoResponse.mapJSON() as! [String : Any])
                    
                    if(userInfoResponse?.userData != nil) {
                        
                        UserManager.sharedInstance.saveUser(user:(userInfoResponse?.userData)!)
                      
                        self.userInfoView?.onSucessUserInfo()
                    }else if let status = userInfoResponse?.status  {
                        //errors found
                        if !status {
                        
                        self.userInfoView?.hideLoadingUserInfoIndicator()
                            if let msg = userInfoResponse?.message {
                            self.userInfoView?.onErrorUserInfo(message: msg)
                            }
                            else{
                                 self.userInfoView?.onErrorUserInfo(message: "Connection error, please try again later".localized())
                            }
                        }
                    }
                        
                   
                } catch {
                    print(error)
                }
                
                break
            case .failure(_):
                self.userInfoView?.hideLoadingUserInfoIndicator()
                self.userInfoView?.onErrorUserInfo(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
}
