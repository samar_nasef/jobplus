//
//  ApplyToJobPresenter.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import UIKit
import Moya
import ObjectMapper
class ApplyToJobPresenter: NSObject {
    var applyToJobView: ApplyToJobView?
    
    func attachView(view: ApplyToJobView) {
        self.applyToJobView = view
    }
    
    func detachView() {
        self.applyToJobView = nil
    }
    func applyToJob(questionAnswersObj:QuestionListRequest , jobId:Int) {
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
        self.applyToJobView?.showLoadingApplyIndicator()
        provider.request(.applyToJob(questionsAnswerObject: questionAnswersObj, jobId: jobId),completion: { result in
            self.applyToJobView?.hideLoadingApplyIndicator()
            switch result {
            case let .success(applyToJobResponse):
                do {
                    print(applyToJobResponse.statusCode)
                    if applyToJobResponse.statusCode == 401 {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                        return
                    }
                        let applyToJobResponse = Mapper<GeneralResponse>().map(JSON:try applyToJobResponse.mapJSON() as! [String : Any])
                        if applyToJobResponse != nil {
                            if let status = applyToJobResponse?.status  {
                                if status == "error"{
                                   // self.applyToJobView?.onErrorApply(message: (applyToJobResponse?.message)!)
                                    if let msg = applyToJobResponse?.message {
                                    self.applyToJobView?.onErrorApply(message: msg)
                                    }
                                    else{
                                       self.applyToJobView?.onErrorApply(message: "Connection error, please try again later".localized())
                                    }
                                }else if status == "success" {
                                    if let user = applyToJobResponse?.user {
                                        if let feedback = user.feedback {
                                            UserManager.sharedInstance.userShouldAddFeedBack = !feedback
                                        }
                                    }
                                    self.applyToJobView?.onSucessApply(status: "1",message: applyToJobResponse?.message ?? "")
                                }
                                else if status == "-1" {
                                    self.applyToJobView?.onSucessApply(status: "-1",message: applyToJobResponse?.message ?? "")
                                }
                                else if status == "-19" {
                                    self.applyToJobView?.onSucessApply(status: "-19",message: applyToJobResponse?.message ?? "")
                                }
                                else if status == "20" {
                                    self.applyToJobView?.onSucessApply(status: "20",message: applyToJobResponse?.message ?? "")
                                }
                                else if status == "30" {
                                    self.applyToJobView?.onSucessApply(status: "30",message: applyToJobResponse?.message ?? "")
                                }
                                else {
                                    self.applyToJobView?.onErrorApply(message: "Connection error, please try again later".localized())
                                }
                            }
                            else{
                                self.applyToJobView?.onErrorApply(message: "Connection error, please try again later".localized())
                            }
                        }
                        else{
                            self.applyToJobView?.onErrorApply(message: "Connection error, please try again later".localized())
                        }
                } catch {
                    print(error)
                    self.applyToJobView?.onErrorApply(message: "Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.applyToJobView?.onErrorApply(message:"Connection error, please try again later".localized())
                break
            }
            
        })
        
    }
    
}
