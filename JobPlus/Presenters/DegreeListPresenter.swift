//
//  DegreeListPresenter.swift
//  PartTime
//
//  Created by passant on 2/5/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class DegreeListPresenter {
    
    
    var degreeListView: DegreeListView?
    
    
    
    func attachView(view: DegreeListView) {
        self.degreeListView = view
    }
    
    func detachView() {
        self.degreeListView = nil
    }
    
    
    func doGetDegreeList() {
        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.degreeListView?.showLoading()
        provider.request(.DegreesTimes , completion: { result in
            self.degreeListView?.hideLoading()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(DegreesTimesApiRes.self, from: degreeTimesResponseData.data)
                        self.degreeListView?.onSucessLoadingDegreeList(degreeWorkingTimes: degreessTimesRes)
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })
//        if let path = Bundle.main.path(forResource: "Degree", ofType: "json") {
//            do {
//                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
//                if let jsonArray = try JSONSerialization
//                    .jsonObject(with: data, options: []) as? [String: AnyObject] {
//                    /* perform your ObjectMapper's mapping operation here */
//                    print("degree list = \(jsonArray)")
//                    let degreeListResponse = Mapper<DegreeListResponse>().map(JSON:try jsonArray)
//                    print("degreeListResponse = \(degreeListResponse?.data)")
//                    if let degreeListArr = degreeListResponse?.data {
//                        self.degreeListView?.onSucessLoadingDegreeList(degreeArr: degreeListArr)
//                    }
//                } else {
//
//                }
//            }
//            catch let error as NSError {
//                print(error)
//            }
//        }

        
    }
    
    
    
    
    
    func doGetcertificatesNewList() {
        
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin,NetworkLoggerPlugin(verbose: true)])
        }
//        let provider = MoyaProvider<JobsApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
        self.degreeListView?.showLoading()
        provider.request(.certificatesNew , completion: { result in
            self.degreeListView?.hideLoading()
                   switch result {
                   case let .success(degreeTimesResponseData):
                       do {
                        let degreessTimesRes = try JSONDecoder().decode(NewCertificateApiRes.self, from: degreeTimesResponseData.data)
                        self.degreeListView?.onSucessLoadingNewCeritficateList(degreeWorkingTimes: degreessTimesRes)
                        
                       } catch {
                           print(error)
                           
                       }
                       
                       break
                   case .failure(_):
                     
                       break
                   }
                   
               })
//        if let path = Bundle.main.path(forResource: "Degree", ofType: "json") {
//            do {
//                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
//                if let jsonArray = try JSONSerialization
//                    .jsonObject(with: data, options: []) as? [String: AnyObject] {
//                    /* perform your ObjectMapper's mapping operation here */
//                    print("degree list = \(jsonArray)")
//                    let degreeListResponse = Mapper<DegreeListResponse>().map(JSON:try jsonArray)
//                    print("degreeListResponse = \(degreeListResponse?.data)")
//                    if let degreeListArr = degreeListResponse?.data {
//                        self.degreeListView?.onSucessLoadingDegreeList(degreeArr: degreeListArr)
//                    }
//                } else {
//
//                }
//            }
//            catch let error as NSError {
//                print(error)
//            }
//        }

        
    }
    
    
    
    
    
}
