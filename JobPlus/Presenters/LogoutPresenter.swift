//
//  LogoutPresenter.swift
//  Najeez
//
//  Created by passant on 5/17/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
class LogoutPresenter {
    var logoutView: LogoutView?
    
    func attachView(view: LogoutView) {
        self.logoutView = view
    }
    
    func detachView() {
        self.logoutView = nil
    }
    
    func doLogout() {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.logoutView?.showLoadingIndicator()
        
        var firebaseToken = ""
        if let token = UserDefaults.standard.string(forKey: "FIREBASE_TOKEN") {
            firebaseToken = token
        }
        provider.request(.logoutUserRequest(firebaseToken:firebaseToken), completion: { result in
            self.logoutView?.hideLoadingIndicator()
            switch result {
            case let .success(logOutResponse):
                do {
                    if logOutResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print("logOutResponse = \(logOutResponse)")
                    print(try logOutResponse.mapJSON() as! [String : Any])
                    let logOutResponse = Mapper<GeneralResponse>().map(JSON:try logOutResponse.mapJSON() as! [String : Any])
                    print(logOutResponse ?? "")
                    /*
                    if let status = logOutResponse?.status {
                    if status {
                        self.logoutView?.onSucess(message: (logOutResponse?.message)!)
                    }else {
                        self.logoutView?.onError(message:(logOutResponse?.message)!)
                    }
                    }
 */
                    self.logoutView?.hideLoadingIndicator()
                    self.logoutView?.onSucess(message: "")
                } catch {
                    print(error)
                    self.logoutView?.hideLoadingIndicator()
                    self.logoutView?.onError(message:"Connection error, please try again later".localized())
                }
                
                
                break
            case .failure(_):
                self.logoutView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
    }
    
    func deleteAccount() {
        var provider = MoyaProvider<AuthorizationApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<AuthorizationApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.logoutView?.showLoadingIndicator()
        
        
        provider.request(.deleteAccount, completion: { result in
            self.logoutView?.hideLoadingIndicator()
            switch result {
            case let .success(logOutResponse):
                do {
                    if logOutResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    print("logOutResponse = \(logOutResponse)")
                    print(try logOutResponse.mapJSON() as! [String : Any])
                    let logOutResponse = Mapper<GeneralResponse>().map(JSON:try logOutResponse.mapJSON() as! [String : Any])
                    print(logOutResponse ?? "")
                    /*
                    if let status = logOutResponse?.status {
                    if status {
                        self.logoutView?.onSucess(message: (logOutResponse?.message)!)
                    }else {
                        self.logoutView?.onError(message:(logOutResponse?.message)!)
                    }
                    }
 */
                    self.logoutView?.hideLoadingIndicator()
                    self.logoutView?.onSucess(message: "")
                } catch {
                    print(error)
                    self.logoutView?.hideLoadingIndicator()
                    self.logoutView?.onError(message:"Connection error, please try again later".localized())
                }
                
                
                break
            case .failure(_):
                self.logoutView?.onError(message:"Connection error, please try again later".localized())
                break
            }
            
        })
    }
}
