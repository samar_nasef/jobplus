//
//  FeedbackPresenter.swift
//  JobPlus
//
//  Created by elsaid yousif on 01/02/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation
import Moya

class FeedBackPresenter {
    var feedbackView: ListAddFeedBackView?
    func attachView(view: ListAddFeedBackView){
        self.feedbackView = view
    }
    
    func listFeedBack(){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.feedbackView?.showLoadingIndicator()
        provider.request(.listFeedbacks, completion: { result in
            switch result {
            case let .success(ApiResponse):
                do {
                    self.feedbackView?.hideLoadingIndicator()
                    let Response = try JSONDecoder().decode(ListFeedBackModel.self, from: ApiResponse.data)
                    if let feedBackList = Response.feedback {
                        self.feedbackView?.onSucess(feedBack: feedBackList)
                    }
                    else{
                        self.feedbackView?.onError()
                    }
                    
                } catch {
                    print(error)
                    self.feedbackView?.hideLoadingIndicator()
                    self.feedbackView?.onError()
                }
                
                break
            case .failure(_):
                self.feedbackView?.hideLoadingIndicator()
                self.feedbackView?.onError()
                break
            }
            
        })
    }
    func addFeedBack(rate: Float, message: String){
        var provider = MoyaProvider<JobsApi>()
        if UserManager.sharedInstance.getUser()?.token != nil {
            let authPlugin = AccessTokenPlugin { () -> String in
                (UserManager.sharedInstance.getUser()?.token!)!
            }
            provider = MoyaProvider<JobsApi>(plugins: [authPlugin, NetworkLoggerPlugin(verbose: true)])
        }
        self.feedbackView?.showLoadingIndicator()
        provider.request(.addFeedback(rate: rate, message: message), completion: { result in
            switch result {
            case let .success(ApiResponse):
                do {
                    self.feedbackView?.hideLoadingIndicator()
                    if ApiResponse.statusCode == 401 {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "unauthRequest"), object: nil)
                                            return
                                        }
                    let Response = try JSONDecoder().decode(AddFeedBackModel.self, from: ApiResponse.data)
                    if let status = Response.status {
                        if status {
                            if let msg = Response.message {
                                self.feedbackView?.onSuccessAddFeedback(message: msg)
                            }
                            else{
                                self.feedbackView?.onSuccessAddFeedback(message: "Connection error, please try again later".localized())
                            }
                        }
                        else{
                            if let msg = Response.message {
                                self.feedbackView?.onErrorAddFeedback(message: msg)
                            }
                            else{
                                self.feedbackView?.onErrorAddFeedback(message: "Connection error, please try again later".localized())
                            }
                        }
                    }
                    else{
                        self.feedbackView?.onErrorAddFeedback(message: "Connection error, please try again later".localized())
                    }
                    
                } catch {
                    print(error)
                    self.feedbackView?.hideLoadingIndicator()
                    self.feedbackView?.onErrorAddFeedback(message: "Connection error, please try again later".localized())
                }
                
                break
            case .failure(_):
                self.feedbackView?.hideLoadingIndicator()
                self.feedbackView?.onErrorAddFeedback(message: "Connection error, please try again later".localized())
                break
            }
            
        })
    }
}
