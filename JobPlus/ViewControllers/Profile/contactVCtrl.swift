//
//  contactVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 11/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField
import TransitionButton

//protocol ContactDataDelegate {
//   func sendContactData(text:String)
//}
class contactVCtrl: UIViewController {
    
    var delegate: SendDataDelegate?
    var spinnerView: UIView?
    let editpresenter = RegisterPresenter()
    var ContactOptionId:Int?
    let presenter = DegreeListPresenter()
    var countryList = [countryItem]()
    var selectedUserCountryId:Int?
    var selectedInstitutionCountryId:Int?
    var selectedCountryIndex: Int?
    var selectedCountry = [String]()
    var selectededuCountry = [String]()
    var selecteedCity = [String]()
    var selectedCityID: Int?
    var selectedCityIndex: Int?
    var cityList = [cityItem]()
    var saudiSelected = false
    //let countryPresenter = CountryListPresenter()
    
    var contactOtpionList = [PreferredContactItem]()
    var contactOtpionArray = ["Email".localized() , "Phone Call".localized(), "SMS".localized(), "In App Message".localized() ]
    var contactOtpion = ""
    
    var oldEmail:String =  ""
    var oldCity:String = ""
    
    @IBOutlet weak var chooseCityBtn: UIButton!
    @IBOutlet weak var city_arrow_ic: UIImageView!
    @IBOutlet weak var cityTxtField_trailing_constraint: NSLayoutConstraint!
    @IBOutlet weak var contactOption: TweeAttributedTextField!
    @IBOutlet weak var cityTxtFeild: TweeAttributedTextField!
    
    @IBOutlet weak var countryTxtFeild: TweeAttributedTextField!
    
    @IBOutlet weak var emailTxtFeild: TweeAttributedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPersonalInfoData()
        //countryPresenter.attachView(view: self)
        //countryPresenter.doGetCountryList()
        presenter.attachView(view: self)
        presenter.doGetcertificatesNewList()
        editpresenter.attachView(view: self)
        setupTextFields()
        self.view.changeFont()
    }
    func setupTextFields(){
        emailTxtFeild.delegate = self
        emailTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        emailTxtFeild.returnKeyType = .next
        emailTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        emailTxtFeild.infoFontSize = 14.0
        emailTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        emailTxtFeild.activeLineWidth = 2
        emailTxtFeild.animationDuration = 0.5
        emailTxtFeild.minimumPlaceholderFontSize = 15
        emailTxtFeild.originalPlaceholderFontSize = 15
        emailTxtFeild.placeholderDuration = 0.2
        emailTxtFeild.placeholderColor = .darkGray
        emailTxtFeild.placeholder = "Email".localized()
        
        countryTxtFeild.delegate = self
        countryTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        countryTxtFeild.returnKeyType = .next
        countryTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        countryTxtFeild.infoFontSize = 14.0
        countryTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        countryTxtFeild.activeLineWidth = 2
        countryTxtFeild.animationDuration = 0.5
        countryTxtFeild.minimumPlaceholderFontSize = 15
        countryTxtFeild.originalPlaceholderFontSize = 15
        countryTxtFeild.placeholderDuration = 0.2
        countryTxtFeild.placeholderColor = .darkGray
        countryTxtFeild.placeholder = "Residence Country".localized()
        
        cityTxtFeild.delegate = self
        cityTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        cityTxtFeild.returnKeyType = .next
        cityTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        cityTxtFeild.infoFontSize = 14.0
        cityTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        cityTxtFeild.activeLineWidth = 2
        cityTxtFeild.animationDuration = 0.5
        cityTxtFeild.minimumPlaceholderFontSize = 15
        cityTxtFeild.originalPlaceholderFontSize = 15
        cityTxtFeild.placeholderDuration = 0.2
        cityTxtFeild.placeholderColor = .darkGray
        cityTxtFeild.placeholder = "Residence City".localized()
        
        contactOption.delegate = self
        contactOption.textColor = UIColor(hexString: "#8c8c8c")
        contactOption.returnKeyType = .next
        contactOption.infoTextColor = UIColor(hexString: "#FF2600")
        contactOption.infoFontSize = 14.0
        contactOption.activeLineColor = UIColor(hexString: "#00AFDF")
        contactOption.activeLineWidth = 2
        contactOption.animationDuration = 0.5
        contactOption.minimumPlaceholderFontSize = 15
        contactOption.originalPlaceholderFontSize = 15
        contactOption.placeholderDuration = 0.2
        contactOption.placeholderColor = .darkGray
        contactOption.placeholder = "Preferred contact option".localized()
    }
    func getPersonalInfoData(){
        print("getPersonalInfoData")
        
        if let email = UserManager.sharedInstance.getUser()?.email {
            emailTxtFeild.text = email
            oldEmail = email
            
        }
        if let countryId = UserManager.sharedInstance.getUser()?.countryId {
            self.selectedUserCountryId = countryId
            
        }
        if let country = UserManager.sharedInstance.getUser()?.selectedCountry {
            print("selectedcountry : \(country)")
            self.countryTxtFeild.text = country
            
        }
        if let city = UserManager.sharedInstance.getUser()?.City_Residence_Name {
            
            cityTxtFeild.text = city
            oldCity = city
            
        }
        if let cityID = UserManager.sharedInstance.getUser()?.CityID {
            selectedCityID = cityID
            if cityID == 0 {
                self.chooseCityBtn.isHidden = true
                self.city_arrow_ic.isHidden = true
                self.cityTxtField_trailing_constraint.constant = 0
            }
        }
       
        
        if let contactId = UserManager.sharedInstance.getUser()?.PreferedContactID {
            self.ContactOptionId = contactId
            
        }
        
        
        if let contact = UserManager.sharedInstance.getUser()?.selectedContact {
            self.contactOption.text = contact
            
        }
        
        
        
    }
    
    @IBAction func chooseContactOption(_ sender: UIButton) {
        self.view.endEditing(true)
        let contactOptionPicker = UIPickerView()
        //        let contactArrayToDisplay = contactOtpionList
        
        let contactArrayToDisplay = L102Language.isRTL ? contactOtpionList.map {$0.nameAr} : contactOtpionList.map {$0.nameEn}
        
        let ids = contactOtpionList.map {$0.id}
        
        if contactArrayToDisplay.count > 0 {
            contactOptionPicker.addCustomPicker(contactArrayToDisplay as NSArray , sender , "Preferred contact option".localized(), 0) { (selectedIndex) in
                
                print(self.contactOtpionArray[selectedIndex])
                self.contactOption.text = contactArrayToDisplay[selectedIndex]
                self.ContactOptionId = ids[selectedIndex]
            }
        }else{
            print("no date in contactArrayToDisplay")
        }
        
        //            if selectedIndex == 0 {
        //                self.contactOtpion = "Email"
        //            }else if selectedIndex == 1 {
        //                self.contactOtpion = "Phone Call"
        //            }
        //            else if selectedIndex == 2 {
        //                self.contactOtpion = "SMS"
        //            }
        //            else if selectedIndex == 3 {
        //                self.contactOtpion = "In App Message"
        //            }
        
        
        //            else {
        //                self.contactOtpion = "female"
        //            }
        //        }
        
    }
    @IBAction func chooseCountry(_ sender: UIButton) {
        print("choose country")
        self.view.endEditing(true)
        //        self.resetCustomTextFieldsErrorMessages(self.userCountryTxtField)
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: true, countriesNames: countriesNames)
        }
        else{
            //            degreePresenter.doGetDegreeList()
        }
        
    }
    @IBAction func chooseCity(_ sender: UIButton) {
        print("choose country")
        self.view.endEditing(true)
        //        self.resetCustomTextFieldsErrorMessages(self.userCountryTxtField)
        let citiesNames = L102Language.isRTL ? cityList.map {$0.cityAr } : cityList.map {$0.cityEn}
        
        if citiesNames.count > 0 {
            showCitySelectPopover(sender: sender, isUserCountry: true, citiesNames: citiesNames)
        }
        else{
            //            degreePresenter.doGetDegreeList()
        }
        
    }
    @IBAction func closePopup(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveBtnAction(_ sender: TransitionButton) {
        
        if(validateLoginTextFields()) {
            let user = UserManager.sharedInstance.getUser()
//            var user:User?
            user?.email =  emailTxtFeild.text!
            user?.countryId = selectedUserCountryId
            user?.City_Residence = cityTxtFeild.text!
            user?.City_Residence_Name = cityTxtFeild.text!
            user?.CityID = self.selectedCityID
            user?.PreferedContactID = ContactOptionId
            user?.SectionID = 3
            editpresenter.doEditNewProfile(data: user!)
        }
        
        
    }
    
    
    func validateLoginTextFields() -> Bool {
        var valid = true
        if let email = self.emailTxtFeild.text , !email.isBlank {
            if !email.isEmail{
                let errorMessage = "please enter valid email".localized()
                self.emailTxtFeild.showInfo(errorMessage, animated: true)
                valid = false
            }else {
                print("valid email")
            }
        }else {
            print("error email")
            let errorMessage = "please enter your email address".localized()
            self.emailTxtFeild.showInfo(errorMessage, animated: true)
            valid = false
        }
//        if let userCountry = self.countryTxtFeild.text , !userCountry.isBlank {
//            print("valid user country")
//        }else {
//            print("error user country")
//            self.countryTxtFeild.showInfo("please enter your Residence Country".localized())
//            valid = false
//        }
//        
//        if let userCity = self.cityTxtFeild.text , !userCity.isBlank {
//            print("valid user country")
//        }else {
//            print("error user country")
//            self.cityTxtFeild.showInfo("please enter your residence city".localized())
//            valid = false
//        }
//        if let contactOptions = self.contactOption.text , !contactOptions.isBlank {
//            print("valid user country")
//        }else {
//            print("error user country")
//            self.contactOption.showInfo("please enter your preferred contact option".localized())
//            valid = false
//        }
        return valid
    }
    
    
    func showCitySelectPopover(sender: UIView,isUserCountry:Bool, citiesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: citiesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        selectionMenu.setSelectedItems(items: selecteedCity) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCity  = self!.cityList.first(where: {$0.cityAr == text!!}) {
                    if let selectedCityID = selectedCity.id{
                            self!.selectedCityID = selectedCityID
                            self!.cityTxtFeild.text = text!!
                    }
                }
            }
            else{
                if let selectedCity  = self!.cityList.first(where: {$0.cityEn == text!!}) {
                    if let selectedCityID = selectedCity.id{
                            self!.selectedCityID = selectedCityID
                            self!.cityTxtFeild.text = text!!
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return citiesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    
    func showCountrySelectPopover(sender: UIView,isUserCountry:Bool, countriesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: countriesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        if isUserCountry {
            selectedCountryName = selectedCountry
        }
        else{
            selectedCountryName = selectededuCountry
        }
        selectionMenu.setSelectedItems(items: selectedCountryName) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCountry  = self!.countryList.first(where: {$0.nameAr == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.countryTxtFeild.text = text!!
                            if (selectedCountry.cities?.count ?? 0) > 0 {
                                self?.chooseCityBtn.isHidden = false
                                self?.city_arrow_ic.isHidden = false
                                self?.cityTxtField_trailing_constraint.constant = 33
                                self?.cityTxtFeild.text = ""
                                self?.cityList = selectedCountry.cities ?? []
                            }
                            else{
                                self?.chooseCityBtn.isHidden = true
                                self?.city_arrow_ic.isHidden = true
                                self?.cityTxtField_trailing_constraint.constant = 0
                                self?.cityTxtFeild.text = ""
                            }
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            
                        }
                    }
                    
                }
            }
            else{
                if let selectedCountry  = self!.countryList.first(where: {$0.nameEn == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.countryTxtFeild.text = text!!
                            if (selectedCountry.cities?.count ?? 0) > 0 {
                                self?.chooseCityBtn.isHidden = false
                                self?.city_arrow_ic.isHidden = false
                                self?.cityTxtField_trailing_constraint.constant = 33
                                self?.cityTxtFeild.text = ""
                                self?.selectedCityID = 0
                                self?.cityList = selectedCountry.cities ?? []
                            }
                            else{
                                self?.chooseCityBtn.isHidden = true
                                self?.city_arrow_ic.isHidden = true
                                self?.cityTxtField_trailing_constraint.constant = 0
                                self?.cityTxtFeild.text = ""
                                self?.selectedCityID = 0
                            }
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return countriesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    
}
extension contactVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.emailTxtFeild {
            self.emailTxtFeild.showInfo("")
        }
        else if textField == self.countryTxtFeild {
            self.countryTxtFeild.showInfo("")
        }
        else if textField == self.cityTxtFeild {
            self.cityTxtFeild.showInfo("")
        }
        else if textField == self.contactOption {
            self.contactOption.showInfo("")
        }
    }
//
    }
extension contactVCtrl: CountryListView{
//    func onError(message: String) {
//        print("onError")
//        self.showErrorMessage(message: message)
//    }
    
    func onSucessLoadingCountryList(countries: [CountryRes]) {
        print("countries count : \(countries.count)")
        self.countryList.removeAll()
        //        self.countryList.append(contentsOf: countries)
        for item in countries{
            self.countryList.append(countryItem(id: item.id, nameEn: item.nameEn, nameAr: item.nameAr, code: item.isoCode,cities: [cityItem(id: 1, cityEn: "x", cityAr: "x")]))
        }
        print("country list : \(self.countryList)")
        
        if let countryId = UserManager.sharedInstance.getUser()?.countryId {
            self.selectedUserCountryId = countryId
            
            for item in self.countryList {
                if countryId == item.id {
                    self.countryTxtFeild.text = L102Language.isRTL ? item.nameAr : item.nameEn
                }
            }
            
        }
        
        
        
        //        self.countryList = countries
    }
}


extension contactVCtrl :DegreeListView {
    func showLoading() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        if let countries = degreeWorkingTimes.countries {
            self.countryList = countries
            if let countryId = UserManager.sharedInstance.getUser()?.countryId {
                self.selectedUserCountryId = countryId
                
                for item in self.countryList {
                    if countryId == item.id {
                        self.countryTxtFeild.text = L102Language.isRTL ? item.nameAr : item.nameEn
                        self.cityList = item.cities ?? []
                    }
                }
                
            }
        }
        if let preferedContact = degreeWorkingTimes.PreferedContact {
            if preferedContact.count > 0 {
                self.contactOtpionList.removeAll()
                self.contactOtpionList.append(contentsOf: preferedContact)
                print("self.contactOtpionList : \(contactOtpionList)")
            }
            
        }
        if let contactId = UserManager.sharedInstance.getUser()?.PreferedContactID {
            self.ContactOptionId = contactId
            
            for item in self.contactOtpionList {
                if contactId == item.id {
                    self.contactOption.text = L102Language.isRTL ? item.nameAr : item.nameEn
                }
            }
        }
    }
    
}

extension contactVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucess() {
        print("on sucess")
        let user1  = UserManager.sharedInstance.getUser()
        user1?.email =  emailTxtFeild.text!
        user1?.countryId = selectedUserCountryId
        user1?.City_Residence = cityTxtFeild.text!
        user1?.City_Residence_Name = cityTxtFeild.text!
        user1?.PreferedContactID = ContactOptionId
        user1?.selectedCountry = countryTxtFeild.text!
        user1?.selectedContact = contactOption.text
        
        UserManager.sharedInstance.saveUser(user:user1!)
        
        self.delegate?.sendData(text: "text")
        
        self.dismiss(animated: true, completion: nil)
        
    }
    func onError(message: String) {
        print("onError")
        self.showErrorMessage(message: message)
        
        let user1  = UserManager.sharedInstance.getUser()
        user1?.email =  oldEmail
        user1?.City_Residence_Name = oldCity
        
        UserManager.sharedInstance.saveUser(user:user1!)
        
//        self.delegate?.sendData(text: "text")
        
//        self.delegate?.sendData(text: "text2error")
    }
}
