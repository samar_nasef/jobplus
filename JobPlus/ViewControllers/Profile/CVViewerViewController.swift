//
//  CVViewerViewController.swift
//  JobPlus
//
//  Created by elsaid yousif on 4/21/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import WebKit

class CVViewerViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var cvURL = ""
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var webToLoadWebView: UIView!
    
    
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        webView = WKWebView(frame: webToLoadWebView.bounds, configuration: WKWebViewConfiguration())
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webToLoadWebView.addSubview(webView)
        if let url = URL(string: cvURL ){
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
