//
//  RegisterViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/11/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import ALCameraViewController
import libPhoneNumber_iOS
import TweeTextField
import MobileCoreServices

class EditProfileViewController: UIViewController, addExperienceDelegate, UIDocumentPickerDelegate {
    func experienceAdded(val: String) {
        self.loadExperience()
    }
    
    var selectedProfileImage :UIImage?
    //@IBOutlet weak var placeholderProfileImageView: UIImageView!
    @IBOutlet weak var addExpBtn: UIButton!
    @IBOutlet weak var profieImageView: UIImageView!
    @IBOutlet weak var birthDateTextField: TweeAttributedTextField!
    @IBOutlet weak var notificationCountLbl: UILabel!
    @IBOutlet weak var DegreeView: UIView!
    @IBOutlet weak var educationCityView: UIView!
    @IBOutlet weak var educationCountryView: UIView!
    @IBOutlet weak var instituteView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var birthDateView: UIView!
    @IBOutlet weak var fullNameView: UIView!
    @IBOutlet weak var fullNameTxtField: TweeAttributedTextField!
    @IBOutlet weak var emailTxtField: TweeAttributedTextField!
    @IBOutlet weak var genderTxtField: TweeAttributedTextField!
    @IBOutlet weak var registerBtn: TransitionButton!
    @IBOutlet weak var institutionTxtField: TweeAttributedTextField!
    @IBOutlet weak var MajorTxtField: TweeAttributedTextField!
    @IBOutlet weak var educationCountryTxtField: TweeAttributedTextField!
    @IBOutlet weak var userCountryTxtField: TweeAttributedTextField!
    @IBOutlet weak var degreeTxtField: TweeAttributedTextField!
    @IBOutlet weak var experienceTableView: UITableView!
    @IBOutlet weak var navArrow: UIButton!
    
    @IBOutlet weak var backBtnView: UIView!
    @IBOutlet weak var userImgContainerView: UIView!
    
    @IBOutlet weak var fullname_bottom_constrain: NSLayoutConstraint!
    @IBOutlet weak var birthdate_bottom_constrain: NSLayoutConstraint!
    @IBOutlet weak var email_bottom_constrain: NSLayoutConstraint!
    @IBOutlet weak var institute_bottom_constrain: NSLayoutConstraint!
    @IBOutlet weak var edu_country_constrain: NSLayoutConstraint!
    @IBOutlet weak var edu_city_bottom_constrain: NSLayoutConstraint!
    @IBOutlet weak var degree_bottom_constrain: NSLayoutConstraint!
    @IBOutlet weak var tbHeight: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var uploadedCVBtn: UIButton!
    @IBOutlet weak var fileToUploadBtn: UIButton!
    @IBOutlet weak var CvStackView: UIStackView!
    @IBOutlet weak var contact_top_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var WorkingTimesView: UIView!
    @IBOutlet weak var workingTimesTxtField: TweeAttributedTextField!
    @IBOutlet weak var userCityView: UIView!
    @IBOutlet weak var userCityTxtField: TweeAttributedTextField!
    @IBOutlet weak var country_bottom_constraint: NSLayoutConstraint!
    
    var documentInteractionController: UIDocumentInteractionController!
    let allowedFileExtension = ["doc","docx","pdf"]
    var CVbase64String = ""
    var CVExtension = ""
    var CVUrl = ""
    var cvPrevUploaded = false
    var cvPrevUrl = ""
    var expIDToEdit = -1
    var cvViewerType = 1 // 1 -> open new file , 2 -> open uploaded file
    //    @IBOutlet var companyNameTextFields: [CustomTextFiled]!
    //    @IBOutlet var jobTitleTextFields: [CustomTextFiled]!
    //    @IBOutlet var yearsOfExperiencetextFields: [CustomTextFiled]!
    
    ////////////////////////////////////////////////
    var birthDateTime:Int?
    var genderArray = ["Male".localized() , "Female".localized()]
    var gender = ""
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = true
    var allowResizing: Bool = true
    var allowMoving: Bool = true
    var minimumSize: CGSize = CGSize(width: 200, height: 200)
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    var countryList = [countryItem]()
    var selectedUserCountryId:Int?
    var selectedInstitutionCountryId:Int?
    var degreeList = [certificate]()
    var workingTimes = [worktims]()
    var experience: Array<Experience> = []
    var selectedDegreeKey = 0 //"Other"{
//        didSet{
//            self.showOtherTextField(show: (selectedDegreeKey == self.degreeList[5].id) )
//        }
//    }
    let presenter = RegisterPresenter()
    let countryPresenter = CountryListPresenter()
    let degreePresenter = DegreeListPresenter()
    var experienceObject: ExperienceObjectToPost? = nil
    var user_mobile = ""
    var experienceArr = [Experience]()
    var openProfile = false
    var selectedCountryIndex: Int?
    var selectedCountry = [String]()
    var selectededuCountry = [String]()
    var certificate_id = -1
    var worktime_id = -1
    override func viewDidAppear(_ animated: Bool) {
        //        let notiCount = UserManager.sharedInstance.notificationCount
        //        if notiCount > 0 {
        //            self.notificationCountLbl.text = (" \(String(notiCount)) ")
        //            self.notificationCountLbl.isHidden = false
        //        }
        //        else{
        //             self.notificationCountLbl.isHidden = true
        //        }
        addExpBtn.backgroundColor = .white
        backBtnView.backgroundColor = .white
        print(self.CvStackView.layer.frame.height)
        self.contact_top_height_constraint.constant = self.CvStackView.layer.frame.height + 10
    }
    //    @IBAction func notificationOpenAction(_ sender: Any) {
    //       if UserManager.sharedInstance.getUser() != nil {
    //           if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
    //
    //               if let navigator = navigationController {
    //                   navigator.pushViewController(viewController, animated: true)
    //               }
    //           }
    //       }
    //       else{
    //           self.showErrorMessage(message: "you have to login first".localized())
    //        }
    //
    //    }
    @IBAction func fileUpload(_ sender: Any) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
        documentPicker.delegate = self

        self.present(documentPicker, animated: true)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {

    print(urls)
       
        let file_name = urls[0].lastPathComponent
        let file_extension = file_name.split(separator: ".")
        CVExtension = String(file_extension[file_extension.count - 1])
        let index = allowedFileExtension.firstIndex(of: CVExtension)
        
        if index ?? -1 >= 0 {
            self.CVUrl = urls[0].absoluteString
            let data = try! Data(contentsOf: urls[0])
            CVbase64String = data.base64EncodedString()
            let cvMsg = NSLocalizedString("New CV Uploaded", comment: "")
            self.fileToUploadBtn.setTitle(cvMsg, for: .normal)
            self.fileToUploadBtn.setTitle(cvMsg, for: .selected)
            DispatchQueue.main.async {
                self.fileToUploadBtn.isHidden = false
                if self.cvPrevUploaded {
                    self.contact_top_height_constraint.constant = 78
                }
                else{
                self.contact_top_height_constraint.constant = 42
                }
            }
            
        }
        else{
            self.showErrorMessage(message: NSLocalizedString("Not Allowed Extention", comment: ""))
        }
        
    }
    @IBAction func openUploadedCv(_ sender: Any) {
        cvViewerType = 2
        performSegue(withIdentifier: "openCVSeg", sender: self)
        
    }
    @IBAction func openFileToUpload(_ sender: Any) {
        cvViewerType = 1
         performSegue(withIdentifier: "openCVSeg", sender: self)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        
        self.view.changeFont()
         
        //registerBtn.dropShadow()
        setupTextFields()
        presenter.attachView(view: self)
        //countryPresenter.attachView(view: self)
        degreePresenter.attachView(view: self)
        //countryPresenter.doGetCountryList()
        degreePresenter.doGetDegreeList()
        userImgContainerView.layerGradient()
        profieImageView.layer.cornerRadius = 47
        experienceTableView.delegate = self
        experienceTableView.dataSource = self
        //        NotificationCenter.default.addObserver(self,
        //                                               selector: #selector(self.loadExperience),
        //                                               name: NSNotification.Name(rawValue: "loadExperience"),
        //                                               object: nil)
        setProfileData()
        if UserManager.sharedInstance.userRegisterForFirstTime == 1 {
            self.showSucessMessage(message: "completeProfileMsg".localized())
            UserManager.sharedInstance.userRegisterForFirstTime = 0
            UserManager.sharedInstance.openProfile = true
        }
        // self.addExpBtn.backgroundColor = UIColor.white
    }
    
    @objc func loadExperience(){
        if UserManager.sharedInstance.experience.count > 0 {
            for item in UserManager.sharedInstance.experience {
                if self.expIDToEdit > -1 {
                    self.experience[expIDToEdit].companyName = item.companyName
                    self.experience[expIDToEdit].jobTitle = item.jobTitle
                    self.experience[expIDToEdit].yearsOfExperience = item.yearsOfExperience
                    self.expIDToEdit = -1
                    DispatchQueue.main.async {
                        self.experienceTableView.reloadData()
                    }
                }
                else{
                    self.experience.append(item)
                }
            }
            UserManager.sharedInstance.experience = [Experience]()
            self.experienceTableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // checkCountryCodeEmpty()
        //self.navigationController?.setupNavigationControllerOfPartTime()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupTextFields(){
        
        fullNameView.layer.cornerRadius = 2
        fullNameView.layer.shadowColor = UIColor.black.cgColor
        fullNameView.layer.shadowOpacity = 0.2
        fullNameView.layer.shadowOffset = .zero
        fullNameView.layer.shadowRadius = 10
        
        WorkingTimesView.layer.cornerRadius = 2
        WorkingTimesView.layer.shadowColor = UIColor.black.cgColor
        WorkingTimesView.layer.shadowOpacity = 0.2
        WorkingTimesView.layer.shadowOffset = .zero
        WorkingTimesView.layer.shadowRadius = 10
        
        userCityView.layer.cornerRadius = 2
        userCityView.layer.shadowColor = UIColor.black.cgColor
        userCityView.layer.shadowOpacity = 0.2
        userCityView.layer.shadowOffset = .zero
        userCityView.layer.shadowRadius = 10
        
        birthDateView.layer.cornerRadius = 2
        birthDateView.layer.shadowColor = UIColor.black.cgColor
        birthDateView.layer.shadowOpacity = 0.2
        birthDateView.layer.shadowOffset = .zero
        birthDateView.layer.shadowRadius = 10
        
        genderView.layer.cornerRadius = 2
        genderView.layer.shadowColor = UIColor.black.cgColor
        genderView.layer.shadowOpacity = 0.2
        genderView.layer.shadowOffset = .zero
        genderView.layer.shadowRadius = 10
        
        contactView.layer.cornerRadius = 2
        contactView.layer.shadowColor = UIColor.black.cgColor
        contactView.layer.shadowOpacity = 0.2
        contactView.layer.shadowOffset = .zero
        contactView.layer.shadowRadius = 10
        
        countryView.layer.cornerRadius = 2
        countryView.layer.shadowColor = UIColor.black.cgColor
        countryView.layer.shadowOpacity = 0.2
        countryView.layer.shadowOffset = .zero
        countryView.layer.shadowRadius = 10
        
        instituteView.layer.cornerRadius = 2
        instituteView.layer.shadowColor = UIColor.black.cgColor
        instituteView.layer.shadowOpacity = 0.2
        instituteView.layer.shadowOffset = .zero
        instituteView.layer.shadowRadius = 10
        
        educationCountryView.layer.cornerRadius = 2
        educationCountryView.layer.shadowColor = UIColor.black.cgColor
        educationCountryView.layer.shadowOpacity = 0.2
        educationCountryView.layer.shadowOffset = .zero
        educationCountryView.layer.shadowRadius = 10
        
        educationCityView.layer.cornerRadius = 2
        educationCityView.layer.shadowColor = UIColor.black.cgColor
        educationCityView.layer.shadowOpacity = 0.2
        educationCityView.layer.shadowOffset = .zero
        educationCityView.layer.shadowRadius = 10
        
        DegreeView.layer.cornerRadius = 2
        DegreeView.layer.shadowColor = UIColor.black.cgColor
        DegreeView.layer.shadowOpacity = 0.2
        DegreeView.layer.shadowOffset = .zero
        DegreeView.layer.shadowRadius = 10
        
        
        fullNameTxtField.delegate = self
        fullNameTxtField.textColor = UIColor(hexString: "#8c8c8c")
        fullNameTxtField.returnKeyType = .next
        fullNameTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        fullNameTxtField.infoFontSize = 14.0
        fullNameTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        fullNameTxtField.activeLineWidth = 2
        fullNameTxtField.animationDuration = 0.5
        fullNameTxtField.minimumPlaceholderFontSize = 15
        fullNameTxtField.originalPlaceholderFontSize = 15
        fullNameTxtField.placeholderDuration = 0.2
        fullNameTxtField.placeholderColor = .darkGray
        fullNameTxtField.placeholder = "Full Name".localized()
        
        emailTxtField.delegate = self
        emailTxtField.textColor = UIColor(hexString: "#8c8c8c")
        emailTxtField.returnKeyType = .next
        emailTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        emailTxtField.infoFontSize = 14.0
        emailTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        emailTxtField.activeLineWidth = 2
        emailTxtField.animationDuration = 0.5
        emailTxtField.minimumPlaceholderFontSize = 15
        emailTxtField.originalPlaceholderFontSize = 15
        emailTxtField.placeholderDuration = 0.2
        emailTxtField.placeholderColor = .darkGray
        emailTxtField.placeholder = "Email Address".localized()
        
        birthDateTextField.delegate = self
        birthDateTextField.textColor = UIColor(hexString: "#8c8c8c")
        birthDateTextField.infoTextColor = UIColor(hexString: "#FF2600")
        birthDateTextField.infoFontSize = 14.0
        birthDateTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        birthDateTextField.activeLineWidth = 2
        birthDateTextField.animationDuration = 0.5
        birthDateTextField.minimumPlaceholderFontSize = 15
        birthDateTextField.originalPlaceholderFontSize = 15
        birthDateTextField.placeholderDuration = 0.2
        birthDateTextField.placeholderColor = .darkGray
        birthDateTextField.placeholder = "Birth Date".localized()
        
        workingTimesTxtField.delegate = self
        workingTimesTxtField.textColor = UIColor(hexString: "#8c8c8c")
        workingTimesTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        workingTimesTxtField.infoFontSize = 14.0
        workingTimesTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        workingTimesTxtField.activeLineWidth = 2
        workingTimesTxtField.animationDuration = 0.5
        workingTimesTxtField.minimumPlaceholderFontSize = 15
        workingTimesTxtField.originalPlaceholderFontSize = 15
        workingTimesTxtField.placeholderDuration = 0.2
        workingTimesTxtField.placeholderColor = .darkGray
        workingTimesTxtField.placeholder = "Working Times".localized()
        
        genderTxtField.delegate = self
        genderTxtField.textColor = UIColor(hexString: "#8c8c8c")
        genderTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        genderTxtField.infoFontSize = 14.0
        genderTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        genderTxtField.activeLineWidth = 2
        genderTxtField.animationDuration = 0.5
        genderTxtField.minimumPlaceholderFontSize = 15
        genderTxtField.originalPlaceholderFontSize = 15
        genderTxtField.placeholderDuration = 0.2
        genderTxtField.placeholderColor = .darkGray
        genderTxtField.placeholder = "Gendure".localized()
        
        userCountryTxtField.delegate = self
        userCountryTxtField.textColor = UIColor(hexString: "#8c8c8c")
        userCountryTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        userCountryTxtField.infoFontSize = 14.0
        userCountryTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        userCountryTxtField.activeLineWidth = 2
        userCountryTxtField.animationDuration = 0.5
        userCountryTxtField.minimumPlaceholderFontSize = 15
        userCountryTxtField.originalPlaceholderFontSize = 15
        userCountryTxtField.placeholderDuration = 0.2
        userCountryTxtField.placeholderColor = .darkGray
        userCountryTxtField.placeholder = "Residence Country".localized()
        
        userCityTxtField.delegate = self
        userCityTxtField.textColor = UIColor(hexString: "#8c8c8c")
        userCityTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        userCityTxtField.infoFontSize = 14.0
        userCityTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        userCityTxtField.activeLineWidth = 2
        userCityTxtField.animationDuration = 0.5
        userCityTxtField.minimumPlaceholderFontSize = 15
        userCityTxtField.originalPlaceholderFontSize = 15
        userCityTxtField.placeholderDuration = 0.2
        userCityTxtField.placeholderColor = .darkGray
        userCityTxtField.placeholder = "Residence City".localized()
        
        educationCountryTxtField.delegate = self
        educationCountryTxtField.textColor = UIColor(hexString: "#8c8c8c")
        educationCountryTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        educationCountryTxtField.infoFontSize = 14.0
        educationCountryTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        educationCountryTxtField.activeLineWidth = 2
        educationCountryTxtField.animationDuration = 0.5
        educationCountryTxtField.minimumPlaceholderFontSize = 15
        educationCountryTxtField.originalPlaceholderFontSize = 15
        educationCountryTxtField.placeholderDuration = 0.2
        educationCountryTxtField.placeholderColor = .darkGray
        educationCountryTxtField.placeholder = "Country".localized()
        
        MajorTxtField.delegate = self
        MajorTxtField.textColor = UIColor(hexString: "#8c8c8c")
        MajorTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        MajorTxtField.infoFontSize = 14.0
        MajorTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        MajorTxtField.activeLineWidth = 2
        MajorTxtField.animationDuration = 0.5
        MajorTxtField.minimumPlaceholderFontSize = 15
        MajorTxtField.originalPlaceholderFontSize = 15
        MajorTxtField.placeholderDuration = 0.2
        MajorTxtField.placeholderColor = .darkGray
        MajorTxtField.placeholder = "Major".localized()
        
        institutionTxtField.delegate = self
        institutionTxtField.textColor = UIColor(hexString: "#8c8c8c")
        institutionTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        institutionTxtField.infoFontSize = 14.0
        institutionTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        institutionTxtField.activeLineWidth = 2
        institutionTxtField.animationDuration = 0.5
        institutionTxtField.minimumPlaceholderFontSize = 15
        institutionTxtField.originalPlaceholderFontSize = 15
        institutionTxtField.placeholderDuration = 0.2
        institutionTxtField.placeholderColor = .darkGray
        institutionTxtField.placeholder = "University / Institution".localized()
        
        degreeTxtField.delegate = self
        degreeTxtField.textColor = UIColor(hexString: "#8c8c8c")
        degreeTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        degreeTxtField.infoFontSize = 14.0
        degreeTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        degreeTxtField.activeLineWidth = 2
        degreeTxtField.animationDuration = 0.5
        degreeTxtField.minimumPlaceholderFontSize = 15
        degreeTxtField.originalPlaceholderFontSize = 15
        degreeTxtField.placeholderDuration = 0.2
        degreeTxtField.placeholderColor = .darkGray
        degreeTxtField.placeholder = "Degree".localized()
    }
    func setProfileData(){
        if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
            if let profileImgUrl = URL(string: profileImg ){
                
                self.profieImageView.kf.setImage(with: profileImgUrl, completionHandler: {
//                    (image, error, cacheType, imageUrl) in
//                    if error != nil {
//                        self.profieImageView.image = image
//                        self.profieImageView.image = image
//                    }
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
//                        self.userImage.image = result
                        
                    }
                })
            }
        }
        else {
            if let gender = UserManager.sharedInstance.getUser()?.gender {
                if gender == "female" {
                    self.profieImageView.image = UIImage(named: "woman")
                }
                else{
                    self.profieImageView.image = UIImage(named: "profile_img")
                }
            }
        }
        if let mobile = UserManager.sharedInstance.getUser()?.mobile {
            
            if let countryCode = UserManager.sharedInstance.getUser()?.mobileCountryPrefix {
                self.user_mobile =  "\(countryCode)\(mobile)"
                
                
            }
            
        }
        
        if let name = UserManager.sharedInstance.getUser()?.name {
            fullNameTxtField.text = name
        }
        if let cvPath = UserManager.sharedInstance.getUser()?.cvPath, cvPath.count > 0 {
            self.cvPrevUploaded = true
            self.cvPrevUrl = cvPath
            let cvMsg = NSLocalizedString("Show CV", comment: "")
            self.uploadedCVBtn.setTitle(cvMsg, for: .normal)
            self.uploadedCVBtn.setTitle(cvMsg, for: .selected)
            DispatchQueue.main.async {
                self.uploadedCVBtn.isHidden = false
                self.contact_top_height_constraint.constant = 42
            }
        }
        
        if let age = UserManager.sharedInstance.getUser()?.birthDate {
            birthDateTextField.text = Int(age)!.getCurrentDateString()
            birthDateTime = Int(age)
        }
        if let email = UserManager.sharedInstance.getUser()?.email {
            emailTxtField.text = email
        }
        
        
        if let gendure = UserManager.sharedInstance.getUser()?.gender {
            self.gender = gendure
            genderTxtField.text = gendure.capitalized.localized()
        }
        
        if let userCountry = UserManager.sharedInstance.getUser()?.country{
            if let userCountryName = userCountry.localizeName {
                self.selectedCountry.append(userCountryName)
                userCountryTxtField.text = userCountryName
            }
            if let userCountryId = userCountry.id{
                self.selectedUserCountryId = userCountryId
            }
            
        }
        if let educationCountry = UserManager.sharedInstance.getUser()?.educCountry{
            if let educationCountryName = educationCountry.localizeName {
                self.selectededuCountry.append(educationCountryName)
                educationCountryTxtField.text = educationCountryName
            }
            if let educationCountryId = educationCountry.id{
                self.selectedInstitutionCountryId = educationCountryId
            }
            
        }
        if let educationMajor = UserManager.sharedInstance.getUser()?.educMajor{
            MajorTxtField.text = educationMajor
        }
        if let educationDegreeKey = UserManager.sharedInstance.getUser()?.educDegree{
            //            self.selectedDegreeKey = educationDegreeKey
            //            if let degree = self.degreeList.first(where: {$0.key == self.selectedDegreeKey}) {
            //                if let degreeName = degree.localizeName{
            self.degreeTxtField.text = educationDegreeKey
            //                }
            //            }
        }
        if let City_Residence = UserManager.sharedInstance.getUser()?.City_Residence {
            self.userCityTxtField.text = City_Residence
        }
        if let educationName = UserManager.sharedInstance.getUser()?.educInstitution{
            self.institutionTxtField.text = educationName
        }
        
        if let experienceArr = UserManager.sharedInstance.getUser()?.experiences {
            if experienceArr.count > 0 {
                self.experience = experienceArr
                self.experienceTableView.reloadData()
            }
        }
        
    }
    
    
    
    private func getCustomTextFieldInputAccessoryView(with items: [UIBarButtonItem]) -> UIToolbar {
        let toolbar: UIToolbar = UIToolbar()
        
        toolbar.barStyle = UIBarStyle.default
        toolbar.items = items
        toolbar.sizeToFit()
        return toolbar
    }
    
    @IBAction func registerAction(_ sender: Any) {
        var base64Image:String?
        if(selectedProfileImage != nil){
            base64Image = (selectedProfileImage?.resizedImageWithinRect(rectSize: CGSize(width: 200, height: 200)).toBase64())!
        }
        if(validateLoginTextFields()) {
            let phone = self.user_mobile
            presenter.doEditProfile(name: fullNameTxtField.text!,
                                    email : emailTxtField.text!,
                                    mobile : phone.replacingOccurrences(of: " ", with: ""),
                                    gender : self.gender,
                                    birthDate : birthDateTime ,
                                    mobileApplicationLocale : L102Language.currentAppleLanguage(),
                                    image: base64Image,
                                    userCountryId: self.selectedUserCountryId! ,
                                    educationCountryId :self.selectedInstitutionCountryId! ,
                                    educationName:self.institutionTxtField.text!,
                                    educationCity:"eduCity",
                                    educationDegreeKey:self.degreeTxtField.text!,
                                    educationOtherDegree:"educationOtherDegree",
                                    educationMajor:self.MajorTxtField.text!,
                                    experienceObject: experienceObject!,
                                    cv_extension: self.CVExtension,
                                    cv: self.CVbase64String, certificate_id: self.certificate_id,
                                    worktime_id: self.worktime_id,City_Residence: self.userCityTxtField.text ?? "")
        }
    }
    @IBAction func chooseGenderBtnAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        let genderPicker = UIPickerView()
        let genderArrayToDisplay = ["Male".localized() , "Female".localized()]
        genderPicker.addCustomPicker(genderArrayToDisplay as NSArray , sender , "Gender", 0) { (selectedIndex) in
            
            print(self.genderArray[selectedIndex])
            self.genderTxtField.text = genderArrayToDisplay[selectedIndex]
            if selectedIndex == 0 {
                self.gender = "male"
            }else {
                self.gender = "female"
            }
        }
    }
    
    @IBAction func birthDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.show(inVC: self)
    }
    
    
    @IBAction func chooseEducationCountryAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.resetCustomTextFieldsErrorMessages(self.educationCountryTxtField)
        //countryPicker(sender: sender, isUserCountry: false)
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: false, countriesNames: countriesNames)
        }
        else{
            degreePresenter.doGetDegreeList()
        }
    }
    
    @IBAction func chooseUserCountryAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.resetCustomTextFieldsErrorMessages(self.userCountryTxtField)
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        //countryPicker(sender: sender, isUserCountry: true)
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: true, countriesNames: countriesNames)
        }
        else{
            degreePresenter.doGetDegreeList()
        }
    }
    
    @IBAction func chooseWorkingTimesAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.workingTimesTxtField.showInfo("")
        self.resetCustomTextFieldsErrorMessages(self.workingTimesTxtField)
        let degreePicker = UIPickerView()
        if  workingTimes.count > 0 {
            var Names = [String]()
             if L102Language.isRTL {
                Names = workingTimes.map { ($0.worktimeAr ?? "")}
            }
             else{
                Names = workingTimes.map { ($0.worktimeEn ?? "")}
            }
            degreePicker.addCustomPicker(Names as NSArray , sender  , "Working Times".localized(), 0) { (selectedIndex) in
                
                let selectedWorkingTime = self.workingTimes[selectedIndex]
                if let selectedId_ = selectedWorkingTime.id {
                    self.worktime_id = selectedId_
                }
                if L102Language.isRTL {
                    if let selectedName = selectedWorkingTime.worktimeAr{
                        self.workingTimesTxtField.text = selectedName
                        
                    }
                }
                else{
                    if let selectedName = selectedWorkingTime.worktimeEn{
                        self.workingTimesTxtField.text = selectedName
                    }
                }
                
            }
        }
        else{
            degreePresenter.doGetDegreeList()
        }
    }
    @IBAction func chooseDegreeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.resetCustomTextFieldsErrorMessages(self.degreeTxtField)
        let degreePicker = UIPickerView()
        if  degreeList.count > 0 {
            var degreeNames = [String]()
             if L102Language.isRTL {
                degreeNames = degreeList.map { ($0.certificateAr ?? "")}
            }
             else{
                degreeNames = degreeList.map { ($0.certificateEn ?? "")}
            }
            degreePicker.addCustomPicker(degreeNames as NSArray , sender  , "Degree".localized(), 0) { (selectedIndex) in
                
                let selectedDegree = self.degreeList[selectedIndex]
                if let selectedId_ = selectedDegree.id {
                    self.certificate_id = selectedId_
                }
                if L102Language.isRTL {
                    if let selectedName = selectedDegree.certificateAr{
                        self.degreeTxtField.text = selectedName
                    }
                }
                else{
                    if let selectedName = selectedDegree.certificateEn{
                        self.degreeTxtField.text = selectedName
                    }
                }
            }
        }
        else{
            degreePresenter.doGetDegreeList()
        }
    }
    
    func countryPicker(sender:UIButton,isUserCountry:Bool) {
        let countryPicker = UIPickerView()
        if  countryList.count > 0 {
            let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
            countryPicker.addCustomPicker(countriesNames as NSArray , sender  , "Country".localized(), 0) { (selectedIndex) in
                
                if let selectedId_ = self.countryList[selectedIndex].id {
                    
                    if let selectedCountry  = self.countryList.first(where: {$0.id == selectedId_}) {
                        if let selectedCountryName = L102Language.isRTL ? selectedCountry.nameAr : selectedCountry.nameEn  {
                            if isUserCountry {
                                self.selectedUserCountryId = selectedId_
                                self.userCountryTxtField.text = selectedCountryName
                            }else{
                                self.selectedInstitutionCountryId = selectedId_
                                self.educationCountryTxtField.text = selectedCountryName
                            }
                            
                        }
                    }
                    
                }
                
            }
        }
    }
    func showCountrySelectPopover(sender: UIView,isUserCountry:Bool, countriesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: countriesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        if isUserCountry {
            selectedCountryName = selectedCountry
        }
        else{
            selectedCountryName = selectededuCountry
        }
        selectionMenu.setSelectedItems(items: selectedCountryName) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
            if let selectedCountry  = self!.countryList.first(where: {$0.nameAr == text!!}) {
                if let selectedCountryID = selectedCountry.id{
                    if isUserCountry {
                        self!.selectedUserCountryId = selectedCountryID
                        self!.userCountryTxtField.text = text!!
                    }else{
                        self!.selectedInstitutionCountryId = selectedCountryID
                        self!.educationCountryTxtField.text = text!!
                    }
                }
                
            }
            }
            else{
                if let selectedCountry  = self!.countryList.first(where: {$0.nameEn == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.userCountryTxtField.text = text!!
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            self!.educationCountryTxtField.text = text!!
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return countriesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
        selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
      selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    func showOtherTextField(show:Bool){
        
    }
    func validateLoginTextFields() -> Bool {
        var valid = true
        let nameTxt = self.fullNameTxtField.text ?? ""
        let trimedTxt = nameTxt.components(separatedBy: .whitespaces).joined()
        if let fullName = self.fullNameTxtField.text , !fullName.isBlank {
            if !fullName.isNameMoreThanTwoCharacters {
                self.fullNameTxtField.showInfo("Full name must be more than four characters".localized())
                valid = false
                self.fullname_bottom_constrain.constant = 35
                
                
            }
            else if trimedTxt.hasSpecialCharacters(){
                self.fullNameTxtField.showInfo("invalid special".localized())
                valid = false
                self.fullname_bottom_constrain.constant = 35
                
            }
            else {
                
                self.fullname_bottom_constrain.constant = 10
                print("valid fullNmae")
            }
        }else {
            print("error fullNmae")
            self.fullname_bottom_constrain.constant = 35
            self.tbHeight.constant = self.experienceTableView.layer.frame.height + 25
            self.fullNameTxtField.showInfo("please enter your name".localized())
            valid = false
        }
        
        //         if let gender = self.genderTxtField.text , !gender.isBlank {
        //
        //         print("valid gender")
        //
        //         }else {
        //         print("error gender")
        //         self.genderTxtField.showInfo("please enter your gender".localized())
        //         valid = false
        //         }
        //        if let birthDate = self.birthDateTextField.text , !birthDate.isBlank {
        //
        //        print("valid birthDate")
        //
        //        }else {
        //        print("error birthDate")
        //        self.birthDateTextField.showInfo("please enter your bithday".localized())
        //        valid = false
        //        }
        
        if let workingTime = self.workingTimesTxtField.text , !workingTime.isBlank {
            print("valid user country")
        }else {
            print("error user country")
            self.workingTimesTxtField.showInfo("please enter your favorite working time".localized())
            valid = false
        }
        
        if let userCity = self.userCityTxtField.text , !userCity.isBlank {
            print("valid user country")
        }else {
            print("error user country")
            self.userCityTxtField.showInfo("please enter your residence city".localized())
            valid = false
        }
        
        if let userCountry = self.userCountryTxtField.text , !userCountry.isBlank {
            print("valid user country")
            self.country_bottom_constraint.constant = 10
        }else {
            print("error user country")
            self.userCountryTxtField.showInfo("please enter your Residence Country".localized())
            self.country_bottom_constraint.constant = 33
            valid = false
        }
        
        if let educationCountry = self.educationCountryTxtField.text , !educationCountry.isBlank {
            self.edu_country_constrain.constant = 33
            //self.tbHeight.constant = self.experienceTableView.layer.frame.height + 25
            print("valid educationCountry")
            
        }else {
            print("error educationCountry")
            self.edu_country_constrain.constant = 70
            self.educationCountryTxtField.showInfo("please enter your University/Institution Country".localized())
            valid = false
        }
        
        if let institutionName = self.institutionTxtField.text , !institutionName.isBlank {
            self.institute_bottom_constrain.constant = 33
            print("valid institutionName")
            
        }else {
            print("error institutionName")
            self.institute_bottom_constrain.constant = 70
            self.institutionTxtField.showInfo("please enter your University/Institution Name".localized())
            valid = false
        }
        
        if let educationMajor = self.MajorTxtField.text , !educationMajor.isBlank {
            self.degree_bottom_constrain.constant = 5.5
            print("valid educationCity")
            
        }else {
            print("error educationCity")
            self.degree_bottom_constrain.constant = 17.5
            self.MajorTxtField.showInfo("please enter your major".localized())
            valid = false
        }
        
        
        
        if let degreeName = self.degreeTxtField.text , !degreeName.isBlank {
            
            self.edu_city_bottom_constrain.constant = 33
            
        }else {
            print("error degreeName")
            self.edu_city_bottom_constrain.constant = 70
            self.degreeTxtField.showInfo("please enter your Degree".localized())
            valid = false
        }
        if let email = self.emailTxtField.text , !email.isBlank {
            if !email.isEmail{
                self.email_bottom_constrain.constant = 70
                self.emailTxtField.showInfo("please enter valid email".localized())
                valid = false
            }else {
                self.email_bottom_constrain.constant = 33
                print("valid email")
            }
        }else {
            print("error email")
            self.email_bottom_constrain.constant = 70
            self.emailTxtField.showInfo("please enter your email address".localized())
            valid = false
        }
        self.experienceObject = ExperienceObjectToPost()
        self.experienceObject?.experiences = [Experience2]()
        //self.experienceObject?.experiences.append(contentsOf: self.experience)
        return valid
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profileImageAction(_ sender: Any) {
        takePhotoCamera()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     
     } */
    
}
extension EditProfileViewController :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.fullNameTxtField) {
            self.view.endEditing(true)
        }
            
        else if(textField == self.emailTxtField) {
            self.view.endEditing(true)
        }
            
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.fullNameTxtField {
            self.fullNameTxtField.showInfo("")
        }else if textField == self.emailTxtField {
            self.emailTxtField.showInfo("")
        }else if textField == self.workingTimesTxtField {
            self.workingTimesTxtField.showInfo("")
        }else if textField == self.userCityTxtField {
                self.userCityTxtField.showInfo("")
        }else if textField == self.birthDateTextField {
            self.birthDateTextField.showInfo("")
        }else if textField == self.genderTxtField{
            self.genderTxtField.showInfo("")
        }else if textField == self.userCountryTxtField{
            self.userCountryTxtField.showInfo("")
        }else if textField == self.educationCountryTxtField{
            self.educationCountryTxtField.showInfo("")
        }else if textField == self.MajorTxtField{
            self.MajorTxtField.showInfo("")
        }else if textField == self.institutionTxtField{
            self.institutionTxtField.showInfo("")
        }else if textField == self.degreeTxtField{
            self.degreeTxtField.showInfo("")
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VerficationCodeViewController" {
            if let destnationViewcontroller = segue.destination as? VerficationCodeViewController {
                destnationViewcontroller.verificationCase = .Profile
            }
        }
        else if segue.identifier == "addExpSeg" {
            let userExpVC: userExperienceVCtrl = segue.destination as! userExperienceVCtrl
            if self.expIDToEdit > -1 {
                userExpVC.editeMode = true
                userExpVC.companyNameToEdit = experience[expIDToEdit].companyName
                userExpVC.jobTitleToEdit = experience[expIDToEdit].jobTitle
                userExpVC.yearsOfExpToEdit = String(experience[expIDToEdit].yearsOfExperience ?? 0)
            }
            userExpVC.delegate = self
        }
        else if segue.identifier == "openCVSeg" {
            if let destnationViewcontroller = segue.destination as? CVViewerViewController {
                if cvViewerType == 1 {
                    destnationViewcontroller.cvURL = CVUrl
                }
                else{
                    destnationViewcontroller.cvURL = cvPrevUrl
                }
            }
            
        }
    }
}
extension EditProfileViewController {
    func takePhotoCamera() {
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            let cameraViewController = CameraViewController(croppingParameters: self.croppingParameters, allowsLibraryAccess: self.libraryEnabled) { [weak self] image, asset in
                self?.profieImageView.image = image
                self?.selectedProfileImage = image
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
            let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: self.croppingParameters) { [weak self] image, asset in
                self?.profieImageView.image = image
                self?.selectedProfileImage = image
                //  self?.deleteButton.isHidden = false
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(libraryViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        actionSheetController.popoverPresentationController?.sourceView = self.view;
        actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 5 - 50,width: UIScreen.main.bounds.width ,height: 1.0);
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
}
extension EditProfileViewController : WWCalendarTimeSelectorProtocol {
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print ("tag = \(selector.view.tag)")
        print("Selected \n\(date)\n---")
        birthDateTextField.text = date.toString(dateFormat:"dd/MM/yyyy")
        birthDateTime = Int(date.timeIntervalSince1970)
    }
    
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        
        //If you want date with 18 year less you can use Calendar unit year for that.
        
        let date_less16 = Calendar.current.date(byAdding: .year, value: -17, to: Date())
        //print("date_less16 = \(date_less16)")
        let dateMax = Calendar.current.date(byAdding: .day, value: -1, to: date_less16!)
        //print(dateMax)
        if date < dateMax! {
            //myDate is earlier than Now (date and time)
            return true
        } else {
            //myDate is equal or after than Now (date and time)
            return false
        }
    }
}
extension EditProfileViewController :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.registerBtn.startAnimation()
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.registerBtn.stopAnimation(animationStyle: .shake, completion: {
            
        })
    }
    
    func onSucess() {
        self.registerBtn.stopAnimation()
        
        if UserManager.sharedInstance.getUser()?.mobileVerified == false {
            self.performSegue(withIdentifier: "VerficationCodeViewController", sender: nil)
        }else {
            self.showSucessMessage(message: "Your profile is edited sucessfully".localized())
            self.navigationController?.popViewController(animated:true)
        }
        //self.performSegue(withIdentifier: "VerficationCodeViewController", sender: nil)
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}


extension EditProfileViewController: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        birthDateTextField.text = date.toString(dateFormat:"dd/MM/yyyy")
        birthDateTime = date.dateToTimeStamp()
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker() -> GMDatePicker {
        
        let datePicker = GMDatePicker()
        datePicker.delegate = self
        let calendar = Calendar.current
        //calendar.timeZone = TimeZone(identifier: "UTC")!
        var startingDate: Date? = Date()
        if let userBirthDate = birthDateTime{
            startingDate = Date(timeIntervalSince1970: Double(userBirthDate))
        }
        datePicker.config.startDate = startingDate ?? Date()
        let date_less16 = calendar.date(byAdding: .year, value: -17, to: Date())
        datePicker.config.maxDate =  calendar.date(byAdding: .day, value: -1, to: date_less16!)
        datePicker.config.animationDuration = 0.0
        
        datePicker.config.cancelButtonTitle = "CancelPicker".localized()
        datePicker.config.confirmButtonTitle = "Done".localized()
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor.frenchBlue
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
        return datePicker
    }
}

extension EditProfileViewController:CountryListView{
    func onSucessLoadingCountryList(countries: [CountryRes]) {
//        self.countryList.removeAll()
//        self.countryList.append(contentsOf: countries)
    }
}
extension EditProfileViewController:DegreeListView{
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        
    }
    
    func showLoading() {
        
    }
    
    func hideLoading() {
        
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        self.degreeList.removeAll()
        if let degrees = degreeWorkingTimes.certificates {
        self.degreeList.append(contentsOf: degrees)
            
        }
        if let certificate_id = UserManager.sharedInstance.getUser()?.certificate_id {
            if let selectedCertificte = self.degreeList.first(where: {$0.id == certificate_id}) {
                self.certificate_id = selectedCertificte.id ?? -1
                self.degreeTxtField.text = L102Language.isRTL ?  selectedCertificte.certificateAr : selectedCertificte.certificateEn
            }
            
        }
        if let countries = degreeWorkingTimes.countries {
            if countries.count > 0 {
                self.countryList.removeAll()
                self.countryList.append(contentsOf: countries)
            }
        }
        self.workingTimes.removeAll()
        if let workingTimesList = degreeWorkingTimes.worktimes {
            self.workingTimes = workingTimesList
        }
       
        if let worktime_id_ = UserManager.sharedInstance.getUser()?.worktime_id {
            if let selectedWorktime = self.workingTimes.first(where: {$0.id == worktime_id_}) {
                self.worktime_id = selectedWorktime.id ?? -1
                self.workingTimesTxtField.text = L102Language.isRTL ?  selectedWorktime.worktimeAr : selectedWorktime.worktimeEn
            }
        }
    }
    
//    func onSucessLoadingDegreeList(degreeArr: [Degree]) {
//        self.degreeList.removeAll()
//        self.degreeList.append(contentsOf: degreeArr)
//    }
}

extension EditProfileViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.experience.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "expCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! experienceTVCell
        cell.companyView.changeFont()
        cell.companyNameTxtField.text = self.experience[indexPath.row].companyName
        cell.companyNameTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        cell.companyNameTxtField.infoFontSize = 14.0
        cell.companyNameTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        cell.companyNameTxtField.textColor = UIColor(hexString: "#8c8c8c")
        cell.companyNameTxtField.activeLineWidth = 2
        cell.companyNameTxtField.animationDuration = 0.5
        cell.companyNameTxtField.minimumPlaceholderFontSize = 15
        cell.companyNameTxtField.originalPlaceholderFontSize = 15
        cell.companyNameTxtField.placeholderDuration = 0.2
        cell.companyNameTxtField.placeholderColor = .darkGray
        cell.companyNameTxtField.placeholder = "Company Name".localized()
        
        cell.companyView.layer.cornerRadius = 2
        cell.companyView.layer.shadowColor = UIColor.black.cgColor
        cell.companyView.layer.shadowOpacity = 0.2
        cell.companyView.layer.shadowOffset = .zero
        cell.companyView.layer.shadowRadius = 10
        
        cell.jobTitleView.changeFont()
        cell.jobTitleTxtField.text = self.experience[indexPath.row].jobTitle
        cell.jobTitleTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        cell.jobTitleTxtField.infoFontSize = 14.0
        cell.jobTitleTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        cell.jobTitleTxtField.activeLineWidth = 2
        cell.jobTitleTxtField.animationDuration = 0.5
        cell.jobTitleTxtField.textColor = UIColor(hexString: "#8c8c8c")
        cell.jobTitleTxtField.minimumPlaceholderFontSize = 15
        cell.jobTitleTxtField.originalPlaceholderFontSize = 15
        cell.jobTitleTxtField.placeholderDuration = 0.2
        cell.jobTitleTxtField.placeholderColor = .darkGray
        cell.jobTitleTxtField.placeholder = "Job Title".localized()
        cell.jobTitleView.layer.cornerRadius = 2
        cell.jobTitleView.layer.shadowColor = UIColor.black.cgColor
        cell.jobTitleView.layer.shadowOpacity = 0.2
        cell.jobTitleView.layer.shadowOffset = .zero
        cell.jobTitleView.layer.shadowRadius = 10
        
        cell.noOfYearsTxtField.text = String(self.experience[indexPath.row].yearsOfExperience ?? 0)
        
        cell.noOfYearsView.changeFont()
        cell.noOfYearsTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        cell.noOfYearsTxtField.infoFontSize = 14.0
        cell.noOfYearsTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        cell.noOfYearsTxtField.textColor = UIColor(hexString: "#8c8c8c")
        cell.noOfYearsTxtField.activeLineWidth = 2
        cell.noOfYearsTxtField.animationDuration = 0.5
        cell.noOfYearsTxtField.minimumPlaceholderFontSize = 15
        cell.noOfYearsTxtField.originalPlaceholderFontSize = 15
        cell.noOfYearsTxtField.placeholderDuration = 0.2
        cell.noOfYearsTxtField.placeholderColor = .darkGray
        cell.noOfYearsTxtField.placeholder = "Years of experience".localized()
        
        cell.noOfYearsView.layer.cornerRadius = 2
        cell.noOfYearsView.layer.shadowColor = UIColor.black.cgColor
        cell.noOfYearsView.layer.shadowOpacity = 0.2
        cell.noOfYearsView.layer.shadowOffset = .zero
        cell.noOfYearsView.layer.shadowRadius = 10
        
        cell.editExp_btn.tag = indexPath.row
        cell.editExp_btn.addTarget(self, action: #selector(editExpAction(_:)), for: .touchUpInside)
        
        cell.deleteExp_btn.tag = indexPath.row
        cell.deleteExp_btn.addTarget(self, action: #selector(deleteExpAction(_:)), for: .touchUpInside)
        if self.experience.count != indexPath.row + 1 {
            cell.sepertorView.isHidden = false
        }
        return cell
    }
    
    @objc func editExpAction(_ sender: UIButton){
        self.expIDToEdit = sender.tag
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "addExpSeg", sender: self)
        }
    }
    
    @objc func deleteExpAction(_ sender: UIButton){
        if self.experience.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        self.experience.remove(at: sender.tag)
        DispatchQueue.main.async {
            self.experienceTableView.reloadData()
        }
    }
    
}
