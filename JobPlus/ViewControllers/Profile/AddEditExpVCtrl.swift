//
//  AddEditExpVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 04/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField
import TransitionButton


class AddEditExpVCtrl: UIViewController,UITextViewDelegate {
    
    //let countryPresenter = CountryListPresenter()
    var delegate: SendDataDelegate?
    let presenter = RegisterPresenter()
    let degreePresenter = DegreeListPresenter()
    var selectedexperiences = [Experience]()
    var worksectorsList = [workSectorItem]()
    var workFieldsList = [Job_categoriesItem]()
    var jobTypesList = [workSectorItem]()
    
    var worksectorId : Int = 0
    var jobTybeId : Int = 0
    var selectedexpId : Int = -1
    var countryList = [countryItem]()
    var selectedUserCountryId:Int?
    var selectedInstitutionCountryId:Int?
    var selectedCountryIndex: Int?
    var selectedCountry = [String]()
    var selectededuCountry = [String]()
    var selecteedCity = [String]()
    var selectedCityID: Int?
    var selectedWorkField = [String]()
    var selectedWorkFieldID: Int?
    var selectedCityIndex: Int?
    var cityList = [cityItem]()
    var city_id = 0
    var toDate:String?
    var fromDate:String?
    var fromDateObj: Date?
    var toDateObj: Date?
    var toDatee: Date?
    var fromDatee: Date?
    var dateType = 0 // 0 -> from , 1 -> To
    let maxLenghth = 350
    @IBOutlet weak var noticePeriodReStar: UILabel!
    @IBOutlet weak var dutiesTextView: UITextView!
    @IBOutlet weak var chooseCityBtn: UIButton!
    @IBOutlet weak var city_arrow_ic: UIImageView!
    @IBOutlet weak var cityTxtField_trailing_constraint: NSLayoutConstraint!
    @IBOutlet weak var noOfDutiesCharatersLbl: UILabel!
    @IBOutlet weak var exp_period_top: NSLayoutConstraint!
    @IBOutlet weak var companyNameTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var jobTitleTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var monthsTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var yearsTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var noticePeriodTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var noticePeriodView: UIView!
    @IBOutlet weak var noticePeriodLabel: UILabel!
    @IBOutlet weak var cityTextFeild: TweeAttributedTextField!
    @IBOutlet weak var workSectorTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var workFieldTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var currentWorkState: UISwitch!
    @IBOutlet weak var jobtypeTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var countryTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var ToTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var toDateReqStar: UILabel!
    @IBOutlet weak var toDateLbl: UILabel!
    @IBOutlet weak var toDateView: UIView!
    @IBOutlet weak var fromTxtFeild: TweeAttributedTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        degreePresenter.attachView(view: self)
        degreePresenter.doGetcertificatesNewList()
        //countryPresenter.attachView(view: self)
        //countryPresenter.doGetCountryList()
        setupTextFields()
        self.view.changeFont()
        self.dutiesTextView.placeholder = "Duties & Responsibilities".localized()
        self.dutiesTextView.delegate = self
        if selectedexpId != -1 {
            print("selectedexpId from edit :  \(selectedexpId)")
            let item = selectedexperiences[selectedexpId]
            if let jobCategory = item.job_category{
                self.selectedWorkFieldID = jobCategory.id
                self.selectedWorkField.append(L102Language.isRTL ? (jobCategory.nameAr ?? "") : (jobCategory.name ?? ""))
                self.workFieldTxtFeild.text = selectedWorkField[0]
            }
            
            self.companyNameTxtFeild.text = item.companyName
            self.jobTitleTxtFeild.text = item.jobTitle
            self.cityTextFeild.text = item.cityName
            self.selectedUserCountryId = item.country_id
            self.selectedCityID = item.city_id
            self.dutiesTextView.text = item.Duties ?? ""
            self.ToTxtFeild.text = (Int(item.toDate ?? "0") ?? 0).getCurrentDateString()
            self.fromTxtFeild.text = (Int(item.fromDate ?? "0") ?? 0).getCurrentDateString()
            if let to = item.toDate {
                self.toDate = to
                self.toDateObj = Date(timeIntervalSince1970: TimeInterval(Int(to)!))
            }
            if let from = item.fromDate {
                self.fromDate = from
                self.fromDateObj = Date(timeIntervalSince1970: TimeInterval(Int(from)!))
            }
            self.city_id = item.city_id ?? 0
            if city_id == 0 {
                self.chooseCityBtn.isHidden = true
                self.city_arrow_ic.isHidden = true
                self.cityTxtField_trailing_constraint.constant = 0
            }
            var state:Bool
            
            if item.CurrentlyWorking == 1 {
                state = true
                toDateLbl.isHidden = true
                toDateReqStar.isHidden = true
                toDateView.isHidden = true
            }else{
                state = false
            }
            currentWorkState.setOn(state, animated: true)
            if item.CurrentlyWorking == 1 {
                exp_period_top.constant = 117
                noticePeriodLabel.isHidden = false
                noticePeriodView.isHidden = false
                noticePeriodReStar.isHidden = false
                noticePeriodTxtFeild.text = item.noticePeriod
            }else{
                exp_period_top.constant = 20
                noticePeriodLabel.isHidden = true
                noticePeriodView.isHidden = true
                noticePeriodReStar.isHidden = true
            }
            if let years = item.yearsOfExperience {
                yearsTxtFeild.text = String(years)
            }
            if let months = item.month_of_experience {
                monthsTxtFeild.text = String(months)
            }
        }
    }
    @IBAction func toDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.tag = 2
        self.dateType = 1
        datePicker.show(inVC: self)
    }
    @IBAction func fromDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.tag = 1
        self.dateType = 0
        datePicker.show(inVC: self)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.noOfDutiesCharatersLbl.text = "\(textView.text.count)/\(maxLenghth)"
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            return textView.text.count + (text.count - range.length) <= maxLenghth
        }
    func setupTextFields(){
        companyNameTxtFeild.delegate = self
        companyNameTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        companyNameTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        companyNameTxtFeild.infoFontSize = 14.0
        companyNameTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        companyNameTxtFeild.activeLineWidth = 2
        companyNameTxtFeild.animationDuration = 0.5
        companyNameTxtFeild.minimumPlaceholderFontSize = 15
        companyNameTxtFeild.originalPlaceholderFontSize = 15
        companyNameTxtFeild.placeholderDuration = 0.2
        companyNameTxtFeild.placeholderColor = .darkGray
        companyNameTxtFeild.placeholder = "Company name".localized()
        
        jobTitleTxtFeild.delegate = self
        jobTitleTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        jobTitleTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        jobTitleTxtFeild.infoFontSize = 14.0
        jobTitleTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        jobTitleTxtFeild.activeLineWidth = 2
        jobTitleTxtFeild.animationDuration = 0.5
        jobTitleTxtFeild.minimumPlaceholderFontSize = 15
        jobTitleTxtFeild.originalPlaceholderFontSize = 15
        jobTitleTxtFeild.placeholderDuration = 0.2
        jobTitleTxtFeild.placeholderColor = .darkGray
        jobTitleTxtFeild.placeholder = "Job title".localized()
        
        workSectorTxtFeild.delegate = self
        workSectorTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        workSectorTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        workSectorTxtFeild.infoFontSize = 14.0
        workSectorTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        workSectorTxtFeild.activeLineWidth = 2
        workSectorTxtFeild.animationDuration = 0.5
        workSectorTxtFeild.minimumPlaceholderFontSize = 15
        workSectorTxtFeild.originalPlaceholderFontSize = 15
        workSectorTxtFeild.placeholderDuration = 0.2
        workSectorTxtFeild.placeholderColor = .darkGray
        workSectorTxtFeild.placeholder = "workSector2".localized()
        
        jobtypeTxtFeild.delegate = self
        jobtypeTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        jobtypeTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        jobtypeTxtFeild.infoFontSize = 14.0
        jobtypeTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        jobtypeTxtFeild.activeLineWidth = 2
        jobtypeTxtFeild.animationDuration = 0.5
        jobtypeTxtFeild.minimumPlaceholderFontSize = 15
        jobtypeTxtFeild.originalPlaceholderFontSize = 15
        jobtypeTxtFeild.placeholderDuration = 0.2
        jobtypeTxtFeild.placeholderColor = .darkGray
        jobtypeTxtFeild.placeholder = "Job type".localized()
        
        countryTxtFeild.delegate = self
        countryTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        countryTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        countryTxtFeild.infoFontSize = 14.0
        countryTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        countryTxtFeild.activeLineWidth = 2
        countryTxtFeild.animationDuration = 0.5
        countryTxtFeild.minimumPlaceholderFontSize = 15
        countryTxtFeild.originalPlaceholderFontSize = 15
        countryTxtFeild.placeholderDuration = 0.2
        countryTxtFeild.placeholderColor = .darkGray
        countryTxtFeild.placeholder = "Country".localized()
        
        cityTextFeild.delegate = self
        cityTextFeild.textColor = UIColor(hexString: "#8c8c8c")
        cityTextFeild.infoTextColor = UIColor(hexString: "#FF2600")
        cityTextFeild.infoFontSize = 14.0
        cityTextFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        cityTextFeild.activeLineWidth = 2
        cityTextFeild.animationDuration = 0.5
        cityTextFeild.minimumPlaceholderFontSize = 15
        cityTextFeild.originalPlaceholderFontSize = 15
        cityTextFeild.placeholderDuration = 0.2
        cityTextFeild.placeholderColor = .darkGray
        cityTextFeild.placeholder = "City".localized()
        
        workFieldTxtFeild.delegate = self
        workFieldTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        workFieldTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        workFieldTxtFeild.infoFontSize = 14.0
        workFieldTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        workFieldTxtFeild.activeLineWidth = 2
        workFieldTxtFeild.animationDuration = 0.5
        workFieldTxtFeild.minimumPlaceholderFontSize = 15
        workFieldTxtFeild.originalPlaceholderFontSize = 15
        workFieldTxtFeild.placeholderDuration = 0.2
        workFieldTxtFeild.placeholderColor = .darkGray
        workFieldTxtFeild.placeholder = "Job field".localized()
        
        noticePeriodTxtFeild.delegate = self
        noticePeriodTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
//        noticePeriodTxtFeild.keyboardType = .numberPad
        noticePeriodTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        noticePeriodTxtFeild.infoFontSize = 14.0
        noticePeriodTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        noticePeriodTxtFeild.activeLineWidth = 2
        noticePeriodTxtFeild.animationDuration = 0.5
        noticePeriodTxtFeild.minimumPlaceholderFontSize = 15
        noticePeriodTxtFeild.originalPlaceholderFontSize = 15
        noticePeriodTxtFeild.placeholderDuration = 0.2
        noticePeriodTxtFeild.placeholderColor = .darkGray
        noticePeriodTxtFeild.placeholder = "Notice period".localized()
        
        yearsTxtFeild.delegate = self
        yearsTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        yearsTxtFeild.keyboardType = .numberPad
        yearsTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        yearsTxtFeild.infoFontSize = 13.0
        yearsTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        yearsTxtFeild.activeLineWidth = 2
        yearsTxtFeild.animationDuration = 0.5
        yearsTxtFeild.minimumPlaceholderFontSize = 13
        yearsTxtFeild.originalPlaceholderFontSize = 13
        yearsTxtFeild.placeholderDuration = 0.2
        yearsTxtFeild.placeholderColor = .darkGray
        yearsTxtFeild.infoLabel.numberOfLines = 2
        yearsTxtFeild.minimumFontSize = 13
        yearsTxtFeild.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)
        yearsTxtFeild.placeholder = "Years of experience".localized()
        
        monthsTxtFeild.delegate = self
        monthsTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        monthsTxtFeild.keyboardType = .numberPad
        monthsTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        monthsTxtFeild.infoFontSize = 13.0
        monthsTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        monthsTxtFeild.activeLineWidth = 2
        monthsTxtFeild.animationDuration = 0.5
        monthsTxtFeild.minimumPlaceholderFontSize = 13
        monthsTxtFeild.originalPlaceholderFontSize = 13
        monthsTxtFeild.placeholderDuration = 0.2
        monthsTxtFeild.placeholderColor = .darkGray
        monthsTxtFeild.infoLabel.numberOfLines = 2
        monthsTxtFeild.minimumFontSize = 13
        monthsTxtFeild.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)
        monthsTxtFeild.placeholder = "Months of experience".localized()
    
        fromTxtFeild.delegate = self
        fromTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        fromTxtFeild.keyboardType = .numberPad
        fromTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        fromTxtFeild.infoFontSize = 13.0
        fromTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        fromTxtFeild.activeLineWidth = 2
        fromTxtFeild.animationDuration = 0.5
        fromTxtFeild.minimumPlaceholderFontSize = 13
        fromTxtFeild.originalPlaceholderFontSize = 13
        fromTxtFeild.placeholderDuration = 0.2
        fromTxtFeild.placeholderColor = .darkGray
        fromTxtFeild.infoLabel.numberOfLines = 2
        fromTxtFeild.minimumFontSize = 13
        fromTxtFeild.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)
        fromTxtFeild.placeholder = "From".localized()
        
        ToTxtFeild.delegate = self
        ToTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        ToTxtFeild.keyboardType = .numberPad
        ToTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        ToTxtFeild.infoFontSize = 13.0
        ToTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        ToTxtFeild.activeLineWidth = 2
        ToTxtFeild.animationDuration = 0.5
        ToTxtFeild.minimumPlaceholderFontSize = 13
        ToTxtFeild.originalPlaceholderFontSize = 13
        ToTxtFeild.placeholderDuration = 0.2
        ToTxtFeild.placeholderColor = .darkGray
        ToTxtFeild.infoLabel.numberOfLines = 2
        ToTxtFeild.minimumFontSize = 13
        ToTxtFeild.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)
        ToTxtFeild.placeholder = "To".localized()
        
    }
    @IBAction func changeCurrentWorkState(_ sender: Any) {
        
        if self.currentWorkState.isOn {
            exp_period_top.constant = 117
            noticePeriodLabel.isHidden = false
            noticePeriodView.isHidden = false
            noticePeriodReStar.isHidden = false
            toDateLbl.isHidden = true
            toDateReqStar.isHidden = true
            toDateView.isHidden = true
            monthsTxtFeild.text = ""
            yearsTxtFeild.text = ""
            ToTxtFeild.text = ""
            fromTxtFeild.text = ""
            noticePeriodTxtFeild.text = ""
        }else{
            exp_period_top.constant = 20
            noticePeriodLabel.isHidden = true
            noticePeriodView.isHidden = true
            noticePeriodReStar.isHidden = true
            toDateLbl.isHidden = false
            toDateReqStar.isHidden = false
            toDateView.isHidden = false
            monthsTxtFeild.text = ""
            yearsTxtFeild.text = ""
            ToTxtFeild.text = ""
            fromTxtFeild.text = ""
        }
    }
    @IBAction func chooseCountry(_ sender: UIButton) {
        
        print("choose country")
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: true, countriesNames: countriesNames)
        }
        else{
            //            degreePresenter.doGetDegreeList()
        }
        
    }
    @IBAction func chooseWorkField(_ sender: UIButton) {
        
        print("choose WorkField")
        self.view.endEditing(true)
        let fieldsNames = L102Language.isRTL ? workFieldsList.map {$0.nameAr } : workFieldsList.map {$0.nameEn}
        
        if fieldsNames.count > 0 {
            showWorkFieldsSelectPopover(sender: sender, fieldsNames: fieldsNames)
        }
        else{
             degreePresenter.doGetcertificatesNewList()
        }
    }
    @IBAction func chooseCity(_ sender: UIButton) {
        print("choose country")
        
        self.view.endEditing(true)
        //        self.resetCustomTextFieldsErrorMessages(self.userCountryTxtField)
        let citiesNames = L102Language.isRTL ? cityList.map {$0.cityAr } : cityList.map {$0.cityEn}
        
        if citiesNames.count > 0 {
            showCitySelectPopover(sender: sender, isUserCountry: true, citiesNames: citiesNames)
        }
        else{
            //            degreePresenter.doGetDegreeList()
        }
    }
    func showWorkFieldsSelectPopover(sender: UIView, fieldsNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: fieldsNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        selectionMenu.setSelectedItems(items: selectedWorkField) { [weak self] (text, index, selected, selectedList) in
            self!.workFieldTxtFeild.showInfo("")
            if L102Language.isRTL {
                if let selectedField  = self!.workFieldsList.first(where: {$0.nameAr == text!!}) {
                    if let selectedFieldID = selectedField.id{
                            self!.selectedWorkFieldID = selectedFieldID
                            self!.workFieldTxtFeild.text = text!!
                    }
                }
            }
            else{
                if let selectedField  = self!.workFieldsList.first(where: {$0.nameEn == text!!}) {
                    if let selectedFieldID = selectedField.id{
                            self!.selectedWorkFieldID = selectedFieldID
                            self!.workFieldTxtFeild.text = text!!
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return fieldsNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    func showCitySelectPopover(sender: UIView,isUserCountry:Bool, citiesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: citiesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        selectionMenu.setSelectedItems(items: selecteedCity) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCity  = self!.cityList.first(where: {$0.cityAr == text!!}) {
                    if let selectedCityID = selectedCity.id{
                            self!.selectedCityID = selectedCityID
                            self!.cityTextFeild.text = text!!
                    }
                }
            }
            else{
                if let selectedCity  = self!.cityList.first(where: {$0.cityEn == text!!}) {
                    if let selectedCityID = selectedCity.id{
                            self!.selectedCityID = selectedCityID
                            self!.cityTextFeild.text = text!!
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return citiesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    func showCountrySelectPopover(sender: UIView,isUserCountry:Bool, countriesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: countriesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        if isUserCountry {
            selectedCountryName = selectedCountry
        }
        else{
            selectedCountryName = selectededuCountry
        }
        selectionMenu.setSelectedItems(items: selectedCountryName) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCountry  = self!.countryList.first(where: {$0.nameAr == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
//                        if isUserCountry {
//                            self!.selectedUserCountryId = selectedCountryID
//                            self!.countryTxtFeild.text = text!!
//                            self!.countryTxtFeild.showInfo("")
//                        }else{
//                            self!.selectedInstitutionCountryId = selectedCountryID
//                            //                        self!.educationCountryTxtField.text = text!!
//                        }
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.countryTxtFeild.text = text!!
                            if (selectedCountry.cities?.count ?? 0) > 0 {
                                self?.chooseCityBtn.isHidden = false
                                self?.city_arrow_ic.isHidden = false
                                self?.cityTxtField_trailing_constraint.constant = 33
                                self?.cityTextFeild.text = ""
                                self?.cityList = selectedCountry.cities ?? []
                            }
                            else{
                                self?.chooseCityBtn.isHidden = true
                                self?.city_arrow_ic.isHidden = true
                                self?.cityTxtField_trailing_constraint.constant = 0
                                self?.cityTextFeild.text = ""
                            }
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            
                        }
                    }
                    
                }
            }
            else{
                if let selectedCountry  = self!.countryList.first(where: {$0.nameEn == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
//                        if isUserCountry {
//                            self!.selectedUserCountryId = selectedCountryID
//                            self!.countryTxtFeild.text = text!!
//                            self!.countryTxtFeild.showInfo("")
//                        }else{
//                            self!.selectedInstitutionCountryId = selectedCountryID
//                            //                            self!.educationCountryTxtField.text = text!!
//                        }
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.countryTxtFeild.text = text!!
                            if (selectedCountry.cities?.count ?? 0) > 0 {
                                self?.chooseCityBtn.isHidden = false
                                self?.city_arrow_ic.isHidden = false
                                self?.cityTxtField_trailing_constraint.constant = 33
                                self?.cityTextFeild.text = ""
                                self?.selectedCityID = 0
                                self?.cityList = selectedCountry.cities ?? []
                            }
                            else{
                                self?.chooseCityBtn.isHidden = true
                                self?.city_arrow_ic.isHidden = true
                                self?.cityTxtField_trailing_constraint.constant = 0
                                self?.cityTextFeild.text = ""
                                self?.selectedCityID = 0
                            }
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return countriesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    
    
    @IBAction func chooseJobType(_ sender: UIButton) {
        print("chooseJobType")
        //        self.view.endEditing(true)
        let languagePicker1 = UIPickerView()
        let languageArrayToDisplay = L102Language.isRTL ? jobTypesList.map {$0.nameAr} : jobTypesList.map {$0.nameEn}
        if languageArrayToDisplay.count > 0 {
            
            languagePicker1.addCustomPicker(languageArrayToDisplay as NSArray , sender , "Job type", 0) { (selectedIndex) in
                
                self.jobtypeTxtFeild.text = languageArrayToDisplay[selectedIndex]
                self.jobtypeTxtFeild.showInfo("")
                self.jobTybeId = self.jobTypesList[selectedIndex].id!
                
            }
            
        }else{
            print("no date in languagesArrayToDisplay")
        }
        
        
    }
    @IBAction func chooseWorksector(_ sender: UIButton) {
        print("chooseWorksector")
        self.view.endEditing(true)
        let languagePicker = UIPickerView()
        let languageArrayToDisplay = L102Language.isRTL ? worksectorsList.map {$0.nameAr} : worksectorsList.map {$0.nameEn}
        if languageArrayToDisplay.count > 0 {
            
            languagePicker.addCustomPicker(languageArrayToDisplay as NSArray , sender , "Work sector", 0) { (selectedIndex) in
                
                self.workSectorTxtFeild.text = languageArrayToDisplay[selectedIndex]
                self.workSectorTxtFeild.showInfo("")
                self.worksectorId = self.worksectorsList[selectedIndex].id!
            }
            
        }else{
            print("no date in languagesArrayToDisplay")
        }
        
        
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func saveAction(_ sender: Any) {
        
        //
        if(validateTextFields()) {
            
            print("saveAction")
            if self.companyNameTxtFeild.text!.count > 70{
                print("companyNameTxtFeild.text!.count:  \(companyNameTxtFeild.text!.count)")
                self.showErrorMessage(message: "Company name may not be greater than 70 characters".localized())
                return
            }
            
            if self.jobTitleTxtFeild.text!.count > 70{
                print("jobTitleTxtFeild.text!.count:  \(jobTitleTxtFeild.text!.count)")
                self.showErrorMessage(message: "Job title may not be greater than 70 characters".localized())
                return
            }
            if self.cityTextFeild.text!.count > 70{
                print("cityTextFeild.text!.count:  \(cityTextFeild.text!.count)")
                self.showErrorMessage(message: "City may not be greater than 70 characters".localized())
                return
            }
            
            if self.noticePeriodTxtFeild.text!.count > 50{
                print("noticePeriodTxtFeild.text!.count:  \(noticePeriodTxtFeild.text!.count)")
                self.showErrorMessage(message: "Notice period may not be greater than 50 characters".localized())
                return
            }
            
            
            
            if selectedexpId != -1 {
                print("save edit exp")
                
                let user = UserManager.sharedInstance.getUser()
                let lang = ExperienceObjectToPost()
                lang.experiences = [Experience2]()
                
                var tmpArr  =  [Experience2]()
                let selectedExpItem = selectedexperiences[selectedexpId]
                var currentlyWorking:Int
                if self.currentWorkState.isOn {
                    currentlyWorking = 1
                }else{
                    currentlyWorking = 0
                }
                selectedExpItem.CurrentlyWorking = currentlyWorking
                selectedExpItem.jobTitle = jobTitleTxtFeild.text!
                selectedExpItem.companyName = companyNameTxtFeild.text!
                print("yearsTxtFeild.text : \(yearsTxtFeild.text)")
                
                selectedExpItem.yearsOfExperience = Int(yearsTxtFeild.text!.replacedArabicDigitsWithEnglish)!
                selectedExpItem.month_of_experience = Int(monthsTxtFeild.text!.replacedArabicDigitsWithEnglish)!
                selectedExpItem.noticePeriod = noticePeriodTxtFeild.text!
                selectedExpItem.cityName = cityTextFeild.text!
                selectedExpItem.country_id = selectedUserCountryId
                selectedExpItem.job_category_id = selectedWorkFieldID
                selectedExpItem.worksector_id = worksectorId
                selectedExpItem.jobtype_id = jobTybeId
                selectedExpItem.city_id = selectedCityID
                selectedExpItem.Duties = dutiesTextView.text ?? ""
                selectedExpItem.fromDate = fromDate
                selectedExpItem.toDate = toDate
                selectedExpItem.job_category_id = selectedWorkFieldID
                for item in selectedexperiences {
                    let exp : Experience2 = Experience2(jobTitle: item.jobTitle, companyName: item.companyName, yearsOfExperience: item.yearsOfExperience ?? 0, month_of_experience: item.month_of_experience ?? 0, noticePeriod: item.noticePeriod ?? "", cityName: item.cityName ?? "", CurrentlyWorking: item.CurrentlyWorking ?? 0, country_id: item.country_id ?? nil, worksector_id:  item.worksector_id ?? 0, jobtype_id: item.jobtype_id ?? 0, city_id: item.city_id ?? 0, Duties: item.Duties ?? "", fromDate: item.fromDate ?? "0", toDate: item.toDate ?? "0", job_category_id:  item.job_category_id)
                    tmpArr.append(exp)
                }
                lang.experiences.append(contentsOf: tmpArr)
                user?.experiences2 =  lang
                user?.SectionID = 5
                presenter.doEditNewProfile(data: user!)
            }else{
                print("save add exp")
                let user = UserManager.sharedInstance.getUser()
                let lang = ExperienceObjectToPost()
                lang.experiences = [Experience2]()
                var tmpArr  =  [Experience2]()
                for item in selectedexperiences {
                    let exp : Experience2 = Experience2(jobTitle: item.jobTitle, companyName: item.companyName, yearsOfExperience: item.yearsOfExperience ?? 0, month_of_experience: item.month_of_experience ?? 0, noticePeriod: item.noticePeriod ?? "", cityName: item.cityName ?? "", CurrentlyWorking: item.CurrentlyWorking ?? 0, country_id: item.country?.id ?? nil, worksector_id:  item.worksector?.id ?? 0, jobtype_id: item.jobtype?.id ?? 0, city_id: item.city_id ?? 0, Duties: item.Duties ?? "", fromDate: (item.fromDate ?? "0"), toDate: (item.toDate ?? "0"), job_category_id: item.job_category_id)
                    tmpArr.append(exp)
                }
                
                lang.experiences.append(contentsOf: tmpArr)
                
                var currentlyWorking:Int
                if self.currentWorkState.isOn {
                    currentlyWorking = 1
                }else{
                    currentlyWorking = 0
                }
                var m : Int = 0
                if let month = Int(monthsTxtFeild.text!.replacedArabicDigitsWithEnglish){
                    m = month
                }
                
                var Y : Int = 0
                if let month = Int(yearsTxtFeild.text!.replacedArabicDigitsWithEnglish){
                    Y = month
                }
                
                let exp = Experience2(jobTitle: jobTitleTxtFeild.text!, companyName: companyNameTxtFeild.text!, yearsOfExperience: Y, month_of_experience: m, noticePeriod: noticePeriodTxtFeild.text!, cityName: cityTextFeild.text!, CurrentlyWorking: currentlyWorking, country_id: selectedUserCountryId ?? nil, worksector_id: worksectorId, jobtype_id: jobTybeId, city_id: selectedCityID ?? 0, Duties: dutiesTextView.text ?? "", fromDate: fromDate, toDate: toDate, job_category_id: selectedWorkFieldID)
                lang.experiences.append(exp)
                user?.experiences2 =  lang
                user?.SectionID = 5
                presenter.doEditNewProfile(data: user!)
            }
         
        }
    }
    func validateTextFields() -> Bool {
        var valid = true
        if let val = self.companyNameTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.companyNameTxtFeild.showInfo("please enter your company name".localized())
            valid = false
        }
        if let val = self.jobTitleTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.jobTitleTxtFeild.showInfo("please enter your job title".localized())
            valid = false
        }
        if let val = self.workSectorTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.workSectorTxtFeild.showInfo("please enter work sector".localized())
            valid = false
        }
        if let val = self.workFieldTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.workFieldTxtFeild.showInfo("please enter job field".localized())
            valid = false
        }
        if let val = self.jobtypeTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.jobtypeTxtFeild.showInfo("please enter job type".localized())
            valid = false
        }
//        if let val = self.countryTxtFeild.text , !val.isBlank {
//            print("valid")
//        }else {
//            print("error")
//            self.countryTxtFeild.showInfo("entercountry".localized())
//            valid = false
//        }
//        if let val = self.cityTextFeild.text , !val.isBlank {
//            print("valid")
//        }else {
//            print("error")
//            self.cityTextFeild.showInfo("entercity".localized())
//            valid = false
//        }
//        if currentWorkState.isOn {
//            if let val = self.noticePeriodTxtFeild.text , !val.isBlank {
//                print("valid")
//            }else {
//                print("error")
//                self.noticePeriodTxtFeild.showInfo("please enter notice period".localized())
//                valid = false
//            }
//        }
        if let val = self.fromTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.fromTxtFeild.showInfo("enter start date".localized())
            valid = false
        }
        if let val = self.ToTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            if !currentWorkState.isOn {
            self.ToTxtFeild.showInfo("enter end date".localized())
            valid = false
            }
        }
        return valid
    }
}
extension AddEditExpVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == companyNameTxtFeild {
            companyNameTxtFeild.showInfo("")
        }
        else if textField == jobTitleTxtFeild {
            jobTitleTxtFeild.showInfo("")
        }
        else if textField == countryTxtFeild {
            countryTxtFeild.showInfo("")
        }
        else if textField == workSectorTxtFeild {
            workSectorTxtFeild.showInfo("")
        }
        else if textField == workFieldTxtFeild {
            workFieldTxtFeild.showInfo("")
        }
        else if textField == jobtypeTxtFeild {
            jobtypeTxtFeild.showInfo("")
        }
        else if textField == cityTextFeild {
            cityTextFeild.showInfo("")
        }
        else if textField == noticePeriodTxtFeild {
            noticePeriodTxtFeild.showInfo("")
        }
        else if textField == yearsTxtFeild {
            yearsTxtFeild.showInfo("")
        }
        else if textField == monthsTxtFeild {
            monthsTxtFeild.showInfo("")
        }
    }
}
extension AddEditExpVCtrl :DegreeListView {
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    func showLoading() {
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        if let languages = degreeWorkingTimes.WorkSectors {
            if languages.count > 0 {
                self.worksectorsList.removeAll()
                self.worksectorsList.append(contentsOf: languages)
                print("self.worksectorsList : \(worksectorsList)")
            }}
        if let languages = degreeWorkingTimes.JobTypes {
            if languages.count > 0 {
                self.jobTypesList.removeAll()
                self.jobTypesList.append(contentsOf: languages)
                print("self.jobTypesList : \(jobTypesList)")
            }}
        if let countries = degreeWorkingTimes.countries {
            self.countryList = countries            
            if selectedexpId != -1 {
                let item = selectedexperiences[selectedexpId]
                self.selectedUserCountryId = item.country?.id
                for item in self.countryList {
                    if self.selectedUserCountryId == item.id {
                        self.countryTxtFeild.text = L102Language.isRTL ? item.nameAr : item.nameEn
                        self.cityList = item.cities ?? []
                    }
                }
            }
        }
        if let Job_categories = degreeWorkingTimes.Job_categories {
            self.workFieldsList = Job_categories
            if selectedexpId != -1 {
                let item = selectedexperiences[selectedexpId]
                self.selectedWorkFieldID = item.job_category_id
                if let index = self.workFieldsList.firstIndex(where: {$0.id == item.job_category_id}){
                    self.selectedWorkField.removeAll()
                    self.selectedWorkField.append(L102Language.isRTL ? (workFieldsList[index].nameAr ?? "") : (workFieldsList[index].nameEn ?? ""))
                    self.workFieldTxtFeild.text = L102Language.isRTL ? (workFieldsList[index].nameAr ?? "") : (workFieldsList[index].nameEn ?? "")
                }
            }
        }
        //
        if selectedexpId != -1 {
            print("selectedexpId from edit :  \(selectedexpId)")
            let item = selectedexperiences[selectedexpId]
            self.workSectorTxtFeild.text =  item.worksector?.name
            if let id = item.worksector?.id {
            self.worksectorId = id
            }
            self.jobtypeTxtFeild.text = item.jobtype?.name
            if let id = item.jobtype?.id {
            self.jobTybeId = id
            }
        }
    }
}
extension AddEditExpVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucess() {
        print("on sucess")
        let user1  = UserManager.sharedInstance.getUser()
        UserManager.sharedInstance.saveUser(user:user1!)
        self.delegate?.sendData(text: "exp")
        self.dismiss(animated: true, completion: nil)
    }
}


extension AddEditExpVCtrl: CountryListView{
    func onSucessLoadingCountryList(countries: [CountryRes]) {
        print("countries count : \(countries.count)")
        self.countryList.removeAll()
        //        self.countryList.append(contentsOf: countries)
        for item in countries{
            self.countryList.append(countryItem(id: item.id, nameEn: item.nameEn, nameAr: item.nameAr, code: item.isoCode,cities: [cityItem(id: 1, cityEn: "x", cityAr: "x")]))
        }
        
        if selectedexpId != -1 {
            let item = selectedexperiences[selectedexpId]
            self.selectedUserCountryId = item.country?.id
            for item in self.countryList {
                if self.selectedUserCountryId == item.id {
                    self.countryTxtFeild.text = L102Language.isRTL ? item.nameAr : item.nameEn
                }
            }
        }
    }
}
extension AddEditExpVCtrl: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        if gmDatePicker.tag == 1{
            if let to = toDateObj {
                if date >= to {
                    self.showErrorMessage(message: "To date must be after from date".localized())
                    return
                }
            }
            fromTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
            fromTxtFeild.showInfo("")
            fromDate = "\(date.dateToTimeStamp())"
            fromDatee = date
            fromDateObj = date
            if (toDate != nil || self.currentWorkState.isOn) {
                let calendar = Calendar.current
                let months = calendar.dateComponents([.month], from: fromDatee!, to: toDatee ?? Date())
                self.yearsTxtFeild.text = "\(Int(months.month! / 12))"
                self.monthsTxtFeild.text = "\(Int(months.month! % 12))"
                
            }
        }else{
            if let from = fromDateObj {
                if date <= from {
                    self.showErrorMessage(message: "To date must be after from date".localized())
                    return
                }
            }
            ToTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
            ToTxtFeild.showInfo("")
            toDate = "\(date.dateToTimeStamp())"
            toDatee = date
            toDateObj = date
            if (fromDate != nil) {
                let calendar = Calendar.current
                let months = calendar.dateComponents([.month], from: fromDatee!, to: toDatee!)
                self.yearsTxtFeild.text = "\(Int(months.month! / 12))"
                self.monthsTxtFeild.text = "\(Int(months.month! % 12))"
            }
        }
        
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker() -> GMDatePicker {
        
        let datePicker = GMDatePicker()
        datePicker.delegate = self
        datePicker.config.maxDate =  Date()
        datePicker.config.animationDuration = 0.0
        if self.dateType == 0 && self.fromDate != nil {
            datePicker.config.startDate = Date(timeIntervalSince1970: Double(self.fromDate ?? "0") ?? 0.0)
        }
        else if self.toDate != nil {
            datePicker.config.startDate = Date(timeIntervalSince1970: Double(self.toDate ?? "0") ?? 0.0)
        }
        datePicker.config.cancelButtonTitle = "CancelPicker".localized()
        datePicker.config.confirmButtonTitle = "Done".localized()
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor.frenchBlue
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
        return datePicker
    }
    
    
}

