 //
 //  ProfileVCtrl.swift
 //  JobPlus
 //
 //  Created by elsaid yousif on 12/1/19.
 //  Copyright © 2019 elsaid yousif. All rights reserved.
 //
 
 import UIKit
 import SideMenu
 import ALCameraViewController
 
 class ProfileVCtrl: UIViewController {
    
    @IBOutlet weak var navArrow: UIButton!
    //@IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var userImgContainerView: UIView!
    @IBOutlet weak var appliedJobsSTV: UIStackView!
    @IBOutlet weak var FavsSTV: UIStackView!
    @IBOutlet weak var changePassSTV: UIStackView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var changePassLbl: UILabel!
    var userInfoPresenter:UserInfoPresenter = UserInfoPresenter()
    let presenter = RegisterPresenter()
    var selectedProfileImage :UIImage?
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = true
    var allowResizing: Bool = true
    var allowMoving: Bool = true
    var minimumSize: CGSize = CGSize(width: 200, height: 200)
    var spinnerView: UIView?
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    
    private func updateMenus() {
        let settings = makeSettings()
        SideMenuManager.default.leftMenuNavigationController?.settings = settings
        SideMenuManager.default.rightMenuNavigationController?.settings = settings
    }
    private func makeSettings() -> SideMenuSettings {
        var settings = SideMenuSettings()
        
        settings.menuWidth = 240
        settings.statusBarEndAlpha = 0
        return settings
    }
    private func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        //SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        if L102Language.isRTL { SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
        }
        else{ SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
            
        }
    }
    @IBAction func changeProfileImg(_ sender: Any) {
        takePhotoCamera()
    }
    func takePhotoCamera() {
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            let cameraViewController = CameraViewController(croppingParameters: self.croppingParameters, allowsLibraryAccess: self.libraryEnabled) { [weak self] image, asset in
                self?.userImage.image = image
                self?.selectedProfileImage = image
                let base64Image:String? = (image!.resizedImageWithinRect(rectSize: CGSize(width: 200, height: 200)).toBase64())
                self?.presenter.doEditProfileImage(image: base64Image)
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true, completion: nil)
    }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
            let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: self.croppingParameters) { [weak self] image, asset in
                self?.userImage.image = image
                self?.selectedProfileImage = image
                //  self?.deleteButton.isHidden = false
                let base64Image:String? = (image!.resizedImageWithinRect(rectSize: CGSize(width: 200, height: 200)).toBase64())
                self?.presenter.doEditProfileImage(image: base64Image)
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(libraryViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        actionSheetController.popoverPresentationController?.sourceView = self.view;
        actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 5 - 50,width: UIScreen.main.bounds.width ,height: 1.0);
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        //        let notiCount = UserManager.sharedInstance.notificationCount
        //        if notiCount > 0 {
        //            self.notificationCount.text = (" \(String(notiCount)) ")
        //            self.notificationCount.isHidden = false
        //        }
        //        else{
        //             self.notificationCount.isHidden = true
        //        }
        
        if UserManager.sharedInstance.getUser() != nil {
            if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
                if let profileImgUrl = URL(string: profileImg){
                    self.userImage.kf.setImage(with: profileImgUrl)
                }
            }
            else {
                if let gender = UserManager.sharedInstance.getUser()?.gender {
                    if gender == "female" {
                        self.userImage.image = UIImage(named: "woman")
                    }
                    else{
                        self.userImage.image = UIImage(named: "profile_img")
                    }
                }
            }
            if let userName = UserManager.sharedInstance.getUser()?.name {
                self.userName.text = userName
            }
        }
    }
    //    @IBAction func notificationOpenAction(_ sender: Any) {
    //        if UserManager.sharedInstance.getUser() != nil {
    //            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
    //                
    //                if let navigator = navigationController {
    //                    navigator.pushViewController(viewController, animated: true)
    //                }
    //            }
    //        }
    //        else{
    //            self.showErrorMessage(message: "you have to login first".localized())
    //        }
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if let mobile = UserManager.sharedInstance.getUser()?.mobile {
            if mobile.contains("+") {
                self.changePassLbl.text =  mobile
            }
            else if let countryCode = UserManager.sharedInstance.getUser()?.mobileCountryPrefix {
                self.changePassLbl.text =  "\(countryCode)\(mobile)"
                
                
            }
            
        }
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        if L102Language.isRTL { SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
        }
        else{ SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
            
        }
        userImgContainerView.layerGradient()
        userImage.layer.cornerRadius = 47
        editProfileBtn.layer.cornerRadius = 25
        //notificationCount.layer.cornerRadius = 4
        
        let AppliedJobsTap = UITapGestureRecognizer(target: self, action: #selector(openApplliedJobs))
        appliedJobsSTV.isUserInteractionEnabled = true
        appliedJobsSTV.addGestureRecognizer(AppliedJobsTap)
        
        let FavsTap = UITapGestureRecognizer(target: self, action: #selector(openFavs))
        
        FavsSTV.isUserInteractionEnabled = true
        FavsSTV.addGestureRecognizer(FavsTap)
        
        let changePassTap = UITapGestureRecognizer(target: self, action: #selector(openChangePassword))
        changePassSTV.isUserInteractionEnabled = true
        changePassSTV.addGestureRecognizer(changePassTap)
        userInfoPresenter.attachView(view: self)
        presenter.attachView(view: self)
        userInfoPresenter.doGetUserInfo()
        setupSideMenu()
        updateMenus()
    }
    
    func setupProfile(){
        if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
            if let profileImgUrl = URL(string: profileImg ){
                self.userImage.kf.setImage(with: profileImgUrl)
                //self.userImage.isHidden = true
            }
        }
        else {
            if let gender = UserManager.sharedInstance.getUser()?.gender {
                if gender == "female" {
                    self.userImage.image = UIImage(named: "woman")
                }
                else{
                    self.userImage.image = UIImage(named: "profile_img")
                }
            }
        }
        if let name = UserManager.sharedInstance.getUser()?.name {
            userName.text = name
        }
        
    }
    
    @objc func openApplliedJobs(){
        print(L102Language.currentAppleLanguage())
        self.pushViewController(viewControllerName: "AppliedJobsViewController", storyBoardName: "Jobs")
    }
    @objc func openFavs(){
        if UserManager.sharedInstance.getUser() != nil {
            performSegue(withIdentifier: "PreferencesViewController", sender: self)
        }
        else{
            
        }
    }
    @objc func openChangePassword(){
        performSegue(withIdentifier: "ChangePasswordViewController", sender: self)
    }
    @IBAction func DismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
 }
 extension ProfileVCtrl:UserInfoView{
    func showLoadingUserInfoIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingUserInfoIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucessUserInfo() {
        setupProfile()
    }
    
    func onErrorUserInfo(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
 }
 extension ProfileVCtrl :RegisterView{
     func onSuccessGetCaptch(captchaCode: String) {
     }
     func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
     }
     
     func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
     }
     
     func onSucess() {
        setupProfile()
             
     }
     
     func onError(message: String) {
         self.showErrorMessage(message: message)
     }
     
     
 }
