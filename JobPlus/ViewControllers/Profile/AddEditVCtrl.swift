//
//  AddEditVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 11/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField
import TransitionButton

class AddEditVCtrl: UIViewController {
    
    
    @IBOutlet weak var saveTitle: TransitionButton!
    var delegate: SendDataDelegate?
    let presenter2 = RegisterPresenter()
    let presenter = DegreeListPresenter()
    var LanguageList = [LangItem]()
    var languageArray = ["a".localized() , "b".localized()]
    var language = ""
    var selectedLangId:Int = 0
    var profeciencyList = [languageStrengthItem]()
    var profeciencyArray = ["c".localized() , "d".localized()]
    var profeciency = ""
    var LanguageId : Int = 0
    var profeciencyId:Int = 0
    var spinnerView: UIView?
    var LanguageIdForEdit : Int = 0
    var profeciencyIdForEdit : Int = 0
    
    var selectedlanguages =  [languageItemFromAPi]()
    @IBOutlet weak var popupTitle: UILabel!
    
    @IBOutlet weak var languageTxtFeild: TweeAttributedTextField!
    
    @IBOutlet weak var profeciencyTxtFeild: TweeAttributedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popupTitle.text = "Add language".localized()
        saveTitle.setTitle("Add".localized(), for: .normal)
        
        if selectedLangId != 0 {
            print("selectedLangId from edit :  \(selectedLangId)")
            popupTitle.text = "Edit language".localized()
            saveTitle.setTitle("Edit".localized(), for: .normal)
        }
        self.view.changeFont()
        presenter2.attachView(view: self)
        presenter.attachView(view: self)
        presenter.doGetcertificatesNewList()
        
    }
    
    
    @IBAction func saveBtnAction(_ sender: Any) {
        
        if(validateLoginTextFields()) {
            print("valid form")
            let user = UserManager.sharedInstance.getUser()
            let lang = languageObjectToPost()
            lang.userLanguages = [LanguageObj]()
            var arr1  =  [LanguageObj]()
            
            if selectedLangId != 0 {
                print("edit")
                for item in selectedlanguages {
                    if selectedLangId == item.id {
                        item.language?.id = LanguageId
                        item.languageStrength?.id = profeciencyId
                    }
                }
            }
            
                
            var selectedLangIds:[Int] = []
            
            for item in selectedlanguages {
                arr1.append(LanguageObj(language_id: (item.language?.id)!, languageStrength_id: (item.languageStrength?.id)!))
                
                selectedLangIds.append((item.language?.id)!)
            }
            lang.userLanguages.append(contentsOf: arr1)
    
          
            if selectedLangId != 0 {
                print("edit")
//                for item in selectedlanguages {
//                    if selectedLangId == item.id {
//                        item.language?.id = LanguageId
//                        item.languageStrength?.id = profeciencyId
//                    }
//                }
            }else{
            if selectedLangIds.contains(LanguageId){
                self.showErrorMessage(message: "Picking same language before".localized())
                self.dismiss(animated: true, completion: nil)
            }else{
                lang.userLanguages.append(LanguageObj(language_id: LanguageId, languageStrength_id: profeciencyId))
            }
            }
//            if (LanguageId == LanguageIdForEdit) && (profeciencyId == profeciencyIdForEdit) {
//                print("data not edited")
//            }else{
//                let index = selectedlanguages.firstIndex(where: {$0.id == LanguageId})
//                if index == nil, LanguageId == 0 {
//                    lang.userLanguages.append(LanguageObj(language_id: LanguageId, languageStrength_id: profeciencyId))
//
//                }
//                else{
//                    self.showErrorMessage(message: "Picking same language before".localized())
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//            }
            user?.userLanguages2 =  lang
            user?.SectionID = 9
            presenter2.doEditNewProfile(data: user!)
        }
    }
    func validateLoginTextFields() -> Bool {
            var valid = true
            
            if let lang = self.languageTxtFeild.text , !lang.isBlank {
                print("valid lang")
            }else {
                print("error lang")
                self.showErrorMessage(message: "Please enter language".localized())
                
                valid = false
            }
            
            if let profeciency = self.profeciencyTxtFeild.text , !profeciency.isBlank {
                print("valid profeciency")
            }else {
                print("error profeciency")
                if valid {
                    self.showErrorMessage(message: "Please enter language profeciency".localized())
                }
                
                
                valid = false
            }
            return valid
        }
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func chooseLanguage(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let languagePicker = UIPickerView()
        let languageArrayToDisplay = L102Language.isRTL ? LanguageList.map {$0.nameAr} : LanguageList.map {$0.nameEn}
        if languageArrayToDisplay.count > 0 {
            
            languagePicker.addCustomPicker(languageArrayToDisplay as NSArray , sender , "Language", 0) { (selectedIndex) in
                
                
                self.languageTxtFeild.text = languageArrayToDisplay[selectedIndex]
                self.LanguageId = self.LanguageList[selectedIndex].id!
                
            }
            
        }else{
            print("no date in languagesArrayToDisplay")
        }
         
    }
    
    
    @IBAction func chooseProfeciency(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let ProfeciencyPicker = UIPickerView()
        let ProfeciencyArrayToDisplay = L102Language.isRTL ? profeciencyList.map {$0.nameAr} : profeciencyList.map {$0.nameEn}
        
        ProfeciencyPicker.addCustomPicker(ProfeciencyArrayToDisplay as NSArray , sender , "Profeciency", 0) { (selectedIndex) in
            
            self.profeciencyTxtFeild.text = ProfeciencyArrayToDisplay[selectedIndex]
            self.profeciencyId = self.profeciencyList[selectedIndex].id!        }
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    

extension AddEditVCtrl :DegreeListView {
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    func showLoading() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        
        if let languages = degreeWorkingTimes.languages {
            if languages.count > 0 {
                self.LanguageList.removeAll()
                self.LanguageList.append(contentsOf: languages)
                print("self.languagesList : \(LanguageList)")
            }}
        
        
        
        if let languages = degreeWorkingTimes.languageStrength {
            if languages.count > 0 {
                self.profeciencyList.removeAll()
                self.profeciencyList.append(contentsOf: languages)
                print("self.profeciencyList : \(profeciencyList)")
            }}
        
        if selectedLangId != 0 {
            print("selectedLangId from edit :  \(selectedLangId)")
            
            for item in selectedlanguages{
                if item.id == selectedLangId{
                    self.languageTxtFeild.text = L102Language.isRTL ? item.language?.nameAr : item.language?.nameEn
                    self.LanguageIdForEdit = (item.language?.id)!
                    self.LanguageId = (item.language?.id)!
                    
                    self.profeciencyTxtFeild.text = L102Language.isRTL ? item.languageStrength?.nameAr : item.languageStrength?.nameEn
                    self.profeciencyIdForEdit = (item.languageStrength?.id)!
                    self.profeciencyId = (item.languageStrength?.id)!
                    
                }
            }
        }
    }
}
extension AddEditVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucess() {
        print("on sucess")
        let user1  = UserManager.sharedInstance.getUser()
        UserManager.sharedInstance.saveUser(user:user1!)
        self.delegate?.sendData(text: "language")
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

