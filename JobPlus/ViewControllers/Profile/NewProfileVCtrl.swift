//
//  NewProfileVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 03/03/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import SideMenu
import ALCameraViewController
import MobileCoreServices
import Floaty

class certificateTableviewCell : UITableViewCell{
    
    
    @IBOutlet weak var certificateNameHeight: NSLayoutConstraint!
    @IBOutlet weak var editCertificate: UIButton!
    @IBOutlet weak var certificateDate: UILabel!
    @IBOutlet weak var certificateName: UILabel!
    @IBOutlet weak var removeCertificate: UIButton!
}

class expTableViewCell:UITableViewCell{
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var noticePeriodTxt: UILabel!
    @IBOutlet weak var periodHeight: NSLayoutConstraint!
    @IBOutlet weak var dateTxt: UILabel!
    @IBOutlet weak var dateHeight: NSLayoutConstraint!
    @IBOutlet weak var countryHeight: NSLayoutConstraint!
    @IBOutlet weak var workSectorHeight: NSLayoutConstraint!
    @IBOutlet weak var companyNameHeight: NSLayoutConstraint!
    @IBOutlet weak var jobTypeHeight: NSLayoutConstraint!
    @IBOutlet weak var jobTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var jobTitleTxt: UILabel!
    @IBOutlet weak var companyNameTxt: UILabel!
    @IBOutlet weak var jobTypeTxt: UILabel!
    @IBOutlet weak var workSectorTxt: UILabel!
    @IBOutlet weak var countryTxt: UILabel!
}

class skillTableViewCell:UITableViewCell{
    @IBOutlet weak var removeSkillBtn: UIButton!
    @IBOutlet weak var skillTitle: UILabel!
}

class educationINfoTableViewCell:UITableViewCell{
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var describtion: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    
    
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    
    
}

struct educatoinInfoCellObj {
    var eduTitle:String
    var eduDescribtion:String
    var eduYear:String
    var eduCountry:String
}

class NewProfileVCtrl: UIViewController ,UITableViewDelegate,UITableViewDataSource , SendDataDelegate ,UIDocumentPickerDelegate, SendDataDelegate2 , AddBioDelegate,FloatyDelegate{
    
    func BioAdded(text: String) {
        print("bio added")
        self.setProfileData()
    }
    
    let floaty = Floaty()
    var DeletesectionId : Int  = 0
    var deletMsg : String = ""
    var deleteId : Int = 0
    let certificatesNewpresenter = DegreeListPresenter()
    var selectedSoftSkillsIds:[Int] = []
    var selectedTechSkillsIds:[Int] = []
    var selectedLangId : Int = 0
    
    var selectedCertificateId : Int = 0
    var selectededuId : Int = 0
    var selectedSkillsIds: [Int] = []
    var skillTxt : String = ""
    
    
    @IBOutlet weak var genderHieght: NSLayoutConstraint!
    
    
    @IBOutlet weak var resedienceCityHeight: NSLayoutConstraint!
    
    @IBOutlet weak var jobTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var nationalityHeight: NSLayoutConstraint!
    
    @IBOutlet weak var fullNameHeight: NSLayoutConstraint!
    
    @IBOutlet weak var userTxtHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var profileSCrollView: UIScrollView!
    
    @IBOutlet weak var disibilityLbl: UILabel!
    @IBOutlet weak var disabilityHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var disibilityTopConstrain: NSLayoutConstraint!
    var educationInfoData = [educatoinInfoCellObj(eduTitle: "title1", eduDescribtion: "desc1", eduYear: "year1", eduCountry: "counrty2"),educatoinInfoCellObj(eduTitle: "title1", eduDescribtion: "desc2", eduYear: "year1", eduCountry: "counrty1")]
    
    var softSkillData = [skillItemFromAPi]()
    var TechSkillData = [techSkillItem]()
    var LanguagesData = [languageItemFromAPi]()
    var eduactionData = [educationItemFromAPi]()
    var certificatesData = [certificateItemFromAPi]()
    
    var experiencesData = [Experience]()
    var birthDateTime:Int?
    var gender = ""
    var selectedCountryIndex: Int?
    var selectedCountry = [String]()
    var selectedUserCountryId:Int?
    var cvViewerType = 1 // 1 -> open new file , 2 -> open uploaded file
    var documentInteractionController: UIDocumentInteractionController!
    let allowedFileExtension = ["doc","docx","pdf"]
    var CVbase64String = ""
    var CVExtension = ""
    var CVUrl = ""
    var cvPrevUploaded = false
    var cvPrevUrl = ""
    var SelectedNationality = ""
    var selectedexpId : Int = -1
    
    @IBOutlet weak var languageBorder: UILabel!
    @IBOutlet weak var certificatesBorder: UILabel!
    @IBOutlet weak var skillsBorder: UILabel!
    @IBOutlet weak var experienceBorder: UILabel!
    @IBOutlet weak var educationBorder: UILabel!
    @IBOutlet weak var contactInfoBorder: UILabel!
    @IBOutlet weak var personalInfoBorder: UILabel!
    @IBOutlet weak var accountSettingBorder: UILabel!
    @IBOutlet weak var languageTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var languageTableView: UITableView!
    @IBOutlet weak var uploadedCvbtn: UIButton!
    @IBOutlet weak var cityTxt: UILabel!
    @IBOutlet weak var cvToUploadBtn: UIButton!
    @IBOutlet weak var techSkillsTableView: UITableView!
    @IBOutlet weak var softSkillsTableView: UITableView!
    @IBOutlet weak var preferredContactOption: UILabel!
    @IBOutlet weak var techTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bioHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var bioLblHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var countryTxt: UILabel!
    @IBOutlet weak var mobileTxt: UILabel!
    @IBOutlet weak var emailTxt: UILabel!
    @IBOutlet weak var jobTitleTxt: UILabel!
    @IBOutlet weak var genderTxt: UILabel!
    @IBOutlet weak var main_view_height: NSLayoutConstraint!
    @IBOutlet weak var userNameTxt: UILabel!
    @IBOutlet weak var ageTxt: UILabel!
    @IBOutlet weak var fullNameTxt: UILabel!
    @IBOutlet weak var userBioTxt: UILabel!
    @IBOutlet weak var cerificatesTableView: UITableView!
    @IBOutlet weak var drivingLicenseTxt: UILabel!
    @IBOutlet weak var nationalityTxt: UILabel!
    @IBOutlet weak var education_info_tableView: UITableView!
    @IBOutlet weak var experienceTableView: UITableView!
    @IBOutlet weak var educationTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var movableScrollView: UIScrollView!
    @IBOutlet weak var movableView: UIStackView!
    @IBOutlet weak var certificatesTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var experioenceTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var softSkillTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var userImgContainerView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var accountSettingsView: UIView!
    @IBOutlet weak var personalInfoView: UIView!
    @IBOutlet weak var contactInfoView: UIView!
    @IBOutlet weak var educationInfoView: UIView!
    @IBOutlet weak var workExpView: UIView!
    @IBOutlet weak var skillsView: UIView!
    @IBOutlet weak var certificatesView: UIView!
    @IBOutlet weak var languagesView: UIView!
    @IBOutlet weak var showMoreBtn: UIButton!
    
    var userInfoPresenter:UserInfoPresenter = UserInfoPresenter()
    let presenter = RegisterPresenter()
    var selectedProfileImage :UIImage?
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = true
    var allowResizing: Bool = true
    var allowMoving: Bool = true
    var minimumSize: CGSize = CGSize(width: 200, height: 200)
    var spinnerView: UIView?
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    
    func sendData (text:String) {
        print("new profile send data from delegate : \(text)")
        self.setProfileData()
    }
    
    @IBAction func showMoreBio(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BioViewCtrl") as? BioVCtrl {
            viewController.viewOnly = true
            viewController.modalPresentationStyle = .overCurrentContext
            self.present(viewController, animated: true, completion: nil)
        }
    }
    func sendData2 (text:String) {
        print("new profile send data2 from delegate : \(text)")
        self.setProfileData()
    }
    
    //    func sendData3 (text:String) {
    //        print("new profile send data3 from delegate : \(text)")
    //        self.setProfileData()
    //         }
    
    
    @IBAction func fileUpload(_ sender: Any) {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
        documentPicker.delegate = self
        
        self.present(documentPicker, animated: true)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        let file_name = urls[0].lastPathComponent
        let file_extension = file_name.split(separator: ".")
        CVExtension = String(file_extension[file_extension.count - 1]).lowercased()
        let index = allowedFileExtension.firstIndex(of: CVExtension)
        
        if index ?? -1 >= 0 {
            self.CVUrl = urls[0].absoluteString
            let data = try! Data(contentsOf: urls[0])
            CVbase64String = data.base64EncodedString()
            let cvMsg = NSLocalizedString("New CV Uploaded", comment: "")
            self.cvToUploadBtn.setTitle(cvMsg, for: .normal)
            self.cvToUploadBtn.setTitle(cvMsg, for: .selected)
            let user = UserManager.sharedInstance.getUser()
            user?.SectionID = 10
            user?.cv = CVbase64String
            user?.cv_extension = CVExtension
            presenter.doEditNewProfile(data: user!)
            DispatchQueue.main.async {
                self.cvToUploadBtn.isHidden = false
                if self.cvPrevUploaded {
                    //                    self.contact_top_height_constraint.constant = 78
                }
                else{
                    //                self.contact_top_height_constraint.constant = 42
                }
            }
            
        }
        else{
            self.showErrorMessage(message: NSLocalizedString("Not Allowed Extention", comment: ""))
        }
        
    }
    
    @IBAction func uploadedCVAction(_ sender: Any) {
        cvViewerType = 2
        performSegue(withIdentifier: "openCVSeg", sender: self)
    }
    
    @IBAction func cvTouploadAction(_ sender: Any) {
        cvViewerType = 1
        performSegue(withIdentifier: "openCVSeg", sender: self)
    }
    
    
    
    @IBAction func addExperience(_ sender: Any) {
        
        
        if experiencesData.count >= 4 {
            self.showErrorMessage(message: "You reached the max number of allowed work experience items".localized())
        }else{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "addexp", sender: self)
            }
        }
        
        
    }
    
    
    @IBAction func addEducation(_ sender: Any) {
        
        //        profileSCrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)
        if eduactionData.count >= 3 {
            self.showErrorMessage(message: "You reached the max number of allowed educational information items".localized())
        }else{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "eduSegue", sender: self)
            }
        }
        
        
    }
    
    
    
    
    @IBAction func generateCvAction(_ sender: Any) {
        
//        if let subscribedUser = UserManager.sharedInstance.subscribedUser {
//            if subscribedUser {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "generateCvSeg", sender: self)
                }
//            }else{
//                
//                self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
//                
//                
//            }}
//        
//        
        
    }
    
    
    
    
    @IBAction func addTechnicalSkill(_ sender: Any) {
        
        skillTxt = "tech"
        selectedSkillsIds = selectedTechSkillsIds
        if selectedSkillsIds.count >= 6 {
            self.showErrorMessage(message: "You reached the max number of allowed technical skills items".localized())
        }else{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "addSkill", sender: self)
            }
        }
        
    }
    
    
    @IBAction func addSoftSkill(_ sender: Any) {
        skillTxt = "soft"
        selectedSkillsIds = selectedSoftSkillsIds
        if selectedSkillsIds.count >= 10 {
            self.showErrorMessage(message: "You reached the max number of allowed soft skills items".localized())
        }else{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "addSkill", sender: self)
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        //        var b = BioVCtrl()
        //        b.delegate = self
        floaty.buttonColor = #colorLiteral(red: 1, green: 0.7725490196, blue: 0.2235294118, alpha: 1)
        floaty.buttonImage = UIImage(named: "upArrow")
        floaty.fabDelegate = self
        let item = FloatyItem()
        item.buttonColor = UIColor.clear
        item.circleShadowColor = UIColor.clear
        item.titleShadowColor = UIColor.clear
        if L102Language.isRTL {
            Floaty.global.rtlMode = true
        }
        floaty.addItem(item: item)
        //        self.view.addSubview(floaty)
        
        
        self.userBioTxt.text = "Profile Bio".localized()
        
        profileSCrollView.delegate = self
        
        cerificatesTableView.delegate = self
        cerificatesTableView.dataSource = self
        cerificatesTableView.tableFooterView = UIView()
        
        experienceTableView.delegate = self
        experienceTableView.dataSource = self
        experienceTableView.tableFooterView = UIView()
        
        education_info_tableView.delegate = self
        education_info_tableView.dataSource = self
        education_info_tableView.tableFooterView = UIView()
        
        
        softSkillsTableView.delegate = self
        softSkillsTableView.dataSource = self
        softSkillsTableView.tableFooterView = UIView()
        
        techSkillsTableView.delegate = self
        techSkillsTableView.dataSource = self
        techSkillsTableView.tableFooterView = UIView()
        
        languageTableView.delegate = self
        languageTableView.dataSource = self
        languageTableView.tableFooterView = UIView()
        
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
            self.movableScrollView.transform = CGAffineTransform(rotationAngle: CGFloat(CGFloat.pi));
            self.movableView.transform =  CGAffineTransform(rotationAngle: CGFloat(CGFloat.pi));
        }
        if L102Language.isRTL { SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
        }
        else{ SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
            
        }
        userImgContainerView.layerGradient()
        userImage.layer.cornerRadius = 47
        setupSideMenu()
        updateMenus()
        certificatesNewpresenter.attachView(view: self)
        certificatesNewpresenter.doGetcertificatesNewList()
        presenter.attachView(view: self)
        userInfoPresenter.attachView(view: self)
        userInfoPresenter.doGetUserInfo()
        setProfileData()
        initPullToRefresh()
    }
    func initPullToRefresh()
    {
        self.view.isUserInteractionEnabled = false
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        profileSCrollView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.userInfoPresenter.doGetUserInfo()
            }, loadingView: loadingView)
        self.profileSCrollView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        //self.profileSCrollView?.dg_setPullToRefreshBackgroundColor(profileSCrollView.backgroundColor!)
    }
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
        scrollToTop()
        floaty.close()
        
    }
    
    
    
    private func scrollToTop() {
        print("scrollToTop")
        profileSCrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func setProfileData(){
        print("gat  data again")
        DispatchQueue.main.async {
            self.main_view_height.constant = 1640
        }
        selectedSkillsIds.removeAll()
        selectedSoftSkillsIds.removeAll()
        selectedTechSkillsIds.removeAll()
        if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
            if let profileImgUrl = URL(string: profileImg ){
                
                self.userImage.kf.setImage(with: profileImgUrl, placeholder: UIImage(named: "profile_img"), completionHandler: {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    
                    }
                })
            }
        }
        else {
            if let gender = UserManager.sharedInstance.getUser()?.gender {
                if gender == "female" {
                    self.userImage.image = UIImage(named: "woman")
                }
                else{
                    self.userImage.image = UIImage(named: "profile_img")
                }
            }
            else{
                self.userImage.image = UIImage(named: "profile_img")
            }
        }
        if let mobile = UserManager.sharedInstance.getUser()?.mobile {
            
            if let countryCode = UserManager.sharedInstance.getUser()?.mobileCountryPrefix {
                self.mobileTxt.text =  "\(countryCode)\(mobile)"
            }
        }
        
        if let contact = UserManager.sharedInstance.getUser()?.selectedContact {
            self.preferredContactOption.text = "Preferred contact option".localized() + " : " + contact
            
        }
        
        if let name = UserManager.sharedInstance.getUser()?.name {
            
            if let lastname = UserManager.sharedInstance.getUser()?.LastName {
                print("new profile 1name : \(name)")
                print("new profile 2lastName : \(lastname)")
                fullNameTxt.text = name + " " + lastname
                userNameTxt.text = name + " " + lastname
                
                fullNameHeight.constant = fullNameTxt.text!.height(constraintedWidth: fullNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                
                userTxtHeightConstraint.constant = userNameTxt.text!.height(constraintedWidth: userNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                self.main_view_height.constant += fullNameHeight.constant
                self.main_view_height.constant += userTxtHeightConstraint.constant
                
            }else{
                print("no last name")
                fullNameTxt.text = name
                userNameTxt.text = name
                fullNameHeight.constant = fullNameTxt.text!.height(constraintedWidth: fullNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                
                userTxtHeightConstraint.constant = userNameTxt.text!.height(constraintedWidth: userNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                self.main_view_height.constant += fullNameHeight.constant
                self.main_view_height.constant += userTxtHeightConstraint.constant
            }
        }
        if let nat = UserManager.sharedInstance.getUser()?.NationalityObject {
            nationalityTxt.text = L102Language.isRTL ? nat.nameAr : nat.nameEn
            
            nationalityHeight.constant = nationalityTxt.text!.height(constraintedWidth: nationalityTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            self.main_view_height.constant += nationalityHeight.constant
            
            
        }
        if let bio = UserManager.sharedInstance.getUser()?.userBio {
            
            userBioTxt.text = bio
            let numberOfLines = userBioTxt.calculateMaxLines()
            if numberOfLines == 1 {
                self.bioHeightConstrain.constant = 40
                self.bioLblHeightConstrain.constant = 40
                self.showMoreBtn.isHidden = true
            }
            else if numberOfLines == 2 {
                self.bioHeightConstrain.constant = 90
                self.bioLblHeightConstrain.constant = 80
                self.showMoreBtn.isHidden = true
            }
            else if numberOfLines == 3 {
                self.bioHeightConstrain.constant = 110
                self.bioLblHeightConstrain.constant = 100
                self.showMoreBtn.isHidden = true
            }
            else{
                self.bioLblHeightConstrain.constant = 100
                self.bioHeightConstrain.constant = 130
                self.showMoreBtn.isHidden = false
            }
        }
        else{
            self.bioHeightConstrain.constant = 40
            self.bioLblHeightConstrain.constant = 40
            self.showMoreBtn.isHidden = true
        }
        if let nat = UserManager.sharedInstance.getUser()?.PreferedWork {
            jobTitleTxt.text = nat
            jobTitleHeight.constant = jobTitleTxt.text!.height(constraintedWidth: jobTitleTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            self.main_view_height.constant += jobTitleHeight.constant
            
        }
        
        if let driving = UserManager.sharedInstance.getUser()?.DrivingLicense {
            print("driving : \(driving)")
            if driving == 0 {
                drivingLicenseTxt.text = "Not Have Driving License".localized()
            }else{
                drivingLicenseTxt.text = "Have Driving License".localized()
            }
            
        }
        
        if let cvPath = UserManager.sharedInstance.getUser()?.cvPath, cvPath.count > 0 {
            self.cvPrevUploaded = true
            self.cvPrevUrl = cvPath
            let cvMsg = NSLocalizedString("Show CV", comment: "")
            self.uploadedCvbtn.setTitle(cvMsg, for: .normal)
            self.uploadedCvbtn.setTitle(cvMsg, for: .selected)
            DispatchQueue.main.async {
                self.uploadedCvbtn.isHidden = false
                self.cvToUploadBtn.isHidden = true
            }
        }
        
        if let experiences = UserManager.sharedInstance.getUser()?.experiences {
            self.experiencesData = experiences
            print("experiences: \(experiences)")
            
            
            var tabelHeight:Double = 0
            //
            for (index,item) in experiencesData.enumerated(){
                print("index :\(index)")
                let jobTitleHeight = item.jobTitle.height(constraintedWidth: UIScreen.main.bounds.width - 90 , font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
                
                let companyNameHeight = item.companyName.height(constraintedWidth: UIScreen.main.bounds.width - 90, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
                
                var jobTypeHeight:CGFloat = 0
                if let x = item.jobtype?.name {
                    jobTypeHeight = x.height(constraintedWidth: UIScreen.main.bounds.width - 90, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
                }
                var workSectorHeight:CGFloat = 0
                
                if let x  =  item.worksector?.name {
                    workSectorHeight = x.height(constraintedWidth: UIScreen.main.bounds.width - 90, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
                }
                var countryHeight:CGFloat = 0
                
                if let x  = item.country?.nameAr {
                    countryHeight = x.height(constraintedWidth: UIScreen.main.bounds.width - 90, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
                }
                
                
                
                var year:String = ""
                var month:String = ""
                
                if (item.yearsOfExperience != nil) {
                    year = String(item.yearsOfExperience!) + "Years".localized()
                }
                if (item.month_of_experience != nil) {
                    month =  String(item.month_of_experience!) + "Months".localized()
                }
                
                var periodHeight:CGFloat = 0
                if let x = item.noticePeriod {
                    periodHeight = x.height(constraintedWidth: UIScreen.main.bounds.width - 90, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
                    
                }
                let h1 = jobTitleHeight+companyNameHeight+jobTypeHeight
                
                
                var h2 = workSectorHeight+countryHeight+100+periodHeight
                
                if item.jobTitle.count >= 60  ||  item.companyName.count >= 60 {
                    h2 = h2 + 10
                }
                if item.jobTitle.count >= 60  &&  item.companyName.count >= 60{
                    h2 = h2 + 20
                }
                item.cellHeight = Double(h1+h2)
                
                tabelHeight += item.cellHeight!
                
            }
            
            
            DispatchQueue.main.async {
                //                self.experioenceTableViewHeightConstraint.constant = CGFloat(self.experiencesData.count * 230)
                
                self.experioenceTableViewHeightConstraint.constant = CGFloat(tabelHeight )
                
                
                
                
                self.main_view_height.constant += self.experioenceTableViewHeightConstraint.constant
            }
            
            experienceTableView.reloadData()
            
        }
        
        if let softSkills = UserManager.sharedInstance.getUser()?.softSkills {
            print("softSkills: \(softSkills)")
            self.softSkillData.removeAll()
            self.softSkillData = softSkills
            DispatchQueue.main.async {
                self.softSkillTableHeightConstraint.constant = CGFloat(self.softSkillData.count * 75)
                self.main_view_height.constant += self.softSkillTableHeightConstraint.constant
            }
            softSkillsTableView.reloadData()
        }
        
        
        if let softSkills = UserManager.sharedInstance.getUser()?.techSkills {
            
            print("techSkills: \(softSkills)")
            self.TechSkillData.removeAll()
            for item in softSkills{
                
                self.TechSkillData.append( techSkillItem(id: item.techSkill?.id, nameEn: item.techSkill?.nameEn, nameAr: item.techSkill?.nameAr ))
            }
            print(" self.softSkillData : \( self.TechSkillData)")
            DispatchQueue.main.async {
                self.techTableViewHeightConstraint.constant = CGFloat(self.TechSkillData.count * 75)
                self.main_view_height.constant += self.techTableViewHeightConstraint.constant
            }
            
            techSkillsTableView.reloadData()
        }
        if let certificates = UserManager.sharedInstance.getUser()?.userCertificates {
            self.certificatesData = certificates
            print("certificates: \(certificates)")
            
            var tabelHeight:Double = 0
            //
            for (index,item) in certificatesData.enumerated(){
                print("index :\(index)")
                var height = 0
                if let instit = item.Institution {
                    let txt = L102Language.isRTL ? instit.nameAr : instit.nameEn
                    height = Int((txt ?? "").height(constraintedWidth:  UIScreen.main.bounds.width - 50 , font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!))
                }
                else if let instit = item.InstitutionID, instit <= 0 {
                    if let institText = item.InstitutionText, !(institText.isBlank){
                        height = Int((institText).height(constraintedWidth:  UIScreen.main.bounds.width - 50 , font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!))
                    }
                }
                height += Int((item.certificateName ?? "").height(constraintedWidth:  UIScreen.main.bounds.width - 50 , font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!))
                
                print("h= : \(height)")
                item.cellHeight = Double(height + 120)
                
                tabelHeight += item.cellHeight!
                print("tableHeight : \(tabelHeight)")
                
            }
            DispatchQueue.main.async {
                self.certificatesTableViewHeight.constant = CGFloat(tabelHeight)
                self.main_view_height.constant += self.certificatesTableViewHeight.constant
            }
            
            self.cerificatesTableView.reloadData()
            
        }
        
        if let langs = UserManager.sharedInstance.getUser()?.userLanguages {
            self.LanguagesData = langs
            DispatchQueue.main.async {
                self.languageTableViewHeight.constant = CGFloat(self.LanguagesData.count * 52)
                self.main_view_height.constant += self.languageTableViewHeight.constant
            }
            
            languageTableView.reloadData()
        }
        
        
        
        if let edu = UserManager.sharedInstance.getUser()?.EducationalInformation {
            self.eduactionData = edu
            var tabelHeight:Double = 0
            //
            for (index,item) in eduactionData.enumerated(){
                var titleHeight:CGFloat = 25
                if let intName = item.Institution_Name{
                    titleHeight = intName.height(constraintedWidth: UIScreen.main.bounds.width - 90, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                }
                
                let degree = L102Language.isRTL ? item.EduInfoDegree?.certificateAr : item.EduInfoDegree?.certificateEn
                let major = item.majorName
                let t2 = (degree ?? "") + " , " + (major ?? "" ) + "\n" + (item.grades ?? "")
                let descriptionHeight = t2.height(constraintedWidth: UIScreen.main.bounds.width - 50, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                let height = Double(titleHeight + descriptionHeight + 80)
                item.cellHeight = height
                
                print("item height from for  : \(item.cellHeight ?? 0.0)")
                tabelHeight += item.cellHeight!
                print("tableHeight : \(tabelHeight)")
                
            }
            
            
            
            DispatchQueue.main.async {
                self.educationTableViewHeightConstraint.constant = CGFloat(tabelHeight)
                
                self.main_view_height.constant += self.educationTableViewHeightConstraint.constant
            }
            
            education_info_tableView.reloadData()
        }
        
        if let age = UserManager.sharedInstance.getUser()?.birthDate {
            birthDateTime = Int(age)
        }
        if let agestring = UserManager.sharedInstance.getUser()?.birthDateString {
            ageTxt.text = agestring
            
        }
        if let email = UserManager.sharedInstance.getUser()?.email {
            emailTxt.text = email
        }
        
        
        if let gendure = UserManager.sharedInstance.getUser()?.gender {
            genderHieght.constant = 30
            self.gender = gendure
            genderTxt.text = gendure.capitalized.localized()
        }
        else{
            genderHieght.constant = 0
        }
        
        if let userCountry = UserManager.sharedInstance.getUser()?.country{
            if let userCountryName = userCountry.localizeName {
                self.selectedCountry.append(userCountryName)
                countryTxt.text = userCountryName
            }
            if let userCountryId = userCountry.id{
                self.selectedUserCountryId = userCountryId
            }
            
        }
        if let City_Residence = UserManager.sharedInstance.getUser()?.City_Residence_Name {
            self.cityTxt.text = City_Residence
            
            resedienceCityHeight.constant = cityTxt.text!.height(constraintedWidth: cityTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            print("resedienceCityHeight.constant:\(resedienceCityHeight.constant)")
            
        }
        print("self.main_view_height.constant :\(self.main_view_height.constant )")
        if let disiblityType = UserManager.sharedInstance.getUser()?.DisabilityType {
            if let disiblityStatus = UserManager.sharedInstance.getUser()?.Disability {
                if disiblityStatus == 1 {
                    self.disibilityTopConstrain.constant = 7.5
                    self.disabilityHeightConstrain.constant = 30
                    if UserManager.sharedInstance.disabilityyItems.count > 0 {
                        let disabilities = disiblityType.split(separator: ",")
                        for i in 0..<disabilities.count {
                            if let selected = (UserManager.sharedInstance.disabilityyItems).first(where: {$0.id == (Int(disabilities[i]) ?? -1)}) {
                                if let selectedName = L102Language.isRTL ? selected.nameAr : selected.nameEn  {
                                    
                                    if i == 0 {
                                        self.disibilityLbl.text = "\("Disability".localized()): \(selectedName)"
                                    }
                                    else{
                                        self.disibilityLbl.text = "\(self.disibilityLbl.text ?? ""), \(selectedName)"
                                    }
                                }
                            }
                        }
                        let height = disibilityLbl.text!.height(constraintedWidth: fullNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 19.0)!)
                        disabilityHeightConstrain.constant = height
                        main_view_height.constant += height
                    }
                }
                else{
                    self.disibilityTopConstrain.constant = 0
                    self.disabilityHeightConstrain.constant = 0
                    self.disibilityLbl.text = ""
                }
            }
            else{
                self.disibilityTopConstrain.constant = 0
                self.disabilityHeightConstrain.constant = 0
                self.disibilityLbl.text = ""
            }
        }
        else{
            self.disibilityTopConstrain.constant = 0
            self.disabilityHeightConstrain.constant = 0
            self.disibilityLbl.text = ""
        }
    }
    @IBAction func goToLanguages(_ sender: Any) {
        
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = true
        experienceBorder.isHidden = true
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = true
        languageBorder.isHidden = false
        
        profileSCrollView.setContentOffset(CGPoint(x: languagesView.frame.minX, y: languagesView.frame.minY), animated: true)
    }
    
    @IBAction func goToCertificates(_ sender: Any) {
        
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = true
        experienceBorder.isHidden = true
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = false
        languageBorder.isHidden = true
        
        profileSCrollView.setContentOffset(CGPoint(x: certificatesView.frame.minX, y: certificatesView.frame.minY), animated: true)
    }
    
    @IBAction func goTOskill(_ sender: Any) {
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = true
        experienceBorder.isHidden = true
        skillsBorder.isHidden = false
        certificatesBorder.isHidden = true
        languageBorder.isHidden = true
        
        profileSCrollView.setContentOffset(CGPoint(x: skillsView.frame.minX, y: skillsView.frame.minY), animated: true)
    }
    
    @IBAction func gotoWorkExperience(_ sender: Any) {
        
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = true
        experienceBorder.isHidden = false
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = true
        languageBorder.isHidden = true
        
        profileSCrollView.setContentOffset(CGPoint(x: workExpView.frame.minX, y: workExpView.frame.minY) , animated: true)
    }
    
    
    @IBAction func goToAccountSettings(_ sender: Any) {
        accountSettingBorder.isHidden = false
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = true
        experienceBorder.isHidden = true
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = true
        languageBorder.isHidden = true
        profileSCrollView.setContentOffset(CGPoint(x: accountSettingsView.frame.minX, y: accountSettingsView.frame.minY), animated: true)
        
    }
    @IBAction func goToEducation(_ sender: Any) {
        
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = false
        experienceBorder.isHidden = true
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = true
        languageBorder.isHidden = true
        
        profileSCrollView.setContentOffset(CGPoint(x: educationInfoView.frame.minX, y: educationInfoView.frame.minY), animated: true)
    }
    
    @IBAction func gotoPersonalInfo(_ sender: Any) {
        
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = false
        contactInfoBorder.isHidden = true
        educationBorder.isHidden = true
        experienceBorder.isHidden = true
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = true
        languageBorder.isHidden = true
        
        profileSCrollView.setContentOffset(CGPoint(x: personalInfoView.frame.minX, y: personalInfoView.frame.minY), animated: true)
    }
    
    
    @IBAction func gotoContactInfo(_ sender: Any) {
        
        accountSettingBorder.isHidden = true
        personalInfoBorder.isHidden = true
        contactInfoBorder.isHidden = false
        educationBorder.isHidden = true
        experienceBorder.isHidden = true
        skillsBorder.isHidden = true
        certificatesBorder.isHidden = true
        languageBorder.isHidden = true
        
        profileSCrollView.setContentOffset(CGPoint(x: contactInfoView.frame.minX, y: contactInfoView.frame.minY), animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editPersonalInfo" {
            if let destnationViewcontroller = segue.destination as? editContactInfoVCtrl {
                destnationViewcontroller.delegate = self
                destnationViewcontroller.SelectedDisability = self.disibilityLbl.text ?? ""
                destnationViewcontroller.Natoinality = self.nationalityTxt.text ?? ""
            }
        }else if segue.identifier == "addSkill" {
            if let destnationViewcontroller = segue.destination as? SkillsVCtrl {
                destnationViewcontroller.fromView = skillTxt
                destnationViewcontroller.selectedSkillsIds = selectedSkillsIds
                destnationViewcontroller.delegate = self
            }
        }
        
        else  if segue.identifier == "openCVSeg" {
            if let destnationViewcontroller = segue.destination as? CVViewerViewController {
                if cvViewerType == 1 {
                    destnationViewcontroller.cvURL = CVUrl
                }
                else{
                    destnationViewcontroller.cvURL = cvPrevUrl
                }
            }
            
        }else  if segue.identifier == "addLanguage" {
            if let destnationViewcontroller = segue.destination as? AddEditVCtrl {
                destnationViewcontroller.selectedlanguages = self.LanguagesData
                destnationViewcontroller.selectedLangId = self.selectedLangId
                destnationViewcontroller.delegate = self
                self.selectedLangId = 0
            }
            
        }else  if segue.identifier == "addCertificate" {
            if let destnationViewcontroller = segue.destination as? addEditCertificateVCtrl {
                destnationViewcontroller.selectedcertificates = self.certificatesData
                destnationViewcontroller.selectedCertificateId = self.selectedCertificateId
                destnationViewcontroller.delegate = self
                self.selectedCertificateId = 0
            }
            
        }else if segue.identifier == "openBio" {
            if let destnationViewcontroller = segue.destination as? BioVCtrl {
                destnationViewcontroller.delegate = self
                
            }
            
        }else if segue.identifier == "contactSegue" {
            if let destnationViewcontroller = segue.destination as? contactVCtrl {
                destnationViewcontroller.delegate = self
                
            }
            
        }else if segue.identifier == "eduSegue" {
            if let destnationViewcontroller = segue.destination as? addEducationalInfoVCtrl {
                destnationViewcontroller.delegate = self
                destnationViewcontroller.selectededus = self.eduactionData
                destnationViewcontroller.selectededuId = self.selectededuId
                self.selectededuId = 0
                
            }
            
        }
        else if segue.identifier == "deleteSegue" {
            if let destnationViewcontroller = segue.destination as? DeltePopUpVCtrl {
                destnationViewcontroller.delegate = self
                if DeletesectionId == 9 {
                    
                    destnationViewcontroller.DeletesectionId = DeletesectionId
                    destnationViewcontroller.deletMsg = deletMsg
                    destnationViewcontroller.deleteId = deleteId
                    destnationViewcontroller.LanguagesData = LanguagesData
                    
                }else if DeletesectionId == 8 {
                    destnationViewcontroller.DeletesectionId = DeletesectionId
                    destnationViewcontroller.deletMsg = deletMsg
                    destnationViewcontroller.deleteId = deleteId
                    destnationViewcontroller.certificatesData = certificatesData
                    
                    
                }else if DeletesectionId == 4 {
                    
                    destnationViewcontroller.DeletesectionId = DeletesectionId
                    destnationViewcontroller.deletMsg = deletMsg
                    destnationViewcontroller.deleteId = deleteId
                    destnationViewcontroller.eduactionData = eduactionData
                    
                }else if DeletesectionId == 5 {
                    
                    destnationViewcontroller.DeletesectionId = DeletesectionId
                    destnationViewcontroller.deletMsg = deletMsg
                    destnationViewcontroller.deleteId = deleteId
                    destnationViewcontroller.experiencesData = experiencesData
                }else if DeletesectionId == 6 {
                    
                    destnationViewcontroller.DeletesectionId = DeletesectionId
                    destnationViewcontroller.deletMsg = deletMsg
                    destnationViewcontroller.deleteId = deleteId
                    destnationViewcontroller.selectedSoftSkillsIds = selectedSoftSkillsIds
                }else if DeletesectionId == 7 {
                    
                    destnationViewcontroller.DeletesectionId = DeletesectionId
                    destnationViewcontroller.deletMsg = deletMsg
                    destnationViewcontroller.deleteId = deleteId
                    destnationViewcontroller.selectedTechSkillsIds = selectedTechSkillsIds
                }
            }
            
        }else if segue.identifier == "addexp" {
            if let destnationViewcontroller = segue.destination as? AddEditExpVCtrl {
                destnationViewcontroller.selectedexperiences = self.experiencesData
                destnationViewcontroller.selectedexpId = self.selectedexpId
                destnationViewcontroller.delegate = self
                self.selectedexpId = -1
            }
        }else  if segue.identifier == "CongratulationsViewController" {
            if let destinationVC = segue.destination as? CongratulationsViewController {
                
                destinationVC.from = "profile"
                if let msg = UserManager.sharedInstance.NotSubscribedMeassage{
                    print("msg : \(msg)")
                    destinationVC.titleMsg = msg
                }
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        setProfileData()
    }
    
    private func updateMenus() {
        let settings = makeSettings()
        SideMenuManager.default.leftMenuNavigationController?.settings = settings
        SideMenuManager.default.rightMenuNavigationController?.settings = settings
    }
    private func makeSettings() -> SideMenuSettings {
        var settings = SideMenuSettings()
        
        settings.menuWidth = 240
        settings.statusBarEndAlpha = 0
        return settings
    }
    private func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        //SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        if L102Language.isRTL { SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
        }
        else{ SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
            
        }
    }
    
    @IBAction func changeProfileImg(_ sender: Any) {
        takePhotoCamera()
    }
    func takePhotoCamera() {
        let actionSheetController: UIAlertController = UIAlertController(title: "Profile Image".localized(), message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel2".localized(), style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
        let DeleteImgAction: UIAlertAction = UIAlertAction(title: "Delete Image".localized(), style: .destructive) { action -> Void in
            //Just dismiss the action sheet
            self.userInfoPresenter.deleteUserProfileImg()
        }
        actionSheetController.addAction(DeleteImgAction)
        }
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture".localized(), style: .default) { action -> Void in
            let cameraViewController = CameraViewController(croppingParameters: self.croppingParameters, allowsLibraryAccess: self.libraryEnabled) { [weak self] image, asset in
                self?.userImage.image = image
                self?.selectedProfileImage = image
                if let img = image {
                    let base64Image:String? = (img.resizedImageWithinRect(rectSize: CGSize(width: 200, height: 200)).toBase64())
                    
                    self?.presenter.doEditProfileImage(image: base64Image)
                }
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll".localized(), style: .default) { action -> Void in
            let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: self.croppingParameters) { [weak self] image, asset in
                self?.userImage.image = image
                self?.selectedProfileImage = image
                //  self?.deleteButton.isHidden = false
                if let img = image {
                    let base64Image:String? = (img.resizedImageWithinRect(rectSize: CGSize(width: 200, height: 200)).toBase64())
                    
                    self?.presenter.doEditProfileImage(image: base64Image)
                }
                
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(libraryViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        
        actionSheetController.popoverPresentationController?.sourceView = self.view;
        actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 5 - 50,width: UIScreen.main.bounds.width ,height: 1.0);
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func setupProfile(){
        if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
            if let profileImgUrl = URL(string: profileImg ){
                self.userImage.kf.setImage(with: profileImgUrl, placeholder: UIImage(named: "profile_img"))
                //self.userImage.isHidden = true
            }
        }
        else {
            if let gender = UserManager.sharedInstance.getUser()?.gender {
                if gender == "female" {
                    self.userImage.image = UIImage(named: "woman")
                }
                else{
                    self.userImage.image = UIImage(named: "profile_img")
                }
            }
            else{
                self.userImage.image = UIImage(named: "profile_img")
            }
        }
        if let name = UserManager.sharedInstance.getUser()?.name {
            userName.text = name
        }
        
    }
    
    
    @IBAction func DismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView.tag {
        case 4:
            if self.eduactionData.count != 0 {
                return eduactionData.count
            }else {
                return 0
            }
        case 5:
            if self.experiencesData.count != 0 {
                return experiencesData.count
            }else {
                return 0
            }
        case 6:
            if self.softSkillData.count != 0 {
                return softSkillData.count
            }else {
                return 0
            }
            
        case 7:
            if self.TechSkillData.count != 0 {
                return TechSkillData.count
            }else {
                return 0
            }
        case 8:
            if self.certificatesData.count != 0 {
                return certificatesData.count
            }else {
                return 0
            }
        case 9:
            if self.LanguagesData.count != 0 {
                return LanguagesData.count
            }else {
                return 0
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("heightForRowAtIndexPath func ")
        switch tableView.tag {
        case 4:
            return CGFloat(self.eduactionData[indexPath.row].cellHeight!)
        case 5:
            
            return CGFloat(self.experiencesData[indexPath.row].cellHeight!)
        case 6:
            return 75
            
        case 7:
            return 75
        case 8:
            return CGFloat(self.certificatesData[indexPath.row].cellHeight!)
        case 9:
            return 52
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView.tag {
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "eduInfoCell", for: indexPath) as! educationINfoTableViewCell
            
            let item = self.eduactionData[indexPath.row]
            if item.InstitutionID == -1 {
                print("-1 InstitutionID : \(item.Institution_Name ?? "")")
                cell.title.text = item.Institution_Name
                
                cell.titleHeight.constant = item.Institution_Name!.height(constraintedWidth: cell.title.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                
            }else{
                print("else InstitutionID ")
                let t = L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn
                print("else InstitutionID :\(t ?? "")")
                
                print("item.Institution_Name :\(item.Institution_Name ?? "")")
                if let xx = t {
                    print("tt")
                    cell.title.text = xx
                    cell.titleHeight.constant = xx.height(constraintedWidth: cell.title.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                }else{
                    print("Institution_Name")
                    cell.title.text = item.Institution_Name
                    if let intsName = item.Institution_Name{
                        cell.titleHeight.constant = intsName.height(constraintedWidth: cell.title.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                    }
                    
                }
                
            }
            let degree = L102Language.isRTL ? item.EduInfoDegree?.certificateAr : item.EduInfoDegree?.certificateEn
            let major = item.majorName
            
            var t2 = (degree ?? "") + " , " + (major ?? "")
            let grades = item.grades ?? ""
            if grades.count > 0 {
                t2 = "\(t2) \n\("Grades".localized()): \(grades)"
            }
            cell.describtion.text = t2
            
            cell.descriptionHeight.constant = t2.height(constraintedWidth: cell.describtion.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            print("cell.descriptionHeight.constant : \(cell.descriptionHeight.constant)")
            var f  = ""
            var t = ""
            if let from = item.fromDate {
                f = Int(from)!.getCurrentDateString()
                //                f = Int(from)!
                cell.year.text = f.components(separatedBy: "/")[2] + " - " + "Current".localized()
            }
            if let to = item.toDate {
                t = Int(to)!.getCurrentDateString()
                cell.year.text =  f.components(separatedBy: "/")[2]  + " - " + t.components(separatedBy: "/")[2]
            }
            cell.country.text =  L102Language.isRTL ? item.EduInfoCountry?.nameAr : item.EduInfoCountry?.nameEn
            cell.editBtn.tag = item.id!
            cell.editBtn.addTarget(self, action: #selector(editedu(_:)), for: .touchUpInside)
            
            cell.deleteBtn.tag = item.id!
            cell.deleteBtn.addTarget(self, action: #selector(removeedu(_:)), for: .touchUpInside)
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceCell", for: indexPath) as! expTableViewCell
            let item = self.experiencesData[indexPath.row]
            cell.jobTitleTxt.text = item.jobTitle
            
            print("cell.jobTitleTxt.height : \(cell.jobTitleTxt.height) ,, item.jobtitleHeight:\(item.jobTitle.height(constraintedWidth: cell.jobTitleTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!))")
            
            cell.jobTitleHeight.constant = item.jobTitle.height(constraintedWidth: cell.jobTitleTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            
            cell.companyNameTxt.text = item.companyName
            cell.companyNameHeight.constant = item.companyName.height(constraintedWidth: cell.companyNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            
            print("cell.companyNameHeight.constant : \(cell.companyNameHeight.constant)")
            
            cell.jobTypeTxt.text = item.jobtype?.name
            if let x = item.jobtype?.name {
                cell.jobTypeHeight.constant = x.height(constraintedWidth: cell.jobTypeTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!) + 5
            }
            cell.workSectorTxt.text = item.worksector?.name
            if let x  =  item.worksector?.name {
                cell.workSectorHeight.constant = x.height(constraintedWidth: cell.workSectorTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
            }
            cell.countryTxt.text =  L102Language.isRTL ? item.country?.nameAr : item.country?.nameEn
            if let x  = item.country?.nameAr {
                cell.countryHeight.constant = x.height(constraintedWidth: cell.countryTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!) + 5
            }
            var year:String = ""
            var month:String = ""
            
            if (item.yearsOfExperience != nil) {
                year = String(item.yearsOfExperience!) + "Years".localized()
            }
            if (item.month_of_experience != nil) {
                month =  String(item.month_of_experience!) + "Months".localized()
            }
            cell.dateTxt.text =  "( " + year + " " + month + " ) "
            cell.dateHeight.constant = 30
            
            print("item.noticePeriod : \(item.noticePeriod)")
            if let x = item.noticePeriod {
                print("if x: \(x) ,, count: \(x.count)")
                cell.noticePeriodTxt.text = x
               
                cell.periodHeight.constant = x.height(constraintedWidth: cell.noticePeriodTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!) 
                        
                print("cell.noticePeriodTxt.width : \(cell.noticePeriodTxt.width)")
                if x.count == 0 {
                    cell.noticePeriodTxt.isHidden = true
                }else{
                    cell.noticePeriodTxt.isHidden = false
                }
                
            }else{
                print("else noticePeriod")
                cell.noticePeriodTxt.isHidden = true
                cell.periodHeight.constant = 0
            }
            print("cell.periodHeight.constant : \(cell.periodHeight.constant)")
            cell.editBtn.tag = indexPath.row
            cell.editBtn.addTarget(self, action: #selector(editExp(_:)), for: .touchUpInside)
            cell.removeBtn.tag = indexPath.row
            cell.removeBtn.addTarget(self, action: #selector(removeExp(_:)), for: .touchUpInside)
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "skillCell", for: indexPath) as! skillTableViewCell
            let item = self.softSkillData[indexPath.row]
            
            selectedSoftSkillsIds.removeAll()
            for item in self.softSkillData {
                if item.skillTyp == 1 {
                selectedSoftSkillsIds.append((item.softSkill?.id!)!)
                }
            }
            if item.skillTyp == 1 {
                cell.skillTitle.text =  L102Language.isRTL ? item.softSkill?.nameAr : item.softSkill?.nameEn
            }
            else{
                cell.skillTitle.text =  item.skillText
            }
            cell.removeSkillBtn.tag = item.id!
            cell.removeSkillBtn.addTarget(self, action: #selector(removeSoftSkill(_:)), for: .touchUpInside)
            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "techSkillCell", for: indexPath) as! skillTableViewCell
            print("TechSkillData : \(TechSkillData)")
            let item = self.TechSkillData[indexPath.row]
            selectedTechSkillsIds.removeAll()
            for item in self.TechSkillData {
                selectedTechSkillsIds.append(item.id!)
            }
            print("item : \(item)")
            cell.skillTitle.text = L102Language.isRTL ? item.nameAr : item.nameEn
            
            cell.removeSkillBtn.tag = item.id!
            cell.removeSkillBtn.addTarget(self, action: #selector(removeTechSkill(_:)), for: .touchUpInside)
            
            return cell
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "certificateCell", for: indexPath) as! certificateTableviewCell
            print("certificatesData : \(certificatesData)")
            let item = self.certificatesData[indexPath.row]
            
            print("item : \(item)")
            
            cell.certificateName.text = item.certificateName
            
            cell.certificateNameHeight.constant = item.certificateName!.height(constraintedWidth: cell.certificateName.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)+5
                        
            if let end = item.CertificateEndDate {
                if let dateOfIssue = item.DateOfIssue {
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd/MM/yyyy"
                dateFormatterGet.timeZone = TimeZone.current
                let estimatedDate = dateFormatterGet.date(from: Int(item.CertificateEndDate ?? "0")!.getCurrentDateString())
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM yyyy"
                let estimatedDateString = dateFormatter.string(from: estimatedDate!)
                print("estimatedDateString : \(estimatedDateString)")
                let dateFormatterGet2 = DateFormatter()
                dateFormatterGet2.dateFormat = "dd/MM/yyyy"
                dateFormatterGet2.timeZone = TimeZone.current
                if let estimatedDate2 = dateFormatterGet2.date(from: Int(dateOfIssue)!.getCurrentDateString()) {
                let dateFormatter2 = DateFormatter()
                dateFormatter2.dateFormat = "MMM yyyy"
                let estimatedDateString2 = dateFormatter2.string(from: estimatedDate2)
                print("estimatedDateString : \(estimatedDateString2)")
                cell.certificateDate.text = estimatedDateString2 + " - " + estimatedDateString
                }
                }
                else{
                    cell.certificateDate.text = ""
                }
            }
            else{
                if let dateOfIssue = item.DateOfIssue {
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd/MM/yyyy"
                dateFormatterGet.timeZone = TimeZone.current
                if let estimatedDate = dateFormatterGet.date(from: Int(dateOfIssue)!.getCurrentDateString()) {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM yyyy"
                let estimatedDateString = dateFormatter.string(from: estimatedDate)
                print("estimatedDateString : \(estimatedDateString)")
                cell.certificateDate.text = estimatedDateString
                }
                }
                else{
                    cell.certificateDate.text = ""
                }
            }
            if item.Institution != nil {
                if let instit = item.Institution {
                    if L102Language.isRTL {
                        cell.certificateDate.text = "\(cell.certificateDate.text ?? "")\n\(instit.nameAr ?? "")"
                    }
                    else{
                        cell.certificateDate.text = "\(cell.certificateDate.text ?? "")\n\(instit.nameEn ?? "")"
                    }
                }
            }
            else if let institID = item.InstitutionID, institID <= 0 {
                cell.certificateDate.text = "\(cell.certificateDate.text ?? "")\n\(item.InstitutionText ?? "")"
            }
            cell.editCertificate.tag = item.id!
            cell.editCertificate.addTarget(self, action: #selector(editcertificate(_:)), for: .touchUpInside)
            
            cell.removeCertificate.tag = item.id!
            cell.removeCertificate.addTarget(self, action: #selector(removecertificate(_:)), for: .touchUpInside)
            
            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath) as! educationINfoTableViewCell
            print("LanguagesData : \(LanguagesData)")
            if LanguagesData.count > 0 {
                let item = self.LanguagesData[indexPath.row]
                print("item : \(item)")
                let lang = L102Language.isRTL ? item.language?.nameAr : item.language?.nameEn
                let prof = L102Language.isRTL ? item.languageStrength?.nameAr : item.languageStrength?.nameEn
                cell.title.text = lang! + " : " + (prof ?? "")
                cell.deleteBtn.tag = item.id!
                cell.deleteBtn.addTarget(self, action: #selector(removelanguage(_:)), for: .touchUpInside)
                cell.editBtn.tag = item.id!
                cell.editBtn.addTarget(self, action: #selector(editlanguage(_:)), for: .touchUpInside)
            }
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "eduInfoCell", for: indexPath) as! educationINfoTableViewCell
            
            let item = self.educationInfoData[indexPath.row]
            cell.title.text = item.eduTitle
            return cell
        }
    }
    
    @objc func removelanguage(_ btn: UIButton){
        print("removelanguage")
        if self.LanguagesData.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        else {
            DeletesectionId = 9
            deletMsg = "Are you sure you want to delete language?".localized()
            deleteId = btn.tag
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "deleteSegue", sender: self)
            }
        }
    }
    
    @objc func editExp(_ btn: UIButton){
        print("edit exp")
        self.selectedexpId = btn.tag
        
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "addexp", sender: self)
        }
        
    }
    
    
    @IBAction func addcertificateAction(_ sender: Any) {
        //        addCertificate
        if certificatesData.count >= 8 {
            self.showErrorMessage(message: "You reached the max number of allowed certificate items".localized())
        }else{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "addCertificate", sender: self)
            }
        }
        
    }
    
    
    @IBAction func addLanguage(_ sender: Any) {
        print("addLanguage")
        if LanguagesData.count >= 10 {
            self.showErrorMessage(message: "You reached the max number of allowed language items".localized())
        }else{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "addLanguage", sender: self)
            }
        }
    }
    
    @objc func editlanguage(_ btn: UIButton){
        print("editlanguage")
        
        self.selectedLangId = btn.tag
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "addLanguage", sender: self)
        }
    }
    @objc func removeTechSkill(_ btn: UIButton){
        print("remove tech")
        if self.TechSkillData.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        else {
            DeletesectionId = 7
            deletMsg = "Are you sure you want to delete technical skill?".localized()
            deleteId = btn.tag
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "deleteSegue", sender: self)
            }
        }
    }
    
    
    @objc func editcertificate(_ btn: UIButton){
        print("editcertificate")
        
        self.selectedCertificateId = btn.tag
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "addCertificate", sender: self)
        }
    }
    
    
    @objc func removeExp(_ btn: UIButton){
        print("removeExp")
        if self.experiencesData.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        else{
            DeletesectionId = 5
            deletMsg = "Are you sure you want to delete work experience?".localized()
            deleteId = btn.tag
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "deleteSegue", sender: self)
            }
        }
    }
    
    
    @objc func removecertificate(_ btn: UIButton){
        print("removecertificate")
        if self.certificatesData.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        else{
            DeletesectionId = 8
            deletMsg = "Are you sure you want to delete certificate?".localized()
            deleteId = btn.tag
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "deleteSegue", sender: self)
            }
        }
    }
    
    @objc func editedu(_ btn: UIButton){
        print("editedu")
        
        self.selectededuId = btn.tag
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "eduSegue", sender: self)
        }
    }
    
    @objc func removeedu(_ btn: UIButton){
        print("removedu")
        if self.eduactionData.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        else{
            DeletesectionId = 4
            deletMsg = "Are you sure you want to delete education information?".localized()
            deleteId = btn.tag
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "deleteSegue", sender: self)
            }
        }
    }
    @objc func removeSoftSkill(_ btn: UIButton){
        
        print("remove soft :\(btn.tag) ,, selectedSoftSkillsIds : \(selectedSoftSkillsIds)")
        
        if self.softSkillData.count == 1 {
            self.showErrorMessage(message: NSLocalizedString("You can't delete this item", comment: ""))
        }
        else {
        DeletesectionId = 6
        deletMsg = "Are you sure you want to delete soft skill?".localized()
        deleteId = btn.tag
        
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "deleteSegue", sender: self)
        }
    }
        
    }
    
}
extension NewProfileVCtrl:UserInfoView{
    func showLoadingUserInfoIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingUserInfoIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucessUserInfo() {
        //       setupProfile()
        self.profileSCrollView.dg_stopLoading()
        self.view.isUserInteractionEnabled = true
        setProfileData()
    }
    
    func onErrorUserInfo(message: String) {
        self.profileSCrollView.dg_stopLoading()
        self.showErrorMessage(message: message)
    } 
}
extension NewProfileVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess() {
        //       setupProfile()
        setProfileData()
        
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}

extension NewProfileVCtrl :DegreeListView {
    func showLoading() {
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        if let preferedContact = degreeWorkingTimes.PreferedContact {
            if preferedContact.count > 0 {
                
                if let contactId = UserManager.sharedInstance.getUser()?.PreferedContactID {
                    
                    
                    for item in preferedContact {
                        if contactId == item.id {
                            self.preferredContactOption.text = "Preferred contact option".localized() + " : " + ((L102Language.isRTL ? item.nameAr : item.nameEn)!)
                        }
                    }
                }
                if let disiblityType = UserManager.sharedInstance.getUser()?.DisabilityType {
                    if let disiblityStatus = UserManager.sharedInstance.getUser()?.Disability {
                        if disiblityStatus == 1 {
                            self.disibilityTopConstrain.constant = 7.5
                            self.disabilityHeightConstrain.constant = 30
                            if let items = degreeWorkingTimes.DisabilityType {
                                UserManager.sharedInstance.disabilityyItems = items
                                if UserManager.sharedInstance.disabilityyItems.count > 0 {
                                    let disabilities = disiblityType.split(separator: ",")
                                    for i in 0..<disabilities.count {
                                        if let selected = items.first(where: {$0.id == (Int(disabilities[i]) ?? -1)}) {
                                            if let selectedName = L102Language.isRTL ? selected.nameAr : selected.nameEn  {
                                                if i == 0 {
                                                    self.disibilityLbl.text = "\("Disability".localized()): \(selectedName)"
                                                }
                                                else{
                                                    self.disibilityLbl.text = "\(self.disibilityLbl.text ?? ""), \(selectedName)"
                                                }
                                            }
                                        }
                                    }
                                    let height = disibilityLbl.text!.height(constraintedWidth: fullNameTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 19.0)!)
                                    disabilityHeightConstrain.constant = height
                                    main_view_height.constant += height
                                }
                            }
                        }
                        else{
                            self.disibilityTopConstrain.constant = 0
                            self.disabilityHeightConstrain.constant = 0
                            self.disibilityLbl.text = ""
                        }
                    }
                }
                if let nationality = UserManager.sharedInstance.getUser()?.NationalityObject {
                    self.nationalityTxt.text = L102Language.isRTL ? nationality.nameAr : nationality.nameEn
                    self.SelectedNationality = (L102Language.isRTL ? nationality.nameAr : nationality.nameEn) ?? ""
                    nationalityHeight.constant = nationalityTxt.text!.height(constraintedWidth: nationalityTxt.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
                }
            }}
    }
}
extension NewProfileVCtrl: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scroll happened")
        
        if scrollView.contentOffset.y == 0 {
            // TOP
            accountSettingBorder.isHidden = false
            personalInfoBorder.isHidden = true
            contactInfoBorder.isHidden = true
            educationBorder.isHidden = true
            experienceBorder.isHidden = true
            skillsBorder.isHidden = true
            certificatesBorder.isHidden = true
            languageBorder.isHidden = true
            movableScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.floaty.removeFromSuperview()
        }else{
            self.view.addSubview(floaty)
        }
        
    }
}

extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font!], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
