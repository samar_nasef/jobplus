//
//  userExperienceVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/12/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField

protocol addExperienceDelegate : NSObjectProtocol {
    func experienceAdded(val: String)
}
class userExperienceVCtrl: UIViewController {

    weak var delegate: addExperienceDelegate?
    @IBOutlet weak var noOfYearsView: UIView!
    @IBOutlet weak var jobNameView: UIView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var userExperienceView: UIView!
    @IBOutlet weak var companyNameTxt: TweeAttributedTextField!
    @IBOutlet weak var jobNameTxtField: TweeAttributedTextField!
    @IBOutlet weak var noOfYearsTxtField: TweeAttributedTextField!
    @IBOutlet weak var DoneBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    var editeMode = false
    var companyNameToEdit = ""
    var jobTitleToEdit = ""
    var yearsOfExpToEdit = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        userExperienceView.insertHorizontalGradient()
        // Do any additional setup after loading the view.
        companyView.layer.cornerRadius = 2
        companyView.layer.shadowColor = UIColor.black.cgColor
        companyView.layer.shadowOpacity = 0.2
        companyView.layer.shadowOffset = .zero
        companyView.layer.shadowRadius = 10
        
        jobNameView.layer.cornerRadius = 2
        jobNameView.layer.shadowColor = UIColor.black.cgColor
        jobNameView.layer.shadowOpacity = 0.2
        jobNameView.layer.shadowOffset = .zero
        jobNameView.layer.shadowRadius = 10
        
        noOfYearsView.layer.cornerRadius = 2
        noOfYearsView.layer.shadowColor = UIColor.black.cgColor
        noOfYearsView.layer.shadowOpacity = 0.2
        noOfYearsView.layer.shadowOffset = .zero
        noOfYearsView.layer.shadowRadius = 10
        self.view.changeFont()
        setupTextFields()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view == self.view {
        self.dismiss(animated: true, completion: nil)
        }
    }
    func validateLoginTextFields() -> Bool {
    var valid = true
    if let companyName = self.companyNameTxt.text , companyName.isBlank {
            let errorMessage = "please enter your company name".localized()
            self.companyNameTxt.showInfo(errorMessage, animated: true)
            valid = false
       
    }else if let companyName = self.companyNameTxt.text , !companyName.isBlank{
        self.companyNameTxt.showInfo("", animated: true)
        }
    if let jobName = self.jobNameTxtField.text, jobName.isBlank {
        let errorMessage = "please enter your job title".localized()
        self.jobNameTxtField.showInfo(errorMessage, animated: true)
        valid = false
    }
    else if let jobName = self.jobNameTxtField.text, !jobName.isBlank {
        self.jobNameTxtField.showInfo("", animated: true)
    }
        if let noOfYears = self.noOfYearsTxtField.text, noOfYears.isBlank{
        let errorMessage = "please enter your years of experience".localized()
         valid = false
        self.noOfYearsTxtField.showInfo(errorMessage, animated: true)
        }
        else if let noOfYears = self.noOfYearsTxtField.text, !noOfYears.isBlank{
        self.noOfYearsTxtField.showInfo("", animated: true)
        }
        return valid
    }
    
    func setupTextFields(){
            companyNameTxt.delegate = self
            companyNameTxt.infoTextColor = UIColor(hexString: "#FF2600")
        companyNameTxt.returnKeyType = .next
            companyNameTxt.infoFontSize = 14.0
            companyNameTxt.activeLineColor = UIColor(hexString: "#00AFDF")
            companyNameTxt.activeLineWidth = 2
            companyNameTxt.animationDuration = 0.5
            companyNameTxt.minimumPlaceholderFontSize = 16
            companyNameTxt.originalPlaceholderFontSize = 16
            companyNameTxt.placeholderDuration = 0.2
        companyNameTxt.placeholderColor = .darkGray
        
            companyNameTxt.placeholder = "Company Name".localized()
        
        jobNameTxtField.delegate = self
        jobNameTxtField.returnKeyType = .next
        jobNameTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        jobNameTxtField.infoFontSize = 14.0
        jobNameTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        jobNameTxtField.activeLineWidth = 2
        jobNameTxtField.animationDuration = 0.5
        jobNameTxtField.minimumPlaceholderFontSize = 16
        jobNameTxtField.originalPlaceholderFontSize = 16
        jobNameTxtField.placeholderDuration = 0.2
        jobNameTxtField.placeholderColor = .darkGray
        jobNameTxtField.placeholder = "Job Title".localized()
        
        noOfYearsTxtField.delegate = self
         noOfYearsTxtField.returnKeyType = .done
        noOfYearsTxtField.keyboardType = .numberPad
        noOfYearsTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        noOfYearsTxtField.infoFontSize = 14.0
        noOfYearsTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        noOfYearsTxtField.activeLineWidth = 2
        noOfYearsTxtField.animationDuration = 0.5
        noOfYearsTxtField.minimumPlaceholderFontSize = 16
        noOfYearsTxtField.originalPlaceholderFontSize = 16
        noOfYearsTxtField.placeholderDuration = 0.2
        noOfYearsTxtField.placeholderColor = .darkGray
        noOfYearsTxtField.placeholder = "Years of experience".localized()
        if editeMode {
            companyNameTxt.text = companyNameToEdit
            jobNameTxtField.text = jobTitleToEdit
            noOfYearsTxtField.text = yearsOfExpToEdit
        }
    }
    @IBAction func DoneAction(_ sender: Any) {
//        if(validateLoginTextFields()) {
//            print("here")
//            let user_exp = Experience(jobTitle: self.jobNameTxtField.text ?? "", companyName: self.companyNameTxt.text ?? "", yearsOfExperience: Int(self.noOfYearsTxtField.text?.replacedArabicDigitsWithEnglish ?? "0") ?? 0)
//            UserManager.sharedInstance.experience.append(user_exp)
//            if self.delegate != nil {
//                self.delegate?.experienceAdded(val: "")
//            self.dismiss(animated: true, completion: nil)
//            }
////            self.dismiss(animated: true) {
////                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadExperience"), object: nil)
////            }
//        }
    }
    
    @IBAction func Cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension userExperienceVCtrl :UITextFieldDelegate {
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if(textField == companyNameTxt) {
        self.jobNameTxtField.becomeFirstResponder()
    }
    else if(textField == jobNameTxtField) {
        self.noOfYearsTxtField.becomeFirstResponder()
    }
    else if(textField == noOfYearsTxtField) {
        self.view.endEditing(true)
    }
    else {
        self.view.endEditing(true)
    }
    return true
}
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == companyNameTxt) {
            self.companyNameTxt.showInfo("", animated: true)
        }
        else if(textField == jobNameTxtField) {
            self.jobNameTxtField.showInfo("", animated: true)
        }
        else if(textField == noOfYearsTxtField) {
            self.noOfYearsTxtField.showInfo("", animated: true)
        }
        
    }
}
