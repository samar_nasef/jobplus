//
//  editContactInfoVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 09/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField

protocol SendDataDelegate {
    func sendData(text:String)
}


class editContactInfoVCtrl: UIViewController {
    
    var delegate: SendDataDelegate?
    
    @IBOutlet weak var firstNameBottomConstraint: NSLayoutConstraint!
    var birthDateTime:Int?
    var genderArray = ["Male".localized() , "Female".localized()]
    var gender: String?
    var lkpsNotLoaded = true
    let presenter = RegisterPresenter()
    let degreePresenter = DegreeListPresenter()
    var spinnerView: UIView?
    var disibilityItems = [DisabilityTypeItem]()
    var selecteedDisibility = [String]()
    var selectedDisibilityID = [Int]()
    var selectedDisibilityIndex: Int?
    var Disability = 0
    var SelectedDisability = ""
    var selectedNationality = [String]()
    var countryList = [countryItem]()
    var Natoinality = ""
    var selectedUserCountryId:Int?
    
    var selectedExperience = [String]()
    var experienceList = [experienceItem]()
    var Experience = ""
    var selectedUserExperienceId:Int?
    
    @IBOutlet weak var drivingLicenseState: UISwitch!
    @IBOutlet weak var genderTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var birthDateTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var jobTitleTxt: TweeAttributedTextField!
    @IBOutlet weak var nationality: TweeAttributedTextField!
    @IBOutlet weak var lastNameTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var DisabilitySwitch: UISwitch!
    @IBOutlet weak var nameTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var disablilityTypeLbl: UILabel!
    @IBOutlet weak var disabilityTypeView: UIView!
    @IBOutlet weak var disabilityTypeTxtField: TweeAttributedTextField!
    @IBOutlet weak var formHeight: NSLayoutConstraint!
    //@IBOutlet weak var experienceTypeTxtField: TweeAttributedTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        self.view.changeFont()
        getPersonalInfoData()
        presenter.attachView(view: self)
        degreePresenter.attachView(view: self)
        degreePresenter.doGetcertificatesNewList()
        if let disiblityType = UserManager.sharedInstance.getUser()?.DisabilityType {
            if let disiblityStatus = UserManager.sharedInstance.getUser()?.Disability {
                if disiblityStatus == 1 {
                    if UserManager.sharedInstance.disabilityyItems.count > 0 {
                        disibilityItems = UserManager.sharedInstance.disabilityyItems
                        let disabilities = disiblityType.split(separator: ",")
                        selecteedDisibility.removeAll()
                        for i in 0..<disabilities.count {
                            if disibilityItems[i].id == (Int(disabilities[i]) ?? -1) {
                                disibilityItems[i].selected = true
                                selecteedDisibility.append((L102Language.isRTL ? disibilityItems[i].nameAr : disibilityItems[i].nameEn) ?? "")
                            }
                            else{
                                disibilityItems[i].selected = false
                            }
                        }
                    }
                }
            }
        }
        
    }
    @IBAction func changeDisabilityState(_ sender: Any) {
        if self.DisabilitySwitch.isOn {
            Disability = 1
            SelectedDisability = ""
            disabilityTypeTxtField.text = ""
            selectedDisibilityID.removeAll()
            selecteedDisibility.removeAll()
            disabilityTypeView.isHidden = false
            disablilityTypeLbl.isHidden = false
            formHeight.constant = 970
            if lkpsNotLoaded {
                degreePresenter.doGetcertificatesNewList()
            }
        }else{
            Disability = 0
            disabilityTypeView.isHidden = true
            disablilityTypeLbl.isHidden = true
            disabilityTypeTxtField.text = ""
            SelectedDisability = ""
            selectedDisibilityID.removeAll()
            selecteedDisibility.removeAll()
            formHeight.constant = 870
        }
    }
    @IBAction func selectDisibilityAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let Names = L102Language.isRTL ? disibilityItems.map {$0.nameAr } : disibilityItems.map {$0.nameEn}
        if Names.count > 0 {
            showDisibilitySelectPopover(sender: sender, isUserCountry: true, Names: Names)
        }
        else{
            //            degreePresenter.doGetDegreeList()
        }
    }
    func showDisibilitySelectPopover(sender: UIView,isUserCountry:Bool, Names: [String?]) {
        self.disabilityTypeTxtField.showInfo("")
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .multiple, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        //selecteedDisibility.removeAll()
        // selected items
        selectionMenu.setSelectedItems(items: selecteedDisibility) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedDisibility = self!.disibilityItems.first(where: {$0.nameAr == text!!}) {
                    if selectedDisibility.id != nil{
                        self!.disibilityItems[index].selected = selected
                        self!.selectedDisibilityID.removeAll()
                        self!.selecteedDisibility.removeAll()
                        self!.disabilityTypeTxtField.text = ""
                        for item in self!.disibilityItems {
                            if (item.selected ?? false) {
                                self!.selectedDisibilityID.append(item.id ?? -1)
                                self!.selecteedDisibility.append(item.nameAr ?? "")
                                self!.disabilityTypeTxtField.text! += "\(item.nameAr ?? ""), "
                            }
                        }
                        self!.SelectedDisability =  self!.disabilityTypeTxtField.text!
                    }
                }
            }
            else{
                if let selectedDisibility  = self!.disibilityItems.first(where: {$0.nameEn == text!!}) {
                    if selectedDisibility.id != nil{
                        self!.disibilityItems[index].selected = selected
                        self!.selectedDisibilityID.removeAll()
                        self!.selecteedDisibility.removeAll()
                        self!.disabilityTypeTxtField.text = ""
                        for item in self!.disibilityItems {
                            if (item.selected ?? false) {
                                self!.selectedDisibilityID.append(item.id ?? -1)
                                self!.selecteedDisibility.append(item.nameEn ?? "")
                                self!.disabilityTypeTxtField.text! += "\(item.nameEn ?? ""), "
                            }
                        }
                        self!.SelectedDisability =  self!.disabilityTypeTxtField.text!
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return Names.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .checkbox
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    @IBAction func selectExperienceAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let Names = L102Language.isRTL ? experienceList.map {$0.nameAr } : experienceList.map {$0.nameEn}
        if Names.count > 0 {
            showExperienceSelectPopover(sender: sender, Names: Names)
        }
        else{
        }
    }
    func showExperienceSelectPopover(sender: UIView, Names: [String?]) {
        //self.experienceTypeTxtField.showInfo("")
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        selectionMenu.setSelectedItems(items: selectedExperience) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedExperience  = self!.experienceList.first(where: {$0.nameAr == text!!}) {
                    if let selectedExperienceID = selectedExperience.id{
                        self!.selectedUserExperienceId = selectedExperienceID
                        //self!.experienceTypeTxtField.text = text!!
                        self!.Experience = text!!
                    }
                    
                }
            }
            else{
                if let selectedExperience  = self!.experienceList.first(where: {$0.nameEn == text!!}) {
                    if let selectedExperienceID = selectedExperience.id{
                        self!.selectedUserExperienceId = selectedExperienceID
                        //self!.experienceTypeTxtField.text = text!!
                        self!.Experience = text!!
                    }
                    
                }
            }
        }
    
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    
    @IBAction func chooseUserCountryAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.resetCustomTextFieldsErrorMessages(self.nationality)
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: true, countriesNames: countriesNames)
        }
        else{
            degreePresenter.doGetcertificatesNewList()
        }
    }
    func showCountrySelectPopover(sender: UIView,isUserCountry:Bool, countriesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: countriesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        if isUserCountry {
            selectedCountryName.append(self.Natoinality)
        }
        selectionMenu.setSelectedItems(items: selectedCountryName) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCountry  = self!.countryList.first(where: {$0.nameAr == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.nationality.text = text!!
                            self!.Natoinality = text!!
                        }
                    }
                    
                }
            }
            else{
                if let selectedCountry  = self!.countryList.first(where: {$0.nameEn == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.nationality.text = text!!
                            self!.Natoinality = text!!
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return countriesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        getPersonalInfoData()
    }
    
    
    @IBAction func birthDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.show(inVC: self)
        
    }
    
    
    @IBAction func chooseGender(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let genderPicker = UIPickerView()
        let genderArrayToDisplay = ["Male".localized() , "Female".localized()]
        genderPicker.addCustomPicker(genderArrayToDisplay as NSArray , sender , "Gender", 0) { (selectedIndex) in
            
            print(self.genderArray[selectedIndex])
            self.genderTxtFeild.text = genderArrayToDisplay[selectedIndex]
            if selectedIndex == 0 {
                self.gender = "male"
            }else {
                self.gender = "female"
            }
        }
        
    }
    
    func getPersonalInfoData(){
        print("getPersonalInfoData")
        
        if let name = UserManager.sharedInstance.getUser()?.name {
            nameTxtFeild.text = name
            
        }
        
        if let name = UserManager.sharedInstance.getUser()?.LastName {
            print("last name from edit info \(name)")
            lastNameTxtFeild.text = name
            
        }
        
        if let nat = UserManager.sharedInstance.getUser()?.NationalityObject {
            nationality.text = L102Language.isRTL ? nat.nameAr : nat.nameEn
            selectedUserCountryId = nat.id
            selectedNationality.append((L102Language.isRTL ? nat.nameAr : nat.nameEn) ?? "")
        }
        
        if let nat = UserManager.sharedInstance.getUser()?.PreferedWork {
            jobTitleTxt.text = nat
            
        }
        if let experience = UserManager.sharedInstance.getUser()?.experienceid {
            self.selectedUserExperienceId = experience
            
        }
        if let driving = UserManager.sharedInstance.getUser()?.DrivingLicense {
            
            print("if driving from personal Info")
            var state:Bool
            
            if driving == 1 {
                state = true
            }else{
                state = false
            }
            drivingLicenseState.setOn(state, animated: true)
            
            
            
        }else{
            print("else driving from personal Info")
            drivingLicenseState.setOn(false, animated: true)
        }
        
        if let age = UserManager.sharedInstance.getUser()?.birthDate {
            birthDateTxtFeild.text = Int(age)!.getCurrentDateString()
            birthDateTime = Int(age)
        }
        
        
        
        if let gendure = UserManager.sharedInstance.getUser()?.gender {
            self.gender = gendure
            genderTxtFeild.text = gendure.capitalized.localized()
        }
        if let Disability = UserManager.sharedInstance.getUser()?.Disability {
            self.Disability = Disability
            if Disability == 1 {
                self.DisabilitySwitch.setOn(true, animated: true)
                //                if lkpsNotLoaded {
                //                degreePresenter.doGetcertificatesNewList()
                //                }
                disabilityTypeView.isHidden = false
                disablilityTypeLbl.isHidden = false
                disabilityTypeTxtField.text = self.SelectedDisability
                selecteedDisibility.removeAll()
                for item in disibilityItems {
                    if (item.selected ?? false) {
                        self.selecteedDisibility.append((L102Language.isRTL ? item.nameAr : item.nameEn) ?? "")
                    }
                }
                formHeight.constant = 970
            }
            else{
                self.DisabilitySwitch.setOn(false, animated: false)
                disabilityTypeView.isHidden = true
                disablilityTypeLbl.isHidden = true
                disabilityTypeTxtField.text = ""
                formHeight.constant = 870
            }
        }
        
    }
    
    
    
    
    @IBAction func closePopupAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveBtnAction(_ sender: Any) {
        
        if(validateLoginTextFields()) {
            print("valid form")
            var driving:Int
            if self.drivingLicenseState.isOn {
                driving = 1
            }else{
                driving = 0
            }
            
            let user = UserManager.sharedInstance.getUser()
            user?.name =  nameTxtFeild.text!
            user?.LastName = lastNameTxtFeild.text!
            user?.Nationality = String(self.selectedUserCountryId ?? 0)
            user?.PreferedWork = jobTitleTxt.text!
            user?.SectionID = 2
            user?.DrivingLicense = driving
            user?.birthDate2 = birthDateTime
            user?.gender = gender
            user?.Disability = Disability
            user?.experienceid = self.selectedUserExperienceId
            let selectedDisabilities = (selectedDisibilityID.map{String($0)}).joined(separator: ",")
            user?.DisabilityType = selectedDisabilities
            presenter.doEditNewProfile(data: user!)
        }
        
    }
    
    func validateLoginTextFields() -> Bool {
        var valid = true
        
        let nameTxt = self.nameTxtFeild.text ?? ""
        let trimedTxt = nameTxt.components(separatedBy: .whitespaces).joined()
        if let fullName = self.nameTxtFeild.text , !fullName.isBlank {
            if !fullName.isNameMoreThanTwoCharacters {
                self.nameTxtFeild.showInfo("Full name must be more than four characters".localized())
                valid = false
            }
            else if trimedTxt.hasSpecialCharacters(){
                self.nameTxtFeild.showInfo("invalid special".localized())
                valid = false
                
            }
            else {
            }
        }else {
            print("error fullNmae")
            self.nameTxtFeild.showInfo("enterFName".localized())
            valid = false
        }
        
        
        
        let lastNameTxt = self.lastNameTxtFeild.text ?? ""
        let trimedTxt2 = lastNameTxt.components(separatedBy: .whitespaces).joined()
        if let fullName = self.lastNameTxtFeild.text , !fullName.isBlank {
            if !fullName.isNameMoreThanTwoCharacters {
                self.lastNameTxtFeild.showInfo("Full name must be more than four characters".localized())
                valid = false
            }
            else if trimedTxt2.hasSpecialCharacters(){
                self.lastNameTxtFeild.showInfo("invalid special".localized())
                valid = false            }
            else {
                print("valid fullNmae")
            }
        }else {
            print("error fullNmae")
            self.lastNameTxtFeild.showInfo("enterlName".localized())
            valid = false
        }
        
        //        if let gender = self.genderTxtFeild.text , !gender.isBlank {
        //
        //               print("valid gender")
        //
        //               }else {
        //               print("error gender")
        //               self.genderTxtFeild.showInfo("please enter your gender".localized())
        //               valid = false
        //               }
        if let birthDate = self.birthDateTxtFeild.text , !birthDate.isBlank {
            
            print("valid birthDate")
            
        }else {
            print("error birthDate")
            self.birthDateTxtFeild.showInfo("please enter your bithday".localized())
            valid = false
        }
        if let nat = self.nationality.text , !nat.isBlank {
            print("valid nationality")
        }else {
            print("error nationality")
            self.nationality.showInfo("please enter your nationality".localized())
            valid = false
        }
//        if let exp = self.experienceTypeTxtField.text , !exp.isBlank {
//            print("valid experience")
//        }else {
//            print("error experience")
//            self.experienceTypeTxtField.showInfo("please enter your experience".localized())
//            valid = false
//        }
        if let jobTitle = self.jobTitleTxt.text , !jobTitle.isBlank {
            print("valid jobTitle")
        }else {
            print("error jobTitle")
            self.jobTitleTxt.showInfo("please enter your preferred work".localized())
            valid = false
        }
        if self.DisabilitySwitch.isOn {
            if self.SelectedDisability.count == 0 {
                self.disabilityTypeTxtField.showInfo("Please select disability type".localized())
                valid = false
            }
        }
        return valid
    }
    
    
    
    func setupTextFields(){
        
        
        
        
        nameTxtFeild.delegate = self
        nameTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        nameTxtFeild.returnKeyType = .next
        nameTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        nameTxtFeild.infoFontSize = 14.0
        nameTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        nameTxtFeild.activeLineWidth = 2
        nameTxtFeild.animationDuration = 0.5
        nameTxtFeild.minimumPlaceholderFontSize = 15
        nameTxtFeild.originalPlaceholderFontSize = 15
        nameTxtFeild.placeholderDuration = 0.2
        nameTxtFeild.placeholderColor = .darkGray
        nameTxtFeild.placeholder = "First Name".localized()
        
        
        lastNameTxtFeild.delegate = self
        lastNameTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        lastNameTxtFeild.returnKeyType = .next
        lastNameTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        lastNameTxtFeild.infoFontSize = 14.0
        lastNameTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        lastNameTxtFeild.activeLineWidth = 2
        lastNameTxtFeild.animationDuration = 0.5
        lastNameTxtFeild.minimumPlaceholderFontSize = 15
        lastNameTxtFeild.originalPlaceholderFontSize = 15
        lastNameTxtFeild.placeholderDuration = 0.2
        lastNameTxtFeild.placeholderColor = .darkGray
        lastNameTxtFeild.placeholder = "Last Name".localized()
        
        
        jobTitleTxt.delegate = self
        jobTitleTxt.textColor = UIColor(hexString: "#8c8c8c")
        jobTitleTxt.returnKeyType = .next
        jobTitleTxt.infoTextColor = UIColor(hexString: "#FF2600")
        jobTitleTxt.infoFontSize = 14.0
        jobTitleTxt.activeLineColor = UIColor(hexString: "#00AFDF")
        jobTitleTxt.activeLineWidth = 2
        jobTitleTxt.animationDuration = 0.5
        jobTitleTxt.minimumPlaceholderFontSize = 15
        jobTitleTxt.originalPlaceholderFontSize = 15
        jobTitleTxt.placeholderDuration = 0.2
        jobTitleTxt.placeholderColor = .darkGray
        jobTitleTxt.placeholder = "Preferred work field".localized()
        
        
        nationality.delegate = self
        nationality.textColor = UIColor(hexString: "#8c8c8c")
        nationality.returnKeyType = .next
        nationality.infoTextColor = UIColor(hexString: "#FF2600")
        nationality.infoFontSize = 14.0
        nationality.activeLineColor = UIColor(hexString: "#00AFDF")
        nationality.activeLineWidth = 2
        nationality.animationDuration = 0.5
        nationality.minimumPlaceholderFontSize = 15
        nationality.originalPlaceholderFontSize = 15
        nationality.placeholderDuration = 0.2
        nationality.placeholderColor = .darkGray
        nationality.placeholder = "Nationality".localized()
        
        
//        experienceTypeTxtField.delegate = self
//        experienceTypeTxtField.textColor = UIColor(hexString: "#8c8c8c")
//        experienceTypeTxtField.returnKeyType = .next
//        experienceTypeTxtField.infoTextColor = UIColor(hexString: "#FF2600")
//        experienceTypeTxtField.infoFontSize = 14.0
//        experienceTypeTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
//        experienceTypeTxtField.activeLineWidth = 2
//        experienceTypeTxtField.animationDuration = 0.5
//        experienceTypeTxtField.minimumPlaceholderFontSize = 15
//        experienceTypeTxtField.originalPlaceholderFontSize = 15
//        experienceTypeTxtField.placeholderDuration = 0.2
//        experienceTypeTxtField.placeholderColor = .darkGray
//        experienceTypeTxtField.placeholder = "Experience".localized()
        
        birthDateTxtFeild.delegate = self
        birthDateTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        birthDateTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        birthDateTxtFeild.infoFontSize = 14.0
        birthDateTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        birthDateTxtFeild.activeLineWidth = 2
        birthDateTxtFeild.animationDuration = 0.5
        birthDateTxtFeild.minimumPlaceholderFontSize = 15
        birthDateTxtFeild.originalPlaceholderFontSize = 15
        birthDateTxtFeild.placeholderDuration = 0.2
        birthDateTxtFeild.placeholderColor = .darkGray
        birthDateTxtFeild.placeholder = "Birth Date".localized()
        //
        //
        genderTxtFeild.delegate = self
        genderTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        genderTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        genderTxtFeild.infoFontSize = 14.0
        genderTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        genderTxtFeild.activeLineWidth = 2
        genderTxtFeild.animationDuration = 0.5
        genderTxtFeild.minimumPlaceholderFontSize = 15
        genderTxtFeild.originalPlaceholderFontSize = 15
        genderTxtFeild.placeholderDuration = 0.2
        genderTxtFeild.placeholderColor = .darkGray
        genderTxtFeild.placeholder = "Gendure".localized()
        
        disabilityTypeTxtField.delegate = self
        disabilityTypeTxtField.textColor = UIColor(hexString: "#8c8c8c")
        disabilityTypeTxtField.returnKeyType = .done
        disabilityTypeTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        disabilityTypeTxtField.infoFontSize = 14.0
        disabilityTypeTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        disabilityTypeTxtField.activeLineWidth = 2
        disabilityTypeTxtField.animationDuration = 0.5
        disabilityTypeTxtField.minimumPlaceholderFontSize = 15
        disabilityTypeTxtField.originalPlaceholderFontSize = 15
        disabilityTypeTxtField.placeholderDuration = 0.2
        disabilityTypeTxtField.placeholderColor = .darkGray
        disabilityTypeTxtField.placeholder = "Disability Type".localized()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension editContactInfoVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.nameTxtFeild) {
            self.view.endEditing(true)
        }
        //
        //        else if(textField == self.emailTxtField) {
        //            self.view.endEditing(true)
        //        }
        
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.nameTxtFeild {
            self.nameTxtFeild.showInfo("")
        }
        else if textField == self.lastNameTxtFeild {
            self.lastNameTxtFeild.showInfo("")
        }
        else if textField == self.nationality {
            self.nationality.showInfo("")
        }
        else if textField == self.jobTitleTxt {
            self.jobTitleTxt.showInfo("")
        }
        else if textField == self.disabilityTypeTxtField {
            self.disabilityTypeTxtField.showInfo("")
        }
        
    }
    //
}


extension editContactInfoVCtrl: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        birthDateTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
        birthDateTime = date.dateToTimeStamp()
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker() -> GMDatePicker {
        
        let datePicker = GMDatePicker()
        datePicker.delegate = self
        let calendar = Calendar.current
        //calendar.timeZone = TimeZone(identifier: "UTC")!
        var startingDate: Date? = Date()
        if let userBirthDate = birthDateTime{
            startingDate = Date(timeIntervalSince1970: Double(userBirthDate))
        }
        datePicker.config.startDate = startingDate ?? Date()
        let date_less16 = calendar.date(byAdding: .year, value: -17, to: Date())
        datePicker.config.maxDate =  calendar.date(byAdding: .day, value: -1, to: date_less16!)
        datePicker.config.animationDuration = 0.0
        
        datePicker.config.cancelButtonTitle = "CancelPicker".localized()
        datePicker.config.confirmButtonTitle = "Done".localized()
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor.frenchBlue
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
        return datePicker
    }
}


extension editContactInfoVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess() {
        if UserManager.sharedInstance.getUser()?.mobileVerified == false {
            self.performSegue(withIdentifier: "VerficationCodeViewController", sender: nil)
        }else {
            let user1  = UserManager.sharedInstance.getUser()
            user1?.name = nameTxtFeild.text
            user1?.LastName = lastNameTxtFeild.text
            user1?.birthDate = String(birthDateTime!)
            user1?.gender = gender
            user1?.Nationality = nationality.text
            user1?.PreferedWork = jobTitleTxt.text
            var driving:Int
            if self.drivingLicenseState.isOn {
                driving = 1
            }else{
                driving = 0
            }
            user1?.DrivingLicense = driving
            UserManager.sharedInstance.saveUser(user:user1!)
            self.delegate?.sendData(text: "text")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    
}

extension editContactInfoVCtrl : WWCalendarTimeSelectorProtocol {
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print ("tag = \(selector.view.tag)")
        print("Selected \n\(date)\n---")
        birthDateTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
        birthDateTime = Int(date.timeIntervalSince1970)
    }
    
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        
        //If you want date with 18 year less you can use Calendar unit year for that.
        
        let date_less16 = Calendar.current.date(byAdding: .year, value: -17, to: Date())
        //print("date_less16 = \(date_less16)")
        let dateMax = Calendar.current.date(byAdding: .day, value: -1, to: date_less16!)
        //print(dateMax)
        if date < dateMax! {
            //myDate is earlier than Now (date and time)
            return true
        } else {
            //myDate is equal or after than Now (date and time)
            return false
        }
    }
}
extension editContactInfoVCtrl :DegreeListView {
    func showLoading() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print(degreeWorkingTimes)
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print(degreeWorkingTimes)
        if var items = degreeWorkingTimes.DisabilityType {
            if items.count > 0 {
                lkpsNotLoaded = false
                UserManager.sharedInstance.disabilityyItems = items
                if let disiblityType = UserManager.sharedInstance.getUser()?.DisabilityType {
                    if items.count > 0 {
                        let disabilities = disiblityType.split(separator: ",")
                        selecteedDisibility.removeAll()
                        for i in 0..<disabilities.count {
                            if let selectedIndex = items.firstIndex(where: {$0.id == (Int(disabilities[i]) ?? -1)}) {
                                items[selectedIndex].selected = true
                                selecteedDisibility.append((L102Language.isRTL ? items[selectedIndex].nameAr : items[selectedIndex].nameEn) ?? "")
                                selectedDisibilityID.append(items[selectedIndex].id ?? 0)
                                disabilityTypeTxtField.text = (selecteedDisibility.map{String($0)}).joined(separator: ",")
                            }
                            else{
                                items[i].selected = false
                            }
                        }
                    }
                }
                self.disibilityItems = items
                UserManager.sharedInstance.disabilityyItems = items
            }
        }
        if let items = degreeWorkingTimes.countries {
            if items.count > 0 {
                lkpsNotLoaded = false
                self.countryList = items
            }
        }
        if let items = degreeWorkingTimes.experiences {
            if items.count > 0 {
                lkpsNotLoaded = false
                self.experienceList = items
                if let selectedUserExperience = items.firstIndex(where: {$0.id == self.selectedUserExperienceId}) {
                    let SelectedExpTxt = (L102Language.isRTL ? items[selectedUserExperience].nameAr : items[selectedUserExperience].nameEn) ?? ""
                    selectedExperience.append(SelectedExpTxt)
                    //self.experienceTypeTxtField.text = SelectedExpTxt
                }
            }
        }
    }
    
    
}
