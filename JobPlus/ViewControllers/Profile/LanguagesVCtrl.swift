//
//  LanguagesVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 21/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit

class LanguagesVCtrl: UIViewController {

    let presenter = DegreeListPresenter()
    var LanguageList = [PreferredContactItem]()
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.attachView(view: self)
        presenter.doGetcertificatesNewList()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LanguagesVCtrl :DegreeListView {
    func showLoading() {
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        if let preferedContact = degreeWorkingTimes.PreferedContact {
            if preferedContact.count > 0 {
                self.contactOtpionList.removeAll()
                self.contactOtpionList.append(contentsOf: preferedContact)
        print("self.contactOtpionList : \(contactOtpionList)")
    }}

    }
    
   
    
   
    
//    func onError(message: String) {
//        print("on error \(message)")
//        self.showErrorMessage(message: message)
//    }
    
    
}
