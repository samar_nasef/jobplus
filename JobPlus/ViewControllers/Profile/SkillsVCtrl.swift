//
//  SkillsVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 20/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit

class skillstableViewCell:UITableViewCell{
    
    @IBOutlet weak var skillTitle: UILabel!
}

protocol SendDataDelegate2 {
    func sendData2(text:String)
}



class SkillsVCtrl: UIViewController,UITableViewDelegate ,UITableViewDataSource{
    
    var delegate: SendDataDelegate2?
    let presenter = DegreeListPresenter()
    let presenter2 = RegisterPresenter()
    var fromView = ""
    var searching = false
    var searchSkill = [String]()
    var SkillsArrayToDisplay = [String]()
    var spinnerView: UIView?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var skillsTableView: UITableView!
    
    var softskillsList = [softSkillItem]()
    var techskillsList = [techSkillItem]()
    var selectedSkillsIds: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("fomView : \(fromView)")
        print("selectedSkillsIds from skills: \(selectedSkillsIds)")
        
        if fromView == "soft" {
            popupTitle.text = "Soft skills".localized()
        }else{
            popupTitle.text = "Technical skills".localized()
        }
        
        self.searchBar.placeholder = "Search here".localized()
//        if L102Language.currentAppleLanguage() == "ar" {
//            self.searchBar.placeholder = "Search here".localized()
//        }else {
//            self.searchBar.placeholder = "Search here".localized()
//        }
        
        presenter2.attachView(view: self)
        presenter.attachView(view: self)
        presenter.doGetcertificatesNewList()
        skillsTableView.tableFooterView = UIView()
        skillsTableView.backgroundColor = .clear
        
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveSkillText(_ sender: Any) {
        if ((searchBar.text ?? "").count == 0) {
            self.showErrorMessage(message: "Please enter skill name".localized())
            return
        }
        if let user = UserManager.sharedInstance.getUser() {
        let skill = SkillObjectToPost()
        skill.SoftSkills = [skillObj]()
            if let userSoftSkills = user.softSkills {
                for item in userSoftSkills {
                    if item.skillTyp == 1 {
                        skill.SoftSkills.append(skillObj(skill_id: item.softSkill?.id, skillTyp: item.skillTyp, skillText: nil))
                    }
                    else{
                        skill.SoftSkills.append(skillObj(skill_id: nil, skillTyp: item.skillTyp, skillText: item.skillText ?? nil))
                    }
                    
                }
            }
            
        
            
       
        if  skill.SoftSkills.count >= 10 {
            self.showErrorMessage(message: "You reached the max number of allowed soft skills items".localized())
        }else{
            skill.SoftSkills.append(skillObj(skill_id: nil, skillTyp: 3, skillText: searchBar.text ?? nil))
                let user = UserManager.sharedInstance.getUser()
                user?.SoftSkills =  skill
                user?.SectionID = 6
                presenter2.doEditNewProfile(data: user!)
            
        }
    }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchSkill.count
        }else{
            if fromView == "soft" {
                return softskillsList.count
            }else{
                return techskillsList.count
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if fromView == "soft"{
            var skillId:Int = -1
            var SkillName:String = ""
            if searching{
                print(" search selected soft :\(searchSkill[indexPath.row]) ,, row : \(indexPath.row)" )
                SkillName = searchSkill[indexPath.row]
                
                for item in softskillsList{
                    if L102Language.isRTL {
                        if item.nameAr == SkillName {
                            skillId = item.id!
                        }
                    }else{
                        if item.nameEn == SkillName {
                            skillId = item.id!
                        }
                    }
                }
                print("skilll id from serach : \(skillId)")
            }else{
                
                print("selected soft :\(softskillsList[indexPath.row])" )
                SkillName = softskillsList[indexPath.row].nameAr!
                skillId = softskillsList[indexPath.row].id!
            }
            if let user = UserManager.sharedInstance.getUser() {
            let skill = SkillObjectToPost()
            skill.SoftSkills = [skillObj]()
                if let userSoftSkills = user.softSkills {
                    for item in userSoftSkills {
                        if item.skillTyp == 1 {
                            skill.SoftSkills.append(skillObj(skill_id: item.softSkill?.id, skillTyp: item.skillTyp, skillText: nil))
                        }
                        else {
                            skill.SoftSkills.append(skillObj(skill_id: nil, skillTyp: item.skillTyp, skillText: item.skillText ?? nil))
                        }
                        
                    }
                }
                
            
                
           
            if  skill.SoftSkills.count >= 10 {
                self.showErrorMessage(message: "You reached the max number of allowed soft skills items".localized())
            }else{
                skill.SoftSkills.append(skillObj(skill_id: skillId, skillTyp: 1, skillText: nil))
                if selectedSkillsIds.contains(skillId) {
                    self.showErrorMessage(message: "Picking same soft skill before".localized())
                    self.dismiss(animated: true, completion: nil)
                }
                else{
                    let user = UserManager.sharedInstance.getUser()
                    user?.SoftSkills =  skill
                    user?.SectionID = 6
                    presenter2.doEditNewProfile(data: user!)
                }
            }
        }
        }else{
            var skillId:Int = -1
            var SkillName:String = ""
                       
            if searching{
                
                SkillName = searchSkill[indexPath.row]
                
                for item in techskillsList{
                    if L102Language.isRTL {
                        if item.nameAr == SkillName {
                            skillId = item.id!
                        }
                    }else{
                        if item.nameEn == SkillName {
                            skillId = item.id!
                        }
                    }
                }
                print("skilll id from serach : \(skillId)")
            }else{
                
                print("selected tech :\(techskillsList[indexPath.row])" )
                SkillName = techskillsList[indexPath.row].nameAr!
                skillId = techskillsList[indexPath.row].id!
            }
            
            let skill = SkillObjectToPost()
            skill.SoftSkills = [skillObj]()
            var arr  =  [skillObj]()
            for item in selectedSkillsIds {
                arr.append(skillObj(skill_id: item, skillTyp: 1, skillText: nil ) )
            }
            arr.append(skillObj(skill_id: skillId, skillTyp: 1, skillText: nil ) )
            skill.SoftSkills.append(contentsOf: arr)
            
            if  skill.SoftSkills.count > 8{
                self.showErrorMessage(message: "You reached the max number of allowed technical skills items".localized())
            }else{
                
                if selectedSkillsIds.contains(skillId) {
                    self.showErrorMessage(message: "Picking same technical skill before".localized())
                    self.dismiss(animated: true, completion: nil)
                }
                else{
                    let user = UserManager.sharedInstance.getUser()
                    user?.SoftSkills =  skill
                    user?.SectionID = 7
                    presenter2.doEditNewProfile(data: user!)
                }
            }
            
        }
        
        self.delegate?.sendData2(text: "text")
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "skillCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! skillstableViewCell
        cell.skillTitle.textAlignment = .center
        if searching{
            cell.skillTitle.text = searchSkill[indexPath.row]
        }else{
            if fromView == "soft" {
                SkillsArrayToDisplay = L102Language.isRTL ? softskillsList.map {($0.nameAr ?? "")} : softskillsList.map {($0.nameEn ?? "")}
                
                cell.skillTitle.text = SkillsArrayToDisplay[indexPath.row]
            }else{
                SkillsArrayToDisplay = L102Language.isRTL ? techskillsList.map {($0.nameAr ?? "")} : techskillsList.map {($0.nameEn ?? "")}
                
                cell.skillTitle.text = SkillsArrayToDisplay[indexPath.row]
            }
        }
        return cell
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SkillsVCtrl:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
        searchSkill = SkillsArrayToDisplay.filter({($0.uppercased()).contains(searchText.uppercased())
            
        })
        }
        else{
            searchSkill = SkillsArrayToDisplay.filter({$0.prefix(searchText.count) == searchText })
        }
        searching = true
        skillsTableView.reloadData()
    }
}

extension SkillsVCtrl :DegreeListView {
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    func showLoading() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        
        if let softSkills = degreeWorkingTimes.softSkills {
            if softSkills.count > 0 {
                self.softskillsList.removeAll()
                self.softskillsList.append(contentsOf: softSkills)
                skillsTableView.reloadData()
                print("self.softskillsList : \(softskillsList)")
            }}
        
        if let techSkills = degreeWorkingTimes.techSkills {
            if techSkills.count > 0 {
                self.techskillsList.removeAll()
                self.techskillsList.append(contentsOf: techSkills)
                skillsTableView.reloadData()
                print("self.techskillsList : \(techskillsList)")
            }}
    }
}
extension SkillsVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucess() {
        print("onSucess")
        let user1  = UserManager.sharedInstance.getUser()
        UserManager.sharedInstance.saveUser(user:user1!)
        print("UserManager.sharedInstance.getUser() :\(UserManager.sharedInstance.getUser()?.Nationality)")
        self.delegate?.sendData2(text: "skills")
        
        self.dismiss(animated: true, completion: nil)
        //        }
        
    }
}

