//
//  popupVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 09/02/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import UIKit

class popupTVCell:UITableViewCell{
    @IBOutlet weak var Title: UILabel!
}

protocol popupSendData {
    func sendData(text:String, id: Int)
}

class popupVCtrl: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: popupSendData?
    var searching = false
    var searchItems = [institutionItem]()
    var ItemsArrayToDisplay = [institutionItem]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var itemsTableView: UITableView!
    
    var itemsList = [institutionItem]()
    var selectedItemsIds: [Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ItemsArrayToDisplay.append(institutionItem(id: -1, nameEn: "Other", nameAr: "أخرى", Active: 1))
        itemsList = ItemsArrayToDisplay
        self.popupTitle.text = "University / Institution".localized()
        // Do any additional setup after loading the view.
        self.searchBar.placeholder = "Enter institution name".localized()
        itemsTableView.backgroundColor = .clear
        itemsTableView.reloadData()
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveItemText(_ sender: Any) {
        if ((searchBar.text ?? "").count == 0) {
            self.showErrorMessage(message: "Please enter institution name".localized())
            return
        }
        self.delegate?.sendData(text: (searchBar.text ?? ""), id: -1)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchItems.count
        }else{
            return itemsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if searching{
            if searchItems[indexPath.row].id == -1 {
                self.searchBar.text = nil
                self.searchBar.becomeFirstResponder()
            }
            else{
                self.delegate?.sendData(text: (L102Language.isRTL ? searchItems[indexPath.row].nameAr: searchItems[indexPath.row].nameEn) ?? "", id: searchItems[indexPath.row].id ?? 0)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            if itemsList[indexPath.row].id == -1 {
                self.searchBar.text = nil
                self.searchBar.becomeFirstResponder()
            }
            else{
                self.delegate?.sendData(text: (L102Language.isRTL ? itemsList[indexPath.row].nameAr  : itemsList[indexPath.row].nameEn) ?? "", id: itemsList[indexPath.row].id ?? 0)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "itemCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! popupTVCell
        cell.Title.textAlignment = .center
        if searching{
            cell.Title.text = L102Language.isRTL ? searchItems[indexPath.row].nameAr  : searchItems[indexPath.row].nameEn
        }else{
            
            cell.Title.text = L102Language.isRTL ? ItemsArrayToDisplay[indexPath.row].nameAr :  ItemsArrayToDisplay[indexPath.row].nameEn
            
        }
        return cell
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension popupVCtrl:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            searchItems = ItemsArrayToDisplay
            if L102Language.isRTL {
                searchItems = searchItems.filter({($0.nameAr?.uppercased() ?? "").contains(searchText.uppercased())
                    
                })
            }
            else{
                searchItems = searchItems.filter({($0.nameEn?.uppercased() ?? "").contains(searchText.uppercased())
                    
                })
            }
            if let other = searchItems.first(where: {$0.id == -1}) {
                print(other.nameEn ?? "")
            }
            else{
                searchItems.append(institutionItem(id: -1, nameEn: "Other", nameAr: "أخرى", Active: 1))
            }
            searching = true
            itemsTableView.reloadData()
        }
        else{
            searchItems = ItemsArrayToDisplay
            searching = true
            itemsTableView.reloadData()
        }
    }
}
