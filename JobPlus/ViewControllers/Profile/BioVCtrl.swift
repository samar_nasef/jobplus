//
//  BioVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 24/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
protocol AddBioDelegate : NSObjectProtocol {
    func BioAdded(text:String)
}


class BioVCtrl: UIViewController, UITextViewDelegate {
    weak var delegate: AddBioDelegate?
    //    var senddelegate: SendDataDelegate3?
    let presenter = RegisterPresenter()
    var spinnerView: UIView?
    let maxLenghth = 300
    var viewOnly = false
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var noOfWordsLbl: UILabel!
    @IBOutlet var saveBtnOutlet: UIView!
    @IBOutlet weak var error: UILabel!
    
    func textViewDidChange(_ textView: UITextView) {
        DispatchQueue.main.async {
            self.noOfWordsLbl.text = String(textView.text.count) + "/300 "
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.count + (text.count - range.length) <= self.maxLenghth
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        error.isHidden = true
        self.bioTextView.placeholder = "Write about yourself".localized()
        
        if let bio = UserManager.sharedInstance.getUser()?.userBio {
            bioTextView.text = bio
            DispatchQueue.main.async {
                self.noOfWordsLbl.text = String(bio.count) + "/300 "
            }
        }
                if viewOnly {
                    DispatchQueue.main.async {
                        self.bioTextView.isEditable = false
                        self.saveBtnOutlet.isHidden = true
                    }
                    
                }
        presenter.attachView(view: self)

        
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        
        if let bio = self.bioTextView.text , !bio.isBlank {
            error.isHidden = true
            print("valid bio")
            if self.bioTextView.text.count > 300{
                error.isHidden = false
                error.text =  "You reached the max number of characters".localized()
            }
//            else if self.bioTextView.text.count == 1 {
//                error.isHidden = false
//                error.text =  "Bio must be at least 2 characters".localized()
//            }
            else{
                error.isHidden = true
                let user = UserManager.sharedInstance.getUser()
                user?.userBio =  bioTextView.text!
                user?.SectionID = 1
                
                
                presenter.doEditNewProfile(data: user!)
            }
        }else {
            
            error.isHidden = false
            error.text =  "Please enter your Bio".localized()
//            error.isHidden = true
//            let user = UserManager.sharedInstance.getUser()
//            user?.userBio =  ""
//            user?.SectionID = 1
//
//
//            presenter.doEditNewProfile(data: user!)
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension BioVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess() {
        print("on sucess")
        let user1  = UserManager.sharedInstance.getUser()
        user1?.userBio = bioTextView.text
        UserManager.sharedInstance.saveUser(user:user1!)
        
        print(" self.delegate : \(self.delegate)")
        if self.delegate != nil {
            self.delegate?.BioAdded(text: "bio")
            self.dismiss(animated: true, completion: nil)
        }
        
        print("delegate2")
    }
    
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    
}

