//
//  CollapsibleTableViewHeader.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 5/30/16.
//  Copyright © 2016 Yong Su. All rights reserved.
//

import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int)
}

class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    var tapHeader = true
    let itemImg = UIImageView()
    let titleLabel = UILabel()
    let CountLabel = UILabel()
    let arrowImg = UIImageView()
    //let headerView = UIView()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        // Content View
        contentView.backgroundColor = .white
        let marginGuide = contentView.layoutMarginsGuide
        
        // Container view
        
//        contentView.addSubview(headerView)
//        headerView.translatesAutoresizingMaskIntoConstraints = false
//        headerView.backgroundColor = .lightGray
//        headerView.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor, constant: 10).isActive = true
//        headerView.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 4).isActive = true
//        headerView.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: 10).isActive = true
//        headerView.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor, constant: 4).isActive = true
        
        //let headerViewMarginGuide = headerView.layoutMarginsGuide
        // Arrow image
        contentView.addSubview(arrowImg)
        arrowImg.translatesAutoresizingMaskIntoConstraints = false
        arrowImg.widthAnchor.constraint(equalToConstant: 12).isActive = true
        arrowImg.heightAnchor.constraint(equalToConstant: 12).isActive = true
        arrowImg.topAnchor.constraint(equalTo: marginGuide.topAnchor,constant: 8).isActive = true
        arrowImg.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: 0).isActive = true
        //arrowImg.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        
        // Count label
        contentView.addSubview(CountLabel)
        CountLabel.textColor = UIColor.black
        //CountLabel.textAlignment = .center
        CountLabel.tag = 1
        CountLabel.translatesAutoresizingMaskIntoConstraints = false
        CountLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        CountLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        CountLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 3).isActive = true
        //CountLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        //CountLabel.trailingAnchor.constraint(equalTo: arrowImg.layoutMarginsGuide.leadingAnchor, constant: 8).isActive = true
        CountLabel.leadingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: -65).isActive = true
        
        //Item image
        contentView.addSubview(itemImg)
        itemImg.translatesAutoresizingMaskIntoConstraints = false
        itemImg.widthAnchor.constraint(equalToConstant: 23).isActive = true
        itemImg.heightAnchor.constraint(equalToConstant: 24).isActive = true
        itemImg.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 3).isActive = true
        itemImg.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        
        // Title label
        contentView.addSubview(titleLabel)
        titleLabel.textColor = UIColor.black
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 250).isActive = true
        titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: itemImg.layoutMarginsGuide.leadingAnchor, constant: 4).isActive = true
        //titleLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor, constant: 40).isActive = true
        titleLabel.textColor = .black
        CountLabel.textColor = .black
        //
        // Call tapHeader when tapping on this header
        //
        if tapHeader {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(_:))))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // Trigger toggle section when tapping on the header
    //
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        //
        // Animate the arrow rotation (see Extensions.swf)
        //
        if L102Language.isRTL {
            if collapsed {
                arrowImg.image = UIImage(named: "next-ar")
            }
            else{
                arrowImg.image = UIImage(named: "ic_down_arrow")
                arrowImg.contentMode = .scaleAspectFit
            }
        }
        else{
           //arrowImg.rotate(collapsed ? 0.0 : .pi / 2)
            if collapsed {
                arrowImg.image = UIImage(named: "next")
            }
            else{
                arrowImg.image = UIImage(named: "ic_down_arrow")
                arrowImg.contentMode = .scaleAspectFit
            }
        }
    }
    
}
