//
//  DeltePopUpVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 10/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit

class DeltePopUpVCtrl: UIViewController {
    
    var spinnerView: UIView?
    let presenter = RegisterPresenter()
    var delegate: SendDataDelegate?
    var DeletesectionId : Int  = 0
    var deletMsg : String = ""
    var deleteId : Int = 0
    var LanguagesData = [languageItemFromAPi]()
    var certificatesData = [certificateItemFromAPi]()
    var eduactionData = [educationItemFromAPi]()
    var experiencesData = [Experience]()
    var selectedSoftSkillsIds:[Int] = []
    var selectedTechSkillsIds:[Int] = []
    
    @IBOutlet weak var deleteMsgLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        deleteMsgLabel.text =  deletMsg
        
        
    }
    
    
    @IBAction func deleteAction(_ sender: Any) {
        
        if DeletesectionId == 9 {
            
            for (index, item)in LanguagesData.enumerated() {
                if item.id == deleteId {
                    print("item from remove : \(item)")
                    LanguagesData.remove(at: index)
                }
                
            }
            print("languages after remove one : \(LanguagesData)")
            //            languageTableView.reloadData()
            
            let user = UserManager.sharedInstance.getUser()
            let lang = languageObjectToPost()
            lang.userLanguages = [LanguageObj]()
            //        lang.userLanguages.append(contentsOf:[LanguageObj(language_id: LanguageId, languageStrength_id: profeciencyId)])
            
            var arr1  =  [LanguageObj]()
            for item in LanguagesData {
                
                arr1.append(LanguageObj(language_id: (item.language?.id)!, languageStrength_id: (item.languageStrength?.id)!))
            }
            
            lang.userLanguages.append(contentsOf: arr1)
            
            
            
            //        selectedlanguages.append(LanguageObj(language_id: LanguageId, languageStrength_id: profeciencyId))
            //        lang.userLanguages.append(contentsOf: selectedlanguages)
            
            user?.userLanguages2 =  lang
            user?.SectionID = 9
            
            presenter.doEditNewProfile(data: user!)
            
        }else if DeletesectionId == 8 {
            for (index, item)in certificatesData.enumerated() {
                if item.id == deleteId {
                    print("item from remove : \(item)")
                    certificatesData.remove(at: index)
                }
                
            }
            print("certificatesData after remove one : \(certificatesData)")
            //                cerificatesTableView.reloadData()
            
            
            
            let certificates = certificateObjectToPost()
            certificates.userCertificates = [CertificateObj]()
            
            var arr  =  [CertificateObj]()
            for item in certificatesData {
                var to_date: Int?
                if let to = item.CertificateEndDate {
                    to_date = Int(to)
                }
                arr.append(CertificateObj(certificateName: item.certificateName!, DateOfIssue: Int(item.DateOfIssue ?? "") ?? nil, CertificateEndDate: to_date ?? nil, HasIssueDate: item.HasIssueDate!, InstitutionID: item.InstitutionID,InstitutionText: item.InstitutionText ?? "") )
            }
            
            
            
            
            certificates.userCertificates.append(contentsOf: arr)
            
            let user = UserManager.sharedInstance.getUser()
            user?.userCertificates2 =  certificates
            user?.SectionID = 8
            presenter.doEditNewProfile(data: user!)
            
        }else if DeletesectionId == 4 {
            for (index, item)in eduactionData.enumerated() {
                if item.id == deleteId {
                    print("item from remove : \(item)")
                    eduactionData.remove(at: index)
                }
                
            }
            print("eduactionData after remove one : \(eduactionData)")
            //                cerificatesTableView.reloadData()
            
            
            
            let edus = eduactionalObjectToPost()
            edus.EducationalInformation = [educationalObj]()
            
            var arr2  =  [educationalObj]()
            for item in eduactionData {
                //                 arr.append(CertificateObj(certificateName: item.certificateName!, DateOfIssue: Int(item.DateOfIssue!)!, CertificateEndDate: Int(item.CertificateEndDate!)!, HasIssueDate: item.HasIssueDate!) )
                
                //                var name = L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn
                
                var name : String?
                if item.InstitutionID == -1 {
                    name = item.Institution_Name
                }else{
                    name = L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn
                }
                
                var to_date: Int?
                if let to = item.toDate {
                    to_date = Int(to)
                }
                
                //arr2.append(educationalObj(Institution_Name: name!, InstitutionID: item.InstitutionID!, country_id: item.country_id!, certificate_id: item.certificate_id!, majorName: item.majorName!,toDate: item.toDate!,fromDate: item.fromDate!))
                arr2.append(educationalObj(Institution_Name: name ?? "", InstitutionID: item.InstitutionID ?? 0, country_id: item.country_id ?? 0, certificate_id: item.certificate_id ?? 0, majorName: item.majorName ?? "", grades:item.grades ?? "" , toDate: to_date, fromDate: Int(item.fromDate ?? "0") ?? 0, specialitilty_id: item.specialitilty_id))
            }
            
            
            
            
            edus.EducationalInformation.append(contentsOf: arr2)
            
            let user = UserManager.sharedInstance.getUser()
            user?.EducationalInformation2 =  edus
            user?.SectionID = 4
            presenter.doEditNewProfile(data: user!)
        }else if DeletesectionId == 5 {
            //            for (index, item)in eduactionData.enumerated() {
            //                    if item.id == deleteId {
            //                        print("item from remove : \(item)")
            experiencesData.remove(at: deleteId)
            //                    }
            //
            //                }
            print("experiencesData after remove one : \(experiencesData)")
            //                cerificatesTableView.reloadData()
            
            
            let lang = ExperienceObjectToPost()
            lang.experiences = [Experience2]()
            var tmpArr  =  [Experience2]()
            for item in experiencesData {
                
                let exp : Experience2 = Experience2(jobTitle: item.jobTitle, companyName: item.companyName, yearsOfExperience: item.yearsOfExperience ?? 0, month_of_experience: item.month_of_experience ?? 0, noticePeriod: item.noticePeriod ?? "", cityName: item.cityName ?? "", CurrentlyWorking: item.CurrentlyWorking ?? 0, country_id: item.country?.id ?? 0, worksector_id:  item.worksector?.id ?? 0, jobtype_id: item.jobtype?.id ?? 0, city_id: item.city_id ?? 0, Duties: item.Duties ?? "", fromDate: item.fromDate ?? "0", toDate: item.toDate ?? "0", job_category_id: item.job_category_id)
                tmpArr.append(exp)
                //                tmpArr.append(Experience2(jobTitle: item.jobTitle!, companyName: item.companyName!, yearsOfExperience: item.yearsOfExperience!, month_of_experience: item.month_of_experience!, noticePeriod: item.noticePeriod!, cityName: item.cityName!, CurrentlyWorking: item.CurrentlyWorking!, country_id: item.country?.id!, worksector_id: item.worksector?.id!, jobtype_id: item.jobtype?.id!))
                
            }
            
            lang.experiences.append(contentsOf: tmpArr)
            
            //            var edus = eduactionalObjectToPost()
            //            edus.EducationalInformation = [educationalObj]()
            
            //             var arr2  =  [educationalObj]()
            for item in eduactionData {
                //                 arr.append(CertificateObj(certificateName: item.certificateName!, DateOfIssue: Int(item.DateOfIssue!)!, CertificateEndDate: Int(item.CertificateEndDate!)!, HasIssueDate: item.HasIssueDate!) )
                var name = L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn
                
                //                arr2.append(educationalObj(Institution_Name: name!, InstitutionID: item.InstitutionID!, country_id: item.country_id!, certificate_id: item.certificate_id!, majorName: item.majorName!,toDate: item.toDate!,fromDate: item.fromDate!))
            }
            
            
            
            
            //            edus.EducationalInformation.append(contentsOf: arr2)
            
            let user = UserManager.sharedInstance.getUser()
            user?.experiences2 =  lang
            user?.SectionID = 5
            presenter.doEditNewProfile(data: user!)
        }else if DeletesectionId == 6 {
            if let user = UserManager.sharedInstance.getUser() {
            let skill = SkillObjectToPost()
            skill.SoftSkills = [skillObj]()
                if let userSoftSkills = user.softSkills {
                    for item in userSoftSkills {
                        if item.skillTyp == 1 {
                            if item.id != deleteId {
                            skill.SoftSkills.append(skillObj(skill_id: item.softSkill?.id, skillTyp: item.skillTyp, skillText: nil))
                            }
                        }
                        else{
                            if item.id != deleteId {
                            skill.SoftSkills.append(skillObj(skill_id: nil, skillTyp: item.skillTyp, skillText: item.skillText ?? nil))
                            }
                        }
                        
                    }
                }
                let user = UserManager.sharedInstance.getUser()
                    user?.SoftSkills =  skill
                    user?.SectionID = 6
                presenter.doEditNewProfile(data: user!)
           
        }
        }else  if DeletesectionId == 7{
            //            for (index, item)in selectedTechSkillsIds.enumerated() {
            //                if item == deleteId{
            //                    print("item from remove : \(item)")
            //                    selectedTechSkillsIds.remove(at: index)
            //                }
            //
            //            }
            //
            ////            techSkillsTableView.reloadData()
            //            let skill = SkillObjectToPost()
            //             skill.SoftSkills = [skillObj]()
            //
            //
            //             var arr  =  [skillObj]()
            //             for item in selectedTechSkillsIds {
            //                 arr.append(skillObj(skill_id: item ) )
            //             }
            //
            //             skill.SoftSkills.append(contentsOf: arr)
            //
            //             let user = UserManager.sharedInstance.getUser()
            //             user?.SoftSkills =  skill
            //             user?.SectionID = 7
            //             presenter.doEditNewProfile(data: user!)
            //
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension DeltePopUpVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess() {
        //       setupProfile()
        self.delegate?.sendData(text: "delete")
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}
