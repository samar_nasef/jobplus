//
//  addEducationalInfoVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 04/05/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField
import TransitionButton

class addEducationalInfoVCtrl: UIViewController {
    
    @IBOutlet weak var dropDownIcon: UIImageView!
    @IBOutlet weak var instituteNameBtn: UIButton!
    var delegate: SendDataDelegate?
    let presenter = RegisterPresenter()
    let presenter2 = DegreeListPresenter()
    var toDate:Int?
    var fromDate:Int?
    var fromDateObj: Date?
    var toDateObj: Date?
    var countryList = [countryItem]()
    var selectedUserCountryId:Int?
    var selectedInstitutionCountryId:Int?
    var selectedCountryIndex: Int?
    var selectedCountry = [String]()
    var selectededuCountry = [String]()
    var specialities = [specialityItem]()
    var selectedSpeciality = [String]()
    var selectedSpecialityID: Int?
    var InstitutionsList = [institutionItem]()
    var certificatesList = [certificatesItem]()
    var selectededus = [educationItemFromAPi]()
    //let countryPresenter = CountryListPresenter()
    var degreeId : Int = 0
    var institutionId:Int=0
    var selectededuId : Int = 0
    var IndtituteIdForEdit : Int = 0
    var degreeIdForEdit : Int = 0
    var spinnerView: UIView?
    var RequireMajor = 0
    var RequireDate = 0
    var RequireGrades = 0
    
    @IBOutlet weak var ToTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var ToState: UISwitch!
    @IBOutlet weak var fromTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var majorTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var countryTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var educatioalIstTextFeild: TweeAttributedTextField!
    @IBOutlet weak var degreeTxtFeild: TweeAttributedTextField!
    
    @IBOutlet weak var generalMajorTxtField: TweeAttributedTextField!
    @IBOutlet weak var GradesLblHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var GradesBottomConstrain: NSLayoutConstraint!
    @IBOutlet weak var GradesTopConstain: NSLayoutConstraint!
    @IBOutlet weak var GradesTxtFieldHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var GradesLbl: UILabel!
    @IBOutlet weak var GradesTxtView: UIView!
    @IBOutlet weak var GradesTextField: TweeAttributedTextField!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var majorReqStar: UILabel!
    @IBOutlet weak var gradesReqStar: UILabel!
    @IBOutlet weak var fromReqStar: UILabel!
    @IBOutlet weak var toReqStar: UILabel!
    @IBOutlet weak var toDateStack: UIView!
    @IBOutlet weak var toLbl: UILabel!
    @IBOutlet weak var toBtnToBeDisabled: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        presenter.attachView(view: self)
        //countryPresenter.attachView(view: self)
        //countryPresenter.doGetCountryList()
        presenter2.attachView(view: self)
        presenter2.doGetcertificatesNewList()
        self.view.changeFont()
        DispatchQueue.main.async {
            self.GradesLbl.isHidden = true
            self.GradesTopConstain.constant = 0
            self.GradesBottomConstrain.constant = 0
            self.GradesLblHeightConstrain.constant = 0
            self.GradesTxtView.isHidden = true
            self.GradesTxtFieldHeightConstrain.constant = 0
            self.mainViewHeight.constant = 800
            self.GradesTextField.text = ""
        }
        if selectededuId != 0 {
            print("selectededuId from edit :  \(selectededuId)")
            
            for item in selectededus{
                if item.id == selectededuId{
                    if let speciality = item.specialitilty{
                        self.selectedSpeciality.append(L102Language.isRTL ? (speciality.nameAr ?? "") : (speciality.nameEn ?? ""))
                        self.selectedSpecialityID = speciality.id
                        self.generalMajorTxtField.text = self.selectedSpeciality[0]
                    }
                    self.majorTxtFeild.text = item.majorName
                    if let grades = item.grades {
                        self.GradesTextField.text = grades
                        self.RequireGrades = 0
                        if self.RequireGrades == 1 {
                            DispatchQueue.main.async {
                                self.GradesLbl.isHidden = false
                                self.GradesTopConstain.constant = 25
                                self.GradesBottomConstrain.constant = 5
                                self.GradesLblHeightConstrain.constant = 26
                                self.GradesTxtView.isHidden = false
                                self.GradesTxtFieldHeightConstrain.constant = 56
                                self.mainViewHeight.constant = 900
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self.GradesLbl.isHidden = true
                                self.GradesTopConstain.constant = 0
                                self.GradesBottomConstrain.constant = 0
                                self.GradesLblHeightConstrain.constant = 0
                                self.GradesTxtView.isHidden = true
                                self.GradesTxtFieldHeightConstrain.constant = 0
                                self.mainViewHeight.constant = 800
                                self.GradesTextField.text = ""
                            }
                        }
                    }
                    
                    if let fromDatee = item.fromDate {
                        self.fromDate = Int(fromDatee)!
                        self.fromDateObj = Date(timeIntervalSince1970: TimeInterval(Int(fromDatee)!))
                    }
                    
                    if let to = item.toDate {
                        self.toDate = Int(to)!
                        self.toDateObj = Date(timeIntervalSince1970: TimeInterval(Int(to)!))
                        self.ToTxtFeild.text = Int(to)!.getCurrentDateString()
                    }
                    else{
                        ToState.isOn = true
                        self.toLbl.isHidden = true
                        self.toReqStar.isHidden = true
                        self.toDateStack.isHidden = true
                        self.toBtnToBeDisabled.isUserInteractionEnabled = false
                        self.ToTxtFeild.isUserInteractionEnabled = false
                        self.ToTxtFeild.showInfo("")
                        self.ToTxtFeild.placeholder = ""
                        self.ToTxtFeild.text = ""
                    }
                    if let fromDatee = item.fromDate {
                        self.fromTxtFeild.text = Int(fromDatee)!.getCurrentDateString()
                    }
                    
                }
            }
        }
    }
    func setupTextFields(){
        degreeTxtFeild.delegate = self
        degreeTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        degreeTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        degreeTxtFeild.infoFontSize = 14.0
        degreeTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        degreeTxtFeild.activeLineWidth = 2
        degreeTxtFeild.animationDuration = 0.5
        degreeTxtFeild.minimumPlaceholderFontSize = 15
        degreeTxtFeild.originalPlaceholderFontSize = 15
        degreeTxtFeild.placeholderDuration = 0.2
        degreeTxtFeild.placeholderColor = .darkGray
        degreeTxtFeild.placeholder = "Degree".localized()
        
        generalMajorTxtField.delegate = self
        generalMajorTxtField.textColor = UIColor(hexString: "#8c8c8c")
        generalMajorTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        generalMajorTxtField.infoFontSize = 14.0
        generalMajorTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        generalMajorTxtField.activeLineWidth = 2
        generalMajorTxtField.animationDuration = 0.5
        generalMajorTxtField.minimumPlaceholderFontSize = 15
        generalMajorTxtField.originalPlaceholderFontSize = 15
        generalMajorTxtField.placeholderDuration = 0.2
        generalMajorTxtField.placeholderColor = .darkGray
        generalMajorTxtField.placeholder = "General Major".localized()
        
        educatioalIstTextFeild.delegate = self
        educatioalIstTextFeild.textColor = UIColor(hexString: "#8c8c8c")
        educatioalIstTextFeild.infoTextColor = UIColor(hexString: "#FF2600")
        educatioalIstTextFeild.infoFontSize = 13.0
        educatioalIstTextFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        educatioalIstTextFeild.activeLineWidth = 2
        educatioalIstTextFeild.animationDuration = 0.5
        educatioalIstTextFeild.minimumPlaceholderFontSize = 15
        educatioalIstTextFeild.originalPlaceholderFontSize = 15
        educatioalIstTextFeild.placeholderDuration = 0.2
        educatioalIstTextFeild.placeholderColor = .darkGray
        educatioalIstTextFeild.infoLabel.numberOfLines = 2
        educatioalIstTextFeild.placeholder = "University / Institution".localized()
        
        countryTxtFeild.delegate = self
        countryTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        countryTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        countryTxtFeild.infoFontSize = 14.0
        countryTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        countryTxtFeild.activeLineWidth = 2
        countryTxtFeild.animationDuration = 0.5
        countryTxtFeild.minimumPlaceholderFontSize = 15
        countryTxtFeild.originalPlaceholderFontSize = 15
        countryTxtFeild.placeholderDuration = 0.2
        countryTxtFeild.placeholderColor = .darkGray
        countryTxtFeild.infoLabel.numberOfLines = 2
        countryTxtFeild.placeholder = "Country".localized()
        
        majorTxtFeild.delegate = self
        majorTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        majorTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        majorTxtFeild.infoFontSize = 14.0
        majorTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        majorTxtFeild.activeLineWidth = 2
        majorTxtFeild.animationDuration = 0.5
        majorTxtFeild.minimumPlaceholderFontSize = 15
        majorTxtFeild.originalPlaceholderFontSize = 15
        majorTxtFeild.placeholderDuration = 0.2
        majorTxtFeild.placeholderColor = .darkGray
        majorTxtFeild.placeholder = "Detailed Major".localized()
        
        GradesTextField.delegate = self
        GradesTextField.textColor = UIColor(hexString: "#8c8c8c")
        GradesTextField.infoTextColor = UIColor(hexString: "#FF2600")
        GradesTextField.infoFontSize = 14.0
        GradesTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        GradesTextField.activeLineWidth = 2
        GradesTextField.animationDuration = 0.5
        GradesTextField.minimumPlaceholderFontSize = 15
        GradesTextField.originalPlaceholderFontSize = 15
        GradesTextField.placeholderDuration = 0.2
        GradesTextField.placeholderColor = .darkGray
        GradesTextField.keyboardType = .numberPad
        GradesTextField.placeholder = "Grades".localized()
        
        fromTxtFeild.delegate = self
        fromTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        fromTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        fromTxtFeild.infoFontSize = 14.0
        fromTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        fromTxtFeild.activeLineWidth = 2
        fromTxtFeild.animationDuration = 0.5
        fromTxtFeild.minimumPlaceholderFontSize = 15
        fromTxtFeild.originalPlaceholderFontSize = 15
        fromTxtFeild.placeholderDuration = 0.2
        fromTxtFeild.placeholderColor = .darkGray
        fromTxtFeild.infoLabel.numberOfLines = 2
        fromTxtFeild.placeholder = "From".localized()
        
        ToTxtFeild.delegate = self
        ToTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        ToTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        ToTxtFeild.infoFontSize = 14.0
        ToTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        ToTxtFeild.activeLineWidth = 2
        ToTxtFeild.animationDuration = 0.5
        ToTxtFeild.minimumPlaceholderFontSize = 15
        ToTxtFeild.originalPlaceholderFontSize = 15
        ToTxtFeild.placeholderDuration = 0.2
        ToTxtFeild.placeholderColor = .darkGray
        ToTxtFeild.infoLabel.numberOfLines = 2
        ToTxtFeild.placeholder = "To".localized()
    }
    @IBAction func toDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.tag = 2
        datePicker.show(inVC: self)
    }
    
    @IBAction func toStateChanged(_ sender: Any) {
        
        if self.ToState.isOn {
            self.toBtnToBeDisabled.isUserInteractionEnabled = false
            self.ToTxtFeild.isUserInteractionEnabled = false
            self.ToTxtFeild.showInfo("")
            self.ToTxtFeild.placeholder = ""
            self.ToTxtFeild.text = ""
            self.toLbl.isHidden = true
            self.toReqStar.isHidden = true
            self.toDateStack.isHidden = true
            
        }else{
            self.ToTxtFeild.placeholder = "To".localized()
            self.toBtnToBeDisabled.isUserInteractionEnabled = true
            self.toLbl.isHidden = false
            if  self.RequireDate == 1 {
                self.toReqStar.isHidden = false
            }
            else{
                self.toReqStar.isHidden = true
            }
            self.toDateStack.isHidden = false
        }
    }
    
    @IBAction func fromDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.tag = 1
        datePicker.show(inVC: self)
    }
    
    @IBAction func chooseCountry(_ sender: UIButton) {
        print("choose country")
        self.view.endEditing(true)
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: true, countriesNames: countriesNames)
        }
    }
    
    @IBAction func chooseEducationIxt(_ sender: UIButton) {
        print("choose country")
        self.view.endEditing(true)
        educatioalIstTextFeild.showInfo("")
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popupViewCtrl") as? popupVCtrl {
            viewController.modalPresentationStyle = .overCurrentContext
            viewController.delegate = self
            viewController.ItemsArrayToDisplay = InstitutionsList
            self.present(viewController, animated: true, completion: nil)
        }
        //        let Names = L102Language.isRTL ? InstitutionsList.map {$0.nameAr} : InstitutionsList.map {$0.nameEn}
        //
        //        if Names.count > 0 {
        //            showSchoolSelectPopover(sender: sender, isUserCountry: true, Names: Names)
        //        }
        //        self.view.endEditing(true)
        //        let languagePicker = UIPickerView()
        //        let languageArrayToDisplay = L102Language.isRTL ? InstitutionsList.map {$0.nameAr} : InstitutionsList.map {$0.nameEn}
        //        if languageArrayToDisplay.count > 0 {
        //
        //            languagePicker.addCustomPicker(languageArrayToDisplay as NSArray , sender , "", 0) { (selectedIndex) in
        //
        //                if self.InstitutionsList[selectedIndex].id! == -1 {
        //                    self.instituteNameBtn.isUserInteractionEnabled = false
        //                    self.dropDownIcon.isHidden = true
        //                    self.educatioalIstTextFeild.text = ""
        ////                    self.educatioalIstTextFeild.showInfo("")
        //                    self.institutionId = -1
        //
        //                }else{
        //                    self.instituteNameBtn.isUserInteractionEnabled = true
        //                    self.dropDownIcon.isHidden = false
        //                    self.educatioalIstTextFeild.text = languageArrayToDisplay[selectedIndex]
        //                    self.educatioalIstTextFeild.showInfo("")
        //                    self.institutionId = self.InstitutionsList[selectedIndex].id!
        //                }
        //
        //            }
        //
        //        }else{
        //            print("no date in languagesArrayToDisplay")
        //        }
        
    }
    
    
    @IBAction func chooseDegree(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let languagePicker = UIPickerView()
        let languageArrayToDisplay = L102Language.isRTL ? certificatesList.map {$0.certificateAr} : certificatesList.map {$0.certificateEn}
        if languageArrayToDisplay.count > 0 {
            
            languagePicker.addCustomPicker(languageArrayToDisplay as NSArray , sender , "Degree", 0) { (selectedIndex) in
                if self.institutionId == -1 {
                    self.instituteNameBtn.isUserInteractionEnabled = true
                    self.dropDownIcon.isHidden = false
                    self.educatioalIstTextFeild.text = ""
                    //                    self.educatioalIstTextFeild.tintColor = .clear
                    
                    self.institutionId = 0
                }
                
                
                self.degreeTxtFeild.text = languageArrayToDisplay[selectedIndex]
                self.degreeTxtFeild.showInfo("")
                self.degreeId = self.certificatesList[selectedIndex].id!
                self.RequireDate = self.certificatesList[selectedIndex].RequireDate ?? 0
                self.RequireMajor = self.certificatesList[selectedIndex].RequireMajor ?? 0
                self.RequireGrades = 0
                if self.RequireGrades == 1 {
                    self.gradesReqStar.isHidden = false
                }
                else{
                    self.gradesReqStar.isHidden = true
                }
//                if self.RequireMajor == 1 {
//                    self.majorReqStar.isHidden = false
//                }
//                else{
//                    self.majorReqStar.isHidden = true
//                }
                if self.RequireDate == 1 {
                    self.fromReqStar.isHidden = false
                    self.toReqStar.isHidden = false
                }
                else{
                    self.fromReqStar.isHidden = true
                    self.toReqStar.isHidden = true
                }
                if self.RequireGrades == 1 {
                    DispatchQueue.main.async {
                        self.GradesLbl.isHidden = false
                        self.GradesTopConstain.constant = 25
                        self.GradesBottomConstrain.constant = 5
                        self.GradesLblHeightConstrain.constant = 26
                        self.GradesTxtView.isHidden = false
                        self.GradesTxtFieldHeightConstrain.constant = 56
                        self.mainViewHeight.constant = 900
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.GradesLbl.isHidden = true
                        self.GradesTopConstain.constant = 0
                        self.GradesBottomConstrain.constant = 0
                        self.GradesLblHeightConstrain.constant = 0
                        self.GradesTxtView.isHidden = true
                        self.GradesTxtFieldHeightConstrain.constant = 0
                        self.mainViewHeight.constant = 800
                        self.GradesTextField.text = ""
                    }
                }
            }
            
        }else{
            print("no date in languagesArrayToDisplay")
        }
        
        
    }
    @IBAction func closePopup(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtn(_ sender: Any) {
        if(validateLoginTextFields()) {
            
            
            
            if self.educatioalIstTextFeild.text!.count > 70{
                print("educatioalIstTextFeild.text!.count:  \(educatioalIstTextFeild.text!.count)")
                self.showErrorMessage(message: "Educational institution name may not greater than 70 characters".localized())
                return
            }
            if self.majorTxtFeild.text!.count > 70{
                print("majorTxtFeild.text!.count: \(majorTxtFeild.text!.count)")
                self.showErrorMessage(message: "Major may not greater than 70 characters".localized())
                return
            }
            if self.GradesTextField.text!.count > 70{
                print("majorTxtFeild.text!.count: \(GradesTextField.text!.count)")
                self.showErrorMessage(message: "Grades may not greater than 70 characters".localized())
                return
            }
            if selectededuId == 0 {
                print("add edu")
                let edus = eduactionalObjectToPost()
                edus.EducationalInformation = [educationalObj]()
                
                var arr  =  [educationalObj]()
                for item in selectededus {
                    //                    var name : String = ""
                    //                    if item.InstitutionID == -1 {
                    //                        name = item.Institution_Name!
                    //                    }else{
                    //                        name = (L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn)!
                    //                    }
                    
                    //                    let name = L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn
                    var to_date: Int?
                    if let to = item.toDate {
                        to_date = Int(to)
                    }
                    var from_datee: Int?
                    if let from = item.fromDate {
                        from_datee = Int(from)
                    }
                    arr.append(educationalObj(Institution_Name: item.Institution_Name ?? "", InstitutionID: item.InstitutionID ?? 0, country_id: item.country_id ?? nil, certificate_id: item.certificate_id ?? 0, majorName: item.majorName ?? "", grades: item.grades ?? "",toDate: to_date,fromDate: from_datee, specialitilty_id: item.specialitilty_id) )
                }
                
                if let x = toDate{
                    var from_datee: Int?
                    if let from = fromDate {
                        from_datee = Int(from)
                    }
                    arr.append(educationalObj(Institution_Name:educatioalIstTextFeild.text ?? "", InstitutionID: institutionId, country_id: selectedUserCountryId ?? nil, certificate_id: degreeId, majorName: majorTxtFeild.text ?? "", grades: GradesTextField.text ?? "",toDate: x,fromDate: from_datee, specialitilty_id: self.selectedSpecialityID))
                }else{
                    arr.append(educationalObj(Institution_Name:educatioalIstTextFeild.text ?? "", InstitutionID: institutionId, country_id: selectedUserCountryId ?? nil, certificate_id: degreeId, majorName: majorTxtFeild.text ?? "", grades: GradesTextField.text ?? "",toDate: nil,fromDate: fromDate ?? nil, specialitilty_id: self.selectedSpecialityID))
                }
                edus.EducationalInformation.append(contentsOf: arr)
                
                let user = UserManager.sharedInstance.getUser()
                user?.EducationalInformation2 =  edus
                user?.SectionID = 4
                presenter.doEditNewProfile(data: user!)
            }
            else{
                print("edit edu")
                let edus = eduactionalObjectToPost()
                edus.EducationalInformation = [educationalObj]()
                var arr  =  [educationalObj]()
                for item in selectededus{
                    if item.id == selectededuId{
                        item.Institution_Name = educatioalIstTextFeild.text!
                        item.InstitutionID = institutionId
                        item.country_id = selectedUserCountryId
                        item.certificate_id = degreeId
                        item.majorName = majorTxtFeild.text
                        item.grades =  GradesTextField.text ?? ""
                        item.specialitilty_id = selectedSpecialityID
                        if !ToState.isOn {
                            item.toDate = String(toDate ?? 0)
                        }
                        else{
                            item.toDate = nil
                        }
                        var from_date: Int?
                        if let from = fromDate {
                            from_date = Int(from)
                            item.fromDate = String(from_date!)
                        }
                        else{
                            item.fromDate = nil
                        }
                        
                    }
                    var to_date: Int?
                    if let to = item.toDate {
                        to_date = Int(to)
                    }
                    var from_date: Int?
                    if let from = item.fromDate {
                        from_date = Int(from)
                    }
                    arr.append(educationalObj(Institution_Name: item.Institution_Name ?? "", InstitutionID: item.InstitutionID ?? 0, country_id: item.country_id ?? 0, certificate_id: item.certificate_id ?? 0, majorName: item.majorName ?? "", grades: item.grades ?? "",toDate: to_date,fromDate: from_date, specialitilty_id: item.specialitilty_id) )
                }
                edus.EducationalInformation.append(contentsOf: arr)
                let user = UserManager.sharedInstance.getUser()
                user?.EducationalInformation2 =  edus
                user?.SectionID = 4
                presenter.doEditNewProfile(data: user!)
            }
        }
    }
    func validateLoginTextFields() -> Bool {
        var valid = true
        if let val = self.degreeTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.degreeTxtFeild.showInfo("please enter your Degree".localized())
            valid = false
        }
        
        if let val = self.educatioalIstTextFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.educatioalIstTextFeild.showInfo("please enter your University/Institution Nam".localized())
            valid = false
        }
        
        if let val = self.educatioalIstTextFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.educatioalIstTextFeild.showInfo("please enter your University/Institution Name".localized())
            valid = false
        }
        if let val = self.generalMajorTxtField.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.generalMajorTxtField.showInfo("please enter your general major".localized())
            valid = false
        }
        //        if let val = self.countryTxtFeild.text , !val.isBlank {
        //            print("valid")
        //        }else {
        //            print("error")
        //            self.countryTxtFeild.showInfo("please enter your University/Institution Country".localized())
        //            valid = false
        //        }
        if let val = self.majorTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
//            if RequireMajor == 1 {
//                self.majorTxtFeild.showInfo("please enter your major".localized())
//                valid = false
//            }
        }
        if self.RequireGrades == 1 {
            if let val = self.GradesTextField.text , !val.isBlank {
                print("valid")
            }else {
                print("error")
                self.GradesTextField.showInfo("please enter your grades".localized())
                valid = false
            }
        }
        
        if let val = self.fromTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            if RequireDate == 1 {
                self.fromTxtFeild.showInfo("enter start date".localized())
                valid = false
            }
        }
        if !ToState.isOn {
            if let val = self.ToTxtFeild.text , !val.isBlank {
                print("valid")
            }else {
                print("error")
                if RequireDate == 1 {
                    self.ToTxtFeild.showInfo("enter end date".localized())
                    valid = false
                }
            }
        }
        
        return valid
    }
    func showSchoolSelectPopover(sender: UIView,isUserCountry:Bool, Names: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        let selectedName = educatioalIstTextFeild.text ?? ""
        var selectedItem = [String]()
        selectedItem.append(selectedName)
        selectionMenu.setSelectedItems(items: selectedItem) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedSchool  = self!.InstitutionsList.first(where: {$0.nameAr == text!!}) {
                    if let selectedID = selectedSchool.id{
                        if selectedID == -1 {
                            self!.instituteNameBtn.isUserInteractionEnabled = false
                            self!.dropDownIcon.isHidden = true
                            self!.educatioalIstTextFeild.text = ""
                            //                    self.educatioalIstTextFeild.showInfo("")
                            self!.educatioalIstTextFeild.becomeFirstResponder()
                            self!.institutionId = -1
                            
                        }else{
                            self!.instituteNameBtn.isUserInteractionEnabled = true
                            self!.dropDownIcon.isHidden = false
                            self!.educatioalIstTextFeild.text = selectedSchool.nameAr
                            self!.educatioalIstTextFeild.showInfo("")
                            self!.institutionId = selectedID
                        }
                    }
                }
            }
            else{
                if let selectedSchool  = self!.InstitutionsList.first(where: {$0.nameEn == text!!}) {
                    if let selectedID = selectedSchool.id{
                        if selectedID == -1 {
                            self!.instituteNameBtn.isUserInteractionEnabled = false
                            self!.dropDownIcon.isHidden = true
                            self!.educatioalIstTextFeild.text = ""
                            //                    self.educatioalIstTextFeild.showInfo("")
                            self!.institutionId = -1
                            
                        }else{
                            self!.instituteNameBtn.isUserInteractionEnabled = true
                            self!.dropDownIcon.isHidden = false
                            self!.educatioalIstTextFeild.text = selectedSchool.nameEn
                            self!.educatioalIstTextFeild.showInfo("")
                            self!.institutionId = selectedID
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return Names.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    func showCountrySelectPopover(sender: UIView,isUserCountry:Bool, countriesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: countriesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        if isUserCountry {
            selectedCountryName = selectedCountry
        }
        else{
            selectedCountryName = selectededuCountry
        }
        selectionMenu.setSelectedItems(items: selectedCountryName) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCountry  = self!.countryList.first(where: {$0.nameAr == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.countryTxtFeild.text = text!!
                            self!.countryTxtFeild.showInfo("")
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            //                        self!.educationCountryTxtField.text = text!!
                        }
                    }
                    
                }
            }
            else{
                if let selectedCountry  = self!.countryList.first(where: {$0.nameEn == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedUserCountryId = selectedCountryID
                            self!.countryTxtFeild.text = text!!
                            self!.countryTxtFeild.showInfo("")
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            //                            self!.educationCountryTxtField.text = text!!
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return countriesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    @IBAction func chooseGeneralMajor(_ sender: UIButton) {
        print("choose GeneralMajor")
        self.view.endEditing(true)
        let GeneralMajorNames = L102Language.isRTL ? specialities.map {$0.nameAr } : specialities.map {$0.nameEn}
        
        if GeneralMajorNames.count > 0 {
            showGeneralMajorPopover(sender: sender, specialities: GeneralMajorNames)
        }
        else{
            presenter2.doGetcertificatesNewList()
        }
    }
    func showGeneralMajorPopover(sender: UIView, specialities: [String?]) {
        
        // selection type as multiple with subTitle Cell
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: specialities, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        selectionMenu.setSelectedItems(items: selectedSpeciality) { [weak self] (text, index, selected, selectedList) in
            self!.generalMajorTxtField.showInfo("")
            if L102Language.isRTL {
                if let selectedField  = self!.specialities.first(where: {$0.nameAr == text!!}) {
                    if let selectedFieldID = selectedField.id{
                        self!.selectedSpecialityID = selectedFieldID
                        self!.generalMajorTxtField.text = text!!
                    }
                }
            }
            else{
                if let selectedField  = self!.specialities.first(where: {$0.nameEn == text!!}) {
                    if let selectedFieldID = selectedField.id{
                        self!.selectedSpecialityID = selectedFieldID
                        self!.generalMajorTxtField.text = text!!
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return specialities.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension addEducationalInfoVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == degreeTxtFeild {
            degreeTxtFeild.showInfo("")
        }
        else if textField == educatioalIstTextFeild {
            educatioalIstTextFeild.showInfo("")
        }
        else if textField == countryTxtFeild {
            countryTxtFeild.showInfo("")
        }
        else if textField == majorTxtFeild {
            majorTxtFeild.showInfo("")
        }
        else if textField == GradesTextField {
            GradesTextField.showInfo("")
        }
        else if textField == ToTxtFeild {
            ToTxtFeild.showInfo("")
        }
        else if textField == fromTxtFeild {
            fromTxtFeild.showInfo("")
        }
    }
}



extension addEducationalInfoVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess() {
        print("onSucess")
        let user1  = UserManager.sharedInstance.getUser()
        UserManager.sharedInstance.saveUser(user:user1!)
        self.delegate?.sendData(text: "certificate")
        self.dismiss(animated: true, completion: nil)
        
    }
}

extension addEducationalInfoVCtrl: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        if gmDatePicker.tag == 1{
            if let to = toDateObj {
                if date >= to {
                    self.showErrorMessage(message: "To date must be after from date".localized())
                    return
                }
            }
            fromTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
            fromTxtFeild.showInfo("")
            fromDateObj = date
            fromDate = date.dateToTimeStamp()
        }else{
            if let from = fromDateObj {
                if date <= from {
                    self.showErrorMessage(message: "To date must be after from date".localized())
                    return
                }
            }
            toDateObj = date
            ToTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
            ToTxtFeild.showInfo("")
            toDate = date.dateToTimeStamp()
        }
        
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker() -> GMDatePicker {
        
        let datePicker = GMDatePicker()
        datePicker.delegate = self
        let calendar = Calendar.current
        //calendar.timeZone = TimeZone(identifier: "UTC")!
        var startingDate: Date? = Date()
        if let userBirthDate = toDate{
            startingDate = Date(timeIntervalSince1970: Double(userBirthDate))
        }
        if let userBirthDate = fromDate{
            startingDate = Date(timeIntervalSince1970: Double(userBirthDate))
        }
        
        datePicker.config.startDate = startingDate ?? Date()
        let date_less16 = calendar.date(byAdding: .year, value: 0, to: Date())
        datePicker.config.maxDate =  calendar.date(byAdding: .day, value: -1, to: date_less16!)
        datePicker.config.animationDuration = 0.0
        
        datePicker.config.cancelButtonTitle = "CancelPicker".localized()
        datePicker.config.confirmButtonTitle = "Done".localized()
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor.frenchBlue
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
        return datePicker
    }
    
    
}


extension addEducationalInfoVCtrl: CountryListView{
    func onSucessLoadingCountryList(countries: [CountryRes]) {
        print("countries count : \(countries.count)")
        self.countryList.removeAll()
        for item in countries{
            self.countryList.append(countryItem(id: item.id, nameEn: item.nameEn, nameAr: item.nameAr, code: item.isoCode,cities: [cityItem(id: 1, cityEn: "x", cityAr: "x")]))
        }
        if selectededuId != 0 {
            print("selectededuId from edit :  \(selectededuId)")
            
            for item in selectededus{
                if item.id == selectededuId{
                    
                    self.selectedUserCountryId = item.country_id
                    
                    for item in self.countryList {
                        if self.selectedUserCountryId == item.id {
                            self.countryTxtFeild.text = L102Language.isRTL ? item.nameAr : item.nameEn
                        }
                    }
                    
                    
                }}}
        
        if selectededuId != 0 {
            print("selectededuId from edit :  \(selectededuId)")
            
            for item in selectededus{
                if item.id == selectededuId{
                    self.majorTxtFeild.text = item.majorName
                    self.GradesTextField.text = item.grades ?? ""
                    if let from = item.fromDate {
                        self.fromDate = Int(from)!
                        self.fromDateObj = Date(timeIntervalSince1970: TimeInterval(Int(from)!))
                    }
                    if let to = item.toDate {
                        self.toDate = Int(to)!
                        self.toDateObj = Date(timeIntervalSince1970: TimeInterval(Int(to)!))
                        self.ToTxtFeild.text = Int(to)!.getCurrentDateString()
                    }
                    else{
                        ToState.isOn = true
                        self.toBtnToBeDisabled.isUserInteractionEnabled = false
                        self.ToTxtFeild.isUserInteractionEnabled = false
                        self.ToTxtFeild.showInfo("")
                        self.ToTxtFeild.placeholder = ""
                        self.ToTxtFeild.text = ""
                    }
                    if let from = item.fromDate {
                        self.fromTxtFeild.text = Int(from)!.getCurrentDateString()
                    }
                }}}
    }
}
extension addEducationalInfoVCtrl :DegreeListView {
    
    func showLoading() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        if let languages = degreeWorkingTimes.certificates {
            if languages.count > 0 {
                self.certificatesList.removeAll()
                self.certificatesList.append(contentsOf: languages)
                print("self.certificatesList : \(certificatesList)")
            }}
        if let countries = degreeWorkingTimes.countries {
            self.countryList = countries
        }
        if let languages = degreeWorkingTimes.Institutions {
            if languages.count > 0 {
                self.InstitutionsList.removeAll()
                self.InstitutionsList.append(contentsOf: languages)
                //self.InstitutionsList.append(institutionItem(id: -1, nameEn: "Other", nameAr: "اخرى", Active: 1))
                print("self.InstitutionsList : \(InstitutionsList)")
            }}
        if let specialities = degreeWorkingTimes.specialities {
            self.specialities = specialities
        }
        if selectededuId != 0 {
            print("selectededuId from edit :  \(selectededuId)")
            
            for item in selectededus{
                if item.id == selectededuId{
                    
                    self.selectedUserCountryId = item.country_id
                    
                    for item in self.countryList {
                        if self.selectedUserCountryId == item.id {
                            self.countryTxtFeild.text = L102Language.isRTL ? item.nameAr : item.nameEn
                        }
                    }
                    
                    
                }}}
        
        if selectededuId != 0 {
            print("selectededuId from edit :  \(selectededuId)")
            
            for item in selectededus{
                if item.id == selectededuId{
                    self.majorTxtFeild.text = item.majorName
                    self.GradesTextField.text = item.grades ?? ""
                    if let fromDatee = item.fromDate {
                        self.fromDate = Int(fromDatee)!
                        self.fromDateObj = Date(timeIntervalSince1970: TimeInterval(Int(fromDatee)!))
                    }
                    
                    if let to = item.toDate {
                        self.toDate = Int(to)!
                        self.toDateObj = Date(timeIntervalSince1970: TimeInterval(Int(to)!))
                        self.ToTxtFeild.text = Int(to)!.getCurrentDateString()
                    }
                    else{
                        ToState.isOn = true
                        self.toBtnToBeDisabled.isUserInteractionEnabled = false
                        self.ToTxtFeild.isUserInteractionEnabled = false
                        self.ToTxtFeild.showInfo("")
                        self.ToTxtFeild.placeholder = ""
                        self.ToTxtFeild.text = ""
                    }
                    if let fromDatee = item.fromDate {
                        self.fromTxtFeild.text = Int(fromDatee)!.getCurrentDateString()
                    }
                    
                }}}
        if selectededuId != 0 {
            print("selectededuId from edit :  \(selectededuId)")
            for item in selectededus{
                if item.id == selectededuId{
                    if item.EduInfoInstitution != nil{
                        self.educatioalIstTextFeild.text = L102Language.isRTL ? item.EduInfoInstitution?.nameAr : item.EduInfoInstitution?.nameEn
                        self.IndtituteIdForEdit = (item.EduInfoInstitution?.id)!
                        self.institutionId = (item.EduInfoInstitution?.id)!
                    }else{
                        self.instituteNameBtn.isUserInteractionEnabled = false
                        self.dropDownIcon.isHidden = true
                        
                        self.educatioalIstTextFeild.text = item.Institution_Name
                        self.IndtituteIdForEdit = item.InstitutionID!
                        self.institutionId = item.InstitutionID!
                    }
                    
                    if item.EduInfoDegree != nil {
                        self.degreeTxtFeild.text = L102Language.isRTL ? item.EduInfoDegree?.certificateAr : item.EduInfoDegree?.certificateEn
                        self.degreeIdForEdit = (item.EduInfoDegree?.id) ?? 0
                        self.degreeId = (item.EduInfoDegree?.id)!
                    }
                    if let selectedCertificate = certificatesList.first(where: {$0.id == self.degreeId}){
                        self.RequireGrades = 0
                        self.RequireDate = selectedCertificate.RequireDate ?? 0
                        self.RequireMajor = selectedCertificate.RequireMajor ?? 0
                        if self.RequireGrades == 1 {
                            DispatchQueue.main.async {
                                self.GradesLbl.isHidden = false
                                self.gradesReqStar.isHidden = false
                                self.GradesTopConstain.constant = 25
                                self.GradesBottomConstrain.constant = 5
                                self.GradesLblHeightConstrain.constant = 26
                                self.GradesTxtView.isHidden = false
                                self.GradesTxtFieldHeightConstrain.constant = 56
                                self.mainViewHeight.constant = 900
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self.GradesLbl.isHidden = true
                                self.gradesReqStar.isHidden = true
                                self.GradesTopConstain.constant = 0
                                self.GradesBottomConstrain.constant = 0
                                self.GradesLblHeightConstrain.constant = 0
                                self.GradesTxtView.isHidden = true
                                self.GradesTxtFieldHeightConstrain.constant = 0
                                self.mainViewHeight.constant = 800
                                self.GradesTextField.text = ""
                            }
                        }
//                        if self.RequireMajor == 1 {
//                            self.majorReqStar.isHidden = false
//                        }
//                        else{
//                            self.majorReqStar.isHidden = true
//                        }
                        if self.RequireDate == 1 {
                            self.fromReqStar.isHidden = false
                            self.toReqStar.isHidden = false
                            if ToState.isOn == true {
                                self.toLbl.isHidden = true
                                self.toReqStar.isHidden = true
                                self.toDateStack.isHidden = true
                            }
                        }
                        else{
                            self.fromReqStar.isHidden = true
                            self.toReqStar.isHidden = true
                            if ToState.isOn == true {
                                self.toLbl.isHidden = true
                                self.toReqStar.isHidden = true
                                self.toDateStack.isHidden = true
                            }
                        }
                    }
                }
            }
        }
    }
}
extension addEducationalInfoVCtrl: popupSendData {
    func sendData(text: String, id: Int) {
        self.institutionId = id
        self.educatioalIstTextFeild.text = text
    }
    
    
}
