//
//  changePhoneVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/18/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TransitionButton
import TweeTextField

class changePhoneVCtrl: UIViewController {
    
    @IBOutlet weak var forgotPasswordButton: TransitionButton!
    @IBOutlet weak var countryCodeField: CTKFlagPhoneNumberTextField!
    @IBOutlet weak var phoneNumberTextField: TweeAttributedTextField!

    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneViewInternal: UIView!
    @IBOutlet weak var backBtn: UIButton!
    let checkPhoneForChange = ChangePhonePresenter()
    var phone = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //forgotPasswordButton.dropShadow()
        self.checkPhoneForChange.attachView(view: self)
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getCode"), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.getCode(_:)),
                                                      name: NSNotification.Name(rawValue: "getCode"),
                                                      object: nil)
        setupKeyboard()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        self.view.changeFont()
        countryCodeLbl.text = (countryCodeField.getCountryPhoneCode() ?? "+966")
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func getCode(_ notification: NSNotification){
        print("getCode \(String(describing: notification.userInfo?["code"]))")
        if let code = notification.userInfo?["code"] {
            self.resetCustomTextFieldsErrorMessages(self.phoneNumberTextField)
            self.countryCodeLbl.text = code as? String
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async{
            self.phoneViewInternal.semanticContentAttribute = .forceLeftToRight
            self.countryCodeField.semanticContentAttribute = .forceLeftToRight
        }
    }
    func setupKeyboard(){
        phoneView.layer.cornerRadius = 2
        phoneView.layer.shadowColor = UIColor.black.cgColor
        phoneView.layer.shadowOpacity = 0.2
        phoneView.layer.shadowOffset = .zero
        phoneView.layer.shadowRadius = 10
        
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.returnKeyType = .next
        phoneNumberTextField.infoTextColor = UIColor(hexString: "#FF2600")
        phoneNumberTextField.infoFontSize = 14.0
        phoneNumberTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        phoneNumberTextField.activeLineWidth = 2
        phoneNumberTextField.animationDuration = 0.5
        phoneNumberTextField.minimumPlaceholderFontSize = 15
        phoneNumberTextField.originalPlaceholderFontSize = 15
        phoneNumberTextField.placeholderDuration = 0.2
        phoneNumberTextField.placeholderColor = .darkGray
        phoneNumberTextField.delegate = self
        phoneNumberTextField.textColor = UIColor(hexString: "#8C8C8C")
        phoneNumberTextField.placeholder = "Phone".localized()
        setUpCountryCode()
    }
    func setUpCountryCode(){
        countryCodeField.borderStyle = .none
        countryCodeField.parentViewController = self
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
    }
    private func getCustomTextFieldInputAccessoryView(with items: [UIBarButtonItem]) -> UIToolbar {
        let toolbar: UIToolbar = UIToolbar()
        
        toolbar.barStyle = UIBarStyle.default
        toolbar.items = items
        toolbar.sizeToFit()
        return toolbar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if validateForgotPassword() {
            var phone_no = phoneNumberTextField.text!.replacedArabicDigitsWithEnglish
            if phone_no.first == "0" {
              phone_no.remove(at: (phone_no.startIndex))
            }
            let phone = countryCodeField.text! + phone_no
            self.phone = phone
            checkPhoneForChange.doCheckPhone(mobile: phone)
        }
    }
    func validateForgotPassword()->Bool{
        var valid = true
        if let phoneNumber = self.phoneNumberTextField.text , !phoneNumber.isBlank{
            
            if phoneNumber.isValidPhoneNumber(code: countryCodeField.getCountryPhoneCode() ?? "+966") {
                print("valid phoneNumber")
            }else {
                let errorMessage = "please enter valid phone number".localized()
                self.phoneNumberTextField.showInfo(errorMessage, animated: true)
                
                valid = false
            }
        }else {
            print("error phoneNumber")
            let errorMessage  = "please enter your phone number".localized()
            self.phoneNumberTextField.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        return valid
        
    }
    
    func countryCodeTxtFieldConfiguration(){
        
        countryCodeField.borderStyle = .none
        countryCodeField.parentViewController = self
        countryCodeField.textAlignment = .center
        //        Example of customizing the textField input accessory view
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
        countryCodeField.flagSize = CGSize(width: 25, height: 25)
        
        
        countryCodeField.setFlag(with: Locale.current.regionCode ?? "SA")
        print(Locale.current.regionCode ?? "SA")
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeForgetPassSeg" {
            let destintion  = segue.destination as? ForgotPasswordTokenViewController
            let phone = countryCodeField.text! +  phoneNumberTextField.text!
            destintion?.phone = phone
        }
    }
    
    
}
extension changePhoneVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == self.phoneNumberTextField) {
            self.view.endEditing(true)
        }
            
        else {
            self.view.endEditing(true)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        if textField == phoneNumberTextField {
//            if phoneNumberTextField.text?.count == 0 && string == "0" {
//
//                return false
//            }
            if range.location == 14 {
                
                return false
            }
        }
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.phoneNumberTextField {
            self.phoneNumberTextField.showInfo("", animated: true)
        }
    }
}
extension changePhoneVCtrl:CheckPhoneForChangeView {
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.forgotPasswordButton.startAnimation()
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.forgotPasswordButton.stopAnimation()
    }
    
    func onSucess() {
        
        self.showSucessMessage(message:"Verification code has been sent successfully".localized())
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Authorizations", bundle: nil).instantiateViewController(withIdentifier: "VerficationCodeViewController") as? VerficationCodeViewController {
                viewController.phone = self.phone.replacedArabicDigitsWithEnglish
                viewController.verificationCase = .Profile
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    func onError(message: String) {
        self.view.isUserInteractionEnabled = true
        self.showErrorMessage(message: message)
    }
    
    
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


