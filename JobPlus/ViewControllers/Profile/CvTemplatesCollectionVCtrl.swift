//
//  CvTemplatesCollectionVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 26/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class cvTempleteCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var cvImg: UIImageView!
    @IBOutlet weak var downloadCv: UIButton!
    @IBOutlet weak var showCv: UIButton!
}

private let reuseIdentifier = "cvTemplateCell"

//class CvTemplatesCollectionVCtrl: UICollectionViewController {
class CvTemplatesCollectionVCtrl: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout, WKNavigationDelegate {
    
    @IBOutlet weak var cvTemplatesCollection: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    
    let editpresenter = RegisterPresenter()
    let presenter = DegreeListPresenter()
    var cvTemplatesList = [cvTempleteItem]()
    var selectedTmp : Int = 0
    var Selectedimg : String = ""
    var spinnerView: UIView?
    var downloadedCVUrl:String = ""
    
    var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("")
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        presenter.attachView(view: self)
        editpresenter.attachView(view: self)
        presenter.doGetcertificatesNewList()
        // Register cell classes
        //        self.cvTemplatesCollection!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    //    override
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    //    override
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return cvTemplatesList.count
    }
    
    //    override
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! cvTempleteCollectionViewCell
        
        
        let item =  self.cvTemplatesList[indexPath.row]
        print("item.imageAr! : \(item.imageAr!)")
        //        cell.cvImg.image = UIImage(named: item.imageAr!)
        //        cell.cvImg.imageFromServerURL(withUrl: item.imageAr!)
        cell.cvImg.kf.indicatorType = .activity
        let url = URL(string:  L102Language.isRTL ? item.imageAr! : item.imageEn! )
        
        //        let processor = ResizingImageProcessor(referenceSize: CGSize(width: 80, height:60))
        
        cell.cvImg.kf.setImage(
            with: url,
            placeholder: UIImage(named: item.cvName ?? " "),
            options: [
                
                //                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage,
                
            ], completionHandler:
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                })
        //cell.catNameLbl.text = L102Language.isRTL ? homeCats[indexPath.row].nameAr : homeCats[indexPath.row].name
        
        cell.showCv.tag = indexPath.row
        cell.showCv.addTarget(self, action: #selector(openTemplateImg(_:)), for: .touchUpInside)
        cell.downloadCv.tag = indexPath.row
        cell.downloadCv.addTarget(self, action: #selector(downloadCV(_:)), for: .touchUpInside)
        return cell
    }
    @objc func downloadCV(_ btn: UIButton){
        if let subscribedUser = UserManager.sharedInstance.subscribedUser {
            if subscribedUser {
                let user = UserManager.sharedInstance.getUser()
                user?.SectionID = 11
                user?.cv_id = cvTemplatesList[btn.tag].id!
                editpresenter.doEditNewProfile(data: user!)
            }
            else{
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                    viewController.modalPresentationStyle = .overCurrentContext
                    viewController.from = "profile"
                    if let msg = UserManager.sharedInstance.NotSubscribedMeassage{
                        print("msg : \(msg)")
                        viewController.titleMsg = msg
                    }
                    self.present(viewController, animated: true, completion: nil)
                }
            }
        }
        
    }
    @objc func openTemplateImg(_ btn: UIButton){
        print("openTemplateImg")
        self.selectedTmp = cvTemplatesList[btn.tag].id!
        self.Selectedimg  = L102Language.isRTL ? cvTemplatesList[btn.tag].imageAr! : cvTemplatesList[btn.tag].imageEn!
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "opnImgsegue", sender: self)
        }
        
    }
    
    
    
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "opnImgsegue" {
            if let destnationViewcontroller = segue.destination as? FullScreenVCtrl {
                
                destnationViewcontroller.Selectedimg = self.Selectedimg
                
            }
            
            
        }else if segue.identifier == "openDownloadedCv" {
            if let destnationViewcontroller = segue.destination as? openDownloadedCVVCtrl {
                
                destnationViewcontroller.downloadedCV = self.downloadedCVUrl
                
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width/2.0) - 5 //Display 2 elements in a row.
        collectionViewSize.height = 240
        return collectionViewSize
    }
    
    
}
extension CvTemplatesCollectionVCtrl :DegreeListView {
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    func showLoading() {
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(String(describing: degreeWorkingTimes.cvTemplates))")
        if let templates = degreeWorkingTimes.cvTemplates {
            if templates.count > 0 {
                self.cvTemplatesList.removeAll()
                self.cvTemplatesList.append(contentsOf: templates)
                cvTemplatesCollection.reloadData()
                print("self.cvTemplatesList : \(cvTemplatesList)")
                
            }}
    }
}

extension CvTemplatesCollectionVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess() {
        print("onSucess")
        let user1  = UserManager.sharedInstance.getUser()
        if let downloadedLinkCV = user1?.CvDownloadLink {
            print("downloadedLinkCV : \(downloadedLinkCV)")
            self.downloadedCVUrl = downloadedLinkCV
            self.performSegue(withIdentifier: "openDownloadedCv", sender: self)
            
//            //webView = WKWebView(frame: self.view.bounds, configuration: WKWebViewConfiguration())  CGRect(x: 0, y: 0, width: 2000, height: 900)
//            webView = WKWebView(frame: self.view.bounds, configuration: WKWebViewConfiguration())
//            webView.navigationDelegate = self
//            webView.load(URLRequest(url: URL(string: downloadedLinkCV.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!))
        }
    }
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        let pdfData = self.webView.exportAsPdfFromWebView()
//        let activityViewController = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
//        present(activityViewController, animated: true, completion: nil)
//    }
}
//extension WKWebView {
//
//// Call this function when WKWebView finish loading
// func exportAsPdfFromWebView() -> NSMutableData {
//    let pdfData = createPdfFile(printFormatter: self.viewPrintFormatter())
//    return pdfData
// }
//
// func createPdfFile(printFormatter: UIViewPrintFormatter) -> NSMutableData {
//    
//    let originalBounds = self.bounds
//    self.bounds = CGRect(x: originalBounds.origin.x, y: bounds.origin.y, width: self.bounds.size.width, height: self.scrollView.contentSize.height)
//    let pdfPageFrame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.scrollView.contentSize.height)
//    let printPageRenderer = UIPrintPageRenderer()
//    printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
//    printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 800, height: 1400)), forKey: "paperRect")
//    printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 800, height: 1400)), forKey: "printableRect")
//    self.bounds = originalBounds
//    return printPageRenderer.generatePdfData()
// }
//
// // Save pdf file in document directory
// func saveWebViewPdf(data: NSMutableData) -> String {
//    
//    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//    let docDirectoryPath = paths[0]
//    let pdfPath = docDirectoryPath.appendingPathComponent("webViewPdf.pdf")
//    if data.write(to: pdfPath, atomically: true) {
//        return pdfPath.path
//    } else {
//        return ""
//    }
//  }
//}
//
//extension UIPrintPageRenderer {
//
// func generatePdfData() -> NSMutableData {
//    let pdfData = NSMutableData()
//    UIGraphicsBeginPDFContextToData(pdfData, CGRect(x: 0, y: 0, width: 800, height: 1400), nil)
//    self.prepare(forDrawingPages: NSMakeRange(0, self.numberOfPages))
//    let printRect = UIGraphicsGetPDFContextBounds()
//    for pdfPage in 0 ..< self.numberOfPages {
//        UIGraphicsBeginPDFPage()
//        self.drawPage(at: pdfPage, in: printRect)
//    }
//    UIGraphicsEndPDFContext();
//    return pdfData
// }
//}
