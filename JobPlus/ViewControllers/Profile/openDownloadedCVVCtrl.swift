//
//  openDownloadedCVVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 18/05/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import WebKit
import PDFKit

class openDownloadedCVVCtrl: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var cvView: UIView!
    @IBOutlet weak var navArrow: UIButton!
    var webView: WKWebView!
    var downloadedCV:String = ""
    var cvDownloaded = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        print("webView : \(downloadedCV)")
        webView = WKWebView(frame: CGRect( x: 0, y: 20, width: cvView.bounds.width, height: cvView.bounds.height ), configuration: WKWebViewConfiguration())
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cvView.addSubview(webView)
        var request = URLRequest(url: URL(string: downloadedCV.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)
        //let userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36"
        //request.addValue(userAgent, forHTTPHeaderField: "User-Agent")
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        webView.load(request)
        webView.allowsBackForwardNavigationGestures = true
        
    }
    @IBAction func dismissView(_ sender: Any) {
        webView.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        cvDownloaded = true
    }
    @IBAction func goBack(_ sender: Any) {
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if cvDownloaded {
            let pdfData = self.exportAsPdfFromWebView()
            let activityViewController = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
            present(activityViewController, animated: true, completion: nil)
        }
        
    }
     func exportAsPdfFromWebView() -> NSMutableData {
        let pdfData = createPdfFile(printFormatter: self.webView.viewPrintFormatter())
         //self.webView.load(URLRequest(url: URL(dataRepresentation: pdfData as Data, relativeTo: nil)!))
        return pdfData
     }
    
    func createPdfFile(printFormatter: UIViewPrintFormatter) -> NSMutableData {

        let originalBounds = self.webView.bounds
        self.webView.bounds = CGRect(x: originalBounds.origin.x, y: webView.bounds.origin.y, width: self.webView.bounds.size.width, height: self.webView.scrollView.contentSize.height)
        let printPageRenderer = UIPrintPageRenderer()
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 595, height: 842)), forKey: "paperRect")
        printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 595, height: 842)), forKey: "printableRect")
        self.webView.bounds = originalBounds
        return printPageRenderer.generatePdfData()
     }
    
     // Save pdf file in document directory
     func saveWebViewPdf(data: NSMutableData) -> String {
    
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("webViewPdf.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
      }


}
//extension WKWebView {
//
//// Call this function when WKWebView finish loading
// func exportAsPdfFromWebView() -> NSMutableData {
//    let pdfData = createPdfFile(printFormatter: self.viewPrintFormatter())
//    return pdfData
// }
//
// func createPdfFile(printFormatter: UIViewPrintFormatter) -> NSMutableData {
//
//    let originalBounds = self.bounds
//    self.bounds = CGRect(x: originalBounds.origin.x, y: bounds.origin.y, width: self.bounds.size.width, height: self.scrollView.contentSize.height)
//    let pdfPageFrame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.scrollView.contentSize.height)
//    let printPageRenderer = UIPrintPageRenderer()
//    printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
//    printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 800, height: 1400)), forKey: "paperRect")
//    printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 800, height: 1400)), forKey: "printableRect")
//    self.bounds = originalBounds
//    return printPageRenderer.generatePdfData()
// }
//
// // Save pdf file in document directory
// func saveWebViewPdf(data: NSMutableData) -> String {
//
//    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//    let docDirectoryPath = paths[0]
//    let pdfPath = docDirectoryPath.appendingPathComponent("webViewPdf.pdf")
//    if data.write(to: pdfPath, atomically: true) {
//        return pdfPath.path
//    } else {
//        return ""
//    }
//  }
//}
//
extension UIPrintPageRenderer {

 func generatePdfData() -> NSMutableData {
    let pdfData = NSMutableData()
    UIGraphicsBeginPDFContextToData(pdfData, CGRect(x: 0, y: 0, width: 595, height: 842), nil)
    self.prepare(forDrawingPages: NSMakeRange(0, self.numberOfPages))
    let printRect = UIGraphicsGetPDFContextBounds()
    for pdfPage in 0 ..< self.numberOfPages {
        UIGraphicsBeginPDFPage()
        self.drawPage(at: pdfPage, in: printRect)
    }
    UIGraphicsEndPDFContext();
    return pdfData
 }
}
