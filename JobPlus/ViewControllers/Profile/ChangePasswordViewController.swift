//
//  ChangePasswordViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/25/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import TweeTextField

class ChangePasswordViewController: UIViewController {
    @IBOutlet weak var changePasswordButton: TransitionButton!
    @IBOutlet weak var oldPasswordTextField: TweeAttributedTextField!
    @IBOutlet weak var newPasswordTextField: TweeAttributedTextField!
    @IBOutlet weak var rePasswordTextField: TweeAttributedTextField!
    let  changePasswordPresenter = ChangePasswordPresenter()
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var newPassView: UIView!
    @IBOutlet weak var rePassView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.changeFont()
        
        oldPasswordView.layer.cornerRadius = 2
        oldPasswordView.layer.shadowColor = UIColor.black.cgColor
        oldPasswordView.layer.shadowOpacity = 0.2
        oldPasswordView.layer.shadowOffset = .zero
        oldPasswordView.layer.shadowRadius = 10
        oldPasswordTextField.isSecureTextEntry = true
        newPasswordTextField.isSecureTextEntry = true
        rePasswordTextField.isSecureTextEntry = true
        
        newPassView.layer.cornerRadius = 2
        newPassView.layer.shadowColor = UIColor.black.cgColor
        newPassView.layer.shadowOpacity = 0.2
        newPassView.layer.shadowOffset = .zero
        newPassView.layer.shadowRadius = 10
        
        rePassView.layer.cornerRadius = 2
        rePassView.layer.shadowColor = UIColor.black.cgColor
        rePassView.layer.shadowOpacity = 0.2
        rePassView.layer.shadowOffset = .zero
        rePassView.layer.shadowRadius = 10
        
        setupTextFields()
        changePasswordPresenter.attachView(view: self)
        self.title = "Change Password".localized()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setupNavigationControllerOfPartTime()
    }
    func setupTextFields(){
        oldPasswordTextField.delegate = self
        oldPasswordTextField.returnKeyType = .next
        
        oldPasswordTextField.infoTextColor = UIColor(hexString: "#FF2600")
        oldPasswordTextField.infoFontSize = 14.0
        oldPasswordTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        oldPasswordTextField.activeLineWidth = 2
        oldPasswordTextField.animationDuration = 0.5
        oldPasswordTextField.minimumPlaceholderFontSize = 15
        oldPasswordTextField.originalPlaceholderFontSize = 15
        oldPasswordTextField.placeholderDuration = 0.2
        oldPasswordTextField.placeholderColor = .darkGray
        oldPasswordTextField.delegate = self
        oldPasswordTextField.placeholder = "Old Password".localized()
        oldPasswordTextField.textColor = UIColor(hexString: "#8C8C8C")
        
        newPasswordTextField.delegate = self
        newPasswordTextField.returnKeyType = .next
        newPasswordTextField.infoTextColor = UIColor(hexString: "#FF2600")
        newPasswordTextField.infoFontSize = 14.0
        newPasswordTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        newPasswordTextField.activeLineWidth = 2
        newPasswordTextField.animationDuration = 0.5
        newPasswordTextField.minimumPlaceholderFontSize = 15
        newPasswordTextField.originalPlaceholderFontSize = 15
        newPasswordTextField.placeholderDuration = 0.2
        newPasswordTextField.placeholderColor = .darkGray
        newPasswordTextField.delegate = self
        newPasswordTextField.placeholder = "newPassword".localized()
        newPasswordTextField.textColor = UIColor(hexString: "#8C8C8C")
        
        rePasswordTextField.delegate = self
        rePasswordTextField.returnKeyType = .done
        rePasswordTextField.infoTextColor = UIColor(hexString: "#FF2600")
        rePasswordTextField.infoFontSize = 14.0
        rePasswordTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        rePasswordTextField.activeLineWidth = 2
        rePasswordTextField.animationDuration = 0.5
        rePasswordTextField.minimumPlaceholderFontSize = 15
        rePasswordTextField.originalPlaceholderFontSize = 15
        rePasswordTextField.placeholderDuration = 0.2
        rePasswordTextField.placeholderColor = .darkGray
        rePasswordTextField.delegate = self
        rePasswordTextField.placeholder = "RePassword".localized()
        rePasswordTextField.textColor = UIColor(hexString: "#8C8C8C")
    }
    func validateChangePasswordFields() -> Bool {
        var valid = true
        if let password = self.oldPasswordTextField.text , !password.isBlank {
            if password.isPasswordMoreThaneightCharacters {
                print("valid password")
            }else {
                self.oldPasswordTextField.showInfo("password must be more than eight characters".localized())
                valid = false
            }
        }else {
            print("error password")
            self.oldPasswordTextField.showInfo("please enter your password".localized())
            valid = false
        }
        if let password = self.newPasswordTextField.text , !password.isBlank {
            if password.isPasswordMoreThaneightCharacters {
                print("valid password")
            }else {
                self.newPasswordTextField.showInfo("password must be more than eight characters".localized())
                valid = false
            }
        }else {
            print("error password")
            self.newPasswordTextField.showInfo("please enter your new password".localized())
            valid = false
        }
        if let confirmPassword = self.rePasswordTextField.text , !confirmPassword.isBlank {
            
                if self.rePasswordTextField.text != self.newPasswordTextField.text {
                    self.rePasswordTextField.showInfo("passwordNotMatched".localized())
                    valid = false
                }
                else{
                print("valid confirmPassword")
                }
            
        }else {
            print("error confirmPassword")
            self.rePasswordTextField.showInfo("please confirm your password".localized())
            valid = false
        }
        
        
        return valid
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        if validateChangePasswordFields() {
            self.changePasswordPresenter.dochangePassword(oldPassword: self.oldPasswordTextField.text!, password: self.rePasswordTextField.text!)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
extension ChangePasswordViewController :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.oldPasswordTextField {
            self.newPasswordTextField.becomeFirstResponder()
        } else if textField == self.newPasswordTextField {
            self.rePasswordTextField.becomeFirstResponder()
        }else
        if(textField == self.rePasswordTextField) {
            self.view.endEditing(true)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.oldPasswordTextField {
            self.oldPasswordTextField.showInfo("")
        }
        if textField == self.newPasswordTextField {
            self.newPasswordTextField.showInfo("")
        }
        if textField == self.rePasswordTextField {
            self.rePasswordTextField.showInfo("")
        }
    }
}

extension ChangePasswordViewController:ChangePasswordView{
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.changePasswordButton.startAnimation()
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.changePasswordButton.stopAnimation()
    }
    
    func onSucess() {
        self.view.isUserInteractionEnabled = true
        self.changePasswordButton.stopAnimation()
        self.showSucessMessage(message: "Success Change Password".localized())
        self.navigationController?.popViewController(animated: true)
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}
