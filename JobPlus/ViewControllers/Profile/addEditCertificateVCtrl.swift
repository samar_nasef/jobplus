//
//  addEditCertificateVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 10/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField


class addEditCertificateVCtrl: UIViewController {
    
    let presenter = RegisterPresenter()
    var issueDate:Int?
    var issueDateObj:Date?
    var expiryDate:Int?
    var epxiryDateObj:Date?
    var InstitutionsList = [institutionItem]()
    var institutionId:Int?
    var delegate: SendDataDelegate?
    let presenter2 = DegreeListPresenter()
    @IBOutlet weak var formHeight: NSLayoutConstraint!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var expiryDateState: UISwitch!
    @IBOutlet weak var expiryDateTxtFeild: TweeAttributedTextField!
    
    @IBOutlet weak var expityDateView: UIView!
    @IBOutlet weak var expiryDateTxt: TweeAttributedTextField!
    @IBOutlet weak var issueDateView: UIView!
    
    @IBOutlet weak var issueDateTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var cerificateNameTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var educatioalIstTextFeild: TweeAttributedTextField!
    @IBOutlet weak var certificateNameView: UIView!
    @IBOutlet weak var dropDownIcon: UIImageView!
    @IBOutlet weak var instituteNameBtn: UIButton!
    @IBOutlet weak var institTop: NSLayoutConstraint!
    var selectedcertificates = [certificateItemFromAPi]()
    var selectedCertificateId : Int = 0
    var spinnerView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        self.view.changeFont()
        presenter.attachView(view: self)
        presenter2.attachView(view: self)
        presenter2.doGetcertificatesNewList()
        if selectedCertificateId != 0 {
            print("selectedCertificateId from edit :  \(selectedCertificateId)")
            
            for item in selectedcertificates{
                if item.id == selectedCertificateId{
                    self.cerificateNameTxtFeild.text = item.certificateName
                    if let dateOfIssue = item.DateOfIssue {
                        self.issueDateTxtFeild.text = Int(dateOfIssue)!.getCurrentDateString()
                        
                        print("issueDateTxtFeild : \(Int(dateOfIssue)!.getCurrentDateString())")
                    }
                    
                    
                    var state:Bool
                    
                    if item.HasIssueDate == 1 {
                        state = true
                    }else{
                        state = false
                    }
                    self.expiryDateState.setOn(state, animated: true)
                    if let date = item.DateOfIssue {
                        self.issueDate = Int(date)
                        self.issueDateObj = Date(timeIntervalSince1970: TimeInterval(Int(date)!))
                    }
                    if let end = item.CertificateEndDate {
                        self.expiryDate = Int(end)
                        self.epxiryDateObj = Date(timeIntervalSince1970: TimeInterval(Int(end)!))
                    }
                    
                    if item.HasIssueDate == 0 {
                        if let end = item.CertificateEndDate {
                            self.expiryDateTxt.text = Int(end)!.getCurrentDateString()
                            print("expiryDateTxt : \(Int(end)!.getCurrentDateString())")
                        }
                        
                    }else{
                        institTop.constant = 20
                        expityDateView.isHidden = true
                        expiryDateLabel.isHidden = true
                        formHeight.constant = 550
                    }
                    if let selectedInstitutionID = item.InstitutionID {
                        self.institutionId = selectedInstitutionID
                    }
                    if let institutionTxt = item.InstitutionText, !(institutionTxt.isBlank) {
                        self.educatioalIstTextFeild.text = institutionTxt
                    }
                    if let instit = item.Institution, (self.institutionId ?? 0) > 0 {
                        if L102Language.isRTL {
                            self.educatioalIstTextFeild.text = instit.nameAr ?? ""
                        }
                        else{
                            self.educatioalIstTextFeild.text = instit.nameEn ?? ""
                        }
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func issueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker(1)
        datePicker.tag = 1
        datePicker.show(inVC: self)
        
    }
    
    @IBAction func expiryDateAction(_ sender: Any) {
        
       // if let x = issueDate {
            self.view.endEditing(true)
            let datePicker = setupDatePicker(2)
            datePicker.tag = 2
            datePicker.show(inVC: self)
       // }else{
       //    self.onError(message: "please enter isssue date".localized())
       // }
        
        
    }
    
    
    @IBAction func saveBtn(_ sender: Any) {
        if(validateTextFields()) {
//            if !expiryDateState.isOn{
//                if issueDate > expiryDate {
//                    self.onError(message: "".localized())
//                }
//            }
            
            print("saveAction")
            
            if self.cerificateNameTxtFeild.text!.count > 70{
                print("cerificateNameTxtFeild.text!.count:  \(cerificateNameTxtFeild.text!.count)")
                self.showErrorMessage(message: "Certification name may not be greater than 70 characters".localized())
                return
            }
            
            
            if selectedCertificateId == 0 {
                print("add certificate")
                let certificates = certificateObjectToPost()
                certificates.userCertificates = [CertificateObj]()
                var arr  =  [CertificateObj]()
                for item in selectedcertificates {
                    var to_date: Int?
                    if let to = item.CertificateEndDate {
                        to_date = Int(to)
                    }
                    
                    arr.append(CertificateObj(certificateName: item.certificateName!, DateOfIssue: Int(item.DateOfIssue ?? "") ?? nil, CertificateEndDate: to_date ?? nil, HasIssueDate: item.HasIssueDate ?? nil, InstitutionID: item.InstitutionID,InstitutionText: item.InstitutionText ) )
                }
                var HasIssue:Int
                if self.expiryDateState.isOn {
                    HasIssue = 1
                    expiryDate = nil
                }else{
                    HasIssue = 0
                }
                var institTxt: String?
                if self.institutionId == nil || self.institutionId == -1 {
                    institTxt = self.educatioalIstTextFeild.text ?? ""
                }
                else{
                    institTxt = nil
                }
                arr.append(CertificateObj(certificateName: cerificateNameTxtFeild.text ?? "", DateOfIssue: issueDate ?? nil, CertificateEndDate: expiryDate ?? nil, HasIssueDate: HasIssue, InstitutionID: self.institutionId,InstitutionText: institTxt))
                certificates.userCertificates.append(contentsOf: arr)
                
                let user = UserManager.sharedInstance.getUser()
                user?.userCertificates2 =  certificates
                user?.SectionID = 8
                presenter.doEditNewProfile(data: user!)
            }
        else{
            print("edit certificate")
            let certificates = certificateObjectToPost()
            certificates.userCertificates = [CertificateObj]()
            var arr  =  [CertificateObj]()
            for item in selectedcertificates {
                if item.id == selectedCertificateId {
                    item.certificateName = cerificateNameTxtFeild.text!
                    if let issueeDate = issueDate {
                        item.DateOfIssue = String(issueeDate)
                    }
                    
                    var HasIssue:Int
                    if self.expiryDateState.isOn {
                        HasIssue = 1
                        expiryDate = nil
                        item.CertificateEndDate = nil
                    }else{
                        HasIssue = 0
                        if let expiryDatee = expiryDate {
                            item.CertificateEndDate = String(expiryDatee)
                        }
                    }
                    item.HasIssueDate = HasIssue
                    
                    if self.institutionId == nil || self.institutionId == -1 {
                        item.InstitutionID = -1
                        item.InstitutionText = self.educatioalIstTextFeild.text ?? ""
                    }
                    else{
                        item.InstitutionID = institutionId
                        item.InstitutionText = nil
                    }
                }
                var to_date: Int?
                if let to = item.CertificateEndDate {
                    to_date = Int(to)
                }
                arr.append(CertificateObj(certificateName: item.certificateName!, DateOfIssue: Int(item.DateOfIssue ?? "") ?? nil, CertificateEndDate: to_date ?? nil, HasIssueDate: item.HasIssueDate ?? nil, InstitutionID: item.InstitutionID,InstitutionText: item.InstitutionText) )
            }
            certificates.userCertificates.append(contentsOf: arr)
            let user = UserManager.sharedInstance.getUser()
            user?.userCertificates2 =  certificates
            user?.SectionID = 8
            presenter.doEditNewProfile(data: user!)
        }
        }
    }
    func validateTextFields() -> Bool {
        var valid = true
        if let val = self.cerificateNameTxtFeild.text , !val.isBlank {
            print("valid")
        }else {
            print("error")
            self.cerificateNameTxtFeild.showInfo("please enter your Degree".localized())
            valid = false
        }
//        if let val = self.issueDateTxtFeild.text , !val.isBlank {
//            print("valid")
//        }else {
//            print("error")
//            self.issueDateTxtFeild.showInfo("please enter isssue date".localized())
//            valid = false
//        }
//        if !expiryDateState.isOn{
//            if let val = self.expiryDateTxt.text , !val.isBlank {
//                print("valid")
//            }else {
//                print("error")
//                self.expiryDateTxt.showInfo("please enter expiry date".localized())
//                valid = false
//            }
//        }
        
        
        
        
        return valid
    }
    @IBAction func changeExpiryDateState(_ sender: Any) {
        if self.expiryDateState.isOn {
            expityDateView.isHidden = true
            expiryDateLabel.isHidden = true
            formHeight.constant = 550
            institTop.constant = 20
            
        }else{
            institTop.constant = 122
            expityDateView.isHidden = false
            expiryDateLabel.isHidden = false
            formHeight.constant = 600
        }
    }
    @IBAction func chooseEducationIxt(_ sender: UIButton) {
        print("choose country")
        self.view.endEditing(true)
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popupViewCtrl") as? popupVCtrl {
                viewController.modalPresentationStyle = .overCurrentContext
            viewController.delegate = self
            viewController.ItemsArrayToDisplay = InstitutionsList
            self.present(viewController, animated: true, completion: nil)
        }
//        var Names = [String]()
//        Names = L102Language.isRTL ? InstitutionsList.map {$0.nameAr ?? ""} : InstitutionsList.map {$0.nameEn ?? ""}
//
//        if Names.count > 0 {
//            showSchoolSelectPopover(sender: sender, isUserCountry: true, Names: Names)
//        }
    }
    @IBAction func closePopup(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
//    func showSchoolSelectPopover(sender: UIView,isUserCountry:Bool, Names: [String]) {
//
//        // selection type as multiple with subTitle Cell
//        //let countriesNames = countryList.map {$0.localizeName}
//        let selectionMenu = RSSelectionMenu(selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
//
//            cell.textLabel?.text = name
//            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
//        }
//
//        // selected items
//        let selectedName = educatioalIstTextFeild.text ?? ""
//        var selectedItem = [String]()
//        selectedItem.append(selectedName)
//        selectionMenu.setSelectedItems(items: selectedItem) { [weak self] (text, index, selected, selectedList) in
//            if L102Language.isRTL {
//                if let selectedSchool  = self!.InstitutionsList.first(where: {$0.nameAr == text!!}) {
//                    if let selectedID = selectedSchool.id{
//                        if selectedID == -1 {
//                            self!.instituteNameBtn.isUserInteractionEnabled = false
//                            self!.dropDownIcon.isHidden = true
//                            self!.educatioalIstTextFeild.text = ""
//                            self!.institutionId = nil
//                            self!.educatioalIstTextFeild.becomeFirstResponder()
//                        }else{
//                            self!.instituteNameBtn.isUserInteractionEnabled = true
//                            self!.dropDownIcon.isHidden = false
//                            self!.educatioalIstTextFeild.text = selectedSchool.nameAr
//                            self!.educatioalIstTextFeild.showInfo("")
//                            self!.institutionId = selectedID
//                        }
//                    }
//                }
//            }
//            else{
//                if let selectedSchool  = self!.InstitutionsList.first(where: {$0.nameEn == text!!}) {
//                    if let selectedID = selectedSchool.id{
//                        if selectedID == -1 {
//                            self!.instituteNameBtn.isUserInteractionEnabled = false
//                            self!.dropDownIcon.isHidden = true
//                            self!.educatioalIstTextFeild.text = ""
//        //                    self.educatioalIstTextFeild.showInfo("")
//                            self!.institutionId = nil
//
//                        }else{
//                            self!.instituteNameBtn.isUserInteractionEnabled = true
//                            self!.dropDownIcon.isHidden = false
//                            self!.educatioalIstTextFeild.text = selectedSchool.nameEn
//                            self!.educatioalIstTextFeild.showInfo("")
//                            self!.institutionId = selectedID
//                        }
//                    }
//
//                }
//            }
//        }
//
//        // search bar
//        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
//            return Names.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
//            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
//        }
//        if L102Language.isRTL {
//            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
//        }
//        else{
//            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
//        }
//        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
//        if #available(iOS 13.0, *) {
//            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
//            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
//                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
//        } else {
//            // Fallback on earlier versions
//        }
//
//
//        // cell selection style
//        selectionMenu.cellSelectionStyle = .tickmark
//
//        // show as popover
//        // specify popover size if needed
//        // size = nil (auto adjust size)
//        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
//    }
    func setupTextFields(){
        issueDateTxtFeild.delegate = self
        issueDateTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        issueDateTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        issueDateTxtFeild.infoFontSize = 14.0
        issueDateTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        issueDateTxtFeild.activeLineWidth = 2
        issueDateTxtFeild.animationDuration = 0.5
        issueDateTxtFeild.minimumPlaceholderFontSize = 15
        issueDateTxtFeild.originalPlaceholderFontSize = 15
        issueDateTxtFeild.placeholderDuration = 0.2
        issueDateTxtFeild.placeholderColor = .darkGray
        issueDateTxtFeild.placeholder = "Issue date".localized()
        
        
        cerificateNameTxtFeild.delegate = self
        cerificateNameTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        cerificateNameTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        cerificateNameTxtFeild.infoFontSize = 14.0
        cerificateNameTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        cerificateNameTxtFeild.activeLineWidth = 2
        cerificateNameTxtFeild.animationDuration = 0.5
        cerificateNameTxtFeild.minimumPlaceholderFontSize = 15
        cerificateNameTxtFeild.originalPlaceholderFontSize = 15
        cerificateNameTxtFeild.placeholderDuration = 0.2
        cerificateNameTxtFeild.placeholderColor = .darkGray
        cerificateNameTxtFeild.placeholder = "Certification name".localized()
        
        expiryDateTxtFeild.delegate = self
        expiryDateTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        expiryDateTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        expiryDateTxtFeild.infoFontSize = 14.0
        expiryDateTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        expiryDateTxtFeild.activeLineWidth = 2
        expiryDateTxtFeild.animationDuration = 0.5
        expiryDateTxtFeild.minimumPlaceholderFontSize = 15
        expiryDateTxtFeild.originalPlaceholderFontSize = 15
        expiryDateTxtFeild.placeholderDuration = 0.2
        expiryDateTxtFeild.placeholderColor = .darkGray
        expiryDateTxtFeild.placeholder = "Expiry date".localized()
        
        educatioalIstTextFeild.delegate = self
        educatioalIstTextFeild.textColor = UIColor(hexString: "#8c8c8c")
        educatioalIstTextFeild.infoTextColor = UIColor(hexString: "#FF2600")
        educatioalIstTextFeild.infoFontSize = 13.0
        educatioalIstTextFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        educatioalIstTextFeild.activeLineWidth = 2
        educatioalIstTextFeild.animationDuration = 0.5
        educatioalIstTextFeild.minimumPlaceholderFontSize = 15
        educatioalIstTextFeild.originalPlaceholderFontSize = 15
        educatioalIstTextFeild.placeholderDuration = 0.2
        educatioalIstTextFeild.placeholderColor = .darkGray
        educatioalIstTextFeild.infoLabel.numberOfLines = 2
        educatioalIstTextFeild.placeholder = "University / Institution".localized()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension addEditCertificateVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.cerificateNameTxtFeild {
            self.cerificateNameTxtFeild.showInfo("")
        }
    }
    
}



extension addEditCertificateVCtrl :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func onError(message: String) {
        print("on error \(message)")
        self.showErrorMessage(message: message)
    }
    
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucess() {
        print("onSucess")
        let user1  = UserManager.sharedInstance.getUser()
        UserManager.sharedInstance.saveUser(user:user1!)
        self.delegate?.sendData(text: "certificate")
        self.dismiss(animated: true, completion: nil)
    }
}




extension addEditCertificateVCtrl: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print("selected date : \(date)")
        if gmDatePicker.tag == 1{
            if let to = epxiryDateObj {
                if date >= to {
                    self.showErrorMessage(message: "Expiry date must be after issue date".localized())
                    return
                }
            }
            issueDateTxtFeild.text = date.toString(dateFormat:"dd/MM/yyyy")
            issueDate = date.dateToTimeStamp()
            issueDateTxtFeild.showInfo("")
            expiryDateTxt.text = ""
            expiryDate = 0
        }else{
            if let from = issueDateObj {
                if date <= from {
                    self.showErrorMessage(message: "Expiry date must be after issue date".localized())
                    return
                }
            }
            expiryDateTxt.text = date.toString(dateFormat:"dd/MM/yyyy")
            expiryDate = date.dateToTimeStamp()
            expiryDateTxt.showInfo("")
        }
        
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker(_ type:Int) -> GMDatePicker {
        
        let datePicker = GMDatePicker()
        datePicker.delegate = self
        let calendar = Calendar.current
        //calendar.timeZone = TimeZone(identifier: "UTC")!
        var startingDate: Date? = Date()
        var minDate: Date? = Date()
       
       
        
        if type == 1 {
            
            
            datePicker.config.startDate = startingDate ?? Date()
        }else{
            if let userBirthDate = expiryDate{
                startingDate = Date(timeIntervalSince1970: Double(userBirthDate))
            }
            
            datePicker.config.startDate = startingDate ?? Date()
            
            if let userBirthDate = issueDate{
                minDate = Date(timeIntervalSince1970: Double(userBirthDate))
                
                
//                let monthsToAdd = 2
                let daysToAdd = 1
//                let yearsToAdd = 5
//                let currentDate = Date()
                var dateComponent = DateComponents()
//                dateComponent.month = monthsToAdd
                dateComponent.day = daysToAdd
//                dateComponent.year = yearsToAdd
                let futureDate = Calendar.current.date(byAdding: dateComponent, to: minDate!)
//                print("c:\(currentDate)")
                print("f:\(futureDate!)")
                
                datePicker.config.minDate =  futureDate ?? Date()
            }
            
        }
      
        
        let date_less16 = calendar.date(byAdding: .year, value: 0, to: Date())
        if type == 1{
            print("max for issue date only")
            datePicker.config.maxDate =  calendar.date(byAdding: .day, value: -1, to: date_less16!)
        }
       
        
        datePicker.config.animationDuration = 0.0
        
        datePicker.config.cancelButtonTitle = "CancelPicker".localized()
        datePicker.config.confirmButtonTitle = "Done".localized()
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor.frenchBlue
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
        return datePicker
    }
}


extension addEditCertificateVCtrl :DegreeListView {
    
    func showLoading() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        print("onSucessLoadingDegreeList")
    }
    
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
        
        if let languages = degreeWorkingTimes.Institutions {
            if languages.count > 0 {
                self.InstitutionsList.removeAll()
                self.InstitutionsList.append(contentsOf: languages)
                //self.InstitutionsList.append(institutionItem(id: -1, nameEn: "Other", nameAr: "اخرى", Active: 1))
                print("self.InstitutionsList : \(InstitutionsList)")
            }}
        
    }
}
extension addEditCertificateVCtrl: popupSendData {
    func sendData(text: String, id: Int) {
        self.institutionId = id
        self.educatioalIstTextFeild.text = text
    }
    
    
}
