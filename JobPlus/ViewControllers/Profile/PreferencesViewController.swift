//
//  PreferencesViewController.swift
//  PartTime
//
//  Created by passant on 1/3/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton

extension PreferencesViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        companiesArr = companiesAllArr
        if searchText.count > 0 {
            companiesArr = companiesArr.filter({(($0.name ?? "0").uppercased()).contains(searchText.uppercased())
            
        })
        }
                companiesTV.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

class PreferencesViewController: UIViewController {
        
    
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: TransitionButton!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var favsView: UIView!
    @IBOutlet weak var favsArrow: UIImageView!
    @IBOutlet weak var favTVHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var sectorView: UIView!
    @IBOutlet weak var sectorArrow: UIImageView!
    @IBOutlet weak var sectorsTV: UITableView!
    @IBOutlet weak var sectorsTVHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var timesView: UIView!
    @IBOutlet weak var timesArrow: UIImageView!
    @IBOutlet weak var timesTV: UITableView!
    @IBOutlet weak var timesTVHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var typesView: UIView!
    @IBOutlet weak var typesArrow: UIImageView!
    @IBOutlet weak var typesTV: UITableView!
    @IBOutlet weak var typesTVHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var companiesView: UIView!
    @IBOutlet weak var companiesTV: UITableView!
    @IBOutlet weak var compTVHeightConst: NSLayoutConstraint!
    @IBOutlet weak var citiesView: UIView!
    @IBOutlet weak var citiesTV: UITableView!
    @IBOutlet weak var citiesTVHeightConst: NSLayoutConstraint!
    @IBOutlet weak var companiesArrow: UIImageView!
    @IBOutlet weak var citiesArrow: UIImageView!
    //@IBOutlet weak var notificationCountLbl: UILabel!
    var searchController: UISearchBar!
    //MARK:- Initial values
    var itemCountForWindless = 10
    var page = 1
    var offset = 10
    var checkAll = false
    var categoriesArr = [CategoryItem]()
    var checkedCategoriesArr = [CategoryItem]()
    var sectorsArr = [workSector]()
    var checkecdSectorsArr = [workSector]()
    var timesArr = [WorkTimeItem]()
    var checkedTimesArr = [WorkTimeItem]()
    var typesArr = [JobTypeItem]()
    var checkedTypesArr = [JobTypeItem]()
    var companiesArr = [Company]()
    var companiesAllArr = [Company]()
    var checkedcompaniesArr = [Company]()
    var citiesArr = [City]()
    var checkedcitiesArr = [City]()
    let catgoriesPresenter = CatgoriesPresenter()
    let addPreferedCategoriesPresenter = PreferencesPresenter()
    var catgoriesResponse:FavoritesRes?
    var catsExpanded = false
    var sectorsExpanded = false
    var timesExpanded = false
    var typesExpanded = false
    var companiesExpanded = false
    var citiesExpanded = false
    var spinnerView: UIView?
    override func viewDidAppear(_ animated: Bool) {
        //        let notiCount = UserManager.sharedInstance.notificationCount
        //        if notiCount > 0 {
        //            self.notificationCountLbl.text = (" \(String(notiCount)) ")
        //            self.notificationCountLbl.isHidden = false
        //        }
        //        else{
        //             self.notificationCountLbl.isHidden = true
        //        }
    }
    
    //    @IBAction func notificationOpenAction(_ sender: Any) {
    //           if UserManager.sharedInstance.getUser() != nil {
    //               if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
    //
    //                   if let navigator = navigationController {
    //                       navigator.pushViewController(viewController, animated: true)
    //                   }
    //               }
    //           }
    //           else{
    //               self.showErrorMessage(message: "you have to login first".localized())
    //           }
    //       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        initPullToRefresh()
        catgoriesPresenter.attachView(view:self)
        addPreferedCategoriesPresenter.attachView(view: self)
        self.tableView.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        //get user check list
        catgoriesPresenter.getCatgories()
        self.view.changeFont()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        //setUpUI()
        
        favsView.layer.cornerRadius = 2
        favsView.layer.shadowColor = UIColor.black.cgColor
        favsView.layer.shadowOpacity = 0.2
        favsView.layer.shadowOffset = .zero
        favsView.layer.shadowRadius = 10
        
        typesView.layer.cornerRadius = 2
        typesView.layer.shadowColor = UIColor.black.cgColor
        typesView.layer.shadowOpacity = 0.2
        typesView.layer.shadowOffset = .zero
        typesView.layer.shadowRadius = 10
        
        sectorView.layer.cornerRadius = 2
        sectorView.layer.shadowColor = UIColor.black.cgColor
        sectorView.layer.shadowOpacity = 0.2
        sectorView.layer.shadowOffset = .zero
        sectorView.layer.shadowRadius = 10
        
        timesView.layer.cornerRadius = 2
        timesView.layer.shadowColor = UIColor.black.cgColor
        timesView.layer.shadowOpacity = 0.2
        timesView.layer.shadowOffset = .zero
        timesView.layer.shadowRadius = 10
        
        companiesView.layer.cornerRadius = 2
        companiesView.layer.shadowColor = UIColor.black.cgColor
        companiesView.layer.shadowOpacity = 0.2
        companiesView.layer.shadowOffset = .zero
        companiesView.layer.shadowRadius = 10
        
        citiesView.layer.cornerRadius = 2
        citiesView.layer.shadowColor = UIColor.black.cgColor
        citiesView.layer.shadowOpacity = 0.2
        citiesView.layer.shadowOffset = .zero
        citiesView.layer.shadowRadius = 10
        
        let searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: 0, width: companiesTV.layer.frame.width - 50, height: 70)
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Search here".localized()
        searchBar.sizeToFit()
        companiesTV.tableHeaderView = searchBar
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func collapseExpandFavs(_ sender: Any) {
        if catsExpanded {
            DispatchQueue.main.async {
                self.favsView.backgroundColor = .white
                self.favsArrow.image = L102Language.isRTL ? UIImage(named: "polygon1Copy5_new") : UIImage(named: "polygon1Copy4_new")
                self.favTVHeightConstrain.constant = 0
                self.mainViewHeight.constant -= CGFloat(self.categoriesArr.count * 65)
                self.catsExpanded = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.favsView.backgroundColor = UIColor(hexString: "#e6e6e6")
                self.favsArrow.image = UIImage(named: "ic_down_arrow_new")
                self.favTVHeightConstrain.constant = CGFloat(self.categoriesArr.count * 65)
                self.mainViewHeight.constant += CGFloat(self.categoriesArr.count * 65)
                self.catsExpanded = true
                self.tableView.reloadData()
            }
        }
    }
    @IBAction func collapseExpandWorkSectors(_ sender: Any) {
        if sectorsExpanded {
            DispatchQueue.main.async {
                self.sectorView.backgroundColor = .white
                self.sectorArrow.image = L102Language.isRTL ? UIImage(named: "polygon1Copy5_new") : UIImage(named: "polygon1Copy4_new")
                self.sectorsTVHeightConstrain.constant = 0
                self.mainViewHeight.constant -= CGFloat(self.sectorsArr.count * 65)
                self.sectorsExpanded = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.sectorView.backgroundColor = UIColor(hexString: "#e6e6e6")
                self.sectorArrow.image = UIImage(named: "ic_down_arrow_new")
                self.sectorsTVHeightConstrain.constant = CGFloat(self.sectorsArr.count * 65)
                self.mainViewHeight.constant += CGFloat(self.sectorsArr.count * 65)
                self.sectorsExpanded = true
                self.sectorsTV.reloadData()
            }
        }
    }
    @IBAction func collapseExpandWorkTimes(_ sender: Any) {
        if timesExpanded {
            DispatchQueue.main.async {
                self.timesView.backgroundColor = .white
                self.timesArrow.image = L102Language.isRTL ? UIImage(named: "polygon1Copy5_new") : UIImage(named: "polygon1Copy4_new")
                self.timesTVHeightConstrain.constant = 0
                self.mainViewHeight.constant -= CGFloat(self.timesArr.count * 65)
                self.timesExpanded = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.timesView.backgroundColor = UIColor(hexString: "#e6e6e6")
                self.timesArrow.image = UIImage(named: "ic_down_arrow_new")
                self.timesTVHeightConstrain.constant = CGFloat(self.timesArr.count * 65)
                self.mainViewHeight.constant += CGFloat(self.timesArr.count * 65)
                self.timesExpanded = true
                self.timesTV.reloadData()
            }
        }
    }
    @IBAction func collapseExpandJobTypes(_ sender: Any) {
        if typesExpanded {
            DispatchQueue.main.async {
                self.typesView.backgroundColor = .white
                self.typesArrow.image = L102Language.isRTL ? UIImage(named: "polygon1Copy5_new") : UIImage(named: "polygon1Copy4_new")
                self.typesTVHeightConstrain.constant = 0
                self.mainViewHeight.constant -= CGFloat(self.typesArr.count * 65)
                self.typesExpanded = false
            }
        }
        else{
            let mainViewHeight = CGFloat(self.typesArr.count * 65)
            let typesTVHeight = CGFloat(self.typesArr.count * 65)
            
            DispatchQueue.main.async {
                self.typesView.backgroundColor = UIColor(hexString: "#e6e6e6")
                self.typesArrow.image = UIImage(named: "ic_down_arrow_new")
                self.typesTVHeightConstrain.constant = typesTVHeight
                self.mainViewHeight.constant += mainViewHeight
                self.typesExpanded = true
                self.typesTV.reloadData()
            }
        }
    }
    
    @IBAction func collapseExpandCompanies(_ sender: Any) {
        if companiesExpanded {
            DispatchQueue.main.async {
                self.companiesView.backgroundColor = .white
                self.companiesArrow.image = L102Language.isRTL ? UIImage(named: "polygon1Copy5_new") : UIImage(named: "polygon1Copy4_new")
                self.compTVHeightConst.constant = 0
                self.mainViewHeight.constant -= 500
                self.companiesExpanded = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.companiesView.backgroundColor = UIColor(hexString: "#e6e6e6")
                self.companiesArrow.image = UIImage(named: "ic_down_arrow_new")
                self.compTVHeightConst.constant = 500
                self.mainViewHeight.constant += 500
                self.companiesExpanded = true
                self.companiesTV.reloadData()
            }
        }
    }
    @IBAction func collapseExpandCities(_ sender: Any) {
        if citiesExpanded {
            DispatchQueue.main.async {
                self.citiesView.backgroundColor = .white
                self.citiesArrow.image = L102Language.isRTL ? UIImage(named: "polygon1Copy5_new") : UIImage(named: "polygon1Copy4_new")
                self.citiesTVHeightConst.constant = 0
                self.mainViewHeight.constant -= CGFloat(self.citiesArr.count * 65)
                self.citiesExpanded = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.citiesView.backgroundColor = UIColor(hexString: "#e6e6e6")
                self.citiesArrow.image = UIImage(named: "ic_down_arrow_new")
                self.citiesTVHeightConst.constant = CGFloat(self.citiesArr.count * 65)
                self.mainViewHeight.constant += CGFloat(self.citiesArr.count * 65)
                self.citiesExpanded = true
                self.citiesTV.reloadData()
            }
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        // if checkedCategoriesArr.count > 0 {
        let preferredCategoriesIds = checkedCategoriesArr.map { $0.id}
        let prefreredJobTypes = checkedTypesArr.map { $0.id }
        let prefreredWorkSectors = checkecdSectorsArr.map { $0.id }
        let preferedWorkTimes = checkedTimesArr.map {$0.id}
        let preferedCompanies = checkedcompaniesArr.map {$0.id}
        let preferedCities = checkedcitiesArr.map {$0.id}
    
        self.addPreferedCategoriesPresenter.addPreferedCategories(categoriesIds: preferredCategoriesIds as! [Int],  jobTypes: prefreredJobTypes as! [Int], sectors: prefreredWorkSectors as! [Int], times: preferedWorkTimes as! [Int], companies: preferedCompanies as! [Int], cities: preferedCities as! [Int])
        //        }else {
        //            self.showErrorMessage(message: "you have to select at least one category".localized())
        //        }
    }
}

extension PreferencesViewController {
    
    func setUpUI(){
        self.title = "Preferences".localized()
    }
    
    
    func registerTableViewCell(){
        tableView.register(UINib(nibName: PreferencesTableViewCell.getCellIdentifier(), bundle: nil), forCellReuseIdentifier: PreferencesTableViewCell.getCellIdentifier())
        sectorsTV.register(UINib(nibName: PreferencesTableViewCell.getCellIdentifier(), bundle: nil), forCellReuseIdentifier: PreferencesTableViewCell.getCellIdentifier())
        timesTV.register(UINib(nibName: PreferencesTableViewCell.getCellIdentifier(), bundle: nil), forCellReuseIdentifier: PreferencesTableViewCell.getCellIdentifier())
        typesTV.register(UINib(nibName: PreferencesTableViewCell.getCellIdentifier(), bundle: nil), forCellReuseIdentifier: PreferencesTableViewCell.getCellIdentifier())
        companiesTV.register(UINib(nibName: PreferencesTableViewCell.getCellIdentifier(), bundle: nil), forCellReuseIdentifier: PreferencesTableViewCell.getCellIdentifier())
        citiesTV.register(UINib(nibName: PreferencesTableViewCell.getCellIdentifier(), bundle: nil), forCellReuseIdentifier: PreferencesTableViewCell.getCellIdentifier())
    }
    
    
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.catgoriesPresenter.getCatgories()
        }, loadingView: loadingView)
        self.tableView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.tableView?.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
    
    
}

extension PreferencesViewController:UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            if self.categoriesArr.count != 0 {
                return categoriesArr.count
            }else {
                return itemCountForWindless
            }
        case 1:
            if self.sectorsArr.count != 0 {
                return sectorsArr.count
            }else {
                return itemCountForWindless
            }
        case 2:
            if self.timesArr.count != 0 {
                return timesArr.count
            }else {
                return itemCountForWindless
            }
        case 3:
            if self.typesArr.count != 0 {
                return typesArr.count
            }else {
                return itemCountForWindless
            }
        case 4:
            if self.companiesArr.count != 0 {
                return companiesArr.count
            }else {
                return itemCountForWindless
            }
        case 5:
            if self.citiesArr.count != 0 {
                return citiesArr.count
            }else {
                return itemCountForWindless
            }
        default:
            return itemCountForWindless
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PreferencesTableViewCell.getCellIdentifier()) as! PreferencesTableViewCell
        switch tableView.tag {
        case 0:
            if self.categoriesArr.count > 0 {
                let category = self.categoriesArr[indexPath.row]
                if checkedCategoriesArr.contains(where: {$0.id == category.id!}) {
                    print("check it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    
                }else {
                    print(" uncheck it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    cell.categorySelectImgView.backgroundColor = UIColor.white
                }
                if self.checkAll && indexPath.row == 0 {
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                }
                cell.configure(data: self.categoriesArr[indexPath.row])
            }
        case 1:
            if self.sectorsArr.count > 0 {
                let sector = self.sectorsArr[indexPath.row]
                if checkecdSectorsArr.contains(where: {$0.id == sector.id!}) {
                    print("check it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    
                }else {
                    print(" uncheck it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    cell.categorySelectImgView.backgroundColor = UIColor.white
                }
                cell.configureworkSectorItem(data: self.sectorsArr[indexPath.row])
            }
        case 2:
            if self.timesArr.count > 0 {
                let time = self.timesArr[indexPath.row]
                if checkedTimesArr.contains(where: {$0.id == time.id!}) {
                    print("check it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    
                }else {
                    print(" uncheck it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    cell.categorySelectImgView.backgroundColor = UIColor.white
                }
                cell.configureworktims(data: self.timesArr[indexPath.row])
            }
        case 3:
            if self.typesArr.count > 0 {
                let time = self.typesArr[indexPath.row]
                if checkedTypesArr.contains(where: {$0.id == time.id!}) {
                    print("check it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    
                }else {
                    print(" uncheck it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    cell.categorySelectImgView.backgroundColor = UIColor.white
                }
                cell.configureJobTypeItem(data: self.typesArr[indexPath.row])
            }
        case 4:
            if self.companiesArr.count > 0 {
                let time = self.companiesArr[indexPath.row]
                if checkedcompaniesArr.contains(where: {$0.id == time.id!}) {
                    print("check it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    
                }else {
                    print(" uncheck it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    cell.categorySelectImgView.backgroundColor = UIColor.white
                }
                cell.configureCompanyItem(data: self.companiesArr[indexPath.row])
                
            }
        case 5:
            if self.citiesArr.count > 0 {
                let time = self.citiesArr[indexPath.row]
                if checkedcitiesArr.contains(where: {$0.id == time.id!}) {
                    print("check it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    
                }else {
                    print(" uncheck it")
                    cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    cell.categorySelectImgView.backgroundColor = UIColor.white
                }
                cell.configureCityItem(data: self.citiesArr[indexPath.row])
            }
        default:
            print("here")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PreferencesTableViewCell
        print("select category to attach it to user")
        switch tableView.tag {
        case 0:
            let selectedCategory = categoriesArr[indexPath.row]
            if indexPath.row == 0 && self.checkAll {
                checkedCategoriesArr.removeAll()
                for itemIndex in 0..<categoriesArr.count {
                    if tableView.cellForRow(at: IndexPath(row: itemIndex, section: 0)) != nil {
                        let item_cell = tableView.cellForRow(at: IndexPath(row: itemIndex, section: 0)) as! PreferencesTableViewCell
                        item_cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                        item_cell.categorySelectImgView.backgroundColor = .white
                    }
                }
                self.checkAll = false
            }
            else if indexPath.row == 0 && !self.checkAll {
                checkedCategoriesArr.removeAll()
                for itemIndex in 0..<categoriesArr.count {
                    print(itemIndex)
                    if categoriesArr[itemIndex].id != 0 {
                        checkedCategoriesArr.append(categoriesArr[itemIndex])
                    }
                    if tableView.cellForRow(at: IndexPath(row: itemIndex, section: 0)) != nil {
                        let item_cell = tableView.cellForRow(at: IndexPath(row: itemIndex, section: 0)) as! PreferencesTableViewCell
                        item_cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                        if categoriesArr[itemIndex].id != 0 {
                            
                        }
                        
                    }
                    self.checkAll = true
                }
            }
            else if let index = checkedCategoriesArr.index(where: {$0.id == selectedCategory.id!}) {
                print("checked category is exist and user want to unckeck it")
                checkedCategoriesArr.remove(at: index)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                if tableView.cellForRow(at: IndexPath(row: 0, section: 0)) != nil {
                    let item_cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! PreferencesTableViewCell
                    item_cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                    item_cell.categorySelectImgView.backgroundColor = .white
                    cell.categorySelectImgView.backgroundColor = .white
                }
                self.checkAll = false
                
            }else {
                print("checked behaviour is not exist and user want to ckeck it")
                checkedCategoriesArr.append(selectedCategory)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                if self.checkedCategoriesArr.count == self.categoriesArr.count - 1{
                    if tableView.cellForRow(at: IndexPath(row: 0, section: 0)) != nil {
                        let item_cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! PreferencesTableViewCell
                        item_cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                    }
                    self.checkAll = true
                }
            }
        case 1:
            let selectedSector = sectorsArr[indexPath.row]
            if let index = checkecdSectorsArr.index(where: {$0.id == selectedSector.id!}) {
                print("checked category is exist and user want to unckeck it")
                checkecdSectorsArr.remove(at: index)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                
            }else {
                print("checked behaviour is not exist and user want to ckeck it")
                checkecdSectorsArr.append(selectedSector)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                
            }
        case 2:
            let selectedTime = timesArr[indexPath.row]
            if let index = checkedTimesArr.index(where: {$0.id == selectedTime.id!}) {
                print("checked category is exist and user want to unckeck it")
                checkedTimesArr.remove(at: index)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                
            }else {
                print("checked behaviour is not exist and user want to ckeck it")
                checkedTimesArr.append(selectedTime)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                
            }
        case 3:
            let selectedType = typesArr[indexPath.row]
            if let index = checkedTypesArr.index(where: {$0.id == selectedType.id!}) {
                print("checked category is exist and user want to unckeck it")
                checkedTypesArr.remove(at: index)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                
            }else {
                print("checked behaviour is not exist and user want to ckeck it")
                checkedTypesArr.append(selectedType)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                
            }
        case 4:
            let selectedCompany = companiesArr[indexPath.row]
            if let index = checkedcompaniesArr.index(where: {$0.id == selectedCompany.id!}) {
                print("checked category is exist and user want to unckeck it")
                checkedcompaniesArr.remove(at: index)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                
            }else {
                print("checked behaviour is not exist and user want to ckeck it")
                checkedcompaniesArr.append(selectedCompany)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                
            }
        case 5:
            let selectedCity = citiesArr[indexPath.row]
            if let index = checkedcitiesArr.index(where: {$0.id == selectedCity.id!}) {
                print("checked category is exist and user want to unckeck it")
                checkedcitiesArr.remove(at: index)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "un_Selected")
                
            }else {
                print("checked behaviour is not exist and user want to ckeck it")
                checkedcitiesArr.append(selectedCity)
                cell.categorySelectImgView.image = #imageLiteral(resourceName: "group10_NEW")
                
            }
        default:
            print("")
        }
    }
}
extension PreferencesViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}
extension PreferencesViewController : CatgoriesView {
    func onSucess(catgoriesResponse: FavoritesRes) {
        self.catgoriesResponse = catgoriesResponse
        if let catArr = self.catgoriesResponse?.categories {
            self.categoriesArr.removeAll()
            self.categoriesArr.append(contentsOf: catArr)
        }
        if let sectorsArr = self.catgoriesResponse?.WorkSectors {
            self.sectorsArr.removeAll()
            self.sectorsArr.append(contentsOf: sectorsArr)
        }
        if let timesArr = self.catgoriesResponse?.worktimes {
            self.timesArr.removeAll()
            self.timesArr.append(contentsOf: timesArr)
        }
        if let typeArr = self.catgoriesResponse?.JobTypes {
            self.typesArr.removeAll()
            self.typesArr.append(contentsOf: typeArr)
        }
        if let compArr = self.catgoriesResponse?.companies {
            self.companiesArr.removeAll()
            self.companiesArr.append(contentsOf: compArr)
            self.companiesAllArr = companiesArr
        }
        if let citArr = self.catgoriesResponse?.cities {
            self.citiesArr.removeAll()
            self.citiesArr.append(contentsOf: citArr)
        }
        if let userPreferredCategories = UserManager.sharedInstance.getUser()?.categories{
            if userPreferredCategories.count == self.categoriesArr.count - 1 {
                self.checkAll = true
            }
            self.checkedCategoriesArr.append(contentsOf: userPreferredCategories)
        }
        if let userPreferedCompanies = UserManager.sharedInstance.getUser()?.companies {
            self.checkedcompaniesArr.append(contentsOf: userPreferedCompanies)
        }
        if let userPreferedCities = UserManager.sharedInstance.getUser()?.cities {
            self.checkedcitiesArr.append(contentsOf: userPreferedCities)
        }
        if let userPreferredSectors = UserManager.sharedInstance.getUser()?.WorkSectors {
            self.checkecdSectorsArr.append(contentsOf: userPreferredSectors)
        }
        if let userPreferredTimes = UserManager.sharedInstance.getUser()?.WorkTimes {
            self.checkedTimesArr.append(contentsOf: userPreferredTimes)
        }
        if let JobTypes = UserManager.sharedInstance.getUser()?.JobTypes {
            self.checkedTypesArr.append(contentsOf: JobTypes)
        }
    }
    
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
        itemCountForWindless = 0
        self.tableView.reloadData()
        self.tableView.dg_stopLoading()
    }
    
    
    
    func onError(message: String) {
        self.showErrorMessage(message:message)
    }
    
}

extension PreferencesViewController:PreferencesView{
    
    
    func showLoadingPreferencesIndicator() {
        self.saveButton.startAnimation()
    }
    
    func hideLoadingPreferencesIndicator() {
        self.saveButton.stopAnimation()
    }
    
    func onSucess() {
        self.showSucessMessage(message: "Favorite items saved successfully".localized())
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
