//
//  FullScreenVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 27/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import Kingfisher

class FullScreenVCtrl: UIViewController {

    @IBOutlet weak var imgScrollView: UIScrollView!
    @IBOutlet weak var cvImage: UIImageView!
    var Selectedimg : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        imgScrollView.maximumZoomScale = 4
        imgScrollView.minimumZoomScale = 1
        imgScrollView.delegate = self
        
        cvImage.kf.indicatorType = .activity
        let url = URL(string: Selectedimg )
        
//        let processor = ResizingImageProcessor(referenceSize: CGSize(width: 22, height: 22))
        
       cvImage.kf.setImage(
            with: url,
            placeholder: UIImage(named: " "),
            options: [
                
//                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage,
                
            ], completionHandler:
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                })

       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension FullScreenVCtrl: UIScrollViewDelegate
{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return cvImage
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if scrollView.zoomScale > 1 {
            if let img  = cvImage.image{
                let ratioW = cvImage.frame.width /  img.size.width
                let ratioH = cvImage.frame.height /  img.size.height
                
                let ratio = ratioW < ratioH ? ratioW: ratioH
                let newWidth = img.size.width * ratio
                
                let newHeight = img.size.height * ratio
                
                let conditionLeft = newWidth * scrollView.zoomScale > cvImage.frame.width
                
                let left  = 0.5 * (conditionLeft ? newWidth - cvImage.frame.width : (scrollView.frame.width - scrollView.contentSize.width ) )
                
                let conditionTop = newHeight * scrollView.zoomScale > cvImage.frame.height
                
                let top  = 0.5 * (conditionTop ? newHeight - cvImage.frame.height : (scrollView.frame.height - scrollView.contentSize.height ) )
                
                scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
                
                
                
            }
            
        }else{
            scrollView.contentInset = .zero
        }
    }
    
}
