//
//  ViewAnswersViewController.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
enum FAQ {
    case FAQ
    case viewAnswers
}
class ViewAnswersViewController: UIViewController {

    @IBOutlet weak var myAnswersTableView: UITableView!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var notificationCountLbl: UILabel!
    @IBOutlet weak var notiBtn: UIButton!
    let notificationCountPresenter = NotificationPresenter()
    var questionAnswersArr = [QuestionsAnswer]()
    var cellHeights: [IndexPath : CGFloat] = [:]
    var faqResponse:FAQResponse?
    var viewAnswersCase = FAQ.viewAnswers
    var page = 1
    var itemsDefaultForWindless = 10
    var faqPresenter = FAQPresenter()
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserManager.sharedInstance.getUser() != nil {
                    let notiCount = UserManager.sharedInstance.notificationCount
                    if notiCount > 0 {
                        self.notificationCountLbl.text = (" \(String(notiCount)) ")
                        self.notificationCountLbl.isHidden = false
                    }
                    else{
                         self.notificationCountLbl.isHidden = true
                    }
        notificationCountPresenter.getNotificationCount()
        
        }
    }
    @IBAction func notificationOpenAction(_ sender: Any) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        notificationCountPresenter.attachView(view: self)
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        self.registerCell()
        myAnswersTableView.rowHeight = UITableView.automaticDimension
        switch viewAnswersCase {
        case .FAQ:
            
            initPullToRefresh()
            //initLoadMoreToRefresh()
            self.myAnswersTableView.windless
                .apply {
                    $0.beginTime = 0
                    $0.pauseDuration = 1
                    $0.duration = 3
                    $0.animationLayerOpacity = 0.8
                    $0.direction = .left
                    
                }
                .start()
            faqPresenter.attachView(view: self)
            faqPresenter.getFAQData(page: self.page)
             //addBarButtonForMenu()
        case .viewAnswers:
            self.showEmptyView(show: questionAnswersArr.count == 0)
        }
       if UserManager.sharedInstance.getUser() != nil {
           self.notiBtn.isHidden = false
       }
        self.view.changeFont()
        
       
    }
//    func addBarButtonForMenu(){
//        let menuButton   = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_side_menu") ,  style: .plain, target: self, action:  #selector(didTapOpenMenu))
//        navigationItem.leftBarButtonItem = menuButton
//    }
    
//    @objc func didTapOpenMenu(sender: AnyObject){
//        if L102Language.currentAppleLanguage() == "ar" {
//            self.slideMenuController()?.openLeft()
//        } else {
//            self.slideMenuController()?.openRight()
//        }
//    }

    override func viewWillAppear(_ animated: Bool) {
        switch viewAnswersCase {
        case .FAQ:
            self.title = "FAQ".localized()
        case .viewAnswers:
            self.title = "My Answers".localized()
             //self.addBellRightButton()
        }
        
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    func registerCell() {
        
        myAnswersTableView.register(UINib(nibName: "MyAnswersTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAnswersTableViewCell")
    }
    
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        myAnswersTableView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.page = 1
            self?.faqPresenter.getFAQData(page: (self?.page)!)
            }, loadingView: loadingView)
        self.myAnswersTableView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.myAnswersTableView?.dg_setPullToRefreshBackgroundColor(myAnswersTableView.backgroundColor!)
    }
    
//    func initLoadMoreToRefresh()
//    {
//        self.myAnswersTableView?.infiniteScrollIndicatorStyle = .gray
//
//        self.myAnswersTableView?.setShouldShowInfiniteScrollHandler { _ -> Bool in
//            return self.faqResponse?.links?.next != nil
//        }
//        // Add infinite scroll handler
//        self.myAnswersTableView?.addInfiniteScroll { (tableView) -> Void in
//            if self.faqResponse?.links?.next != nil {
//                self.page = self.page + 1
//                self.faqPresenter.getFAQData(page: self.page)
//            }else {
//                self.myAnswersTableView.finishInfiniteScroll()
//            }
//        }
//
//    }
    func showEmptyView(show: Bool){
        
        if show {
            self.emptyView.alpha = 1
            self.myAnswersTableView.alpha = 0
        }else{
            self.myAnswersTableView.alpha = 1
            self.emptyView.alpha = 0
        }
    }
}
extension ViewAnswersViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if questionAnswersArr.count > 0 {
            return questionAnswersArr.count
        }else {
            return itemsDefaultForWindless
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAnswersTableViewCell") as! MyAnswersTableViewCell
        if questionAnswersArr.count > 0 {
            cell.viewAnswersCase = self.viewAnswersCase
            cell.quesAnswer = questionAnswersArr[indexPath.row]
            
            cell.questionNumberLabel.text = "\(indexPath.row + 1)"
            
        }
        return cell
    }
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return 170.0 }
        return height
    }
}
extension ViewAnswersViewController:FAQView{
    func showLoadingIndicator() {
       // self.myAnswersTableView.windless.start()
    }
    
    func hideLoadingIndicator() {
        itemsDefaultForWindless = 0
        self.showEmptyView(show: questionAnswersArr.count == 0)
        self.myAnswersTableView?.reloadData()
        self.myAnswersTableView.windless.end()
        self.myAnswersTableView?.dg_stopLoading()
        self.myAnswersTableView.finishInfiniteScroll()
      
    }
    
    func onSucessLoading(faqResponse: FAQResponse) {
        itemsDefaultForWindless = 0
        if page == 1 {
            self.questionAnswersArr.removeAll()
        }
        self.faqResponse = faqResponse
        if let faqArr = faqResponse.data {
            if !(faqArr.count == 0) {
                
                self.questionAnswersArr.append(contentsOf: faqArr)
                self.myAnswersTableView.reloadData()
                
            }
        }
        self.showEmptyView(show: questionAnswersArr.count == 0)
        self.myAnswersTableView?.reloadData()
        
        
    }
    
    func onError(message: String) {
        itemsDefaultForWindless = 0
        self.showErrorMessage(message: message)
        self.showEmptyView(show: questionAnswersArr.count == 0)
        self.myAnswersTableView?.reloadData()
       
    }
    
    
}
extension ViewAnswersViewController: NotificationView{
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
    
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationCountLbl.text = (" \(String(notificationCount)) ")
            self.notificationCountLbl.isHidden = false
        }
        else{
            self.notificationCountLbl.isHidden = true
        }
    }
    
    
}
