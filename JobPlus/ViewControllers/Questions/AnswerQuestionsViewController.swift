//
//  AnswerQuestionsViewController.swift
//  PartTime
//
//  Created by passant on 8/28/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import GTProgressBar
import KMPlaceholderTextView
import UITextView_Placeholder
import TransitionButton
class AnswerQuestionsViewController: UIViewController {

    @IBOutlet weak var yourAnswerLabel: UILabel!
    @IBOutlet weak var noOfCharacters: UILabel!
    @IBOutlet weak var validationLabel: UILabel!
    @IBOutlet weak var prevQuesBtn: UIButton!
    @IBOutlet weak var nextQuesBtn: TransitionButton!
    //  @IBOutlet weak var answerTextView: KMPlaceholderTextView!
    @IBOutlet weak var answerTextView: UITextView!
    @IBOutlet weak var questionProgressBar: GTProgressBar!
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var questionProgressLabel: UILabel!
    @IBOutlet weak var questionNumberLabel: UILabel!
    @IBOutlet weak var questionTitleView: UIView!
    var spinnerView: UIView?
    let questionsListPresenter = AnswerQuestionsPresenter()
    var questionListArr = [Question]()
    var questionIndex = 0
    var progressValue:Float = 0.0
    var questionAnswersArr = [QuestionsAnswer]()
    var questionAnswerRequestObject = QuestionListRequest()
    var currentAnswer = QuestionsAnswer()
    var currentQuestion = Question()
    var job:Job?
    var applyToJobPresenter = ApplyToJobPresenter()
    var from:String = ""
    var titleMsg:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.applyToJobPresenter.attachView(view: self)
        // Do any additional setup after loading the view.
        self.questionsListPresenter.attachView(view: self)
        if let id = job?.id {
        self.questionsListPresenter.getQuestionsList(jobId: id)
        }
        self.setUpUI()
        self.view.changeFont()
        
    }

    override func viewDidLayoutSubviews() {
       // self.setUpUI()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if L102Language.currentAppleLanguage() == "ar" {
            self.answerTextView.placeholder = "اجب هنا"
        }else {
            self.answerTextView.placeholder = "Describe here"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Questions".localized()
        //self.addBellRightButton()
    }
    
    func setUpUI(){
        //self.questionTitleView.dropShadow()
        let questionTitleshadowView = UIView()
        //questionTitleshadowView.dropShadow()
        self.questionTitleView.addSubview(questionTitleshadowView)
        view.bringSubviewToFront(self.questionTitleView)
        
        let shadowView = UIView()
        //shadowView.dropShadow()
        self.questionNumberLabel.addSubview(shadowView)
        view.bringSubviewToFront(self.questionNumberLabel)
        
        self.answerTextView.delegate = self
        self.answerTextView.layer.borderWidth = 1.0
        self.answerTextView.layer.borderColor = UIColor.silver.cgColor
        self.answerTextView.layer.cornerRadius = 5
        
    }
    
    func displayData(){
        print(questionIndex)
        self.validationLabel.alpha = 0
        self.answerTextView.isUserInteractionEnabled = true
        if questionIndex < questionListArr.count {
            self.questionNumberLabel.text = "\(questionIndex + 1)"
            self.questionProgressLabel.text = "\(questionIndex + 1)" + " " + "from".localized() +  " " +  "\(questionListArr.count)"
        }else if questionIndex == questionListArr.count{
            self.questionNumberLabel.text = "\(questionIndex)"
            self.questionProgressLabel.text = "\(questionIndex)" + " " + "from".localized() +  " " +  "\(questionListArr.count)"
        }
        self.questionProgressBar.animateTo(progress: CGFloat(Float(questionIndex) / Float(self.questionListArr.count)))
        
        
        if questionIndex == 0 {
            
            
            self.prevQuesBtn.isEnabled = false
            self.nextQuesBtn.setTitle("Next".localized(), for: .normal)
        }
        else if (questionIndex  < self.questionListArr.count) && (questionIndex > 0 ){
            self.prevQuesBtn.isEnabled = true
            self.nextQuesBtn.setTitle("Next".localized(), for: .normal)
        }else if questionIndex == self.questionListArr.count {
//            self.prevQuesBtn.isEnabled = true
//            self.answerTextView.isUserInteractionEnabled = false
//            self.nextQuesBtn.setTitle("Apply".localized(), for: .normal)
            print("Done")
            self.questionAnswerRequestObject.questionsAnswers = self.questionAnswersArr
            self.applyToJobPresenter.applyToJob(questionAnswersObj: self.questionAnswerRequestObject, jobId: (self.job?.id)!)
        }
        if questionIndex < self.questionListArr.count {
            currentQuestion = self.questionListArr[questionIndex ]
            if (currentQuestion.name?.containsOnlyEnglishLetters())! {
                self.questionTitleLabel.textAlignment = .left
            }else {
                self.questionTitleLabel.textAlignment = .right
            }
            self.questionTitleLabel.text = currentQuestion.name
            
            if let row = self.questionAnswersArr.index(where: {$0.id == currentQuestion.id}) {
                if let answer = questionAnswersArr[row].answer {
                self.answerTextView.text = answer
                }
            }else {
                //self.answerTextView.text = ""
            }
            
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    @IBAction func nextQuesBtnAction(_ sender: Any) {
        
        self.noOfCharacters.text = "240"
        self.answerTextView.isUserInteractionEnabled = true
        if questionIndex == self.questionListArr.count {
            print("Done")
            self.questionAnswerRequestObject.questionsAnswers = self.questionAnswersArr
            
//            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//            let jobDetailsVC = storyBoard.instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
//            jobDetailsVC.jobDetailsStatus = .applytojob
//            jobDetailsVC.job = self.job
//            jobDetailsVC.applyForJobRequestObj = self.questionAnswerRequestObject
//
//            self.navigationController?.pushViewController(jobDetailsVC, animated: true)
//
//
            
            
            self.applyToJobPresenter.applyToJob(questionAnswersObj: self.questionAnswerRequestObject, jobId: (self.job?.id)!)

            
        }else {
            print("next")
            let questionAnswerObj = QuestionsAnswer()
            questionAnswerObj.question = currentQuestion.name
            questionAnswerObj.id = currentQuestion.id
            
            if self.answerTextView.text.isBlank {
                self.validationLabel.alpha = 1
                self.validationLabel.text = "you have to answer the question before go to next".localized()
            }else {
                self.validationLabel.alpha = 0
                if let row = self.questionAnswersArr.index(where: {$0.id == currentQuestion.id}) {
                   questionAnswersArr[row].answer = self.answerTextView.text
                }else {
                    
                    
                    questionAnswerObj.answer = self.answerTextView.text
                    
                    self.questionAnswersArr.append(questionAnswerObj)
                   // self.answerTextView.text = ""
                }
                self.currentAnswer.answer = questionAnswerObj.answer
                
                questionIndex += 1
                if questionIndex == self.questionListArr.count {
                }else {
                    self.answerTextView.text = ""
                }
                self.displayData()
              //  self.answerTextView.text = ""
               
            }
        }
        
    }
    
    @IBAction func prevQuesBtnAction(_ sender: Any) {
        
       
            print("prev")
            questionIndex -= 1
            self.displayData()
       
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CongratulationsViewController" {
            if let destinationVC = segue.destination as? CongratulationsViewController {
                
                destinationVC.from = self.from
                destinationVC.titleMsg = self.titleMsg
                
                
                
            }
            
        }
    }
}
extension AnswerQuestionsViewController:AnswerQuestionsView{
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess(questionsListResponse: QuestionListResponse) {
        if let questionArr = questionsListResponse.data {
            self.questionListArr = questionArr
            print("questionListArr = \(questionListArr)")
            self.displayData()
        }
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}

extension AnswerQuestionsViewController:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.noOfCharacters.text = "\(240 - textView.text.count)"
        
     return textView.text.count - range.length + text.count <= 240
    }
}

extension AnswerQuestionsViewController:ApplyToJobView{
    func showLoadingApplyIndicator() {
        self.view.isUserInteractionEnabled = false
        self.nextQuesBtn.startAnimation()
    }
    
    func hideLoadingApplyIndicator() {
        self.view.isUserInteractionEnabled = true
        self.nextQuesBtn.stopAnimation()
    }
    
//    func onSucessApply(status:String) {
    func onSucessApply(status:String,message:String){
        if status == "1"{
            UserManager.sharedInstance.jobAppliedOrCanceled = 1
            from = "jobDetailsVC"
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
            
        }else if status == "-19"{
            from = "status-19"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }else if status == "20"{
            from = "profileNotFilled"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        else if status == "30"{
            from = "status-30"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        else{
            from = "status-1"
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
    }
    
    func onErrorApply(message: String) {
        if message == "401" {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                    viewController.modalPresentationStyle = .overCurrentContext
                    viewController.from = "401"
                    self.present(viewController, animated: true, completion: nil)
            }
        }
        else{
            self.showErrorMessage(message: message)
        }

    }
}
