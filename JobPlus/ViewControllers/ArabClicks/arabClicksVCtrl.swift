//
//  arabClicksVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 01/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import TransitionButton
import Kingfisher

struct arabClickItem {
    var title:String
    var imagePath:String
}

class arabClicksCVCell: UICollectionViewCell {
    
    @IBOutlet weak var arabClickTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var arabClickTitle: UILabel!
    @IBOutlet weak var arabClickImage: UIImageView!
    @IBOutlet weak var arabClickBtn: TransitionButton!
    
}

class arabClicksVCtrl: UIViewController,CategoryAndMarketsDelegate {
    
    
    
    @IBOutlet weak var emptyView: EmptyView!
    
    @IBOutlet weak var marketLbl: UILabel!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var categoryLbl: UILabel!
    
    @IBOutlet weak var arabsClicksCV: UICollectionView!
    
    let arabClickPresenter = arabClicksPresenter()
    
    var from:String = ""
    var countryID:Int = 0
    var categoryID:Int = 0

    var categoryData:[arabClickCategoryItem] = []
    var marketData:[arabClickMarketsItem] = []
    var arabclicksOffersData:[arabClickOfferItem] = []
    
    var arabClickName:String = ""
    var arabClickDes:String = ""
    var arabClickPromoCode:String = ""
    var arabClickURL:String = ""
    var refreshControl = UIRefreshControl()
    var spinnerView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        arabsClicksCV.delegate = self
        arabsClicksCV.dataSource = self
        
        arabClickPresenter.attachView(view: self)
//        arabClickPresenter.doGetarabClicksOffers(countryID: countryID, categoryID: categoryID)
        arabClickPresenter.doGetCategoriesAndMarkets()
        
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        
        self.emptyView.msgLbl.text = "No Offers".localized()
        initPullToRefresh()
        
        
    }
    func initPullToRefresh()
    {
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            arabsClicksCV.refreshControl = refreshControl
        } else {
            arabsClicksCV.addSubview(refreshControl)
        }
    }
    @objc func refresh(sender:AnyObject)
      {
        refreshControl.endRefreshing()
//        self.page = 1
        arabClickPresenter.doGetarabClicksOffers(countryID: countryID, categoryID: categoryID)
//        self.loadJobsData()
//        if UserManager.sharedInstance.getUser() != nil {
//            notificationCountPresenter.getNotificationCount()
//        }
      }
    func sendData(id: Int, text: String, from: String) {
        print("from:\(from) ,, text:\(text)")
        
        if from == "category"{
            categoryID = id
            categoryLbl.text = " " + text + " "
        }else if from == "markets"{
            countryID = id
            marketLbl.text = " " + text + " "
        }
        self.emptyView.alpha = 0
        arabClickPresenter.doGetarabClicksOffers(countryID: countryID, categoryID: categoryID)
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        from = "category"
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "categoryMarket", sender: self)
        }

    }
    @IBAction func marketAction(_ sender: Any) {
        from = "markets"
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "categoryMarket", sender: self)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "categoryMarket" {
            if let destnationViewcontroller = segue.destination as? CategoryAndMarketsVCtrl {
                destnationViewcontroller.from = from
                destnationViewcontroller.marketData = marketData
                destnationViewcontroller.categoryData = categoryData
                destnationViewcontroller.delegate = self
            
        }}else if segue.identifier == "arabClicksDetailsSeg" {
            if let destnationViewcontroller = segue.destination as? ArabClicksDetailsVCtrl {
                
                destnationViewcontroller.arabClickName = arabClickName
                destnationViewcontroller.arabClickDes = arabClickDes
                destnationViewcontroller.arabClickPromoCode = arabClickPromoCode
                destnationViewcontroller.arabClickURL = arabClickURL
            }}
        
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
extension arabClicksVCtrl: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arabclicksOffersData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = arabsClicksCV.dequeueReusableCell(withReuseIdentifier: "arabsClickCell", for: indexPath) as! arabClicksCVCell
        
        let arabClickItem = self.arabclicksOffersData[indexPath.row]
        
        cell.arabClickTitle.text = arabClickItem.name
        
        cell.arabClickTitleHeight.constant = arabClickItem.name!.height(constraintedWidth: cell.arabClickTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!)
        
                cell.arabClickImage.kf.indicatorType = .activity
                let url = URL(string: (arabClickItem.arabclickImage ?? "")!)
        
                let processor = DownsamplingImageProcessor(size: cell.arabClickImage.size)
                    |> RoundCornerImageProcessor(cornerRadius: 22)
        
                cell.arabClickImage.kf.setImage(
                    with: url,
                    placeholder: UIImage(named: "jpLogo_splash"),
                    options: [
                        .processor(processor),
                        .scaleFactor(UIScreen.main.scale),
                        .transition(.fade(1)),
                        .cacheOriginalImage
                    ], completionHandler:
                        {
                            result in
                            switch result {
                            case .success(let value):
                                print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            case .failure(let error):
                                print("Job failed: \(error.localizedDescription)")
                            }
                        })
        //        cell.mesasgeImg.layer.borderWidth=1.0
                cell.arabClickImage.layer.masksToBounds = false
        //        cell.mesasgeImg.layer.borderColor = UIColor.whiteColor().CGColor
                cell.arabClickImage.layer.cornerRadius = cell.arabClickImage.frame.size.height/2
                cell.arabClickImage.clipsToBounds = true
        

            cell.arabClickBtn.tag = indexPath.row
            cell.arabClickBtn.addTarget(self, action: #selector(getItAction(_:)), for: .touchUpInside)

        return cell
    }
    @objc func getItAction(_ sender: UIButton){
        
        
        if UserManager.sharedInstance.getUser() == nil {
            self.showErrorMessage(message: "you have to login first".localized())
        }
        else{
         arabClickName = arabclicksOffersData[sender.tag].name!
          arabClickDes = arabclicksOffersData[sender.tag].description!
          arabClickPromoCode = arabclicksOffersData[sender.tag].arabclickPromoCode!
          arabClickURL = arabclicksOffersData[sender.tag].preview_url!
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "arabClicksDetailsSeg", sender: self)
            }
            
           

            

        }

    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let sectorID = homeCats[indexPath.row].id
//        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobsListViewCtrl") as? JobsListVCtrl {
//            viewController.SectorID = sectorID ?? 0
//            viewController.sectorName = (L102Language.isRTL ? homeCats[indexPath.row].nameAr : homeCats[indexPath.row].name) ?? ""
//            if let navigator = navigationController {
//                navigator.pushViewController(viewController, animated: true)
//            }
//        }
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        print("collection view size")

        var collectionViewSize = collectionView.frame.size
        let collectionViewWidth = collectionViewSize.width/2.0 - 5 //Display 2 elements in a row.
        collectionViewSize.width = collectionViewWidth
        print("width : \(collectionViewSize.width)")
        collectionViewSize.height = 150 +  ((self.arabclicksOffersData[indexPath.row].name)!.height(constraintedWidth: collectionViewWidth, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 17.0)!) - 30)
        return collectionViewSize
    }
}
extension arabClicksVCtrl :arabClicksView {
    
    
    func showLoadingIndicator() {
//        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
//        self.view.isUserInteractionEnabled = true
     print("hide loading")
        if let sv = spinnerView{
            print("loading exist")
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    func onSucessGetCategoriesAndMarkets(data: categoriesAndMarketsRes) {
        print("cat_Markets data:\(data)")
        if data.status == true {
            if (data.categories?.count)! > 0 {
                self.categoryData = data.categories!
                if let indexx = self.categoryData.firstIndex(where: {$0.IsDefault == 1})
                {
                    self.categoryID = self.categoryData[indexx].arabclickcategoryID ?? 0
                    categoryLbl.text = " " + (self.categoryData[indexx].name ?? "")  + " "
                }
                else{
                    self.categoryID = 0
                    categoryLbl.text = " " + "All".localized()  + " "
                }
            }
            
            
            if (data.Markets?.count)! > 0 {
                self.marketData = data.Markets!
                self.countryID = self.marketData[0].arabclickcountryID!
                self.marketLbl.text = self.marketData[0].name
                self.arabClickPresenter.doGetarabClicksOffers(countryID: countryID, categoryID: categoryID)
            }
            
        }
        
        
    }
    
    func onSucessGetarabClicksOffers(data: arabClicksOffersRes){
        print("onSucessGetarabClicksOffers :\(data)")
        if data.status == true {
            if data.Offers!.count > 0 {
                self.arabclicksOffersData = data.Offers!
                self.arabsClicksCV.reloadData()
                self.arabsClicksCV.alpha = 1
                self.emptyView.alpha = 0
               
         
            }else{
                self.arabclicksOffersData = []
                self.arabsClicksCV.reloadData()
                print("no offers")
                self.arabsClicksCV.alpha = 0
                self.emptyView.alpha = 1
                
               
            }
//            self.arabclicksOffersData = data.Offers!
//            DispatchQueue.main.async {
               
//            }
        }
    }
//    func onSucessGetAllMessages(data: allMessagesApiRes) {
//        print("onSucessGetAllMessages : \(data)")
//        if let messages = data.data {
//
//            MessagesDate = messages
//
//            if MessagesDate.count == 0 {
//                DispatchQueue.main.async {
//                    self.messagesTableView.alpha = 0
//                    self.emptyView.alpha = 1
//                }
//            }else{
//                DispatchQueue.main.async {
//                    self.messagesTableView.alpha = 1
//                    self.emptyView.alpha = 0
//                }
//            }
//
//        }
//    }
//
    
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
    
}
