//
//  CategoryAndMarketsVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 02/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
protocol CategoryAndMarketsDelegate {
    func sendData(id:Int,text:String,from:String)
}
class CategoryAndMarketsVCtrl: UIViewController,  UITableViewDelegate , UITableViewDataSource {
    
    var delegate: CategoryAndMarketsDelegate?
    
    
    @IBOutlet weak var catAndMarketTableTop: NSLayoutConstraint!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var categoryData:[arabClickCategoryItem] = []
    var marketData:[arabClickMarketsItem] = []
    var from:String = ""
    var searching = false
    var searchCategory = [String]()
    var CategoryArrayToDisplay = [String]()
    var selectedCategoryIds: [Int] = []
    
    
    @IBOutlet weak var popupMainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var popupTableVIew: UITableView!
    
    @IBOutlet weak var popupTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popupTableVIew.delegate = self
        popupTableVIew.dataSource = self
        popupTableVIew.tableFooterView = UIView()
        
        self.searchBar.placeholder = "Search here".localized()
       
        if from == "markets"{
            popupTitle.text = "marketArabCLicks".localized()
            self.searchBar.isHidden = true
            popupMainViewHeight.constant = CGFloat(( marketData.count * 90 ) + 40)
            
            catAndMarketTableTop.constant = 0
            
        }else if from == "category"{
            popupTitle.text = "categoryArabClicks".localized()
            self.searchBar.isHidden = false
            self.searchBar.delegate = self
            catAndMarketTableTop.constant = 56
            
            CategoryArrayToDisplay = categoryData.map {($0.name ?? "")}
                
//                L102Language.isRTL ? softskillsList.map {($0.nameAr ?? "")} : softskillsList.map {($0.nameEn ?? "")}
        }
        
        
       
    }
    

    @IBAction func closePopUp(_ sender: Any) {
        print("close pop up ")
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if from == "markets"{
            return marketData.count
        }else if from == "category"{
            if searching{
                return searchCategory.count
            }else{
                return categoryData.count
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryMarketCell", for: indexPath) as! skillstableViewCell
        if from == "markets"{
            let item = self.marketData[indexPath.row]
            cell.skillTitle.text =  item.name
        }else if from == "category"{
            
            if searching{
                cell.skillTitle.text = searchCategory[indexPath.row]
            }else{
                let item = self.categoryData[indexPath.row]
                cell.skillTitle.text =  item.name
            }
            
            
        }
        return cell
        
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if from == "markets"{
           
            self.delegate?.sendData(id: marketData[indexPath.row].arabclickcountryID ?? 0, text: marketData[indexPath.row].name ?? "" , from: from)
            self.dismiss(animated: true, completion: nil)
        }else if from == "category"{
            var catId:Int = -1
            var catName:String = ""
            if searching{
               
                catName = searchCategory[indexPath.row]
                
                for item in categoryData{
                    if item.name == catName {
                        catId = item.arabclickcategoryID ?? 0
                    }
                }
               
            }else{
                
               
                catName = categoryData[indexPath.row].name ?? ""
                catId = categoryData[indexPath.row].arabclickcategoryID ?? 0
            }
            
            
            self.delegate?.sendData(id: catId, text: catName, from: from)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoryAndMarketsVCtrl:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            searchCategory = CategoryArrayToDisplay.filter({($0.uppercased()).contains(searchText.uppercased())
            
        })
        }
        else{
            searchCategory = CategoryArrayToDisplay.filter({$0.prefix(searchText.count) == searchText })
        }
        searching = true
        popupTableVIew.reloadData()
    }
}
