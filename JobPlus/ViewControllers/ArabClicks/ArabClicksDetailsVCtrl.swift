//
//  ArabClicksDetailsVCtrl.swift
//  JobPlus
//
//  Created by samar nasef on 03/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit

class ArabClicksDetailsVCtrl: UIViewController {

    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var url: UILabel!
    @IBOutlet weak var promoCode: UILabel!
    @IBOutlet weak var arabClickDescription: UITextView!
    @IBOutlet weak var arabClickTitle: UILabel!
    
    
    var arabClickName:String = ""
    var arabClickDes:String = ""
    var arabClickPromoCode:String = ""
    var arabClickURL:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arabClickTitle.text = arabClickName
        arabClickDescription.attributedText = arabClickDes.htmlToAttributedString
//        arabClickDescription.isUserInteractionEnabled = true
//        print("arabClickDes : \(arabClickDes)")
        promoCode.text = arabClickPromoCode
        url.text = arabClickURL
        
        promoCode.isUserInteractionEnabled = true
        let promoCodetap = UITapGestureRecognizer.init(target: self, action: #selector(copyPromoCode))
        promoCodetap.numberOfTapsRequired = 1
        promoCode.addGestureRecognizer(promoCodetap)
        
        url.isUserInteractionEnabled = true
        let urltap = UITapGestureRecognizer.init(target: self, action: #selector(openUrlFunction))
        urltap.numberOfTapsRequired = 1
        url.addGestureRecognizer(urltap)
        
    
        descriptionHeight.constant = arabClickDescription.text.height(constraintedWidth:  arabClickDescription.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 10.4)!) + 10
        
        print("descriptionHeight.constant :\(descriptionHeight.constant)")
        mainViewHeight.constant = 100 + descriptionHeight.constant
        
        
    }
    @objc func copyPromoCode(sender:UITapGestureRecognizer)
    {
        
        let labeltext = promoCode.text
        UIPasteboard.general.string = labeltext
        self.showSucessMessage(message: "Copied to clipboard".localized())

    }
    @objc func openUrlFunction(sender:UITapGestureRecognizer)
    {
        
        guard let url = URL(string: arabClickURL) else { return }
        UIApplication.shared.open(url)

    }

    @IBAction func closePopup(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

