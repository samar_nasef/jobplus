//
//  subscriptionsVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/1/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TransitionButton
import SwiftyStoreKit

class SubscriptionsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var note: UILabel!
    
}

class subscriptionsVCtrl: UIViewController {
    
    @IBOutlet weak var iapHederLbl: UILabel!
    @IBOutlet weak var packagesCV: UICollectionView!
    @IBOutlet weak var iapViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var iapView: UIView!
    @IBOutlet weak var iapLbl: UILabel!
    @IBOutlet weak var iapBtn: UIButton!
    var inAppPurchaseStatus = 0
    @IBOutlet weak var dateFixedText: UILabel!
    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var sNowBtn: TransitionButton!
    @IBOutlet weak var whiteViewHeight: NSLayoutConstraint!
    @IBOutlet weak var msgView: UIView!
    @IBOutlet weak var WhiteView: UIView!
    @IBOutlet weak var subDate: UILabel!
    @IBOutlet weak var subNotes: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    let notificationCountPresenter = NotificationPresenter()
    let subsPresenter = SubscribtionPresenter()
    var itemCountForWindless = 10
    var selectedPackage:Int = -1
    var selectedPackageCost:String = ""
    var selectedPackageTitle:String = ""
    var packagesDataarr:[packagesObj] = []
    var userPackageID = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.iapViewHeightConstraint.constant = 0
            self.iapView.isHidden = true
        }
        
        notificationCountPresenter.attachView(view: self)
        subsPresenter.attachView(view: self)
        
        if UserManager.sharedInstance.getUser() != nil {
            self.notificationBtn.isHidden = false
        }
        
        
        
        //let backgroundImage = UIImageView(frame: self.WhiteView.bounds)
        
        //backgroundImage.image = UIImage(named: "vectorSmartObject1")
        //backgroundImage.contentMode =  .scaleToFill
        //self.WhiteView.insertSubview(backgroundImage, at: 0)
        
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            
            
//            let backgroundImage = UIImageView(frame: self.msgView.bounds)
//
//            backgroundImage.image = UIImage(named: "vectorSmartObject-2")
//            backgroundImage.contentMode =  .scaleToFill
//
//            self.msgView.insertSubview(backgroundImage, at: 0)
            
            
        }else{
            
//            let backgroundImage = UIImageView(frame: self.msgView.bounds)
//
//            backgroundImage.image = UIImage(named: "englishBox")
//            backgroundImage.contentMode =  .scaleToFill
//
//            self.msgView.insertSubview(backgroundImage, at: 0)
        }
        
        
        self.sNowBtn.isHidden = true
        self.WhiteView.isHidden = true
        self.dateFixedText.isHidden = true
        self.whiteViewHeight.constant = 0
        WhiteView.layer.cornerRadius = 10
    }
    
    @IBAction func iapAction(_ sender: Any) {
        
        SwiftyStoreKit.purchaseProduct("com.adroyts.jobplus.nonRenewableSubscription", quantity: 1, atomically: false) { result in
            switch result {
            case .success(let product):
                // fetch content from your server, then:
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
                UserDefaults.standard.set(product.transaction.transactionIdentifier ?? "",forKey: "pendingTransactioniD")
                UserDefaults.standard.set("\(Date())", forKey: "TransactionDate")
                
                print("Purchase Success: \(product.productId) transaction id: \(product.transaction.transactionIdentifier ?? "")")
                //self.subsPresenter.addIAPTransaction(transaction_id: pendingTransactioniD, packageID: UserDefaults.standard.integer(forKey: "selectedPackage"), paymentBrand: 4)
                
            case .error(let error):
                switch error.code {
                case .unknown: print("Unknown error. Please contact support")
                case .clientInvalid: print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid: print("The purchase identifier was invalid")
                case .paymentNotAllowed: print("The device is not allowed to make the payment")
                case .storeProductNotAvailable: print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserManager.sharedInstance.getUser() != nil {
            let notiCount = UserManager.sharedInstance.notificationCount
            if notiCount > 0 {
                self.notificationCount.text = (" \(String(notiCount)) ")
                self.notificationCount.isHidden = false
            }
            else{
                self.notificationCount.isHidden = true
            }
            notificationCountPresenter.getNotificationCount()
            
        }
        if UserManager.sharedInstance.subscribtionURL != "" {
            self.performSegue(withIdentifier: "openSubscribtionSeg", sender: nil)
        }
        userPackageID = -1
        selectedPackage = -1
        subsPresenter.getPackages()
        SwiftyStoreKit.retrieveProductsInfo(["com.adroyts.jobplus.nonRenewableSubscription"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                var iapTitle = product.localizedTitle
                if L102Language.isRTL, iapTitle == "30 Days" {
                    iapTitle = "٣٠ يوم"
                }
                print("Product: \(product.localizedDescription), price: \(priceString)")
                self.iapLbl.text = iapTitle
                self.iapBtn.setTitle("\(priceString)", for: .normal)
                self.iapBtn.setTitle("\(priceString)", for: .selected)
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error!)")
            }
        }
        
    }
    
    
    @IBAction func goback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @objc func subscribeNow(_ btn: UIButton){
        print("packageId \(btn.tag)")
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentInfoVController") as? PaymentInfoVC {
            viewController.PackageId = btn.tag
            viewController.PackageCost = selectedPackageCost
            viewController.PackageTitle = selectedPackageTitle
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    
    
    
    @IBAction func sNowClicked(_ sender: TransitionButton) {
        
        print("selectedPckage:  \(selectedPackage)")
        
        if selectedPackage == -1 {
            print("no package selected ")
            self.showErrorMessage(message:"Please select subscription package".localized())
            
        }
        else{
            print("package selected : \(selectedPackage) ")
            subsPresenter.getHyperPayURL(package_id: selectedPackage, amount: selectedPackageCost, payment_brand: 0)
            
            
        }
        
    }
    
    
    
    
    @IBAction func openNotification(_ sender: UIButton) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
        
    }
    
    
    
}

extension subscriptionsVCtrl: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
           let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
           let size:CGFloat = (packagesCV.frame.size.width - space) / 2.0
           return CGSize(width: size, height: 70)
       }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if inAppPurchaseStatus == 1 {
            iapView.isHidden = false
            iapLbl.isHidden = false
            iapBtn.isHidden = false
            iapHederLbl.isHidden = false
            iapViewHeightConstraint.constant = 100
        }
        else{
            iapView.isHidden = true
            iapLbl.isHidden = true
            iapBtn.isHidden = true
            iapHederLbl.isHidden = true
            iapViewHeightConstraint.constant = 0
        }
        return packagesDataarr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = packagesCV.dequeueReusableCell(withReuseIdentifier: "packagesCell", for: indexPath) as! SubScribtionPackagesCVCell
        
        let item = self.packagesDataarr[indexPath.row]
        cell.whiteView.backgroundColor = .white
        if selectedPackage != packagesDataarr[indexPath.row].id {
            cell.whiteView.borderWidth = 1
            cell.whiteView.borderColor = .white
        }
        else{
            cell.whiteView.borderWidth = 1
            cell.whiteView.borderColor = UIColor(hexString: "#03B3E3")
        }
        cell.Title.text = item.duration_in_days + " " + "Day".localized()
        print(Float(item.cost)!.formattedWithSeparator)
        cell.note.text = "R.S".localized() + " " + Double(item.cost)!.formattedWithSeparator
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if userPackageID > 0 {
            return
        }
        for i in 0..<packagesDataarr.count {
            print("i : \(i)")
            if i == indexPath.row {
                let cell = collectionView.cellForItem(at: indexPath) as! SubScribtionPackagesCVCell
                cell.whiteView.borderWidth = 1
                cell.whiteView.borderColor = UIColor(hexString: "#03B3E3")
                selectedPackage = packagesDataarr[indexPath.row].id
                selectedPackageCost = packagesDataarr[indexPath.row].cost
                selectedPackageTitle = packagesDataarr[indexPath.row].duration_in_days + " " + "Day".localized()
                print("selectedPackages : \(selectedPackage)")
            }
            else{
                let IndexxPath = IndexPath(row: i, section: 0)
                let cell1 = collectionView.cellForItem(at: IndexxPath) as! SubScribtionPackagesCVCell
                cell1.whiteView.borderWidth = 1
                cell1.whiteView.borderColor = .white
            }
                
            }
    }
}
extension subscriptionsVCtrl : subscriptionsView {
    func onSuccessNewPackages(Res: newPacakagesRes) {
        
    }
    
    func iapShowLoading() {
        Loader.showProgress()
    }
    
    func iapHideLoading() {
        Loader.hidePrgoress()
    }
    func onSuccessHyperPay(URLString: String){
//        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentInfoVController") as? PaymentInfoVC {
//            viewController.PackageId = selectedPackage
//            viewController.PackageCost = selectedPackageCost
//            viewController.PackageTitle = selectedPackageTitle
//            if let navigator = navigationController {
//                navigator.pushViewController(viewController, animated: false)
//            }
//        }
        UserManager.sharedInstance.subscribtionURL = URLString
        self.performSegue(withIdentifier: "openSubscribtionSeg", sender: nil)
    }
    func onAddTransactionError() {
        self.showErrorMessageLong(message: NSLocalizedString("Error while communicating with the server. Please make sure to connect to the Internet and reopen the application so that we can complete the process.", comment: ""))
        
    }
    
    func onSuccessAddTransaction() {
        self.showSucessMessage(message:  NSLocalizedString("Successfully Subscribed , Thank You.", comment: ""))
        UserDefaults.standard.removeObject(forKey: "pendingTransactioniD")
        UserDefaults.standard.removeObject(forKey: "TransactionDate")
        self.navigationController?.popViewController(animated: true)
    }
    
    func onSucess(subscriptionResponse: packagesRes) {
        print(subscriptionResponse)
        if let status = subscriptionResponse.status{
            if status == 1 {
                //free user
                self.WhiteView.isHidden = false
                
                self.sNowBtn.isHidden = false
                self.whiteViewHeight.constant = 80
                self.subTitle.text = "Hello,".localized() + (UserManager.sharedInstance.getUser()?.name ?? "")
                
                
//                self.subNotes.text = "You are now enjoying the free 90-day subscription".localized()
                
                

                if let days = subscriptionResponse.free_period_in_days {
                    self.subNotes.text = "You are now enjoying the free ".localized() + String(days) + "-day".localized() + " " + "subscription".localized()
                }
                
                
                self.dateFixedText.isHidden = false
                
                if let SubDate = subscriptionResponse.subscription_date{
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                    dateFormatterGet.timeZone = NSTimeZone.local
                    dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
//                    dateFormatterGet.timeZone = TimeZone.current
                    if L102Language.isRTL {
                        dateFormatterGet.locale = Locale(identifier: "ar_EG")
                    }
                    else{
                        dateFormatterGet.locale = Locale(identifier: "en_US")
                    }

                    guard let estimatedDate = dateFormatterGet.date(from:  SubDate) else {return}
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone.current
                    dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
                    if L102Language.isRTL {
                        dateFormatter.locale = Locale(identifier: "ar_EG")
                    }
                    else{
                        dateFormatter.locale = Locale(identifier: "en_US")
                    }
                    
//                    var dayComponent    = DateComponents()
//                    dayComponent.day    = 1
//                    let theCalendar     = Calendar.current
//                    guard let nextDate        = theCalendar.date(byAdding: dayComponent, to: estimatedDate) else {return}
                    let estimatedDateString = dateFormatter.string(from: estimatedDate)
                    self.subDate.text = estimatedDateString
                }
                self.packagesCV.isHidden = false
                self.packagesDataarr = subscriptionResponse.Packages
//                if self.packagesDataarr.count == 0 {
//                    self.sNowBtn.isHidden = true
//                }
//                else{
//                    self.sNowBtn.isHidden = false
//                }
                self.inAppPurchaseStatus = subscriptionResponse.inAppPurchase ?? 0
                self.packagesCV.reloadData()
                
            }else if status == 0 {
                // see selected packages
                if let userPackage = subscriptionResponse.selectedPackage{
                    print("user subscriped in packages")
                    self.WhiteView.isHidden = false
                    
                    self.sNowBtn.isHidden = true
                    self.whiteViewHeight.constant = 80
                    self.subTitle.text = "Hello,".localized() + (UserManager.sharedInstance.getUser()?.name ?? "")
                    
                    if let days = userPackage.duration_in_days {
                        self.subNotes.text = "You are now enjoying a ".localized() + days + "-day".localized() + " " +  "subscription".localized()
                    }
//                    self.userPackageID = userPackage.id
//                    self.selectedPackage = userPackage.id
                    UserManager.sharedInstance.subscribedUser = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                    self.userPackageID = userPackage.package_id
                    self.selectedPackage = userPackage.package_id
                    self.dateFixedText.isHidden = false
                    
                    if let endDate = userPackage.endDate?.date{
                        let dateFormatterGet = DateFormatter()
                        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
                        if L102Language.isRTL {
                            dateFormatterGet.locale = Locale(identifier: "ar_EG")
                        }
                        else{
                            dateFormatterGet.locale = Locale(identifier: "en_US")
                        }
                        guard let estimatedDate = dateFormatterGet.date(from:  endDate)  else {return}
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
                        dateFormatter.timeZone = TimeZone.current
                        if L102Language.isRTL {
                            dateFormatter.locale = Locale(identifier: "ar_EG")
                        }
                        else{
                            dateFormatter.locale = Locale(identifier: "en_US")
                        }
//                        var dayComponent    = DateComponents()
//                        dayComponent.day    = 1
//                        let theCalendar     = Calendar.current
//                        guard let nextDate        = theCalendar.date(byAdding: dayComponent, to: estimatedDate) else {return}
                        let estimatedDateString = dateFormatter.string(from: estimatedDate)
                        
                        self.subDate.text = estimatedDateString
                        
                    }
                    self.packagesCV.isHidden = false
                    self.packagesDataarr = subscriptionResponse.Packages
//                    if self.packagesDataarr.count == 0 {
//                        self.sNowBtn.isHidden = true
//                    }
//                    else{
//                        self.sNowBtn.isHidden = false
//                    }
                    self.inAppPurchaseStatus = subscriptionResponse.inAppPurchase ?? 0
                    self.packagesCV.reloadData()
                    
                }else{
                    
                    print("user not subscriped in any packages")
                    self.sNowBtn.isHidden = false
                    self.packagesCV.isHidden = false
                    //                    self.WhiteView.isHidden = true
                    //                    self.whiteViewHeight.constant = 0
                    self.WhiteView.isHidden = false
                    
                    self.whiteViewHeight.constant = 80
                    self.subTitle.text = "Sorry,".localized()
                    
                    
                    self.subNotes.text = "Your subscritpion has expired".localized()
                    
                    self.dateFixedText.isHidden = true
                    
                    self.packagesCV.isHidden = false
                    self.packagesDataarr = subscriptionResponse.Packages
//                    if self.packagesDataarr.count == 0 {
//                        self.sNowBtn.isHidden = true
//                    }
//                    else{
//                        self.sNowBtn.isHidden = false
//                    }
                    self.inAppPurchaseStatus = subscriptionResponse.inAppPurchase ?? 0
                    self.packagesCV.reloadData()
                }
            }
        }
        
        
    }
    
    
    func showLoadingIndicator() {
        self.packagesCV.windless.start()
    }
    
    func hideLoadingIndicator() {
        self.packagesCV.windless.end()
        itemCountForWindless = 0
        self.packagesCV.reloadData()
        self.packagesCV.dg_stopLoading()
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
}


extension subscriptionsVCtrl: NotificationView{
    
    
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationCount.text = (" \(String(notificationCount)) ")
            self.notificationCount.isHidden = false
        }
        else{
            self.notificationCount.isHidden = true
        }
    }
}

