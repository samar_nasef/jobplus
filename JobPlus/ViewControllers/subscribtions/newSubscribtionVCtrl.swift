//
//  subscriptionsVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/1/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TransitionButton
import SwiftyStoreKit

//class SubscriptionsTableViewCell: UITableViewCell {
//    
//    @IBOutlet weak var whiteView: UIView!
//    @IBOutlet weak var Title: UILabel!
//    @IBOutlet weak var note: UILabel!
//    
//}

class newSubscribtionVCtrl: UIViewController {
    
    @IBOutlet weak var iapHederLbl: UILabel!
    @IBOutlet weak var iapViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var iapView: UIView!
    @IBOutlet weak var iapLbl: UILabel!
    @IBOutlet weak var iapBtn: UIButton!
    var inAppPurchaseStatus = 0
    
    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var notificationBtn: UIButton!
   
    @IBOutlet weak var whiteViewHeight: NSLayoutConstraint!
    @IBOutlet weak var msgView: UIView!
    @IBOutlet weak var WhiteView: UIView!

    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var noOfjobsPerMonthLbl: UILabel!
    @IBOutlet weak var sub_typeLbl: UILabel!
    @IBOutlet weak var sub_dateLbl: UILabel!
    @IBOutlet weak var sub_endDateLbl: UILabel!
    @IBOutlet weak var premuim_costLbl: UILabel!
    @IBOutlet weak var sub_details_top_constaint: NSLayoutConstraint!
    @IBOutlet weak var sub_details_view_height_constraint: NSLayoutConstraint!
    @IBOutlet var basic_height_constain: NSLayoutConstraint!
    @IBOutlet var basic_top_constaint: NSLayoutConstraint!
    @IBOutlet weak var basicView: UIView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var subscribeNowBtn: TransitionButton!
    @IBOutlet weak var premiumView: UIView!
    @IBOutlet weak var planDescLbl: UILabel!
    @IBOutlet var basicLbl: UILabel!
    @IBOutlet weak var premiumLbl: UILabel!
    @IBOutlet weak var unsubscribeBtn: UIButton!
    let notificationCountPresenter = NotificationPresenter()
    let subsPresenter = SubscribtionPresenter()
    var itemCountForWindless = 10
    var selectedPackage:Int = -1
    var selectedPackageCost:String = ""
    var selectedPackageTitle:String = ""
    var packagesDataarr:[packagesObj] = []
    var userPackageID = -1
    var subscribedUser = false
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.subTitle.text = "Hello,".localized() + (UserManager.sharedInstance.getUser()?.name ?? "")
            self.iapViewHeightConstraint.constant = 0
            self.iapView.isHidden = true
            self.basicView.isHidden = true
            self.basicLbl.isHidden = true
            self.subView.isHidden = true
            self.premiumLbl.isHidden = true
            self.premiumView.isHidden = true
            self.subscribeNowBtn.isHidden = true
            self.unsubscribeBtn.isHidden = true
        }
        
        notificationCountPresenter.attachView(view: self)
        subsPresenter.attachView(view: self)
        
        if UserManager.sharedInstance.getUser() != nil {
            self.notificationBtn.isHidden = false
        }
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        
    }
    @IBAction func openHome(_ sender: Any) {
        UserManager.sharedInstance.userRegisterForFirstTime = 0
        if #available(iOS 13.0, *) {
         let scene = UIApplication.shared.connectedScenes.first
         if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
             let mainwindow = sd.window
              let stry = UIStoryboard(name: "Main", bundle: nil)
          mainwindow?.rootViewController = stry.instantiateInitialViewController()
             }
         }
         else{
         let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                let stry = UIStoryboard(name: "Main", bundle: nil)
                rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
         }
    }
    
    @IBAction func iapAction(_ sender: Any) {
        
        SwiftyStoreKit.purchaseProduct("com.adroyts.jobplus.nonRenewableSubscription", quantity: 1, atomically: false) { result in
            switch result {
            case .success(let product):
                // fetch content from your server, then:
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
                UserDefaults.standard.set(product.transaction.transactionIdentifier ?? "",forKey: "pendingTransactioniD")
                UserDefaults.standard.set("\(Date())", forKey: "TransactionDate")
                UserDefaults.standard.set(self.selectedPackage, forKey: "selectedPackage")
                print("Purchase Success: \(product.productId) transaction id: \(product.transaction.transactionIdentifier ?? "")")
                self.subsPresenter.addIAPTransaction(transaction_id: product.transaction.transactionIdentifier ?? "", packageID: self.selectedPackage, paymentBrand: 4)
                
            case .error(let error):
                switch error.code {
                case .unknown: print("Unknown error. Please contact support")
                case .clientInvalid: print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid: print("The purchase identifier was invalid")
                case .paymentNotAllowed: print("The device is not allowed to make the payment")
                case .storeProductNotAvailable: print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserManager.sharedInstance.getUser() != nil {
            let notiCount = UserManager.sharedInstance.notificationCount
            if notiCount > 0 {
                self.notificationCount.text = (" \(String(notiCount)) ")
                self.notificationCount.isHidden = false
            }
            else{
                self.notificationCount.isHidden = true
            }
            notificationCountPresenter.getNotificationCount()
            
        }
        if UserManager.sharedInstance.subscribtionURL != "" {
            self.performSegue(withIdentifier: "openSubscribtionSeg", sender: nil)
        }
        userPackageID = -1
        selectedPackage = -1
        subsPresenter.getNewPackages()
        SwiftyStoreKit.retrieveProductsInfo(["com.adroyts.jobplus.nonRenewableSubscription"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                var iapTitle = product.localizedTitle
                if L102Language.isRTL, iapTitle == "30 Days" {
                    iapTitle = "٣٠ يوم"
                }
                print("Product: \(product.localizedDescription), price: \(priceString)")
                self.iapLbl.text = iapTitle
                self.iapBtn.setTitle("\(priceString)", for: .normal)
                self.iapBtn.setTitle("\(priceString)", for: .selected)
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error!)")
            }
        }
        
    }
    
    
    @IBAction func goback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @objc func subscribeNow(_ btn: UIButton){
        print("packageId \(btn.tag)")
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentInfoVController") as? PaymentInfoVC {
            viewController.PackageId = selectedPackage
            viewController.PackageCost = selectedPackageCost
            viewController.PackageTitle = selectedPackageTitle
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    
        
    @IBAction func sNowClicked(_ sender: TransitionButton) {
        
        if subscribedUser {
            if #available(iOS 13.0, *) {
             let scene = UIApplication.shared.connectedScenes.first
             if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                 let mainwindow = sd.window
                  let stry = UIStoryboard(name: "Main", bundle: nil)
              mainwindow?.rootViewController = stry.instantiateInitialViewController()
                 }
             }
             else{
             let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    let stry = UIStoryboard(name: "Main", bundle: nil)
                    rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
             }
            
        }
        else{
            print("package selected : \(selectedPackage) ")
            let alert = UIAlertController(title: "Checkout".localized(), message: "Which method you want to pay with ?".localized(), preferredStyle: .actionSheet)

            let madaAction = UIAlertAction(title: "VISA".localized(), style: .default , handler:{ (UIAlertAction)in
                self.subsPresenter.getHyperPayURL(package_id: self.selectedPackage, amount: self.selectedPackageCost, payment_brand: 0)

            })
            //madaAction.setValue(UIImage(named: "discounts_ic"), forKey: "image")
            alert.addAction((madaAction))
            alert.addAction(UIAlertAction(title: "MADA".localized(), style: .default , handler:{ (UIAlertAction)in
                self.subsPresenter.getHyperPayURL(package_id: self.selectedPackage, amount: self.selectedPackageCost, payment_brand: 1)

            }))
            alert.addAction(UIAlertAction(title: "STC".localized(), style: .default , handler:{ (UIAlertAction)in
                self.subsPresenter.getHyperPayURL(package_id: self.selectedPackage, amount: self.selectedPackageCost, payment_brand: 2)

            }))
            alert.addAction(UIAlertAction(title: "Apple Pay".localized(), style: .default , handler:{ (UIAlertAction)in
                self.subsPresenter.getHyperPayURL(package_id: self.selectedPackage, amount: self.selectedPackageCost, payment_brand: 3)

            }))
            alert.addAction(UIAlertAction(title: "Cancel2".localized(), style: .cancel , handler:{ (UIAlertAction)in
                alert.dismiss(animated: true, completion: nil)
            }))
            alert.popoverPresentationController?.sourceView = self.view;
            alert.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 2 ,width: UIScreen.main.bounds.width / 4 ,height: 1.0);
            self.present(alert, animated: true, completion: {

            })
            
        }
        
    }
    
    
    
    @IBAction func unsubscribeAction(_ sender: UIButton) {
        if sender.tag == 0 {
            let actionSheetController: UIAlertController = UIAlertController(title: "".localized(), message:  "Are you sure activate auto subscription ?".localized(), preferredStyle: .alert)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel2".localized(), style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            //Create and add the Cancel action
            let unsubscribeAction: UIAlertAction = UIAlertAction(title: "Yes".localized(), style: .default) { action -> Void in
                self.subsPresenter.autoSubscribtion()
            }
            actionSheetController.addAction(unsubscribeAction)
            actionSheetController.addAction(cancelAction)
            actionSheetController.popoverPresentationController?.sourceView = self.view;
            actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 5 - 50,width: UIScreen.main.bounds.width ,height: 1.0);
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
        }
        else{
            let actionSheetController: UIAlertController = UIAlertController(title: "".localized(), message: "Are you sure cancel auto subscription ?".localized(), preferredStyle: .alert)
            
            //Create and add the Cancel action
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel2".localized(), style: .cancel) { action -> Void in
                //Just dismiss the action sheet
            }
            //Create and add the Cancel action
            let unsubscribeAction: UIAlertAction = UIAlertAction(title: "Yes".localized(), style: .destructive) { action -> Void in
                self.subsPresenter.cancelSubscribtion()
            }
            actionSheetController.addAction(unsubscribeAction)
            actionSheetController.addAction(cancelAction)
            actionSheetController.popoverPresentationController?.sourceView = self.view;
            actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 5 - 50,width: UIScreen.main.bounds.width ,height: 1.0);
            //Present the AlertController
            self.present(actionSheetController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func openNotification(_ sender: UIButton) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
}


extension newSubscribtionVCtrl : subscriptionsView {
    func onSuccessNewPackages(Res: newPacakagesRes) {
        DispatchQueue.main.async {
            self.subTitle.text = "Hello,".localized() + (UserManager.sharedInstance.getUser()?.name ?? "")
            self.iapViewHeightConstraint.constant = 0
            self.iapView.isHidden = true
            self.basicView.isHidden = true
            self.basicLbl.isHidden = true
            self.subView.isHidden = true
            self.premiumLbl.isHidden = true
            self.premiumView.isHidden = true
            self.subscribeNowBtn.isHidden = true
            self.basic_top_constaint.constant = 16
            self.basic_height_constain.constant = 0//102
            self.sub_details_top_constaint.constant = 0//16
            self.sub_details_view_height_constraint.constant = 100
            self.selectedPackage = Res.ActualPackage ?? 0
            self.selectedPackageCost = String(Res.AcutalPrice ?? 0)
            self.inAppPurchaseStatus = Res.inAppPurchase ?? 0
            
            if let noOfJobs = Res.FreeMonthlyJobs {
                self.noOfjobsPerMonthLbl.text = "\("Apply for maximum".localized()) \(noOfJobs) \("jobs per month".localized())"
            }
            if let price = Res.AdvancedPrice, Res.ActualPackage != 0 {
                self.premuim_costLbl.text = price
            }
            
            if let subscribed = Res.subscribed {
                if subscribed {
                    UserManager.sharedInstance.subscribedUser = true
                    self.subscribedUser = true
                    self.subscribeNowBtn.isHidden = false
                    if let sub_type = Res.subscribtionType {
                        self.sub_typeLbl.text = "\("Subscription Type:".localized()) \(sub_type)"
                    }
                    if let sub_date = Res.SubscribeStartDate {
                        self.sub_dateLbl.text = "\("Subscription date:".localized()) \(sub_date.getFormattedDate())"
                    }
                    if let end_date = Res.SubscribeEndDate {
                        self.sub_endDateLbl.text = "\("End date:".localized()) \(end_date.getFormattedDate())"
                    }
                    if let descMsg = Res.subscribtionMessage {
                        if !descMsg.isBlank {
                            self.planDescLbl.text = descMsg
                            self.planDescLbl.isHidden = false
                        }
                    }
                    self.subView.isHidden = false
                    self.basic_top_constaint.constant = 0
                    self.subscribeNowBtn.setTitle("Apply Now".localized(), for: .normal)
                    self.subscribeNowBtn.setTitle("Apply Now".localized(), for: .selected)
                    self.basic_height_constain.constant = 0
                    if let auto = Res.autosubscribed {
                        if auto == 0 {
                            self.unsubscribeBtn.tag = 0
                            self.unsubscribeBtn.setTitle("Subscribe Now".localized(), for: .normal)
                            self.unsubscribeBtn.setTitle("Subscribe Now".localized(), for: .selected)
                            self.unsubscribeBtn.backgroundColor = UIColor(hexString: "#161650")
                        }
                        else{
                            self.unsubscribeBtn.tag = 1
                            self.unsubscribeBtn.setTitle("Unsubscribe".localized(), for: .normal)
                            self.unsubscribeBtn.setTitle("Unsubscribe".localized(), for: .selected)
                            self.unsubscribeBtn.backgroundColor = UIColor(hexString: "#ED2027")
                        }
                    }
                    
                }
                else{
                    if let descMsg = Res.subscribtionMessage {
                        if !descMsg.isBlank {
                            self.planDescLbl.text = descMsg
                            self.planDescLbl.isHidden = false
                        }
                    }
                    self.basicView.isHidden = true
                    self.basicLbl.isHidden = true
                    self.subscribeNowBtn.isHidden = false
                    self.sub_details_top_constaint.constant = 0
                    self.premiumLbl.isHidden = false
                    self.sub_details_view_height_constraint.constant = 0
                    if self.inAppPurchaseStatus == 1 {
                        self.iapView.isHidden = false
                        self.iapLbl.isHidden = false
                        self.iapBtn.isHidden = false
                        self.iapHederLbl.isHidden = false
                        self.iapViewHeightConstraint.constant = 100
                        self.subscribeNowBtn.isHidden = true
                        self.premuim_costLbl.isHidden = true
                    }
                    else{
                        self.iapView.isHidden = true
                        self.iapLbl.isHidden = true
                        self.iapBtn.isHidden = true
                        self.iapHederLbl.isHidden = true
                        self.iapViewHeightConstraint.constant = 0
                        self.subscribeNowBtn.isHidden = false
                        self.premuim_costLbl.isHidden = false
                    }
                }
            }
            self.premiumView.isHidden = false
        }
        
    }
    
    func iapShowLoading() {
        Loader.showProgress()
    }
    
    func iapHideLoading() {
        Loader.hidePrgoress()
    }
    func onSuccessHyperPay(URLString: String){
        UserManager.sharedInstance.subscribtionURL = URLString
        self.performSegue(withIdentifier: "openSubscribtionSeg", sender: nil)
    }
    func onAddTransactionError() {
        self.showErrorMessageLong(message: NSLocalizedString("Error while communicating with the server. Please make sure to connect to the Internet and reopen the application so that we can complete the process.", comment: ""))
        
    }
    
    func onSuccessAddTransaction() {
        self.showSucessMessage(message:  NSLocalizedString("Successfully Subscribed , Thank You.", comment: ""))
        UserDefaults.standard.removeObject(forKey: "pendingTransactioniD")
        UserDefaults.standard.removeObject(forKey: "TransactionDate")
        self.navigationController?.popViewController(animated: true)
    }
    
    func onSucess(subscriptionResponse: packagesRes) {
       
    }
    
    
    func showLoadingIndicator() {
        Loader.showProgress()
    }
    
    func hideLoadingIndicator() {
        Loader.hidePrgoress()
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
}


extension newSubscribtionVCtrl: NotificationView{
    
    
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationCount.text = (" \(String(notificationCount)) ")
            self.notificationCount.isHidden = false
        }
        else{
            self.notificationCount.isHidden = true
        }
    }
}

