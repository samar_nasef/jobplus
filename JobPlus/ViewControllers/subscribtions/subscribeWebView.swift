//
//  subscribeWebView.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/11/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import WebKit

class subscribeWebView: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var webToLoadWebView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        webView = WKWebView(frame: webToLoadWebView.bounds, configuration: WKWebViewConfiguration())
        webView.navigationDelegate = self
        self.webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webToLoadWebView.addSubview(webView)
        if let url = URL(string: UserManager.sharedInstance.subscribtionURL ){
            UserManager.sharedInstance.subscribtionURL = ""
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        }
        else{
            UserManager.sharedInstance.subscribtionURL = ""
        }
        // Do any additional setup after loading the view.
    }
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
            print("loaded")
//            webView.evaluateJavaScript("var viewPortTag=document.createElement('meta');viewPortTag.id='viewport';viewPortTag.name ='viewport';viewPortTag.content = 'width=320, initial-scale=1.0, maximum-scale=1.0, user-scalable=0';document.getElementsByTagName('head')[0].appendChild(viewPortTag);") { (result, error) in
//                if error != nil {
//                }
//            }
        }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
          print("### URL:", self.webView.url!)
            let currentURL = self.webView.url?.absoluteString ?? ""
            if currentURL.contains("testAfterHyperPayment") {
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                  UserManager.sharedInstance.subscribtionURL = ""
                  self.navigationController?.popViewController(animated: true)
                }
            }
        }

        if keyPath == #keyPath(WKWebView.estimatedProgress) {
          // When page load finishes. Should work on each page reload.
          if (self.webView.estimatedProgress == 1) {
            print("### EP:", self.webView.estimatedProgress)
          }
        }
      }
    @IBAction func dismissView(_ sender: Any) {
          UserManager.sharedInstance.subscribtionURL = ""
        self.navigationController?.popViewController(animated: true)
       }
    override func viewWillDisappear(_ animated: Bool) {
        UserManager.sharedInstance.subscribtionURL = ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
