//
//  PaymentInfoVC.swift
//  JobPlus
//
//  Created by elsaid yousif on 2/26/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TransitionButton
import TweeTextField

class PaymentInfoVC: UIViewController {
    
    @IBOutlet weak var stateText: TweeAttributedTextField!
    var selectedCountry = [String]()
    var selectededuCountry = [String]()
    var countryList = [countryItem]()
    //let countryPresenter = CountryListPresenter()
    let degreePresenter = DegreeListPresenter()
    var selectedUserCountryId:Int?
    var selectedInstitutionCountryId:Int?
    
    
    @IBOutlet weak var countryView: UIView!
    
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var lName: TweeAttributedTextField!
    @IBOutlet weak var fName: TweeAttributedTextField!
    @IBOutlet weak var email: TweeAttributedTextField!
    @IBOutlet weak var shippingAddress: TweeAttributedTextField!
    @IBOutlet weak var postalCode: TweeAttributedTextField!
    //    @IBOutlet weak var state: TweeAttributedTextField!
    @IBOutlet weak var city: TweeAttributedTextField!
    
    
    var PackageId :Int = -1
    var PackageCost :String = ""
    var PackageTitle:String = ""
    let payPresenter = PaymentPresenter()
    var itemCountForWindless = 10
    var selectedCountryCode = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentInfoVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentInfoVC.keyboardWillHide2), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        self.countryView.layer.borderWidth = 1
        //        219, 217, 217
        self.countryView.layer.borderColor = UIColor(red:207/255, green:204/255, blue:204/255, alpha: 1).cgColor
        self.countryView.layer.cornerRadius = 10
        
        //        countryPresenter.attachView(view: self)
        //        countryPresenter.doGetCountryList()
        degreePresenter.attachView(view: self)
        degreePresenter.doGetDegreeList()
        print("packageId from payment info \(PackageId) , packageCost: \(PackageCost),packagetitle: \(PackageTitle)")
        payPresenter.attachView(view: self)
        
        //FF2600
        //        fullNameTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        //               fullNameTxtField.infoFontSize = 14.0
        
        //               fullNameTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        
        //               fullNameTxtField.activeLineWidth = 2
        //               fullNameTxtField.animationDuration = 0.5
        
        //               fullNameTxtField.minimumPlaceholderFontSize = 15
        //               fullNameTxtField.originalPlaceholderFontSize = 15
        //               fullNameTxtField.placeholderDuration = 0.2
        //               fullNameTxtField.placeholderColor = .darkGray
        //               fullNameTxtField.placeholder = "Full Name".localized()
        //               fullNameTxtField.textColor = UIColor(hexString: "#8C8C8C")
        //
        
        if L102Language.isRTL {
            self.backbtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backbtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        setupTextFields()
        self.view.changeFont()
        if let name = UserManager.sharedInstance.getUser()?.name {
            self.fName.text = String(name)
        }
        if let email = UserManager.sharedInstance.getUser()?.email {
            self.email.text = email
        }
        
        if let pfname = UserDefaults.standard.string(forKey: "pfname"){
            fName.text = pfname
        }
        if let plname = UserDefaults.standard.string(forKey: "plname"){
            lName.text! = plname
        }
        if let pemail = UserDefaults.standard.string(forKey: "pemail"){
            email.text! = pemail
        }
        if let pCountryCode = UserDefaults.standard.string(forKey: "pCountryCode"){
            if let pCountry = UserDefaults.standard.string(forKey: "pCountry"){
                if L102Language.isRTL {
                    if let selectedCountry  = self.countryList.first(where: {$0.nameAr == pCountry}) {
                        self.selectedCountryCode = selectedCountry.code ?? pCountryCode
                        stateText.text = pCountry
                    }
                }
                else{
                    if let selectedCountry  = self.countryList.first(where: {$0.nameEn == pCountry}) {
                        self.selectedCountryCode = selectedCountry.code ?? pCountryCode
                        stateText.text = pCountry
                    }
                }
                
                
            }
        }
        if let pCity = UserDefaults.standard.string(forKey: "pCity"){
            city.text! = pCity
        }
        if let pAddress = UserDefaults.standard.string(forKey: "pAddress"){
            shippingAddress.text! = pAddress
        }
        if let pPostalCode = UserDefaults.standard.string(forKey: "pPostalCode"){
            postalCode.text! = pPostalCode
        }
    }
    
    
    
    @IBAction func goback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    @IBAction func continueClicked(_ sender: UIButton) {
        
        print("continue")
        //        if let url = URL(string: "https://www.hackingwithswift.com") {
        //            UIApplication.shared.open(url)
        //        }
        //
        var valid = true
        let nameTxt = self.fName.text ?? ""
        let trimedTxt = nameTxt.components(separatedBy: .whitespaces).joined()
        if let fullName = self.fName.text , !fullName.isBlank {
            //            if !fullName.isNameMoreThanTwoCharacters {
            //                let errorMessage = "Full name must be more than four characters".localized()
            //                self.fName.showInfo(errorMessage, animated: true)
            //                valid = false
            //            }
            //            else
            if trimedTxt.hasSpecialCharacters(){
                self.fName.showInfo("invalid special".localized())
                valid = false
            }
            else {
                self.fName.hideInfo(animated: true)
                print("valid fullNmae")
            }
        }else {
            print("error fullNmae")
            let errorMessage = "enterFName".localized()
            self.fName.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        
        
        let nameTxt2 = self.lName.text ?? ""
        let trimedTxt2 = nameTxt2.components(separatedBy: .whitespaces).joined()
        if let LastName = self.lName.text , !LastName.isBlank {
            //            if !fullName.isNameMoreThanTwoCharacters {
            //                let errorMessage = "Full name must be more than four characters".localized()
            //                self.fName.showInfo(errorMessage, animated: true)
            //                valid = false
            //            }
            //            else
            if trimedTxt2.hasSpecialCharacters(){
                self.lName.showInfo("invalid special".localized())
                valid = false
            }
            else {
                self.lName.hideInfo(animated: true)
                print("valid fullNmae")
            }
        }else {
            print("error fullNmae")
            let errorMessage = "enterlName".localized()
            self.lName.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        
        
        if let email = self.email.text , !email.isBlank {
            if !email.isEmail{
                let errorMessage = "please enter valid email".localized()
                self.email.showInfo(errorMessage, animated: true)
                valid = false
            }else {
                self.email.hideInfo(animated: true)
                print("valid email")
            }
        }else {
            print("error email")
            let errorMessage = "please enter your email address".localized()
            self.email.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        
        if let shippAddress = self.shippingAddress.text , shippAddress.isBlank {
            
            print("error email")
            let errorMessage = "enteraddress_shipping".localized()
            self.shippingAddress.showInfo(errorMessage, animated: true)
            valid = false
        }else{
            self.shippingAddress.hideInfo(animated: true)
        }
        
        if let postalcode = self.postalCode.text , postalcode.isBlank {
            
            print("error email")
            let errorMessage = "enterpostal_code_shipping".localized()
            self.postalCode.showInfo(errorMessage, animated: true)
            valid = false
        }else{
            self.postalCode.hideInfo(animated: true)
        }
        
        if let state1 = self.stateText.text , state1.isBlank {
            
            print("error email")
            let errorMessage = "enterstate_shipping".localized()
            self.stateText.showInfo(errorMessage, animated: true)
            valid = false
        }else{
            self.stateText.hideInfo(animated: true)
        }
        
        if let city1 = self.city.text , city1.isBlank {
            
            print("error email")
            let errorMessage = "entercity_shipping".localized()
            self.city.showInfo(errorMessage, animated: true)
            valid = false
        }else{
            self.city.hideInfo(animated: true)
        }
        
        
        if valid == true {
            print("call api ")
            var lang = ""
            if L102Language.isRTL {
                lang = "Arabic"
            }else {
                lang = "English"
            }
            
            var userMobile = ""
            var countrycode = ""
            if let mobile = UserManager.sharedInstance.getUser()?.mobile {
                if mobile.contains("+") {
                    userMobile =  mobile
                    countrycode = ""
                }
                else if let countryCode = UserManager.sharedInstance.getUser()?.mobileCountryPrefix {
                    userMobile =  "\(mobile)"
                    countrycode = countryCode
                    
                    
                }
                
            }
            
            print("payment info country code : \(countrycode)")
            
            UserDefaults.standard.set(fName.text!, forKey: "pfname")
            UserDefaults.standard.set(lName.text!, forKey: "plname")
            UserDefaults.standard.set(email.text!, forKey: "pemail")
            UserDefaults.standard.set(selectedCountryCode, forKey: "pCountryCode")
            UserDefaults.standard.set(stateText.text!, forKey: "pCountry")
            UserDefaults.standard.set(city.text!, forKey: "pCity")
            UserDefaults.standard.set(shippingAddress.text!, forKey: "pAddress")
            UserDefaults.standard.set(postalCode.text!, forKey: "pPostalCode")
            let paymentData = PaymentObj(cc_first_name: fName.text!, cc_last_name: lName.text!, cc_phone_number: countrycode, phone_number: userMobile, email: email.text!, products_per_title: PackageTitle, unit_price: PackageCost, quantity: 1, other_charges: 0, amount: PackageCost, discount: "0", currency: "SAR", reference_no: String(PackageId), ip_customer: "192.168.1.1", billing_address: shippingAddress.text!, state: stateText.text!, city: city.text!, postal_code: postalCode.text!.replacedArabicDigitsWithEnglish, country: selectedCountryCode, shipping_first_name: fName.text!, shipping_last_name: lName.text!, address_shipping: shippingAddress.text!, city_shipping: city.text!, state_shipping: stateText.text!, postal_code_shipping: postalCode.text!.replacedArabicDigitsWithEnglish, country_shipping: selectedCountryCode, msg_lang: lang)
            
            print("paymentData : \(paymentData)")
            
            payPresenter.createPayPage(paymentInfo: paymentData)
            
        }
        
        
        
        
        
    }
    
    func goToHome(){
        self.dismiss(animated: true) {
            if #available(iOS 13.0, *) {
                let scene = UIApplication.shared.connectedScenes.first
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    let mainwindow = sd.window
                    let stry = UIStoryboard(name: "Main", bundle: nil)
                    mainwindow?.rootViewController = stry.instantiateInitialViewController()
                }
            }
            else{
                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                let stry = UIStoryboard(name: "Main", bundle: nil)
                rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
            }
        }
    }
    
    
    func setupTextFields(){
        fName.keyboardType = .default
        fName.returnKeyType = .next
        fName.infoTextColor = UIColor(hexString: "#FF2600")
        fName.infoFontSize = 14.0
        fName.activeLineColor = UIColor(hexString: "#00AFDF")
        fName.activeLineWidth = 2
        fName.animationDuration = 0.5
        fName.minimumPlaceholderFontSize = 15
        fName.originalPlaceholderFontSize = 15
        fName.placeholderDuration = 0.2
        fName.placeholderColor = .darkGray
        fName.delegate = self
        fName.textColor = UIColor(hexString: "#8C8C8C")
        fName.placeholder = "First Name".localized()
        
        lName.keyboardType = .default
        lName.returnKeyType = .next
        lName.infoTextColor = UIColor(hexString: "#FF2600")
        lName.infoFontSize = 14.0
        lName.activeLineColor = UIColor(hexString: "#00AFDF")
        lName.activeLineWidth = 2
        lName.animationDuration = 0.5
        lName.minimumPlaceholderFontSize = 15
        lName.originalPlaceholderFontSize = 15
        lName.placeholderDuration = 0.2
        lName.placeholderColor = .darkGray
        lName.delegate = self
        lName.textColor = UIColor(hexString: "#8C8C8C")
        lName.placeholder = "Last Name".localized()
        
        email.keyboardType = .default
        email.returnKeyType = .done
        email.infoTextColor = UIColor(hexString: "#FF2600")
        email.infoFontSize = 14.0
        email.activeLineColor = UIColor(hexString: "#00AFDF")
        email.activeLineWidth = 2
        email.animationDuration = 0.5
        email.minimumPlaceholderFontSize = 15
        email.originalPlaceholderFontSize = 15
        email.placeholderDuration = 0.2
        email.placeholderColor = .darkGray
        email.delegate = self
        email.textColor = UIColor(hexString: "#8C8C8C")
        email.placeholder = "Email".localized()
        
        stateText.infoTextColor = UIColor(hexString: "#FF2600")
        stateText.infoFontSize = 14.0
        stateText.activeLineColor = UIColor(hexString: "#00AFDF")
        stateText.activeLineWidth = 2
        stateText.animationDuration = 0.5
        stateText.minimumPlaceholderFontSize = 15
        stateText.originalPlaceholderFontSize = 15
        stateText.placeholderDuration = 0.2
        stateText.placeholderColor = .darkGray
        stateText.delegate = self
        stateText.textColor = UIColor(hexString: "#8C8C8C")
        stateText.placeholder = "State".localized()
        
        city.keyboardType = .default
        city.returnKeyType = .next
        city.infoTextColor = UIColor(hexString: "#FF2600")
        city.infoFontSize = 14.0
        city.activeLineColor = UIColor(hexString: "#00AFDF")
        city.activeLineWidth = 2
        city.animationDuration = 0.5
        city.minimumPlaceholderFontSize = 15
        city.originalPlaceholderFontSize = 15
        city.placeholderDuration = 0.2
        city.placeholderColor = .darkGray
        city.delegate = self
        city.textColor = UIColor(hexString: "#8C8C8C")
        city.placeholder = "City".localized()
        
        shippingAddress.keyboardType = .default
        shippingAddress.returnKeyType = .next
        shippingAddress.infoTextColor = UIColor(hexString: "#FF2600")
        shippingAddress.infoFontSize = 14.0
        shippingAddress.activeLineColor = UIColor(hexString: "#00AFDF")
        shippingAddress.activeLineWidth = 2
        shippingAddress.animationDuration = 0.5
        shippingAddress.minimumPlaceholderFontSize = 15
        shippingAddress.originalPlaceholderFontSize = 15
        shippingAddress.placeholderDuration = 0.2
        shippingAddress.placeholderColor = .darkGray
        shippingAddress.delegate = self
        shippingAddress.textColor = UIColor(hexString: "#8C8C8C")
        shippingAddress.placeholder = "Shipping Address".localized()
        
        postalCode.keyboardType = .numberPad
        postalCode.returnKeyType = .done
        postalCode.infoTextColor = UIColor(hexString: "#FF2600")
        postalCode.infoFontSize = 14.0
        postalCode.activeLineColor = UIColor(hexString: "#00AFDF")
        postalCode.activeLineWidth = 2
        postalCode.animationDuration = 0.5
        postalCode.minimumPlaceholderFontSize = 15
        postalCode.originalPlaceholderFontSize = 15
        postalCode.placeholderDuration = 0.2
        postalCode.placeholderColor = .darkGray
        postalCode.delegate = self
        postalCode.textColor = UIColor(hexString: "#8C8C8C")
        postalCode.placeholder = "Postal Code".localized()
    }
    
    @IBAction func chooseCountry(_ sender: UIButton) {
        print("chooseCountry")
        self.view.endEditing(true)
        self.stateText.showInfo("", animated: true)
        //        self.resetCustomTextFieldsErrorMessages(self.stateText)
        let countriesNames = L102Language.isRTL ? countryList.map {$0.nameAr} : countryList.map {$0.nameEn}
        //countryPicker(sender: sender, isUserCountry: true)
        if countriesNames.count > 0 {
            showCountrySelectPopover(sender: sender, isUserCountry: true, countriesNames: countriesNames)
        }
        else{
            degreePresenter.doGetDegreeList()
        }
    }
    
    
    
    func showCountrySelectPopover(sender: UIView,isUserCountry:Bool, countriesNames: [String?]) {
        
        // selection type as multiple with subTitle Cell
        //let countriesNames = countryList.map {$0.localizeName}
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: countriesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        
        // selected items
        var selectedCountryName = [String]()
        if isUserCountry {
            selectedCountryName = selectedCountry
        }
        else{
            selectedCountryName = selectededuCountry
        }
        selectionMenu.setSelectedItems(items: selectedCountryName) { [weak self] (text, index, selected, selectedList) in
            if L102Language.isRTL {
                if let selectedCountry  = self!.countryList.first(where: {$0.nameAr == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedCountryCode = selectedCountry.code ?? ""
                            self!.selectedUserCountryId = selectedCountryID
                            self!.stateText.text = text!!
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            //                        self!.educationCountryTxtField.text = text!!
                        }
                    }
                    
                }
            }
            else{
                if let selectedCountry  = self!.countryList.first(where: {$0.nameEn == text!!}) {
                    if let selectedCountryID = selectedCountry.id{
                        if isUserCountry {
                            self!.selectedCountryCode = selectedCountry.code ?? ""
                            self!.selectedUserCountryId = selectedCountryID
                            self!.stateText.text = text!!
                        }else{
                            self!.selectedInstitutionCountryId = selectedCountryID
                            //                        self!.educationCountryTxtField.text = text!!
                        }
                    }
                    
                }
            }
        }
        
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return countriesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        //        CGSize(width: 200, height: 300)
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
    
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.stateText {
            self.stateText.showInfo("")
        }
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    @objc func keyboardWillHide2(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}


extension PaymentInfoVC : PaymentInfoView {
    func onSucess(paymentResponse : paymentRes) {
        
        print("on success payment res : \(paymentResponse)")
        if paymentResponse.response_code == "4012"{
            //guard let url = URL(string: paymentResponse.payment_url) else { return }
            //   UIApplication.shared.open(url)
            UserManager.sharedInstance.subscribtionURL = paymentResponse.payment_url
            self.navigationController?.popViewController(animated: false)
            
        }else{
            print("page not created")
            self.showErrorMessage(message:paymentResponse.result)
            
        }
        
        
    }
    
    
    func showLoadingIndicator() {
        //        self.subscriptionTaeblView.windless.start()
    }
    
    func hideLoadingIndicator() {
        //        self.subscriptionTaeblView.windless.end()
        itemCountForWindless = 0
        //        self.subscriptionTaeblView.reloadData()
        //        self.subscriptionTaeblView.dg_stopLoading()
    }
    //
    //    func onSucess(subscriptionResponse: SubscriptionResponse) {
    //        self.subsResponse = subscriptionResponse
    //        if let catArr = self.subsResponse?.subscription {
    //            if catArr.count > 0 {
    ////                self.emptyView.alpha = 0
    //                print("catArr : \(catArr)")
    //                self.subscriptionTaeblView.alpha = 1
    //            }else {
    ////                self.emptyView.alpha = 1
    //                 print("catArr2 : \(catArr)")
    //                self.subscriptionTaeblView.alpha = 0
    //            }
    //        }
    //
    //        self.subscriptionTaeblView.reloadData()
    //    }
    //
    func onError(message: String) {
        self.showErrorMessage(message:"err : \(message)")
    }
    
    
    
    
    
    
}

extension PaymentInfoVC :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fName {
            self.lName.becomeFirstResponder()
        }
        else if textField == lName {
            self.email.becomeFirstResponder()
        }
        else if textField == email {
            self.view.endEditing(true)
        }
        else if textField == city {
            self.shippingAddress.becomeFirstResponder()
        }
        else if textField == shippingAddress {
            self.postalCode.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fName {
            self.lName.showInfo("", animated: true)
        }
        else if textField == lName {
            self.lName.showInfo("", animated: true)
        }
        else if textField == email {
            self.email.showInfo("", animated: true)
        }
        else if textField == city {
            self.city.showInfo("", animated: true)
        }
        else if textField == shippingAddress {
            self.shippingAddress.showInfo("", animated: true)
        }
        else if textField == postalCode {
            self.postalCode.showInfo("", animated: true)
        }
    }
}
//extension PaymentInfoVC:CountryListView{
//    func onSucessLoadingCountryList(countries: [CountryRes]) {
//        self.countryList.removeAll()
//        self.countryList.append(contentsOf: countries)
//    }
//}
extension PaymentInfoVC:DegreeListView{
    func showLoading() {
        
    }
    
    func hideLoading() {
        
    }
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
       
    }
    
    func onSucessLoadingDegreeList(degreeWorkingTimes: DegreesTimesApiRes) {
        
        if let countries = degreeWorkingTimes.countries {
            if countries.count > 0 {
                self.countryList.removeAll()
                self.countryList.append(contentsOf: countries)
                if let countryTxt = UserManager.sharedInstance.getUser()?.country {
                    if let userCountryName = countryTxt.localizeName {
                        self.stateText.text = userCountryName
                        if L102Language.isRTL {
                            if let selectedCountry  = self.countryList.first(where: {$0.nameAr == userCountryName}) {
                                self.selectedCountryCode = selectedCountry.code ?? ""
                                print(selectedCountry)
                            }
                        }
                        else{
                            if let selectedCountry  = self.countryList.first(where: {$0.nameEn == userCountryName}) {
                                self.selectedCountryCode = selectedCountry.code ?? ""
                                print(selectedCountry)
                            }
                        }
                        
                    }
                }
                
            }
        }
    }
}
