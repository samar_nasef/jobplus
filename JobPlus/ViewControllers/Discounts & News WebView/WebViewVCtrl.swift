//
//  WebViewVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 9/6/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import WebKit

class WebViewVCtrl: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet weak var headerTitleLbl: UILabel!
    var webView: WKWebView!
    var webViewType = 1 // 1-> Discounts , 2-> News
    
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var webToLoadWebView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        self.webView?.uiDelegate = self
        if webViewType == 1 {
            self.headerTitleLbl.text = "Discounts".localized()
            webView = WKWebView(frame: webToLoadWebView.bounds, configuration: WKWebViewConfiguration())
            webView.navigationDelegate = self
            webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            webToLoadWebView.addSubview(webView)
            if let url = URL(string: L102Language.isRTL ? WebService.discountsURLAr : WebService.discounstURLEn ){
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            }
        }
        else{
            self.headerTitleLbl.text = "News and articles".localized()
            webView = WKWebView(frame: webToLoadWebView.bounds, configuration: WKWebViewConfiguration())
            webView.navigationDelegate = self
            webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            webToLoadWebView.addSubview(webView)
            if let url = URL(string: L102Language.isRTL ? WebService.newsUrlAr : WebService.newsUrlEn ){
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            }
        }
        
        // Do any additional setup after loading the view.
    }
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
    @IBAction func dismissView(_ sender: Any) {
          UserManager.sharedInstance.subscribtionURL = ""
        self.navigationController?.popViewController(animated: true)
       }
    override func viewWillDisappear(_ animated: Bool) {
        UserManager.sharedInstance.subscribtionURL = ""
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
