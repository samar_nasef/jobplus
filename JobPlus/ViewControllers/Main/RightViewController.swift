//
//  RightViewController.swift
//  NajeezAgent
//
//  Created by mohamed shaat on 5/12/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit
import Kingfisher
enum Menu : Int{
    case Catgoery
    case Notifications
    case Terms
    case AboutUs
    case FAQ
    case Language
    case Authorization
}


class RightViewController: UIViewController {
    var mainViewController:UIViewController!
    @IBOutlet weak var userProfileBtn: UIButton!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var placeHolderImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    var arrOfData : [String]  = ["Category".localized(),"Notifications".localized(),"Terms And Conditions".localized(),"About Us".localized()]
    var languageArray = ["English" , "عربي"]
    var selectedIndexPath:IndexPath?
    var logoutPresenter = LogoutPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.logoutPresenter.attachView(view: self)
        self.registerCell()
        
        self.view.changeFont()
        
    }
    
    
    func addUserData(){
//        if UserManager.sharedInstance.getUser() == nil {
//            self.userImgView.image = #imageLiteral(resourceName: "ic_user")
//        }else {
        if let profileImg = UserManager.sharedInstance.getUser()?.imagePath as? String{
            if let profileImgUrl = URL(string: profileImg){
                self.userImgView.kf.setImage(with: profileImgUrl)
                self.placeHolderImageView.isHidden = true
            }
        }
       // }
        if let userName = UserManager.sharedInstance.getUser()?.name {
            self.userNameLbl.text = userName
        }
    }


    func registerCell() {
        menuTableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserManager.sharedInstance.getUser() == nil {
            self.userProfileBtn.isEnabled = false
            arrOfData = ["Category".localized(),"Notifications".localized(),"Terms And Conditions".localized(),"About Us".localized() ,"FAQ".localized() ,"Language".localized(), "Sign in".localized()]
        }else {
            self.userProfileBtn.isEnabled = true
            arrOfData = ["Category".localized(),"Notifications".localized(),"Terms And Conditions".localized(),"About Us".localized() ,"FAQ".localized() ,"Logout".localized() ]
        }
        self.menuTableView.reloadData()
        self.addUserData()
    }
    
    
    func changeViewController(_ menu: Menu  ) {
        
        switch menu {
            
        case  .Catgoery:
            print("MyOrders")
            //categori
            //self.mainViewController.navigationController?.popToRootViewController(animated: true)
            self.mainViewController.pushViewController(viewControllerName: "CatgoriesViewController", storyBoardName: "Main")
        case .Notifications:
            
            self.mainViewController.pushViewController(viewControllerName: "NotificationsViewController", storyBoardName: "Notifications")
        case .Terms:
            
            self.mainViewController.pushViewController(viewControllerName: "TermsAndConditionsViewController", storyBoardName: "Settings")
        case .AboutUs:
            self.mainViewController.pushViewController(viewControllerName: "AboutViewController", storyBoardName: "Settings")
            
        case .Language:
            self.languageAction()
            
        case .Authorization:
            if UserManager.sharedInstance.getUser() == nil {
                //go to register
                self.clearDataAndStart()
            }else {
                self.logOut()
            }
        case .FAQ:
            goToFAQ()
       
            
        }
        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ENGLISH {
         self.slideMenuController()?.closeLeft()
        } else {
        self.slideMenuController()?.closeRight()
        }
    }
    
    func goToFAQ(){
        let storyBoard = UIStoryboard.init(name: "Jobs", bundle: nil)
        let viewAnswerQuesVC = storyBoard.instantiateViewController(withIdentifier: "ViewAnswersViewController") as! ViewAnswersViewController
        viewAnswerQuesVC.viewAnswersCase = .FAQ
        
        self.mainViewController.navigationController?.pushViewController(viewAnswerQuesVC, animated: true)
    }
    
    func logOut(){
        if UserManager.sharedInstance.getUser() == nil {
            self.showErrorMessage(message: "you have to login first".localized())
        }else {
            
            let alert = UIAlertController(title: "Logout".localized(), message: "Are you Shure to Logout ?".localized(), preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: .default , handler:{ (UIAlertAction)in
                self.logoutPresenter.doLogout()
                //self.clearDataAndStart()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel , handler:{ (UIAlertAction)in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: {
                
            })
        }
    }
    func languageAction(){
        if let selectedIndex = selectedIndexPath {
            let cell = self.menuTableView.cellForRow(at: selectedIndex) as! MenuTableViewCell
            let genderPicker = UIPickerView()
            
            
            var startIndex = 0
            if MoLocalization.currentAppleLanguage() == MoLocalization.Language.ARABIC {
                startIndex = 1
            }
            genderPicker.addCustomPicker(languageArray as NSArray , cell.languageBtn , "Language".localized(), startIndex) { (selectedIndex) in
                    switch(selectedIndex) {
                    case 0 :
                        cell.languageLabel.text = self.languageArray[0]
                        MoLocalization.setCurrentLang(lang:MoLocalization.Language.ENGLISH)
                    case 1 :
                        cell.languageLabel.text = self.languageArray[1]
                        MoLocalization.setCurrentLang(lang:MoLocalization.Language.ARABIC)
                    default:
                        cell.languageLabel.text = self.languageArray[0]
                        MoLocalization.setCurrentLang(lang:MoLocalization.Language.ENGLISH)
                    }
//                print(genderPicker.numberOfRows(inComponent: 0))
//                    if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ARABIC {
//                        genderPicker.selectedRow(inComponent: 0)
//                    }else {
//                        genderPicker.selectedRow(inComponent: 1)
//                    }
                    self.handleLanguage()
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.setupMenu()
                    
            }
        }
    }

    @IBAction func goToUserProfileBtnAction(_ sender: UIButton) {
        self.mainViewController.pushViewController(viewControllerName: "MyProfileViewController", storyBoardName: "Profile")
        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ENGLISH {
            self.slideMenuController()?.closeLeft()
        }else {
        self.slideMenuController()?.closeRight()
        }
    }
}
extension RightViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOfData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ARABIC {
            cell.languageLabel.text = self.languageArray[1]
        }else {
            cell.languageLabel.text = self.languageArray[0]
        }
        cell.setData(arrOfData[indexPath.row])
        if let menu = Menu(rawValue: indexPath.row) {
            if UserManager.sharedInstance.getUser() != nil {
                cell.languageLabel.alpha = 0
                cell.languageBtn.isHidden = true
                switch menu {
                    
                case  .Catgoery, .Terms , .Notifications , .AboutUs , .FAQ :
                    cell.underLineView.alpha = 1
                case .Language:
                   break
                case .Authorization:
                    cell.underLineView.alpha = 0
                
                }
            }else {
              
                switch menu {
                    
                case  .Catgoery, .Terms , .Notifications , .AboutUs , .FAQ:
                    cell.underLineView.alpha = 1
                    cell.languageLabel.alpha = 0
                    cell.languageBtn.isHidden = true
                case .Language:
                    cell.underLineView.alpha = 1
                    cell.languageLabel.alpha = 1
                    cell.languageBtn.isHidden = false
                case .Authorization:
                    cell.underLineView.alpha = 0
                    cell.languageLabel.alpha = 0
                    cell.languageBtn.isHidden = true
                
                }
            }
        }
        return cell
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedMenu:Menu?
        if UserManager.sharedInstance.getUser() != nil {
            //logged in
            if indexPath.row == 5 {
                selectedMenu = Menu.Authorization
            }else {
                if let menu = Menu(rawValue: indexPath.row) {
                    selectedMenu = menu
                }
            }
        }else {
            //not logged in
            if let menu = Menu(rawValue: indexPath.row) {
                selectedMenu = menu
            }
        }
        selectedIndexPath = IndexPath(row: indexPath.row, section: indexPath.section)
        self.changeViewController(selectedMenu!)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
extension RightViewController:LogoutView {
    func showLoadingIndicator() {
        Loader.showProgress()
    }
    
    func hideLoadingIndicator() {
        Loader.hidePrgoress()
    }
    
    func onSucess(message: String) {
        // self.showSucessMessage(message: message)
        self.clearDataAndStart()
    }
    
    func onError(message: String) {
        self.showErrorMessage(message:message)
    }
    
    
}
