//
//  JobDetailsViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 8/26/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import DynamicBlurView
import SafariServices
//import GoogleMobileAds

enum JobDetailsStatus {
    case unregistered
    case applytojob
    case viewanswers
    case answerQues
    case finishDone
}
//,GADFullScreenContentDelegate
class JobDetailsViewController: UIViewController {
    
    //var interstitial: GADInterstitialAd?
    //var bannerView: GADBannerView!
    var numberOfAppliedJobsThisMonth:Int?
    var FullScreenAdAfter:Int?
    
    var from:String = ""
    var titleMsg:String = ""
    var fromApplied = false
    @IBOutlet weak var salaryHeaderLbl: UILabel!
    @IBOutlet weak var ReqsLbl: UILabel!
    @IBOutlet weak var jobDetailsLbl: UILabel!
    @IBOutlet weak var jobNameDetailsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var requirementsTabelView: UITableView!
    @IBOutlet weak var jobDetailsBtn: TransitionButton!
    @IBOutlet weak var jobCatgoeryImageView: UIImageView!
    @IBOutlet weak var jobDescriptionLabel: UILabel!
    @IBOutlet weak var companyAddressLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var jobType: UIButton!
    @IBOutlet weak var jobName: UILabel!
    //@IBOutlet weak var notificationCountLbl: UILabel!
    @IBOutlet weak var salaryLbl: UILabel!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var jobDateLabel: UILabel!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var jobDetails_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var req_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var jobDetailsScrollView: UIScrollView!
    @IBOutlet weak var salary_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var job_details_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var view_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var expiredJobLbl: UILabel!
    @IBOutlet weak var applyingDate: UILabel!
    @IBOutlet weak var noOfVacancies: UILabel!
    @IBOutlet weak var noOfVacsVal: UILabel!
    @IBOutlet weak var appliedDateHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vacanciesHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var jobTypeWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var jobStatusIcon: UIImageView!
    @IBOutlet weak var jobStatusLbl: UILabel!
    @IBOutlet weak var jobStatus_height_constrain: NSLayoutConstraint!
    @IBOutlet weak var jobStatus_img_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var trackJobSTVHeightConst: NSLayoutConstraint!
    @IBOutlet weak var before_app_reviewd_view: UIView!
    @IBOutlet weak var app_reviewed_view: UIView!
    @IBOutlet weak var after_app_reviewed_view: UIView!
    @IBOutlet weak var afterJobApplied: UIView!
    @IBOutlet weak var before_accepted_interview_view: UIView!
    
    @IBOutlet weak var interviewSTV: UIStackView!
    @IBOutlet weak var accepted_interview_view: UIView!
    @IBOutlet weak var after_accepted_interview_view: UIView!
    @IBOutlet weak var accept_reject_view: UIView!
    @IBOutlet weak var accept_reject_img: UIImageView!
    @IBOutlet weak var jobStatus_img_top_constraint: NSLayoutConstraint!
    @IBOutlet weak var accept_reject_lbl: UILabel!
    @IBOutlet weak var before_accept_reject_view: UIView!
    @IBOutlet weak var trackJobHeightConst: NSLayoutConstraint!
    @IBOutlet weak var jobAppliedLbl: UILabel!
    @IBOutlet weak var AppRevieedLbl: UILabel!
    @IBOutlet weak var interViewLbl: UILabel!
    @IBOutlet weak var AcceptLbl: UILabel!
    @IBOutlet weak var disabilityEnabled_IC: UIImageView!
    @IBOutlet weak var disabilityEnabledLbl: UILabel!
    @IBOutlet weak var trackJobSTV: UIStackView!
    @IBOutlet weak var disabilityEnabledHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var qualifications_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var qualificationsLbl: UILabel!
    @IBOutlet weak var qualifications_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var qualifications_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var speciality_hieght_constraint: NSLayoutConstraint!
    @IBOutlet weak var specialityDataLbl: UILabel!
    @IBOutlet weak var speciality_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var eperience_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var experience_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var eperience_dataLbl: UILabel!
    @IBOutlet weak var languages_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var languages_dataLbl: UILabel!
    @IBOutlet weak var languages_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var age_dataLbl: UILabel!
    @IBOutlet weak var age_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var age_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var gender_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var gender_dataLbl: UILabel!
    @IBOutlet weak var gender_data_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var progressBar3: ProgressBar!
    @IBOutlet weak var qualification_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var qualification_top_constraint: NSLayoutConstraint!
    @IBOutlet weak var qualification_data_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var speciality_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var speciality_data_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var exp_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var exp_data_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var lang_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var lang_data_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var age_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var age_data_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var gender_bottom_constraint: NSLayoutConstraint!
    @IBOutlet weak var gender_data_bottom_constraint: NSLayoutConstraint!
    
    var spinnerView: UIView?
    ///////////////////////////////////////////////
    var job:jobDetail?
    var jobIDForDetails = 0
    var jobItem: jobItem2ForAds?
    var myQuestionAnswersArr = [QuestionsAnswer]()
    var jobDetailsStatus : JobDetailsStatus = .applytojob
    var applyForJobRequestObj : QuestionListRequest?
    var applyToJobPresenter = ApplyToJobPresenter()
    var jobDetailsPresenter = JobDetailsPresenter()
    var jobRequirementsArray = [String] ()
    var interstitialNotLoaded = true
    var cancelApply = false
    var cellHeights: [IndexPath : CGFloat] = [
        :]
    var jobExpired = false
    override func viewDidAppear(_ animated: Bool) {
        
        //        let notiCount = UserManager.sharedInstance.notificationCount
        //        if notiCount > 0 {
        //            self.notificationCountLbl.text = (" \(String(notiCount)) ")
        //            self.notificationCountLbl.isHidden = false
        //        }
        //        else{
        //             self.notificationCountLbl.isHidden = true
        //        }
        if UserManager.sharedInstance.jobAppliedOrCanceled > 0 {
            if UserManager.sharedInstance.jobIndexAppliedOrCanceled != nil {
                self.jobDetailsPresenter.doGetJobDetails(jobId: jobIDForDetails)
            }
        }
        self.EvaluateApp()
//        if let subscribedUser = UserManager.sharedInstance.subscribedUser {
//            if !subscribedUser {
//                print(" subscribedUser from details : \(subscribedUser)")
//        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
//        addBannerViewToView(bannerView)
//        bannerView.adUnitID = KeysAndValues.BannerViewAdUnitID
//        bannerView.rootViewController = self
//        let frame = { () -> CGRect in
//          if #available(iOS 11.0, *) {
//            return view.frame.inset(by: view.safeAreaInsets)
//          } else {
//            return view.frame
//          }
//        }()
//        let viewWidth = frame.size.width
//        bannerView.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth)
//        bannerView.load(GADRequest())
//                
//                
//                
//                
//            }}
        
    }
    
//    func addBannerViewToView(_ bannerView: GADBannerView) {
//      bannerView.translatesAutoresizingMaskIntoConstraints = false
//      view.addSubview(bannerView)
//      view.addConstraints(
//        [NSLayoutConstraint(item: bannerView,
//                            attribute: .bottom,
//                            relatedBy: .equal,
//                            toItem: bottomLayoutGuide,
//                            attribute: .top,
//                            multiplier: 1,
//                            constant: 0),
//         NSLayoutConstraint(item: bannerView,
//                            attribute: .centerX,
//                            relatedBy: .equal,
//                            toItem: view,
//                            attribute: .centerX,
//                            multiplier: 1,
//                            constant: 0)
//        ])
//     }
    
    @IBAction func notificationOpenAction(_ sender: Any) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
//    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//      print("Ad did present full screen content.")
//    }
//
//    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
//    {
//      print("Ad failed to present full screen content with error \(error.localizedDescription).")
//    }
//
//    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//      print("Ad did dismiss full screen content.")
//    }
    @objc func EvaluateApp(){
        if UserManager.sharedInstance.userShouldAddFeedBack {
            UserManager.sharedInstance.userShouldAddFeedBack = false
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "EvaluateApp"), object: nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                let alert = UIAlertController(title: "Feedbacks".localized(), message: "Please evaluate app and send your feedback to enhance the app experience.".localized(), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Evaluate Now".localized(), style: .default, handler: { action in
                    if let viewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "FeedBackViewCtrl") as? FeedBackVCtrl {
                        
                        if let navigator = self.navigationController {
                            navigator.pushViewController(viewController, animated: false)
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title: "Later".localized(), style: .default, handler: { action in
                    alert.dismiss(animated: true)
                }))
                if let presentedVC = self.presentedViewController{
                    presentedVC.present(alert, animated: true, completion: nil)
                }else{
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        requirementsTabelView.rowHeight = UITableView.automaticDimension
        self.applyToJobPresenter.attachView(view: self)
        self.jobDetailsPresenter.attachView(view: self)
        initPullToRefresh()
        self.jobExpired = jobItem?.jobExpired ?? false
        jobName.text = jobItem?.title
        jobType.setTitle("\(jobItem?.job_type_name ?? "") - \(jobItem?.workTimeText ?? "")" , for: [])
        let jobTypeString = "\(jobItem?.job_type_name ?? "") - \(jobItem?.workTimeText ?? "")"
        let btnWidth = jobTypeString.width(withConstrainedHeight: 24, font: UIFont(name: "HelveticaNeueLTArabic-Bold", size: 11.0)!)
        jobTypeWidthConstrain.constant = btnWidth + 20
        jobType.sizeToFit()
        if let text_color = jobItem?.job_type_color {
            jobType.backgroundColor = UIColor(hexString: text_color)
        }
        if let image = jobItem?.imagePath {
            if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                if subscribedUser || fromApplied {
                    if let catgoeryImgUrl = URL(string: image){
                        jobCatgoeryImageView.kf.setImage(with: catgoeryImgUrl)
                        jobCatgoeryImageView.kf.setImage(
                            with: catgoeryImgUrl,
                            placeholder: UIImage(named: "company_ic_large"),
                            options: [
                                .scaleFactor(UIScreen.main.scale),
                                .transition(.fade(1)),
                                .cacheOriginalImage
                            ])
                        {
                            result in
                            switch result {
                            case .success(let value):
                                print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            case .failure(let error):
                                print("Job failed: \(error.localizedDescription)")
                            }
                        }
                        
                    }
                }
            }
        }else {
            jobCatgoeryImageView.image = UIImage(named: "company_ic_large")
        }
        if let disabilityEnabled = jobItem?.DisabilityEnabled {
            if disabilityEnabled == 1 {
                disabilityEnabledLbl.isHidden = false
                disabilityEnabled_IC.isHidden = false
                disabilityEnabledHeightConstrain.constant = 62
            }
            else{
                disabilityEnabledLbl.isHidden = true
                disabilityEnabled_IC.isHidden = true
                disabilityEnabledHeightConstrain.constant = 42
            }
        }
        else{
            disabilityEnabledLbl.isHidden = true
            disabilityEnabled_IC.isHidden = true
            disabilityEnabledHeightConstrain.constant = 42
        }
        self.companyAddressLabel.text = jobItem?.city
        if let creationDate = jobItem?.created_at {
            self.jobDateLabel.text = creationDate.getFormattedDate()
            if let expired_date = jobItem?.expireDate {
                           self.jobDateLabel.text = creationDate.getFormattedDate() + " : " + (expired_date + " 00:00:00").getFormattedDate()
            }
        }
        
        if self.jobExpired {
            DispatchQueue.main.async {
                self.expiredJobLbl.isHidden = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.expiredJobLbl.isHidden = true
            }
        }
        if let subscribedUser = UserManager.sharedInstance.subscribedUser {
            if subscribedUser || fromApplied {
                self.companyNameLabel.text = jobItem?.companyName
            }
        }
        
        NotificationCenter.default.removeObserver(self, name: .afterUserLogin, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.afterUserLoginAction), name: .afterUserLogin , object: nil)
        self.view.changeFont()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if let jobId = self.jobItem?.id{
            self.jobIDForDetails = jobId
            self.jobDetailsPresenter.doGetJobDetails(jobId: jobId)
        }
        else if UserManager.sharedInstance.openDynamicLinkJobID > -1 {
            UserManager.sharedInstance.openDynamicLink = false
            self.jobIDForDetails = UserManager.sharedInstance.openDynamicLinkJobID
            self.jobDetailsPresenter.doGetJobDetails(jobId: UserManager.sharedInstance.openDynamicLinkJobID)
        }
        
    }
    func initPullToRefresh()
    {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        jobDetailsScrollView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.tableViewHeightConstraint.constant = 1
            self?.jobDetailsPresenter.doGetJobDetails(jobId: self?.jobIDForDetails ?? 0)
            self?.jobDetailsScrollView.dg_stopLoading()
            }, loadingView: loadingView)
        self.jobDetailsScrollView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.jobDetailsScrollView?.dg_setPullToRefreshBackgroundColor(jobDetailsScrollView.backgroundColor!)
    }
    @objc func afterUserLoginAction() {
        // Code Here.
        if let jobId = self.jobItem?.id{
            self.jobDetailsPresenter.doGetJobDetails(jobId: jobId)
        }
    }
    func registerCell() {
        
        requirementsTabelView.register(UINib(nibName: "RequirementsTableViewCell", bundle: nil), forCellReuseIdentifier: "RequirementsTableViewCell")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
    }
    
//    func loadInterstitial() {
//        print("loadInterstitial")
//     let request = GADRequest()
//     GADInterstitialAd.load(
//        withAdUnitID: KeysAndValues.InterstitialAdUnitID, request: request
//     ) { (ad, error) in
//       if let error = error {
//         print("Failed to load interstitial ad with error: \(error.localizedDescription)")
//         return
//       }
//       self.interstitial = ad
//       self.interstitial?.fullScreenContentDelegate = self
//       self.interstitial?.present(fromRootViewController: self)
//     }
//   }
    
    func setDetails() {
        
//        if let subscribedUser = UserManager.sharedInstance.subscribedUser {
//            if !subscribedUser {
//
//                if numberOfAppliedJobsThisMonth! >= FullScreenAdAfter!{
//                    if self.interstitialNotLoaded {
//                        self.interstitialNotLoaded = false
//                        self.loadInterstitial()
//                    }
//
//
//                }
//           }}
        
        if let job = self.job {
            
            jobName.text = job.title
            jobType.setTitle("\(job.job_type_name  ?? "" ) - \(job.workTimeText ?? "")" , for: [])
            let jobTypeString = "\(job.job_type_name  ?? "" ) - \(job.workTimeText ?? "")"
            let btnWidth = jobTypeString.width(withConstrainedHeight: 24, font: UIFont(name: "HelveticaNeueLTArabic-Bold", size: 11.0)!)
            jobTypeWidthConstrain.constant = btnWidth + 20
            if let text_color = job.job_type_color {
                jobType.backgroundColor = UIColor(hexString: text_color)
            }
            if let disabilityEnabled = job.DisabilityEnabled {
                if disabilityEnabled == 1 {
                    disabilityEnabledLbl.isHidden = false
                    disabilityEnabled_IC.isHidden = false
                    disabilityEnabledHeightConstrain.constant = 62
                }
                else{
                    disabilityEnabledLbl.isHidden = true
                    disabilityEnabled_IC.isHidden = true
                    disabilityEnabledHeightConstrain.constant = 42
                }
            }
            else{
                disabilityEnabledLbl.isHidden = true
                disabilityEnabled_IC.isHidden = true
                disabilityEnabledHeightConstrain.constant = 42
            }
            if let userValidity = job.userJobValidatiy, userValidity > 0 {
                let userValidityDivision = (userValidity / 100.0)
                let roundedVal = (userValidityDivision*100).rounded()/100
                self.progressBar3.progress = (CGFloat(roundedVal)*100).rounded()/100
            }
            if let subscribedUser = job.subscribedUser {
                if subscribedUser {
                    UserManager.sharedInstance.subscribedUser = true
                    if let image = job.imagePath {
                        if let catgoeryImgUrl = URL(string: image){
                            jobCatgoeryImageView.kf.setImage(with: catgoeryImgUrl)
                            jobCatgoeryImageView.kf.setImage(
                                with: catgoeryImgUrl,
                                placeholder: UIImage(named: "company_ic_large"),
                                options: [
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1)),
                                    .cacheOriginalImage
                                ])
                            {
                                result in
                                switch result {
                                case .success(let value):
                                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                case .failure(let error):
                                    print("Job failed: \(error.localizedDescription)")
                                }
                            }
                        }
                        
                    }else {
                        jobCatgoeryImageView.image = UIImage(named: "company_ic_large")
                    }
                }
            }
            
            
            self.companyAddressLabel.text = job.city
            if let creationDate = job.created_at {
                self.jobDateLabel.text = creationDate.getFormattedDate()
                if let expired_date = job.expireDate {
                self.jobDateLabel.text = creationDate.getFormattedDate() + " : " + (expired_date + " 00:00:00").getFormattedDate()
                }
            }
            var notExpired = true
            if let expired_date = job.expireDate {
            let status = expired_date.checkExiration()
                if status {
                    notExpired = false
                    DispatchQueue.main.async {
                        self.expiredJobLbl.isHidden = false
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.expiredJobLbl.isHidden = true
                    }
                }
            }
            else{
                notExpired = true
                DispatchQueue.main.async {
                    self.expiredJobLbl.isHidden = true
                }
            }
            if let subscribedUser = job.subscribedUser {
                if subscribedUser {
                    self.companyNameLabel.text = job.companyName
                }
            }
            
            if let jobStatus = job.job_status, jobStatus != 0, notExpired {
                DispatchQueue.main.async {
//                    [11:48 AM, 4/22/2021] Maged: 1 -> appliedJop
//                    [11:49 AM, 4/22/2021] Maged: 4 -> application reviewed
//                    [11:49 AM, 4/22/2021] Maged: 3 -> rejected After application reviewed
//                    [11:49 AM, 4/22/2021] Maged: 5 -> accept for Interview
//                    [11:49 AM, 4/22/2021] Maged: 6 ->rejected After interview
//                    [11:50 AM, 4/22/2021] Maged: 2 ->accept After interview
                    self.trackJobSTV.isHidden = false
                    self.interviewSTV.isHidden = false
                    self.jobAppliedLbl.text = NSLocalizedString("Job applied", comment: "")
                    self.AppRevieedLbl.text = NSLocalizedString("Application reviewed", comment: "")
                    self.interViewLbl.text = NSLocalizedString("Accepted for interview", comment: "")
                    self.AcceptLbl.text = NSLocalizedString("Accepted", comment: "")
                    self.cancelApply = true
                    if jobStatus == 4 {
                        self.afterJobApplied.backgroundColor = UIColor(red: 22/255, green: 22/255, blue: 80/255, alpha: 1)
                        self.before_app_reviewd_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
//                        self.app_reviewed_view.backgroundColor = UIColor(red: 158/255, green: 117/255, blue: 178/255, alpha: 1)
                        self.app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                    }
                    else if jobStatus == 3 {
                        self.afterJobApplied.backgroundColor = UIColor(red: 22/255, green: 22/255, blue: 80/255, alpha: 1)
                        self.before_app_reviewd_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.after_app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.before_accepted_interview_view.backgroundColor = .red
                        self.interviewSTV.isHidden = true
                        self.accept_reject_view.backgroundColor = .red
                        self.accept_reject_img.image = UIImage(named: "rejected_ic")
                        self.AcceptLbl.text = NSLocalizedString("Rejected", comment: "")
                        self.jobDetailsBtn.isUserInteractionEnabled = false
                        self.cancelApply = false
                    }
                    else if jobStatus == 5 {
                        self.afterJobApplied.backgroundColor = UIColor(red: 22/255, green: 22/255, blue: 80/255, alpha: 1)
                        self.before_app_reviewd_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.after_app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.before_accepted_interview_view.backgroundColor = UIColor(red: 255/255, green: 197/255, blue: 57/255, alpha: 1)
                        self.accepted_interview_view.backgroundColor = UIColor(red: 255/255, green: 197/255, blue: 57/255, alpha: 1)
                        self.cancelApply = true
                    }
                    else if jobStatus == 2 {
                        self.afterJobApplied.backgroundColor = UIColor(red: 22/255, green: 22/255, blue: 80/255, alpha: 1)
                        self.before_app_reviewd_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.after_app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.before_accepted_interview_view.backgroundColor = UIColor(red: 255/255, green: 197/255, blue: 57/255, alpha: 1)
//                        self.accepted_interview_view.backgroundColor = UIColor(red: 245/255, green: 130/255, blue: 56/255, alpha: 1)
                        self.accepted_interview_view.backgroundColor = UIColor(red: 255/255, green: 197/255, blue: 57/255, alpha: 1)
                        self.after_accepted_interview_view.backgroundColor = UIColor(red: 245/255, green: 130/255, blue: 56/255, alpha: 1)
                        self.before_accept_reject_view.backgroundColor = UIColor(red: 44/255, green: 154/255, blue: 66/255, alpha: 1)
                        self.accept_reject_view.backgroundColor = UIColor(red: 44/255, green: 154/255, blue: 66/255, alpha: 1)
                        self.cancelApply = false
                    }
                    else if jobStatus == 6 {
                        self.afterJobApplied.backgroundColor = UIColor(red: 22/255, green: 22/255, blue: 80/255, alpha: 1)
                        self.before_app_reviewd_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.after_app_reviewed_view.backgroundColor = UIColor(red: 0/255, green: 204/255, blue: 255/255, alpha: 1)
                        self.before_accepted_interview_view.backgroundColor = UIColor(red: 255/255, green: 197/255, blue: 57/255, alpha: 1)
                        self.accepted_interview_view.backgroundColor = UIColor(red: 255/255, green: 197/255, blue: 57/255, alpha: 1)
                        self.after_accepted_interview_view.backgroundColor = UIColor(red: 245/255, green: 130/255, blue: 56/255, alpha: 1)
                        self.before_accept_reject_view.backgroundColor = .red
                        self.accept_reject_view.backgroundColor = .red
                        self.accept_reject_img.image = UIImage(named: "rejected_ic")
                        self.AcceptLbl.text = NSLocalizedString("Rejected", comment: "")
                        self.jobDetailsBtn.isUserInteractionEnabled = false
                        self.cancelApply = false
                        
                    }
                    self.trackJobSTVHeightConst.constant = 105
                    self.view_height_constraint.constant += 105
                    if self.cancelApply {
                        self.jobDetailsBtn.backgroundColor = .red
                        self.jobDetailsBtn.setTitle("Cancel Your Applocation".localized(), for: .normal)
                        self.jobDetailsBtn.setTitle("Cancel Your Applocation".localized(), for: .selected)
                        self.jobDetailsBtn.isHidden = false
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    self.view_height_constraint.constant -= self.trackJobSTVHeightConst.constant
                    self.trackJobSTVHeightConst.constant = 0
                    self.trackJobSTV.isHidden = true
                    self.cancelApply = false
                }
            }
            self.jobDescriptionLabel.text = job.description ?? ""
            if self.jobDescriptionLabel.text?.count ?? 0 > 0 {
                DispatchQueue.main.async {
                    self.jobDetailsLbl.isHidden = false
                }
            }
            else{
                DispatchQueue.main.async {
                    self.jobDetails_height_constraint.constant = 0
                    self.job_details_data_height_constraint.constant = 0
                }
            }
            
            if UserManager.sharedInstance.getUser() != nil {
                if let appliedFlag = job.appliedForJob {
                    if appliedFlag {
                        self.jobDetailsStatus = .finishDone
                        
                    }else {
                        //logged in user
                        if let questCount = job.questionsCount {
                            if questCount > 0 {
                                self.jobDetailsStatus = .answerQues
                            }
                            else{
                                self.jobDetailsStatus = .applytojob
                            }
                        }
                        
                    }
                }else {
                    if let questCount = job.questionsCount {
                        if questCount > 0 {
                            self.jobDetailsStatus = .answerQues
                        }
                        else{
                            self.jobDetailsStatus = .applytojob
                        }
                    }
                }
                
            }else {
                //skipped user
                self.jobDetailsStatus = .unregistered
            }
        }
        if let jobrequirementsArr = job?.requirements {
            self.jobRequirementsArray = [String]()
            self.jobRequirementsArray = jobrequirementsArr
            DispatchQueue.main.async {
                if jobrequirementsArr.count > 0 {
                    if jobrequirementsArr.count == 1 {
                        let trimedReq = jobrequirementsArr[0].components(separatedBy: .whitespaces).joined()
                        if trimedReq.count == 0 {
                            self.ReqsLbl.isHidden = true
                            DispatchQueue.main.async {
                                self.req_height_constraint.constant = 0
                            }
                        }
                        else{
                            self.ReqsLbl.isHidden = false
                            self.requirementsTabelView.reloadData()
                        }
                    }
                    else{
                        self.ReqsLbl.isHidden = false
                        self.requirementsTabelView.reloadData()
                    }
                }
                else{
                    self.ReqsLbl.isHidden = true
                    DispatchQueue.main.async {
                        self.req_height_constraint.constant = 0
                    }
                }
            }
        }
        if let appliedDate = job?.appliedForJobDate {
            if !appliedDate.isEmpty {
                DispatchQueue.main.async {
                    self.appliedDateHeightConstraint.constant = 26
                    self.applyingDate.text = "You applied to the job at ".localized() + appliedDate.getFormattedDate()
                }
            }
            else{
                DispatchQueue.main.async {
                    self.appliedDateHeightConstraint.constant = 0
                    self.applyingDate.text = "".localized()
                }
            }
        }
        else{
            DispatchQueue.main.async {
                self.appliedDateHeightConstraint.constant = 0
                self.applyingDate.text = "".localized()
            }
        }
        if let vacancies = job?.candidateNumbers {
            if vacancies > 0 {
                DispatchQueue.main.async{
                    self.vacanciesHeightConstraint.constant = 26
                    self.noOfVacsVal.text = String(vacancies)
                }
            }
            
        }
        if let qualifications = job?.Qualifications, !qualifications.isBlank {
            self.qualifications_height_constraint.constant = 25
            self.qualification_top_constraint.constant = 2
            self.qualification_bottom_constraint.constant = 2
            self.qualification_data_bottom_constraint.constant = 2
            self.qualificationsLbl.text = qualifications
        }
        else{
            self.qualifications_height_constraint.constant = 0
            self.qualification_bottom_constraint.constant = 0
            self.qualification_data_bottom_constraint.constant = 0
        }
        if let speciality = job?.specialityName, !speciality.isBlank {
            self.speciality_hieght_constraint.constant = 25
            self.speciality_bottom_constraint.constant = 2
            self.speciality_data_bottom_constraint.constant = 2
            self.specialityDataLbl.text = speciality
        }
        else{
            self.speciality_hieght_constraint.constant = 0
            self.speciality_bottom_constraint.constant = 0
            self.speciality_data_bottom_constraint.constant = 0
        }
        if let experience = job?.experienceName, !experience.isBlank {
            self.experience_height_constraint.constant = 25
            self.exp_bottom_constraint.constant = 2
            self.exp_data_bottom_constraint.constant = 2
            self.eperience_dataLbl.text = experience
        }
        else{
            self.experience_height_constraint.constant = 0
            self.exp_bottom_constraint.constant = 0
            self.exp_data_bottom_constraint.constant = 0
        }
        if let languages = job?.JobLanguages {
            if languages.count > 0 {
                self.languages_height_constraint.constant = 25
                self.lang_bottom_constraint.constant = 2
                self.lang_data_bottom_constraint.constant = 2
                self.languages_dataLbl.text = L102Language.isRTL ? (languages.map{String($0.nameAr ?? "")}).joined(separator: ",") : (languages.map{String($0.nameEn ?? "")}).joined(separator: ",")
            }
            else{
                self.languages_data_height_constraint.constant = 0
                self.lang_bottom_constraint.constant = 0
                self.lang_data_bottom_constraint.constant = 0
            }
        }
        else{
            self.languages_data_height_constraint.constant = 0
            self.lang_bottom_constraint.constant = 0
            self.lang_data_bottom_constraint.constant = 0
        }
        if let ageFrom = job?.ageFrom, ageFrom > 0 {
            self.age_height_constraint.constant = 25
            var ageDesc = "From".localized() + " " + String(ageFrom) + " " + "Years".localized()
            if let ageTo = job?.ageTo, ageTo > 0 {
                ageDesc += " " + "To".localized() + " " + String(ageTo) + " " + "Years".localized()
            }
            self.age_bottom_constraint.constant = 2
            self.age_data_bottom_constraint.constant = 2
            self.age_dataLbl.text = ageDesc
        }
        else{
            self.age_bottom_constraint.constant = 0
            self.age_data_bottom_constraint.constant = 0
            self.age_height_constraint.constant = 0
        }
        if let gender = job?.gender {
            self.gender_height_constraint.constant = 25
            self.gender_bottom_constraint.constant = 2
            self.gender_data_bottom_constraint.constant = 2
            if gender == 1 {
                self.gender_dataLbl.text = "Male".localized()
            }
            else if gender == 2 {
                self.gender_dataLbl.text = "Female".localized()
            }
            else if gender == 3 {
                self.gender_dataLbl.text = "Male".localized() + ", " + "Female".localized()
            }
            
        }
        else{
            self.gender_height_constraint.constant = 0
            self.gender_bottom_constraint.constant = 0
            self.gender_data_bottom_constraint.constant = 0
        }
        if let salary = job?.SalaryText {
            if !salary.isEmpty {
                self.salaryLbl.text = salary
                
                DispatchQueue.main.async {
                    self.salaryHeaderLbl.isHidden = false
                    self.salaryLbl.isHidden = false
                }
            }
            else{
//                DispatchQueue.main.async {
//                    self.salary_height_constraint.constant = 0
//                }
                self.salaryLbl.text = "We are sorry, the company did not provide salary".localized()
                DispatchQueue.main.async {
                    self.salaryHeaderLbl.isHidden = false
                    self.salaryLbl.isHidden = false
                }
            }
        }
        else{
        //                DispatchQueue.main.async {
        //                    self.salary_height_constraint.constant = 0
        //                }
                        self.salaryLbl.text = "We are sorry, the company did not provide salary".localized()
                        DispatchQueue.main.async {
                            self.salaryHeaderLbl.isHidden = false
                            self.salaryLbl.isHidden = false
                        }
                    }
        
        self.setUpJobBtnStatus()
        if job?.job_status == 2 || job?.job_status == 3 || job?.job_status == 6 {
            DispatchQueue.main.async {
                self.jobDetailsBtn.isHidden = true
            }
            
        }
        DispatchQueue.main.async {
            let constansHeight1 = self.qualifications_height_constraint.constant + self.qualifications_data_height_constraint.constant + self.speciality_hieght_constraint.constant + self.speciality_data_height_constraint.constant + self.experience_height_constraint.constant + self.eperience_data_height_constraint.constant
            let constansHeight2 = self.languages_height_constraint.constant + self.languages_data_height_constraint.constant + self.age_height_constraint.constant + self.age_data_height_constraint.constant + self.gender_height_constraint.constant + self.gender_data_height_constraint.constant + self.trackJobSTVHeightConst.constant
            self.view_height_constraint.constant = 600.0 + self.TopView.frame.height + self.jobDescriptionLabel.frame.height + self.requirementsTabelView.frame.height + constansHeight1 + constansHeight2
        }
    }
    
    func setUpJobBtnStatus(){
        switch jobDetailsStatus {
        case .applytojob:
            print("apply to job")
            self.jobDetailsBtn.setTitle("Apply for this Job".localized(), for: .normal)
            self.jobDetailsBtn.setTitleColor(UIColor.white, for: .normal)
            self.jobDetailsBtn.backgroundColor = UIColor(hexString: "#161650")
            
        case .unregistered:
            print("skipped users")
            self.jobDetailsBtn.setTitle("Register to Apply".localized(), for: .normal)
            self.jobDetailsBtn.setTitleColor(UIColor.white, for: .normal)
            self.jobDetailsBtn.backgroundColor = UIColor(hexString: "#161650")
            
        case .answerQues:
            print("go to answerQues")
            self.jobDetailsBtn.setTitle("Answer the questions to Apply".localized(), for: .normal)
            self.jobDetailsBtn.setTitleColor(UIColor.white, for: .normal)
            self.jobDetailsBtn.backgroundColor = UIColor(hexString: "#161650")
            
        case .viewanswers:
            print("view answers")
            if self.myQuestionAnswersArr.count == 0 {
                self.jobDetailsBtn.setTitle("View My Answers".localized(), for: .normal)
                self.jobDetailsBtn.setTitleColor(UIColor.white, for: .normal)
                self.jobDetailsBtn.backgroundColor = UIColor(hexString: "#161650")
            }else {
                self.jobDetailsBtn.setTitle("View My Answers".localized(), for: .normal)
                self.jobDetailsBtn.setTitleColor(UIColor.white, for: .normal)
                self.jobDetailsBtn.backgroundColor = UIColor(hexString: "#161650")
            }
            
        case .finishDone :
            print("finish")
            self.jobDetailsBtn.setTitle("Applied".localized(), for: .normal)
            self.jobDetailsBtn.setTitleColor(UIColor.white, for: .normal)
            self.jobDetailsBtn.backgroundColor = UIColor(hexString: "#00ccff")
        }
        DispatchQueue.main.async {
            if !self.jobExpired && !self.cancelApply{
                self.jobDetailsBtn.isHidden = false
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func jobDetailsBtnAction(_ sender: Any) {
        if let subscribedUser = UserManager.sharedInstance.subscribedUser {
            if !subscribedUser {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                    viewController.modalPresentationStyle = .overCurrentContext
                    viewController.from = "profile"
                    if let msg = UserManager.sharedInstance.NotSubscribedMeassage{
                        print("msg : \(msg)")
                        viewController.titleMsg = msg
                    }
                    self.present(viewController, animated: true, completion: nil)
                }
            }
            else{
//                var profileFilled = true
//                if let name = UserManager.sharedInstance.getUser()?.name {
//                    if name.count == 0 {
//                        profileFilled = false
//                    }
//                }
//                else{
//                    profileFilled = false
//                }
//                if let lastName = UserManager.sharedInstance.getUser()?.LastName {
//                    if lastName.count == 0 {
//                        profileFilled = false
//                    }
//                }
//                else{
//                    profileFilled = false
//                }
//                if let phone = UserManager.sharedInstance.getUser()?.mobile {
//                    if phone.count == 0 {
//                        profileFilled = false
//                    }
//                }
//                else{
//                    profileFilled = false
//                }
//                if let email = UserManager.sharedInstance.getUser()?.email {
//                    if email.count == 0 {
//                        profileFilled = false
//                    }
//                }
//                else{
//                    profileFilled = false
//                }
//                if let age = UserManager.sharedInstance.getUser()?.birthDate {
//                    if age.count == 0 {
//                        profileFilled = false
//                    }
//                }
//                else{
//                    profileFilled = false
//                }
////                if let gender = UserManager.sharedInstance.getUser()?.gender {
////                    if gender.count == 0 {
////                        profileFilled = false
////                    }
////                }
////                else{
////                    profileFilled = false
////                }
//                if let education = UserManager.sharedInstance.getUser()?.EducationalInformation {
//                    if education.count == 0 {
//                        profileFilled = false
//                    }
//                }
//                else{
//                    profileFilled = false
//                }
//                
//                if !profileFilled {
//                    from = "profileNotFilled"
//                    self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
//                    return
//                }
                switch jobDetailsStatus {
                case .applytojob:
                    print("apply to job")
                    if let userValidity = self.job?.userJobValidatiy, userValidity < 50.0 {
                        from = "userNotSuitablleToApply"
                        self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
                    }
                    else{
                        if let ExternalApply = self.job?.ExternalApply {
                            if ExternalApply == 1 {
                                self.jobDetailsPresenter.increaseClicks(jobId: (self.job?.id)!)
                                if let url = URL(string: self.job?.ExternalApplyLink ?? "" ){
                                    let vc = SFSafariViewController(url: url)
                                    present(vc, animated: true)
                                }
                            }
                            else{
                                self.applyToJobPresenter.applyToJob(questionAnswersObj: QuestionListRequest() , jobId: (self.job?.id)!)
                            }
                        }
                        else{
                            self.applyToJobPresenter.applyToJob(questionAnswersObj: QuestionListRequest() , jobId: (self.job?.id)!)
                        }
                    }
                    
                //
                case .unregistered:
                    print("skipped users ")
                    
                    if #available(iOS 13.0, *) {
                        let scene = UIApplication.shared.connectedScenes.first
                        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                            let mainwindow = sd.window
                            let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                            mainwindow?.rootViewController = stry.instantiateInitialViewController()
                        }
                    }
                    else{
                        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                        let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                        rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
                    }
                case .viewanswers:
                    print("view answers")
                    
                    
                    let storyBoard = UIStoryboard.init(name: "Jobs", bundle: nil)
                    let viewAnswerQuesVC = storyBoard.instantiateViewController(withIdentifier: "ViewAnswersViewController") as! ViewAnswersViewController
                    if self.myQuestionAnswersArr.count > 0 {
                        
                        viewAnswerQuesVC.viewAnswersCase = .viewAnswers
                        viewAnswerQuesVC.questionAnswersArr = self.myQuestionAnswersArr
                        self.navigationController?.pushViewController(viewAnswerQuesVC, animated: true)
                    }else {
                        self.showErrorMessage(message: "This job don't have questions and answers".localized())
                    }
                case .answerQues:
                    print("go to answerQues")
                    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let answerQuesVC = storyBoard.instantiateViewController(withIdentifier: "AnswerQuestionsViewCtrl") as! ApplyToJobVCtrl
                    answerQuesVC.job = self.job
                    
                    self.navigationController?.pushViewController(answerQuesVC, animated: true)
                    
                case .finishDone :
                    print("finish")
                if cancelApply {
                    self.jobDetailsPresenter.cancelApplyToJob(jobId: (self.job?.id)!)
                }
                else{
                    self.showSucessMessage(message: "you applied for this job".localized())
                }
                }
            }
        }
        
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        from = "jobDetailsVC"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CongratulationsViewController" {
            if let destinationVC = segue.destination as? CongratulationsViewController {
                
                destinationVC.from = self.from
                destinationVC.titleMsg = self.titleMsg
                if from == "userNotSuitablleToApply" {
                    destinationVC.applyToJobDelegate = self
                }
                else if from == "jobDetailsVC" {
                    destinationVC.applyToJobDelegate = self
                }
                
            }
            
        }
    }
    
    
}
//extension JobDetailsViewController:ApplyToJobView{
//    func showLoadingIndicator() {
//        self.view.isUserInteractionEnabled = false
//        self.jobDetailsBtn.startAnimation()
//    }
//
//    func hideLoadingIndicator() {
//        self.view.isUserInteractionEnabled = true
//        self.jobDetailsBtn.stopAnimation()
//    }
//
//    func onSucess() {
//        self.jobDetailsStatus = .finishDone
//        self.setUpJobBtnStatus()
//    }
//
//    func onError(message: String) {
//        self.showErrorMessage(message: message)
//    }
//
//
//}
extension JobDetailsViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("ccccddd \(self.jobRequirementsArray.count)")
        return  self.jobRequirementsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequirementsTableViewCell") as! RequirementsTableViewCell
        cell.requirementLabel.text = self.jobRequirementsArray[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
        self.tableViewHeightConstraint.constant += cellHeights[indexPath]!
        
        self.view_height_constraint.constant = 600.0 + self.TopView.frame.height + self.jobDescriptionLabel.frame.height + self.tableViewHeightConstraint.constant + self.qualifications_height_constraint.constant + self.qualifications_data_height_constraint.constant + self.speciality_hieght_constraint.constant + self.speciality_data_height_constraint.constant + self.experience_height_constraint.constant + self.eperience_data_height_constraint.constant + self.languages_height_constraint.constant + self.languages_data_height_constraint.constant + self.age_height_constraint.constant + self.age_data_height_constraint.constant + self.gender_height_constraint.constant + self.gender_data_height_constraint.constant + self.trackJobSTVHeightConst.constant
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return 44.0 }
        
        return height
    }
    
}

extension JobDetailsViewController:ApplyToJobView{
    func showLoadingApplyIndicator() {
        self.view.isUserInteractionEnabled = false
        // self.doneBtn.startAnimation()
    }
    
    func hideLoadingApplyIndicator() {
        self.view.isUserInteractionEnabled = true
        //   self.doneBtn.stopAnimation()
    }
    
    func onSucessApply(status:String,message:String) {
        if status == "1"{
            UserManager.sharedInstance.jobAppliedOrCanceled = 1
            from = "jobDetailsVC"
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
            
        }else if status == "-19"{
            from = "status-19"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }else if status == "20"{
            from = "profileNotFilled"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        else if status == "30"{
            from = "status-30"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        else{
            from = "status-1"
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        self.jobDetailsPresenter.doGetJobDetails(jobId: jobIDForDetails)
    }
    
    func onErrorApply(message: String) {
        if message == "401" {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                    viewController.modalPresentationStyle = .overCurrentContext
                    viewController.from = "401"
                    self.present(viewController, animated: true, completion: nil)
            }
        }
        else{
            self.showErrorMessage(message: message)
        }
        
    }
}
extension JobDetailsViewController:JobDetailsView{
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    func onSucessLoadingJob(job: jobDetail) {
        print("onSucessLoadingJob")
        self.job = job
        setDetails()
        
    }
func onSucessLoadingDatils(job:jobDetail,numberOfAppliedJobsThisMonth:Int,FullScreenAdAfter:Int){
    
        self.job = job
        self.numberOfAppliedJobsThisMonth = numberOfAppliedJobsThisMonth
        self.FullScreenAdAfter = FullScreenAdAfter
        setDetails()
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}
extension JobDetailsViewController: ApplyToJobDelegate {
    func OpenEvaluateApp() {
        self.EvaluateApp()
    }
    
    func ApplyToJob() {
        if let ExternalApply = self.job?.ExternalApply {
            if ExternalApply == 1 {
                self.jobDetailsPresenter.increaseClicks(jobId: (self.job?.id)!)
                if let url = URL(string: self.job?.ExternalApplyLink ?? "" ){
                    let vc = SFSafariViewController(url: url)
                    present(vc, animated: true)
                }
            }
            else{
                self.applyToJobPresenter.applyToJob(questionAnswersObj: QuestionListRequest() , jobId: (self.job?.id)!)
            }
        }
        else{
            self.applyToJobPresenter.applyToJob(questionAnswersObj: QuestionListRequest() , jobId: (self.job?.id)!)
        }
    }
}
extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        formatter.minimumFractionDigits = 2
        return formatter
    }()
}
extension Numeric {
    var formattedWithSeparator: String { Formatter.withSeparator.string(for: self) ?? "" }
}

protocol ApplyToJobDelegate {
    func ApplyToJob()
    func OpenEvaluateApp()
}
