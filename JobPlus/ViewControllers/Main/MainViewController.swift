//
//  MainViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/19/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
enum Sort {
    case Ascending
    case Descending
}
class MainViewController: UIViewController{
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var scrollViewContainer: UIScrollView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    var itemCountForWindless = 10
    let jobsTypesPresenter = JobsTypesPresenter()
    let cityListPresenter = CityListPresenter()
    let jobsPresenter = JobsPresenter()
    var genderArray = ["Male".localized() , "Female".localized()]
    var jobsTypesResponse:JobsTypesResponse?
    var jobsResponse:JobListResponse?
    var selectedCatgoeryId:Int?
    var selectedCategory:Catgoery?
    var selectedTypeId:Int?
    var selectedCityId:Int?
    var searchText:String?
    var sort = Sort.Descending
    var order = KeysAndValues.ORDERDESC
    var cityList = [City]()
    override func viewDidLoad() {
        super.viewDidLoad()
         registerCell()
        initPullToRefresh()
        initLoadMoreToRefresh()
        self.tableView.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        jobsTypesPresenter.attachView(view:self)
        jobsPresenter.attachView(view:self)
        cityListPresenter.attachView(view: self)
        jobsTypesPresenter.getJobsTypes()
        cityListPresenter.doGetCityList()
        searchTextField.delegate = self
        
        addBarButtonForMenu()
//        if MoLocalization.currentAppleLanguage() == MoLocalization.Language.ARABIC {
//            searchTextField.textAlignment = .right
//        }else{
//            searchTextField.textAlignment = .left
//        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.afterUserLoginAction), name: .afterUserLogin , object: nil)
        self.view.changeFont()
        
    }
    @objc func afterUserLoginAction() {
       // jobsTypesResponse = JobsTypesResponse()
        //jobsResponse = JobListResponse()
        jobsTypesPresenter.getJobsTypes()
    }
    func addBarButtonForMenu(){
        let menuButton   = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_side_menu") ,  style: .plain, target: self, action:  #selector(didTapOpenMenu))
        navigationItem.leftBarButtonItem = menuButton
    }
    
    @IBAction func searchBtnAction(_ sender: UIButton) {
        if searchTextField.isFirstResponder {
            searchTextField.resignFirstResponder()
        }else {
            searchTextField.becomeFirstResponder()
        }
    }
    @objc func didTapOpenMenu(sender: AnyObject){
//        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ENGLISH {
//            self.slideMenuController()?.openLeft()
//        } else {
//            self.slideMenuController()?.openRight()
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if let categoryName = selectedCategory?.name{
        self.title = categoryName
        }
        self.navigationController?.presentTransparentNavigationBar()
          // setNavigationBarTitleAndColor()
//         self.addBellRightButton()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func profileAction(_ sender: Any) {
        self.performSegue(withIdentifier:"MyProfileViewController", sender: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
    }
   
    func setNavigationBarTitleAndColor(){
        self.navigationController?.setNavigationTintColor(tintColor: UIColor.saffron)
        self.navigationController?.setNavigationBarTintColor(barTintColor: UIColor.frenchBlue)
        self.navigationController?.setNavigationBarTitleColor(color:UIColor.white, fontSize: 20)
       // self.title = "Part time"
    }
  
    func registerCell() {
        tableView.register(UINib(nibName: "JobTableViewCell", bundle: nil), forCellReuseIdentifier: "JobTableViewCell")
    }

    
    
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.jobsTypesPresenter.getJobsTypes()
            }, loadingView: loadingView)
        self.tableView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.tableView?.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
    func initLoadMoreToRefresh()
    {
        self.tableView?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.tableView?.addInfiniteScroll { (tableView) -> Void in
            
            if let metaData = self.jobsResponse?.meta {
                if let currentPage = metaData.currentPage , let lastPage = metaData.lastPage {
                    if currentPage == lastPage {
                        //ending paging
                        self.tableView.finishInfiniteScroll()
                    }else {
                        if self.jobsResponse?.meta?.links?.next != nil {
                            self.jobsPresenter.getJobs(link: self.jobsResponse?.meta?.links?.next as! String)
                        }
                    }
                }
            }
//            if self.jobsResponse?.meta?.links?.next != nil {
//                self.jobsPresenter.getJobs(link: self.jobsResponse?.meta?.links?.next as! String)
//            }else {
//                self.tableView.finishInfiniteScroll()
//            }
        }
        
    }
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollViewContainer.contentOffset.y)
//        if scrollView.contentOffset.y < 200 {
//            scrollViewContainer.isScrollEnabled = true
//            scrollViewContainer.setContentOffset(scrollView.contentOffset, animated: false)
//        }else {
//            scrollViewContainer.isScrollEnabled = false
//            scrollViewContainer?.contentOffset.y = 200
//            
//            //scrollViewContainer.isUserInteractionEnabled = false
//        }
    }
    
    @IBAction func showFilterAction(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Filter".localized(), message: "you can filter by ...".localized(), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "City".localized(), style: .default , handler:{ (UIAlertAction)in
            self.filterByCity(sender)
        }))
        
        alert.addAction(UIAlertAction(title: "Type".localized(), style: .default , handler:{ (UIAlertAction)in
            self.filterByType(sender)
        }))
        alert.addAction(UIAlertAction(title: "CancelPicker".localized(), style: .cancel , handler:{ (UIAlertAction)in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    func filterByType(_ sender:Any){
        let genderPicker = UIPickerView()
        // var genderArrayToDisplay = ["Male".localized() , "Female".localized()]
        if let jobsTypes = jobsTypesResponse?.jobsTypes {
            var jobsTypesNames = jobsTypes.map { $0.name}
            jobsTypesNames.append("All".localized())
            genderPicker.addCustomPicker(jobsTypesNames as NSArray , sender as! UIButton , "Filter".localized(), 0) { (selectedIndex) in
                
                let selectedJobType = jobsTypesNames[selectedIndex]
                if selectedJobType == "All".localized() {
                    self.selectedCatgoeryId = nil
                    self.selectedCategory = nil
                    self.selectedTypeId = nil
                    self.searchText = nil
                    self.selectedCityId = nil
                    self.jobsTypesPresenter.getJobsTypes()
                }else {
                    
                    if self.jobsTypesResponse != nil {
                        self.selectedTypeId = self.jobsTypesResponse?.jobsTypes![selectedIndex].id
                        self.jobsPresenter.getJobs(title: self.searchText, jobTypeID: self.selectedTypeId, jobCatgoeryID: self.selectedCatgoeryId , jobCityID: self.selectedCityId , order:self.order)
                    }
                }
            }
        }
    }
  
    func filterByCity(_ sender:Any){
        let cityPicker = UIPickerView()
        if  cityList.count > 0 {
            var citiesNames = cityList.map { $0.localizeName}
            citiesNames.append("All".localized())
            cityPicker.addCustomPicker(citiesNames as NSArray , sender as! UIButton , "Filter".localized(), 0) { (selectedIndex) in
                
                let selectedCity = citiesNames[selectedIndex]
                if selectedCity == "All".localized() {
                    self.selectedCatgoeryId = nil
                    self.selectedCategory = nil
                    self.selectedTypeId = nil
                    self.searchText = nil
                    self.selectedCityId = nil
                    self.jobsTypesPresenter.getJobsTypes()
                }else {
                    
                    self.selectedCityId = self.cityList[selectedIndex].id
                    self.jobsPresenter.getJobs(title: self.searchText, jobTypeID: self.selectedTypeId, jobCatgoeryID: self.selectedCatgoeryId  , jobCityID: self.selectedCityId , order:self.order)
                }
            }
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "JobDetailsViewController" {
            let job = sender as? Job
            let destination = segue.destination as?JobDetailsViewController
            destination?.job = job
       
        }
    }
 
    @IBAction func sort(_ sender: UIButton) {
        switch sort {
        case .Ascending:
            sort = .Descending
            sortButton.setImage(#imageLiteral(resourceName: "sortDescending"), for: .normal)
            print("Descending")
            
           // jobsResponse?.jobs = (jobsResponse?.jobs?.sorted(by: {$0.timestamp! < $1.timestamp!}))!
            self.order = KeysAndValues.ORDERDESC
            self.jobsTypesPresenter.getJobsTypes()
        case .Descending:
            sort = .Ascending
            sortButton.setImage(#imageLiteral(resourceName: "sortAscending"), for: .normal)
            print("Ascending")
//            jobsResponse?.jobs = (jobsResponse?.jobs?.sorted(by: {$0.timestamp! > $1.timestamp!}))!
            self.order = KeysAndValues.ORDERASC
            self.jobsTypesPresenter.getJobsTypes()
            
        }
        sortButton.isEnabled = false
       // self.tableView.reloadData()
    }
    
}
extension MainViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if jobsResponse == nil {
            return itemCountForWindless
        }else {
            return (jobsResponse?.jobs?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobTableViewCell", for: indexPath) as!  JobTableViewCell
        if let job = jobsResponse?.jobs![indexPath.row] {
         cell.setUpCell(job: job)
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if jobsResponse?.jobs != nil {
     self.performSegue(withIdentifier:"JobDetailsViewController", sender: jobsResponse?.jobs![indexPath.row])
        }
        //        let mainViewController = self.storyboard?.instantiateViewController(withIdentifier:"MainViewController")as?MainViewController
        //        self.slideMenuController()?.changeMainViewController(mainViewController!, close: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 193.0
    }
    
}
extension MainViewController :JobsTypesView {
    func showLoadingIndicator() {
//    self.tableView.windless.start()
    }
    
    func hideLoadingIndicator() {
   // Loader.hidePrgoress()
//         itemCountForWindless = 0
//        self.tableView.reloadData()
//        self.tableView.windless.end()
    }
    
    func onSucess(jobsResponse: JobsTypesResponse) {
        self.jobsTypesResponse = jobsResponse
        jobsPresenter.getJobs(title: searchText, jobTypeID: selectedTypeId, jobCatgoeryID: selectedCatgoeryId , jobCityID: self.selectedCityId , order:self.order)
    }
    
    func onError(message: String) {
        self.showErrorMessage(message:message)
        self.tableView.windless.end()
        itemCountForWindless = 0
        self.tableView.dg_stopLoading()
        sortButton.isEnabled = true
        self.tableView.reloadData()
    }
    
    
}
extension MainViewController :UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(string == " " &&
            string.trimmingCharacters(in: .whitespaces).isEmpty )
        {
            return false
        }
        searchText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if searchText == "" {
            jobsTypesPresenter.getJobsTypes()
        }else {
            jobsPresenter.getJobs(title: searchText, jobTypeID: selectedTypeId, jobCatgoeryID: selectedCatgoeryId , jobCityID: self.selectedCityId, order:self.order)
        }
        return true
    }
}
extension MainViewController : JobsView {
    func onSuccessNextLinkCall(jobsResponse: JobListResponse) {
      
            self.tableView.finishInfiniteScroll()
            self.tableView.dg_stopLoading()
            self.jobsResponse?.jobs = (self.jobsResponse?.jobs)! + jobsResponse.jobs!
            self.jobsResponse?.meta = jobsResponse.meta
            self.emptyView.alpha = 0
            self.tableView.alpha = 1
            if let jobs = jobsResponse.jobs , jobs.count == 0 {
                // self.tableView.alpha = 0
                // self.emptyView.alpha = 1
            }
            self.sortButton.isEnabled = true
    
        
        tableView.reloadData()
        
      
    }
    
    func onSucess(jobsResponse: JobListResponse) {
        self.tableView.windless.end()
        itemCountForWindless = 0
        self.tableView.dg_stopLoading()
        self.jobsResponse = jobsResponse
        
        self.emptyView.alpha = 0
        self.tableView.alpha = 1
        if let jobs = jobsResponse.jobs , jobs.count == 0 {
            self.tableView.alpha = 0
            self.emptyView.alpha = 1
        }
        sortButton.isEnabled = true
        
        self.tableView.reloadData()
    }
    
}

extension MainViewController:CityListView{
    func onSucessLoadingCityList(cities: [City]) {
        self.cityList.removeAll()
        self.cityList.append(contentsOf: cities)
    }
    
    func showLoadingCityListIndicator() {
        
    }
    
    func hideLoadingCityListIndicator() {
    }
    
    
    
    
    
}
