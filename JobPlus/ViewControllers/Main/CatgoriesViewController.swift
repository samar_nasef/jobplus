//
//  CatgoriesViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
class CatgoriesViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: EmptyView!
    var itemCountForWindless = 10
    let catgoriesPresenter = CatgoriesPresenter()
    var catgoriesResponse:CatgoriesResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.mainViewController = self
        registerCell()
        initPullToRefresh()
        //initLoadMoreToRefresh()
        catgoriesPresenter.attachView(view:self)
        self.tableView.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        catgoriesPresenter.getCatgories()
        //adjustMenuWidth()
        //handleNavigationBarRightLeft()
        if UserManager.sharedInstance.getUser() == nil {
           // self.addBarButtonForBack()
        }
        NotificationCenter.default.removeObserver(self, name: .afterUserLogin, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.afterUserLoginAction), name: .afterUserLogin , object: nil)
        self.view.changeFont()
        
    }
    @objc func afterUserLoginAction() {
        
        catgoriesPresenter.getCatgories()
    }
    
//    func addBarButtonForBack(){
//
//        let button = UIButton(type: .custom)
//
//
//        var imageBack = #imageLiteral(resourceName: "ic_back_arrow_arabic")
//        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ARABIC {
//            imageBack = imageBack.flippedImage()!
//        }
//        button.setImage(imageBack, for: .normal)
//
//        button.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
//        button.addTarget(self, action: #selector(didTapBack), for: UIControlEvents.touchUpInside)
//        let barButton = UIBarButtonItem(customView: button)
//        navigationItem.rightBarButtonItem = barButton
//
//
//    }
    
    @objc func didTapBack(sender: AnyObject){
        print("didTapSkip")
        //go to register
        self.clearDataAndStart()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        setNavigationBarTitleAndColor()
//        addBarButtonForMenu()
//        if MoLocalization.currentAppleLanguage() == MoLocalization.Language.ARABIC {
//            SlideMenuOptions.rightPanFromBezel = true
//        }else {
//            SlideMenuOptions.panFromBezel = true
//        }
    }
    
//    func addBarButtonForMenu(){
//        let menuButton   = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_side_menu") ,  style: .plain, target: self, action:  #selector(didTapOpenMenu))
//        navigationItem.leftBarButtonItem = menuButton
//    }
//
//    @objc func didTapOpenMenu(sender: AnyObject){
//        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ENGLISH {
//            self.slideMenuController()?.openLeft()
//        } else {
//            self.slideMenuController()?.openRight()
//        }
//    }
//    func adjustMenuWidth(){
//        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ENGLISH {
//        self.slideMenuController()?.changeLeftViewWidth(self.view.frame.width - 70 )
//        }else {
//        self.slideMenuController()?.changeRightViewWidth(self.view.frame.width - 70)
//        }
//    }
   

    
    func setNavigationBarTitleAndColor(){
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "ic_back_arrow")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "ic_back_arrow")
       self.navigationController?.setupNavigationControllerOfPartTime()
        self.title = "Category".localized()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func registerCell() {
        tableView.register(UINib(nibName: "CatgoeryTableViewCell", bundle: nil), forCellReuseIdentifier: "CatgoeryTableViewCell")
    }
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.catgoriesPresenter.getCatgories()
            }, loadingView: loadingView)
        self.tableView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.tableView?.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
    func initLoadMoreToRefresh()
    {
        self.tableView?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.tableView?.addInfiniteScroll { (tableView) -> Void in
         
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
//        self.title = ""
//        if MoLocalization.currentAppleLanguage() == MoLocalization.Language.ARABIC {
//            SlideMenuOptions.rightPanFromBezel = false
//        }else {
//            SlideMenuOptions.panFromBezel = false
//        }
    }
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "main" {
//            let destination = segue.destination as? MainViewController
//            let catgoery = sender as? Catgoery
//            destination?.selectedCatgoeryId = catgoery?.id
//            destination?.title = catgoery?.name
//            destination?.selectedCategory = catgoery
//
//
//        }
    }
 

}
extension CatgoriesViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if catgoriesResponse == nil {
        return itemCountForWindless
        } else {
            return (catgoriesResponse?.catgoery?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatgoeryTableViewCell", for: indexPath) as!  CatgoeryTableViewCell
        if catgoriesResponse != nil {
            let catgoery = catgoriesResponse?.catgoery![indexPath.row]
            cell.catgoeryNameLabel.text = catgoery?.name
            //cell.moreImageView.flipInarabic = true
            cell.numberOfJobsLabel.text = "\(catgoery?.numberOfJobs ?? 0 as AnyObject)" + " " + "Jobs".localized()
            
            if let image = catgoery?.imagePath {
                if let catgoeryImgUrl = URL(string: image){
                    cell.catgoeryImageView.kf.setImage(with: catgoeryImgUrl)
                }
            }else {
                cell.catgoeryImageView.image = #imageLiteral(resourceName: "categoryDefault")
            }
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if catgoriesResponse != nil {
            self.performSegue(withIdentifier:"main", sender: catgoriesResponse?.catgoery![indexPath.row])
        }
//        let mainViewController = self.storyboard?.instantiateViewController(withIdentifier:"MainViewController")as?MainViewController
//        self.slideMenuController()?.changeMainViewController(mainViewController!, close: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116.0
    }
    
}
extension CatgoriesViewController : CatgoriesView {
    func showLoadingIndicator() {
        self.tableView.windless.start()
    }
    
    func hideLoadingIndicator() {
        self.tableView.windless.end()
        itemCountForWindless = 0
        self.tableView.reloadData()
        self.tableView.dg_stopLoading()
    }
    
    func onSucess(catgoriesResponse: CatgoriesResponse) {
        self.catgoriesResponse = catgoriesResponse
        if let catArr = self.catgoriesResponse?.catgoery {
            if catArr.count > 0 {
                self.emptyView.alpha = 0
                self.tableView.alpha = 1
            }else {
                self.emptyView.alpha = 1
                self.tableView.alpha = 0
            }
        }
        
        self.tableView.reloadData()
    }
    
    func onError(message: String) {
        self.showErrorMessage(message:message)
    }
    
}



