//
//  CongratulationsViewController.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

class CongratulationsViewController: UIViewController {
    
    
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var congratulationTitleHeight: NSLayoutConstraint!
    
    @IBOutlet weak var descViewiHeight: NSLayoutConstraint!
    
    @IBOutlet weak var congratulationDescHeight: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var congratulateTitle: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var congratulateDescription: UILabel!
    @IBOutlet weak var subscribeBtn: UIButton!
    var from:String = ""
    var titleMsg:String = ""
    var task = DispatchWorkItem {}
    var applyToJobDelegate: ApplyToJobDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("congratulate from  : \(from)")
        view.changeFont()
        if from == "jobDetailsVC" || from == "applyToJob" {
            print("display congrate view")
            
            noteLabel.isHidden = false
            
            subscribeBtn.isHidden = true
            subscribeBtn.isUserInteractionEnabled = false
            congratulateTitle.text = "Congratulations".localized()
            congratulateTitle.font =  UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!
            congratulateTitle.contentMode = .center
            congratulateTitle.textAlignment = .center
            congratulationTitleHeight.constant = "Congratulations".height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!) + 10
        
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            
            
            congratulateDescription.text = "you applied for this job".localized()
            
            // Delay the dismissal by 5 seconds
//            let when = DispatchTime.now() + 5
//            DispatchQueue.main.asyncAfter(deadline: when){
//                // your code with delay
//                self.goToHome()
//            }
            // execute task in 2 seconds
            task = DispatchWorkItem { self.goToHome() }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: task)

            // optional: cancel task
            
            
        }
        else if from == "401" {
            noteLabel.isHidden = true
            subscribeBtn.setTitle("Go To Login".localized(), for: .normal)
            subscribeBtn.setTitle("Go To Login".localized(), for: .selected)
            subscribeBtn.isHidden = false
            subscribeBtn.isUserInteractionEnabled = true
            congratulateDescription.text = "your login session ended \n please, go to login".localized()
            
            congratulateTitle.text = ""
            
            congratulationTitleHeight.constant = titleMsg.height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!)  + 10
        
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            
        }
        else if from == "userNotSuitablleToApply" {
            noteLabel.isHidden = true
            subscribeBtn.setTitle("Apply Now".localized(), for: .normal)
            subscribeBtn.setTitle("Apply Now".localized(), for: .selected)
            subscribeBtn.isHidden = false
            subscribeBtn.isUserInteractionEnabled = true
            congratulateDescription.text = "Your profile not match the job".localized()
            
            congratulateTitle.text = ""
            
            congratulationTitleHeight.constant = titleMsg.height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!)  + 10
        
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            
        }else if from == "status-19" {
            print("display subscription view for status-19")
            
            noteLabel.isHidden = true
            
            subscribeBtn.isHidden = false
            subscribeBtn.isUserInteractionEnabled = true
            
            congratulateTitle.text = titleMsg
            congratulateTitle.font =  UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!
            congratulateTitle.contentMode = .center
            congratulateTitle.textAlignment = .center
            congratulationTitleHeight.constant = titleMsg.height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!)  + 10
            
//            let alert = UIAlertController(title: "congratulationTitleHeight : \(congratulationTitleHeight.constant)", message: "titleMsg : \(titleMsg)", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
////            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
//
//            self.present(alert, animated: true)
            
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            
//                + " " + "To Apply".localized()
            
            congratulateDescription.text = "You need to subscribe".localized()
            
            
            
        }else if from == "profile"{
            print("display subscription view for profile generate cv ")
            
            noteLabel.isHidden = true
            
            subscribeBtn.isHidden = false
            subscribeBtn.isUserInteractionEnabled = true
            print("titleMsg:\(titleMsg)")
            let attr = try? NSAttributedString(htmlString: titleMsg, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!)
            congratulateTitle.attributedText = attr
            congratulateTitle.numberOfLines = 3
            congratulateTitle.font =  UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!
            congratulateTitle.contentMode = .center
            congratulateTitle.textAlignment = .center
            congratulationTitleHeight.constant = titleMsg.height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!)  + 15
        
//            let alert = UIAlertController(title: "congratulationTitleHeight : \(congratulationTitleHeight.constant) ,, no of lines : \(congratulateTitle.numberOfLines)", message: "attr : \(congratulateTitle.attributedText)", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
////            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
//            DispatchQueue.main.async {
//            self.present(alert, animated: true)
//            }
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            
//                + " " + "To Apply".localized()
            
//            congratulateDescription.text = "You need to subscribe".localized()
            congratulateDescription.text = " "
            congratulationDescHeight.constant = 10
        }
        else if from == "profileNotFilled" {
            noteLabel.isHidden = true
            subscribeBtn.isHidden = false
            subscribeBtn.setTitle("Go To Profile".localized(), for: .normal)
            subscribeBtn.setTitle("Go To Profile".localized(), for: .selected)
            subscribeBtn.isUserInteractionEnabled = true
            
            congratulateTitle.text = ""
            congratulationTitleHeight.constant = 0
        
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            congratulateDescription.text = titleMsg.isBlank ? "Please fill your profile to be able to apply for the job".localized() :  titleMsg
            
            
            
        }
        else if from == "status-30" {
            noteLabel.isHidden = true
            subscribeBtn.isHidden = false
            subscribeBtn.setTitle("OkTxt".localized(), for: .normal)
            subscribeBtn.setTitle("OkTxt".localized(), for: .selected)
            subscribeBtn.isUserInteractionEnabled = true
            
            let attr = try? NSAttributedString(htmlString: titleMsg, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!)
            congratulateTitle.attributedText = attr
            congratulateTitle.numberOfLines = 3
            congratulateTitle.font =  UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!
            congratulateTitle.contentMode = .center
            congratulateTitle.textAlignment = .center
            congratulationTitleHeight.constant = titleMsg.height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!)  + 15
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            mainViewHeight.constant = descViewiHeight.constant + 55
            congratulateDescription.text = " "
            congratulationDescHeight.constant = 10
        }
        else{
            print("display subscription view")
            noteLabel.isHidden = true
            subscribeBtn.isHidden = false
            subscribeBtn.isUserInteractionEnabled = true
            
            congratulateTitle.text = "To Apply".localized()
            congratulateTitle.font =  UIFont(name: "HelveticaNeueLTArabic-Roman", size: 18.0)!
            congratulateTitle.contentMode = .center
            congratulateTitle.textAlignment = .center
            congratulationTitleHeight.constant = "To Apply".height(constraintedWidth: congratulateTitle.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 22.0)!)  + 10
        
            descViewiHeight.constant = 145 + congratulationTitleHeight.constant
            
            mainViewHeight.constant = descViewiHeight.constant + 55
            
            
            congratulateDescription.text = "You need to subscribe".localized()
            
            
            
        }
        
        self.view.changeFont()
    }
    
    
    @IBAction func dismissBtnAction(_ sender: Any) {
        if from != "401" {
        task.cancel()
        self.goToHome()
        }
    }
    func goToHome(){
        
        self.dismiss(animated: true) {
            if self.from == "jobDetailsVC" {
                self.applyToJobDelegate?.OpenEvaluateApp()
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissViewAfterRes"), object: nil)
//            if #available(iOS 13.0, *) {
//                let scene = UIApplication.shared.connectedScenes.first
//                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
//                    let mainwindow = sd.window
//                    let stry = UIStoryboard(name: "Main", bundle: nil)
//                    mainwindow?.rootViewController = stry.instantiateInitialViewController()
//                }
//            }
//            else{
//                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//                let stry = UIStoryboard(name: "Main", bundle: nil)
//                rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
//            }
        }
    }
    
    
    
    @IBAction func subcribeAction(_ sender: UIButton) {
        
        if from == "401"{
            self.clearDataAndStart()
        }
        else if from == "profileNotFilled" {
            self.dismiss(animated: true) {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openProfile"), object: nil)
                
            }
        }
        else if from == "userNotSuitablleToApply" {
            self.dismiss(animated: true) {
                self.applyToJobDelegate?.ApplyToJob()
            }
        }
        else if from == "status-30"{
            self.dismiss(animated: true)
        }
        else{
            self.dismiss(animated: true) {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "opensubscribtions"), object: nil)
                
            }
        }
        
        
        
        
        
    }
    
}
