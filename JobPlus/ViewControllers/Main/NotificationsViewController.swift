//
//  NotificationsViewController.swift
//  NajeezAgent
//
//  Created by passant on 5/14/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll
class NotificationsViewController: UIViewController {

    @IBOutlet weak var notificationsSwitcher: UISegmentedControl!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var reloaDBtn: UIButton!
    var notification_type = 1
    var notificationPresenter = NotificationPresenter()
    var defaultItemCountForWindless = 10
    var page = 1
    var notificationsArr = [NotificationItem]()
    var notificationResponseObj :NotificaationsListResponse?
    var cellHeights: [IndexPath : CGFloat] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationsSwitcher.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!, NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.selected)
        notificationsSwitcher.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!, NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        
        self.view.changeFont()
        self.setUpUI()
        self.notificationPresenter.attachView(view: self)
        self.registerTableViewCell()
        notificationTableView.rowHeight = UITableView.automaticDimension
        //UIView.appearance().semanticContentAttribute =  .forceRightToLeft
        //  if UserManager.sharedInstance.getUser() != nil {
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        self.initPullToRefresh()
        self.initLoadMoreToRefresh()
        self.notificationTableView.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        
        //addBarButtonForMenu()
        if self.notification_type == 1 {
        self.notificationPresenter.doGetGeneralNotificationList(page: self.page)
        }
        else{
            self.notificationsSwitcher.selectedSegmentIndex = 1
            self.notificationPresenter.doGetJobsNotificationList(page: self.page)
        }
        // }else {
        // self.notificationTableView.alpha = 0
        // self.emptyView.alpha = 1
        // }
    }

    @IBAction func dismissView(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
//    func addBarButtonForMenu(){
//        let menuButton   = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_side_menu") ,  style: .plain, target: self, action:  #selector(didTapOpenMenu))
//        navigationItem.leftBarButtonItem = menuButton
//    }
//
//    @objc func didTapOpenMenu(sender: AnyObject){
//        if MoLocalization.getCurrentLanguage() == MoLocalization.Language.ENGLISH {
//            self.slideMenuController()?.openLeft()
//        } else {
//            self.slideMenuController()?.openRight()
//        }
//    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setupNavigationControllerOfPartTime()
        self.title = "Notifications".localized()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
        //self.navigationController?.presentTransparentNavigationBar()
    }
    func setUpUI(){
    }
    
    func registerTableViewCell(){
        
        notificationTableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
    }
    
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        notificationTableView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.page = 1
            // self?.ordersArr.removeAll()
            if self?.notificationsSwitcher.selectedSegmentIndex == 0 {
                self?.notificationPresenter.doGetGeneralNotificationList(page: self!.page)
            }
            else{
                self?.notificationPresenter.doGetJobsNotificationList(page: self!.page)
            }
            
            }, loadingView: loadingView)
        self.notificationTableView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        self.notificationTableView?.dg_setPullToRefreshBackgroundColor(self.notificationTableView.backgroundColor!)
        
        
        
    }
    
    @IBAction func reloadAction(_ sender: Any) {
        self.page = 1
        if self.notificationsSwitcher.selectedSegmentIndex == 0 {
            self.notificationPresenter.doGetGeneralNotificationList(page: self.page)
        }
        else{
            self.notificationPresenter.doGetJobsNotificationList(page: self.page)
        }
    }
    
    @IBAction func Noti_type_changed(_ sender: UISegmentedControl) {
                        
            switch sender.selectedSegmentIndex {
            case 0:
                self.page = 1
                self.notificationTableView.windless
                    .apply {
                        $0.beginTime = 0
                        $0.pauseDuration = 1
                        $0.duration = 3
                        $0.animationLayerOpacity = 0.8
                        $0.direction = .left
                        
                    }
                    .start()
                
                //addBarButtonForMenu()
                self.notificationPresenter.doGetGeneralNotificationList(page: self.page)
            case 1:
                self.page = 1
                self.notificationTableView.windless
                    .apply {
                        $0.beginTime = 0
                        $0.pauseDuration = 1
                        $0.duration = 3
                        $0.animationLayerOpacity = 0.8
                        $0.direction = .left
                        
                    }
                    .start()
                
                //addBarButtonForMenu()
                self.notificationPresenter.doGetJobsNotificationList(page: self.page)
            default:
                break;
            }
    }
    func initLoadMoreToRefresh()
    {
        
        self.notificationTableView?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.notificationTableView?.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return self.page != self.notificationResponseObj?.last_page
        }
        self.notificationTableView?.addInfiniteScroll { (tableView) -> Void in
            
            if self.page == self.notificationResponseObj?.last_page {
                //ending paging
                self.notificationTableView.finishInfiniteScroll()
            }else {
                self.page += 1
                if self.notificationsSwitcher.selectedSegmentIndex == 0 {
                    self.notificationPresenter.doGetGeneralNotificationList(page: self.page)
                }
                else{
                    self.notificationPresenter.doGetJobsNotificationList(page: self.page)
                }
                
            }
        }
    }

}
extension NotificationsViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if self.notificationsArr.count != 0 {
            return notificationsArr.count
        }else if UserManager.sharedInstance.getUser() == nil {
            return 0
        }else {
            return defaultItemCountForWindless
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        if self.notificationsArr.count > 0 {
            if L102Language.isRTL {
                cell.notificationTitleLbl.text = self.notificationsArr[indexPath.row].bodyAr
            }
            else{
                cell.notificationTitleLbl.text = self.notificationsArr[indexPath.row].bodyEn
            }
            let created_at = self.notificationsArr[indexPath.row].created_at + "Z"
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            dateFormatterGet.timeZone = NSTimeZone.local
            if L102Language.isRTL {
                dateFormatterGet.locale = Locale(identifier: "ar_EG")
            }
            else{
                dateFormatterGet.locale = Locale(identifier: "en_US")
            }
            guard let creationDate = dateFormatterGet.date(from: created_at) else {return cell}
            cell.notificationDateLbl.text = timeAgoSince(creationDate)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let jobID = self.notificationsArr[indexPath.row].job_id {
            if jobID > 0 {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                    if let navigator = navigationController {
                        UserManager.sharedInstance.openDynamicLinkJobID = jobID
                        UserManager.sharedInstance.openDynamicLink = true
                        navigator.pushViewController(viewController, animated: false)
                    }
                }
            }
        }
      //  let selectedNotification = self.notificationsArr[indexPath.row]
        //if let payLoad = selectedNotification.payload {
//            if let type = payLoad.type , let orderId = payLoad.orderId {
//            HandlePushNotification.checkType(type: "\(type)" , orderId: "\(orderId)")
//            }
        //}
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return 110.0 }
        return height
    }

}

extension NotificationsViewController:NotificationView{
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        
    }
    
    func onSucess(notificationObject: NotificaationsListResponse) {
         // defaultItemCountForWindless = 0
        if page == 1 {
            self.notificationsArr.removeAll()
        }
        notificationResponseObj = notificationObject
        if let notificationsArr = notificationObject.data {
            if notificationsArr.count == 0 &&  page == 1 {
                self.notificationTableView.alpha = 0
                self.emptyView.alpha = 1
                 self.reloaDBtn.isHidden = false
            }else {
                for item in notificationsArr{
                    self.notificationsArr.append(item)
                }
                //self.notificationsArr = notificationsArr
                self.notificationTableView.reloadData()
                self.notificationTableView.alpha = 1
                self.emptyView.alpha = 0
                 self.reloaDBtn.isHidden = true
            }
        }
            self.notificationTableView?.reloadData()
        self.notificationPresenter.doreadNotificationList()
        
    }
    
    func showLoadingIndicator() {
        self.notificationTableView.windless.start()
        self.notificationTableView.alpha = 1
        self.emptyView.alpha = 0
        self.reloaDBtn.isHidden = true
    }
    
    func hideLoadingIndicator() {
         defaultItemCountForWindless = 0
        self.notificationTableView?.reloadData()
        self.notificationTableView.windless.end()
        self.notificationTableView?.dg_stopLoading()
        self.notificationTableView.finishInfiniteScroll()
        self.notificationTableView.alpha = 1
        self.emptyView.alpha = 0
         self.reloaDBtn.isHidden = true
    }
    func onError(message: String) {
         defaultItemCountForWindless = 0
        self.showErrorMessage(message: message)
        self.notificationTableView?.reloadData()
        self.notificationTableView.alpha = 1
        self.emptyView.alpha = 0
        self.reloaDBtn.isHidden = true
    }
    
    public func timeAgoSince(_ date: Date) -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        
        if let year = components.year, year >= 2 {
            let value = NSLocalizedString("years ago", comment: "")
            return "\(year) \(value)"
        }
        
        if let year = components.year, year >= 1 {
            return NSLocalizedString("Last year", comment: "")
        }
        
        if let month = components.month, month >= 2 {
            let value = NSLocalizedString("months ago", comment: "")
            return "\(month) \(value)"
        }
        
        if let month = components.month, month >= 1 {
            return NSLocalizedString("Last month", comment: "")
        }
        
        if let week = components.weekOfYear, week >= 2 {
            let value = NSLocalizedString("weeks ago", comment: "")
            return "\(week) \(value)"
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return NSLocalizedString("Last week", comment: "")
        }
        
        if let day = components.day, day >= 2 {
            let value = NSLocalizedString("days ago", comment: "")
            return "\(day) \(value)"
        }
        
        if let day = components.day, day >= 1 {
            return NSLocalizedString("Yesterday", comment: "")
        }
        
        if let hour = components.hour, hour >= 2 {
            let value = NSLocalizedString("hours ago", comment: "")
            return "\(hour) \(value)"
        }
        
        if let hour = components.hour, hour >= 1 {
            return NSLocalizedString("An hour ago", comment: "")
        }
        
        if let minute = components.minute, minute >= 2 {
            let value = NSLocalizedString("minutes ago", comment: "")
            return "\(minute) \(value)"
        }
        
        if let minute = components.minute, minute >= 1 {
            return NSLocalizedString("A minute ago", comment: "")
        }
        
        if let second = components.second, second >= 3 {
            let value = NSLocalizedString("seconds ago", comment: "")
            return "\(second) \(value)"
        }
        
        return NSLocalizedString("Just now", comment: "")
        
    }
}
