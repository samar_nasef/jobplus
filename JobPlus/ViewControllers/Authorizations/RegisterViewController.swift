//
//  RegisterViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/11/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import ALCameraViewController
import TweeTextField
import LaraCrypt

enum MESSAGE {
    case MATCH
    case NOT_MATCH
}

class RegisterViewController: UIViewController {
    var selectedProfileImage :UIImage?
    //@IBOutlet weak var placeholderProfileImageView: UIImageView!
    //@IBOutlet weak var profieImageView: UIImageView!
    @IBOutlet weak var birthDateTextField: TweeAttributedTextField!
    @IBOutlet weak var fullNameTxtField: TweeAttributedTextField!
    @IBOutlet weak var lastNameTxtField: TweeAttributedTextField!
    @IBOutlet weak var phoneNumberTxtField: TweeAttributedTextField!
    @IBOutlet weak var emailTxtField: TweeAttributedTextField!
    @IBOutlet weak var passwordTxtField: TweeAttributedTextField!
    @IBOutlet weak var confirmPasswordTxtField: TweeAttributedTextField!
    @IBOutlet weak var countryCodeTxtField: CTKFlagPhoneNumberTextField!
    @IBOutlet weak var genderTxtField: TweeAttributedTextField!
    @IBOutlet weak var registerBtn: TransitionButton!
    @IBOutlet weak var phoneViewInternal: UIView!
    @IBOutlet weak var labelCaptcha: UILabel!
    @IBOutlet weak var captchaValView: UIView!
    @IBOutlet weak var textfieldEnterCaptcha: TweeAttributedTextField!
   // @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var acceptTermsButton: UIButton!
    @IBOutlet weak var termsErrorMessage: UILabel!
    
    @IBOutlet weak var fullNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var birthDateView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    ////////////////////////////////////////////////
    var birthDateTime:Int?
    var genderArray = ["Male".localized() , "Female".localized()]
    var gender = ""
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = true
    var allowResizing: Bool = true
    var allowMoving: Bool = true
    var minimumSize: CGSize = CGSize(width: 200, height: 200)
    var croppingParameters: CroppingParameters {
        return CroppingParameters(isEnabled: croppingEnabled, allowResizing: allowResizing, allowMoving: allowMoving, minimumSize: minimumSize)
    }
    let presenter = RegisterPresenter()
    var captchInfo: String?
    var isTermsAccepted = false
    var registerCase = VerificationCase.Authorization
    
    
    @objc func getCode(_ notification: NSNotification){
        print("getCode \(String(describing: notification.userInfo?["code"]))")
        if let code = notification.userInfo?["code"] {
            self.countryCodeLbl.text = code as? String
            self.resetCustomTextFieldsErrorMessages(self.phoneNumberTxtField)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        //registerBtn.dropShadow()
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getCode"), object: nil)
               NotificationCenter.default.addObserver(self,
                                                      selector: #selector(self.getCode(_:)),
                                                             name: NSNotification.Name(rawValue: "getCode"),
                                                             object: nil)
        fullNameView.layer.cornerRadius = 2
        fullNameView.layer.shadowColor = UIColor.black.cgColor
        fullNameView.layer.shadowOpacity = 0.2
        fullNameView.layer.shadowOffset = .zero
        fullNameView.layer.shadowRadius = 10
        
        lastNameView.layer.cornerRadius = 2
        lastNameView.layer.shadowColor = UIColor.black.cgColor
        lastNameView.layer.shadowOpacity = 0.2
        lastNameView.layer.shadowOffset = .zero
        lastNameView.layer.shadowRadius = 10
        
        passwordView.layer.cornerRadius = 2
        passwordView.layer.shadowColor = UIColor.black.cgColor
        passwordView.layer.shadowOpacity = 0.2
        passwordView.layer.shadowOffset = .zero
        passwordView.layer.shadowRadius = 10
        
        confirmPasswordView.layer.cornerRadius = 2
        confirmPasswordView.layer.shadowColor = UIColor.black.cgColor
        confirmPasswordView.layer.shadowOpacity = 0.2
        confirmPasswordView.layer.shadowOffset = .zero
        confirmPasswordView.layer.shadowRadius = 10
        
        emailView.layer.cornerRadius = 2
        emailView.layer.shadowColor = UIColor.black.cgColor
        emailView.layer.shadowOpacity = 0.2
        emailView.layer.shadowOffset = .zero
        emailView.layer.shadowRadius = 10
        
        genderView.layer.cornerRadius = 2
        genderView.layer.shadowColor = UIColor.black.cgColor
        genderView.layer.shadowOpacity = 0.2
        genderView.layer.shadowOffset = .zero
        genderView.layer.shadowRadius = 10
        
        birthDateView.layer.cornerRadius = 2
        birthDateView.layer.shadowColor = UIColor.black.cgColor
        birthDateView.layer.shadowOpacity = 0.2
        birthDateView.layer.shadowOffset = .zero
        birthDateView.layer.shadowRadius = 10
        
        phoneNumberView.layer.cornerRadius = 2
        phoneNumberView.layer.shadowColor = UIColor.black.cgColor
        phoneNumberView.layer.shadowOpacity = 0.2
        phoneNumberView.layer.shadowOffset = .zero
        phoneNumberView.layer.shadowRadius = 10
        
        captchaValView.layer.shadowColor = UIColor.black.cgColor
        captchaValView.layer.shadowOpacity = 0.2
        captchaValView.layer.shadowOffset = .zero
        captchaValView.layer.shadowRadius = 10
        
        setupTextFields()
        self.title = "Sign up".localized()
        presenter.attachView(view: self)
        // Do any additional setup after loading the view.
        self.countryCodeTxtFieldConfiguration()
        self.view.changeFont()
        labelCaptcha.text = ""
        labelCaptcha.font = UIFont(name: "PartyLetPlain", size: 35)
        textfieldEnterCaptcha.text = ""
        self.reloadCaptcha()
    }
    func reloadCaptcha() {
        presenter.getCaptchaInfo(uuid: UIDevice.current.identifierForVendor?.uuidString ?? "")
    }
    @IBAction func backBttnAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupTextFields(){
        fullNameTxtField.delegate = self
        fullNameTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        fullNameTxtField.infoFontSize = 14.0
        fullNameTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        fullNameTxtField.activeLineWidth = 2
        fullNameTxtField.animationDuration = 0.5
        fullNameTxtField.minimumPlaceholderFontSize = 15
        fullNameTxtField.originalPlaceholderFontSize = 15
        fullNameTxtField.placeholderDuration = 0.2
        fullNameTxtField.placeholderColor = .darkGray
        fullNameTxtField.placeholder = "First Name".localized()
        fullNameTxtField.textColor = UIColor(hexString: "#8C8C8C")
        
        lastNameTxtField.delegate = self
        lastNameTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        lastNameTxtField.infoFontSize = 14.0
        lastNameTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        lastNameTxtField.activeLineWidth = 2
        lastNameTxtField.animationDuration = 0.5
        lastNameTxtField.minimumPlaceholderFontSize = 15
        lastNameTxtField.originalPlaceholderFontSize = 15
        lastNameTxtField.placeholderDuration = 0.2
        lastNameTxtField.placeholderColor = .darkGray
        lastNameTxtField.placeholder = "Last Name".localized()
        lastNameTxtField.textColor = UIColor(hexString: "#8C8C8C")
        
        phoneNumberTxtField.delegate = self
        phoneNumberTxtField.returnKeyType = .next
        phoneNumberTxtField.keyboardType = .numberPad
        phoneNumberTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        phoneNumberTxtField.infoFontSize = 14.0
        phoneNumberTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        phoneNumberTxtField.activeLineWidth = 2
        phoneNumberTxtField.animationDuration = 0.5
        phoneNumberTxtField.minimumPlaceholderFontSize = 15
        phoneNumberTxtField.originalPlaceholderFontSize = 15
        phoneNumberTxtField.placeholderDuration = 0.2
        phoneNumberTxtField.placeholderColor = .darkGray
        phoneNumberTxtField.textColor = UIColor(hexString: "#8C8C8C")
        phoneNumberTxtField.placeholder = "Phone".localized()
        
        emailTxtField.delegate = self
        emailTxtField.keyboardType = .emailAddress
        emailTxtField.returnKeyType = .next
        emailTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        emailTxtField.infoFontSize = 14.0
        emailTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        emailTxtField.activeLineWidth = 2
        emailTxtField.animationDuration = 0.5
        emailTxtField.minimumPlaceholderFontSize = 15
        emailTxtField.originalPlaceholderFontSize = 15
        emailTxtField.placeholderDuration = 0.2
        emailTxtField.placeholderColor = .darkGray
        emailTxtField.textColor = UIColor(hexString: "#8C8C8C")
        emailTxtField.placeholder = "Email Address".localized()
        
        passwordTxtField.delegate = self
        passwordTxtField.returnKeyType = .next
        passwordTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        passwordTxtField.infoFontSize = 14.0
        passwordTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        passwordTxtField.activeLineWidth = 2
        passwordTxtField.animationDuration = 0.5
        passwordTxtField.minimumPlaceholderFontSize = 15
        passwordTxtField.originalPlaceholderFontSize = 15
        passwordTxtField.placeholderDuration = 0.2
        passwordTxtField.placeholderColor = .darkGray
        passwordTxtField.textColor = UIColor(hexString: "#8C8C8C")
        passwordTxtField.isSecureTextEntry = true
        passwordTxtField.placeholder = "Password".localized()
        
        confirmPasswordTxtField.delegate = self
        confirmPasswordTxtField.returnKeyType = .next
        confirmPasswordTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        confirmPasswordTxtField.infoFontSize = 14.0
        confirmPasswordTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        confirmPasswordTxtField.activeLineWidth = 2
        confirmPasswordTxtField.animationDuration = 0.5
        confirmPasswordTxtField.minimumPlaceholderFontSize = 15
        confirmPasswordTxtField.originalPlaceholderFontSize = 15
        confirmPasswordTxtField.placeholderDuration = 0.2
        confirmPasswordTxtField.placeholderColor = .darkGray
        confirmPasswordTxtField.textColor = UIColor(hexString: "#8C8C8C")
        confirmPasswordTxtField.isSecureTextEntry = true
        confirmPasswordTxtField.placeholder = "Confirm Password".localized()
        
        
        birthDateTextField.delegate = self
        birthDateTextField.infoTextColor = UIColor(hexString: "#FF2600")
        birthDateTextField.infoFontSize = 14.0
        birthDateTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        birthDateTextField.activeLineWidth = 2
        birthDateTextField.animationDuration = 0.5
        birthDateTextField.minimumPlaceholderFontSize = 15
        birthDateTextField.originalPlaceholderFontSize = 15
        birthDateTextField.placeholderDuration = 0.2
        birthDateTextField.placeholderColor = .darkGray
        birthDateTextField.textColor = UIColor(hexString: "#8C8C8C")
        birthDateTextField.placeholder = "Birth Date".localized()
        
        genderTxtField.delegate = self
        genderTxtField.infoTextColor = UIColor(hexString: "#FF2600")
        genderTxtField.infoFontSize = 14.0
        genderTxtField.activeLineColor = UIColor(hexString: "#00AFDF")
        genderTxtField.activeLineWidth = 2
        genderTxtField.animationDuration = 0.5
        genderTxtField.minimumPlaceholderFontSize = 15
        genderTxtField.originalPlaceholderFontSize = 15
        genderTxtField.placeholderDuration = 0.2
        genderTxtField.placeholderColor = .darkGray
        genderTxtField.textColor = UIColor(hexString: "#8C8C8C")
        genderTxtField.placeholder = "Gendure".localized()
        
        textfieldEnterCaptcha.returnKeyType = .done
        textfieldEnterCaptcha.infoTextColor = UIColor(hexString: "#FF2600")
        textfieldEnterCaptcha.infoFontSize = 14.0
        textfieldEnterCaptcha.infoLabel.numberOfLines = 0
        textfieldEnterCaptcha.activeLineColor = UIColor(hexString: "#00AFDF")
        textfieldEnterCaptcha.activeLineWidth = 2
        textfieldEnterCaptcha.animationDuration = 0.5
        textfieldEnterCaptcha.minimumPlaceholderFontSize = 15
        textfieldEnterCaptcha.originalPlaceholderFontSize = 15
        textfieldEnterCaptcha.placeholderDuration = 0.2
        textfieldEnterCaptcha.placeholderColor = .darkGray
        textfieldEnterCaptcha.textAlignment = .center
        textfieldEnterCaptcha.textColor = UIColor(hexString: "#8C8C8C")
        textfieldEnterCaptcha.delegate = self
        textfieldEnterCaptcha.placeholder = "Enter Captcha".localized()
        textfieldEnterCaptcha.delegate = self
    }
    
    func checkIsMatchTwoString() -> MESSAGE {
        let input = textfieldEnterCaptcha.text
        if input?.lowercased() == decryptData(data: self.captchInfo ?? "").lowercased() {
            return MESSAGE.MATCH
        }
        else{
            return MESSAGE.NOT_MATCH
        }
    }
    
    func countryCodeTxtFieldConfiguration(){
        countryCodeTxtField.borderStyle = .none
        countryCodeTxtField.parentViewController = self
        countryCodeTxtField.textAlignment = .center
        //        Example of customizing the textField input accessory view
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeTxtField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
        countryCodeTxtField.flagSize = CGSize(width: 25, height: 25)
        
//        countryCodeTxtField.semanticContentAttribute = .forceLeftToRight
//        countryCodeTxtField.leftView?.semanticContentAttribute = .forceLeftToRight
        
        countryCodeTxtField.setFlag(with: Locale.current.regionCode ?? "SA")
        print(Locale.current.regionCode ?? "SA")
    }
    @IBAction func actionReloadCaptcha(sender: UIButton) {
        reloadCaptcha()
    }
    func decryptData(data: String) -> String {
        return LaraCrypt().decrypt(Message: data, Key: "K9bpP3TfB+wbHA8FNx6B/7+CNsJVRB4xtoHNNWIPMO0=")
    }
    func encryptData(data: String) -> String {
        let enc = LaraCrypt().encrypt(Message: data, Key: "K9bpP3TfB+wbHA8FNx6B/7+CNsJVRB4xtoHNNWIPMO0=")
        print(LaraCrypt().decrypt(Message: enc, Key: "K9bpP3TfB+wbHA8FNx6B/7+CNsJVRB4xtoHNNWIPMO0="))
        return LaraCrypt().encrypt(Message: data, Key: "K9bpP3TfB+wbHA8FNx6B/7+CNsJVRB4xtoHNNWIPMO0=")
    }
    //"eyJpdiI6IlcyQ3RaNGxvTmE4R3d5K05ET2l4b2c9PSIsInZhbHVlIjoiMk1DU1N3YUVtOHlraTNIV0M0Q3dxUT09IiwibWFjIjoiNDJjYjE3NTIzNDEzZDIwZmUxOGI4ZmZhODdkMTNhNjI0Y2ZiOTZiMTUzNmE4OWJkZjgwN2ViYzIwZjU3Mzk5YyJ9"
    func checkCountryCodeEmpty(){
        //  countryCodeTxtField.setFlag(with: Locale.current.regionCode!)
        if countryCodeTxtField.text == "" {
            countryCodeTxtField.text = "+966"
            countryCodeTxtField.setFlag(with: "SA")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        // checkCountryCodeEmpty()
//        DispatchQueue.main.async {
//            self.phoneViewInternal.semanticContentAttribute = .forceLeftToRight
//            self.phoneNumberView.semanticContentAttribute = .forceLeftToRight
//        }
    }
    
    @objc func validate() {
        print(countryCodeTxtField.getFormattedPhoneNumber() ?? "No formatted phone number")
        print(countryCodeTxtField.getCountryPhoneCode() ?? "No country phone code")
        print(countryCodeTxtField.getRawPhoneNumber() ?? "No raw phone number")
    }
    
    private func getCustomTextFieldInputAccessoryView(with items: [UIBarButtonItem]) -> UIToolbar {
        let toolbar: UIToolbar = UIToolbar()
        
        toolbar.barStyle = UIBarStyle.default
        toolbar.items = items
        toolbar.sizeToFit()
        return toolbar
    }
    
    @IBAction func registerAction(_ sender: Any) {
        var base64Image:String?
        if(selectedProfileImage != nil){
            base64Image = (selectedProfileImage?.resizedImageWithinRect(rectSize: CGSize(width: 200, height: 200)).toBase64())!
        }
        //        var birthDateTime:Int?
        //        if let birthDateTime_ = self.birthDateTime {
        //            birthDateTime = birthDateTime_
        //        }
        if(validateLoginTextFields()) {
            var phone_no = phoneNumberTxtField.text!.replacedArabicDigitsWithEnglish
            if phone_no.first == "0" {
              phone_no.remove(at: (phone_no.startIndex))
            }
            let phone = countryCodeTxtField.text! + phone_no
            
            presenter.doRegister(name: fullNameTxtField.text!,LastName: lastNameTxtField.text!, email: emailTxtField.text!, mobile: phone, password: passwordTxtField.text!, gender: self.gender, birthDate: birthDateTime, mobileApplicationLocale: L102Language.currentAppleLanguage(), image:base64Image, DeviceGuid:  UIDevice.current.identifierForVendor?.uuidString ?? "" , CaptchaCode:  textfieldEnterCaptcha.text ?? "")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async{
                 self.phoneViewInternal.semanticContentAttribute = .forceLeftToRight
                 self.countryCodeTxtField.semanticContentAttribute = .forceLeftToRight
             }
        
    }
    @IBAction func chooseGenderBtnAction(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            self.view.endEditing(true)
                   let genderPicker = UIPickerView()
                   let genderArrayToDisplay = ["Male".localized() , "Female".localized()]
                   genderPicker.addCustomPicker(genderArrayToDisplay as NSArray , sender , "Gender", 0) { (selectedIndex) in
                       
                       print(self.genderArray[selectedIndex])
                    self.genderTxtField.showInfo("")
                       self.genderTxtField.text = genderArrayToDisplay[selectedIndex]
                       if selectedIndex == 0 {
                           self.gender = "male"
                       }else {
                           self.gender = "female"
                       }
                   }
        }
       
    }
    
    @IBAction func birthDateAction(_ sender: Any) {
        self.view.endEditing(true)
        let datePicker = setupDatePicker()
        datePicker.show(inVC: self)
    }
    
    func validateLoginTextFields() -> Bool {
        var valid = true
        let nameTxt = self.fullNameTxtField.text ?? ""
        let trimedTxt = nameTxt.components(separatedBy: .whitespaces).joined()
        if let fullName = self.fullNameTxtField.text , !fullName.isBlank {
            if !fullName.isNameMoreThanTwoCharacters {
                let errorMessage = "Full name must be more than four characters".localized()
                self.fullNameTxtField.showInfo(errorMessage, animated: true)
                valid = false
            }
            else if trimedTxt.hasSpecialCharacters(){
                self.fullNameTxtField.showInfo("invalid special".localized())
                valid = false
            }
            else {
                
                print("valid fullNmae")
            }
        }else {
            print("error fullNmae")
            let errorMessage = "enterFName".localized()
            self.fullNameTxtField.showInfo(errorMessage, animated: true)
            valid = false
        }
        let lastNameTxt = self.lastNameTxtField.text ?? ""
        let trimedLastNameTxt = lastNameTxt.components(separatedBy: .whitespaces).joined()
        if let fullName = self.lastNameTxtField.text , !fullName.isBlank {
            if !fullName.isNameMoreThanTwoCharacters {
                let errorMessage = "Full name must be more than four characters".localized()
                self.lastNameTxtField.showInfo(errorMessage, animated: true)
                valid = false
            }
            else if trimedLastNameTxt.hasSpecialCharacters(){
                self.lastNameTxtField.showInfo("invalid special".localized())
                valid = false
            }
            else {
                
                print("valid fullNmae")
            }
        }else {
            print("error fullNmae")
            let errorMessage = "enterlName".localized()
            self.lastNameTxtField.showInfo(errorMessage, animated: true)
            valid = false
        }
//         if let gender = self.genderTxtField.text , !gender.isBlank {
//
//                print("valid gender")
//
//                }else {
//                print("error gender")
//                self.genderTxtField.showInfo("please enter your gender".localized())
//                valid = false
//                }
               if let birthDate = self.birthDateTextField.text , !birthDate.isBlank {

               print("valid birthDate")

               }else {
               print("error birthDate")
               self.birthDateTextField.showInfo("please enter your bithday".localized())
               valid = false
               }
        if let phoneNumber = self.phoneNumberTxtField.text?.replacedArabicDigitsWithEnglish , !phoneNumber.isBlank{
            if phoneNumber.isValidPhoneNumber(code: countryCodeTxtField.text!){
                print("valid phoneNumber")
            }else {
                let errorMessage = "please enter valid phone number".localized()
                self.phoneNumberTxtField.showInfo(errorMessage, animated: true)
                valid = false
            }
        }else {
            print("error phoneNumber")
            let errorMessage = "enter your phone number".localized()
            self.phoneNumberTxtField.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        if let password = self.passwordTxtField.text , !password.isBlank {
            if password.isPasswordMoreThaneightCharacters {
                print("valid password")
            }else {
                let errorMessage = "password must be more than eight characters".localized()
                self.passwordTxtField.showInfo(errorMessage, animated: true)
                valid = false
            }
        }else {
            print("error password")
            let errorMessage = "please enter your password".localized()
            self.passwordTxtField.showInfo(errorMessage, animated: true)
            valid = false
        }
        if let confirmPassword = self.confirmPasswordTxtField.text , !confirmPassword.isBlank {
            
                print("valid confirmPassword")
                if self.confirmPasswordTxtField.text != self.passwordTxtField.text {
                    print("error confirmPassword")
                    let errorMessage = "PassWordOldNotMatched".localized()
                    self.confirmPasswordTxtField.showInfo(errorMessage, animated: true)
                    valid = false
                }
                else {
                self.confirmPasswordTxtField.showInfo("", animated: true)
                }
            
        }
        else {
            let errorMessage = "please confirm your password".localized()
            self.confirmPasswordTxtField.showInfo(errorMessage, animated: true)
            valid = false
        }
        /*
         if let birthDate = self.birthDateTextField.text , birthDate.isBlank {
         print("error confirmPassword")
         self.birthDateTextField.errorMessage = "please enter your birth date".localized()
         valid = false
         }
         */
        
//        if(self.passwordTxtField.text != self.confirmPasswordTxtField.text ) {
//            let errorMessage = "New Passwords Should Be Equal".localized()
//            self.passwordTxtField.showInfo(errorMessage, animated: true)
//            let errorMessage2 = "New Passwords Should Be Equal".localized()
//            self.confirmPasswordTxtField.showInfo(errorMessage2, animated: true)
//            valid = false
//        }
        
        
        if let email = self.emailTxtField.text , !email.isBlank {
            if !email.isEmail{
                let errorMessage = "please enter valid email".localized()
                self.emailTxtField.showInfo(errorMessage, animated: true)
                valid = false
            }else {
                print("valid email")
            }
        }else {
            print("error email")
            let errorMessage = "please enter your email address".localized()
            self.emailTxtField.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        if !isTermsAccepted {
            termsErrorMessage.alpha = 1
            valid = false
        }else {
            termsErrorMessage.alpha = 0
        }
        // valivate captcha
        if textfieldEnterCaptcha.text!.isEmpty {
            valid = false
             textfieldEnterCaptcha.showInfo("Enter Captcha".localized(), animated: true)
        }
        else {
            switch checkIsMatchTwoString() {
            case .MATCH:
                textfieldEnterCaptcha.showInfo("", animated: true)
                break
            case .NOT_MATCH:
                valid = false
                textfieldEnterCaptcha.showInfo("Not Matched".localized(), animated: true)
                break
            }
        }
        return valid
    }
    
//    @IBAction func deleteButtonAction(_ sender: Any) {
//        self.deleteButton.isHidden = true
//        self.profieImageView.image = nil
//        self.selectedProfileImage = nil
//        self.placeholderProfileImageView.isHidden = false
//    }
    @IBAction func profileImageAction(_ sender: Any) {
        takePhotoCamera()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VerficationCodeViewController" {
            if let destnationViewcontroller = segue.destination as? VerficationCodeViewController {
                destnationViewcontroller.verificationCase = registerCase
            }
        }else if segue.identifier == "TermsAndConditionsViewController" {
            if let destinationViewController = segue.destination as? TermsAndConditionsViewController{
                destinationViewController.termsCase = .fromAuthorization
            }
        }
    }
    
    @IBAction func acceptTerms(_ sender: UIButton) {
        termsErrorMessage.alpha = 0
        if isTermsAccepted {
            self.acceptTermsButton.setImage(#imageLiteral(resourceName: "roundedRectangle3_new"), for: .normal)
            isTermsAccepted = false
        }else {
            self.acceptTermsButton.setImage(#imageLiteral(resourceName: "check_new"), for: .normal)
            isTermsAccepted = true
            
        }
    }
}
extension RegisterViewController :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.fullNameTxtField) {
            self.lastNameTxtField.becomeFirstResponder()
        }
        if(textField == self.lastNameTxtField) {
            self.view.endEditing(true)
        }
        else if(textField == phoneNumberTxtField) {
            self.emailTxtField.becomeFirstResponder()
        }
        else if(textField == passwordTxtField) {
            self.confirmPasswordTxtField.becomeFirstResponder()
        }
        else if(textField == self.emailTxtField) {
            self.passwordTxtField.becomeFirstResponder()
        }
        else if(textField == confirmPasswordTxtField) {
            self.textfieldEnterCaptcha.becomeFirstResponder()
        }
        else if(textField == textfieldEnterCaptcha) {
            self.view.endEditing(true)
        }
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        if textField == phoneNumberTxtField {
//            if phoneNumberTxtField.text?.count == 0 && string == "0" {
//
//                return false
//            }
            if range.location == 14 {
                
                return false
            }
        }
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.fullNameTxtField {
            self.fullNameTxtField.showInfo("", animated: true)
        } else if textField == self.phoneNumberTxtField {
            self.phoneNumberTxtField.showInfo("", animated: true)
        }else if textField == self.lastNameTxtField {
            self.lastNameTxtField.showInfo("", animated: true)
        }
        else if textField == self.emailTxtField {
            self.emailTxtField.showInfo("", animated: true)
        }else if textField  == self.passwordTxtField {
            self.passwordTxtField.showInfo("", animated: true)
            self.confirmPasswordTxtField.showInfo("", animated: true)
        }else if textField  == self.confirmPasswordTxtField {
            self.confirmPasswordTxtField.showInfo("", animated: true)
        }else if textField == self.birthDateTextField {
            self.birthDateTextField.showInfo("", animated: true)
        }else if textField == self.genderTxtField{
            self.genderTxtField.showInfo("", animated: true)
        }else if textField  == self.textfieldEnterCaptcha {
            self.textfieldEnterCaptcha.showInfo("", animated: true)
        }
        
    }
}
extension RegisterViewController {
    func takePhotoCamera() {
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            let cameraViewController = CameraViewController(croppingParameters: self.croppingParameters, allowsLibraryAccess: self.libraryEnabled) { [weak self] image, asset in
                //self?.profieImageView.image = image
                //self?.placeholderProfileImageView.isHidden = true
                self?.selectedProfileImage = image
                //self?.deleteButton.isHidden = false
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
            let libraryViewController = CameraViewController.imagePickerViewController(croppingParameters: self.croppingParameters) { [weak self] image, asset in
                //self?.profieImageView.image = image
                //self?.selectedProfileImage = image
                //self?.placeholderProfileImageView.isHidden = true
                //self?.deleteButton.isHidden = false
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(libraryViewController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
}
extension RegisterViewController : WWCalendarTimeSelectorProtocol {
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print ("tag = \(selector.view.tag)")
        print("Selected \n\(date)\n---")
        birthDateTextField.showInfo("")
        birthDateTextField.text = date.toString(dateFormat:"dd/MM/yyyy")
        birthDateTime = Int(date.timeIntervalSince1970)
    }
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        
        
        //If you want date with 18 year less you can use Calendar unit year for that.
        
        let date_less16 = Calendar.current.date(byAdding: .year, value: -17, to: Date())
        
        let dateMax = Calendar.current.date(byAdding: .day, value: -1, to: date_less16!)
        print(dateMax!)
        if date < dateMax! {
            //myDate is earlier than Now (date and time)
            return true
        } else {
            //myDate is equal or after than Now (date and time)
            return false
        }
    }
    
}
extension RegisterViewController :RegisterView{
    func onSuccessGetCaptch(captchaCode: String) {
        self.captchInfo = captchaCode
        labelCaptcha.text = decryptData(data: captchaCode)
        labelCaptcha.font = UIFont(name: "PartyLetPlain", size: 35)
        textfieldEnterCaptcha.text = ""
    }
    
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.registerBtn.startAnimation()
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.registerBtn.stopAnimation(animationStyle: .shake, completion: {
            
        })
    }
    
    func onSucess() {
        self.registerBtn.stopAnimation()
        self.performSegue(withIdentifier: "VerficationCodeViewController", sender: nil)
    }
    
    func onError(message: String) {
        textfieldEnterCaptcha.text = ""
        reloadCaptcha()
        self.showErrorMessage(message: message)
    }
    
    
}

extension RegisterViewController: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        birthDateTextField.showInfo("")
        birthDateTextField.text = date.toString(dateFormat:"dd/MM/yyyy")
        birthDateTime = date.dateToTimeStamp()
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker() -> GMDatePicker {
        
        let datePicker = GMDatePicker()
        datePicker.delegate = self
        let calendar = Calendar.current
        //calendar.timeZone = TimeZone(identifier: "UTC")!
        var _: Date? = calendar.date(byAdding: .year, value: -3, to: Date())
        //        if let userBirthDate = self.childBirthDate{
        //            startingDate = childBirthDate.toDate(withFormat: "dd-MM-yyyy")
        //        }
        //datePicker.config.st_tingDate ?? Date()
        _ = calendar.date(byAdding: .year, value: -3, to: Date())
        let date_less16 = calendar.date(byAdding: .year, value: -17, to: Date())
        datePicker.config.maxDate =  calendar.date(byAdding: .day, value: -1, to: date_less16!)
        //datePicker.config.minDate =  calendar.date(byAdding: .day, value: -1, to: date_less16!)
        datePicker.config.animationDuration = 0.0
        
        datePicker.config.cancelButtonTitle = "CancelPicker".localized()
        datePicker.config.confirmButtonTitle = "Done".localized()
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor.frenchBlue
        
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
        return datePicker
    }
    
}
