//
//  VerficationCodeViewController.swift
//  Najeez
//
//  Created by passant on 5/13/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit
import PinCodeTextField
import TransitionButton
import libPhoneNumber_iOS
import TweeTextField

enum VerificationCase {
    case Authorization
    case Profile
}
class VerficationCodeViewController: UIViewController {
    
    
    @IBOutlet weak var verificationCodeContainer: UIView!
    @IBOutlet weak var sendVerificationCodeBtn: TransitionButton!
    @IBOutlet weak var resendVerficationCodeBtn: TransitionButton!
    @IBOutlet weak var verificationCode: KWVerificationCodeView!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    var phone = ""
    var timer = Timer()
    var seconds = 60
    let verficationCodePresenter = VerificationCodePresenter()
    var verficationCode = ""
    var verificationCase = VerificationCase.Profile
    override func viewDidLoad() {
        super.viewDidLoad()
        verificationCode.focus()
        verificationCode.delegate = self
        verificationCode.underlineColor = UIColor(hexFromString: "#EDEDF1")
        verificationCode.underlineSelectedColor = UIColor(hexFromString: "#B190C1")
        self.verficationCodePresenter.attachView(view: self)
        //sendVerificationCodeBtn.isEnabled = false
        verificationCodeContainer.layer.shadowColor = UIColor.black.cgColor
        verificationCodeContainer.layer.shadowOpacity = 0.2
        verificationCodeContainer.layer.shadowOffset = .zero
        verificationCodeContainer.layer.shadowRadius = 10
        runTimer()
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        verificationCode.semanticContentAttribute = .forceLeftToRight
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
    }
    
    
    func runTimer() {
        self.seconds = 60
        DispatchQueue.main.async {
            self.resendVerficationCodeBtn.isEnabled = false
            self.resendVerficationCodeBtn.backgroundColor = .lightGray
            //            self.resendVerficationCodeBtn.setTitleColor(UIColor.lightGray, for: .normal)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
            DispatchQueue.main.async {
                self.resendVerficationCodeBtn.isEnabled = true
                self.resendVerficationCodeBtn.backgroundColor = UIColor(hexString: "#9E75B2")
                //               self.resendVerficationCodeBtn.setTitleColor(UIColor(hexString: "#ef3622"), for: .normal)
            }
        }
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimer() {
        if seconds != 0
        {
            DispatchQueue.main.async {
                self.seconds -= 1     //This will decrement(count down)the seconds.
                self.timerLbl.text = " 00:\(self.seconds)" //This will update the label.
            }
            
        }
        else{
            DispatchQueue.main.async {
                self.timer.invalidate()
            }
            
        }
    }
    
    @IBAction func dismissVerficationCode(_ sender: Any) {
        print("back")
        DispatchQueue.main.async {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)//(animated: true, completion: nil)
        }
            else{
                self.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func verfiyBtnAction(_ sender: UIButton) {
        if !verificationCode.hasValidCode() {
                self.showErrorMessage(message: "Please enter verification code first".localized())
            return
        }
        if verificationCase == .Authorization {
            self.verficationCodePresenter.doSendVerificationCode(verificationCode: Int(self.verificationCode.getVerificationCode().replacedArabicDigitsWithEnglish)! )
        }
        else{
            self.verficationCodePresenter.doSendVerificationCodeToChangePhone(verificationCode: Int(self.verificationCode.getVerificationCode().replacedArabicDigitsWithEnglish)!, mobile: self.phone)
        }
    }
    @IBAction func resendVerificationCodeBtnAction(_ sender: Any) {
        self.runTimer()
        self.verficationCodePresenter.doResendSendVerificationCode(secondPhone: self.phone)
    }
    func goToHome(){
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //       //appDelegate.setupMenu()
        if #available(iOS 13.0, *) {
            let scene = UIApplication.shared.connectedScenes.first
            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                let mainwindow = sd.window
                let stry = UIStoryboard(name: "Main", bundle: nil)
                mainwindow?.rootViewController = stry.instantiateInitialViewController()
            }
        }
        else{
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            let stry = UIStoryboard(name: "Main", bundle: nil)
            rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
        }
        
    }
}

extension VerficationCodeViewController :KWVerificationCodeViewDelegate{
    func didChangeVerificationCode() {
       // sendVerificationCodeBtn.isEnabled = verificationCode.hasValidCode()
        if  verificationCode.hasValidCode() {
            self.verfiyBtnAction(self.sendVerificationCodeBtn)
        }
    }
    
    
}

extension VerficationCodeViewController:VerficationCodeView{
    func successChangePhone(message: String) {
        self.showSucessMessage(message: message)
        self.sendVerificationCodeBtn.stopAnimation()
        if let phoneVerified = UserManager.sharedInstance.getUser()?.mobileVerified {
            if phoneVerified {
                self.dismiss(animated: true, completion: nil)
                switch verificationCase {
                case .Authorization :
                    
                    goToHome()
                case .Profile:
                    goToHome()
                }
                
                
                
            }else{
                
            }
        }
    }
    
    func showResendLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.resendVerficationCodeBtn.startAnimation()
    }
    
    func hideResendLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.resendVerficationCodeBtn.stopAnimation(animationStyle: .shake, completion: {
            
        })
    }
    func onSucessResend(message: String) {
        self.view.isUserInteractionEnabled = true
        self.resendVerficationCodeBtn.stopAnimation()
        self.showSucessMessage(message: "Verification code has been sent successfully".localized())
    }
    
    
    
    //----------------------------------------------------------------------------------------
    
    func showVerficationLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.sendVerificationCodeBtn.startAnimation()
    }
    
    func hideVerficationLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.sendVerificationCodeBtn.stopAnimation(animationStyle: .shake, completion: {
            
        })
    }
    
    func onSucessVerfication() {
        self.sendVerificationCodeBtn.stopAnimation()
        if let phoneVerified = UserManager.sharedInstance.getUser()?.mobileVerified {
            if phoneVerified {
                self.dismiss(animated: true, completion: nil)
                switch verificationCase {
                case .Authorization :
                    UserManager.sharedInstance.userRegisterForFirstTime = 1
                    goToHome()
                case .Profile:
                    goToHome()
                }
                
                
                
            }else{
                
            }
        }
    }
    
    func onErrorVerfication(message: String) {
        self.view.isUserInteractionEnabled = true
        self.verificationCode.clear()
        self.showErrorMessage(message: message)
    }
    
    
}

extension VerficationCodeViewController:PinCodeTextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
        self.verficationCode = value
    }
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
}
private var xoAssociationKeyForBottomConstrainInVC: UInt8 = 0

extension UIViewController {
    
    var containerDependOnKeyboardBottomConstrain :NSLayoutConstraint! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKeyForBottomConstrainInVC) as? NSLayoutConstraint
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKeyForBottomConstrainInVC, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func watchForKeyboard () {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.containerDependOnKeyboardBottomConstrain.constant = keyboardFrame.height
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.containerDependOnKeyboardBottomConstrain.constant = 0
            self.view.layoutIfNeeded()
        })
    }
}



