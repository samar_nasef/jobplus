//
//  BrowserVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 2/10/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import WebKit

class BrowserVCtrl: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var register = true
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var webToLoadWebView: UIView!
    @IBOutlet weak var headerTitleLbl: UILabel!
    
    
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        webView = WKWebView(frame: webToLoadWebView.bounds, configuration: WKWebViewConfiguration())
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webToLoadWebView.addSubview(webView)
        if register {
            headerTitleLbl.text = "Register with us as company".localized()
            if L102Language.isRTL {
                if let url = URL(string: WebService.URLDASHBOARDDOMAINar + WebService.URLCOMPANYSIGNUPDOMAIN ){
                    webView.load(URLRequest(url: url))
                    webView.allowsBackForwardNavigationGestures = true
                }
            }
            else{
                if let url = URL(string: WebService.URLDASHBOARDDOMAINen + WebService.URLCOMPANYSIGNUPDOMAIN ){
                    webView.load(URLRequest(url: url))
                    webView.allowsBackForwardNavigationGestures = true
                }
            }
        }
        else{
            headerTitleLbl.text = "Login as company".localized()
            if L102Language.isRTL {
                if let url = URL(string: WebService.URLDASHBOARDDOMAINar + WebService.URLCOMPANYSIGNINDOMAIN ){
                    webView.load(URLRequest(url: url))
                    webView.allowsBackForwardNavigationGestures = true
                }
            }
            else{
                if let url = URL(string: WebService.URLDASHBOARDDOMAINen + WebService.URLCOMPANYSIGNINDOMAIN ){
                    webView.load(URLRequest(url: url))
                    webView.allowsBackForwardNavigationGestures = true
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
