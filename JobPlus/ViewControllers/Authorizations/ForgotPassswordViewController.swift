//
//  ForgotPassswordViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/8/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import TweeTextField
class ForgotPassswordViewController: UIViewController {
    @IBOutlet weak var forgotPasswordButton: TransitionButton!
    @IBOutlet weak var countryCodeField: CTKFlagPhoneNumberTextField!
    @IBOutlet weak var phoneNumberTextField: TweeAttributedTextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneViewInternal: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var countryCodeLbl: UILabel!
    let forgetPasswordPresenter = ForgetPasswordPresenter()
    
    @objc func getFCode(_ notification: NSNotification){
        print("getCode \(String(describing: notification.userInfo?["code"]))")
        if let code = notification.userInfo?["code"] {
            self.countryCodeLbl.text = code as? String
            self.resetCustomTextFieldsErrorMessages(self.phoneNumberTextField)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //forgotPasswordButton.dropShadow()
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getFCode"), object: nil)
               NotificationCenter.default.addObserver(self,
                                                      selector: #selector(self.getFCode),
                                                             name: NSNotification.Name(rawValue: "getFCode"),
                                                             object: nil)
        self.forgetPasswordPresenter.attachView(view: self)
        phoneView.layer.cornerRadius = 2
        phoneView.layer.shadowColor = UIColor.black.cgColor
        phoneView.layer.shadowOpacity = 0.2
        phoneView.layer.shadowOffset = .zero
        phoneView.layer.shadowRadius = 10
        setupKeyboard()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        self.view.changeFont()
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async{
            self.phoneViewInternal.semanticContentAttribute = .forceLeftToRight
            self.countryCodeField.semanticContentAttribute = .forceLeftToRight
        }
    }
    func setupKeyboard(){
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.returnKeyType = .next
        phoneNumberTextField.infoTextColor = UIColor(hexString: "#FF2600")
        phoneNumberTextField.infoFontSize = 14.0
        phoneNumberTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        phoneNumberTextField.activeLineWidth = 2
        phoneNumberTextField.animationDuration = 0.5
        phoneNumberTextField.minimumPlaceholderFontSize = 15
        phoneNumberTextField.originalPlaceholderFontSize = 15
        phoneNumberTextField.placeholderDuration = 0.2
        phoneNumberTextField.placeholderColor = .darkGray
        phoneNumberTextField.delegate = self
        phoneNumberTextField.textColor = UIColor(hexString: "#8C8C8C")
        phoneNumberTextField.placeholder = "Phone".localized()
        setUpCountryCode()
        countryCodeLbl.text = countryCodeField.getCountryPhoneCode() ?? ""
        
    }
    func setUpCountryCode(){
        countryCodeField.borderStyle = .none
        countryCodeField.parentViewController = self
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
    }
    private func getCustomTextFieldInputAccessoryView(with items: [UIBarButtonItem]) -> UIToolbar {
        let toolbar: UIToolbar = UIToolbar()
        
        toolbar.barStyle = UIBarStyle.default
        toolbar.items = items
        toolbar.sizeToFit()
        return toolbar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if validateForgotPassword() {
            var phone_no = phoneNumberTextField.text!.replacedArabicDigitsWithEnglish
            if phone_no.first == "0" {
              phone_no.remove(at: (phone_no.startIndex))
            }
            let phone = countryCodeField.text! + phone_no
            forgetPasswordPresenter.doForgetPassword(mobile: phone)
        }
    }
    func validateForgotPassword()->Bool{
        var valid = true
        if let phoneNumber = self.phoneNumberTextField.text , !phoneNumber.isBlank{
            if phoneNumber.isValidPhoneNumber(code: countryCodeField.getCountryPhoneCode() ?? "+966") {
                print("valid phoneNumber")
            }else {
                let errorMessage = "please enter valid phone number".localized()
                self.phoneNumberTextField.showInfo(errorMessage, animated: true)
                
                valid = false
            }
        }else {
            print("error phoneNumber")
            let errorMessage  = "please enter your phone number".localized()
            self.phoneNumberTextField.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        return valid
        
    }
    
    func countryCodeTxtFieldConfiguration(){
        
        countryCodeField.borderStyle = .none
        countryCodeField.parentViewController = self
        countryCodeField.textAlignment = .center
        //        Example of customizing the textField input accessory view
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
        countryCodeField.flagSize = CGSize(width: 25, height: 25)
        
//        countryCodeField.semanticContentAttribute = .forceLeftToRight
//        countryCodeField.leftView?.semanticContentAttribute = .forceLeftToRight
//        countryCodeField.setFlag(with: Locale.current.regionCode!)
        print(Locale.current.regionCode ?? "SA")
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeForgetPassSeg" {
            let destintion  = segue.destination as? ForgotPasswordTokenViewController
            let phone = phoneNumberTextField.text!
            destintion?.phone = phone
            destintion?.verficationCodeStr = countryCodeField.text!
        }
    }
    
    
}
extension ForgotPassswordViewController :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == self.phoneNumberTextField) {
            self.view.endEditing(true)
        }
            
        else {
            self.view.endEditing(true)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        if textField == phoneNumberTextField {
//            if phoneNumberTextField.text?.count == 0 && string == "0" {
//
//                return false
//            }
            if range.location == 14 {
                
                return false
            }
        }
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.phoneNumberTextField {
            self.phoneNumberTextField.showInfo("", animated: true)
        }
    }
}
extension ForgotPassswordViewController:ForgetPasswordView {
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.forgotPasswordButton.startAnimation()
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.forgotPasswordButton.stopAnimation()
    }
    
    func onSucess() {
        
        self.showSucessMessage(message:"Code Sent Sucessfully".localized())
        self.performSegue(withIdentifier:"changeForgetPassSeg", sender: self)
    }
    
    func onError(message: String) {
        self.view.isUserInteractionEnabled = true
        self.showErrorMessage(message: message)
    }
    
    
}
