//
//  HomeViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/4/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS
import SafariServices

class HomeViewController: UIViewController {
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var rigesterCompanyBtn: UIButton!
    @IBOutlet weak var companyLoginBtn: UIButton!
    @IBOutlet weak var companiesHeaderView: UIView!
    @IBOutlet weak var usersHeaderView: UIView!
    //home controller opened at launchining or from job details as skipped user
    var homeCase = VerificationCase.Authorization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupNavigationBar()
       
        //addBarButtonForSkip()
        self.view.changeFont()
        // Do any additional setup after loading the view.
//        let phone : String = "+966550555555"
//        let phonUtil = NBPhoneNumberUtil()
//        do {
//            let nbPhone : NBPhoneNumber = try phonUtil.parse(withPhoneCarrierRegion: phone)
//            let countryCode : NSNumber = nbPhone.countryCode
//            let number = nbPhone.nationalNumber
//            print("--------\(countryCode) \(number)")
//        }catch{
//            //Phone Number Invalid!
//        }
    }
    func addBarButtonForSkip(){
        
        let button = UIButton(type: .custom)
        button.setTitle("Skip".localized(), for: .normal)
        button.titleLabel?.font = FontHelper.defaultRegularFontWithSize(size: 17.7)
        button.setTitleColor(UIColor.trolleyGrey, for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 80, height: 25)
        button.addTarget(self, action: #selector(didTapSkip), for: UIControl.Event.touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        
        navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func skipLogin(_ sender: Any) {
        UserManager.sharedInstance.removeUser()
        if #available(iOS 13.0, *) {
               let scene = UIApplication.shared.connectedScenes.first
               if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                   let mainwindow = sd.window
                    let stry = UIStoryboard(name: "Main", bundle: nil)
                mainwindow?.rootViewController = stry.instantiateInitialViewController()
                   }
               }
               else{
               let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                      let stry = UIStoryboard(name: "Main", bundle: nil)
                      rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
               }
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    @objc func didTapSkip(sender: AnyObject){
    }
    override func viewDidAppear(_ animated: Bool) {
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    func setupNavigationBar(){
     self.navigationController?.presentTransparentNavigationBar()
   //  self.navigationController?.se
     //self.navigationController?.topViewController?.title = ""
    }
    @IBAction func companySignUp(_ sender: Any) {
        if L102Language.isRTL {
            if let url = URL(string: WebService.URLDASHBOARDDOMAINar + WebService.URLCOMPANYSIGNUPDOMAIN ){
                let vc = SFSafariViewController(url: url)
                
                present(vc, animated: true)
            }
        }
        else{
            if let url = URL(string: WebService.URLDASHBOARDDOMAINen + WebService.URLCOMPANYSIGNUPDOMAIN ){
                let vc = SFSafariViewController(url: url)
                present(vc, animated: true)
            }
        }
    }
    @IBAction func companySinIn(_ sender: Any) {
        if L102Language.isRTL {
            if let url = URL(string: WebService.URLDASHBOARDDOMAINar + WebService.URLCOMPANYSIGNINDOMAIN ){
                let vc = SFSafariViewController(url: url)
                
                present(vc, animated: true)
            }
        }
        else{
            if let url = URL(string: WebService.URLDASHBOARDDOMAINen + WebService.URLCOMPANYSIGNINDOMAIN ){
                let vc = SFSafariViewController(url: url)
                present(vc, animated: true)
            }
        }
    }
    
    func setupUI(){
        usersHeaderView.customLayerGradient(color1: "#161650" , color2: "#161650", cornerRadius: 10)
        companiesHeaderView.customLayerGradient(color1: "#00CCFF", color2: "#00CCFF", cornerRadius: 10)
        signInButton.customLayerGradient(color1: "#47C0AC", color2: "#47C0AC", cornerRadius: 22.5)
        signInButton.titleLabel?.font = FontHelper.defaultRegularFontWithSize(size: 16)
        signupButton.customLayerGradient(color1: "#47C0AC", color2: "#47C0AC", cornerRadius: 22.5)
        signupButton.titleLabel?.font = FontHelper.defaultRegularFontWithSize(size: 16)
        rigesterCompanyBtn.customLayerGradient(color1: "#47C0AC", color2: "#47C0AC", cornerRadius: 22.5)
        rigesterCompanyBtn.titleLabel?.font = FontHelper.defaultRegularFontWithSize(size: 16)
        companyLoginBtn.customLayerGradient(color1: "#47C0AC", color2: "#47C0AC", cornerRadius: 22.5)
        companyLoginBtn.titleLabel?.font = FontHelper.defaultRegularFontWithSize(size: 16)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.performSegue(withIdentifier:"LoginViewController", sender: self)
    }
    @IBAction func registerAction(_ sender: Any) {
         self.performSegue(withIdentifier:"RegisterViewController", sender: self)
    }
    
    @IBAction func registerAsCompany(_ sender: UIButton) {
        //http://api.parttime-app.com/api/v1/
        //stage:http://dashboard.jobs.10degrees.io/company/signup
        //production:http://dashboard.parttime-app.com/company/signup
//        if let url = URL(string: WebService.URLDASHBOARDDOMAIN + WebService.URLCOMPANYSIGNUPDOMAIN ){
//            self.openUrl(url: url)
//        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginViewController" {
            if let destinationVC = segue.destination as? LoginViewController{
                destinationVC.loginCase = homeCase
            }
            
        }else if segue.identifier == "RegisterViewController" {
            if let destinationVc = segue.destination as? RegisterViewController{
                destinationVc.registerCase = self.homeCase
            }
        }
        else if segue.identifier == "loginComapnySeg" {
            let destination = segue.destination as! BrowserVCtrl
            destination.register = false
        }
    }
    
}
