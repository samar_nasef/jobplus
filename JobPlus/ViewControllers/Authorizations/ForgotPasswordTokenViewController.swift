//
//  ForgotPasswordTokenViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/19/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import TweeTextField

class ForgotPasswordTokenViewController: UIViewController {
    
    @IBOutlet weak var verificationCode: KWVerificationCodeView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var rePasswordView: UIView!
    @IBOutlet weak var forgotPasswordButton: TransitionButton!
    //@IBOutlet weak var emailTokenTextField: TweeAttributedTextField!
    @IBOutlet weak var passwordTextField: TweeAttributedTextField!
    @IBOutlet weak var rePasswordTextField: TweeAttributedTextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var forgotPassBtn: TransitionButton!
    @IBOutlet weak var CodeView: UIView!
    let forgetPasswordPresenter = ForgetPasswordPresenter()
    var phone = ""
    var verficationCodeStr = ""
    var countryCode = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //forgotPasswordButton.dropShadow()
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        self.forgetPasswordPresenter.attachView(view: self)
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        
        CodeView.layer.cornerRadius = 2
        CodeView.layer.shadowColor = UIColor.black.cgColor
        CodeView.layer.shadowOpacity = 0.2
        CodeView.layer.shadowOffset = .zero
        CodeView.layer.shadowRadius = 5
        
        newPasswordView.layer.cornerRadius = 2
        newPasswordView.layer.shadowColor = UIColor.black.cgColor
        newPasswordView.layer.shadowOpacity = 0.2
        newPasswordView.layer.shadowOffset = .zero
        newPasswordView.layer.shadowRadius = 10
        
        rePasswordView.layer.cornerRadius = 2
        rePasswordView.layer.shadowColor = UIColor.black.cgColor
        rePasswordView.layer.shadowOpacity = 0.2
        rePasswordView.layer.shadowOffset = .zero
        rePasswordView.layer.shadowRadius = 10
        
        setupTextFields()
        verificationCode.delegate = self
        
        // Do any additional setup after loading the view.
        self.view.changeFont()
        verificationCode.focus()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.verificationCode.backgroundColor = .white
            self.CodeView.backgroundColor = .white
            self.view.bringSubviewToFront(self.CodeView)
        }
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setupTextFields(){
//        emailTokenTextField.delegate = self
//        emailTokenTextField.returnKeyType = .next
//        emailTokenTextField.infoTextColor = UIColor(hexString: "#FF2600")
//        emailTokenTextField.infoFontSize = 12.0
//        emailTokenTextField.activeLineColor = UIColor(hexString: "#00AFDF")
//        emailTokenTextField.activeLineWidth = 2
//        emailTokenTextField.animationDuration = 0.5
//        emailTokenTextField.minimumPlaceholderFontSize = 13
//        emailTokenTextField.originalPlaceholderFontSize = 17
//        emailTokenTextField.placeholderDuration = 0.2
//        emailTokenTextField.placeholderColor = UIColor(hexString: "#8c8c8c")
//        emailTokenTextField.placeholder = "Enter SMS Token".localized()
//        emailTokenTextField.isSecureTextEntry = false
        verificationCode.underlineColor = UIColor(hexFromString: "#EDEDF1")
        verificationCode.underlineSelectedColor = UIColor(hexFromString: "#B190C1")
        passwordTextField.delegate = self
        passwordTextField.returnKeyType = .next
        passwordTextField.infoTextColor = UIColor(hexString: "#FF2600")
        passwordTextField.infoFontSize = 14.0
        passwordTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        passwordTextField.activeLineWidth = 2
        passwordTextField.animationDuration = 0.5
        passwordTextField.minimumPlaceholderFontSize = 15
        passwordTextField.originalPlaceholderFontSize = 15
        passwordTextField.placeholderDuration = 0.2
        passwordTextField.placeholderColor = .darkGray
        passwordTextField.placeholder = "newPassword".localized()
        passwordTextField.isSecureTextEntry = true
        
        rePasswordTextField.delegate = self
        rePasswordTextField.returnKeyType = .done
        rePasswordTextField.infoTextColor = UIColor(hexString: "#FF2600")
        rePasswordTextField.infoFontSize = 14.0
        rePasswordTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        rePasswordTextField.activeLineWidth = 2
        rePasswordTextField.animationDuration = 0.5
        rePasswordTextField.minimumPlaceholderFontSize = 15
        rePasswordTextField.originalPlaceholderFontSize = 15
        rePasswordTextField.placeholderDuration = 0.2
        rePasswordTextField.placeholderColor = .darkGray
        rePasswordTextField.placeholder = "RePassword".localized()
        rePasswordTextField.isSecureTextEntry = true
    }
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if validateForgotPasswordFields(){
            if !verificationCode.hasValidCode() {
                self.showErrorMessage(message: "Please enter verification code first".localized())
            }
            else{
                var phone_no = self.phone
                           if phone_no.first == "0" {
                             phone_no.remove(at: (phone_no.startIndex))
                           }
                           let phone_All_no = verficationCodeStr + phone_no
               self.forgetPasswordPresenter.doForgetPasswordTokenRequest(phone: phone_All_no, password: passwordTextField.text!, forgotPasswordToken: self.verificationCode.getVerificationCode().replacedArabicDigitsWithEnglish)
            }
        }
        else if !verificationCode.hasValidCode() {
                self.showErrorMessage(message: "Please enter verification code first".localized())
        }
    }
    func validateForgotPasswordFields() -> Bool {
        var valid = true
//        if let emailToken = self.emailTokenTextField.text , emailToken.isBlank{
//            let errorMessage = "please enter your email token".localized()
//            self.emailTokenTextField.showInfo(errorMessage, animated: true)
//            valid = false
//        }
        if let password = self.passwordTextField.text , !password.isBlank {
            if password.isPasswordMoreThaneightCharacters {
                print("valid password")
            }else {
                let errorMessage = "password must be more than eight characters".localized()
                self.passwordTextField.showInfo(errorMessage, animated: true)
                valid = false
            }
        }else {
            print("error password")
            let errorMessage = "please enter your new password".localized()
            self.passwordTextField.showInfo(errorMessage, animated: true)
            valid = false
        }
        if let confirmPassword = self.rePasswordTextField.text , !confirmPassword.isBlank {
                
                if self.rePasswordTextField.text != self.passwordTextField.text {
                    self.rePasswordTextField.showInfo("passwordNotMatched".localized())
                    valid = false
                }
                else{
                print("valid confirmPassword")
            }
        }else {
            print("error confirmPassword")
            let errorMessage = "please confirm your password".localized()
            self.rePasswordTextField.showInfo(errorMessage, animated: true)
            valid = false
        }
        
        
        return valid
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension ForgotPasswordTokenViewController :KWVerificationCodeViewDelegate{
    func didChangeVerificationCode() {
//        if  verificationCode.hasValidCode() {
//            self.passwordTextField.isEnabled = true
//            self.rePasswordTextField.isEnabled = true
//            self.forgotPassBtn.isEnabled = true
//        }
//        else{
//            self.passwordTextField.isEnabled = false
//            self.rePasswordTextField.isEnabled = false
//            self.forgotPassBtn.isEnabled = false
//        }
    }
    
    
}
extension ForgotPasswordTokenViewController :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField == self.emailTokenTextField {
//            self.passwordTextField.becomeFirstResponder()
//        } else
       if textField == self.passwordTextField {
             self.rePasswordTextField.becomeFirstResponder()
        }
        if(textField == self.rePasswordTextField) {
            self.view.endEditing(true)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
//        if textField == self.emailTokenTextField {
//            self.emailTokenTextField.showInfo("", animated: true)
//        }
        if textField == self.passwordTextField {
            self.passwordTextField.showInfo("", animated: true)
        }
        if textField == self.rePasswordTextField {
            self.rePasswordTextField.showInfo("", animated: true)
        }
    }
}
extension ForgotPasswordTokenViewController:ForgetPasswordView {
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.forgotPasswordButton.startAnimation()
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.forgotPasswordButton.stopAnimation()
    }
    
    func onSucess() {
        
        self.showSucessMessage(message:"Password changed Successfully".localized())
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func onError(message: String) {
        self.view.isUserInteractionEnabled = true
        self.showErrorMessage(message: message)
    }
    
    
}
