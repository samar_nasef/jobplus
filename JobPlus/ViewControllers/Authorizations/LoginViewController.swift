//
//  LoginViewController.swift
//  PartTime
//
//  Created by mohamed shaat on 7/4/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import TransitionButton
import TweeTextField
import SafariServices
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var loginButtonAction: TransitionButton!

    @IBOutlet weak var left_phone_icon_constraint: NSLayoutConstraint!
    @IBOutlet weak var right_phone_icon_constraint: NSLayoutConstraint!
    
    @IBOutlet weak var phone_img_left: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    
    @IBOutlet weak var phoneViewInternal: UIView!
    @IBOutlet weak var phone_img_right: UIImageView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var countryCodeField: CTKFlagPhoneNumberTextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var phoneNumberTextField: TweeAttributedTextField!
    @IBOutlet weak var passwordTextField: TweeAttributedTextField!
    private var loginPresenter = LoginPresenter()
    var loginCase = VerificationCase.Authorization
    var appeared = true
    @objc func getCode(_ notification: NSNotification){
        print("getCode \(String(describing: notification.userInfo?["code"]))")
        if let code = notification.userInfo?["code"] {
            if !appeared {
                return
            }
            self.countryCodeLbl.text = code as? String
            self.resetCustomTextFieldsErrorMessages(self.phoneNumberTextField)
        }
    }

    
     override func viewWillDisappear(_ animated: Bool) {
        
         //NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getCode"), object: nil)
    }
    
    
    override func viewDidLoad() {
    super.viewDidLoad()
        countryCodeField.delegate = self
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getCode"), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.getCode(_:)),
                                                      name: NSNotification.Name(rawValue: "getCode"),
                                                      object: nil)
        phoneView.layer.cornerRadius = 2
        phoneView.layer.shadowColor = UIColor.black.cgColor
        phoneView.layer.shadowOpacity = 0.2
        phoneView.layer.shadowOffset = .zero
        phoneView.layer.shadowRadius = 10
        
        passwordView.layer.cornerRadius = 2
        passwordView.layer.shadowColor = UIColor.black.cgColor
        passwordView.layer.shadowOpacity = 0.2
        passwordView.layer.shadowOffset = .zero
        passwordView.layer.shadowRadius = 10
        self.loginPresenter.attachView(view: self)
        self.navigationController?.topViewController?.title = ""
        setupTextFields()
        //self.phoneNumberTextField.becomeFirstResponder()
        self.countryCodeTxtFieldConfiguration()
        self.view.changeFont()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
//        if L102Language.isRTL {
//            DispatchQueue.main.async{
//                self.phone_img_right.isHidden = true
//                self.left_phone_icon_constraint.constant = 0.0
//            }
//        }
//        else{
//            DispatchQueue.main.async{
//                self.phone_img_left.isHidden = true
//                self.right_phone_icon_constraint.constant = 0.0
//            }
//        }
    }

    override func viewDidAppear(_ animated: Bool) {

            DispatchQueue.main.async{
                self.phoneViewInternal.semanticContentAttribute = .forceLeftToRight
                self.countryCodeField.semanticContentAttribute = .forceLeftToRight
            }
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getCode"), object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.getCode(_:)),
//                                                      name: NSNotification.Name(rawValue: "getCode"),
//                                                      object: nil)
    }
    override func viewDidLayoutSubviews() {
        
    }
    func countryCodeTxtFieldConfiguration(){
        
        countryCodeField.borderStyle = .none
        countryCodeField.parentViewController = self
        countryCodeField.textAlignment = .center
        //        Example of customizing the textField input accessory view
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
        countryCodeField.flagSize = CGSize(width: 25, height: 25)
        
//        countryCodeField.semanticContentAttribute = .forceLeftToRight
//        countryCodeField.leftView?.semanticContentAttribute = .forceLeftToRight
        countryCodeField.setFlag(with: Locale.current.regionCode ?? "SA")
        print(Locale.current.regionCode ?? "SA")
    }
    func checkCountryCodeEmpty(){
        //  countryCodeTxtField.setFlag(with: Locale.current.regionCode!)
        if countryCodeField.text == "" {
            countryCodeField.text = "+966"
            countryCodeField.setFlag(with: "SA")
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        appeared = true
        //loginButtonAction.dropShadow()
        loginButtonAction.layer.cornerRadius = 26
        DispatchQueue.main.async{
            self.phoneViewInternal.semanticContentAttribute =  .forceLeftToRight
            self.phoneViewInternal.semanticContentAttribute = .forceLeftToRight
        }
        self.navigationController?.presentTransparentNavigationBar()
        // checkCountryCodeEmpty()
    }
    
    
    func setupTextFields(){
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.returnKeyType = .next
        phoneNumberTextField.infoTextColor = UIColor(hexString: "#FF2600")
        phoneNumberTextField.infoFontSize = 14.0
        phoneNumberTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        phoneNumberTextField.activeLineWidth = 2
        phoneNumberTextField.animationDuration = 0.5
        phoneNumberTextField.minimumPlaceholderFontSize = 15
        phoneNumberTextField.originalPlaceholderFontSize = 15
        phoneNumberTextField.placeholderDuration = 0.2
        phoneNumberTextField.placeholderColor = .darkGray
        phoneNumberTextField.delegate = self
        phoneNumberTextField.textColor = UIColor(hexString: "#8C8C8C")
        //phoneNumberTextField.placeholderLabel.text = "Phone".localized()
        //phoneNumberTextField.localizationKey = "Phone"
        phoneNumberTextField.placeholder = "Phone".localized()
        
        countryCodeField.keyboardType = .numberPad
        countryCodeField.semanticContentAttribute =  .forceLeftToRight
        
        passwordTextField.returnKeyType = .done
        passwordTextField.infoTextColor = UIColor(hexString: "#FF2600")
        passwordTextField.infoFontSize = 14.0
        passwordTextField.activeLineColor = UIColor(hexString: "#00AFDF")
        passwordTextField.activeLineWidth = 2
        passwordTextField.animationDuration = 0.5
        passwordTextField.minimumPlaceholderFontSize = 15
        passwordTextField.originalPlaceholderFontSize = 15
        passwordTextField.placeholderDuration = 0.2
        passwordTextField.placeholderColor = .darkGray
        passwordTextField.textColor = UIColor(hexString: "#8C8C8C")
        passwordTextField.delegate = self
        //passwordTextField.placeholderLabel.text = "Password".localized()
        //passwordTextField.localizationKey = "Password"
        passwordTextField.placeholder = "Password".localized()
        passwordTextField.isSecureTextEntry = true
        passwordTextField.delegate = self
        setUpCountryCode()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setUpCountryCode(){
        countryCodeField.borderStyle = .none
        countryCodeField.parentViewController = self
        let items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save, target: self, action: nil),
            UIBarButtonItem(title: "Item 1", style: .plain, target: self, action: nil),
            UIBarButtonItem(title: "Item 2", style: .plain, target: self, action: nil)
        ]
        countryCodeField.textFieldInputAccessoryView = getCustomTextFieldInputAccessoryView(with: items)
    }
    
    @objc func validate() {
        print(countryCodeField?.getFormattedPhoneNumber() ?? "No formatted phone number")
        print(countryCodeField.getCountryPhoneCode() ?? "No country phone code")
        print(countryCodeField.getRawPhoneNumber() ?? "No raw phone number")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VerficationCodeViewController" {
            if let destnationViewcontroller = segue.destination as? VerficationCodeViewController {
                destnationViewcontroller.verificationCase = .Authorization
            }
        }
    }
    private func getCustomTextFieldInputAccessoryView(with items: [UIBarButtonItem]) -> UIToolbar {
        let toolbar: UIToolbar = UIToolbar()
        
        toolbar.barStyle = UIBarStyle.default
        toolbar.items = items
        toolbar.sizeToFit()
        return toolbar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func forgotPasswordAction(_ sender: Any) {
        appeared = false
        self.performSegue(withIdentifier:"ForgotPassswordViewController", sender: self)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        //self.performSegue(withIdentifier:"VerficationCodeViewController", sender: self)
        if validateLoginTextFields() {
            var phone_no = phoneNumberTextField.text!.replacedArabicDigitsWithEnglish
            if phone_no.first == "0" {
              phone_no.remove(at: (phone_no.startIndex))
            }
            let phone = countryCodeField.text! + phone_no
            self.loginPresenter.doLogin(mobile:phone , password:passwordTextField.text!)
        }
    }
    func validateLoginTextFields() -> Bool {
        var valid = true
        if let phoneNumber = self.phoneNumberTextField.text , !phoneNumber.isBlank{
            if phoneNumber.isValidPhoneNumber(code: countryCodeField.getCountryPhoneCode() ?? "+966") {
                print("valid phoneNumber")
            }else {
                let errorMessage = "please enter valid phone number".localized()
                self.phoneNumberTextField.showInfo(errorMessage, animated: true)
                
               valid = false
            }
        }else {
            print("error phoneNumber")
            let errorMessage  = "enter your phone number".localized()
            self.phoneNumberTextField.showInfo(errorMessage, animated: true)
            valid = false
        }
        if let password = self.passwordTextField.text , !password.isBlank {
            
            if password.isPasswordMoreThaneightCharacters {
                print("valid password")
            }else {
                let errorMessage = "password must be more than eight characters".localized()
                self.passwordTextField.showInfo(errorMessage, animated: true)
                
                  valid = false
            }
        }else {
            print("error password")
            let errorMessage = "enter your password".localized()
            self.passwordTextField.showInfo(errorMessage, animated: true)
              valid = false
        }
        
        return valid
    }
  
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        if textField == phoneNumberTextField {
//            if phoneNumberTextField.text?.count == 0 && string == "0" {
//                
//                return false
//            }
            if range.location == 14 {
                
                return false
            }
        }
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.phoneNumberTextField {
            self.phoneNumberTextField.showInfo("", animated: true)
        }
        if textField  == self.passwordTextField {
            self.passwordTextField.showInfo("", animated: true)
        }
        
    }
}
    
extension LoginViewController :LoginView{
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        self.loginButtonAction.startAnimation()
    }
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        self.loginButtonAction.stopAnimation(animationStyle: .shake, completion: {
            self.loginButtonAction.layer.cornerRadius = 26
        })
    }
    func onSucess() {
        self.view.isUserInteractionEnabled = true
        if let phoneVerified = UserManager.sharedInstance.getUser()?.mobileVerified {
            if phoneVerified {
                self.loginButtonAction.stopAnimation()
                goToHome()
                
            }else{
                self.loginButtonAction.stopAnimation()
                self.performSegue(withIdentifier: "VerficationCodeViewController", sender: nil)
            }
        }
    }
    func goToHome(){
    
        switch loginCase {
        case .Authorization:
           if #available(iOS 13.0, *) {
            let scene = UIApplication.shared.connectedScenes.first
            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                let mainwindow = sd.window
                 let stry = UIStoryboard(name: "Main", bundle: nil)
             mainwindow?.rootViewController = stry.instantiateInitialViewController()
                }
            }
            else{
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                   let stry = UIStoryboard(name: "Main", bundle: nil)
                   rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
            }
        case .Profile:
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: JobDetailsViewController.self) {
                     NotificationCenter.default.post(name: .afterUserLogin ,object: nil, userInfo: nil)
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    
                    break
                }
            }
        }
      
    }
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}
