//
//  MessagesViewController.swift
//  JobPlus
//
//  Created by samar nasef on 13/07/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import Kingfisher
//import GoogleMobileAds

class messagesCell:UITableViewCell{
    
    @IBOutlet weak var messageTitle: UILabel!
    
    @IBOutlet weak var mesasgeImg: UIImageView!
    
    
    @IBOutlet weak var messageDescribtion: UILabel!
    
    @IBOutlet weak var messageDate: UILabel!
    
    
    @IBOutlet weak var messageCount: UILabel!
    
    
}
struct messageItem {
    var title:String
    var des:String
    var Date:String
    var Count:String
    var imagePath:String
}
class MessagesViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
   
    //var bannerView: GADBannerView!
//    private let banner: GADBannerView = {
//       let banner = GADBannerView()
//        banner.adUnitID =  "ca-app-pub-4429154996614971/6621885841"
//        banner.load(GADRequest())
//        banner.backgroundColor = .lightGray
//        return banner
//    }()

    let messagesPresenter = MessagesPresenter()
    
    
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    
    @IBOutlet weak var messagesTableView: UITableView!
//    var MessagesDate:[messageItem] = [messageItem(title: "title1", des: "des1", Date: "13 march 2021", Count: "1", imagePath:"imagePath"),messageItem(title: "title2", des: "des2", Date: " 14 march 2021", Count: "22", imagePath:"imagePath2")]
    var MessagesDate:[messagesItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        messagesTableView.tableFooterView = UIView()
        messagesPresenter.attachView(view: self)
//        messagesPresenter.doGetAllMessages()
        
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        
        self.emptyView.msgLbl.text = "You do not have any messages yet.".localized()
        initPullToRefresh()
//        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
//        bannerView.adUnitID = "ca-app-pub-4429154996614971/3731998622"
//          banner.rootViewController = self
//        view.addSubview(banner)
//        bannerView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
//          addBannerViewToView(bannerView)
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        banner.frame = CGRect(x: 0, y: view.frame.size.height - 100 , width: view.frame.size.width, height: 100).integral
//    }
//    func addBannerViewToView(_ bannerView: GADBannerView) {
//       bannerView.translatesAutoresizingMaskIntoConstraints = false
//       view.addSubview(bannerView)
//       view.addConstraints(
//         [NSLayoutConstraint(item: bannerView,
//                             attribute: .bottom,
//                             relatedBy: .equal,
//                             toItem: bottomLayoutGuide,
//                             attribute: .top,
//                             multiplier: 1,
//                             constant: 0),
//          NSLayoutConstraint(item: bannerView,
//                             attribute: .centerX,
//                             relatedBy: .equal,
//                             toItem: view,
//                             attribute: .centerX,
//                             multiplier: 1,
//                             constant: 0)
//         ])
//      }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessagesDate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! messagesCell
        
        let item = self.MessagesDate[indexPath.row]
//        cell.messageTitle.text =  item.title
//        cell.messageDescribtion.text = item.des
//        cell.messageDate.text = item.Date
//        cell.messageCount.text = item.Count
//
//
//
//        cell.mesasgeImg.kf.indicatorType = .activity
//        let url = URL(string: (item.imagePath ?? "")!)
//
//        let processor = DownsamplingImageProcessor(size: cell.mesasgeImg.size)
//            |> RoundCornerImageProcessor(cornerRadius: 22)
//
//        cell.mesasgeImg.kf.setImage(
//            with: url,
//            placeholder: UIImage(named: "jpLogo_splash"),
//            options: [
//                .processor(processor),
//                .scaleFactor(UIScreen.main.scale),
//                .transition(.fade(1)),
//                .cacheOriginalImage
//            ], completionHandler:
//                {
//                    result in
//                    switch result {
//                    case .success(let value):
//                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
//                    case .failure(let error):
//                        print("Job failed: \(error.localizedDescription)")
//                    }
//                })
////        cell.mesasgeImg.layer.borderWidth=1.0
//        cell.mesasgeImg.layer.masksToBounds = false
////        cell.mesasgeImg.layer.borderColor = UIColor.whiteColor().CGColor
//        cell.mesasgeImg.layer.cornerRadius = cell.mesasgeImg.frame.size.height/2
//        cell.mesasgeImg.clipsToBounds = true
        
        return cell
        
    }
    

    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        messagesTableView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
//            self?.page = 1
//            self?.appliedJobsPresenter.getAppliedJobs(page: (self?.page)!)
            self?.messagesPresenter.doGetAllMessages()
            }, loadingView: loadingView)
        self.messagesTableView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)

        self.messagesTableView?.dg_setPullToRefreshBackgroundColor(messagesTableView.backgroundColor!)
    }
//
//    func initLoadMoreToRefresh()
//    {
//        self.appliedJobsTabelView?.infiniteScrollIndicatorStyle = .gray
//
//        // Add infinite scroll handler
//        self.appliedJobsTabelView?.setShouldShowInfiniteScrollHandler { _ -> Bool in
//            return self.page != self.appliedJobsResponse?.last_page
//        }
//
//        self.appliedJobsTabelView?.addInfiniteScroll { (tableView) -> Void in
//
//            if self.page == self.appliedJobsResponse?.last_page {
//                //ending paging
//                self.appliedJobsTabelView.finishInfiniteScroll()
//            }else {
//                self.page += 1
//                self.appliedJobsPresenter.getAppliedJobs(page: self.page)
//
//            }
//        }
//
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MessagesViewController :messagesView {
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
    }
    
    func onSucessGetAllMessages(data: allMessagesApiRes) {
        print("onSucessGetAllMessages : \(data)")
        if let messages = data.data {
            
            MessagesDate = messages
            
            if MessagesDate.count == 0 {
                DispatchQueue.main.async {
                    self.messagesTableView.alpha = 0
                    self.emptyView.alpha = 1
                }
            }else{
                DispatchQueue.main.async {
                    self.messagesTableView.alpha = 1
                    self.emptyView.alpha = 0
                }
            }
            
        }
    }
    
    func onSucessMessageDetails(msg: String) {
        print("onSucessMessageDetails")
    }
    
    func onSucessAddNewMessageOrReplay(msg: String) {
        print("onSucessAddNewMessageOrReplay")
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
   
   
    
//    func onSucessLoadingNewCeritficateList(degreeWorkingTimes: NewCertificateApiRes) {
//        print("onSucessLoadingNewCeritficateList : \(degreeWorkingTimes)")
//        if let preferedContact = degreeWorkingTimes.PreferedContact {
//            if preferedContact.count > 0 {
//
//                if let contactId = UserManager.sharedInstance.getUser()?.PreferedContactID {
//
//
//                    for item in preferedContact {
//                        if contactId == item.id {
//                            self.preferredContactOption.text = "Preferred contact option".localized() + " : " + ((L102Language.isRTL ? item.nameAr : item.nameEn)!)
//                        }
//                    }
//                }
//
//    }}
//
//    }
    
   
   
    
    
}
