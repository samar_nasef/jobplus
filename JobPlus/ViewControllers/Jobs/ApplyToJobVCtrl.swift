//
//  ApplyToJobVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/13/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TransitionButton

class ApplyToJobVCtrl: UIViewController {
    
    var from:String = ""
    var spinnerView: UIView?
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var previousBtnOutlet: UIButton!
    @IBOutlet weak var nextBtnOutlet: TransitionButton!
    @IBOutlet weak var noPagesLbl: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var quesViewContainer: UIView!
    //@IBOutlet weak var notificationCountLbl: UILabel!
    //let questionListArr = ["Question 1", "Question 2", "Question 3", "Question 4", "Question 5"]
    //var questionIndex = 0
    var qLbls: [UILabel] = []
    var qTextViews: [UITextView] = []
    var questionsViews: [UIView] = []
    let questionsListPresenter = AnswerQuestionsPresenter()
    var questionListArr = [Question]()
    var questionIndex = 0
    var questionAnswersArr = [QuestionsAnswer]()
    var questionAnswerRequestObject = QuestionListRequest()
    var currentAnswer = QuestionsAnswer()
    var currentQuestion = Question()
    var job:jobDetail?
    var applyToJobPresenter = ApplyToJobPresenter()
    var titleMsg:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        self.applyToJobPresenter.attachView(view: self)
        // Do any additional setup after loading the view.
        self.questionsListPresenter.attachView(view: self)
        if let id = job?.id {
        self.questionsListPresenter.getQuestionsList(jobId: id)
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.dismissViewAfterRes),
                                               name: NSNotification.Name(rawValue: "dismissViewAfterRes"),
                                               object: nil)
        // Do any additional setup after loading the view.
        self.previousBtnOutlet.isHidden = true
        self.nextBtnOutlet.isHidden = true
        self.previousBtnOutlet.backgroundColor = UIColor(hexString: "#6adaf9")
        
    }
    @objc func dismissViewAfterRes(){
       self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "EvaluateApp"), object: nil)
    }
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func displayData(){
        noPagesLbl.text = String(questionIndex + 1) + "/" + String(questionListArr.count)
        pageControl.numberOfPages = questionListArr.count
        pageControl.currentPage = 0
        DispatchQueue.main.async {
            self.pageControl.isHidden = false
        }
        if questionIndex == questionListArr.count - 1 {
            self.nextBtnOutlet.setTitle("Apply Now".localized(), for: .normal)
            self.nextBtnOutlet.setTitle("Apply Now".localized(), for: .selected)
        }
        if questionListArr.count == 1 {
            self.previousBtnOutlet.isHidden = true
            self.nextBtnOutlet.isHidden = false
        }
        else if questionListArr.count > 1 {
            self.previousBtnOutlet.isHidden = false
            self.nextBtnOutlet.isHidden = false
        }
        for i in 0..<questionListArr.count{
            let qLbl = UILabel()
            qLbl.text = questionListArr[i].name
            qLbl.lineBreakMode = .byWordWrapping
            qLbl.numberOfLines = 0
            qLbl.layoutIfNeeded()
            qLbl.textAlignment = .center
            qLbl.backgroundColor = .white
            qLbl.font = UIFont(name: "HelveticaNeueLTArabic-Bold", size: 14.2)
            qLbl.textColor = UIColor(hexString: "#353235")
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            toolBar.sizeToFit()
            
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Done".localized(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.hideKeyboard))
            cancelButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!], for: .normal)
            cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black ], for: .normal)
            toolBar.setItems([ spaceButton, cancelButton ], animated: false)
            toolBar.isUserInteractionEnabled = true
            
            let qTextView = UITextView()
            qTextView.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12)
            qTextView.placeholderColor = UIColor(hexString: "#8c8c8c")
            qTextView.placeholder = "Answer here".localized()
            qTextView.backgroundColor = UIColor(hexString: "#f9f9fb")
            qTextView.layer.cornerRadius = 6.7
            qTextView.inputAccessoryView = toolBar
            self.qLbls.append(qLbl)
            self.qTextViews.append(qTextView)
        }
        if questionListArr.count > 0 {
        addQuestionView(QuesIndex: 0)
        }
    }
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        
//        let notiCount = UserManager.sharedInstance.notificationCount
//        if notiCount > 0 {
//            self.notificationCountLbl.text = (" \(String(notiCount)) ")
//            self.notificationCountLbl.isHidden = false
//        }
//        else{
//             self.notificationCountLbl.isHidden = true
//        }
    }
    
//    @IBAction func notificationOpenAction(_ sender: Any) {
//        if UserManager.sharedInstance.getUser() != nil {
//            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
//
//                if let navigator = navigationController {
//                    navigator.pushViewController(viewController, animated: true)
//                }
//            }
//        }
//        else{
//            self.showErrorMessage(message: "you have to login first".localized())
//        }
//    }
    
    func removeQuestionView(QuesIndex: Int){
        self.questionsViews[QuesIndex].removeFromSuperview()
    }
    func addQuestionView(QuesIndex: Int) {
        let vWidth: CGFloat = quesViewContainer.frame.size.width
        let viewWidth: Float = Float(vWidth) - Float(10 * ((questionListArr.count - 1 ) - QuesIndex))
        let vHeight: CGFloat = quesViewContainer.frame.size.height
        let viewHeight: Float = Float(vHeight) - Float(10 * QuesIndex)
        let centerOrgin = CGPoint(x: 0.0,
                                  y:0.0)
        
        let qView = UIView(frame: CGRect(origin: centerOrgin, size: CGSize(width: CGFloat(viewWidth), height: CGFloat(viewHeight))))
        
        qView.layer.cornerRadius = 7
        qView.backgroundColor = .white
        qView.layer.shadowColor = UIColor.black.cgColor
        qView.layer.shadowOpacity = 0.2
        qView.layer.shadowOffset = .zero
        qView.layer.shadowRadius = 5
        
        
        qView.addSubview(qLbls[QuesIndex])
        qView.addSubview(qTextViews[QuesIndex])
        
        self.questionsViews.append(qView)
        self.quesViewContainer.addSubview(qView)
        
        qView.translatesAutoresizingMaskIntoConstraints = false
        qLbls[QuesIndex].translatesAutoresizingMaskIntoConstraints = false
        qTextViews[QuesIndex].translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            qView.widthAnchor.constraint(equalToConstant: CGFloat(viewWidth)),
            qView.heightAnchor.constraint(equalToConstant: CGFloat(viewHeight)),
            qView.centerXAnchor.constraint(equalTo: self.quesViewContainer.centerXAnchor),
            qLbls[QuesIndex].topAnchor.constraint(equalTo: qView.topAnchor, constant: 20),
            qLbls[QuesIndex].trailingAnchor.constraint(equalTo: qView.trailingAnchor, constant: -20),
            qLbls[QuesIndex].leadingAnchor.constraint(equalTo: qView.leadingAnchor, constant: 20),
            //qLbls[QuesIndex].heightAnchor.constraint(equalToConstant: 40),
            qTextViews[QuesIndex].topAnchor.constraint(equalTo: qLbls[QuesIndex].bottomAnchor, constant: 20),
            qTextViews[QuesIndex].trailingAnchor.constraint(equalTo: qView.trailingAnchor, constant: -20),
            qTextViews[QuesIndex].leadingAnchor.constraint(equalTo: qView.leadingAnchor, constant: 20),
            qTextViews[QuesIndex].bottomAnchor.constraint(equalTo: qView.bottomAnchor, constant: -20)
            
        ])
    }
    
    @IBAction func PreviousAction(_ sender: Any) {
        if questionsViews.count >= 0 &&  questionIndex > 0 {
            self.previousBtnOutlet.backgroundColor = UIColor(hexString: "#00AFED")
            
            removeQuestionView(QuesIndex: questionIndex)
            questionsViews.remove(at: questionIndex)
            questionIndex -= 1
            noPagesLbl.text = String(questionIndex + 1)  + "/" + String(questionListArr.count)
            pageControl.currentPage = questionIndex
            
            self.nextBtnOutlet.setTitle("Next".localized(), for: .normal)
            self.nextBtnOutlet.setTitle("Next".localized(), for: .selected)
        }
        if questionIndex == 0 {
            self.previousBtnOutlet.backgroundColor = UIColor(hexString: "#6adaf9")
        }
        //
    }
    @IBAction func NextAction(_ sender: Any) {
        if qTextViews[questionIndex].text.isBlank {
            self.showErrorMessage(message: "you have to answer the question before go to next".localized())
            return
        }
        if questionIndex < questionListArr.count - 1 {
            questionIndex += 1
            self.previousBtnOutlet.backgroundColor = UIColor(hexString: "#00AFED")
            addQuestionView(QuesIndex: questionIndex)
            noPagesLbl.text = String(questionIndex + 1) + "/" + String(questionListArr.count)
            pageControl.currentPage = questionIndex
            if questionIndex == questionListArr.count - 1 {
                self.nextBtnOutlet.setTitle("Apply Now".localized(), for: .normal)
                self.nextBtnOutlet.setTitle("Apply Now".localized(), for: .selected)
            }
        }
        else if questionIndex + 1 == self.questionListArr.count {
            
            for itemIndex in 0..<questionListArr.count {
                let questionAnswerObj = QuestionsAnswer()
                questionAnswerObj.question = questionListArr[itemIndex].name
                questionAnswerObj.id = questionListArr[itemIndex].id
                questionAnswerObj.answer = self.qTextViews[itemIndex].text
                questionAnswerObj.name = questionListArr[itemIndex].name
                 self.questionAnswersArr.append(questionAnswerObj)
            }
            self.questionAnswerRequestObject.questionsAnswers = self.questionAnswersArr
            DispatchQueue.main.async {
                self.previousBtnOutlet.isHidden = true
            }
            if let userValidity = self.job?.userJobValidatiy, userValidity < 50.0 {
                from = "userNotSuitablleToApply"
                self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
            }
            else{
                self.applyToJobPresenter.applyToJob(questionAnswersObj: self.questionAnswerRequestObject, jobId: (self.job?.id)!)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CongratulationsViewController" {
            if let destinationVC = segue.destination as? CongratulationsViewController {
                if from == "userNotSuitablleToApply" {
                    destinationVC.applyToJobDelegate = self
                }
                destinationVC.from = self.from
                destinationVC.titleMsg = self.titleMsg
            }
        }
    }
        
}
extension ApplyToJobVCtrl: ApplyToJobDelegate{
    func OpenEvaluateApp() {
    }
    
    func ApplyToJob() {
        self.applyToJobPresenter.applyToJob(questionAnswersObj: self.questionAnswerRequestObject, jobId: (self.job?.id)!)
    }
}
extension ApplyToJobVCtrl:AnswerQuestionsView{
    func showLoadingIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess(questionsListResponse: QuestionListResponse) {
        if let questionArr = questionsListResponse.data {
            self.questionListArr = questionArr
            print("questionListArr = \(questionListArr)")
            self.displayData()
        }
    }
    
    func onError(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}

extension ApplyToJobVCtrl:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //self.noOfCharacters.text = "\(240 - textView.text.count)"
        
     return textView.text.count - range.length + text.count <= 240
    }
}

extension ApplyToJobVCtrl:ApplyToJobView{
    func showLoadingApplyIndicator() {
        self.view.isUserInteractionEnabled = false
        Loader.showProgress()
        self.nextBtnOutlet.startAnimation()
    }
    
    func hideLoadingApplyIndicator() {
        self.view.isUserInteractionEnabled = true
        self.nextBtnOutlet.stopAnimation()
        Loader.hidePrgoress()
    }
    
//    func onSucessApply(status: String) {
    func onSucessApply(status:String,message:String){
        if status == "1"{
            UserManager.sharedInstance.jobAppliedOrCanceled = 1
            from = "applyToJob"
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
            
        }else if status == "-19"{
            from = "status-19"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }else if status == "20"{
            from = "profileNotFilled"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        else if status == "30"{
            from = "status-30"
            titleMsg = message
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
        else{
            from = "status-1"
            self.performSegue(withIdentifier: "CongratulationsViewController", sender: nil)
        }
    }
    
    func onErrorApply(message: String) {
        if message == "401" {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                    viewController.modalPresentationStyle = .overCurrentContext
                    viewController.from = "401"
                    self.present(viewController, animated: true, completion: nil)
            }
        }
        else{
            self.showErrorMessage(message: message)
        }

        DispatchQueue.main.async {
            self.previousBtnOutlet.isHidden = false
            self.nextBtnOutlet.isHidden = false
        }
    }
}
