//
//  AppliedJobsViewController.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import Kingfisher

class AppliedJobsViewController: UIViewController {
    
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var appliedJobsTabelView: UITableView!
    @IBOutlet weak var notificationCountLbl: UILabel!
    var itemCountForWindless = 5
    var page = 1
    let appliedJobsPresenter = AppliedJobsPresenter()
    var appliedJobsArr = [JobItem]()
    var appliedJobsResponse : JobListResponse?
    
    override func viewDidAppear(_ animated: Bool) {
        let notiCount = UserManager.sharedInstance.notificationCount
        if notiCount > 0 {
            self.notificationCountLbl.text = (" \(String(notiCount)) ")
            self.notificationCountLbl.isHidden = false
        }
        else{
            self.notificationCountLbl.isHidden = true
        }
        if UserManager.sharedInstance.jobAppliedOrCanceled > 0 {
            if let indexPath = UserManager.sharedInstance.jobIndexAppliedOrCanceled {
                if UserManager.sharedInstance.jobAppliedOrCanceled == 2 {
                    appliedJobsArr.remove(at: indexPath.row)
                    appliedJobsTabelView.deleteRows(at: [indexPath], with: .automatic)
                    if appliedJobsArr.count == 0 {
                        DispatchQueue.main.async {
                            self.appliedJobsTabelView.alpha = 0
                            self.emptyView.alpha = 1
                        }
                    }
                }
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
            else{
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
        }
    }
    @IBAction func notificationOpenAction(_ sender: UIButton) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        self.emptyView.msgLbl.text = "You did not apply for any jobs yet.".localized()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        
        // Do any additional setup after loading the view.
        appliedJobsPresenter.attachView(view: self)
        self.appliedJobsTabelView.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        initPullToRefresh()
        initLoadMoreToRefresh()
        if UserManager.sharedInstance.getUser() == nil {
            self.appliedJobsTabelView.alpha = 0
            self.emptyView.alpha = 1
        }else {
            self.appliedJobsTabelView.alpha = 1
            self.emptyView.alpha = 0
            appliedJobsPresenter.getAppliedJobs(page: self.page)
        }
        self.view.changeFont()
        appliedJobsTabelView.backgroundColor = .clear
    }
    
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appliedJobsTabelView?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.page = 1
            self?.appliedJobsPresenter.getAppliedJobs(page: (self?.page)!)
        }, loadingView: loadingView)
        self.appliedJobsTabelView?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.appliedJobsTabelView?.dg_setPullToRefreshBackgroundColor(appliedJobsTabelView.backgroundColor!)
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initLoadMoreToRefresh()
    {
        self.appliedJobsTabelView?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.appliedJobsTabelView?.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return self.page != self.appliedJobsResponse?.last_page
        }
        
        self.appliedJobsTabelView?.addInfiniteScroll { (tableView) -> Void in
            
            if self.page == self.appliedJobsResponse?.last_page {
                //ending paging
                self.appliedJobsTabelView.finishInfiniteScroll()
            }else {
                self.page += 1
                self.appliedJobsPresenter.getAppliedJobs(page: self.page)
                
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Applied Job".localized()
        self.navigationController?.setupNavigationControllerOfPartTime()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
}

extension AppliedJobsViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appliedJobsResponse == nil {
            return itemCountForWindless
        }else {
            return appliedJobsArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appliedJobsTabelView.dequeueReusableCell(withIdentifier: "HomeJobsCell", for: indexPath) as! HomeJobsTVCell
        
        if appliedJobsResponse == nil {
            // self.HomeJobsTV.alpha = 0
        }
        else{
            cell.whiteContainerView.layer.cornerRadius = 10
            cell.ApplyJobBtn.layer.cornerRadius = 12.3
            cell.ApplyJobBtn.isHidden = true
            
            if (appliedJobsArr.count) > 0 {
                
                let item = appliedJobsArr[indexPath.row]
                if let disabilityEnabled = item.DisabilityEnabled {
                    if disabilityEnabled == 1 {
                        cell.disability_ic.isHidden = false
                    }
                    else{
                        cell.disability_ic.isHidden = true
                    }
                }
                else{
                    cell.disability_ic.isHidden = true
                }
                cell.jobTitle.text = item.title ?? ""
                cell.JobImg.kf.indicatorType = .activity
                let url = URL(string: (item.imagePath ?? "")!)
                
                let processor = DownsamplingImageProcessor(size: cell.JobImg.size)
                |> RoundCornerImageProcessor(cornerRadius: 22)
                
                cell.JobImg.kf.setImage(
                    with: url,
                    placeholder: UIImage(named: "company_ic_large"),
                    options: [
                        .processor(processor),
                        .scaleFactor(UIScreen.main.scale),
                        .transition(.fade(1)),
                        .cacheOriginalImage
                    ], completionHandler:
                        {
                            result in
                            switch result {
                            case .success(let value):
                                print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            case .failure(let error):
                                print("Job failed: \(error.localizedDescription)")
                            }
                        })
                cell.AppliedCompany.text = item.companyName
                cell.JobCompany.text = item.city
                
                cell.JobLocation.text = "\(item.job_type_name ?? "") - \(item.workTimeText ?? "")"
                if let text_color = item.job_type_color {
                    cell.JobLocation.textColor = UIColor(hexString: text_color)
                }
                if let acceptence_status = item.job_status {
                    if acceptence_status == 1 {
                        cell.JobPostedTime.text = "Job applied".localized()
                        cell.JobPostedTime.textColor = UIColor(hexString: "f58238")
                        cell.jobStatusIcon.image = UIImage(named: "pending")
                        if let jobExpired = item.jobExpired {
                            if jobExpired {
                                cell.JobPostedTime.text = "Expired".localized()
                                cell.JobPostedTime.textColor = UIColor(hexString: "#f58238")
                            }
                        }
                    }
                    else if acceptence_status == 2 {
                        cell.JobPostedTime.text = "Accepted".localized()
                        cell.JobPostedTime.textColor = UIColor(hexString: "#2c9a42")
                        cell.jobStatusIcon.image = UIImage(named: "accepted")
                    }
                    else if acceptence_status == 3 || acceptence_status == 6 {
                        cell.JobPostedTime.text = "Rejected".localized()
                        cell.JobPostedTime.textColor = UIColor(hexString: "#db0000")
                        cell.jobStatusIcon.image = UIImage(named: "refused")
                    }
                    else if acceptence_status == 4 {
                        cell.JobPostedTime.text = "Application reviewed".localized()
                        cell.JobPostedTime.textColor = UIColor(red: 158/255, green: 117/255, blue: 178/255, alpha: 1)
                        cell.jobStatusIcon.image = UIImage(named: "pending")
                    }
                    else if acceptence_status == 5 {
                        cell.JobPostedTime.text = "Accepted for interview".localized()
                        cell.JobPostedTime.textColor = UIColor(red: 245/255, green: 130/255, blue: 56/255, alpha: 1)
                        cell.jobStatusIcon.image = UIImage(named: "pending")
                    }
                    else{
                        if let jobExpired = item.jobExpired {
                            if jobExpired {
                                cell.JobPostedTime.text = "Expired".localized()
                                cell.JobPostedTime.textColor = UIColor(hexString: "#f58238")
                            }
                        }
                    }
                }
                if let creationDate = item.created_at {
                    cell.JobType.text = creationDate.getFormattedDate()
                    //cell.JobPostedTime.text = "The job has expired".localized()
                    //                     cell.JobPostedTime.textColor = UIColor(hexString: "#f58238")
                    //                    cell.jobStatusIcon.image = UIImage(named: "pending")
                }
                else{
                    //cell.JobPostedTime.text = "The job has expired".localized()
                    //                     cell.JobPostedTime.textColor = UIColor(hexString: "#f58238")
                    //                    cell.jobStatusIcon.image = UIImage(named: "pending")
                }
                
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
        if appliedJobsArr.count > 0 {
            let item = appliedJobsArr[indexPath.row]
            if item.jobExpired! {
                cell.alpha = 0.5
                
            }
            else{
                cell.alpha = 1
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let authorizationStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let jobDetailsVC = authorizationStoryboard.instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
        
        
        var selectedItem:jobItem2ForAds?
        let i = appliedJobsArr[indexPath.row]
        selectedItem = jobItem2ForAds(id:  i.id, title: i.title ?? "", companyName: i.companyName, imagePath: i.imagePath ?? "", city: i.city, job_type_id: i.job_type_id ?? 0, job_type_name: i.job_type_name ?? "", job_type_color: i.job_type_color ?? "", created_at: i.created_at ?? "", appliedForJob: i.appliedForJob ?? false , expireDate: i.expireDate ?? "", job_status: i.job_status ?? 0 , jobExpired: i.jobExpired ?? false, workTimeText: i.workTimeText ?? "", DisabilityEnabled: i.DisabilityEnabled ?? 0)
        jobDetailsVC.jobItem = selectedItem
        jobDetailsVC.fromApplied = true
        UserManager.sharedInstance.jobIndexAppliedOrCanceled = indexPath
        self.navigationController?.pushViewController(jobDetailsVC, animated: true)
    }
}
extension AppliedJobsViewController:AppliedJobsView{
    func showLoadingIndicator() {
        self.appliedJobsTabelView.windless.start()
        self.appliedJobsTabelView.alpha = 1
    }
    
    func hideLoadingIndicator() {
        self.appliedJobsTabelView.finishInfiniteScroll()
        self.appliedJobsTabelView.dg_stopLoading()
        self.appliedJobsTabelView.windless.end()
        itemCountForWindless = 0
        self.appliedJobsTabelView.reloadData()
        self.appliedJobsTabelView.alpha = 1
    }
    
    func onSucess(appliedJobsListResponse: JobListResponse) {
        itemCountForWindless = 0
        if page == 1 {
            self.appliedJobsArr.removeAll()
        }
        if let userDeleted = appliedJobsListResponse.userDeleted {
            if userDeleted == 1 {
                self.showErrorMessage(message: "Sorry, your account has been suspended, you can contact our support team".localized())
                clearDataAndStart()
            }
        }
        self.appliedJobsResponse = appliedJobsListResponse
        if var appliedJobs = appliedJobsListResponse.data{
            if appliedJobs.count == 0 , page == 1{
                
                self.appliedJobsTabelView.alpha = 0
                
                self.emptyView.alpha = 1
                
            }else {
                
                for index in 0..<appliedJobs.count {
                    
                    if let expired_date = appliedJobs[index].expireDate {
                        let staus = expired_date.checkExiration()
                        appliedJobs[index].jobExpired = staus
                    }
                    else{
                        appliedJobs[index].jobExpired = false
                    }
                }
                self.appliedJobsArr.append(contentsOf: appliedJobs)
                self.appliedJobsTabelView.alpha = 1
            }
        }
        
        self.appliedJobsTabelView?.reloadData()
        
    }
    
    func onSuccessNextLinkCall(appliedJobsResponse: JobListResponse) {
        
    }
    
    func onError(message: String) {
        itemCountForWindless = 0
        self.appliedJobsTabelView.reloadData()
        self.appliedJobsTabelView.alpha = 1
        self.showErrorMessage(message: message)
    }
    
    
}
