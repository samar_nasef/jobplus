//
//  invoiceDetailsVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 19/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import UIKit
import WebKit

class invoiceDetailsVCtrl: UIViewController {
    
    var invoice: InvoiceItem?
    var webView: WKWebView!
    @IBOutlet weak var officialNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var taIDLbl: UILabel!
    @IBOutlet weak var invoiceNoLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var invoiceDateLbl: UILabel!
    @IBOutlet weak var subTypeLbl: UILabel!
    @IBOutlet weak var subDurationLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var vatLb: UILabel!
    @IBOutlet weak var totalVatCollected: UILabel!
    @IBOutlet weak var qrcodeImgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        officialNameLbl.text = "\(invoice?.JobPlusOfficialNameEn ?? "")\n\(invoice?.JobPlusOfficialNameAr ?? "")"
        addressLbl.text = L102Language.isRTL ? invoice?.JobPlusAddressAr ?? "" : invoice?.JobPlusAddressEn ?? ""
        taIDLbl.text = invoice?.TaxIdNumber ?? ""
        invoiceNoLbl.text = "\(invoice?.InvoiceNumber ?? 0)"
        usernameLbl.text = invoice?.Username ?? ""
        emailLbl.text = invoice?.Email
        invoiceDateLbl.text = (invoice?.created_at ?? "").getFormattedDate()
        subTypeLbl.text = invoice?.PackageName ?? ""
        subDurationLbl.text = invoice?.end_subscribtation_date
        subTotalLbl.text = "\(invoice?.TotalPrice ?? "") \("SAR".localized())"
        vatLb.text = (invoice?.Vat ?? "") + "%"
        totalVatCollected.text = "\(invoice?.VatValue ?? 0.0) \("SAR".localized())"
        
        let image = generateQRCode(from: String((invoice?.qrCodeData ?? "")))
        qrcodeImgView.image = image
        webView = WKWebView(frame: self.view.bounds, configuration: WKWebViewConfiguration())
        //webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //cvView.addSubview(webView)
        if let urlTxt = invoice?.pdfFilePath{
            if let url = URL(string: urlTxt.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                webView.load(URLRequest(url: url))
            }
        }
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.utf8)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 4, y: 4)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    @IBAction func dismissViewAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func downloadInvoice(_ sender: Any) {
        let pdfData = self.exportAsPdfFromWebView()
        let activityViewController = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    func exportAsPdfFromWebView() -> NSMutableData {
       let pdfData = createPdfFile(printFormatter: self.webView.viewPrintFormatter())
       return pdfData
    }
   
   func createPdfFile(printFormatter: UIViewPrintFormatter) -> NSMutableData {

       let originalBounds = self.webView.bounds
       self.webView.bounds = CGRect(x: originalBounds.origin.x, y: webView.bounds.origin.y, width: self.webView.bounds.size.width, height: self.webView.scrollView.contentSize.height)
       let pdfPageFrame = CGRect(x: 0, y: 0, width: self.webView.bounds.size.width, height: self.webView.scrollView.contentSize.height)
       let printPageRenderer = UIPrintPageRenderer()
       printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
       printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 595, height: 842)), forKey: "paperRect")
       printPageRenderer.setValue(NSValue(cgRect: CGRect(x: 0, y: 0, width: 595, height: 842)), forKey: "printableRect")
       self.webView.bounds = originalBounds
       return printPageRenderer.generatePdfData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
