//
//  invoiceesListVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 19/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import UIKit

class invoiceesListVCtrl: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var InvoicesTV: UITableView!
    @IBOutlet weak var notiBtn: UIButton!
    @IBOutlet weak var notificationsCountLbl: UILabel!
    let notificationCountPresenter = NotificationPresenter()
    let invoicesPresenter = InvoicesPresenter()
    var spinnerView: UIView?
    var invoices: [InvoiceItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            
        }
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        initPullToRefresh()
        notificationCountPresenter.attachView(view: self)
        invoicesPresenter.attachView(view: self)
        loadInvoices()
    }
    func loadInvoices(){
        invoicesPresenter.getInvoicesList()
    }
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let notiCount = UserManager.sharedInstance.notificationCount
        if notiCount > 0 {
            self.notificationsCountLbl.text = (" \(notiCount) ")
            self.notificationsCountLbl.isHidden = false
        }
        else{
             self.notificationsCountLbl.isHidden = true
        }
    }
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        InvoicesTV?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.loadInvoices()
        }, loadingView: loadingView)
        self.InvoicesTV?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        self.InvoicesTV?.dg_setPullToRefreshBackgroundColor(InvoicesTV.backgroundColor!)
    }
    @IBAction func openNotification(_ sender: UIButton) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = InvoicesTV.dequeueReusableCell(withIdentifier: "invoicesCell", for: indexPath) as! invoicesTVCell
        cell.whiteContainerView.layer.cornerRadius = 10
        cell.subscriptionTpeLbl.text = invoices[indexPath.row].PackageName ?? ""
        cell.invoiceNoLbl.text = "\(invoices[indexPath.row].InvoiceNumber ?? 0)"
        if let sub_date = invoices[indexPath.row].created_at {
            cell.subDateLbl.text = sub_date.getFormattedDate()
        }
        if let end_date = invoices[indexPath.row].end_subscribtation_date {
            cell.expiryDateLbl.text = end_date.getFormattedDate()
            if end_date.checkExirationWithTime() {
                cell.SubStatusBtn.setTitle("Finished".localized(), for: .normal)
                cell.SubStatusBtn.setTitle("Finished".localized(), for: .selected)
                cell.SubStatusBtn.setTitleColor(.gray, for: .normal)
                cell.SubStatusBtn.setTitleColor(.gray, for: .selected)
                cell.SubStatusBtn.borderColor = .gray
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "invoiceDetailsViewCtrl") as? invoiceDetailsVCtrl {
                viewController.modalPresentationStyle = .overCurrentContext
            viewController.invoice = self.invoices[indexPath.row]
            self.present(viewController, animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension invoiceesListVCtrl : InvoicesView {
    func onSucess(Response:[InvoiceItem]) {
        self.InvoicesTV.dg_stopLoading()
        if Response.count > 0 {
            emptyView.alpha = 0
            self.invoices = Response
            self.InvoicesTV.reloadData()
        }
        else{
            emptyView.alpha = 1
        }
    }
    
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onError(message: String) {
        self.InvoicesTV.dg_stopLoading()
        self.showErrorMessage(message:message)
    }
}
extension invoiceesListVCtrl: NotificationView{
    
    
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationsCountLbl.text = (" \(String(notificationCount)) ")
            self.notificationsCountLbl.isHidden = false
        }
        else{
            self.notificationsCountLbl.isHidden = true
        }
    }
}
