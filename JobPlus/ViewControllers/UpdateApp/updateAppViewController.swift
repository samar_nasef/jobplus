//
//  updateAppViewController.swift
//  farawla
//
//  Created by Elsaid Yousif on 7/1/18.
//  Copyright © 2018 Elsaid Yousif. All rights reserved.
//

import UIKit

class updateAppViewController: UIViewController {

    var showThanks = true;
    @IBOutlet weak var updateMsgTxt: UITextView!
    @IBOutlet weak var msgText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.changeFont()
        if showThanks == true {
            thanksBtn.isHidden = false
            msgText.text = NSLocalizedString("newVersionStore", comment: "")
        }
        else{
            thanksBtn.isHidden = true
            msgText.text = NSLocalizedString("notSupportedVersion", comment: "")
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateBtn(_ sender: Any) {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1493332366"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBOutlet weak var thanksBtn: UIButton!
    @IBAction func thanksBtn(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
