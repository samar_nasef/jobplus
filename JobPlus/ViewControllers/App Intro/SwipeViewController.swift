//
//  SwipeViewController.swift
//  ViewPager
//
//  Created by MACBOOK PRO RETINA on 30/11/2018.
//  Copyright © 2018 MACBOOK PRO RETINA. All rights reserved.
//

import UIKit

class SwipeViewController : UICollectionViewController,UICollectionViewDelegateFlowLayout {
    
    let pages = [
        introPage(imageName: "slide1", headerText: "Welcome to JobPlus,\n an Extra Job, Extra income".localized()),
        introPage(imageName: "slide2", headerText: "All you have to do is fill in \n and arrange your profile".localized()),
        introPage(imageName: "slide3", headerText: "Attach the CV file \n if you want".localized()),
        introPage(imageName: "slide4", headerText: "Searching for jobs and finding \n the right job for you".localized())
    ]
    
    let previousButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Skip".localized(), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 21.0)!
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(PreviousButtonTapped), for: .touchUpInside)
        return button
    }()
    
    let nextButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Next".localized(), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 21.0)!
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(NextButtonTapped), for: .touchUpInside)
        return button
    }()
    let startButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Start".localized(), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 21.0)!
        button.setTitleColor(UIColor.white, for: .normal)
        button.isHidden = true
        button.layer.cornerRadius = 26
        button.backgroundColor = UIColor(red: 0, green: 175/255, blue: 223/255, alpha: 1)
        button.addTarget(self, action: #selector(NextButtonTapped), for: .touchUpInside)
        return button
    }()
    let emptyLbl : UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    let pagecontrol : UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 4
        pc.isUserInteractionEnabled = false
        pc.currentPageIndicatorTintColor = UIColor(red: 174/255, green: 140/255, blue: 192/255, alpha: 1)
        pc.pageIndicatorTintColor = UIColor(red: 0/255, green: 175/255, blue: 223/255, alpha: 1)
        return pc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        settingBottomControl()
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.register(PageCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.isPagingEnabled = true
    }
    
    func settingBottomControl() {
        
        let BottomStackView = UIStackView(arrangedSubviews: [previousButton,emptyLbl,nextButton])
        BottomStackView.translatesAutoresizingMaskIntoConstraints = false
        BottomStackView.distribution = .fillEqually
        view.addSubview(BottomStackView)
        NSLayoutConstraint.activate([
            BottomStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            BottomStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            BottomStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            BottomStackView.heightAnchor.constraint(equalToConstant: 50)
            ])
        
        let pagerView = UIStackView(arrangedSubviews: [pagecontrol])
        pagerView.translatesAutoresizingMaskIntoConstraints = false
        BottomStackView.distribution = .fillEqually
        view.addSubview(pagerView)
        NSLayoutConstraint.activate([
            pagerView.bottomAnchor.constraint(equalTo: BottomStackView.topAnchor, constant: -90),
            pagerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pagerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pagerView.heightAnchor.constraint(equalToConstant: 50)
            ])
        view.addSubview(startButton)
        NSLayoutConstraint.activate([
            startButton.bottomAnchor.constraint(equalTo: BottomStackView.topAnchor, constant: -30),
            startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            startButton.widthAnchor.constraint(equalToConstant: 250),
            startButton.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
    
    @objc func NextButtonTapped() {
        if pagecontrol.currentPage == 3 {
            UserDefaults.standard.set(1, forKey: "introDone")
            if #available(iOS 13.0, *) {
                let scene = UIApplication.shared.connectedScenes.first
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    let mainwindow = sd.window
                    let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                    mainwindow?.rootViewController = stry.instantiateInitialViewController()
                }
            }
            else{
                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
            }
        }
        else if pagecontrol.currentPage == 2 {
            DispatchQueue.main.async {
                self.previousButton.isHidden = true
                self.nextButton.isHidden = true
                self.startButton.isHidden = false
            }
            let nextIndexPath = min(pagecontrol.currentPage + 1,pages.count - 1)
            collectionView?.scrollToItem(at: IndexPath(item: nextIndexPath, section: 0) , at: .centeredHorizontally, animated: true)
            pagecontrol.currentPage = nextIndexPath
        }
        else{
            DispatchQueue.main.async {
                self.previousButton.isHidden = false
                self.nextButton.isHidden = false
                self.startButton.isHidden = true
            }
            let nextIndexPath = min(pagecontrol.currentPage + 1,pages.count - 1)
            collectionView?.scrollToItem(at: IndexPath(item: nextIndexPath, section: 0) , at: .centeredHorizontally, animated: true)
            pagecontrol.currentPage = nextIndexPath
        }
        
        
    }
    
    @objc func PreviousButtonTapped() {
//        let nextIndexPath = max(pagecontrol.currentPage - 1,0)
//        collectionView?.scrollToItem(at: IndexPath(item: nextIndexPath, section: 0) , at: .centeredHorizontally, animated: true)
//        pagecontrol.currentPage = nextIndexPath
        UserDefaults.standard.set(1, forKey: "introDone")
        if #available(iOS 13.0, *) {
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            let mainwindow = sd.window
             let stry = UIStoryboard(name: "Authorizations", bundle: nil)
         mainwindow?.rootViewController = stry.instantiateInitialViewController()
            }
        }
        else{
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Authorizations", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! PageCell
        cell.page = pages[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if L102Language.isRTL {
            pagecontrol.currentPage = 3 - Int(targetContentOffset.pointee.x / view.frame.width)
        }
        else{
            pagecontrol.currentPage = Int(targetContentOffset.pointee.x / view.frame.width)
        }
        if pagecontrol.currentPage == 3 {
            DispatchQueue.main.async {
                self.previousButton.isHidden = true
                self.nextButton.isHidden = true
                self.startButton.isHidden = false
            }
        }
        else{
            DispatchQueue.main.async {
                self.previousButton.isHidden = false
                self.nextButton.isHidden = false
                self.startButton.isHidden = true
            }
        }
    }
    
    
}
