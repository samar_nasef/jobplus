//
//  PageCell.swift
//  ViewPager
//
//  Created by MACBOOK PRO RETINA on 30/11/2018.
//  Copyright © 2018 MACBOOK PRO RETINA. All rights reserved.
//

import UIKit

class PageCell : UICollectionViewCell {
    
    var page: introPage?  {
        didSet {
            MyImgView.image = UIImage(named: (page?.imageName)!)
            let attributedtext = NSMutableAttributedString(string: (page?.headerText)!, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)])
            descriptionTextView.attributedText = attributedtext
            descriptionTextView.textAlignment = .center
        }
    }
    
   private let MyImgView : UIImageView = {
        let Img = UIImageView(image: #imageLiteral(resourceName: "jpLogo_splash"))
        Img.translatesAutoresizingMaskIntoConstraints = false
        Img.contentMode = .scaleAspectFit
        return Img
    }()
    
    private let descriptionTextView : UITextView = {
        let textview = UITextView()
        let attributedtext = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.font : UIFont(name: "HelveticaNeueLTArabic-Bold", size: 21.0)!])
        attributedtext.append(NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Bold", size: 21.0)!,NSAttributedString.Key.foregroundColor: UIColor.black]))
        textview.attributedText = attributedtext
        textview.textAlignment = .center
        textview.tag = 1
        textview.semanticContentAttribute = .unspecified
        textview.isEditable = false
        textview.isScrollEnabled = false
        textview.backgroundColor = .clear
        textview.translatesAutoresizingMaskIntoConstraints = false
        return textview
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        let topViewContainer = UIView()
        addSubview(topViewContainer)
        topViewContainer.translatesAutoresizingMaskIntoConstraints = false
        topViewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 30).isActive = true
        topViewContainer.leftAnchor.constraint(equalTo: leftAnchor,constant: 80).isActive = true
        topViewContainer.rightAnchor.constraint(equalTo: rightAnchor,constant: -80).isActive = true
        topViewContainer.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.4).isActive = true
        topViewContainer.addSubview(MyImgView)
        MyImgView.centerXAnchor.constraint(equalTo: topViewContainer.centerXAnchor).isActive = true
        MyImgView.centerYAnchor.constraint(equalTo: topViewContainer.centerYAnchor).isActive = true
        MyImgView.widthAnchor.constraint(equalTo: topViewContainer.widthAnchor).isActive = true
        MyImgView.heightAnchor.constraint(equalTo: topViewContainer.heightAnchor, multiplier: 1).isActive = true
        MyImgView.contentMode = .scaleAspectFit
        addSubview(descriptionTextView)
          descriptionTextView.topAnchor.constraint(equalTo: topViewContainer.bottomAnchor, constant: 0).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: leftAnchor,constant: 20).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: rightAnchor,constant: -20).isActive = true
        descriptionTextView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.25).isActive = true
        //descriptionTextView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -150).isActive = true
    }
    
}
