//
//  AppSettingsVCtrl.swift
//  Pods
//
//  Created by elsaid yousif on 02/01/2022.
//

import UIKit
import StoreKit

class AppSettingsVCtrl: UIViewController {
    var languageArray = ["English" , "عربي"]
    var logoutPresenter = LogoutPresenter()
    var deleteAccountPresenter = DeleteAccountPresenter()
    let registerPresenter = RegisterPresenter()
    var notificationPresenter = NotificationPresenter()
    let settingsPresenter = SettingsPagesPresenter()
    var spinnerView: UIView?
    @IBOutlet weak var notificationStatus: UISwitch!
    @IBOutlet weak var languageSTV: UIStackView!
    @IBOutlet weak var subscriptionsSTV: UIStackView!
    @IBOutlet weak var deleteAccountSTV: UIStackView!
    @IBOutlet weak var invoicesSTV: UIStackView!
    @IBOutlet weak var termsSTV: UIStackView!
    @IBOutlet weak var AboutSTV: UIStackView!
    @IBOutlet weak var faqSTV: UIStackView!
    @IBOutlet weak var rateSTV: UIStackView!
    @IBOutlet weak var contactSTV: UIStackView!
    @IBOutlet weak var favoritesSTV: UIStackView!
    @IBOutlet weak var langBtn: UIButton!
    @IBOutlet weak var notificationsSTV: UIStackView!
    @IBOutlet weak var under_notificationsLbl: UILabel!
    @IBOutlet weak var under_invoicesLbl: UILabel!
    @IBOutlet weak var under_favsLbl: UILabel!
    @IBOutlet weak var under_subscribtionsLbl: UILabel!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var signOutBtn: UIButton!
    @IBOutlet weak var notiBtn: UIButton!
    @IBOutlet weak var underFeedbackLbl: UILabel!
    @IBOutlet weak var feedbackSTV: UIStackView!
    @IBOutlet weak var notificationsCountLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        
        let langTap = UITapGestureRecognizer(target: self, action: #selector(languageTapped))
        languageSTV.isUserInteractionEnabled = true
        languageSTV.addGestureRecognizer(langTap)
        
        let SubscriptionsTap = UITapGestureRecognizer(target: self, action: #selector(opensubscribtions))
        subscriptionsSTV.isUserInteractionEnabled = true
        subscriptionsSTV.addGestureRecognizer(SubscriptionsTap)
        
        let deleteAccountTap = UITapGestureRecognizer(target: self, action: #selector(deleteAccount))
        deleteAccountSTV.isUserInteractionEnabled = true
        deleteAccountSTV.addGestureRecognizer(deleteAccountTap)
        
        let invoicesTap = UITapGestureRecognizer(target: self, action: #selector(openInvoices))
        invoicesSTV.isUserInteractionEnabled = true
        invoicesSTV.addGestureRecognizer(invoicesTap)
        
        let favsTap = UITapGestureRecognizer(target: self, action: #selector(openFavs))
        favoritesSTV.isUserInteractionEnabled = true
        favoritesSTV.addGestureRecognizer(favsTap)
        
        let termsTap = UITapGestureRecognizer(target: self, action: #selector(openTerms))
        termsSTV.isUserInteractionEnabled = true
        termsSTV.addGestureRecognizer(termsTap)
        
        let aboutTap = UITapGestureRecognizer(target: self, action: #selector(openAbout))
        AboutSTV.isUserInteractionEnabled = true
        AboutSTV.addGestureRecognizer(aboutTap)
        
        let contactTap = UITapGestureRecognizer(target: self, action: #selector(openContact))
        contactSTV.isUserInteractionEnabled = true
        contactSTV.addGestureRecognizer(contactTap)
        
        let rateTap = UITapGestureRecognizer(target: self, action: #selector(rateApp))
        rateSTV.isUserInteractionEnabled = true
        rateSTV.addGestureRecognizer(rateTap)
        
        let faqsTap = UITapGestureRecognizer(target: self, action: #selector(openFAQs))
        faqSTV.isUserInteractionEnabled = true
        faqSTV.addGestureRecognizer(faqsTap)
        
        let feedbackTap = UITapGestureRecognizer(target: self, action: #selector(openFeedback))
        feedbackSTV.isUserInteractionEnabled = true
        feedbackSTV.addGestureRecognizer(feedbackTap)
        
        if UserManager.sharedInstance.getUser() == nil {
            self.subscriptionsSTV.isHidden = true
            self.under_subscribtionsLbl.isHidden = true
            self.invoicesSTV.isHidden = true
            self.under_invoicesLbl.isHidden = true
            self.favoritesSTV.isHidden = true
            self.under_favsLbl.isHidden = true
            self.notificationsSTV.isHidden = true
            self.deleteAccountSTV.isHidden = true
            
            self.under_notificationsLbl.isHidden = true
            self.notiBtn.isHidden = true
            self.notificationsCountLbl.isHidden = true
            self.feedbackSTV.isHidden = true
            self.underFeedbackLbl.isHidden = true
            self.signOutBtn.setTitle("Sign in".localized(), for: .normal)
            self.signOutBtn.setTitle("Sign in".localized(), for: .selected)
            self.signOutBtn.backgroundColor = .systemGreen
            
        }
        else{
            self.subscriptionsSTV.isHidden = true
            self.deleteAccountSTV.isHidden = false
            self.under_subscribtionsLbl.isHidden = true
            self.notiBtn.isHidden = false
            self.notificationsCountLbl.isHidden = false
            if UserManager.sharedInstance.notificationStatus  == 1 {
                    self.notificationStatus.isOn = true
                }
                else{
                    self.notificationStatus.isOn = false
                }
        }
        
        logoutPresenter.attachView(view: self)
        deleteAccountPresenter.attachView(view: self)
        self.logoutPresenter.attachView(view: self)
        self.notificationPresenter.attachView(view: self)
        self.settingsPresenter.attachView(view: self)
        self.registerPresenter.attachView(view: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        if L102Language.isRTL{
            self.langBtn.titleLabel?.textAlignment = .right
            self.langBtn.contentHorizontalAlignment = .right
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        let notiCount = UserManager.sharedInstance.notificationCount
        if notiCount > 0 {
            self.notificationsCountLbl.text = (" \(notiCount) ")
            self.notificationsCountLbl.isHidden = false
        }
        else{
             self.notificationsCountLbl.isHidden = true
        }
    }
    @IBAction func notificationOpenAction(_ sender: UIButton) {
           if UserManager.sharedInstance.getUser() != nil {
               if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                   
                   if let navigator = navigationController {
                       navigator.pushViewController(viewController, animated: true)
                   }
               }
           }
           else{
               self.showErrorMessage(message: "you have to login first".localized())
           }
       }
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       }

    @IBAction func NotificationStatusSwitch(_ sender: Any) {
        let status = UserManager.sharedInstance.notificationStatus
        if status == 1 {
            notificationPresenter.changeNotificationStatus(status: 0)
        }
        else{
            notificationPresenter.changeNotificationStatus(status: 1)
        }
    }
    @objc func deleteAccount(){
        let alert = UIAlertController(title: "Delete Account".localized(), message: "you_sure_account".localized(), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Delete Account".localized(), style: .destructive , handler:{ (UIAlertAction)in
            self.deleteAccountPresenter.deleteAccount()
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel , handler:{ (UIAlertAction)in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.popoverPresentationController?.sourceView = self.view;
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 2 ,width: UIScreen.main.bounds.width / 4 ,height: 1.0);
        self.present(alert, animated: true, completion: {
            
        })
    }
    @IBAction func signOutAction(_ sender: Any) {
        if UserManager.sharedInstance.getUser() == nil {
            if #available(iOS 13.0, *) {
                let scene = UIApplication.shared.connectedScenes.first
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    let mainwindow = sd.window
                    let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                    mainwindow?.rootViewController = stry.instantiateInitialViewController()
                }
            }
            else{
                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
            }
        }else {
            
            let alert = UIAlertController(title: "Logout".localized(), message: "Are you Shure to Logout ?".localized(), preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: .default , handler:{ (UIAlertAction)in
                self.logoutPresenter.doLogout()
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel , handler:{ (UIAlertAction)in
                alert.dismiss(animated: true, completion: nil)
            }))
            alert.popoverPresentationController?.sourceView = self.view;
            alert.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 2 ,width: UIScreen.main.bounds.width / 4 ,height: 1.0);
            self.present(alert, animated: true, completion: {
                
            })
        }
    }
    @objc func openTerms(){
        if let viewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    @objc func openAbout(){
        if let viewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    @objc func rateApp(){
        SKStoreReviewController.requestReview()
    }
    @objc func openContact(){
        settingsPresenter.getContactInfo()
    }
    @objc func openFAQs(){
        if let viewController = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "ViewAnswersViewController") as? ViewAnswersViewController {
            
            if let navigator = navigationController {
                viewController.viewAnswersCase = .FAQ
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    @objc func openFeedback(){
        if let viewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "FeedBackViewCtrl") as? FeedBackVCtrl {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    @objc func openFavs(){
        if UserManager.sharedInstance.getUser() != nil {
            self.pushViewController(viewControllerName: "PreferencesViewController", storyBoardName: "Main")
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    
    @objc func openInvoices(){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InvoicesListVCtrl") as? invoiceesListVCtrl {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    @objc func opensubscribtions(){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "newSubscriptionsViewController") as? newSubscribtionVCtrl {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    @objc func languageTapped(){
        
        let genderPicker = UIPickerView()
        
        var startIndex = 1
        if L102Language.currentAppleLanguage() == "en" {
            startIndex = 0
        }
        genderPicker.addCustomPicker(languageArray as NSArray , self.langBtn , "Language".localized(), startIndex) { (selectedIndex) in
            
            
            switch(selectedIndex) {
            case 0 :
                if selectedIndex != startIndex {
                    if UserManager.sharedInstance.getUser() != nil {
                        self.registerPresenter.doEditProfile(mobileApplicationLocale: "en")
                    }
                    else{
                        self.dismiss(animated: true) {
                            self.handleLanguae()
                        }
                    }
                }
            case 1 :
                
                if UserManager.sharedInstance.getUser() != nil {
                    self.registerPresenter.doEditProfile(mobileApplicationLocale: "ar")
                }
                else{
                    self.dismiss(animated: true) {
                        self.handleLanguae()
                    }
                }
            default:
                self.dismiss(animated: true) {
                    self.handleLanguae()
                }
            }
            
            
            
        }
        
    }
    func handleLanguae() {
        DispatchQueue.main.async {
            //self.dismiss(animated: true, completion: nil)
            var transition: UIView.AnimationOptions = .transitionFlipFromLeft
            if L102Language.currentAppleLanguage() == "en" {
                L102Language.setAppleLAnguageTo(lang: "ar-SA")
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                self.jobCatsLbl.textAlignment = .right
//                self.notificationsLbl.textAlignment = .right
//                self.termsConditionsLbl.textAlignment = .right
//                self.aboutLbl.textAlignment = .right
//                self.faqsLbl.textAlignment = .right
//                self.langBtn.contentHorizontalAlignment = .right
                self.langBtn.titleLabel?.textAlignment = .right
//                self.logoutLbl.textAlignment = .right
            } else {
                L102Language.setAppleLAnguageTo(lang: "en")
                transition = .transitionFlipFromRight
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                self.jobCatsLbl.textAlignment = .left
//                self.notificationsLbl.textAlignment = .left
//                self.termsConditionsLbl.textAlignment = .left
//                self.aboutLbl.textAlignment = .left
//                self.faqsLbl.textAlignment = .left
//                self.langBtn.contentHorizontalAlignment = .left
                self.langBtn.titleLabel?.textAlignment = .left
//                self.logoutLbl.textAlignment = .left
            }
            
            let roottView = self.storyboard?.instantiateInitialViewController()
            if #available(iOS 13.0, *) {
                let scene = UIApplication.shared.connectedScenes.first
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    let mainwindow = sd.window
                    mainwindow?.rootViewController = roottView
                    
                    UIView.transition(with: mainwindow!, duration: 0.55001, options: transition, animations: { () -> Void in
                    }) { (finished) -> Void in
                        
                    }
                }
            } else {
                // Fallback on earlier versions
                let mainwindow = (UIApplication.shared.delegate?.window!)!
                mainwindow.rootViewController = roottView
                
                UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
                }) { (finished) -> Void in
                    
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AppSettingsVCtrl:LogoutView, RegisterView {
    func onSuccessGetCaptch(captchaCode: String) {
    }
    func onSucess() {
        self.handleLanguae()
    }
    
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucess(message: String) {
        // self.showSucessMessage(message: message)
        self.clearDataAndStart()
    }
    
    func onError(message: String) {
        if message == "changeLang" {
            self.showErrorMessage(message: "Connection error, please try again later".localized())
        }
        else{
            self.clearDataAndStart()
        }
        
    }
    
    
}

extension AppSettingsVCtrl:NotificationView{
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        if let user = UserManager.sharedInstance.getUser() {
            user.NotificationStatus = notificationStatus
            UserManager.sharedInstance.saveUser(user: user)
        }
        if notificationStatus == 1 {
            self.notificationStatus.isOn = true
        }
        else{
            self.notificationStatus.isOn = false
        }
    }
    
    func onSucess(notificationObject: NotificaationsListResponse) {
    }
}
extension AppSettingsVCtrl:SettingsPagesView{
    func showLoadingPageIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingPageIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    func onSuccessContactInfo(contactInfo: ContactUsRes){
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUSViewController") as? contactUSVCtrl {
                viewController.modalPresentationStyle = .overCurrentContext
            viewController.email = contactInfo.contactEmail ?? ""
            viewController.phone = contactInfo.contactPhone ?? ""
                self.present(viewController, animated: true, completion: nil)
        }
    }
    func onSucessPage(pageObj: Page) {
    }
    
    func onErrorPage(message: String) {
        self.showErrorMessage(message: message)
    }
}
extension AppSettingsVCtrl: DeleteAccountView{
    func onErrorDeleteAccount(message: String) {
        self.showErrorMessage(message: message)
    }
    
    func onSucessDeleteAccount(message: String) {
        self.clearDataAndStart()
    }
}
