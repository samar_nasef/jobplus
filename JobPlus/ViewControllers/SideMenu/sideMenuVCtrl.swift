//
//  sideMenuVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/25/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
import SideMenu

class sideMenuVCtrl: UIViewController {
    
    
    @IBOutlet weak var arabsclicksBtn: UIButton!
    @IBOutlet weak var arabaClickSTV: UIStackView!
    @IBOutlet weak var messagesBtn: UIButton!
    
    @IBOutlet weak var MessagesSTV: UIStackView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userImgContainerView: UIView!
    @IBOutlet weak var settingsSTV: UIStackView!
    @IBOutlet weak var manageSubscribtionsSTV: UIStackView!
    @IBOutlet weak var jobCatsLbl: UILabel!
    @IBOutlet weak var notificationsLbl: UILabel!
    
    @IBOutlet weak var settingsBtn: UIButton!
    
    @IBOutlet weak var logoutLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var logoutSTV: UIStackView!
    @IBOutlet weak var NotificationsSTV: UIStackView!
    
    @IBOutlet weak var jobCatsSTV: UIStackView!
    @IBOutlet weak var AppliedJobSTV: UIStackView!
    @IBOutlet weak var DiscountsSTV: UIStackView!
    @IBOutlet weak var NewsSTV: UIStackView!
    @IBOutlet weak var openProfileBtn: UIButton!
    @IBOutlet weak var subscribedUserImg: UIImageView!
    @IBOutlet weak var recommendedJoboSTV: UIStackView!
    
    
    var logoutPresenter = LogoutPresenter()
    var spinnerView: UIView?

    override func viewWillAppear(_ animated: Bool) {
        if UserManager.sharedInstance.getUser() != nil {
            if UserManager.sharedInstance.showOffers == 1 {
                DiscountsSTV.isHidden = false
            }
            else{
                DiscountsSTV.isHidden = true
            }
            if UserManager.sharedInstance.showNewsArticles == 1 {
                NewsSTV.isHidden = false
            }
            else{
                NewsSTV.isHidden = true
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        logoutPresenter.attachView(view: self)
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        userImgContainerView.layerGradient()
        userImg.layer.cornerRadius = 47
        self.logoutPresenter.attachView(view: self)
        if L102Language.isRTL { SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
        }
        else{ SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
            
        }
        
        if L102Language.isRTL{
            self.settingsBtn.contentHorizontalAlignment = .right
            
            self.messagesBtn.contentHorizontalAlignment = .right
            self.arabsclicksBtn.contentHorizontalAlignment = .right
            
            
            
        }
        else{
            self.settingsBtn.contentHorizontalAlignment = .left
           
            self.messagesBtn.contentHorizontalAlignment = .left
            self.arabsclicksBtn.contentHorizontalAlignment = .left
            
        }
        let appliedJobsTap = UITapGestureRecognizer(target: self, action: #selector(openAppliedJobs))
        AppliedJobSTV.isUserInteractionEnabled = true
        AppliedJobSTV.addGestureRecognizer(appliedJobsTap)
        
        let recommenedTap = UITapGestureRecognizer(target: self, action: #selector(openRecommendedJobs))
        recommendedJoboSTV.isUserInteractionEnabled = true
        recommendedJoboSTV.addGestureRecognizer(recommenedTap)
        
       
        let discountsTap = UITapGestureRecognizer(target: self, action: #selector(openDiscounts))
        DiscountsSTV.isUserInteractionEnabled = true
        DiscountsSTV.addGestureRecognizer(discountsTap)
        
        
        let messagesTap = UITapGestureRecognizer(target: self, action: #selector(openMessages))
        MessagesSTV.isUserInteractionEnabled = true
        MessagesSTV.addGestureRecognizer(messagesTap)
    
        let arabClicksTap = UITapGestureRecognizer(target: self, action: #selector(openarabsClick))
        arabaClickSTV.isUserInteractionEnabled = true
        arabaClickSTV.addGestureRecognizer(arabClicksTap)
        
        let newsTap = UITapGestureRecognizer(target: self, action: #selector(openNews))
        NewsSTV.isUserInteractionEnabled = true
        NewsSTV.addGestureRecognizer(newsTap)
        
        let jobCatsTap = UITapGestureRecognizer(target: self, action: #selector(jobCatsAction))
        jobCatsSTV.isUserInteractionEnabled = true
        jobCatsSTV.addGestureRecognizer(jobCatsTap)
        
        let manageSubscribtionsTap = UITapGestureRecognizer(target: self, action: #selector(manageSubscriptionsTapped))
        manageSubscribtionsSTV.isUserInteractionEnabled = true
        manageSubscribtionsSTV.addGestureRecognizer(manageSubscribtionsTap)
       
        let settingsTap = UITapGestureRecognizer(target: self, action: #selector(SettingsTapped))
        settingsSTV.isUserInteractionEnabled = true
        settingsSTV.addGestureRecognizer(settingsTap)
        
        let logoutTap = UITapGestureRecognizer(target: self, action: #selector(logOutApp))
        logoutSTV.isUserInteractionEnabled = true
        logoutSTV.addGestureRecognizer(logoutTap)
        
        
        // Do any additional setup after loading the view.
        
        if UserManager.sharedInstance.getUser() == nil {
            logoutLbl.text = "Sign in".localized()
        }
        else{
            logoutLbl.text = "Logout".localized()
            let userNameTap = UITapGestureRecognizer(target: self, action: #selector(OpenProfile))
            userNameLbl.isUserInteractionEnabled = true
            userNameLbl.addGestureRecognizer(userNameTap)
            userImgContainerView.isHidden = false
            userImg.isHidden = false
            let userImgTap = UITapGestureRecognizer(target: self, action: #selector(OpenProfile))
            userImg.isUserInteractionEnabled = true
            userImg.addGestureRecognizer(userImgTap)
            NotificationsSTV.isHidden = false
            let notificationsTap = UITapGestureRecognizer(target: self, action: #selector(openNotifications))
            NotificationsSTV.isUserInteractionEnabled = true
            NotificationsSTV.addGestureRecognizer(notificationsTap)
            
            AppliedJobSTV.isHidden = false
            manageSubscribtionsSTV.isHidden = false
            recommendedJoboSTV.isHidden = false
            //DiscountsSTV.isHidden = false
            //NewsSTV.isHidden = false
            MessagesSTV.isHidden =  true
            arabaClickSTV.isHidden =  false
            if UserManager.sharedInstance.showOffers == 1 {
                DiscountsSTV.isHidden = false
            }
            else{
                DiscountsSTV.isHidden = true
            }
            if UserManager.sharedInstance.showNewsArticles == 1 {
                NewsSTV.isHidden = false
            }
            else{
                NewsSTV.isHidden = true
            }
        }
        
        if let subsrcibed = UserManager.sharedInstance.subscribedUser {
            if subsrcibed && UserManager.sharedInstance.getUser()?.token != nil {
                self.subscribedUserImg.isHidden = false
            }
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.showSubscribedImg),
                                               name: NSNotification.Name(rawValue: "showSubscribedImg"),
                                               object: nil)
        self.addUserData()
        
    }
    @objc func showSubscribedImg(){
        DispatchQueue.main.async {
            if let subsrcibed = UserManager.sharedInstance.subscribedUser {
                if subsrcibed && UserManager.sharedInstance.getUser()?.token != nil {
                    self.subscribedUserImg.isHidden = false
                }
                else{
                    self.subscribedUserImg.isHidden = true
                }
            }
        }
    }
    @IBAction func openProfileAction(_ sender: Any) {
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openProfile"), object: nil)
        }
    }
    
    @objc func openAppliedJobs(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openAppliedJobs"), object: nil)
        }
    }
    
    @objc func openDiscounts(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "opnDiscounts"), object: nil)
        }
    }
    
    @objc func openMessages(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMessages"), object: nil)
        }
    }
    
    @objc func openarabsClick(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "arabClicks"), object: nil)
        }
    }
    @objc func openNews(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNews"), object: nil)
        }
    }
    
    @objc func openRecommendedJobs(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openRecommendedJobs"), object: nil)
        }
    }
    
    @objc func jobCatsAction(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadHome"), object: nil)
        }
    }
    
    @objc func openNotifications(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNotifications"), object: nil)
        }
    }
    
    
    
    
    func addUserData(){
        //        if UserManager.sharedInstance.getUser() == nil {
        //            self.userImgView.image = #imageLiteral(resourceName: "ic_user")
        //        }else {
        if UserManager.sharedInstance.getUser() != nil {
            if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
                if let profileImgUrl = URL(string: profileImg){
                    self.userImg.kf.setImage(with: profileImgUrl, placeholder: UIImage(named: "profile_img"))
                }
                
            }
            else {
                if let gender = UserManager.sharedInstance.getUser()?.gender {
                    if gender == "female" {
                        self.userImg.image = UIImage(named: "woman")
                    }
                    else{
                        self.userImg.image = UIImage(named: "profile_img")
                    }
                }
                else{
                    self.userImg.image = UIImage(named: "profile_img")
                }
            }
            // }
            if let userName = UserManager.sharedInstance.getUser()?.name {
                self.userNameLbl.text = userName + " " + ( UserManager.sharedInstance.getUser()?.LastName ?? "" )
            }
        }
    }
    
    @objc func OpenProfile(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openProfile"), object: nil)
        }
    }
    
    @objc func logOutApp(){
        
        if UserManager.sharedInstance.getUser() == nil {
            if #available(iOS 13.0, *) {
                let scene = UIApplication.shared.connectedScenes.first
                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                    let mainwindow = sd.window
                    let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                    mainwindow?.rootViewController = stry.instantiateInitialViewController()
                }
            }
            else{
                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                let stry = UIStoryboard(name: "Authorizations", bundle: nil)
                rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
            }
        }else {
            
            let alert = UIAlertController(title: "Logout".localized(), message: "Are you Shure to Logout ?".localized(), preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: .default , handler:{ (UIAlertAction)in
                self.logoutPresenter.doLogout()
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel , handler:{ (UIAlertAction)in
                alert.dismiss(animated: true, completion: nil)
            }))
            alert.popoverPresentationController?.sourceView = self.view;
            alert.popoverPresentationController?.sourceRect = CGRect(x: 0,y: UIScreen.main.bounds.height / 2 ,width: UIScreen.main.bounds.width / 4 ,height: 1.0);
            self.present(alert, animated: true, completion: {
                
            })
        }
    }
    
    @objc func SettingsTapped(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openSettings"), object: nil)
        }
    }
    
    @objc func manageSubscriptionsTapped(){
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "opensubscribtions"), object: nil)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension sideMenuVCtrl:LogoutView {
    func onSucess() {
       
    }
    
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucess(message: String) {
        // self.showSucessMessage(message: message)
        self.clearDataAndStart()
    }
    
    func onError(message: String) {
        if message == "changeLang" {
            self.showErrorMessage(message: "Connection error, please try again later".localized())
        }
        else{
            self.clearDataAndStart()
        }
        
    }
    
    
}

