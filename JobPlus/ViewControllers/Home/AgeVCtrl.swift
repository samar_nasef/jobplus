//
//  AgeVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 31/01/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField

class AgeVCtrl: UIViewController {
    var delegate: getAge?
    @IBOutlet weak var fromTxtFeild: TweeAttributedTextField!
    @IBOutlet weak var toTxtFeild: TweeAttributedTextField!
    
    override func viewDidLoad() {
        self.setupTextFields()
        self.view.changeFont()
        
    }
    @IBAction func DismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveDataForSearch(_ sender: Any) {
        let fromAge = Int(fromTxtFeild.text ?? "0") ?? 0
        let toAge = Int(toTxtFeild.text ?? "0") ?? 0
        if fromAge == 0 {
            self.fromTxtFeild.showInfo("Enter age from!".localized())
        }
        else if toAge > 0 && toAge < fromAge {
            self.showErrorMessage(message: "To age must be greater than from age".localized())
        }
        else{
            self.delegate?.getAge(from: fromAge, to: toAge)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    func setupTextFields(){
        fromTxtFeild.delegate = self
        fromTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        fromTxtFeild.returnKeyType = .next
        fromTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        fromTxtFeild.infoFontSize = 14.0
        fromTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        fromTxtFeild.activeLineWidth = 2
        fromTxtFeild.animationDuration = 0.5
        fromTxtFeild.minimumPlaceholderFontSize = 15
        fromTxtFeild.originalPlaceholderFontSize = 15
        fromTxtFeild.placeholderDuration = 0.2
        fromTxtFeild.placeholderColor = .darkGray
        fromTxtFeild.keyboardType = .numberPad
        fromTxtFeild.placeholder = "From".localized()
        
        toTxtFeild.delegate = self
        toTxtFeild.textColor = UIColor(hexString: "#8c8c8c")
        toTxtFeild.returnKeyType = .done
        toTxtFeild.infoTextColor = UIColor(hexString: "#FF2600")
        toTxtFeild.infoFontSize = 14.0
        toTxtFeild.activeLineColor = UIColor(hexString: "#00AFDF")
        toTxtFeild.activeLineWidth = 2
        toTxtFeild.animationDuration = 0.5
        toTxtFeild.minimumPlaceholderFontSize = 15
        toTxtFeild.originalPlaceholderFontSize = 15
        toTxtFeild.placeholderDuration = 0.2
        toTxtFeild.placeholderColor = .darkGray
        toTxtFeild.keyboardType = .numberPad
        toTxtFeild.placeholder = "To".localized()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("start typing")
        
        self.resetCustomTextFieldsErrorMessages(textField)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.resetCustomTextFieldsErrorMessages(textField)
        
        return true
    }
    func resetCustomTextFieldsErrorMessages(_ textField: UITextField){
        if textField == self.fromTxtFeild {
            self.fromTxtFeild.showInfo("")
        }
    }
}
extension AgeVCtrl :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.fromTxtFeild) {
            self.toTxtFeild.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return true
    }
}
