//
//  JobsListVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 14/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
//import GoogleMobileAds
import Floaty


class JobsListVCtrl: UIViewController, UITableViewDelegate, UITableViewDataSource ,FloatyDelegate{
    
    
    var JobsAndAdsArr = [AnyObject]()
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var sectorNameLbl: UILabel!
    @IBOutlet weak var HomeJobsTV: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    let floaty = Floaty()
    var itemCountForWindless = 8
    let jobsPresenter = JobsPresenter()
    var jobsResponse:JobListResponse?
    var page = 1
    var SectorID = 0
    var sectorName = ""
    var spinnerView: UIView?
    var order = KeysAndValues.ORDERDESC
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        //        let chunk = arr.chunks(5)
        //        print("chunks:\(chunk)")
        
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
        }
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search here".localized(),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        self.sectorNameLbl.text = self.sectorName
        HomeJobsTV.layer.cornerRadius = 10.0
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        initPullToRefresh()
        initLoadMoreToRefresh()
        jobsPresenter.attachView(view:self)
        loadJobsData()
        emptyView.msgLbl.text = "There are no jobs".localized()
        // Do any additional setup after loading the view.
        
        
        floaty.buttonColor = #colorLiteral(red: 1, green: 0.7725490196, blue: 0.2235294118, alpha: 1)
        floaty.buttonImage = UIImage(named: "upArrow")
        floaty.fabDelegate = self
        let item = FloatyItem()
        item.buttonColor = UIColor.clear
        item.circleShadowColor = UIColor.clear
        item.titleShadowColor = UIColor.clear
        floaty.addItem(item: item)
        if L102Language.isRTL {
            Floaty.global.rtlMode = true
        }
        
    }
    
    
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
        scrollToTop()
        floaty.close()
        
    }
    
    
    
    private func scrollToTop() {
        print("scrollToTop")
        let topRow = IndexPath(row: 0,
                               section: 0)
        self.HomeJobsTV.scrollToRow(at: topRow,
                                    at: .top,
                                    animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserManager.sharedInstance.jobAppliedOrCanceled > 0 {
            if let indexPath = UserManager.sharedInstance.jobIndexAppliedOrCanceled {
                let cell = HomeJobsTV.cellForRow(at: indexPath) as? HomeJobsTVCell
                if UserManager.sharedInstance.jobAppliedOrCanceled == 1 {
                    if JobsAndAdsArr[indexPath.row] is jobItem2ForAds {
                        (JobsAndAdsArr[indexPath.row] as? jobItem2ForAds)?.appliedForJob = true
                        cell?.ApplyJobBtn.setTitle("Applied".localized(), for: .normal)
                        cell?.ApplyJobBtn.setTitle("Applied".localized(), for: .selected)
                        cell?.ApplyJobBtn.backgroundColor = UIColor(hexString: "#00ccff")
                    }
                    
                }
                else{
                    if JobsAndAdsArr[indexPath.row] is jobItem2ForAds {
                        (JobsAndAdsArr[indexPath.row] as? jobItem2ForAds)?.appliedForJob = false
                        cell?.ApplyJobBtn.setTitle("Apply Now".localized(), for: .normal)
                        cell?.ApplyJobBtn.setTitle("Apply Now".localized(), for: .selected)
                        cell?.ApplyJobBtn.backgroundColor = UIColor(hexString: "#161650")
                    }
                    
                }
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
            else{
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
        }
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    @IBAction func goback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func initPullToRefresh()
    {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        HomeJobsTV?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.page = 1
            self?.loadJobsData()
        }, loadingView: loadingView)
        self.HomeJobsTV?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        self.HomeJobsTV?.dg_setPullToRefreshBackgroundColor(HomeJobsTV.backgroundColor!)
    }
    
    func initLoadMoreToRefresh()
    {
        self.HomeJobsTV?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.HomeJobsTV?.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return self.page != self.jobsResponse?.last_page
        }
        self.HomeJobsTV?.addInfiniteScroll { (tableView) -> Void in
            
            if self.page == self.jobsResponse?.last_page {
                //ending paging
                self.HomeJobsTV.finishInfiniteScroll()
            }else {
                self.page += 1
                self.loadJobsData()
                
            }
        }
    }
    func loadJobsData(){
        
        self.HomeJobsTV.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        
        self.emptyView.alpha = 0
        self.jobsPresenter.getJobsBySectorID(ID: SectorID, page: self.page)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JobsAndAdsArr.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("heightForRowAt")
        if let _ =  JobsAndAdsArr[indexPath.row] as? jobItem2ForAds {
            return 135
        }else{
            return 85
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HomeJobsTV.dequeueReusableCell(withIdentifier: "HomeJobsCell", for: indexPath) as! HomeJobsTVCell
        if let jobItem =  JobsAndAdsArr[indexPath.row] as? jobItem2ForAds {
            
            if jobsResponse == nil {
                // self.HomeJobsTV.alpha = 0
            }
            else{
                cell.whiteContainerView.layer.cornerRadius = 10
                cell.ApplyJobBtn.layer.cornerRadius = 12.3
                cell.shareBtn.layer.cornerRadius = 12.3
                cell.shareBtn.tag = indexPath.row
                cell.shareBtn.setTitle("Share".localized(), for: .normal)
                cell.shareBtn.setTitle("Share".localized(), for: .selected)
                
                cell.shareBtn.backgroundColor = UIColor(hexString: "#161650")
                cell.shareBtn.addTarget(self, action: #selector(shareJob2(_:)), for: .touchUpInside)
                if (jobsResponse?.data!.count)! > 0 {
                    
                    //                    let item = jobsResponse?.data![indexPath.row]
                    let item = JobsAndAdsArr[indexPath.row] as? jobItem2ForAds
                    if let disabilityEnabled = item?.DisabilityEnabled {
                        if disabilityEnabled == 1 {
                            cell.disability_ic.isHidden = false
                        }
                        else{
                            cell.disability_ic.isHidden = true
                        }
                    }
                    else{
                        cell.disability_ic.isHidden = true
                    }
                    if let expired_status = item?.jobExpired  {
                        if expired_status {
                            cell.ApplyJobBtn.isHidden = true
                            cell.shareBtn.isHidden = true
                        }
                        else{
                            cell.ApplyJobBtn.isHidden = false
                            cell.shareBtn.isHidden = false
                        }
                    }
                    else{
                        cell.ApplyJobBtn.isHidden = false
                        cell.shareBtn.isHidden = false
                    }
                    if let appliedForJobStatus = item?.appliedForJob {
                        if appliedForJobStatus {
                            cell.ApplyJobBtn.setTitle("Applied".localized(), for: .normal)
                            cell.ApplyJobBtn.setTitle("Applied".localized(), for: .selected)
                            
                            //cell.ApplyJobBtn.customLayerGradient(color1: "#af8dc0", color2: "#9e75b2", cornerRadius: 12.3)
                            cell.ApplyJobBtn.backgroundColor = UIColor(hexString: "#00ccff")
                        }
                        else{
                            cell.ApplyJobBtn.setTitle("Apply Now".localized(), for: .normal)
                            cell.ApplyJobBtn.setTitle("Apply Now".localized(), for: .selected)
                            
                            cell.ApplyJobBtn.backgroundColor = UIColor(hexString: "#161650")
                            //cell.ApplyJobBtn.customLayerGradient(color1: "#00afdf", color2: "#0abced", cornerRadius: 12.3)
                        }
                    }
                    cell.jobTitle.text = item?.title ?? ""
                    if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                        if subscribedUser {
                            cell.JobImg.kf.indicatorType = .activity
                            let url = URL(string: (item?.imagePath ?? "")!)
                            
                            let processor = ResizingImageProcessor(referenceSize: CGSize(width: 22, height: 22))
                            
                            cell.JobImg.kf.setImage(
                                with: url,
                                placeholder: UIImage(named: "company_ic_large"),
                                options: [
                                    
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1)),
                                    .cacheOriginalImage,
                                    
                                ], completionHandler:
                                    {
                                        result in
                                        switch result {
                                        case .success(let value):
                                            print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                        case .failure(let error):
                                            print("Job failed: \(error.localizedDescription)")
                                        }
                                    })
                            
                        }
                    }
                    if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                        if subscribedUser {
                            cell.comNameHeightConstraint.constant = 20
                            cell.JobCompany.text = item?.companyName
                        }
                    }
                    cell.JobType.text = "\(item?.job_type_name ?? "" ) - \(item?.workTimeText ?? "")"
                    if let text_color = item?.job_type_color {
                        cell.JobType.textColor = UIColor(hexString: text_color)
                    }
                    else{
                        cell.JobType.textColor = UIColor(hexString: "#9E75B2")
                    }
                    cell.JobLocation.text = item?.city
                    
                    if let creationDate = item?.created_at {
                        cell.JobPostedTime.text = creationDate.getFormattedDate()
                    }
                }
            }
            
            cell.selectionStyle = .none
            
        }else{
            //            print("ad cell")
            //            let cell = HomeJobsTV.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as! adsTVCell
            //
            ////            cell.BannerView.adUnitID = "ca-app-pub-6076183060289610/6283608958"
            ////            "ca-app-pub-3940256099942544/2934735716"
            //            cell.BannerView.adUnitID = KeysAndValues.BannerViewAdUnitID
            //            cell.BannerView.rootViewController = self
            //            let frame = { () -> CGRect in
            //              if #available(iOS 11.0, *) {
            //                return view.frame.inset(by: view.safeAreaInsets)
            //              } else {
            //                return view.frame
            //              }
            //            }()
            //            let viewWidth = frame.size.width
            //            cell.BannerView.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth)
            //
            //            cell.BannerView.load(GADRequest())
            //            cell.selectionStyle = .none
            //            return cell
        }
        
        return cell
        
        
    }
    @objc func shareJob(_ sender: UIButton){
        let jobIndex = sender.tag
        self.spinnerView = UIViewController.displaySpinner(onView: self.view)
        if (jobsResponse?.data!.count)! > 0 {
            
            let item = jobsResponse?.data![jobIndex]
            //let urlShare = "https://jobplus.page.link/job?id=\(item?.id ?? 0)"
            var components = URLComponents()
            components.scheme = "https"
            //"www.jobplus.biz/parttime/public"
            components.host = "jobplus.page.link"
            components.path = "/job"
            let jobQueryItem = URLQueryItem(name: "id", value: "\(item?.id ?? 0)")
            components.queryItems = [jobQueryItem]
            
            guard let linkParameter = components.url else {return}
            print("I am sharing link \(linkParameter.absoluteURL)")
            
            guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://jobplus.page.link") else{
                print("couldnot create link")
                return
            }
            shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            shareLink.socialMetaTagParameters?.title = item?.title ?? ""
            shareLink.socialMetaTagParameters?.imageURL = URL(string: "https://www.jobplus.biz/parttime/public/img/landing-page/icon/JP_Logo1.png")
            if let myBundleID = Bundle.main.bundleIdentifier {
                shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleID)
            }
            shareLink.iOSParameters?.appStoreID = "1493332366"
            shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.adroyts.jobplus")
            guard let longURL = shareLink.url else {return}
            print("longUrl \(longURL.absoluteString)")
            shareLink.shorten { (url, warnings, error) in
                guard error == nil else {
                    print(error!)
                    return
                }
                if let url = url {
                    print("shorten url \(url.absoluteString)")
                    
                    self.showShareActivity(msg: "\(item?.title ?? "") \n\(item?.job_type_name ?? "")", image: UIImage(named: "jpLogo_splash"), url: url.absoluteString, sourceRect: nil)
                }
            }
            
        }
        
    }
    @IBAction func openSearchAction(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchViewCtrl") as? searchVCtrl {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @objc func shareJob2(_ sender: UIButton){
        let jobIndex = sender.tag
        self.spinnerView = UIViewController.displaySpinner(onView: self.view)
        if (jobsResponse?.data!.count)! > 0 {
            
            let item = JobsAndAdsArr[jobIndex] as? jobItem2ForAds //jobsResponse?.data![jobIndex]
            guard let link = URL(string: "https://jobplus.page.link/job?id=\(item?.id ?? 0)") else { return }
            let dynamicLinksDomainURIPrefix = "https://jobplus.page.link"
            let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
            
            linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.adroyts.jobplus")
            linkBuilder?.iOSParameters?.appStoreID = "1493332366"
            
            linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.adroyts.jobplus")
            
            
            linkBuilder?.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            linkBuilder?.socialMetaTagParameters?.title = item?.title ?? ""
            linkBuilder?.socialMetaTagParameters?.descriptionText = item?.job_type_name ?? ""
            linkBuilder?.socialMetaTagParameters?.imageURL = URL(string: "https://www.jobplus.biz/parttime/public/img/landing-page/icon/JP_Logo1.png")
            
            guard let longDynamicLink = linkBuilder?.url else { return }
            print("The long URL is: \(longDynamicLink)")
            linkBuilder?.options = DynamicLinkComponentsOptions()
            linkBuilder?.shorten() { url, warnings, error in
                guard let url = url, error == nil else { if let sv = self.spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
                    self.showErrorMessage(message: "Failed to generate share link, please try again later.")
                    print("shorten error \(String(describing: error))"); return }
                print("The short URL is: \(url)")
                self.showShareActivity(msg: "\(item?.title ?? "") \n\(item?.job_type_name ?? "")", image: UIImage(named: "jpLogo_splash"), url: url.absoluteString, sourceRect: nil)
            }
            
        }
    }
    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems:  [msg ?? "", url ?? ""], applicationActivities: nil)
        activityViewController.setValue("Job Plus", forKey: "Subject")
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        activityViewController.popoverPresentationController?.sourceView = self.view;
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
        
        self.present(activityViewController, animated: true) {
            if let sv = self.spinnerView{
                UIViewController.removeSpinner(spinner: sv)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
        //        if let item = jobsResponse?.data![indexPath.row] {
        if let item = JobsAndAdsArr[indexPath.row] as? jobItem2ForAds{
            
            if let expired_status = item.jobExpired {
                if expired_status {
                    cell.alpha = 0.5
                }
                else{
                    cell.alpha = 1
                }
                
            }
            else{
                cell.alpha = 1
            }
        }
    }
    //&& !(jobsResponse?.data![indexPath.row].jobExpired)!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if jobsResponse?.data != nil  {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                viewController.jobItem = JobsAndAdsArr[indexPath.row] as? jobItem2ForAds //jobsResponse?.data![indexPath.row]
                if let navigator = navigationController {
                    UserManager.sharedInstance.jobIndexAppliedOrCanceled = indexPath
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension JobsListVCtrl : JobsView {
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        itemCountForWindless = 0
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onError(message: String) {
        self.showErrorMessage(message:message)
        self.HomeJobsTV.windless.end()
        itemCountForWindless = 0
        self.HomeJobsTV.dg_stopLoading()
        if let count = jobsResponse?.data?.count {
            if count > 0 {
                jobsResponse?.data? = []
            }
        }
        self.HomeJobsTV.reloadData()
    }
    
    func onSuccessNextLinkCall(jobsResponse: JobListResponse) {
        
        
    }
    
    func onSucess(jobsResponse: JobListResponse) {
        self.HomeJobsTV.windless.end()
        self.HomeJobsTV.dg_stopLoading()
        self.HomeJobsTV.finishInfiniteScroll()
        self.jobsResponse = jobsResponse
        self.HomeJobsTV.alpha = 1
        if let userDeleted = jobsResponse.userDeleted {
            if userDeleted == 1 {
                self.showErrorMessage(message: "Sorry, your account has been suspended, you can contact our support team".localized())
                clearDataAndStart()
            }
        }
        if page == 1 {
            self.JobsAndAdsArr.removeAll()
        }
        if let jobs = jobsResponse.data , jobs.count == 0 {
            if page == 1 {
                self.emptyView.alpha = 1
                self.HomeJobsTV.alpha = 0
            }
        }
        else{
            if let subscribed = jobsResponse.subscribedUser {
                if subscribed {
                    UserManager.sharedInstance.subscribedUser = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                }
                else{
                    UserManager.sharedInstance.subscribedUser = false
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                }
            }
            
            self.emptyView.alpha = 0
            self.HomeJobsTV.alpha = 1
            for index in 0..<self.jobsResponse!.data!.count {
                if let expired_date = self.jobsResponse!.data?[index].expireDate {
                    let staus = expired_date.checkExiration()
                    self.jobsResponse!.data?[index].jobExpired = staus
                }
                else{
                    self.jobsResponse!.data?[index].jobExpired = false
                }
            }
        }
        for i in  self.jobsResponse!.data! {
            JobsAndAdsArr.append(jobItem2ForAds(id: i.id, title: i.title ?? "", companyName: i.companyName, imagePath: i.imagePath ?? "", city: i.city, job_type_id: i.job_type_id ?? 0, job_type_name: i.job_type_name ?? "", job_type_color: i.job_type_color ?? "", created_at: i.created_at!, appliedForJob: i.appliedForJob ?? false , expireDate: i.expireDate ?? "", job_status: i.job_status ?? 0, jobExpired: i.jobExpired ?? false, workTimeText: i.workTimeText ?? "", DisabilityEnabled: i.DisabilityEnabled ?? 0))
            
        }
        self.HomeJobsTV.reloadData()
    }
    
}
extension JobsListVCtrl: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scroll happened")
        
        if scrollView.contentOffset.y == 0 {
            // TOP
            self.floaty.removeFromSuperview()
        }else{
            self.view.addSubview(floaty)
        }
        
    }
}
