//
//  searchVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 12/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

class searchVCtrl: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var HomeJobsTV: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var jobTypeBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var workTimesBtn: UIButton!
    @IBOutlet weak var companyBtn: UIButton!
    @IBOutlet weak var sectorBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var searchFiltersView: UIView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var jobTypeLbl: UILabel!
    @IBOutlet weak var workTimeLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var workSectorLbl: UILabel!
    @IBOutlet weak var jobsCountLbl: UILabel!
    @IBOutlet weak var disabilityBtn: UIButton!
    @IBOutlet weak var disabilityLbl: UILabel!
    @IBOutlet weak var experienceLbl: UILabel!
    @IBOutlet weak var experienceBtn: UIButton!
    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var specialityBtn: UIButton!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var ageBtn: UIButton!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var genderBtn: UIButton!
    
    var itemCountForWindless = 8
    let jobsTypesPresenter = JobsTypesPresenter()
    let jobsPresenter = JobsPresenter()
    var jobsResponse:JobListResponse?
    var cityList = [cityItem]()
    var selectedCityName = [String]()
    var selectedCityId:Int?
    var jobsTypesResponse:JobsTypesResponse?
    var selectedTypeName = [String]()
    var selectedTypeId:Int?
    var companiesList = [companyItem]()
    var selectedCompanyName = [String]()
    var selectedCompanyID:Int?
    var workingTimes = [worktims]()
    var selectedWorkingTime = [String]()
    var selectedWTID:Int?
    var workSectorsList = [workSectorItem]()
    var selectedSector = [String]()
    var selectedSectorID: Int?
    var disabilityList = [DisabilityTypeItem]()
    var selectedDisability = [String]()
    var selectedDisabilityID: Int?
    var experienceList = [experienceItem]()
    var selectedExperience = [String]()
    var selectedExperienceID: Int?
    var specialityList = [specialityItem]()
    var selectedSpeciality = [String]()
    var selectedSpecialityID: Int?
    var genderList = ["Male".localized() , "Female".localized(), "Both".localized()]
    var selectedGender = [String]()
    var selectedGenderIndex: Int?
    var ageFrom: Int?
    var ageTo: Int?
    var searchText:String? = ""
    var page = 1
    var jobToLoadType = JobToLoad.Newest
    var selectedJobTypeIndex = 0
    var spinnerView: UIView?
    var selectedTypeInde = 0
    var selectedCityIndex  = 0
    var selectetWTIndex = 0
    var filterTypeCityOrWorkingTime = 1 // 1-> ciy filter , 2-> working filter
    let searchPresenter = SearchPresenter()
    var order = KeysAndValues.ORDERDESC
    override func viewDidLoad() {
        print("search filters")
        super.viewDidLoad()
        
        
//        cityBtn.layer.cornerRadius = 6
        
        
        if L102Language.isRTL {
            self.backBtn.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.scrollView.transform = CGAffineTransform(rotationAngle: CGFloat(CGFloat.pi));
            self.searchFiltersView.transform =  CGAffineTransform(rotationAngle: CGFloat(CGFloat.pi));
        }
        self.view.changeFont()
        searchBarView.layer.cornerRadius = 16.0
        searchBarView.layer.shadowColor = UIColor.black.cgColor
        searchBarView.layer.shadowOpacity = 0.2
        searchBarView.layer.shadowOffset = .zero
        searchBarView.layer.shadowRadius = 5
        
        HomeJobsTV.layer.cornerRadius = 10.0
        selectedCityName.append("All".localized())
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search here".localized(),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        initPullToRefresh()
        initLoadMoreToRefresh()
       
        jobsTypesPresenter.attachView(view:self)
        jobsPresenter.attachView(view:self)
        searchPresenter.attachView(view: self)
        searchTextField.delegate = self
        searchTextField.becomeFirstResponder()
        self.emptyView.alpha = 0
        emptyView.msgLbl.text = "There are no jobs".localized()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserManager.sharedInstance.jobAppliedOrCanceled > 0 {
            if let indexPath = UserManager.sharedInstance.jobIndexAppliedOrCanceled {
                let cell = HomeJobsTV.cellForRow(at: indexPath) as? HomeJobsTVCell
                if UserManager.sharedInstance.jobAppliedOrCanceled == 1 {
                    jobsResponse?.data![indexPath.row].appliedForJob = true
                    cell?.ApplyJobBtn.setTitle("Applied".localized(), for: .normal)
                    cell?.ApplyJobBtn.setTitle("Applied".localized(), for: .selected)
                    cell?.ApplyJobBtn.backgroundColor = UIColor(hexString: "#00ccff")
                }
                else{
                    jobsResponse?.data![indexPath.row].appliedForJob = false
                    cell?.ApplyJobBtn.setTitle("Apply Now".localized(), for: .normal)
                    cell?.ApplyJobBtn.setTitle("Apply Now".localized(), for: .selected)
                    cell?.ApplyJobBtn.backgroundColor = UIColor(hexString: "#161650")
                }
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
            else{
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
        }
    }
    @IBAction func goback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetFilters(_ sender: Any) {
        self.jobsCountLbl.text = ""
        self.selectedCityId = nil
        self.selectedCityName = [String]()
        self.cityLbl.text = " City ".localized()
        
        self.selectedTypeId = nil
        self.selectedTypeName = [String]()
        self.jobTypeLbl.text = " Job Type ".localized()
        
        self.selectedWTID = nil
        self.selectedWorkingTime = [String]()
        self.workTimeLbl.text = " Work Times ".localized()
        
        self.selectedSectorID = nil
        self.selectedSector = [String]()
        self.workSectorLbl.text = " Work Sectors ".localized()
        
        self.selectedCompanyID = nil
        self.selectedCompanyName = [String]()
        self.companyLbl.text = " Company Name ".localized()
        
        self.selectedDisabilityID = nil
        self.selectedDisability = [String]()
        self.disabilityLbl.text = " Disability ".localized()
        
        self.selectedExperienceID = nil
        self.selectedExperience = [String]()
        self.experienceLbl.text = " Experience ".localized()
        
        self.selectedSpecialityID = nil
        self.selectedSpeciality = [String]()
        self.specialityLbl.text = " Speciality ".localized()
        
        self.selectedGenderIndex = nil
        self.selectedGender = [String]()
        self.genderLbl.text = " Gender ".localized()
        
        self.ageTo = nil
        self.ageFrom = nil
        self.ageLbl.text = " Age ".localized()
        
        self.searchTextField.text = ""
        self.HomeJobsTV.alpha = 0
    }
    @IBAction func cityFilterAction(_ sender: UIButton) {
        print("filterByCityiso")
        
        //let cityPicker = UIPickerView()
        if  cityList.count > 0 {
            let citiesNames = L102Language.isRTL ? cityList.map { $0.cityAr} : cityList.map { $0.cityEn }
            showCitySelectPopover(sender: sender, citiesNames: citiesNames)
        }
        else{
            searchPresenter.getCountries()
        }
    }
    @IBAction func typeFilterAction(_ sender: UIButton) {
        self.jobsTypesPresenter.getJobsTypes()
        
    }
    @IBAction func workTimeFilterAction(_ sender: UIButton) {
        if  workingTimes.count > 0 {
            let workingTimesNames = L102Language.isRTL ? workingTimes.map { $0.worktimeAr} : workingTimes.map { $0.worktimeEn }
            showWorkingTimesSelectPopover(sender: sender, workingTimesNames: workingTimesNames)
        }
        else{
            searchPresenter.getWorkingTimes()
        }
    }
    func showWorkingTimesSelectPopover(sender: UIView, workingTimesNames: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: workingTimesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedCompanyName)")
        selectionMenu.setSelectedItems(items: selectedWorkingTime) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedWorkingTime.count > 0 {
                if self!.selectedWorkingTime[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedTime  = self!.workingTimes.first(where: {$0.worktimeAr == text!!}) {
                    if let selectedID = selectedTime.id{
                        self!.selectedWTID = selectedID
                        self!.workTimeLbl.text = " Work Times ".localized() + ":" + ((text ?? "") ?? "")
                        print("1 - selected id:\(self?.selectedWTID ?? 0)")
                        self!.selectedWorkingTime.removeAll()
                        self!.selectedWorkingTime.append(text!!)
                    }
                }
                else{
                    print("3 - selected id:\(self?.selectedWTID ?? 0)")
                    self!.selectedWorkingTime.removeAll()
                    self!.workTimeLbl.text = " Work Times ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedWorkingTime.append(text!!)
                }
            }
            else{
                if let selectedTime  = self!.workingTimes.first(where: {$0.worktimeEn == text!!}) {
                    if let selectedID = selectedTime.id{
                        
                        self!.selectedWTID = selectedID
                        print("2 - selected id:\(self?.selectedWTID ?? 0)")
                        self!.selectedWorkingTime.removeAll()
                        self!.workTimeLbl.text = " Work Times ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedWorkingTime.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedWTID ?? 0)")
                    self!.selectedWorkingTime.removeAll()
                    self!.workTimeLbl.text = " Work Times ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedWorkingTime.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return workingTimesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300)), from: self)
    }
    @IBAction func CompanyName(_ sender: UIButton) {
        if  companiesList.count > 0 {
            let companiesNames = companiesList.map { $0.name}
            showCompaniesSelectPopover(sender: sender, companiesNames: companiesNames)
        }
        else{
            searchPresenter.getCompanies()
        }
    }
    func showCompaniesSelectPopover(sender: UIView, companiesNames: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: companiesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedCompanyName)")
        selectionMenu.setSelectedItems(items: selectedCompanyName) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedCityName.count > 0 {
                if self!.selectedCityName[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedCompany  = self!.companiesList.first(where: {$0.name == text!!}) {
                    if let selectedCompID = selectedCompany.id{
                        
                        self!.selectedCompanyID = selectedCompID
                        print("1 - selected id:\(self?.selectedCompanyID ?? 0)")
                        self!.selectedCompanyName.removeAll()
                        self!.companyLbl.text = " Company Name ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedCompanyName.append(text!!)
                    }
                    
                }
                else{
                    print("3 - selected id:\(self?.selectedCompanyID ?? 0)")
                    self!.selectedCompanyName.removeAll()
                    self!.companyLbl.text = " Company Name ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedCompanyName.append(text!!)
                }
            }
            else{
                if let selectedCompID  = self!.companiesList.first(where: {$0.name == text!!}) {
                    if let selectedCompID = selectedCompID.id{
                        
                        self!.selectedCompanyID = selectedCompID
                        print("2 - selected id:\(self?.selectedCompanyID ?? 0)")
                        self!.selectedCompanyName.removeAll()
                        self!.companyLbl.text = " Company Name ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedCompanyName.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedCompanyID ?? 0)")
                    self!.selectedCompanyName.removeAll()
                    self!.companyLbl.text = " Company Name ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedCompanyName.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return companiesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300)), from: self)
    }
    @IBAction func workSectorsAction(_ sender: UIButton) {
        if  workSectorsList.count > 0 {
            let workSectorNames = L102Language.isRTL ? workSectorsList.map { $0.nameAr} : workSectorsList.map { $0.nameEn }
            showWorkSectorsSelectPopover(sender: sender, workSectorNames: workSectorNames)
        }
        else{
            searchPresenter.getWorkSectors()
        }
    }
    @IBAction func AgeAction(_ sender: Any) {
        
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                DispatchQueue.main.async {
                    if let controller = mainStoryBoard.instantiateViewController(withIdentifier: "AgeViewCtrl") as? AgeVCtrl {
                        controller.delegate = self
                        self.present(controller, animated: true, completion: nil)
                    }
                }
    }
    func showWorkSectorsSelectPopover(sender: UIView, workSectorNames: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: workSectorNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedSector)")
        selectionMenu.setSelectedItems(items: selectedSector) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedSector.count > 0 {
                if self!.selectedSector[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedTime  = self!.workSectorsList.first(where: {$0.nameAr == text!!}) {
                    if let selectedID = selectedTime.id{
                        self!.selectedSectorID = selectedID
                        print("1 - selected id:\(self?.selectedSectorID ?? 0)")
                        self!.selectedSector.removeAll()
                        self!.workSectorLbl.text = " Work Sectors ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedSector.append(text!!)
                    }
                }
                else{
                    print("3 - selected id:\(self?.selectedSectorID ?? 0)")
                    self!.selectedSector.removeAll()
                    self!.workSectorLbl.text = " Work Sectors ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedSector.append(text!!)
                }
            }
            else{
                if let selectedTime  = self!.workSectorsList.first(where: {$0.nameEn == text!!}) {
                    if let selectedID = selectedTime.id{
                        
                        self!.selectedSectorID = selectedID
                        print("2 - selected id:\(self?.selectedSectorID ?? 0)")
                        self!.selectedSector.removeAll()
                        self!.workSectorLbl.text = " Work Sectors ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedSector.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedSectorID ?? 0)")
                    self!.selectedSector.removeAll()
                    self!.workSectorLbl.text = " Work Sectors ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedSector.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return workSectorNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300) ), from: self)
    }
    @IBAction func disabilityTypesAction(_ sender: UIButton) {
        if  disabilityList.count > 0 {
            let Names = L102Language.isRTL ? disabilityList.map { $0.nameAr} : disabilityList.map { $0.nameEn }
            showDisabilitySelectPopover(sender: sender, Names: Names)
        }
        else{
            searchPresenter.getDisabilitTypes()
        }
    }
    func showDisabilitySelectPopover(sender: UIView, Names: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedDisability)")
        selectionMenu.setSelectedItems(items: selectedDisability) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedDisability.count > 0 {
                if self!.selectedDisability[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedItem  = self!.disabilityList.first(where: {$0.nameAr == text!!}) {
                    if let selectedID = selectedItem.id{
                        self!.selectedDisabilityID = selectedID
                        print("1 - selected id:\(self?.selectedDisabilityID ?? 0)")
                        self!.selectedDisability.removeAll()
                        self!.disabilityLbl.text = " Disability ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedDisability.append(text!!)
                    }
                }
                else{
                    print("3 - selected id:\(self?.selectedDisabilityID ?? 0)")
                    self!.selectedDisability.removeAll()
                    self!.disabilityLbl.text = " Disability ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedDisability.append(text!!)
                }
            }
            else{
                if let selectedItem  = self!.disabilityList.first(where: {$0.nameEn == text!!}) {
                    if let selectedID = selectedItem.id{
                        
                        self!.selectedDisabilityID = selectedID
                        print("2 - selected id:\(self?.selectedDisabilityID ?? 0)")
                        self!.selectedDisability.removeAll()
                        self!.disabilityLbl.text = " Disability ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedDisability.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedDisabilityID ?? 0)")
                    self!.selectedDisability.removeAll()
                    self!.disabilityLbl.text = " Disability ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedDisability.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return Names.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300) ), from: self)
    }
    
    @IBAction func experiencesAction(_ sender: UIButton) {
        if  experienceList.count > 0 {
            let Names = L102Language.isRTL ? experienceList.map { $0.nameAr} : experienceList.map { $0.nameEn }
            showExperienceSelectPopover(sender: sender, Names: Names)
        }
        else{
            searchPresenter.getExperiences()
        }
    }
    func showExperienceSelectPopover(sender: UIView, Names: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedExperience)")
        selectionMenu.setSelectedItems(items: selectedExperience) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedExperience.count > 0 {
                if self!.selectedExperience[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedItem  = self!.experienceList.first(where: {$0.nameAr == text!!}) {
                    if let selectedID = selectedItem.id{
                        self!.selectedExperienceID = selectedID
                        print("1 - selected id:\(self?.selectedExperienceID ?? 0)")
                        self!.selectedExperience.removeAll()
                        self!.experienceLbl.text = " Experience ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedExperience.append(text!!)
                    }
                }
                else{
                    print("3 - selected id:\(self?.selectedExperienceID ?? 0)")
                    self!.selectedExperience.removeAll()
                    self!.experienceLbl.text = " Experience ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedExperience.append(text!!)
                }
            }
            else{
                if let selectedItem  = self!.experienceList.first(where: {$0.nameEn == text!!}) {
                    if let selectedID = selectedItem.id{
                        
                        self!.selectedExperienceID = selectedID
                        print("2 - selected id:\(self?.selectedExperienceID ?? 0)")
                        self!.selectedExperience.removeAll()
                        self!.experienceLbl.text = " Experience ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedExperience.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedExperienceID ?? 0)")
                    self!.selectedExperience.removeAll()
                    self!.experienceLbl.text = " Experience ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedExperience.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return Names.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300) ), from: self)
    }
    
    @IBAction func specialityAction(_ sender: UIButton) {
        if  specialityList.count > 0 {
            let Names = L102Language.isRTL ? specialityList.map { $0.nameAr} : specialityList.map { $0.nameEn }
            showSpecialitySelectPopover(sender: sender, Names: Names)
        }
        else{
            searchPresenter.getSpecialities()
        }
    }
    func showSpecialitySelectPopover(sender: UIView, Names: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedSpeciality)")
        selectionMenu.setSelectedItems(items: selectedSpeciality) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedSpeciality.count > 0 {
                if self!.selectedSpeciality[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedItem  = self!.specialityList.first(where: {$0.nameAr == text!!}) {
                    if let selectedID = selectedItem.id{
                        self!.selectedSpecialityID = selectedID
                        print("1 - selected id:\(self?.selectedSpecialityID ?? 0)")
                        self!.selectedSpeciality.removeAll()
                        self!.specialityLbl.text = " Speciality ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedSpeciality.append(text!!)
                    }
                }
                else{
                    print("3 - selected id:\(self?.selectedSpecialityID ?? 0)")
                    self!.selectedSpeciality.removeAll()
                    self!.specialityLbl.text = " Speciality ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedSpeciality.append(text!!)
                }
            }
            else{
                if let selectedItem  = self!.specialityList.first(where: {$0.nameEn == text!!}) {
                    if let selectedID = selectedItem.id{
                        
                        self!.selectedSpecialityID = selectedID
                        print("2 - selected id:\(self?.selectedSpecialityID ?? 0)")
                        self!.selectedSpeciality.removeAll()
                        self!.specialityLbl.text = " Speciality ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedSpeciality.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedSpecialityID ?? 0)")
                    self!.selectedSpeciality.removeAll()
                    self!.specialityLbl.text = " Speciality ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedSpeciality.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return Names.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300) ), from: self)
    }
    @IBAction func genderAction(_ sender: UIButton) {
        showGenderSelectPopover(sender: sender, Names: genderList)
    }
    func showGenderSelectPopover(sender: UIView, Names: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: Names, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        selectionMenu.setSelectedItems(items: selectedGender) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedGender.count > 0 {
                if self!.selectedGender[0] == text!! {
                    return
                }
            }
                        self!.selectedGenderIndex = index + 1
                        self!.selectedGender.removeAll()
                        self!.genderLbl.text = " Gender ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedGender.append(text!!)
                    
            self!.page = 1
            self!.loadJobsData()
        }
        
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        }
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300) ), from: self)
    }
    func filterByType(_ sender:UIButton){
        
        if let jobsTypes = jobsTypesResponse?.jobsTypes {
            let jobsTypesNames = jobsTypes.map { $0.name}
            let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: jobsTypesNames, cellType: .basic) { (cell, name, indexPath) in
                
                cell.textLabel?.text = name
                cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            }
            selectionMenu.setSelectedItems(items: selectedTypeName) { [weak self] (text, index, selected, selectedList) in
                if self!.selectedTypeName.count > 0 {
                    if self!.selectedTypeName[0] == text!! {
                        return
                    }
                }
                if L102Language.isRTL {
                    if let selectedType  = jobsTypes.first(where: {$0.name == text!!}) {
                        if let selectedTypeID = selectedType.id{
                            
                            self!.selectedTypeId = selectedTypeID
                            print("1 - selected id:\(self?.selectedTypeId ?? 0)")
                            self!.selectedTypeName.removeAll()
                            self!.jobTypeLbl.text = " Job Type ".localized() + ":" + ((text ?? "") ?? "")
                            self!.selectedTypeName.append(text!!)
                        }
                        
                    }
                    else{
                        print("3 - selected id:\(self?.selectedTypeId ?? 0)")
                        self!.selectedTypeName.removeAll()
                        self!.jobTypeLbl.text = " Job Type ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedTypeName.append(text!!)
                    }
                }
                else{
                    if let selectedType  = jobsTypes.first(where: {$0.name == text!!}) {
                        if let selectedTypeID = selectedType.id{
                            self!.selectedTypeId = selectedTypeID
                            print("2 - selected id:\(self?.selectedTypeId ?? 0)")
                            self!.selectedTypeName.removeAll()
                            self!.jobTypeLbl.text = " Job Type ".localized() + ":" + ((text ?? "") ?? "")
                            self!.selectedTypeName.append(text!!)
                        }
                    }
                    else{
                        print("4- selected id:\(self?.selectedTypeId ?? 0)")
                        self!.selectedTypeName.removeAll()
                        self!.jobTypeLbl.text = " Job Type ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedTypeName.append(text!!)
                    }
                }
                self!.page = 1
                self!.loadJobsData()
            }
            
            // search bar
            
            selectionMenu.showSearchBar { (searchText) -> ([String?]) in
                return jobsTypesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            }
            if L102Language.isRTL {
                selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
            }
            else{
                selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
            }
            //selectionMenu.searchBar?.placeholder = "Search here".localized()
            // show empty data label - provide custom text (if needed)
            
            selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
            // cell selection style
            selectionMenu.cellSelectionStyle = .tickmark
            if #available(iOS 13.0, *) {
                selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
                selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
            }
            
            selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300)), from: self)
        }
    }
    func showCitySelectPopover(sender: UIView, citiesNames: [String?]) {
        let selectionMenu = RSSelectionMenu( selectionStyle: .single, dataSource: citiesNames, cellType: .basic) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            cell.textLabel?.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
        }
        print("selected name : \(selectedCityName)")
        selectionMenu.setSelectedItems(items: selectedCityName) { [weak self] (text, index, selected, selectedList) in
            if self!.selectedCityName.count > 0 {
                if self!.selectedCityName[0] == text!! {
                    return
                }
            }
            if L102Language.isRTL {
                if let selectedCity  = self!.cityList.first(where: {$0.cityAr == text!!}) {
                    if let selectedCCityID = selectedCity.id{
                        
                        self!.selectedCityId = selectedCCityID
                        print("1 - selected id:\(self?.selectedCityId ?? 0)")
                        self!.selectedCityName.removeAll()
                        self!.cityLbl.text = " City ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedCityName.append(text!!)
                    }
                    
                }
                else{
                    print("3 - selected id:\(self?.selectedCityId ?? 0)")
                    self!.selectedCityName.removeAll()
                    self!.cityLbl.text = " City ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedCityName.append(text!!)
                }
            }
            else{
                if let selectedCity  = self!.cityList.first(where: {$0.cityEn == text!!}) {
                    if let selectedCCityID = selectedCity.id{
                        
                        self!.selectedCityId = selectedCCityID
                        print("2 - selected id:\(self?.selectedCityId ?? 0)")
                        self!.selectedCityName.removeAll()
                        self!.cityLbl.text = " City ".localized() + ":" + ((text ?? "") ?? "")
                        self!.selectedCityName.append(text!!)
                    }
                    
                }
                else{
                    print("4- selected id:\(self?.selectedCityId ?? 0)")
                    self!.selectedCityName.removeAll()
                    self!.cityLbl.text = " City ".localized() + ":" + ((text ?? "") ?? "")
                    self!.selectedCityName.append(text!!)
                }
            }
            self!.page = 1
            self!.loadJobsData()
        }
        // search bar
        
        selectionMenu.showSearchBar { (searchText) -> ([String?]) in
            return citiesNames.filter({ ($0!.lowercased().contains(searchText.lowercased())) })
            //return self?.dataArray.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        if L102Language.isRTL {
            selectionMenu.searchBar?.semanticContentAttribute = .forceRightToLeft
        }
        else{
            selectionMenu.searchBar?.semanticContentAttribute = .forceLeftToRight
        }
        //selectionMenu.searchBar?.placeholder = "Search here".localized()
        // show empty data label - provide custom text (if needed)
        
        selectionMenu.showEmptyDataLabel(text:  "No Records Found".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!])
        // cell selection style
        selectionMenu.cellSelectionStyle = .tickmark
        if #available(iOS 13.0, *) {
            selectionMenu.searchBar?.searchTextField.font = UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15)!
            selectionMenu.searchBar?.searchTextField.attributedPlaceholder =  NSAttributedString(string: "Search".localized(),
                                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        } else {
            // Fallback on earlier versions
        }
        
        // show as popover
        // specify popover size if needed
        // size = nil (auto adjust size)
        selectionMenu.show(style: .popover(sourceView: sender, size: CGSize(width: 250, height: 300)), from: self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    @IBAction func searchBtn(_ sender: Any) {
        self.jobToLoadType = .Search
        self.view.endEditing(true)
        self.page = 1
        self.loadJobsData()
    }
    func loadJobsData(){
        if self.searchText?.count == 0 && self.selectedTypeId == nil && self.selectedCityId == nil && self.selectedWTID == nil && self.selectedSectorID == nil && self.selectedCompanyID == nil && self.selectedDisabilityID == nil && self.selectedExperienceID == nil && self.selectedSpecialityID == nil && self.selectedGenderIndex == nil && self.ageFrom == nil {
            self.showErrorMessage(message: "EnterSearch".localized())
        }
        else{
            self.HomeJobsTV.windless
                .apply {
                    $0.beginTime = 0
                    $0.pauseDuration = 1
                    $0.duration = 3
                    $0.animationLayerOpacity = 0.8
                    $0.direction = .left
                    
                }
                .start()
            
            self.emptyView.alpha = 0
            self.jobsPresenter.getJobs(title: self.searchText, jobTypeID: self.selectedTypeId, page: self.page, jobCityID: self.selectedCityId, order: self.order, worktime_id: self.selectedWTID, WorkSectorID: self.selectedSectorID, companyID: self.selectedCompanyID, disabilityID: self.selectedDisabilityID, experienceID: self.selectedExperienceID, specialityID: self.selectedSpecialityID, genderID: self.selectedGenderIndex, ageFrom: self.ageFrom, ageTo: self.ageTo)
        }
    }
    func initPullToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        HomeJobsTV?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.loadJobsData()
        }, loadingView: loadingView)
        self.HomeJobsTV?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        self.HomeJobsTV?.dg_setPullToRefreshBackgroundColor(HomeJobsTV.backgroundColor!)
    }
    
    func initLoadMoreToRefresh()
    {
        self.HomeJobsTV?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.HomeJobsTV?.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return self.page != self.jobsResponse?.last_page
        }
        self.HomeJobsTV?.addInfiniteScroll { (tableView) -> Void in
            
            if self.page == self.jobsResponse?.last_page {
                //ending paging
                self.HomeJobsTV.finishInfiniteScroll()
            }else {
                self.page += 1
                self.loadJobsData()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if jobsResponse == nil {
            return itemCountForWindless
        }else {
            return (jobsResponse?.data?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HomeJobsTV.dequeueReusableCell(withIdentifier: "HomeJobsCell", for: indexPath) as! HomeJobsTVCell
        if jobsResponse == nil {
            // self.HomeJobsTV.alpha = 0
        }
        else{
            cell.whiteContainerView.layer.cornerRadius = 10
            cell.ApplyJobBtn.layer.cornerRadius = 12.3
            cell.shareBtn.layer.cornerRadius = 12.3
            cell.shareBtn.tag = indexPath.row
            cell.shareBtn.setTitle("Share".localized(), for: .normal)
            cell.shareBtn.setTitle("Share".localized(), for: .selected)
            
            cell.shareBtn.backgroundColor = UIColor(hexString: "#161650")
            cell.shareBtn.addTarget(self, action: #selector(shareJob2(_:)), for: .touchUpInside)
            if (jobsResponse?.data!.count)! > 0 {
                
                let item = jobsResponse?.data![indexPath.row]
                if let disabilityEnabled = item?.DisabilityEnabled {
                    if disabilityEnabled == 1 {
                        cell.disability_ic.isHidden = false
                    }
                    else{
                        cell.disability_ic.isHidden = true
                    }
                }
                else{
                    cell.disability_ic.isHidden = true
                }
                if let expired_status = item?.jobExpired  {
                    if expired_status {
                        cell.ApplyJobBtn.isHidden = true
                        cell.shareBtn.isHidden = true
                    }
                    else{
                        cell.ApplyJobBtn.isHidden = false
                        cell.shareBtn.isHidden = false
                    }
                }
                else{
                    cell.ApplyJobBtn.isHidden = false
                    cell.shareBtn.isHidden = false
                }
                if let appliedForJobStatus = item?.appliedForJob {
                    if appliedForJobStatus {
                        cell.ApplyJobBtn.setTitle("Applied".localized(), for: .normal)
                        cell.ApplyJobBtn.setTitle("Applied".localized(), for: .selected)
                        
                        //cell.ApplyJobBtn.customLayerGradient(color1: "#af8dc0", color2: "#9e75b2", cornerRadius: 12.3)
                        cell.ApplyJobBtn.backgroundColor = UIColor(hexString: "#00ccff")
                    }
                    else{
                        cell.ApplyJobBtn.setTitle("Apply Now".localized(), for: .normal)
                        cell.ApplyJobBtn.setTitle("Apply Now".localized(), for: .selected)
                        
                        cell.ApplyJobBtn.backgroundColor = UIColor(hexString: "#161650")
                        //cell.ApplyJobBtn.customLayerGradient(color1: "#00afdf", color2: "#0abced", cornerRadius: 12.3)
                    }
                }
                cell.jobTitle.text = item?.title ?? ""
                if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                    if subscribedUser {
                        cell.JobImg.kf.indicatorType = .activity
                        let url = URL(string: (item?.imagePath ?? "")!)
                        
                        let processor = ResizingImageProcessor(referenceSize: CGSize(width: 22, height: 22))
                        
                        cell.JobImg.kf.setImage(
                            with: url,
                            placeholder: UIImage(named: "company_ic_large"),
                            options: [
                                
                                .processor(processor),
                                .scaleFactor(UIScreen.main.scale),
                                .transition(.fade(1)),
                                .cacheOriginalImage,
                                
                            ], completionHandler:
                                {
                                    result in
                                    switch result {
                                    case .success(let value):
                                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                    case .failure(let error):
                                        print("Job failed: \(error.localizedDescription)")
                                    }
                                })
                        
                    }
                }
                if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                    if subscribedUser {
                        cell.comNameHeightConstraint.constant = 20
                        cell.JobCompany.text = item?.companyName
                    }
                }
                cell.JobType.text = "\(item?.job_type_name ?? "" ) - \(item?.workTimeText ?? "")"
                if let text_color = item?.job_type_color {
                    cell.JobType.textColor = UIColor(hexString: text_color)
                }
                else{
                    cell.JobType.textColor = UIColor(hexString: "#9E75B2")
                }
                cell.JobLocation.text = item?.city
                
                if let creationDate = item?.created_at! {
                    cell.JobPostedTime.text = creationDate.getFormattedDate()
                }
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    @objc func shareJob(_ sender: UIButton){
        let jobIndex = sender.tag
        self.spinnerView = UIViewController.displaySpinner(onView: self.view)
        if (jobsResponse?.data!.count)! > 0 {
            
            let item = jobsResponse?.data![jobIndex]
            //let urlShare = "https://jobplus.page.link/job?id=\(item?.id ?? 0)"
            var components = URLComponents()
            components.scheme = "https"
            //"www.jobplus.biz/parttime/public"
            components.host = "jobplus.page.link"
            components.path = "/job"
            let jobQueryItem = URLQueryItem(name: "id", value: "\(item?.id ?? 0)")
            components.queryItems = [jobQueryItem]
            
            guard let linkParameter = components.url else {return}
            print("I am sharing link \(linkParameter.absoluteURL)")
            
            guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://jobplus.page.link") else{
                print("couldnot create link")
                return
            }
            shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            shareLink.socialMetaTagParameters?.title = item?.title ?? ""
            shareLink.socialMetaTagParameters?.imageURL = URL(string: "https://www.jobplus.biz/parttime/public/img/landing-page/icon/JP_Logo1.png")
            if let myBundleID = Bundle.main.bundleIdentifier {
                shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleID)
            }
            shareLink.iOSParameters?.appStoreID = "1493332366"
            shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.adroyts.jobplus")
            guard let longURL = shareLink.url else {return}
            print("longUrl \(longURL.absoluteString)")
            shareLink.shorten { (url, warnings, error) in
                guard error == nil else {
                    print(error!)
                    return
                }
                if let url = url {
                    print("shorten url \(url.absoluteString)")
                    
                    self.showShareActivity(msg: "\(item?.title ?? "") \n\(item?.job_type_name ?? "")", image: UIImage(named: "jpLogo_splash"), url: url.absoluteString, sourceRect: nil)
                }
            }
            
        }
        
    }
    @objc func shareJob2(_ sender: UIButton){
        let jobIndex = sender.tag
        self.spinnerView = UIViewController.displaySpinner(onView: self.view)
        if (jobsResponse?.data!.count)! > 0 {
            
            let item = jobsResponse?.data![jobIndex]
            guard let link = URL(string: "https://jobplus.page.link/job?id=\(item?.id ?? 0)") else { return }
            let dynamicLinksDomainURIPrefix = "https://jobplus.page.link"
            let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
            
            linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.adroyts.jobplus")
            linkBuilder?.iOSParameters?.appStoreID = "1493332366"
            
            linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.adroyts.jobplus")
            
            
            linkBuilder?.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            linkBuilder?.socialMetaTagParameters?.title = item?.title ?? ""
            linkBuilder?.socialMetaTagParameters?.descriptionText = item?.job_type_name ?? ""
            linkBuilder?.socialMetaTagParameters?.imageURL = URL(string: "https://www.jobplus.biz/parttime/public/img/landing-page/icon/JP_Logo1.png")
            
            guard let longDynamicLink = linkBuilder?.url else { return }
            print("The long URL is: \(longDynamicLink)")
            linkBuilder?.options = DynamicLinkComponentsOptions()
            linkBuilder?.shorten() { url, warnings, error in
                guard let url = url, error == nil else { if let sv = self.spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
                self.showErrorMessage(message: "Failed to generate share link, please try again later.")
                print("shorten error \(String(describing: error))"); return }
                print("The short URL is: \(url)")
                self.showShareActivity(msg: "\(item?.title ?? "") \n\(item?.job_type_name ?? "")", image: UIImage(named: "jpLogo_splash"), url: url.absoluteString, sourceRect: nil)
            }
            
        }
    }
    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems:  [msg ?? "", url ?? ""], applicationActivities: nil)
        activityViewController.setValue("Job Plus", forKey: "Subject")
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        activityViewController.popoverPresentationController?.sourceView = self.view;
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
        
        self.present(activityViewController, animated: true) {
            if let sv = self.spinnerView{
                UIViewController.removeSpinner(spinner: sv)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
        if let item = jobsResponse?.data![indexPath.row] {
            
            if let expired_status = item.jobExpired {
                if expired_status {
                    cell.alpha = 0.5
                }
                else{
                    cell.alpha = 1
                }
                
            }
            else{
                cell.alpha = 1
            }
        }
    }
    //&& !(jobsResponse?.data![indexPath.row].jobExpired)!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if jobsResponse?.data != nil  {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                

                var selectedItem:jobItem2ForAds?

                if let i = jobsResponse?.data![indexPath.row] {
                    selectedItem = jobItem2ForAds(id:  i.id, title: i.title ?? "", companyName: i.companyName, imagePath: i.imagePath ?? "", city: i.city, job_type_id: i.job_type_id ?? 0, job_type_name: i.job_type_name ?? "", job_type_color: i.job_type_color ?? "", created_at: i.created_at ?? "", appliedForJob: i.appliedForJob ?? false , expireDate: i.expireDate ?? "", job_status: i.job_status ?? 0 , jobExpired: i.jobExpired ?? false, workTimeText: i.workTimeText ?? "", DisabilityEnabled: i.DisabilityEnabled ?? 0)
                }
               
             
                viewController.jobItem = selectedItem
                
                if let navigator = navigationController {
                    UserManager.sharedInstance.jobIndexAppliedOrCanceled = indexPath
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension searchVCtrl :JobsTypesView {
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        itemCountForWindless = 0
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess(jobsResponse: JobsTypesResponse) {
        self.jobsTypesResponse = jobsResponse
        self.filterByType(self.jobTypeBtn!)
    }
    
    func onError(message: String) {
        self.showErrorMessage(message:message)
        self.HomeJobsTV.windless.end()
        itemCountForWindless = 0
        self.HomeJobsTV.dg_stopLoading()
        if let count = jobsResponse?.data?.count {
            if count > 0 {
                jobsResponse?.data? = []
            }
        }
        self.HomeJobsTV.reloadData()
    }
    
    
}
extension searchVCtrl :UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if(string == " " )
        //        {
        //            self.showErrorMessage(message: "enterSearchText".localized())
        //            return false
        //        }
        searchText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //        if textField == searchTextField {
        //            self.jobToLoadType = .Search
        self.view.endEditing(true)
        self.jobToLoadType = .Search
        self.view.endEditing(true)
        self.page = 1
        self.loadJobsData()
        return true
    }
}
extension searchVCtrl : JobsView {
    func onSuccessNextLinkCall(jobsResponse: JobListResponse) {
        
        self.HomeJobsTV.finishInfiniteScroll()
        self.HomeJobsTV.windless.end()
        //itemCountForWindless = 0
        self.HomeJobsTV.dg_stopLoading()
        for item in jobsResponse.data! {
            self.jobsResponse?.data?.append(item)
        }
        self.emptyView.alpha = 0
        self.HomeJobsTV.alpha = 1
        //self.jobsResponse?.meta = jobsResponse.meta
        self.HomeJobsTV.alpha = 1
        if  self.jobsResponse?.data?.count == 0 {
            self.emptyView.alpha = 1
            self.HomeJobsTV.alpha = 0
        }
        else{
            self.emptyView.alpha = 0
            self.HomeJobsTV.alpha = 1
            for index in 0..<self.jobsResponse!.data!.count {
                if let expired_date = self.jobsResponse!.data?[index].expireDate {
                    let staus = expired_date.checkExiration()
                    self.jobsResponse!.data?[index].jobExpired = staus
                }
                else{
                    self.jobsResponse!.data?[index].jobExpired = false
                }
                //                let staus = self.jobsResponse!.data?[index].expireDate?.checkExiration()
                //                self.jobsResponse!.data?[index].jobExpired = staus
            }
        }
        
        
        HomeJobsTV.reloadData()
        
        
    }
    
    func onSucess(jobsResponse: JobListResponse) {
        self.HomeJobsTV.windless.end()
        //itemCountForWindless = 0
        self.HomeJobsTV.dg_stopLoading()
        self.jobsResponse = jobsResponse
        if let total = jobsResponse.total {
            if total == 0 {
                self.jobsCountLbl.text = ""
            }
            else if total > 1 {
                self.jobsCountLbl.text = String(total) + " " + "Jobs".localized()
            }
            else{
                self.jobsCountLbl.text = String(total) + " " + "Job".localized()
            }
        }
        self.HomeJobsTV.alpha = 1
        if let userDeleted = jobsResponse.userDeleted {
            if userDeleted == 1 {
                self.showErrorMessage(message: "Sorry, your account has been suspended, you can contact our support team".localized())
                clearDataAndStart()
            }
        }
        if let jobs = jobsResponse.data , jobs.count == 0 {
            
            self.emptyView.alpha = 1
            self.HomeJobsTV.alpha = 0
        }
        else{
            if let subscribed = jobsResponse.subscribedUser {
                if subscribed {
                    UserManager.sharedInstance.subscribedUser = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                }
                else{
                    UserManager.sharedInstance.subscribedUser = false
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                }
            }
            
            self.emptyView.alpha = 0
            self.HomeJobsTV.alpha = 1
            for index in 0..<self.jobsResponse!.data!.count {
                if let expired_date = self.jobsResponse!.data?[index].expireDate {
                    let staus = expired_date.checkExiration()
                    self.jobsResponse!.data?[index].jobExpired = staus
                }
                else {
                    self.jobsResponse!.data?[index].jobExpired = false
                }
            }
        }
        self.HomeJobsTV.reloadData()
    }
    
}

extension searchVCtrl:SearchView{
    func onSuccessGetSpecialities(specialities: [specialityItem]) {
        self.specialityList.removeAll()
        self.specialityList.append(specialityItem(id: -1, nameEn: "ALL", nameAr: "الكل", selected: false))
        self.specialityList.append(contentsOf: specialities)
        self.specialityAction(specialityBtn)
    }
    
    func onSuccessGetExperiences(experience: [experienceItem]) {
        self.experienceList.removeAll()
        self.experienceList.append(experienceItem(id: -1, nameEn: "ALL", nameAr: "الكل", selected: false))
        self.experienceList.append(contentsOf: experience)
        self.experiencesAction(experienceBtn)
    }
    
    func onSuccessGetDiabilities(disabilities: [DisabilityTypeItem]) {
        self.disabilityList.removeAll()
        self.disabilityList.append(DisabilityTypeItem(id: -1, nameEn: "ALL", nameAr: "الكل", selected: false))
        self.disabilityList.append(contentsOf: disabilities)
        self.disabilityTypesAction(disabilityBtn)
    }
    
    func showLoading() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSuccessGetCities(countries: [countryItem]) {
        if countries.count > 0 {
            self.cityList.removeAll()
            for country in countries {
                if let cities = country.cities {
                    if cities.count > 0 {
                        for city in cities {
                            self.cityList.append(city)
                        }
                    }
                }
            }
            self.cityFilterAction(cityBtn)
        }
    }
    
    func onSuccessGetWorkingTimes(worktimes: [worktims]) {
        self.workingTimes.removeAll()
        self.workingTimes = worktimes
        self.workTimeFilterAction(workTimesBtn)
    }
    
    func onSuccessGetCompanies(companies: [companyItem]) {
        self.companiesList.removeAll()
        self.companiesList = companies
        self.CompanyName(companyBtn)
    }
    
    func onSuccessGetWorkSectors(sectors: [workSectorItem]) {
        self.workSectorsList.removeAll()
        self.workSectorsList = sectors
        self.workSectorsAction(sectorBtn)
    }
}
protocol getAge {
    func getAge(from: Int, to: Int)
}
extension searchVCtrl: getAge {
    func getAge(from: Int, to: Int) {
        self.ageFrom = from
        self.ageTo = to
        if to > 0 {
            if L102Language.isRTL {
                self.ageLbl.text = " Age ".localized() + String(to) + ":" + String(from)
            }
            else{
                self.ageLbl.text = " Age ".localized() + String(from) + ":" + String(to)
            }
        }
        else{
            self.ageLbl.text = " Age ".localized() + ":" + String(from)
        }
        self.page = 1
        self.loadJobsData()
    }
}
