//
//  ViewController.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/24/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher
import SafariServices
import Firebase
//import GoogleMobileAds
import Floaty


enum Sort {
    case Ascending
    case Descending
}

enum JobToLoad {
    case Recomended
    case Newest
    case Search
}
class HomeVCtrl: UIViewController, UITableViewDelegate, UITableViewDataSource,FloatyDelegate {
    
    let floaty = Floaty()
    var JobsAndAdsArr = [jobItem2ForAds]()
    @IBOutlet weak var notiBtn: UIButton!
    @IBOutlet weak var emptyView: EmptyView!
    @IBOutlet weak var HomeTitleLbl: UILabel!
    @IBOutlet weak var notificationsCountLbl: UILabel!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var HomeJobsTV: UITableView!
    @IBOutlet weak var JobsRecommendedLatestSwitcher: UISegmentedControl!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var homeCatsCv: UICollectionView!
    var itemCountForWindless = 8
    let notificationCountPresenter = NotificationPresenter()
    let jobsPresenter = JobsPresenter()
    let homeCatsPresenter = HomeCatsPresenter()
    let storeVersionPresenter = StoreVersionPresenter()
    var jobsResponse:JobListResponse?
    var sort = Sort.Descending
    var order = KeysAndValues.ORDERDESC
    var page = 1
    var jobToLoadType = JobToLoad.Recomended
    var spinnerView: UIView?
    let subsPresenter = SubscribtionPresenter()
    var homeCats = [catItem]()
    var refreshControl = UIRefreshControl()
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    @IBOutlet weak var reloadBtn: UIButton!
    @IBAction func openSearchAction(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchViewCtrl") as? searchVCtrl {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @IBAction func openNotificationsView(_ sender: Any) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    
    @IBAction func tapToRety(_ sender: Any) {
//        if jobToLoadType == .Search {
//            if self.searchText?.count == 0 && self.selectedTypeId == nil && self.selectedCityId == nil && self.selectedWTID == nil && !self.SelectedCityAll && !self.selectedAllTimes && !self.SelectedTypeAll {
//                self.showErrorMessage(message: "EnterSearch".localized())
//                return
//            }
//        }
        DispatchQueue.main.async {
            self.reloadBtn.isHidden = true
            
        }
        self.emptyView.alpha = 0
        self.HomeJobsTV.alpha = 1
        self.loadJobsData()
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        self.jobToLoadType = .Search
        self.view.endEditing(true)
        self.JobsRecommendedLatestSwitcher.selectedSegmentIndex = -1
        self.page = 1
        self.loadJobsData()
    }
    @IBAction func Job_cat_changed(_ sender: UISegmentedControl) {
        print("sender.selectedSegmentIndex :\(sender.selectedSegmentIndex)")
//        if UserManager.sharedInstance.getUser() == nil && sender.selectedSegmentIndex == 1 {
//            self.showErrorMessage(message: "you have to login first".localized())
//            self.JobsRecommendedLatestSwitcher.selectedSegmentIndex = 0
//            return
//        }
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.jobToLoadType = .Recomended
//            DispatchQueue.main.async {
//                self.HomeJobsTV.isHidden = false
//                self.homeCatsCv.isHidden = true
//
//                //self.floaty.removeFromSuperview()
//
//            }
        case 1:
            self.jobToLoadType = .Newest
//            DispatchQueue.main.async {
//                self.HomeJobsTV.isHidden = true
//                self.homeCatsCv.isHidden = false
//            }
        default:
            break;
        }
        
        self.page = 1
        DispatchQueue.main.async {
            self.reloadBtn.isHidden = true
        }
        
        self.loadJobsData()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if UserManager.sharedInstance.openProfile {
            UserManager.sharedInstance.openProfile = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openProfile"), object: nil)
        }
        else{
            if UserManager.sharedInstance.getUser() != nil {
                notificationCountPresenter.getNotificationCount()
            }
        }
        if UserManager.sharedInstance.openDynamicLink {
            UserManager.sharedInstance.openDynamicLink = false
            print(UserManager.sharedInstance.openDynamicLinkJobID)
            if UserManager.sharedInstance.openDynamicLinkJobID > 0 {
            self.performSegue(withIdentifier:"JobDetailsViewController", sender: self)
            }
        }
        if UserManager.sharedInstance.jobAppliedOrCanceled > 0 {
            if let indexPath = UserManager.sharedInstance.jobIndexAppliedOrCanceled {
                let cell = HomeJobsTV.cellForRow(at: indexPath) as? HomeJobsTVCell
                if UserManager.sharedInstance.jobAppliedOrCanceled == 1 {
                    if JobsAndAdsArr[indexPath.row] is jobItem2ForAds {
                        (JobsAndAdsArr[indexPath.row] as? jobItem2ForAds)?.appliedForJob = true
                        cell?.ApplyJobBtn.setTitle("Applied".localized(), for: .normal)
                        cell?.ApplyJobBtn.setTitle("Applied".localized(), for: .selected)
                        cell?.ApplyJobBtn.backgroundColor = UIColor(hexString: "#00ccff")
                    }
                }
                else{
                    if JobsAndAdsArr[indexPath.row] is jobItem2ForAds {
                        (JobsAndAdsArr[indexPath.row] as? jobItem2ForAds)?.appliedForJob = false
                        cell?.ApplyJobBtn.setTitle("Apply Now".localized(), for: .normal)
                        cell?.ApplyJobBtn.setTitle("Apply Now".localized(), for: .selected)
                        cell?.ApplyJobBtn.backgroundColor = UIColor(hexString: "#161650")
                    }
                }
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
            else{
                UserManager.sharedInstance.jobAppliedOrCanceled = 0
                UserManager.sharedInstance.jobIndexAppliedOrCanceled = nil
            }
        }
    }
    
    
    func floatyDidOpen(_ floaty: Floaty) {
      print("Floaty Did Open")
        scrollToTop()
        DispatchQueue.main.async {
            self.floaty.close()
        }
        
       
    }
    
    
    
    private func scrollToTop() {
        print("scrollToTop")
           let topRow = IndexPath(row: 0,
                                  section: 0)
           self.HomeJobsTV.scrollToRow(at: topRow,
                                      at: .top,
                                      animated: true)
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        floaty.buttonColor = #colorLiteral(red: 1, green: 0.7725490196, blue: 0.2235294118, alpha: 1)
        floaty.buttonImage = UIImage(named: "upArrow")
        floaty.fabDelegate = self
        let item = FloatyItem()
        item.buttonColor = UIColor.clear
        item.circleShadowColor = UIColor.clear
        item.titleShadowColor = UIColor.clear
        
        if L102Language.isRTL {
            Floaty.global.rtlMode = true
        }
        
        floaty.addItem(item: item)
        switch self.jobToLoadType {
        case .Newest:
            if L102Language.isRTL {
                Floaty.global.rtlMode = true
            }
           
//            self.view.addSubview(floaty)
        case .Recomended:
            print("Recomended")
        case .Search:
            print("Search")
        }
            
       
        
        
        if UserManager.sharedInstance.getUser() != nil {
            self.notiBtn.isHidden = false
        }
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search here".localized(),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14)!])
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        notificationsCountLbl.layer.cornerRadius = 4
        searchBarView.layer.cornerRadius = 16.0
        searchBarView.layer.shadowColor = UIColor.black.cgColor
        searchBarView.layer.shadowOpacity = 0.2
        searchBarView.layer.shadowOffset = .zero
        searchBarView.layer.shadowRadius = 10
        
        HomeJobsTV.layer.cornerRadius = 10.0
        
        JobsRecommendedLatestSwitcher.layer.cornerRadius = 16
        JobsRecommendedLatestSwitcher.layer.borderColor = UIColor.clear.cgColor
        JobsRecommendedLatestSwitcher.layer.borderWidth = 1
        JobsRecommendedLatestSwitcher.layer.masksToBounds = true
        //        searchBarView.customLayerGradient(color1: "#af8dc0", color2: "#9e75b2" , cornerRadius: 10.0)
        JobsRecommendedLatestSwitcher.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!, NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.selected)
        JobsRecommendedLatestSwitcher.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!, NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
        menuBtn.setImage(UIImage(named: NSLocalizedString("menu_new", comment: "")), for: .normal)
        menuBtn.setImage(UIImage(named: NSLocalizedString("menu_new", comment: "")), for: .selected)
        if L102Language.isRTL {
        menuBtn.setImage(UIImage(named: NSLocalizedString("menu_ar_new", comment: "")), for: .normal)
        menuBtn.setImage(UIImage(named: NSLocalizedString("menu_ar_new", comment: "")), for: .selected)
        }
        
        HomeJobsTV.backgroundColor = .clear
        setupSideMenu()
        updateMenus()
        initPullToRefresh()
        initLoadMoreToRefresh()
        homeCatsPresenter.attachView(homeCatsView: self)
        jobsPresenter.attachView(view:self)
        subsPresenter.attachView(view: self)
        notificationCountPresenter.attachView(view: self)
        storeVersionPresenter.attachView(view: self)
        self.loadJobsData()
        storeVersionPresenter.getStoreVersion()
        emptyView.msgLbl.text = "There are no jobs".localized()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadJobsData),
                                               name: NSNotification.Name(rawValue: "reloadHome"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openProfile),
                                               name: NSNotification.Name(rawValue: "openProfile"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.unauthRequest),
                                               name: NSNotification.Name(rawValue: "unauthRequest"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openNotifications),
                                               name: NSNotification.Name(rawValue: "openNotifications"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openAppliedJobs),
                                               name: NSNotification.Name(rawValue: "openAppliedJobs"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openRecommendedJobs),
                                               name: NSNotification.Name(rawValue: "openRecommendedJobs"),
                                               object: nil)
       
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openDiscounts),
                                               name: NSNotification.Name(rawValue: "opnDiscounts"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openNews),
                                               name: NSNotification.Name(rawValue: "openNews"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openMessages),
                                               name: NSNotification.Name(rawValue: "openMessages"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openArabClicks),
                                               name: NSNotification.Name(rawValue: "arabClicks"),
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.opensubscribtions),
                                               name: NSNotification.Name(rawValue: "opensubscribtions"),
                                               object: nil)

        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openSettings),
                                               name: NSNotification.Name(rawValue: "openSettings"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openServerNotification),
                                               name: NSNotification.Name(rawValue: "openServerNotification"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadNotiCount),
                                               name: NSNotification.Name(rawValue: "reloadNotiCount"),
                                               object: nil)
        if UserManager.sharedInstance.userRegisterForFirstTime == 1 {
            UserManager.sharedInstance.userRegisterForFirstTime = 0
            if UserManager.sharedInstance.getUser() != nil {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVCtlID") as? NewProfileVCtrl {
                    
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: false)
                    }
                }
            }
        }
        if let pendingTransactioniD = UserDefaults.standard.string(forKey: "pendingTransactioniD") {
            if let TransactionDate = UserDefaults.standard.string(forKey: "TransactionDate"){
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
                dateFormatterGet.timeZone = NSTimeZone.local
                if L102Language.isRTL {
                    dateFormatterGet.locale = Locale(identifier: "ar_EG")
                }
                else{
                    dateFormatterGet.locale = Locale(identifier: "en_US")
                }

                guard let TransactionDateObj = dateFormatterGet.date(from:  TransactionDate) else {return}
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year], from: TransactionDateObj, to: Date())
                print(components.year!)
                if components.year! <= 1 {
                    self.subsPresenter.addIAPTransaction(transaction_id: pendingTransactioniD, packageID: UserDefaults.standard.integer(forKey: "selectedPackage"), paymentBrand: 4)
                }
            }
        }
    }
    @objc func reloadNotiCount(){
        if UserManager.sharedInstance.getUser() != nil {
            notificationCountPresenter.getNotificationCount()
        }
    }
    @objc func unauthRequest(){
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                viewController.modalPresentationStyle = .overCurrentContext
                viewController.from = "401"
                self.present(viewController, animated: true, completion: nil)
        }
    }
    @objc func opensubscribtions(){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "newSubscriptionsViewController") as? newSubscribtionVCtrl {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }

    @objc func reloadJobsData(){
        if let navigator = navigationController {
            navigator.popToRootViewController(animated: false)
        }
        if let dataCount = jobsResponse?.data?.count {
            if dataCount > 0 {
                self.HomeJobsTV.setContentOffset(.zero, animated: true)
            }
        }
        self.searchTextField.text = ""
        self.page = 1
        self.HomeJobsTV.windless
            .apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 3
                $0.animationLayerOpacity = 0.8
                $0.direction = .left
                
            }
            .start()
        DispatchQueue.main.async {
            self.reloadBtn.isHidden = true
        }
        
        self.loadJobsData()
    }
    @objc func openProfile(){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVCtlID") as? NewProfileVCtrl {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
    }
    
    @objc func openServerNotification(_ notification: NSNotification){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    if let type = notification.userInfo?["type"] {
                        let type_str = type as! String
                        viewController.notification_type = Int(type_str)!
                    }
                    else{
                        viewController.notification_type = 0
                    }
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
    }
    @objc func openAppliedJobs(){
        if UserManager.sharedInstance.getUser() != nil {
            self.pushViewController(viewControllerName: "AppliedJobsViewController", storyBoardName: "Jobs")
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    @objc func openRecommendedJobs(){
        if UserManager.sharedInstance.getUser() != nil {
            self.pushViewController(viewControllerName: "RecommendedVCtrl", storyBoardName: "Main")
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    @objc func openDiscounts(){
        if UserManager.sharedInstance.getUser() != nil {
            
            if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                if subscribedUser {
                    if L102Language.isRTL {
                        if let url = URL(string: WebService.discountsURLAr ){
                            let vc = SFSafariViewController(url: url)
                            
                            present(vc, animated: true)
                        }
                    }
                    else{
                        if let url = URL(string: WebService.discounstURLEn ){
                            let vc = SFSafariViewController(url: url)
                            present(vc, animated: true)
                        }
                    }
                }else{
                    
                    if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                            viewController.modalPresentationStyle = .overCurrentContext
                        viewController.from = "profile"
                        if let msg = UserManager.sharedInstance.NotSubscribedMeassage{
                            print("msg : \(msg)")
                            viewController.titleMsg = msg
                        }
                            self.present(viewController, animated: true, completion: nil)
                    }
                }
                
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    @objc func openNews(){
        if UserManager.sharedInstance.getUser() != nil {
            if L102Language.isRTL {
                if let url = URL(string: WebService.newsUrlAr ){
                    let vc = SFSafariViewController(url: url)
                    
                    present(vc, animated: true)
                }
            }
            else{
                if let url = URL(string: WebService.newsUrlEn ){
                    let vc = SFSafariViewController(url: url)
                    present(vc, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    
    
    
    @objc func openArabClicks(){
        if UserManager.sharedInstance.getUser() != nil {
            if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                if subscribedUser {
                    if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "arabClicksVC") as? arabClicksVCtrl {
                        
                        if let navigator = navigationController {
                            navigator.pushViewController(viewController, animated: false)
                        }
                    }
                }else{
                    
                    if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CongratulationsViewController") as? CongratulationsViewController {
                            viewController.modalPresentationStyle = .overCurrentContext
                        viewController.from = "profile"
                        if let msg = UserManager.sharedInstance.NotSubscribedMeassage{
                            print("msg : \(msg)")
                            viewController.titleMsg = msg
                        }
                            self.present(viewController, animated: true, completion: nil)
                    }
                   
                    
                }}
            
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
        
    }
    
    @objc func openMessages(){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "messagesViewController") as? adsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    
    @objc func openNotifications(){
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: false)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }
    @objc func openSettings(){
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppSettingsViewCtrl") as? AppSettingsVCtrl {
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: false)
            }
        }
    }
    
    func loadJobsData(){
        let previousTimeVisitAdded = UserDefaults.standard.double(forKey: "previousAddVisit")
        if previousTimeVisitAdded > 0.0 {
            let previousDate = Date(timeIntervalSince1970: previousTimeVisitAdded)
            let calendar = Calendar.current
            let numberOfDaysFromPreviousVisit = calendar.dateComponents([.day], from: calendar.startOfDay(for: previousDate), to: calendar.startOfDay(for: Date()))
            print(numberOfDaysFromPreviousVisit.day ?? 0)
            if (numberOfDaysFromPreviousVisit.day ?? 0 ) > 0 {
                if UserManager.sharedInstance.getUser() != nil {
                    self.homeCatsPresenter.addVisit()
                }
            }
        }
        else{
            if UserManager.sharedInstance.getUser() != nil {
                self.homeCatsPresenter.addVisit()
            }
        }
        switch self.jobToLoadType {
        case .Recomended:
            DispatchQueue.main.async {
                self.HomeJobsTV.isHidden = true
                self.homeCatsCv.isHidden = false
                self.homeCatsPresenter.getHomeCats()
            }
            
        case .Newest:
            DispatchQueue.main.async {
                self.HomeJobsTV.isHidden = false
                self.homeCatsCv.isHidden = true
//                self.HomeJobsTV.windless
//                    .apply {
//                        $0.beginTime = 0
//                        $0.pauseDuration = 1
//                        $0.duration = 3
//                        $0.animationLayerOpacity = 0.8
//                        $0.direction = .left
//                        self.emptyView.alpha = 0
//                        self.HomeJobsTV.alpha = 1
//                    }
//                    .start()
                
    //            self.view.addSubview(floaty)
                self.jobsPresenter.getNewestJobs(page: self.page)
            }
            
        case .Search:
            print("search")
        }
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! SideMenuNavigationController
        if L102Language.isRTL {
            menu.leftSide = false
        }
        else{
            menu.leftSide = true
        }
        
        present(menu, animated: true, completion: nil)
    }
    private func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        //SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        if L102Language.isRTL { SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .right)
        }
        else{ SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
            
        }
    }
    
    func initPullToRefresh()
    {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //let loadingView2 = DGElasticPullToRefreshLoadingViewCircle()
        //loadingView2.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        HomeJobsTV?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self!.page = 1
            self?.loadJobsData()
        }, loadingView: loadingView)
        self.HomeJobsTV?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.HomeJobsTV?.dg_setPullToRefreshBackgroundColor(.white)
        
//        homeCatsCv?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
//
//            self!.page = 1
//            self?.loadJobsData()
//        }, loadingView: loadingView2)
//        self.homeCatsCv?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
//
//        self.homeCatsCv?.dg_setPullToRefreshBackgroundColor(.white)
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            homeCatsCv.refreshControl = refreshControl
        } else {
            homeCatsCv.addSubview(refreshControl)
        }
        
    }
    @objc func refresh(sender:AnyObject)
      {
        refreshControl.endRefreshing()
        self.page = 1
        self.loadJobsData()
        if UserManager.sharedInstance.getUser() != nil {
            notificationCountPresenter.getNotificationCount()
        }
      }
    func initLoadMoreToRefresh()
    {
        self.HomeJobsTV?.infiniteScrollIndicatorStyle = .gray
        
        // Add infinite scroll handler
        self.HomeJobsTV?.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return self.page != self.jobsResponse?.last_page
        }
        self.HomeJobsTV?.addInfiniteScroll { (tableView) -> Void in
            
            if self.page == self.jobsResponse?.last_page {
                //ending paging
                self.HomeJobsTV.finishInfiniteScroll()
            }else {
                self.page += 1
                self.loadJobsData()
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if jobsResponse == nil {
//            return itemCountForWindless
//        }else {
//            return (jobsResponse?.data?.count)!
//
//        }
        return JobsAndAdsArr.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 135
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        JobsAndAdsArr[indexPath.row]
//        jobsResponse?.data

        let cell = HomeJobsTV.dequeueReusableCell(withIdentifier: "HomeJobsCell", for: indexPath) as! HomeJobsTVCell
                if JobsAndAdsArr.count > 0 {
                    cell.whiteContainerView.layer.cornerRadius = 10
                    cell.ApplyJobBtn.layer.cornerRadius = 12.3
                    cell.shareBtn.layer.cornerRadius = 12.3
                    cell.shareBtn.tag = indexPath.row
                    cell.shareBtn.setTitle("Share".localized(), for: .normal)
                    cell.shareBtn.setTitle("Share".localized(), for: .selected)
                    
                    cell.shareBtn.backgroundColor = UIColor(hexString: "#161650")
                    cell.shareBtn.addTarget(self, action: #selector(shareJob2(_:)), for: .touchUpInside)
                    let item = JobsAndAdsArr[indexPath.row]
                    let disabilityEnabled = item.DisabilityEnabled
                        if disabilityEnabled == 1 {
                            cell.disability_ic.isHidden = false
                        }
                        else{
                            cell.disability_ic.isHidden = true
                        }
                    if let expired_status = item.jobExpired  {
                        if expired_status {
                            cell.ApplyJobBtn.isHidden = true
                            cell.shareBtn.isHidden = true
                        }
                        else{
                            cell.ApplyJobBtn.isHidden = false
                            cell.shareBtn.isHidden = false
                        }
                    }
                    else{
                        cell.ApplyJobBtn.isHidden = false
                        cell.shareBtn.isHidden = false
                    }
                    let appliedForJobStatus = item.appliedForJob
                        if appliedForJobStatus {
                            cell.ApplyJobBtn.setTitle("Applied".localized(), for: .normal)
                            cell.ApplyJobBtn.setTitle("Applied".localized(), for: .selected)
                            cell.ApplyJobBtn.backgroundColor = UIColor(hexString: "#00ccff")
                        }
                        else{
                            cell.ApplyJobBtn.setTitle("Apply Now".localized(), for: .normal)
                            cell.ApplyJobBtn.setTitle("Apply Now".localized(), for: .selected)
                            cell.ApplyJobBtn.backgroundColor = UIColor(hexString: "#161650")
                        }
                    cell.jobTitle.text = item.title
                    if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                        if subscribedUser {
                            cell.JobImg.kf.indicatorType = .activity
                            let url = URL(string: (item.imagePath))
                            
                            let processor = ResizingImageProcessor(referenceSize: CGSize(width: 22, height: 22))
                            
                            cell.JobImg.kf.setImage(
                                with: url,
                                placeholder: UIImage(named: "company_ic_large"),
                                options: [
                                    
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1)),
                                    .cacheOriginalImage,
                                    
                                ], completionHandler:
                                    {
                                        result in
                                        switch result {
                                        case .success(let value):
                                            print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                        case .failure(let error):
                                            print("Job failed: \(error.localizedDescription)")
                                        }
                                    })
                            
                        }
                    }
                    if let subscribedUser = UserManager.sharedInstance.subscribedUser {
                        if subscribedUser {
                            cell.comNameHeightConstraint.constant = 20
                            cell.JobCompany.text = item.companyName
                        }
                    }
                    cell.JobType.text = "\(item.job_type_name ) - \(item.workTimeText)"
                    if (item.job_type_color).count > 0 {
                    cell.JobType.textColor = UIColor(hexString: (item.job_type_color))
                    }
                    else{
                        cell.JobType.textColor = UIColor(hexString: "#9E75B2")
                    }
                    cell.JobLocation.text = item.city
                    
                    if (item.created_at).count > 0 {
                        cell.JobPostedTime.text = (item.created_at).getFormattedDate()
                    }
                }
        cell.selectionStyle = .none
        return cell
    }
    @objc func shareJob(_ sender: UIButton){
        let jobIndex = sender.tag
        self.spinnerView = UIViewController.displaySpinner(onView: self.view)
        if (jobsResponse?.data!.count)! > 0 {
            
            let item = jobsResponse?.data![jobIndex]
            //let urlShare = "https://jobplus.page.link/job?id=\(item?.id ?? 0)"
            var components = URLComponents()
            components.scheme = "https"
            //"www.jobplus.biz/parttime/public"
            components.host = "jobplus.page.link"
            components.path = "/job"
            let jobQueryItem = URLQueryItem(name: "id", value: "\(item?.id ?? 0)")
            components.queryItems = [jobQueryItem]
            
            guard let linkParameter = components.url else {return}
            print("I am sharing link \(linkParameter.absoluteURL)")
            
            guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://jobplus.page.link") else{
                print("couldnot create link")
                return
            }
            shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            shareLink.socialMetaTagParameters?.title = item?.title ?? ""
            shareLink.socialMetaTagParameters?.imageURL = URL(string: "https://www.jobplus.biz/parttime/public/img/landing-page/icon/JP_Logo1.png")
            if let myBundleID = Bundle.main.bundleIdentifier {
                shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleID)
            }
            shareLink.iOSParameters?.appStoreID = "1493332366"
            shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.adroyts.jobplus")
            guard let longURL = shareLink.url else {return}
            print("longUrl \(longURL.absoluteString)")
            shareLink.shorten { (url, warnings, error) in
                guard error == nil else {
                    print(error!)
                    return
                }
                if let url = url {
                    print("shorten url \(url.absoluteString)")
                    
                    self.showShareActivity(msg: "\(item?.title ?? "") \n\(item?.job_type_name ?? "")", image: UIImage(named: "jpLogo_splash"), url: url.absoluteString, sourceRect: nil)
                }
            }
            
        }
        
    }
    @objc func shareJob2(_ sender: UIButton){
        let jobIndex = sender.tag
        self.spinnerView = UIViewController.displaySpinner(onView: self.view)
        if (jobsResponse?.data!.count)! > 0 {
            
            let item =  JobsAndAdsArr[jobIndex]  //jobsResponse?.data![jobIndex]
        guard let link = URL(string: "https://jobplus.page.link/job?id=\(item.id)") else { return }
        let dynamicLinksDomainURIPrefix = "https://jobplus.page.link"
        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)

        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.adroyts.jobplus")
        linkBuilder?.iOSParameters?.appStoreID = "1493332366"

        linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.adroyts.jobplus")


        linkBuilder?.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder?.socialMetaTagParameters?.title = item.title
            linkBuilder?.socialMetaTagParameters?.descriptionText = item.job_type_name
        linkBuilder?.socialMetaTagParameters?.imageURL = URL(string: "https://www.jobplus.biz/parttime/public/img/landing-page/icon/JP_Logo1.png")

        guard let longDynamicLink = linkBuilder?.url else { return }
        print("The long URL is: \(longDynamicLink)")
            linkBuilder?.options = DynamicLinkComponentsOptions()
            linkBuilder?.shorten() { url, warnings, error in
                guard let url = url, error == nil else { if let sv = self.spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
                self.showErrorMessage(message: "Failed to generate share link, please try again later.")
                print("shorten error \(String(describing: error))"); return }
              print("The short URL is: \(url)")
                self.showShareActivity(msg: "\(item.title ) \n\(item.job_type_name)", image: UIImage(named: "jpLogo_splash"), url: url.absoluteString, sourceRect: nil)
            }

        }
    }
    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems:  [msg ?? "", url ?? ""], applicationActivities: nil)
        activityViewController.setValue("Job Plus", forKey: "Subject")
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        activityViewController.popoverPresentationController?.sourceView = self.view;
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
        
        self.present(activityViewController, animated: true) {
            if let sv = self.spinnerView{
                UIViewController.removeSpinner(spinner: sv)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 6
            if let expired_status = JobsAndAdsArr[indexPath.row].jobExpired {
                if expired_status {
                    cell.alpha = 0.5
                }
                else{
                    cell.alpha = 1
                }
                
            }
            else{
                cell.alpha = 1
            }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if jobsResponse?.data != nil  {
            UserManager.sharedInstance.jobIndexAppliedOrCanceled = indexPath
            self.performSegue(withIdentifier:"JobDetailsViewController", sender:  JobsAndAdsArr[indexPath.row])
           
            
        }
        
    }
    private func updateMenus() {
        let settings = makeSettings()
        SideMenuManager.default.leftMenuNavigationController?.settings = settings
        SideMenuManager.default.rightMenuNavigationController?.settings = settings
    }
    private func makeSettings() -> SideMenuSettings {
        var settings = SideMenuSettings()
        
        settings.menuWidth = 240
        settings.statusBarEndAlpha = 0
        return settings
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "JobDetailsViewController" {
            let job = sender as? jobItem2ForAds
            let destination = segue.destination as?JobDetailsViewController
            destination?.jobItem = job
            
        }
    }
}

extension HomeVCtrl :JobsTypesView {
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        itemCountForWindless = 0
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess(jobsResponse: JobsTypesResponse) {    }
    
    func onError(message: String) {
        if let notiStatus = UserManager.sharedInstance.getUser()?.NotificationStatus {
            UserManager.sharedInstance.notificationStatus = notiStatus
        }
        self.showErrorMessage(message:message)
        self.HomeJobsTV.windless.end()
        itemCountForWindless = 0
        self.HomeJobsTV.dg_stopLoading()
        if let count = jobsResponse?.data?.count {
            if count > 0 {
                jobsResponse?.data? = []
            }
        }
        self.HomeJobsTV.reloadData()
    }
}

extension HomeVCtrl : JobsView {
    func onSuccessNextLinkCall(jobsResponse: JobListResponse) {
        print("onSuccessNextLinkCall")
        self.HomeJobsTV.finishInfiniteScroll()
        self.HomeJobsTV.windless.end()
        //itemCountForWindless = 0
        self.HomeJobsTV.dg_stopLoading()
        for item in jobsResponse.data! {
            self.jobsResponse?.data?.append(item)
        }
        var jobs = jobsResponse
        DispatchQueue.main.async {
            self.reloadBtn.isHidden = true
        }
        self.emptyView.alpha = 0
        self.HomeJobsTV.alpha = 1
        //self.jobsResponse?.meta = jobsResponse.meta
        self.HomeJobsTV.alpha = 1
        if  self.jobsResponse?.data?.count == 0 {
            self.emptyView.alpha = 1
            DispatchQueue.main.async {
                self.reloadBtn.isHidden = false
            }
            self.HomeJobsTV.alpha = 0
        }
        else{
            DispatchQueue.main.async {
                self.reloadBtn.isHidden = true
            }
            self.emptyView.alpha = 0
            self.HomeJobsTV.alpha = 1
            for index in 0..<jobs.data!.count {
                if let expired_date = jobsResponse.data?[index].expireDate {
                    let staus = expired_date.checkExiration()
                    jobs.data?[index].jobExpired = staus
                }
                else{
                    jobs.data?[index].jobExpired = false
                }
            }
        }
        for i in  jobs.data! {
                JobsAndAdsArr.append(jobItem2ForAds(id: i.id, title: i.title ?? "", companyName: i.companyName, imagePath: i.imagePath ?? "", city: i.city, job_type_id: i.job_type_id ?? 0, job_type_name: i.job_type_name ?? "", job_type_color: i.job_type_color ?? "", created_at: i.created_at!, appliedForJob: i.appliedForJob ?? false , expireDate: i.expireDate ?? "", job_status: i.job_status ?? 0, jobExpired: i.jobExpired ?? false, workTimeText: i.workTimeText ?? "", DisabilityEnabled: i.DisabilityEnabled ?? 0))

            }
        HomeJobsTV.reloadData()
        
        
    }
    
    func onSucess(jobsResponse: JobListResponse) {
        self.HomeJobsTV.windless.end()
        self.HomeJobsTV.dg_stopLoading()
        self.jobsResponse = jobsResponse
        self.HomeJobsTV.alpha = 1
        if let userDeleted = jobsResponse.userDeleted {
            if userDeleted == 1 {
                self.showErrorMessage(message: "Sorry, your account has been suspended, you can contact our support team".localized())
                clearDataAndStart()
            }
        }
        if let jobs = jobsResponse.data , jobs.count == 0 {
            DispatchQueue.main.async {
                self.reloadBtn.isHidden = false
            }
            self.emptyView.alpha = 1
            self.HomeJobsTV.alpha = 0
        }
        else{
            DispatchQueue.main.async {
                self.reloadBtn.isHidden = true
            }
            if let subscribed = jobsResponse.subscribedUser {
                if subscribed {
                    UserManager.sharedInstance.subscribedUser = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                }
                else{
                    UserManager.sharedInstance.subscribedUser = false
                    
                    UserManager.sharedInstance.NotSubscribedMeassage = jobsResponse.NotSubscribedMeassage
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSubscribedImg"), object: nil)
                }
            }
            
            self.emptyView.alpha = 0
            self.HomeJobsTV.alpha = 1
            for index in 0..<self.jobsResponse!.data!.count {
                if let expired_date = self.jobsResponse!.data?[index].expireDate {
                    let staus = expired_date.checkExiration()
                    self.jobsResponse!.data?[index].jobExpired = staus
                }
                else{
                    self.jobsResponse!.data?[index].jobExpired = false
                }
            }
        }
        
//        if jobsResponse.subscribedUser == false {
//            //let adsAfter:Int = jobsResponse.DisplayAdAfter!
//            //let jobArr =  self.jobsResponse!.data?.chunks(adsAfter)  //jobsResponse.data?.chunks(adsAfter)
//            for index in 0 ..< jobArr!.count{
//                for i in  jobArr![index]{
//                    JobsAndAdsArr.append(jobItem2ForAds(id: i.id, title: i.title ?? "", companyName: i.companyName, imagePath: i.imagePath ?? "", city: i.city, job_type_id: i.job_type_id ?? 0, job_type_name: i.job_type_name ?? "", job_type_color: i.job_type_color ?? "", created_at: i.created_at!, appliedForJob: i.appliedForJob ?? false , expireDate: i.expireDate ?? "", job_status: i.job_status ?? 0, jobExpired: i.jobExpired ?? false, workTimeText: i.workTimeText ?? ""))
//                }
//                if jobArr![index].count >= adsAfter{
//                JobsAndAdsArr.append(adItem(id: index, title: "ads"))
//                }
//            }
//        }else{
        JobsAndAdsArr.removeAll()
        for i in  self.jobsResponse!.data! {
                JobsAndAdsArr.append(jobItem2ForAds(id: i.id, title: i.title ?? "", companyName: i.companyName, imagePath: i.imagePath ?? "", city: i.city, job_type_id: i.job_type_id ?? 0, job_type_name: i.job_type_name ?? "", job_type_color: i.job_type_color ?? "", created_at: i.created_at!, appliedForJob: i.appliedForJob ?? false , expireDate: i.expireDate ?? "", job_status: i.job_status ?? 0, jobExpired: i.jobExpired ?? false, workTimeText: i.workTimeText ?? "", DisabilityEnabled: i.DisabilityEnabled ?? 0))

            }
        //}
        
        self.HomeJobsTV.reloadData()
    }
    
}

extension HomeVCtrl:CityListView{
    func onSucessLoadingCityList(cities: [City]) {
        //        self.cityList.removeAll()
        //        self.cityList.append(contentsOf: cities)
    }
    
    func showLoadingCityListIndicator() {
        
    }
    
    func hideLoadingCityListIndicator() {
    }
}
extension HomeVCtrl: NotificationView{
    func showLoading() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoading() {
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
    
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationsCountLbl.text = (" \(String(notificationCount)) ")
            self.notificationsCountLbl.isHidden = false
        }
        else{
            self.notificationsCountLbl.isHidden = true
        }
    }
}


extension HomeVCtrl: subscriptionsView{
    func onSuccessNewPackages(Res: newPacakagesRes) {
        
    }
    
    func onSuccessHyperPay(URLString: String){}
    func onSucess(subscriptionResponse: packagesRes) {}
    
    func onSuccessAddTransaction() {
        self.showSucessMessage(message: NSLocalizedString("Successfully Subscribed , Thank You.", comment: ""))
        UserDefaults.standard.removeObject(forKey: "pendingTransactioniD")
        UserDefaults.standard.removeObject(forKey: "TransactionDate")
        self.page = 1
        self.loadJobsData()
    }
    
    func onAddTransactionError() {}
    
    func iapShowLoading() {}
    
    func iapHideLoading() {}
    
    
}

extension HomeVCtrl: HomeCatsView{
    func onSuccessAddFav(index: Int) {
        let cell = homeCatsCv.cellForItem(at: IndexPath(row: index, section: 0)) as! homeCatsCVCell
        homeCats[index].addToFavourite = true
        cell.addToFavBtn.setImage(UIImage(named: "fav_ic"), for: .normal)
        cell.addToFavBtn.setImage(UIImage(named: "fav_ic"), for: .selected)
    }
    
    func onSuccessRemoveFav(index: Int) {
        let cell = homeCatsCv.cellForItem(at: IndexPath(row: index, section: 0)) as! homeCatsCVCell
        homeCats[index].addToFavourite = false
        cell.addToFavBtn.setImage(UIImage(named: "un_fav_ic"), for: .normal)
        cell.addToFavBtn.setImage(UIImage(named: "un_fav_ic"), for: .selected)
    }
    
    func onSucessGetHomeCats(homeCats: [catItem]) {
        self.homeCatsCv.dg_stopLoading()
        self.homeCats = homeCats
        self.homeCatsCv.reloadData()
    }
}
extension HomeVCtrl: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homeCats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homeCatsCv.dequeueReusableCell(withReuseIdentifier: "homeCatsCell", for: indexPath) as! homeCatsCVCell
        let catItem = self.homeCats[indexPath.row]
        if catItem.addToFavourite ?? false {
            cell.addToFavBtn.setImage(UIImage(named: "fav_ic"), for: .normal)
            cell.addToFavBtn.setImage(UIImage(named: "fav_ic"), for: .selected)
        }
        else{
            cell.addToFavBtn.setImage(UIImage(named: "un_fav_ic"), for: .normal)
            cell.addToFavBtn.setImage(UIImage(named: "un_fav_ic"), for: .selected)
        }
            cell.addToFavBtn.tag = indexPath.row
            cell.addToFavBtn.addTarget(self, action: #selector(addToFavAction(_:)), for: .touchUpInside)
        if homeCats[indexPath.row].id == 0 {
            cell.addToFavBtn.isHidden = true
        }
        else{
            cell.addToFavBtn.isHidden = false
        }
        //cell.catImg.kf.setImage(with: URL(string: homeCats[indexPath.row].imagePath ?? ""))
        
                cell.catImg.kf.indicatorType = .activity
                let url = URL(string: homeCats[indexPath.row].imagePath ?? "")
                
                let processor = ResizingImageProcessor(referenceSize: CGSize(width: 22, height: 22))
                
                cell.catImg.kf.setImage(
                    with: url,
                    placeholder: UIImage(named: "jpLogo_splash"),
                    options: [
                        
                        .processor(processor),
                        .scaleFactor(UIScreen.main.scale),
                        .transition(.fade(1)),
                        .cacheOriginalImage,
                        
                    ], completionHandler:
                        {
                            result in
                            switch result {
                            case .success(let value):
                                print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            case .failure(let error):
                                print("Job failed: \(error.localizedDescription)")
                            }
                        })
        cell.catNameLbl.text = L102Language.isRTL ? homeCats[indexPath.row].nameAr : homeCats[indexPath.row].name
        return cell
    }
    @objc func addToFavAction(_ sender: UIButton){
        if UserManager.sharedInstance.getUser() == nil {
            self.showErrorMessage(message: "you have to login first".localized())
        }
        else{
            if let addedToFav = homeCats[sender.tag].addToFavourite {
                if addedToFav {
                    homeCatsPresenter.removeCatToFave(catID:  homeCats[sender.tag].id ?? 0, index: sender.tag)
                }
                else{
                    homeCatsPresenter.addCatToFave(catID:  homeCats[sender.tag].id ?? 0, index: sender.tag)
                }
            }
            else{
                homeCatsPresenter.addCatToFave(catID:  homeCats[sender.tag].id ?? 0, index: sender.tag)
            }
            
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sectorID = homeCats[indexPath.row].id
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobsListViewCtrl") as? JobsListVCtrl {
            viewController.SectorID = sectorID ?? 0
            viewController.sectorName = (L102Language.isRTL ? homeCats[indexPath.row].nameAr : homeCats[indexPath.row].name) ?? ""
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = collectionViewSize.width/2.0 - 8 //Display Four elements in a row.
        collectionViewSize.height = 128
        return collectionViewSize
    }
}

extension Array {
    func chunks(_ chunkSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: chunkSize).map {
            Array(self[$0..<Swift.min($0 + chunkSize, self.count)])
        }
    }
}
extension HomeVCtrl: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scroll happened")
     
        switch self.jobToLoadType {
        case .Newest:
            if L102Language.isRTL {
                Floaty.global.rtlMode = true
            }
           
            if scrollView.contentOffset.y == 0 {
                // TOP
                self.floaty.removeFromSuperview()
            }else{
                self.view.addSubview(floaty)
            }
            
            
        case .Recomended:
            print("Recomended")
        case .Search:
            print("Search")
        }
        
           
      
    }
}
extension HomeVCtrl: StoreVersionView {
    func storeVersionSuccessCallback(storeVersion: String) {
        if let storeVersionRes = Float(storeVersion) {
            if storeVersionRes > Float(UserManager.sharedInstance.appVersion) {
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                DispatchQueue.main.async {
                    if let controller = mainStoryBoard.instantiateViewController(withIdentifier: "updateViewControler") as? updateAppViewController {
                        controller.showThanks = false
                        self.present(controller, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
