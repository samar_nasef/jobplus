//
//  FeedBackVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 01/02/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import UIKit

class FeedBackVCtrl: UIViewController {

    @IBOutlet weak var ratingView: JStarRatingView!
    @IBOutlet weak var userFeedbackView: UIView!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var feedbackDate: UILabel!
    @IBOutlet weak var feedbackMsg: UILabel!
    @IBOutlet weak var msgView: UIView!
    @IBOutlet weak var msgTxtView: UITextView!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var msgView_top_constraint: NSLayoutConstraint!
    @IBOutlet weak var feedback_hieght_constraint: NSLayoutConstraint!
    @IBOutlet weak var userImgView: UIView!
    let feedbackPresenter = FeedBackPresenter()
    var spinnerView: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
            //self.ratingView.semanticContentAttribute = .forceRightToLeft
        }
        self.userFeedbackView.isHidden = true
        self.msgView.isHidden = true
        self.msgTxtView.placeholder = "Add Comment...".localized()
        self.msgTxtView.cornerRadius = 4
        userFeedbackView.layer.cornerRadius = 4
        userFeedbackView.layer.shadowColor = UIColor.black.cgColor
        userFeedbackView.layer.shadowOpacity = 0.2
        userFeedbackView.layer.shadowOffset = .zero
        userFeedbackView.layer.shadowRadius = 5
        
        msgView.layer.cornerRadius = 4
        msgView.layer.shadowColor = UIColor.black.cgColor
        msgView.layer.shadowOpacity = 0.2
        msgView.layer.shadowOffset = .zero
        msgView.layer.shadowRadius = 5
        
        if UserManager.sharedInstance.getUser() != nil {
            if let profileImg = UserManager.sharedInstance.getUser()?.imagePath, !profileImg.isEmpty{
                if let profileImgUrl = URL(string: profileImg){
                    self.userImg.kf.setImage(with: profileImgUrl, placeholder: UIImage(named: "profile_img"))
                }
                
            }
            else {
                if let gender = UserManager.sharedInstance.getUser()?.gender {
                    if gender == "female" {
                        self.userImg.image = UIImage(named: "woman")
                    }
                    else{
                        self.userImg.image = UIImage(named: "profile_img")
                    }
                }
                else{
                    self.userImg.image = UIImage(named: "profile_img")
                }
            }
            // }
            if let userName = UserManager.sharedInstance.getUser()?.name {
                self.user_name.text = userName + " " + ( UserManager.sharedInstance.getUser()?.LastName ?? "" )
            }
        }
        userImgView.layer.cornerRadius = 30
        userImgView.layer.shadowColor = UIColor.black.cgColor
        userImgView.layer.shadowOpacity = 0.2
        userImgView.layer.shadowOffset = .zero
        userImgView.layer.shadowRadius = 10
        self.view.changeFont()
        self.feedbackPresenter.attachView(view: self)
        self.feedbackPresenter.listFeedBack()
    }
    
    @IBAction func close(_ sender: UIButton) {
       if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func sendFeedbackAction(_ sender: Any) {
        if self.ratingView.rating == 0.0 {
            self.showErrorMessage(message: "Please enter your rating!".localized())
        }
        else if msgTxtView.text.isBlank, self.ratingView.rating < 2.5 {
            self.showErrorMessage(message: "Please enter your comment!".localized())
        }
        else if !msgTxtView.text.isBlank, msgTxtView.text.count < 3 {
            self.showErrorMessage(message: "Please enter your comment with at lease 3 characters!".localized())
        }
        else{
            feedbackPresenter.addFeedBack(rate: ratingView.rating, message: msgTxtView.text)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FeedBackVCtrl: ListAddFeedBackView {
    func showLoadingIndicator() {
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingIndicator() {
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucess(feedBack: [feedBackItem]) {
        if feedBack.count > 0 {
            self.feedbackDate.text = feedBack[0].created_at?.getFormattedDateTime()
            let ratedVal = Float(feedBack[0].rate ?? "0.0")
            self.ratingView.rating = ratedVal ?? 0.0
            self.feedbackMsg.text = feedBack[0].message
//            let feedback_height = (feedBack[0].message ?? " ").height(constraintedWidth: feedbackMsg.width, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 15.0)!)
//            if feedback_height > 35 {
//                self.feedback_hieght_constraint.constant = 70 + feedback_height
//            }
            self.userFeedbackView.isHidden = false
        }
        else{
            self.msgView_top_constraint.constant = 40
            self.msgView.isHidden = false
        }
    }
    
    func onSuccessAddFeedback(message: String) {
        self.showSucessMessage(message: message)
        if let navController = self.navigationController {
             navController.popViewController(animated: true)
         }
         else{
             self.dismiss(animated: true, completion: nil)
         }
    }
    
    func onError() {
        self.showErrorMessage(message: "Connection error, please try again later")
        if let navController = self.navigationController {
             navController.popViewController(animated: true)
         }
         else{
             self.dismiss(animated: true, completion: nil)
         }
    }
    
    func onErrorAddFeedback(message: String) {
        self.showErrorMessage(message: message)
        if let navController = self.navigationController {
             navController.popViewController(animated: true)
         }
         else{
             self.dismiss(animated: true, completion: nil)
         }
    }
    
    
}
