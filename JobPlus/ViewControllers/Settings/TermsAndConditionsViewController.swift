//
//  TermsAndConditionsViewController.swift
//  DareesStudent
//
//  Created by passant on 8/12/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit
enum Terms {
    case fromAuthorization
    case fromMenu
}
class TermsAndConditionsViewController: UIViewController {
    
    @IBOutlet weak var notiBtn: UIButton!
    @IBOutlet weak var notificationCountLbl: UILabel!
    @IBOutlet weak var pageContentLbl: UITextView!
    @IBOutlet weak var navArrow: UIButton!
    let notificationCountPresenter = NotificationPresenter()
    var pageObj :Page?
    var pageCase :SettingPage?
    let settingsPresenter = SettingsPagesPresenter()
    var spinnerView: UIView?
    var termsCase = Terms.fromMenu
    override func viewDidLoad() {
        super.viewDidLoad()
        pageCase = SettingPage.TermsAndConditions
        self.settingsPresenter.attachView(view: self)
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        self.settingsPresenter.doGetSettingsPagesData(slug: (pageCase?.rawValue)!)
        setUpUI()
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
notificationCountPresenter.attachView(view: self)
if UserManager.sharedInstance.getUser() != nil {
           self.notiBtn.isHidden = false
       }
        initPullToRefresh()
    }
    func initPullToRefresh()
    {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pageContentLbl?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.settingsPresenter.doGetSettingsPagesData(slug: (self?.pageCase?.rawValue)!)
            self?.pageContentLbl.dg_stopLoading()
            }, loadingView: loadingView)
        self.pageContentLbl?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.pageContentLbl?.dg_setPullToRefreshBackgroundColor(pageContentLbl.backgroundColor!)
    }
    @IBAction func notificationOpenAction(_ sender: Any) {
        if UserManager.sharedInstance.getUser() != nil {
            if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                
                if let navigator = navigationController {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
        }
        else{
            self.showErrorMessage(message: "you have to login first".localized())
        }
    }

    override func viewDidAppear(_ animated: Bool) {
if UserManager.sharedInstance.getUser() != nil {
            let notiCount = UserManager.sharedInstance.notificationCount
            if notiCount > 0 {
                self.notificationCountLbl.text = (" \(String(notiCount)) ")
                self.notificationCountLbl.isHidden = false
            }
            else{
                 self.notificationCountLbl.isHidden = true
            }
notificationCountPresenter.getNotificationCount()

}
    }
    @IBAction func close(_ sender: UIButton) {
       if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    func setUpUI(){
       
        
     
        if let content = pageObj?.content {
            print(content)
            //let htmlData = NSString(string: content).data(using: String.Encoding.unicode.rawValue)
            //let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            //let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
            
            
            //let htmlData = NSString(string: content).data(using: String.Encoding.unicode.rawValue)
//                       let paragraph = NSMutableParagraphStyle()
//                       if L102Language.currentAppleLanguage() == "ar" {
//
//                           paragraph.alignment = .right
//                       }else {
//                            paragraph.alignment = .left
//                       }
//
//                       let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html , NSAttributedString.Key.paragraphStyle: paragraph, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!] as [AnyHashable : Any]
//                       let attributedString = try! NSAttributedString(data: htmlData!, options: options as! [NSAttributedString.DocumentReadingOptionKey : Any], documentAttributes: nil)
            let attr = try? NSAttributedString(htmlString: content, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14.0)!)
                       self.pageContentLbl.attributedText = attr
            if L102Language.isRTL {
                self.pageContentLbl.textAlignment = .right
                self.pageContentLbl.semanticContentAttribute = .forceRightToLeft
            }
                       
        }
        
    }
    
    
}
extension TermsAndConditionsViewController:SettingsPagesView{
    func onSuccessContactInfo(contactInfo: ContactUsRes) {
        
    }
    
    func showLoadingPageIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingPageIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
                    UIViewController.removeSpinner(spinner: sv)
                }
    }
    
    func onSucessPage(pageObj: Page) {
        self.pageObj = pageObj
        self.setUpUI()
    }
    
    func onErrorPage(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}
extension NSAttributedString {

    convenience init(htmlString html: String, font: UIFont? = nil, useDocumentFontSize: Bool = true) throws {
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        let data = html.data(using: .utf8, allowLossyConversion: true)
        guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
            return
        }

        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
        let range = NSRange(location: 0, length: attr.length)
        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            if let htmlFont = attrib as? UIFont {
                let traits = htmlFont.fontDescriptor.symbolicTraits
                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)

                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitBold.rawValue) != 0 {
                    descrip = descrip.withSymbolicTraits(.traitBold)!
                }

                if (traits.rawValue & UIFontDescriptor.SymbolicTraits.traitItalic.rawValue) != 0 {
                    descrip = descrip.withSymbolicTraits(.traitItalic)!
                }

                attr.addAttribute(.font, value: UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize), range: range)
            }
        }

        self.init(attributedString: attr)
    }

}
extension TermsAndConditionsViewController: NotificationView{
    func showLoadingIndicator() {
        
    }
    
    func hideLoadingIndicator() {
        
    }
    
    func onError(message: String) {
        
    }
    
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationCountLbl.text = (" \(String(notificationCount)) ")
            self.notificationCountLbl.isHidden = false
        }
        else{
            self.notificationCountLbl.isHidden = true
        }
    }
}
