//
//  contactUSVCtrl.swift
//  JobPlus
//
//  Created by elsaid yousif on 18/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import UIKit

class contactUSVCtrl: UIViewController {

    var email = ""
    var phone = ""
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var emailBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        callBtn.setTitle("", for: .normal)
        callBtn.setTitle("", for: .selected)
        emailBtn.setTitle("", for: .normal)
        emailBtn.setTitle("", for: .selected)
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view == self.view {
        self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func callAction(_ sender: Any) {
        let urlString = "tel://\(self.phone)"
                
                let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                
                let URL = NSURL(string: urlStringEncoded!)
                
                if UIApplication.shared.canOpenURL(URL! as URL) {
                    UIApplication.shared.open(URL! as URL)
                }
    }
    
    @IBAction func mailAction(_ sender: Any) {
        let urlString = "mailto://\(self.email)"
                
                let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                
                let URL = NSURL(string: urlStringEncoded!)
                
                if UIApplication.shared.canOpenURL(URL! as URL) {
                    UIApplication.shared.open(URL! as URL)
                }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
