//
//  AboutViewController.swift
//  DareesStudent
//
//  Created by passant on 8/12/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit
enum SettingPage:String{
    case About = "aboutUs"
    case TermsAndConditions = "terms_and_conditions"
}
class AboutViewController: UIViewController {
    
    @IBOutlet weak var pageImgView: UIImageView!
    @IBOutlet weak var pageContentLbl: UITextView!
    @IBOutlet weak var navArrow: UIButton!
    @IBOutlet weak var notificationCountLbl: UILabel!
    @IBOutlet weak var notiBtn: UIButton!
    let notificationCountPresenter = NotificationPresenter()
    var pageObj :Page?
    var pageCase :SettingPage?
    var spinnerView: UIView?
    let settingsPresenter = SettingsPagesPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageCase = SettingPage.About
        self.settingsPresenter.attachView(view: self)
        self.view.sendSubviewToBack(self.view.insertHorizontalGradient())
        self.settingsPresenter.doGetSettingsPagesData(slug: (pageCase?.rawValue)!)
        self.navigationController?.setupNavigationControllerOfPartTime()
        
        if L102Language.isRTL {
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .normal)
            self.navArrow.setImage(UIImage(named: "leftArrow-ar"), for: .selected)
        }
        notificationCountPresenter.attachView(view: self)
        if UserManager.sharedInstance.getUser() != nil {
                   self.notiBtn.isHidden = false
               }

        initPullToRefresh()
    }
    func initPullToRefresh()
    {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        pageContentLbl?.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.settingsPresenter.doGetSettingsPagesData(slug: (self?.pageCase?.rawValue)!)
            self?.pageContentLbl.dg_stopLoading()
            }, loadingView: loadingView)
        self.pageContentLbl?.dg_setPullToRefreshFillColor(UIColor.frenchBlue)
        
        self.pageContentLbl?.dg_setPullToRefreshBackgroundColor(pageContentLbl.backgroundColor!)
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserManager.sharedInstance.getUser() != nil {
                    let notiCount = UserManager.sharedInstance.notificationCount
                    if notiCount > 0 {
                        self.notificationCountLbl.text = (" \(String(notiCount)) ")
                        self.notificationCountLbl.isHidden = false
                    }
                    else{
                         self.notificationCountLbl.isHidden = true
                    }
        notificationCountPresenter.getNotificationCount()
        
        }
    }
    @IBAction func notificationOpenAction(_ sender: Any) {
           if UserManager.sharedInstance.getUser() != nil {
               if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {

                   if let navigator = navigationController {
                       navigator.pushViewController(viewController, animated: true)
                   }
               }
           }
           else{
               self.showErrorMessage(message: "you have to login first".localized())
           }
       }
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    func setUpUI(){
        
        if let content = pageObj?.content {
            print(content)
            
//            let htmlData = NSString(string: content).data(using: String.Encoding.unicode.rawValue)
//            let paragraph = NSMutableParagraphStyle()
//            if L102Language.currentAppleLanguage() == "ar" {
//
//                paragraph.alignment = .right
//            }else {
//                 paragraph.alignment = .left
//            }
//
//            let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html , NSAttributedString.Key.paragraphStyle: paragraph, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!] as [AnyHashable : Any]
//            let attributedString = try! NSAttributedString(data: htmlData!, options: options as! [NSAttributedString.DocumentReadingOptionKey : Any], documentAttributes: nil)
            
            let attr = try? NSAttributedString(htmlString: content, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14.0)!)
            self.pageContentLbl.attributedText = attr
            if L102Language.isRTL {
                self.pageContentLbl.textAlignment = .right
                self.pageContentLbl.semanticContentAttribute = .forceRightToLeft
            }
        }
        
    }
    
    
}
extension AboutViewController:SettingsPagesView{
    func onSuccessContactInfo(contactInfo: ContactUsRes) {
        
    }
    
    func showLoadingPageIndicator() {
        self.view.isUserInteractionEnabled = false
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func hideLoadingPageIndicator() {
        self.view.isUserInteractionEnabled = true
        if let sv = spinnerView{
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func onSucessPage(pageObj: Page) {
        self.pageObj = pageObj
        self.setUpUI()
    }
    
    func onErrorPage(message: String) {
        self.showErrorMessage(message: message)
    }
    
    
}
extension AboutViewController: NotificationView{
    func showLoadingIndicator() {
        
    }
    
    func hideLoadingIndicator() {
        
    }
    
    func onError(message: String) {
        
    }
    
    func onSucess(notificationObject: NotificaationsListResponse) {
        
    }
    func onCountSuccess(notificationCount: Int, notificationStatus: Int) {
        UserManager.sharedInstance.notificationCount = notificationCount
        UserManager.sharedInstance.notificationStatus = notificationStatus
        if notificationCount > 0 {
            self.notificationCountLbl.text = (" \(String(notificationCount)) ")
            self.notificationCountLbl.isHidden = false
        }
        else{
            self.notificationCountLbl.isHidden = true
        }
    }
}
