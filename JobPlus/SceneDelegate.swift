//
//  SceneDelegate.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/24/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
import Firebase
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        if(UserManager.sharedInstance.isLogined()) {
            self.window = UIWindow(frame: windowScene.coordinateSpace.bounds)
            self.window?.windowScene = windowScene
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            window?.rootViewController = storyBoard.instantiateInitialViewController()
            window?.makeKeyAndVisible()
        }
        else{
            window = UIWindow()
            window?.makeKeyAndVisible()
            self.window = UIWindow(frame: windowScene.coordinateSpace.bounds)
            self.window?.windowScene = windowScene
            let introDone = UserDefaults.standard.integer(forKey: "introDone")
            if introDone == 1 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Authorizations", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
                window?.makeKeyAndVisible()
            }
            else{
                if let windowScene = scene as? UIWindowScene {
                        let window = UIWindow(windowScene: windowScene)
                    let layout = UICollectionViewFlowLayout()
                    layout.scrollDirection = .horizontal
                    let swipeviewcontroller = SwipeViewController(collectionViewLayout: layout)
                        window.rootViewController = swipeviewcontroller
                        self.window = window
                        window.makeKeyAndVisible()
                    }
            }
        }
        if let userActivity = connectionOptions.userActivities.first {
            if let incomingURL = userActivity.webpageURL {
                _ = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                    guard error == nil else { return }
                    guard let incomingUrl = dynamicLink?.url else {
                        print("no dynamic url")
                        return
                    }
                    print("recieved dynamicURL \(incomingUrl.absoluteURL)")
                    let urlComponents = (String(incomingUrl.absoluteString)).split(separator: "/")
                    print(urlComponents)
                    if urlComponents[1].contains("www.jobplus.biz") || urlComponents[1].contains("jobplus.page.link") {
                        let lastComponent = (urlComponents[urlComponents.count - 1]).split(separator: "=")
                        let id = lastComponent[1]
                        print(id)
                        UserManager.sharedInstance.openDynamicLinkJobID = Int(id) ?? -1
                        UserManager.sharedInstance.openDynamicLink = true
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let mainViewController = mainStoryBoard.instantiateInitialViewController()
                        self.window?.rootViewController = mainViewController
                    }
                }
            }
        }
    }
    
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity){
        if let incomingUrl = userActivity.webpageURL {
            print(incomingUrl)
            
            
        }
        _ = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            guard error == nil else {
                print("Error handling dynamic link \(String(describing: error?.localizedDescription))")
                return
            }
            if dynamiclink != nil {
                self.handleIncomingDynamicLink(dynamiclink!)
            }
        }
    }
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink){
        guard let incomingUrl = dynamicLink.url else {
            print("no dynamic url")
            return
        }
        print("recieved dynamicURL \(incomingUrl.absoluteURL)")
        let urlComponents = (String(incomingUrl.absoluteString)).split(separator: "/")
        print(urlComponents)
        if urlComponents[1].contains("www.jobplus.biz") || urlComponents[1].contains("jobplus.page.link") {
            let lastComponent = (urlComponents[urlComponents.count - 1]).split(separator: "=")
            let id = lastComponent[1]
            print(id)
            UserManager.sharedInstance.openDynamicLinkJobID = Int(id) ?? -1
            UserManager.sharedInstance.openDynamicLink = true
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateInitialViewController()
            self.window?.rootViewController = mainViewController
        }
    }
    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

