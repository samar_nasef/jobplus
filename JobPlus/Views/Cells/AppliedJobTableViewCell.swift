//
//  AppliedJobTableViewCell.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import Kingfisher
class AppliedJobTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryIconImageView: UIImageView!
    @IBOutlet weak var appliedJobType: UIButton!
    @IBOutlet weak var jobContainerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var appliedJobTitleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var appliedJobCategoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpUI(){
        //jobContainerView.dropShadow()
        appliedJobType.dropShadow()
    }
    
    var appliedJob: Job! {
        didSet{
            if let appliedJobObj = appliedJob  {
                
               
                if let title = appliedJobObj.title{
                    self.appliedJobTitleLabel.text = title
                }else {
                    self.appliedJobTitleLabel.text = " "
                }
                if let category = appliedJobObj.jobCategory {
                    if let categoryIcon = category.imagePath {
                        if let categoryIconUrl =  URL(string: categoryIcon){
                            self.categoryIconImageView.kf.setImage(with: categoryIconUrl)
                        }
                    }else {
                       
                            self.categoryIconImageView.image = #imageLiteral(resourceName: "categoryDefault")
                    }
                    
                    self.appliedJobCategoryName.text = category.name
                    
                }
                
                if let type = appliedJobObj.jobType {
                    if let color = type.color {
                        self.appliedJobType.backgroundColor = UIColor(hexFromString: (color))
                    }
                    self.appliedJobType.setTitle(type.name, for: .normal)
                }else {
                    self.appliedJobType.setTitle(" ", for: .normal)
                }
             
                
                if let jobAddress = appliedJobObj.address {
                    self.locationLabel.text = jobAddress
                }else {
                    self.locationLabel.text = " "
                }
                if let date = appliedJobObj.createdAt {
                    self.dateLabel.text = date
                }
                
            }
        }
    }
    
}
