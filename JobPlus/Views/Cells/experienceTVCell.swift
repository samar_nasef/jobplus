//
//  experienceTVCell.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/13/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit
import TweeTextField

class experienceTVCell: UITableViewCell {

    @IBOutlet weak var companyNameTxtField: TweeAttributedTextField!
    @IBOutlet weak var jobTitleTxtField: TweeAttributedTextField!
    @IBOutlet weak var noOfYearsTxtField: TweeAttributedTextField!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var noOfYearsView: UIView!
    @IBOutlet weak var jobTitleView: UIView!
    @IBOutlet weak var sepertorView: UIView!
    @IBOutlet weak var editExp_btn: UIButton!
    @IBOutlet weak var deleteExp_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
