//
//  CatgoeryTableViewCell.swift
//  PartTime
//
//  Created by mohamed shaat on 7/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

class CatgoeryTableViewCell: UITableViewCell {
    @IBOutlet weak var moreImageView: UIImageView!
    @IBOutlet weak var catgoeryImageView: UIImageView!
    @IBOutlet weak var catgoeryNameLabel: UILabel!
    @IBOutlet weak var numberOfJobsLabel: UILabel!
    @IBOutlet weak var catogeryView: UIView!
    override func awakeFromNib() {
        //catogeryView.dropShadow()
        super.awakeFromNib()
        // Initialization code
        self.contentView.changeFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
