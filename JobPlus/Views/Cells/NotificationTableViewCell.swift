//
//  NotificationTableViewCell.swift
//  Najeez
//
//  Created by passant on 6/19/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationTitleLbl: UILabel!
    @IBOutlet weak var notificationDateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
       // super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var notification: Notification! {
        didSet{
            if let notificationObj = notification  {
                
                if let notificationTitle = notificationObj.body as? String {
                    self.notificationTitleLbl.text = notificationTitle
                }
                if let notificationDate = notificationObj.createdAt {
                    self.notificationDateLbl.text = notificationDate
                }
                
            }
        }
    }
}
