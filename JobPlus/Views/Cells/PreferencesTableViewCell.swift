//
//  PreferencesTableViewCell.swift
//  PartTime
//
//  Created by passant on 1/3/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import UIKit

class PreferencesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categorySelectImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
}
extension PreferencesTableViewCell: ConfigurableCell{
    
    func configure(data: CategoryItem) {
        if L102Language.isRTL {
            if let categoryName = data.nameAr {
                self.categoryNameLabel.text = categoryName
            }
        }
        else{
            if let categoryName = data.name {
                self.categoryNameLabel.text = categoryName
            }
        }
        
    }
    func configureworkSectorItem(data: workSector) {
        if L102Language.isRTL {
            if let categoryName = data.nameAr {
                self.categoryNameLabel.text = categoryName
            }
        }
        else{
            if let categoryName = data.nameEn {
                self.categoryNameLabel.text = categoryName
            }
        }
        
    }
    func configureworktims(data: WorkTimeItem) {
        if L102Language.isRTL {
            if let categoryName = data.worktimeAr {
                self.categoryNameLabel.text = categoryName
            }
        }
        else{
            if let categoryName = data.worktimeEn {
                self.categoryNameLabel.text = categoryName
            }
        }
        
    }
    func configureJobTypeItem(data: JobTypeItem) {
        if L102Language.isRTL {
            if let categoryName = data.nameAr {
                self.categoryNameLabel.text = categoryName
            }
        }
        else{
            if let categoryName = data.name {
                self.categoryNameLabel.text = categoryName
            }
        }
        
    }
    
    func configureCompanyItem(data: Company) {
        if L102Language.isRTL {
            if let categoryName = data.name {
                self.categoryNameLabel.text = categoryName
            }
        }
        else{
            if let categoryName = data.name {
                self.categoryNameLabel.text = categoryName
            }
        }
        
    }
    
    func configureCityItem(data: City) {
        if L102Language.isRTL {
            if let categoryName = data.cityAr {
                self.categoryNameLabel.text = categoryName
            }
        }
        else{
            if let categoryName = data.cityEn {
                self.categoryNameLabel.text = categoryName
            }
        }
        
    }
}

