//
//  MyAnswersTableViewCell.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

class MyAnswersTableViewCell: UITableViewCell {

    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var answerContainerView: UIView!
    @IBOutlet weak var questionNumberLabel: UILabel!
    @IBOutlet weak var answerView: UIView!
    
    var viewAnswersCase = FAQ.viewAnswers
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpUI()
        self.contentView.changeFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
     //   super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpUI(){
        
        self.answerContainerView.dropShadow()
        
        let shadowView = UIView()
        shadowView.dropShadow()
        self.questionNumberLabel.addSubview(shadowView)
    }
    
    var quesAnswer: QuestionsAnswer! {
        didSet{
            if let quesAnswerObj = quesAnswer  {
                if let ques = quesAnswerObj.question{
                    if ques.containsOnlyEnglishLetters() {
                        self.questionTitleLabel.textAlignment = .left
                    }else {
                        self.questionTitleLabel.textAlignment = .right
                    }
                    
                    self.questionTitleLabel.text = ques
                }
//                if let answer = quesAnswerObj.answer{
//                    if answer.containsOnlyEnglishLetters() {
//                        self.answerLabel.textAlignment = .left
//                    }else {
//                        self.answerLabel.textAlignment = .right
//                    }
                    self.answerView.layer.cornerRadius = 6.7
                    if let content = quesAnswerObj.answer {
                        print(content)
                        
//                        let htmlData = NSString(string: content).data(using: String.Encoding.unicode.rawValue)
//                        let paragraph = NSMutableParagraphStyle()
//                        if L102Language.currentAppleLanguage() == "ar" {
//
//                            paragraph.alignment = .right
//                        }else {
//                             paragraph.alignment = .left
//                        }
//
//                        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html , NSAttributedString.Key.paragraphStyle: paragraph, NSAttributedString.Key.font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!] as [AnyHashable : Any]
//                        let attributedString = try! NSAttributedString(data: htmlData!, options: options as! [NSAttributedString.DocumentReadingOptionKey : Any], documentAttributes: nil)
                        let attr = try? NSAttributedString(htmlString: content, font: UIFont(name: "HelveticaNeueLTArabic-Roman", size: 14.0)!)
                        self.answerLabel.attributedText = attr
                    }
                    
                
                /*
                if viewAnswersCase == .viewAnswers {
                
                if let answer = quesAnswerObj.answer{
                    if answer.containsOnlyEnglishLetters() {
                        self.answerLabel.textAlignment = .left
                    }else {
                        self.answerLabel.textAlignment = .right
                    }
                    self.answerLabel.text = answer
                }
                
                }else {
                    if let answer = quesAnswerObj.answer{
                        print(answer)
                        
                        let htmlData = NSString(string: answer).data(using: String.Encoding.unicode.rawValue)
                        
                        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
                        
                        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
                        
                        if answer.containsOnlyEnglishLetters() {
                            self.answerLabel.textAlignment = .left
                        }else {
                            self.answerLabel.textAlignment = .right
                        }
                        
                        self.answerLabel.attributedText = attributedString
                    }
                }
                */
            }
        }
    }
    
}
