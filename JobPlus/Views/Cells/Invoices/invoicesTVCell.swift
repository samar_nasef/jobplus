//
//  invoicesTVCell.swift
//  JobPlus
//
//  Created by elsaid yousif on 19/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import UIKit

class invoicesTVCell: UITableViewCell {

    @IBOutlet weak var whiteContainerView: UIView!
    @IBOutlet weak var subscriptionTpeLbl: UILabel!
    @IBOutlet weak var subDateLbl: UILabel!
    @IBOutlet weak var invoiceNoLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var SubStatusBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
