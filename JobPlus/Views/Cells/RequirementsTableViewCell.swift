//
//  RequirementsTableViewCell.swift
//  PartTime
//
//  Created by passant on 11/1/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

class RequirementsTableViewCell: UITableViewCell {
    @IBOutlet weak var requirementLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.changeFont()
        if L102Language.currentAppleLanguage() == "ar" {
            self.semanticContentAttribute = .forceRightToLeft
        }else {
            self.semanticContentAttribute = .forceLeftToRight
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
       // super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
