//
//  homeCatsCVCell.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import UIKit

class homeCatsCVCell: UICollectionViewCell {
    @IBOutlet weak var addToFavBtn: UIButton!
    @IBOutlet weak var catImg: UIImageView!
    
    @IBOutlet weak var catNameLbl: UILabel!
}
