//
//  MenuTableViewCell.swift
//  NajeezAgent
//
//  Created by passant on 5/13/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var onlineSwitch: UISwitch!
    @IBOutlet weak var onlineLbl: UILabel!
    @IBOutlet weak var menuIconImgView: UIImageView!
    @IBOutlet weak var menuLbl: UILabel!
    @IBOutlet weak var underLineView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.changeFont()
        self.underLineView.alpha = 0
        self.languageLabel.alpha = 0
        self.languageBtn.isHidden = true
    }
    
    open func setData(_ data: Any?) {
        if let menuData = data as? String {
            self.menuLbl.text = menuData
        }
    }
    

}
