//
//  JobTableViewCell.swift
//  PartTime
//
//  Created by mohamed shaat on 8/2/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
import DynamicBlurView
class JobTableViewCell: UITableViewCell {
    @IBOutlet weak var applyBtnView: UIView!
    @IBOutlet weak var disableImgView: UIImageView!
    @IBOutlet weak var applyBtn: UILabel!
    @IBOutlet weak var blurView: DynamicBlurView!
    @IBOutlet weak var initialView: UIView!
    @IBOutlet weak var jobContainer: UIView!
    @IBOutlet weak var jobTypeLabel: UIButton!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var jobCatgoeryImageview: UIImageView!
    @IBOutlet weak var jobAddressLabel: UILabel!
    @IBOutlet weak var jobCatgoeryNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        jobContainer.dropShadow(scale:true)
        // Initialization code
 
            blurView.alpha = 0
            //applyBtnView.dropShadow()
        self.contentView.changeFont()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCell(job:Job) {
        if let jobType = job.jobType {
        self.jobTypeLabel.setTitle(jobType.name, for: [])
        self.jobTypeLabel.backgroundColor = UIColor(hexFromString: (jobType.color)!)
        }
        if let jobTitle = job.title {
            if !jobTitle.containsOnlyEnglishLetters(){
                jobNameLabel.font = FontHelper.defaultArabicRegularFontWithSize(size: 20)
            }else {
                jobNameLabel.font = FontHelper.defaultMediumFontWithSize(size: 20)
            }
        self.jobNameLabel.text = jobTitle
        }else{
            self.jobNameLabel.text = " "
        }
        if let jobAddress = job.address {
        self.jobAddressLabel.text = jobAddress
        }else {
            self.jobAddressLabel.text = " "
        }
        if let jobCategoryImg = job.jobCategory?.imagePath{
            if let catgoeryImgUrl = URL(string: jobCategoryImg ){
                self.jobCatgoeryImageview.kf.setImage(with: catgoeryImgUrl)
            }
        }else {
            jobCatgoeryImageview.image = #imageLiteral(resourceName: "categoryDefault")
        }
        if let jobCatgoeryName = job.jobCategory?.name {
            self.jobCatgoeryNameLabel.text = jobCatgoeryName
        }else {
            
        }
        if let date = job.createdAt {
            self.dateLabel.text = date
        }
      // self.dateLabel.text = "23/12/15"
        
        if let appliedForJob = job.appliedForJob {
            if appliedForJob {
                self.applyBtn.text = "Applied".localized()
            }else {
                self.applyBtn.text = "Apply".localized()
            }
        }else {
            self.applyBtn.text = "Apply".localized()
        }
        if UserManager.sharedInstance.getUser() == nil {
            self.applyBtn.textColor = UIColor.manatee
            self.applyBtn.backgroundColor = UIColor.clear
            disableImgView.alpha = 1
            blurView.blurRadius = 4
        }else {
            self.applyBtn.textColor = UIColor.white
            self.applyBtn.backgroundColor = UIColor.greenBlue
            disableImgView.alpha = 0
            blurView.alpha = 0
        }
    }

}
