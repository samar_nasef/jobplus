//
//  JobsView.swift
//  PartTime
//
//  Created by mohamed shaat on 8/6/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

protocol  JobsView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(jobsResponse:JobListResponse)
    func onSuccessNextLinkCall(jobsResponse:JobListResponse)
    func onError(message:String)
}
