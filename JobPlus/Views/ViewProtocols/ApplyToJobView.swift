//
//  ApplyToJobView.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation

protocol  ApplyToJobView {
    func showLoadingApplyIndicator()
    func hideLoadingApplyIndicator()
//    func onSucessApply(status: String)
    func onSucessApply(status:String,message:String)
    func onErrorApply(message:String)
}
