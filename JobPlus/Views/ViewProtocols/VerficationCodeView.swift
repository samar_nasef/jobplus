//
//  editFirebaseResponse.swift
//  Najeez
//
//  Created by passant on 5/22/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol VerficationCodeView {
    func successChangePhone(message:String)
    func showVerficationLoadingIndicator()
    func hideVerficationLoadingIndicator()
    func onSucessVerfication()
    func onErrorVerfication(message:String)
    
    func showResendLoadingIndicator()
    func hideResendLoadingIndicator()
    func onSucessResend(message:String)
}
