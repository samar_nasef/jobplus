//
//  JobDetailsView.swift
//  PartTime
//
//  Created by passant on 1/3/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
protocol JobDetailsView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucessLoadingJob(job:jobDetail)
    func onSucessLoadingDatils(job:jobDetail,numberOfAppliedJobsThisMonth:Int,FullScreenAdAfter:Int)
    func onError(message:String)
}
