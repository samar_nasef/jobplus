//
//  CountryListView.swift
//  PartTime
//
//  Created by passant on 2/5/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
protocol CountryListView {
    func onSucessLoadingCountryList(countries:[CountryRes])
    func onError(message:String)
}
