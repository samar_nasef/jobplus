//
//  ForgetPasswordView.swift
//  Najeez
//
//  Created by passant on 5/20/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol ForgetPasswordView {

    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess()
    func onError(message:String)
}
