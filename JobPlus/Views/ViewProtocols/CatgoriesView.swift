//
//  CatgoriesView.swift
//  PartTime
//
//  Created by mohamed shaat on 7/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

protocol CatgoriesView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(catgoriesResponse:FavoritesRes)
    func onError(message:String)
}
