//
//  changePhoneView.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/18/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation
protocol CheckPhoneForChangeView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess()
    func onError(message:String)
}
