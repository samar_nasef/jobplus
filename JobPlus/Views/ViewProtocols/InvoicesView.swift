//
//  InvoicesView.swift
//  JobPlus
//
//  Created by elsaid yousif on 19/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import Foundation

protocol InvoicesView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(Response:[InvoiceItem])
    func onError(message:String)
}
