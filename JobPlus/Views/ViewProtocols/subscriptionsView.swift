//
//  subscriptionsView.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/3/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import UIKit

protocol subscriptionsView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSuccessHyperPay(URLString: String)
//    func onSucess(subscriptionResponse:SubscriptionResponse)
    func onSucess(subscriptionResponse:packagesRes)
    func onSuccessNewPackages(Res: newPacakagesRes)
    func onError(message:String)
    func onSuccessAddTransaction()
    func onAddTransactionError()
    func iapShowLoading()
    func iapHideLoading()
}


protocol PaymentInfoView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
//    func onSucess(subscriptionResponse:SubscriptionResponse)
    func onSucess(paymentResponse:paymentRes)
    func onError(message:String)
}

