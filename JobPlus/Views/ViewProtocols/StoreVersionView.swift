//
//  StoreVersionView.swift
//  JobPlus
//
//  Created by elsaid yousif on 29/01/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation

protocol StoreVersionView {
    func storeVersionSuccessCallback(storeVersion: String)
}
