//
//  DeleteAccountView.swift
//  JobPlus
//
//  Created by elsaid yousif on 23/10/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import Foundation
protocol DeleteAccountView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucessDeleteAccount(message:String)
    func onErrorDeleteAccount(message:String)
}
