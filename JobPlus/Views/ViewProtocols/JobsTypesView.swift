//
//  JobsView.swift
//  PartTime
//
//  Created by mohamed shaat on 8/2/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

protocol  JobsTypesView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(jobsResponse:JobsTypesResponse)
    func onError(message:String)
}
