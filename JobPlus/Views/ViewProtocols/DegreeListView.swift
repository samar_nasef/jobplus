//
//  DegreeListView.swift
//  PartTime
//
//  Created by passant on 2/5/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
protocol DegreeListView {
    func showLoading()
    func hideLoading()
    func onSucessLoadingDegreeList(degreeWorkingTimes:DegreesTimesApiRes)
    func onSucessLoadingNewCeritficateList(degreeWorkingTimes:NewCertificateApiRes)
    func onError(message:String)
}
