//
//  searchView.swift
//  JobPlus
//
//  Created by elsaid yousif on 12/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation

protocol SearchView {
    func showLoading()
    func hideLoading()
    func onSuccessGetCities(countries: [countryItem])
    func onSuccessGetWorkingTimes(worktimes: [worktims])
    func onSuccessGetCompanies(companies: [companyItem])
    func onSuccessGetWorkSectors(sectors: [workSectorItem])
    func onSuccessGetDiabilities(disabilities: [DisabilityTypeItem])
    func onSuccessGetSpecialities(specialities: [specialityItem])
    func onSuccessGetExperiences(experience: [experienceItem])
    func onError(message:String)
}
