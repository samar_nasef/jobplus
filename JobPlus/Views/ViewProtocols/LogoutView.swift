//
//  LogoutView.swift
//  Najeez
//
//  Created by passant on 5/17/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol LogoutView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(message:String)
    func onError(message:String)
}
