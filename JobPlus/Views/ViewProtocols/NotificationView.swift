//
//  NotificationView.swift
//  Najeez
//
//  Created by passant on 6/19/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol NotificationView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(notificationObject:NotificaationsListResponse)
    func onError(message:String)
    func onCountSuccess(notificationCount: Int, notificationStatus: Int)
}
