//
//  arabClicksView.swift
//  JobPlus
//
//  Created by samar nasef on 02/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
protocol  arabClicksView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucessGetCategoriesAndMarkets(data:categoriesAndMarketsRes)
    func onSucessGetarabClicksOffers(data: arabClicksOffersRes)
    func onError(message:String)
}
