//
//  RegisterView.swift
//  NajeezAgent
//
//  Created by passant on 5/9/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol RegisterView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess()
    func onError(message:String)
    func onSuccessGetCaptch(captchaCode: String)
}
