//
//  UserInfoView.swift
//  NajeezAgent
//
//  Created by passant on 7/9/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol UserInfoView {
    func showLoadingUserInfoIndicator()
    func hideLoadingUserInfoIndicator()
    func onSucessUserInfo()
    func onErrorUserInfo(message:String)
}
