//
//  FAQView.swift
//  PartTime
//
//  Created by passant on 1/6/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
protocol FAQView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucessLoading(faqResponse:FAQResponse)
    func onError(message:String)
}
