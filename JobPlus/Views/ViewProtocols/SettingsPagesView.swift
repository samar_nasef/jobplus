//
//  SettingsPagesView.swift
//  Najeez
//
//  Created by passant on 6/3/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol SettingsPagesView {
    func showLoadingPageIndicator()
    func hideLoadingPageIndicator()
    func onSucessPage(pageObj:Page)
    func onErrorPage(message:String)
    func onSuccessContactInfo(contactInfo: ContactUsRes)
}
