//
//  homeCatsView.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation

protocol HomeCatsView {
    func showLoading()
    func hideLoading()
    func onSucessGetHomeCats(homeCats:[catItem])
    func onSuccessAddFav(index: Int)
    func onSuccessRemoveFav(index: Int)
    func onError(message:String)
}
