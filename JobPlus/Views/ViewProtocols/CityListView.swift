//
//  CityListView.swift
//  PartTime
//
//  Created by passant on 1/6/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
protocol CityListView {
    func showLoadingCityListIndicator()
    func hideLoadingCityListIndicator()
    func onSucessLoadingCityList(cities:[City])
    func onError(message:String)
}
