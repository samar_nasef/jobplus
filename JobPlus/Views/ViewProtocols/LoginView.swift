//
//  LoginView.swift
//  NajeezAgent
//
//  Created by passant on 5/8/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
protocol LoginView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess()
    func onError(message:String)
}
