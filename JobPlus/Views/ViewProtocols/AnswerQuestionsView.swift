//
//  AnswerQuestionsView.swift
//  PartTime
//
//  Created by passant on 8/28/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import UIKit

protocol  AnswerQuestionsView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(questionsListResponse:QuestionListResponse)
    func onError(message:String)
}
