//
//  AppliedJobsView.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation

protocol  AppliedJobsView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(appliedJobsListResponse:JobListResponse)
    func onSuccessNextLinkCall(appliedJobsResponse:JobListResponse)
    func onError(message:String)
}
