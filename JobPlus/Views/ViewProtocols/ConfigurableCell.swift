//
//  ConfigurableCell.swift
//  squadPartner
//
//  Created by Ahmed on 11/1/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation

protocol ConfigurableCell { // Implemented in UIViewCell
    static var reuseIdentifier: String { get }
    static func getCellIdentifier() -> String
    associatedtype DataType
    associatedtype DataType2
    associatedtype DataType3
    associatedtype DataType4
    associatedtype DataType5
    associatedtype DataType6
    func configure(data: DataType)
    func configureworkSectorItem(data: DataType2)
    func configureworktims(data: DataType3)
    func configureJobTypeItem(data: DataType4)
    func configureCompanyItem(data: DataType5)
    func configureCityItem(data: DataType6)
}

extension ConfigurableCell {
    static var reuseIdentifier: String { return String(describing: Self.self) }
    static func getCellIdentifier() -> String {
        return reuseIdentifier
    }
}
