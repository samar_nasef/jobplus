//
//  ListFeedbackView.swift
//  JobPlus
//
//  Created by elsaid yousif on 01/02/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation

protocol ListAddFeedBackView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucess(feedBack: [feedBackItem])
    func onSuccessAddFeedback(message: String)
    func onError()
    func onErrorAddFeedback(message:String)
}
