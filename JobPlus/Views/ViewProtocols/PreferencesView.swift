//
//  PreferencesView.swift
//  PartTime
//
//  Created by passant on 1/6/19.
//  Copyright © 2019 mohamed shaat. All rights reserved.
//

import Foundation
protocol PreferencesView {
    func showLoadingPreferencesIndicator()
    func hideLoadingPreferencesIndicator()
    func onSucess()
    func onError(message:String)
}
