//
//  messagesView.swift
//  JobPlus
//
//  Created by samar nasef on 13/07/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
protocol  messagesView {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func onSucessGetAllMessages(data:allMessagesApiRes)
    func onSucessMessageDetails(msg:String)
    func onSucessAddNewMessageOrReplay(msg:String)
    func onError(message:String)
}
