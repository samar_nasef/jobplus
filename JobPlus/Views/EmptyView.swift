//
//  EmptyView.swift
//  PartTime
//
//  Created by passant on 9/16/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit
class EmptyView: UIView {
    
    let nibName = "EmptyView"
    var contentView: UIView?
    @IBOutlet weak var msgLbl: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        
        
}
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
