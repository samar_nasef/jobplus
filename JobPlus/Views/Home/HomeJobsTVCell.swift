//
//  HomeJobsTVCell.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/26/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
//import GoogleMobileAds
//class adsTVCell:UITableViewCell{
//    
//    @IBOutlet weak var BannerView: GADBannerView!
//}

class HomeJobsTVCell: UITableViewCell {

    @IBOutlet weak var JobImg: UIImageView!
    @IBOutlet weak var disability_ic: UIImageView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var JobType: UILabel!
    @IBOutlet weak var JobCompany: UILabel!
    @IBOutlet weak var JobLocation: UILabel!
    @IBOutlet weak var JobPostedTime: UILabel!
    @IBOutlet weak var ApplyJobBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var whiteContainerView: UIView!
    @IBOutlet weak var AppliedCompany: UILabel!
    @IBOutlet weak var jobStatusIcon: UIImageView!
    @IBOutlet weak var comNameHeightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

