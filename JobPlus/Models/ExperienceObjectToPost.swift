//
//	ExperienceObjectToPost.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ExperienceObjectToPost : NSObject, NSCoding, Mappable{

    var experiences : [Experience2] = [Experience2]()


	class func newInstance(map: Map) -> Mappable?{
		return ExperienceObjectToPost()
	}
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
	{
		experiences <- map["experiences"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        experiences = (aDecoder.decodeObject(forKey: "experiences") as? [Experience2])!

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if experiences != nil{
            aCoder.encode(experiences, forKey: "experiences")
		}

	}

}

public final class skillObj: NSObject, NSCoding, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let skill_id = "skill_id"
        static let skillTyp = "skillTyp"
        static let skillText = "skillText"
    }
    
    // MARK: Properties
    public var skill_id: Int?
    public var skillTyp: Int?
    public var skillText: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    init(skill_id: Int?, skillTyp: Int?, skillText: String?) {
        if skill_id != nil {
            self.skill_id = skill_id ?? 0
        }
        if skillTyp != nil {
            self.skillTyp = skillTyp ?? 0
        }
        if skillText != nil {
            self.skillText = skillText ?? ""
        }
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        if skill_id != nil {
            skill_id <- map[SerializationKeys.skill_id]
        }
        if skillTyp != nil {
            skillTyp <- map[SerializationKeys.skillTyp]
        }
        if skillText != nil {
            skillText <- map[SerializationKeys.skillText]
        }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if skill_id != nil {
            dictionary[SerializationKeys.skill_id] = skill_id
        }
        if skillTyp != nil {
            dictionary[SerializationKeys.skillTyp] = skillTyp
        }
        if skillText != nil {
            dictionary[SerializationKeys.skillText] = skillText
        }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        if skill_id != nil {
            self.skill_id = (aDecoder.decodeObject(forKey: SerializationKeys.skill_id) as? Int)!
        }
        if skillTyp != nil {
            self.skillTyp = (aDecoder.decodeObject(forKey: SerializationKeys.skillTyp) as? Int)!
        }
        if skillText != nil {
            self.skillText = (aDecoder.decodeObject(forKey: SerializationKeys.skillText) as? String)!
        }
    }
    
    public func encode(with aCoder: NSCoder) {
        if skill_id != nil {
            aCoder.encode(skill_id, forKey: SerializationKeys.skill_id)
        }
        if skillTyp != nil {
            aCoder.encode(skillTyp, forKey: SerializationKeys.skillTyp)
        }
        if skillText != nil {
            aCoder.encode(skillText, forKey: SerializationKeys.skillText)
        }
    }
    
}
public final class CertificateObj: NSObject, NSCoding, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
//        static let id = "id"
        static let certificateName = "certificateName"
        static let DateOfIssue = "DateOfIssue"
        static let CertificateEndDate = "CertificateEndDate"
        static let HasIssueDate = "HasIssueDate"
        static let InstitutionID = "InstitutionID"
        static let InstitutionText = "InstitutionText"
      
    }
    
    // MARK: Properties
    public var certificateName: String = ""
    public var DateOfIssue: Int?
    public var CertificateEndDate: Int?
    public var HasIssueDate: Int?
    public var InstitutionID: Int?
    public var InstitutionText: String?
//    public var id : Int = 0
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    init(certificateName: String,DateOfIssue: Int?,CertificateEndDate: Int?,HasIssueDate: Int?, InstitutionID: Int?, InstitutionText: String? ) {
//        ,id:Int
//        self.id = id
        self.certificateName = certificateName
        self.DateOfIssue = DateOfIssue
        self.CertificateEndDate = CertificateEndDate
        self.HasIssueDate = HasIssueDate
        self.InstitutionID = InstitutionID
        self.InstitutionText = InstitutionText
        
      
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
//        id <- map[SerializationKeys.id]
        certificateName <- map[SerializationKeys.certificateName]
        DateOfIssue <- map[SerializationKeys.DateOfIssue]
        CertificateEndDate <- map[SerializationKeys.CertificateEndDate]
        HasIssueDate <- map[SerializationKeys.HasIssueDate]
        InstitutionID <- map[SerializationKeys.InstitutionID]
        InstitutionText <- map[SerializationKeys.InstitutionText]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        dictionary[SerializationKeys.DateOfIssue] = DateOfIssue
        dictionary[SerializationKeys.certificateName] = certificateName
        dictionary[SerializationKeys.CertificateEndDate] = CertificateEndDate
        dictionary[SerializationKeys.HasIssueDate] = HasIssueDate
        dictionary[SerializationKeys.InstitutionID] = InstitutionID
        dictionary[SerializationKeys.InstitutionText] = InstitutionText
     
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.certificateName = (aDecoder.decodeObject(forKey: SerializationKeys.certificateName) as? String)!
        
        self.DateOfIssue = (aDecoder.decodeObject(forKey: SerializationKeys.DateOfIssue) as? Int)!
        
        self.CertificateEndDate = (aDecoder.decodeObject(forKey: SerializationKeys.CertificateEndDate) as? Int)!
        
        self.HasIssueDate = (aDecoder.decodeObject(forKey: SerializationKeys.HasIssueDate) as? Int)!
        self.InstitutionID = (aDecoder.decodeObject(forKey: SerializationKeys.InstitutionID) as? Int)!
        self.InstitutionText = (aDecoder.decodeObject(forKey: SerializationKeys.InstitutionText) as? String)!
       
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(certificateName, forKey: SerializationKeys.certificateName)
        aCoder.encode(DateOfIssue, forKey: SerializationKeys.DateOfIssue)
        
        aCoder.encode(CertificateEndDate, forKey: SerializationKeys.CertificateEndDate)
        aCoder.encode(HasIssueDate, forKey: SerializationKeys.HasIssueDate)
        aCoder.encode(InstitutionID, forKey: SerializationKeys.InstitutionID)
        aCoder.encode(InstitutionText, forKey: SerializationKeys.InstitutionText)
       
    }
    
}

public final class educationalObj: NSObject, NSCoding, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        
//        static let id = "id"
        static let InstitutionID = "InstitutionID"
        static let Institution_Name = "Institution_Name"
        static let certificate_id = "certificate_id"
        static let country_id = "country_id"
        static let majorName = "majorName"
        static let grades = "grades"
        static let toDate = "toDate"
        static let fromDate = "fromDate"
        static let specialitilty_id = "specialitilty_id"
    }
    
    // MARK: Properties
    public var majorName: String = ""
    static let majorName = "majorName"
    public var grades: String = ""
    public var Institution_Name: String = ""
    public var InstitutionID: Int = 0
    public var country_id: Int?
    public var certificate_id: Int = 0
    public var toDate : Int?
    public var fromDate: Int?
    public var specialitilty_id: Int?
    
//    public var id : Int = 0
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    init(Institution_Name: String,InstitutionID: Int,country_id: Int?,certificate_id: Int,majorName:String,grades: String,toDate:Int?,fromDate:Int?, specialitilty_id: Int? ) {
//        ,id:Int
//        self.id = id
        self.Institution_Name = Institution_Name
        self.InstitutionID = InstitutionID
        self.country_id = country_id
        self.certificate_id = certificate_id
        self.majorName = majorName
        self.grades = grades
        self.toDate = toDate
        self.fromDate = fromDate
        self.specialitilty_id = specialitilty_id
        
      
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
//        id <- map[SerializationKeys.id]
        Institution_Name <- map[SerializationKeys.Institution_Name]
        InstitutionID <- map[SerializationKeys.InstitutionID]
        country_id <- map[SerializationKeys.country_id]
        certificate_id <- map[SerializationKeys.certificate_id]
        majorName <- map[SerializationKeys.majorName]
        grades <- map[SerializationKeys.grades]
        toDate <- map[SerializationKeys.toDate]
        fromDate <- map[SerializationKeys.fromDate]
        specialitilty_id <- map[SerializationKeys.specialitilty_id]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        dictionary[SerializationKeys.Institution_Name] = Institution_Name
        dictionary[SerializationKeys.InstitutionID] = InstitutionID
        dictionary[SerializationKeys.country_id] = country_id
        dictionary[SerializationKeys.certificate_id] = certificate_id
        dictionary[SerializationKeys.majorName] = majorName
        dictionary[SerializationKeys.grades] = grades
        dictionary[SerializationKeys.toDate] = toDate
        dictionary[SerializationKeys.fromDate] = fromDate
        dictionary[SerializationKeys.specialitilty_id] = specialitilty_id
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.Institution_Name = (aDecoder.decodeObject(forKey: SerializationKeys.Institution_Name) as? String)!
        
        self.InstitutionID = (aDecoder.decodeObject(forKey: SerializationKeys.InstitutionID) as? Int)!
        
        self.country_id = (aDecoder.decodeObject(forKey: SerializationKeys.country_id) as? Int)!
        
        self.certificate_id = (aDecoder.decodeObject(forKey: SerializationKeys.certificate_id) as? Int)!
        self.majorName = (aDecoder.decodeObject(forKey: SerializationKeys.majorName) as? String)!
        self.grades = (aDecoder.decodeObject(forKey: SerializationKeys.grades) as? String)!
        self.toDate = (aDecoder.decodeObject(forKey: SerializationKeys.toDate) as? Int)!
        self.fromDate = (aDecoder.decodeObject(forKey: SerializationKeys.fromDate) as? Int)!
        self.specialitilty_id = (aDecoder.decodeObject(forKey: SerializationKeys.specialitilty_id) as? Int)!
        
       
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(Institution_Name, forKey: SerializationKeys.Institution_Name)
        aCoder.encode(InstitutionID, forKey: SerializationKeys.InstitutionID)
        
        aCoder.encode(country_id, forKey: SerializationKeys.country_id)
        aCoder.encode(certificate_id, forKey: SerializationKeys.certificate_id)
        aCoder.encode(majorName, forKey: SerializationKeys.majorName)
        aCoder.encode(grades, forKey: SerializationKeys.grades)
        aCoder.encode(toDate, forKey: SerializationKeys.toDate)
        aCoder.encode(fromDate, forKey: SerializationKeys.fromDate)
        aCoder.encode(specialitilty_id, forKey: SerializationKeys.specialitilty_id)
        
       
    }
    
}


class certificateObjectToPost : NSObject, NSCoding, Mappable{

    var userCertificates : [CertificateObj] = [CertificateObj]()
 


    class func newInstance(map: Map) -> Mappable?{
        return certificateObjectToPost()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        userCertificates <- map["userCertificates"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        userCertificates = (aDecoder.decodeObject(forKey: "userCertificates") as? [CertificateObj])!

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if userCertificates != nil{
            aCoder.encode(userCertificates, forKey: "userCertificates")
        }

    }

}

class eduactionalObjectToPost : NSObject, NSCoding, Mappable{

    var EducationalInformation : [educationalObj] = [educationalObj]()
 


    class func newInstance(map: Map) -> Mappable?{
        return eduactionalObjectToPost()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        EducationalInformation <- map["EducationalInformation"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        EducationalInformation = (aDecoder.decodeObject(forKey: "EducationalInformation") as? [educationalObj])!

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if EducationalInformation != nil{
            aCoder.encode(EducationalInformation, forKey: "EducationalInformation")
        }

    }

}


class SkillObjectToPost : NSObject, NSCoding, Mappable{

    var SoftSkills : [skillObj] = [skillObj]()
 


    class func newInstance(map: Map) -> Mappable?{
        return SkillObjectToPost()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        SoftSkills <- map["SoftSkills"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        SoftSkills = (aDecoder.decodeObject(forKey: "SoftSkills") as? [skillObj])!

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if SoftSkills != nil{
            aCoder.encode(SoftSkills, forKey: "SoftSkills")
        }

    }

}

class languageObjectToPost : NSObject, NSCoding, Mappable{

    var userLanguages : [LanguageObj] = [LanguageObj]()
 


    class func newInstance(map: Map) -> Mappable?{
        return languageObjectToPost()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        userLanguages <- map["userLanguages"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        userLanguages = (aDecoder.decodeObject(forKey: "userLanguages") as? [LanguageObj])!

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if userLanguages != nil{
            aCoder.encode(userLanguages, forKey: "userLanguages")
        }

    }

}
public final class LanguageObj: NSObject, NSCoding, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let language_id = "language_id"
        static let languageStrength_id = "languageStrength_id"
      
    }
    
    // MARK: Properties
    public var language_id: Int = 0
    public var languageStrength_id: Int = 0
    
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    init(language_id: Int,languageStrength_id:Int) {
        self.language_id = language_id
        self.languageStrength_id = languageStrength_id
      
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        language_id <- map[SerializationKeys.language_id]
        languageStrength_id <- map[SerializationKeys.languageStrength_id]
       
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.language_id] = language_id
        dictionary[SerializationKeys.languageStrength_id] = languageStrength_id
     
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.language_id = (aDecoder.decodeObject(forKey: SerializationKeys.language_id) as? Int)!
        self.languageStrength_id = (aDecoder.decodeObject(forKey: SerializationKeys.languageStrength_id) as? Int)!
       
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(language_id, forKey: SerializationKeys.language_id)
        aCoder.encode(languageStrength_id, forKey: SerializationKeys.languageStrength_id)
       
    }
    
}
