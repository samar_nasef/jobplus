//
//  MessagesModel.swift
//  JobPlus
//
//  Created by samar nasef on 13/07/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
struct allMessagesApiRes: Decodable {
    let current_page:Int?
    let data:[messagesItem]?
    let last_page:Int?
   
    
}
struct messagesItem:Decodable {
    let id: Int?
}

