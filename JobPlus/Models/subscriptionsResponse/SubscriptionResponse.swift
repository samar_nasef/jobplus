//
//  SubscriptionResponse.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/3/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation
import ObjectMapper

struct iapSubscribtionRes: Decodable{
    let status: Bool?
}
struct packagesRes:Decodable {
    var Packages:[packagesObj]
    var selectedPackage:selectedPackage?
//    var user_id:Int
    var subscription_date:String?
    var status:Int?
    var free_period_in_days:Int?
    let inAppPurchase: Int?
}
struct newPacakagesRes: Decodable {
    let FreeMonthlyJobs: Int?
    let subscribed: Bool?
    let autosubscribed: Int?
    let AdvancedPrice: String?
    let subscribtionType: String?
    let SubscribeStartDate: String?
    let SubscribeEndDate: String?
    let inAppPurchase: Int?
    let ActualPackage: Int?
    let AcutalPrice: Int?
    let subscribtionMessage: String?
    //let user: User?
}
struct hyperPayRes: Decodable {
    let status: Bool?
    let message: String?
}
struct packagesObj : Decodable{
    
    let id:Int
    var cost:String
    let duration_in_days:String
}
struct selectedPackage:Decodable {
    let id:Int
    let cost:String?
    let duration_in_days:String?
    let package_id:Int
    let endDate:DateObj?
}
struct DateObj:Decodable {
    let date:String
}
struct paymentRes:Decodable{
    let result:String
    let response_code:String
    let payment_url:String
//    let p_id:Int
}

struct PaymentObj {
    var cc_first_name:String
    var cc_last_name:String
    var cc_phone_number:String
    var phone_number:String
    var email:String
    var products_per_title:String
    
    var unit_price:String
    var quantity:Int
    var other_charges:Int
    
    var amount:String
    var discount:String
    
    var currency:String
    var reference_no:String
    var ip_customer:String
    
    var billing_address:String
    var state:String
    var city:String
    var postal_code:String
    
    var country:String
    var shipping_first_name:String
    var shipping_last_name:String
    var address_shipping:String
   
    var city_shipping:String
    
    var state_shipping:String
    var postal_code_shipping:String
    var country_shipping:String
    var msg_lang:String
    
    
  
    
}
class SubscriptionResponse : NSObject, NSCoding, Mappable{

    var subscription : [Subscription]?


    class func newInstance(map: Map) -> Mappable?{
        return SubscriptionResponse()
    }
    required init?(map: Map){}
    override init(){}

    func mapping(map: Map)
    {
        subscription <- map["Packages"]
//         subscription <- map["selectedPackage"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         subscription = aDecoder.decodeObject(forKey: "subscription") as? [Subscription]

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if subscription != nil{
            aCoder.encode(subscription, forKey: "subscription")
        }

    }

}
