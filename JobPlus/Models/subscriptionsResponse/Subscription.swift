//
//  Subscription.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/3/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation
import ObjectMapper


class Subscription : NSObject, NSCoding, Mappable{

    var id : Int?
    var cost : String?
    var duration_in_days : String?


    class func newInstance(map: Map) -> Mappable?{
        return Subscription()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        id <- map["id"]
        cost <- map["cost"]
        duration_in_days <- map["duration_in_days"]
       
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? Int
         cost = aDecoder.decodeObject(forKey: "cost") as?  String
         duration_in_days = aDecoder.decodeObject(forKey: "duration_in_days") as? String
      

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if cost != nil{
            aCoder.encode(cost, forKey: "cost")
        }
        if duration_in_days != nil{
            aCoder.encode(duration_in_days, forKey: "duration_in_days")
        }
       

    }

}
