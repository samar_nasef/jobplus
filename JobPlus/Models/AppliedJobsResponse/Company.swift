//
//	Company.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Company : NSObject, NSCoding, Mappable{
    
    var id : Int?
    var name : String?
    var imagePath :String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return Company()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        imagePath <- map["imagePath"]
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        imagePath = aDecoder.decodeObject(forKey: "imagePath") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if imagePath != nil{
            aCoder.encode(imagePath, forKey: "imagePath")
        }
        
    }
    
}
