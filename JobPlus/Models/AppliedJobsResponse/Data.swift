//
//	Data.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class JobData : NSObject, NSCoding, Mappable{

	var id : Int?
	var job : Job?
	var questionsAnswers : [QuestionsAnswer]?


	class func newInstance(map: Map) -> Mappable?{
		return JobData()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		id <- map["id"]
		job <- map["job"]
		questionsAnswers <- map["questions_answers"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         job = aDecoder.decodeObject(forKey: "job") as? Job
         questionsAnswers = aDecoder.decodeObject(forKey: "questions_answers") as? [QuestionsAnswer]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if job != nil{
			aCoder.encode(job, forKey: "job")
		}
		if questionsAnswers != nil{
			aCoder.encode(questionsAnswers, forKey: "questions_answers")
		}

	}

}
