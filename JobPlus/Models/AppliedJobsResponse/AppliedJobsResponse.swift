//
//	AppliedJobsResponse.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class AppliedJobsResponse : NSObject, NSCoding, Mappable{

	var data : [JobData]?
	var links : Link?
	var meta : Meta?


	class func newInstance(map: Map) -> Mappable?{
		return AppliedJobsResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		data <- map["data"]
		links <- map["links"]
		meta <- map["meta"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObject(forKey: "data") as? [JobData]
         links = aDecoder.decodeObject(forKey: "links") as? Link
         meta = aDecoder.decodeObject(forKey: "meta") as? Meta

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if links != nil{
			aCoder.encode(links, forKey: "links")
		}
		if meta != nil{
			aCoder.encode(meta, forKey: "meta")
		}

	}

}
