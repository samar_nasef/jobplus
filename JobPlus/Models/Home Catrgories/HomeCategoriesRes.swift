//
//  HomeCategoriesRes.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation

struct HomeCategories: Decodable{
    let data: [catItem]?
    let userDeleted: Int?
    let subscribedUser: Bool?
    let NotSubscribedMeassage:String?
    let showOffers: Int?
    let showNewsArticles: Int?
}

struct catItem: Decodable{
    let id: Int?
    let name: String?
    let nameAr: String?
    let jobsCount: Int?
    let imagePath: String?
    var addToFavourite: Bool?
}
