//
//  addVisit.swift
//  JobPlus
//
//  Created by elsaid yousif on 29/01/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation
import ObjectMapper

class AddVisitModel : NSObject, NSCoding, Mappable{

    var status : Bool?
   

    class func newInstance(map: Map) -> Mappable?{
        return AddVisitModel()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        status <- map["status"]
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         
         status = aDecoder.decodeObject(forKey: "status") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }

}
