//
//	Data.swift
//
//	Create by passant on 5/2/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Degree : NSObject, NSCoding, Mappable{

	var key : String?
	var nameAr : String?
	var nameEn : String?


	class func newInstance(map: Map) -> Mappable?{
		return Degree()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		key <- map["key"]
		nameAr <- map["nameAr"]
		nameEn <- map["nameEn"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         key = aDecoder.decodeObject(forKey: "key") as? String
         nameAr = aDecoder.decodeObject(forKey: "nameAr") as? String
         nameEn = aDecoder.decodeObject(forKey: "nameEn") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if key != nil{
			aCoder.encode(key, forKey: "key")
		}
		if nameAr != nil{
			aCoder.encode(nameAr, forKey: "nameAr")
		}
		if nameEn != nil{
			aCoder.encode(nameEn, forKey: "nameEn")
		}

	}

}

extension Degree {
    
    var localizeName: String? {
        if L102Language.currentAppleLanguage() == "ar" {
            return self.nameAr ?? self.nameEn
        }else{
            return self.nameEn ?? self.nameAr
        }
    }
}
