//
//  FavoritesModel.swift
//  JobPlus
//
//  Created by elsaid yousif on 15/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
import ObjectMapper

class FavoritesRes: NSObject, NSCoding, Mappable {
    var status: Bool?
    var categories: [CategoryItem]?
    var JobTypes: [JobTypeItem]?
    var WorkSectors: [workSector]?
    var worktimes: [WorkTimeItem]?
    var cities: [City]?
    var companies: [Company]?
    
    class func newInstance(map: Map) -> Mappable?{
        return FavoritesRes()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        status <- map["status"]
        categories <- map["categories"]
        JobTypes <- map["JobTypes"]
        WorkSectors <- map["WorkSectors"]
        worktimes <- map["worktimes"]
        cities <- map["cities"]
        companies <- map["companies"]
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        status = aDecoder.decodeObject(forKey: "id") as? Bool
        categories = aDecoder.decodeObject(forKey: "categories") as? [CategoryItem]
        JobTypes = aDecoder.decodeObject(forKey: "JobTypes") as? [JobTypeItem]
        WorkSectors = aDecoder.decodeObject(forKey: "WorkSectors") as? [workSector]
        worktimes = aDecoder.decodeObject(forKey: "worktimes") as? [WorkTimeItem]
        cities = aDecoder.decodeObject(forKey: "cities") as? [City]
        companies = aDecoder.decodeObject(forKey: "companies") as? [Company]
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if categories != nil{
            aCoder.encode(categories, forKey: "categories")
        }
        if JobTypes != nil{
            aCoder.encode(JobTypes, forKey: "JobTypes")
        }
        if WorkSectors != nil{
            aCoder.encode(WorkSectors, forKey: "WorkSectors")
        }
        if worktimes != nil{
            aCoder.encode(worktimes, forKey: "worktimes")
        }
        if cities != nil{
            aCoder.encode(cities, forKey: "cities")
        }
        if companies != nil{
            aCoder.encode(companies, forKey: "companies")
        }
    }
}
