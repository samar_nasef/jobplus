//
//	RootClass.swift
//
//	Create by passant on 20/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class LoginResponse : NSObject, NSCoding, Mappable{

	var message : String?
    var error : String?
	var status : Bool?
	var user : User?
    var errors : ErrorResponse?


	class func newInstance(map: Map) -> Mappable?{
		return LoginResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
        error <- map["error"]
		message <- map["message"]
		status <- map["status"]
		user <- map["user"]
        errors <- map["errors"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Bool
         user = aDecoder.decodeObject(forKey: "user") as? User
         errors = aDecoder.decodeObject(forKey: "errors") as? ErrorResponse

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if user != nil{
			aCoder.encode(user, forKey: "user")
		}
        if errors != nil{
            aCoder.encode(errors, forKey: "errors")
        }

	}

}
