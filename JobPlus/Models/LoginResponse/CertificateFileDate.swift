//
//	CertificateFileDate.swift
//
//	Create by passant on 29/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class CertificateFileDate : NSObject, NSCoding, Mappable{

	var date : String?
	var timezone : String?
	var timezoneType : Int?


	class func newInstance(map: Map) -> Mappable?{
		return CertificateFileDate()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		date <- map["date"]
		timezone <- map["timezone"]
		timezoneType <- map["timezone_type"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "date") as? String
         timezone = aDecoder.decodeObject(forKey: "timezone") as? String
         timezoneType = aDecoder.decodeObject(forKey: "timezone_type") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if timezone != nil{
			aCoder.encode(timezone, forKey: "timezone")
		}
		if timezoneType != nil{
			aCoder.encode(timezoneType, forKey: "timezone_type")
		}

	}

}