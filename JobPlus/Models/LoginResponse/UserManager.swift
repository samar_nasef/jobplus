//
//  UserManager.swift
//  Zumra
//
//  Created by Mohamed Shaat on 3/29/18.
//  Copyright © 2018 10degree. All rights reserved.
//

import UIKit

class UserManager: NSObject {
    static let sharedInstance = UserManager()
    private var user:User?
    var subscribedUser: Bool?
    var appVersion = 6.0
    var showNewsArticles: Int = 0
    var showOffers: Int = 0
    var experience: Array<Experience> = []
    var jobIndexAppliedOrCanceled : IndexPath?
    var jobAppliedOrCanceled = 0 // 1 -> applied , 2 -> canceled
    var notificationCount: Int = 0
    var userShouldAddFeedBack = false
    var notificationStatus: Int = 0
    var subscribtionURL = ""
    var userRegisterForFirstTime = 0
    var openDynamicLinkJobID = -1
    var openDynamicLink = false
    var openProfile = false
    var NotSubscribedMeassage:String?
    var addNewInstitute = false
    var disabilityyItems = [DisabilityTypeItem]()
    override init() {
        super.init()
        
        if UserDefaults.standard.object(forKey: "USER") != nil {
            let data = UserDefaults.standard.object(forKey: "USER")
            let user:User = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! User
            self.user = user
        }
    }
    
    func saveUser(user:User)  {
        UserManager.sharedInstance.user = user
        let data = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(data, forKey: "USER")
    }
    func removeUser()
    {
        UserDefaults.standard.removeObject(forKey:"USER" )
        user = nil
    }
    func getUser()->User?
    {
        return user
    }
    func isLogined() -> Bool {
        return UserManager.sharedInstance.user?.mobileVerified != nil && UserManager.sharedInstance.user?.mobileVerified == true
    }
    
}
