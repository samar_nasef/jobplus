//
//	Error.swift
//
//	Create by passant on 22/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ErrorResponse : NSObject, NSCoding, Mappable{

    var birthDate : AnyObject?
    var email : String?
    var gender : String?
    var id : Int?
    var imagePath : AnyObject?
    var mobile : String?
    var mobileApplicationLocale : String?
    var mobileVerified : Bool?
    var name : String?
    var token : String?
    var mobileVerificationCode :String?
    var password : String?
    var oldPassword :String?
    var forgotPasswordToken : String?
    var jobId : String?
    var countryId: String?
    var educDegree: String?
    var userBio:String?
    var LastName: String?
    var nationality: String?
    var preferredWorkFiledTitle: String?
    var cityResidenceName:String?
    var SoftSkills:String?
	class func newInstance(map: Map) -> Mappable?{
		return ErrorResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
        oldPassword <- map["oldPassword"]
        birthDate <- map["birthDate"]
        email <- map["email"]
        gender <- map["gender"]
        id <- map["id"]
        imagePath <- map["imagePath"]
        mobile <- map["mobile"]
        mobileApplicationLocale <- map["mobileApplicationLocale"]
        mobileVerified <- map["mobileVerified"]
        name <- map["name"]
        token <- map["token"]
        mobileVerificationCode <- map["mobileVerificationCode"]
        password <- map["password"]
        forgotPasswordToken <- map["forgotPasswordToken"]
        jobId <- map["job_id"]
        countryId <- map["countryId"]
        educDegree <- map["educDegree"]
        userBio <- map["userBio"]
        LastName <- map["LastName"]
        nationality <- map["Nationality"]
        preferredWorkFiledTitle <- map["PreferedWork"]
        cityResidenceName <- map["City_Residence_Name"]
        SoftSkills <- map["SoftSkills"]
	}
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        oldPassword = aDecoder.decodeObject(forKey: "oldPassword") as? String
        birthDate = aDecoder.decodeObject(forKey: "birthDate") as? AnyObject
        email = aDecoder.decodeObject(forKey: "email") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        imagePath = aDecoder.decodeObject(forKey: "imagePath") as? AnyObject
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        mobileApplicationLocale = aDecoder.decodeObject(forKey: "mobileApplicationLocale") as? String
        mobileVerified = aDecoder.decodeObject(forKey: "mobileVerified") as? Bool
        name = aDecoder.decodeObject(forKey: "name") as? String
        token = aDecoder.decodeObject(forKey: "token") as? String
        countryId = aDecoder.decodeObject(forKey: "countryId") as? String ?? ""
        educDegree = aDecoder.decodeObject(forKey: "educDegree") as? String ?? ""
        userBio = aDecoder.decodeObject(forKey: "userBio") as? String
        LastName = aDecoder.decodeObject(forKey: "LastName") as? String
        nationality = aDecoder.decodeObject(forKey: "nationality") as? String
        preferredWorkFiledTitle = aDecoder.decodeObject(forKey: "preferredWorkFiledTitle") as? String
        cityResidenceName = aDecoder.decodeObject(forKey: "cityResidenceName") as? String
        SoftSkills = aDecoder.decodeObject(forKey: "SoftSkills") as? String ?? ""
        jobId = aDecoder.decodeObject(forKey: "job_id") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if birthDate != nil{
            aCoder.encode(birthDate, forKey: "birthDate")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imagePath != nil{
            aCoder.encode(imagePath, forKey: "imagePath")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if mobileApplicationLocale != nil{
            aCoder.encode(mobileApplicationLocale, forKey: "mobileApplicationLocale")
        }
        if mobileVerified != nil{
            aCoder.encode(mobileVerified, forKey: "mobileVerified")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if jobId != nil{
            aCoder.encode(jobId, forKey: "job_id")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "countryId")
        }
        if educDegree != nil{
            aCoder.encode(educDegree, forKey: "educDegree")
        }
        if userBio != nil{
            aCoder.encode(userBio, forKey: "userBio")
        }
        if LastName != nil{
            aCoder.encode(LastName, forKey: "lastName")
        }
        if nationality != nil{
            aCoder.encode(nationality, forKey: "nationality")
        }
        if preferredWorkFiledTitle != nil{
            aCoder.encode(preferredWorkFiledTitle, forKey: "preferredWorkFiledTitle")
        }
        if cityResidenceName != nil{
            aCoder.encode(cityResidenceName, forKey: "cityResidenceName")
        }
        if SoftSkills != nil{
            aCoder.encode(jobId, forKey: "SoftSkills")
        }
    }
   

}
