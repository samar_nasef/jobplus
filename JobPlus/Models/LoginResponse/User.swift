//
//    User.swift
//
//    Create by passant on 5/2/2019
//    Copyright © 2019. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper

struct userDataObjForApi {
    var name:String?
    var LastName : String?
    //    var mobile:String?
    
    var Nationality : String?
    var PreferedWork : String?
    
    var userBio:String?
    var SectionID:Int?
    var DrivingLicense:Int?
    var birthDate : Int?
    var email : String?
    var gender : String?
    var country_id:Int?
    var CityID:Int?
    var City_Residence_Name:String?
    var PreferedContactID:Int?
    
//    var SoftSkills : SkillObjectToPost?
    
}

struct skillObj2 {
    var skill_id:Int?
}
class skillObj22 : NSObject, NSCoding, Mappable{


    var id : Int?
    var nameAr : String?
    var nameEn : String?
    var skillType: Int?
    
    class func newInstance(map: Map) -> Mappable?{
        return skillObj22()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        nameAr <- map["nameAr"]
        nameEn <- map["nameEn"]
        skillType <- map["skillType"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = (aDecoder.decodeObject(forKey: "id") as? Int)!
        nameAr = (aDecoder.decodeObject(forKey: "nameAr") as? String)!
        nameEn = (aDecoder.decodeObject(forKey: "nameEn") as? String)!
        if skillType != nil{
        skillType = (aDecoder.decodeObject(forKey: "skillType") as? Int)!
        }

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if nameAr != nil{
            aCoder.encode(nameAr, forKey: "nameAr")
        }
        if nameEn != nil{
            aCoder.encode(nameEn, forKey: "nameEn")
        }
        if skillType != nil{
            aCoder.encode(skillType, forKey: "skillType")
        }
        

    }

}

class skillItemFromAPi : NSObject, NSCoding, Mappable{

    var id: Int?
    var skillTyp: Int?
    var softSkill : skillObj22?
    var skillText: String?



    class func newInstance(map: Map) -> Mappable?{
        return skillItemFromAPi()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        softSkill <- map["softSkill"]
        skillTyp <- map["skillTyp"]
        skillText <- map["skillText"]
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        id = (aDecoder.decodeObject(forKey: "id") as? Int?)!
        
        if softSkill != nil{
        softSkill = (aDecoder.decodeObject(forKey: "softSkill") as? skillObj22)!
        }
        if skillTyp != nil{
        skillTyp = (aDecoder.decodeObject(forKey: "softSkill") as? Int?)!
        }
        if skillText != nil{
        skillText = (aDecoder.decodeObject(forKey: "skillText") as? String?)!
        }

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if softSkill != nil{
            aCoder.encode(softSkill, forKey: "softSkill")
        }
        if skillTyp != nil{
            aCoder.encode(skillTyp, forKey: "skillTyp")
        }
        if skillText != nil{
            aCoder.encode(skillText, forKey: "skillText")
        }

    }

}


class techskillItemFromAPi : NSObject, NSCoding, Mappable{

    var techSkill : skillObj22?



    class func newInstance(map: Map) -> Mappable?{
        return techskillItemFromAPi()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        techSkill <- map["techSkill"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        techSkill = (aDecoder.decodeObject(forKey: "techSkill") as? skillObj22)!

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if techSkill != nil{
            aCoder.encode(techSkill, forKey: "techSkill")
        }

    }

}

class User : NSObject, NSCoding, Mappable{
    
    var birthDateString:String?
    var selectedCountry:String?
    var selectedContact:String?
    var PreferedContactID:Int?
    var LastName : String?
    var Nationality : String?
    var NationalityObject : CountryRes?
    var PreferedWork : String?
    var drivingLicense : Bool?
    var userBio:String?
    var SectionID:Int?
    var DrivingLicense:Int?
    var SoftSkills : SkillObjectToPost?
    var userLanguages2 : languageObjectToPost?
    var userLanguages : [languageItemFromAPi]?
    var EducationalInformation : [educationItemFromAPi]?
    var EducationalInformation2 : eduactionalObjectToPost?
    var userCertificates : [certificateItemFromAPi]?
    var userCertificates2 : certificateObjectToPost?
    var softSkills : [skillItemFromAPi]?
    var techSkills : [techskillItemFromAPi]?
    var birthDate : String?
    var birthDate2 : Int?
    var categories : [CategoryItem]?
    var JobTypes: [JobTypeItem]?
    var WorkSectors: [workSector]?
    var WorkTimes: [WorkTimeItem]?
    var country : CountryRes?
    var companies : [Company]?
    var cities : [City]?
    var countryId : Int?
    var educCity : String?
    var educDegree : String?
    var educDegreeTxt : String?
    var educInstitution : String?
    var educMajor : String?
    var educCountry : CountryRes?
    var educCountryId : Int?
    var email : String?
    var gender : String?
    var id : Int?
    var imagePath : String?
    var mobile : String?
    var mobileApplicationLocale : String?
    var mobileCountryPrefix : String?
    var mobileVerified : Bool?
    var name : String?
    var phone : AnyObject?
    var phoneCountry : AnyObject?
    var token : String?
    var myJobsCount:Int?
    var experiences : [Experience]?
    var experiences2 :  ExperienceObjectToPost?
    var cvPath: String?
    var certificate_id: Int?
    var worktime_id: Int?
    var City_Residence: String?
    var City_Residence_Name: String?
    var CityID: Int?
    var cv_id: Int?
    var CvDownloadLink: String?
    var cv: String?
    var cv_extension:String?
    var Disability: Int?
    var DisabilityType: String?
    var NotificationStatus: Int?
    var experienceid: Int?
    var feedback: Bool?
    class func newInstance(map: Map) -> Mappable?{
        return User()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        EducationalInformation <- map["EducationalInformation"]
        userCertificates <- map["userCertificates"]
        birthDateString <- map["birthDateString"]
        userLanguages <- map["userLanguages"]
        userLanguages2 <- map["userLanguages2"]
        selectedCountry <- map["selectedCountry"]
        selectedContact <- map["selectedContact"]
        experiences2 <- map["experiences2"]
        PreferedContactID <- map["PreferedContactID"]
        techSkills <- map["techSkills"]
        softSkills <- map["softSkills"]
        SoftSkills <- map["SoftSkills"]
        birthDate <- map["birthDate"]
        birthDate2 <- map["birthDate2"]
        categories <- map["categories"]
        JobTypes <- map["JobTypes"]
        WorkSectors <- map["WorkSectors"]
        WorkTimes <- map["WorkTimes"]
        cities <- map["cities"]
        companies <- map["companies"]
        country <- map["country"]
        countryId <- map["country_id"]
        educCity <- map["educCity"]
        educDegree <- map["educDegree"]
        educDegreeTxt <- map["educDegreeTxt"]
        educInstitution <- map["educInstitution"]
        educMajor <- map["educMajor"]
        educCountry <- map["educ_country"]
        educCountryId <- map["educ_country_id"]
        email <- map["email"]
        gender <- map["gender"]
        id <- map["id"]
        imagePath <- map["imagePath"]
        mobile <- map["mobile"]
        mobileApplicationLocale <- map["mobileApplicationLocale"]
        mobileCountryPrefix <- map["mobileCountryPrefix"]
        cvPath <- map["cvPath"]
        mobileVerified <- map["mobileVerified"]
        name <- map["name"]
        phone <- map["phone"]
        phoneCountry <- map["phone_country"]
        token <- map["token"]
        myJobsCount <- map["myJobsCount"]
        experiences <- map["experiences"]
        certificate_id <- map["certificate_id"]
        worktime_id <- map["worktime_id"]
        City_Residence <- map["City_Residence"]
        City_Residence_Name <- map["City_Residence_Name"]
        LastName <- map["LastName"]
        Nationality <- map["Nationality"]
        NationalityObject <- map["NationalityObject"]
        PreferedWork <- map["PreferedWork"]
        DrivingLicense <- map["DrivingLicense"]
        userBio <- map["userBio"]
        CvDownloadLink <- map["CvDownloadLink"]
        cv <- map["cv"]
        cv_extension <- map["cv_extension"]
        CityID <- map["CityID"]
        Disability <- map["Disability"]
        DisabilityType <- map["DisabilityType"]
        NotificationStatus <- map["NotificationStatus"]
        experienceid <- map["experienceid"]
        feedback <- map["feedback"]
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        userCertificates = aDecoder.decodeObject(forKey: "userCertificates") as? [certificateItemFromAPi]
        experiences2  = aDecoder.decodeObject(forKey: "experiences2") as? ExperienceObjectToPost
        EducationalInformation = aDecoder.decodeObject(forKey: "EducationalInformation") as? [educationItemFromAPi]
        birthDateString = aDecoder.decodeObject(forKey: "birthDateString") as? String
        userLanguages = aDecoder.decodeObject(forKey: "userLanguages") as? [languageItemFromAPi]
        userLanguages2 = aDecoder.decodeObject(forKey: "userLanguages2") as? languageObjectToPost
        selectedCountry = aDecoder.decodeObject(forKey: "selectedCountry") as? String
        selectedContact = aDecoder.decodeObject(forKey: "selectedContact") as? String
        
        PreferedContactID = aDecoder.decodeObject(forKey: "PreferedContactID") as? Int
        techSkills = aDecoder.decodeObject(forKey: "techSkills") as? [techskillItemFromAPi]
        softSkills = aDecoder.decodeObject(forKey: "softSkills") as? [skillItemFromAPi]
        SoftSkills = aDecoder.decodeObject(forKey: "SoftSkills") as? SkillObjectToPost
        LastName = aDecoder.decodeObject(forKey: "LastName") as? String
        Nationality = aDecoder.decodeObject(forKey: "Nationality") as? String
        NationalityObject = aDecoder.decodeObject(forKey: "NationalityObject") as? CountryRes
        PreferedWork = aDecoder.decodeObject(forKey: "PreferedWork") as? String
        DrivingLicense = aDecoder.decodeObject(forKey: "DrivingLicense") as? Int
        userBio = aDecoder.decodeObject(forKey: "userBio") as? String
        birthDate2 = aDecoder.decodeObject(forKey: "birthDate2") as? Int
        birthDate = aDecoder.decodeObject(forKey: "birthDate") as? String
        categories = aDecoder.decodeObject(forKey: "categories") as? [CategoryItem]
        JobTypes = aDecoder.decodeObject(forKey: "JobTypes") as? [JobTypeItem]
        WorkSectors = aDecoder.decodeObject(forKey: "WorkSectors") as? [workSector]
        WorkTimes = aDecoder.decodeObject(forKey: "WorkTimes") as? [WorkTimeItem]
        country = aDecoder.decodeObject(forKey: "country") as? CountryRes
        cities = aDecoder.decodeObject(forKey: "cities") as? [City]
        companies = aDecoder.decodeObject(forKey: "companies") as? [Company]
        countryId = aDecoder.decodeObject(forKey: "country_id") as? Int
        educCity = aDecoder.decodeObject(forKey: "educCity") as? String
        educDegree = aDecoder.decodeObject(forKey: "educDegree") as? String
        educDegreeTxt = aDecoder.decodeObject(forKey: "educDegreeTxt") as? String
        educInstitution = aDecoder.decodeObject(forKey: "educInstitution") as? String
        educMajor = aDecoder.decodeObject(forKey: "educMajor") as? String
        educCountry = aDecoder.decodeObject(forKey: "educ_country") as? CountryRes
        educCountryId = aDecoder.decodeObject(forKey: "educ_country_id") as? Int
        email = aDecoder.decodeObject(forKey: "email") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        imagePath = aDecoder.decodeObject(forKey: "imagePath") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        mobileApplicationLocale = aDecoder.decodeObject(forKey: "mobileApplicationLocale") as? String
        mobileCountryPrefix = aDecoder.decodeObject(forKey: "mobileCountryPrefix") as? String
        cvPath = aDecoder.decodeObject(forKey: "cvPath") as? String
        mobileVerified = aDecoder.decodeObject(forKey: "mobileVerified") as? Bool
        name = aDecoder.decodeObject(forKey: "name") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? AnyObject
        phoneCountry = aDecoder.decodeObject(forKey: "phone_country") as AnyObject?
        token = aDecoder.decodeObject(forKey: "token") as? String
        myJobsCount = aDecoder.decodeObject(forKey: "myJobsCount") as? Int
        experiences = aDecoder.decodeObject(forKey: "experiences") as? [Experience]
        certificate_id = aDecoder.decodeObject(forKey: "certificate_id") as? Int
        worktime_id = aDecoder.decodeObject(forKey: "worktime_id") as? Int
        City_Residence = aDecoder.decodeObject(forKey: "City_Residence") as? String
        City_Residence_Name = aDecoder.decodeObject(forKey: "City_Residence_Name") as? String
        CvDownloadLink = aDecoder.decodeObject(forKey: "CvDownloadLink") as? String
        CityID = aDecoder.decodeObject(forKey: "CityID") as? Int
        cv = aDecoder.decodeObject(forKey: "cv") as? String
        cv_extension = aDecoder.decodeObject(forKey: "cv_extension") as? String
        Disability = aDecoder.decodeObject(forKey: "Disability") as? Int
        DisabilityType = aDecoder.decodeObject(forKey: "DisabilityType") as? String
        NotificationStatus = aDecoder.decodeObject(forKey: "NotificationStatus") as? Int
        experienceid = aDecoder.decodeObject(forKey: "experienceid") as? Int
        feedback = aDecoder.decodeObject(forKey: "feedback") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if experiences2 != nil{
            aCoder.encode(experiences2, forKey: "experiences2")
        }
        if userCertificates != nil{
            aCoder.encode(userCertificates, forKey: "userCertificates")
        }
        
        if EducationalInformation != nil{
            aCoder.encode(EducationalInformation, forKey: "EducationalInformation")
        }
        
        if birthDateString != nil{
            aCoder.encode(birthDateString, forKey: "birthDateString")
        }
        
        if userLanguages2 != nil{
            aCoder.encode(userLanguages2, forKey: "userLanguages2")
        }
        
        if userLanguages != nil{
            aCoder.encode(userLanguages, forKey: "userLanguages")
        }
        
        if selectedCountry != nil{
            aCoder.encode(selectedCountry, forKey: "selectedCountry")
        }
        
        if selectedContact != nil{
            aCoder.encode(selectedContact, forKey: "selectedContact")
        }
        
        if PreferedContactID != nil{
            aCoder.encode(PreferedContactID, forKey: "PreferedContactID")
        }
        
        if techSkills != nil{
            aCoder.encode(techSkills, forKey: "techSkills")
        }
        if softSkills != nil{
            aCoder.encode(softSkills, forKey: "softSkills")
        }
        if SoftSkills != nil{
            aCoder.encode(SoftSkills, forKey: "SoftSkills")
        }
        if LastName != nil{
            aCoder.encode(LastName, forKey: "LastName")
        }
        if Nationality != nil{
            aCoder.encode(Nationality, forKey: "Nationality")
        }
        if NationalityObject != nil{
            aCoder.encode(NationalityObject, forKey: "NationalityObject")
        }
        if PreferedWork != nil{
            aCoder.encode(PreferedWork, forKey: "PreferedWork")
        }
        if DrivingLicense != nil{
            aCoder.encode(DrivingLicense, forKey: "DrivingLicense")
        }
        if userBio != nil{
            aCoder.encode(userBio, forKey: "userBio")
        }
        
        if birthDate2 != nil{
            aCoder.encode(birthDate2, forKey: "birthDate2")
        }
        if birthDate != nil{
            aCoder.encode(birthDate, forKey: "birthDate")
        }
        if categories != nil{
            aCoder.encode(categories, forKey: "categories")
        }
        if JobTypes != nil{
            aCoder.encode(JobTypes, forKey: "JobTypes")
        }
        if WorkSectors != nil{
            aCoder.encode(WorkSectors, forKey: "WorkSectors")
        }
        if WorkTimes != nil{
            aCoder.encode(WorkTimes, forKey: "WorkTimes")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if cities != nil{
            aCoder.encode(cities, forKey: "cities")
        }
        if companies != nil{
            aCoder.encode(companies, forKey: "companies")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if educCity != nil{
            aCoder.encode(educCity, forKey: "educCity")
        }
        if educDegree != nil{
            aCoder.encode(educDegree, forKey: "educDegree")
        }
        if educDegreeTxt != nil{
            aCoder.encode(educDegreeTxt, forKey: "educDegreeTxt")
        }
        if educInstitution != nil{
            aCoder.encode(educInstitution, forKey: "educInstitution")
        }
        if educMajor != nil{
            aCoder.encode(educMajor, forKey: "educMajor")
        }
        if educCountry != nil{
            aCoder.encode(educCountry, forKey: "educ_country")
        }
        if educCountryId != nil{
            aCoder.encode(educCountryId, forKey: "educ_country_id")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imagePath != nil{
            aCoder.encode(imagePath, forKey: "imagePath")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if mobileApplicationLocale != nil{
            aCoder.encode(mobileApplicationLocale, forKey: "mobileApplicationLocale")
        }
        if mobileCountryPrefix != nil{
            aCoder.encode(mobileCountryPrefix, forKey: "mobileCountryPrefix")
        }
        if cvPath != nil{
            aCoder.encode(cvPath, forKey: "cvPath")
        }
        if mobileVerified != nil{
            aCoder.encode(mobileVerified, forKey: "mobileVerified")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if phoneCountry != nil{
            aCoder.encode(phoneCountry, forKey: "phone_country")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if myJobsCount != nil{
            aCoder.encode(myJobsCount, forKey: "myJobsCount")
        }
        if experiences != nil{
            aCoder.encode(experiences, forKey: "experiences")
        }
        if certificate_id != nil{
            aCoder.encode(certificate_id, forKey: "certificate_id")
        }
        if worktime_id != nil{
            aCoder.encode(worktime_id, forKey: "worktime_id")
        }
        if City_Residence != nil{
            aCoder.encode(City_Residence, forKey: "City_Residence")
        }
        if City_Residence_Name != nil{
            aCoder.encode(City_Residence_Name, forKey: "City_Residence_Name")
        }
        if CvDownloadLink != nil {
            aCoder.encode(CvDownloadLink, forKey: "CvDownloadLink")
        }
        if cv != nil {
            aCoder.encode(cv, forKey: "cv")
        }
        if cv_extension != nil {
            aCoder.encode(cv_extension, forKey: "cv_extension")
        }
        if CityID != nil {
            aCoder.encode(CityID, forKey: "CityID")
        }
        if Disability != nil {
            aCoder.encode(Disability, forKey: "Disability")
        }
        if DisabilityType != nil {
            aCoder.encode(DisabilityType, forKey: "DisabilityType")
        }
        if NotificationStatus != nil {
            aCoder.encode(NotificationStatus, forKey: "NotificationStatus")
        }
        if experienceid != nil {
            aCoder.encode(experienceid, forKey: "experienceid")
        }
        if feedback != nil {
            aCoder.encode(feedback, forKey: "feedback")
        }
    }
}

class JobTypeItem : NSObject, NSCoding, Mappable {
    var id: Int?
    var nameAr: String?
    var name: String?
    
    class func newInstance(map: Map) -> Mappable?{
        return JobTypeItem()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["nameEn"]
        nameAr <- map["nameAr"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "nameEn") as? String
        nameAr = aDecoder.decodeObject(forKey: "nameAr") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "nameEn")
        }
        if nameAr != nil{
            aCoder.encode(name, forKey: "nameAr")
        }
    }
}
class defaultItem : NSObject, NSCoding, Mappable {
    var id: Int?
   
    var name: String?
    
    class func newInstance(map: Map) -> Mappable?{
        return defaultItem()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
       
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
       
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        
    }
}


class workSector : NSObject, NSCoding, Mappable {
    var id: Int?
    var nameAr: String?
    var nameEn: String?
    
    class func newInstance(map: Map) -> Mappable?{
        return workSector()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        nameEn <- map["nameEn"]
        nameAr <- map["nameAr"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        nameEn = aDecoder.decodeObject(forKey: "nameEn") as? String
        nameAr = aDecoder.decodeObject(forKey: "nameAr") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if nameEn != nil{
            aCoder.encode(nameEn, forKey: "nameEn")
        }
        if nameAr != nil{
            aCoder.encode(nameAr, forKey: "nameAr")
        }
    }
}

class jobCatItem : NSObject, NSCoding, Mappable {
    var id: Int?
    var nameAr: String?
    var name: String?
    
    class func newInstance(map: Map) -> Mappable?{
        return jobCatItem()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        nameAr <- map["nameAr"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        nameAr = aDecoder.decodeObject(forKey: "nameAr") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if nameAr != nil{
            aCoder.encode(nameAr, forKey: "nameAr")
        }
    }
}
class eduInfoCertificateItem : NSObject, NSCoding, Mappable {
    var id: Int?
    var certificateEn: String?
    var certificateAr: String?
    
    class func newInstance(map: Map) -> Mappable?{
        return eduInfoCertificateItem()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        certificateEn <- map["certificateEn"]
        certificateAr <- map["certificateAr"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        certificateEn = aDecoder.decodeObject(forKey: "certificateEn") as? String
        certificateAr = aDecoder.decodeObject(forKey: "certificateAr") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if certificateAr != nil{
            aCoder.encode(certificateAr, forKey: "certificateAr")
        }
        if certificateEn != nil{
            aCoder.encode(certificateEn, forKey: "certificateEn")
        }
    }
}

class WorkTimeItem : NSObject, NSCoding, Mappable {
    var id: Int?
    var worktimeAr: String?
    var worktimeEn: String?
    
    class func newInstance(map: Map) -> Mappable?{
        return WorkTimeItem()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        worktimeEn <- map["worktimeEn"]
        worktimeAr <- map["worktimeAr"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        worktimeEn = aDecoder.decodeObject(forKey: "worktimeEn") as? String
        worktimeAr = aDecoder.decodeObject(forKey: "worktimeAr") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if worktimeEn != nil{
            aCoder.encode(worktimeEn, forKey: "worktimeEn")
        }
        if worktimeAr != nil{
            aCoder.encode(worktimeAr, forKey: "worktimeAr")
        }
    }
}

class CategoryItem : NSObject, NSCoding, Mappable {
    var id : Int?
    var imagePath : String?
    var name : String?
    var numberOfJobs : Int?
    var nameAr : String?
    
    class func newInstance(map: Map) -> Mappable?{
        return CategoryItem()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        imagePath <- map["imagePath"]
        name <- map["name"]
        nameAr <- map["nameAr"]
        numberOfJobs <- map["jobsCount"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        imagePath = aDecoder.decodeObject(forKey: "imagePath") as?  String
        name = aDecoder.decodeObject(forKey: "name") as? String
        nameAr = aDecoder.decodeObject(forKey: "nameAr") as? String
        numberOfJobs = aDecoder.decodeObject(forKey: "jobsCount") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imagePath != nil{
            aCoder.encode(imagePath, forKey: "imagePath")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if nameAr != nil{
            aCoder.encode(name, forKey: "nameAr")
        }
        if numberOfJobs != nil{
            aCoder.encode(numberOfJobs, forKey: "jobsCount")
        }
        
    }
}

class certificateItemFromAPi : NSObject, NSCoding, Mappable{
    
    var id : Int?
    var certificateName : String?
    var DateOfIssue : String?
    var CertificateEndDate : String?
    var HasIssueDate : Int?
    var cellHeight : Double?
    var InstitutionID: Int?
    var Institution: skillObj22?
    var InstitutionText: String?


    class func newInstance(map: Map) -> Mappable?{
        return certificateItemFromAPi()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        certificateName <- map["certificateName"]
        DateOfIssue <- map["DateOfIssue"]
        CertificateEndDate <- map["CertificateEndDate"]
        HasIssueDate <- map["HasIssueDate"]
        cellHeight <- map["cellHeight"]
        InstitutionID <- map["InstitutionID"]
        Institution <- map["Institution"]
        InstitutionText <- map["InstitutionText"]
        
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        id = (aDecoder.decodeObject(forKey: "id") as? Int)!
        certificateName = (aDecoder.decodeObject(forKey: "certificateName") as? String)!
        DateOfIssue = (aDecoder.decodeObject(forKey: "DateOfIssue") as? String)
        CertificateEndDate = (aDecoder.decodeObject(forKey: "CertificateEndDate") as? String) ?? "0"
        HasIssueDate = (aDecoder.decodeObject(forKey: "HasIssueDate") as? Int) ?? nil
            cellHeight = (aDecoder.decodeObject(forKey: "cellHeight") as? Double) ?? nil
            InstitutionID = (aDecoder.decodeObject(forKey: "InstitutionID") as? Int) ?? nil
            InstitutionText = (aDecoder.decodeObject(forKey: "InstitutionText") as? String) ?? nil
            Institution = (aDecoder.decodeObject(forKey: "Institution") as? skillObj22) ?? nil
        
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        
        if certificateName != nil{
            aCoder.encode(certificateName, forKey: "certificateName")
        }
        
        if DateOfIssue != nil{
            aCoder.encode(DateOfIssue, forKey: "DateOfIssue")
        }
        
        if CertificateEndDate != nil{
            aCoder.encode(CertificateEndDate, forKey: "CertificateEndDate")
        }
        if HasIssueDate != nil{
            aCoder.encode(HasIssueDate, forKey: "HasIssueDate")
        }
        
        if cellHeight != nil{
            aCoder.encode(cellHeight, forKey: "cellHeight")
        }

        if InstitutionID != nil{
            aCoder.encode(InstitutionID, forKey: "InstitutionID")
        }
        if InstitutionText != nil{
            aCoder.encode(InstitutionText, forKey: "InstitutionText")
        }
        if Institution != nil{
            aCoder.encode(Institution, forKey: "Institution")
        }

    }

}



class educationItemFromAPi : NSObject, NSCoding, Mappable{
    var id : Int?
    var certificate_id : Int?
    var InstitutionID : Int?
    var Institution_Name : String?
    var country_id : Int?
    var fromDate : String?
    var toDate : String?
    var majorName:String?
    var grades: String?
    var EduInfoCountry:workSector?
    var EduInfoInstitution:workSector?
    var EduInfoDegree:eduInfoCertificateItem?
    var cellHeight : Double?
    var specialitilty: workSector?
    var specialitilty_id: Int?
//    "EduInfoDegree": {
//                       "id": 63,
//                       "certificateEn": "Bachelor",
//                       "certificateAr": "بكالوريوس"
//                   },
    

//    var languageStrength : skillObj22?


    class func newInstance(map: Map) -> Mappable?{
        return educationItemFromAPi()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        certificate_id <- map["certificate_id"]
        InstitutionID <- map["InstitutionID"]
        Institution_Name <- map["Institution_Name"]
        
        country_id <- map["country_id"]
        fromDate <- map["fromDate"]
        toDate <- map["toDate"]
        majorName <- map["majorName"]
        grades <- map["grades"]
        EduInfoCountry <- map["EduInfoCountry"]
        EduInfoDegree <- map["EduInfoDegree"]
        EduInfoInstitution <- map["EduInfoInstitution"]
        cellHeight <- map["cellHeight"]
        specialitilty <- map["specialitilty"]
        specialitilty_id <- map["specialitilty_id"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        certificate_id = aDecoder.decodeObject(forKey: "certificate_id") as? Int
        InstitutionID = aDecoder.decodeObject(forKey: "InstitutionID") as? Int
        Institution_Name = aDecoder.decodeObject(forKey: "Institution_Name") as? String
        
        country_id = aDecoder.decodeObject(forKey: "country_id") as? Int
        fromDate = aDecoder.decodeObject(forKey: "fromDate") as? String
        toDate = aDecoder.decodeObject(forKey: "toDate") as? String
        
        majorName = aDecoder.decodeObject(forKey: "majorName") as? String
        grades = aDecoder.decodeObject(forKey: "grades") as? String
        EduInfoCountry = aDecoder.decodeObject(forKey: "EduInfoCountry") as? workSector
        EduInfoDegree = aDecoder.decodeObject(forKey: "EduInfoDegree") as? eduInfoCertificateItem
        EduInfoInstitution = aDecoder.decodeObject(forKey: "EduInfoInstitution") as? workSector
        
        cellHeight = aDecoder.decodeObject(forKey: "cellHeight") as? Double
        specialitilty = aDecoder.decodeObject(forKey: "specialitilty") as? workSector
        specialitilty_id = aDecoder.decodeObject(forKey: "specialitilty_id") as? Int
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        
        if certificate_id != nil{
            aCoder.encode(certificate_id, forKey: "certificate_id")
        }
        if InstitutionID != nil{
            aCoder.encode(InstitutionID, forKey: "InstitutionID")
        }
        if Institution_Name != nil{
            aCoder.encode(Institution_Name, forKey: "Institution_Name")
        }
        
        if country_id != nil{
            aCoder.encode(country_id, forKey: "country_id")
        }
        if fromDate != nil{
            aCoder.encode(fromDate, forKey: "fromDate")
        }
        if toDate != nil{
            aCoder.encode(toDate, forKey: "toDate")
        }
        
        if majorName != nil{
            aCoder.encode(majorName, forKey: "majorName")
        }
        if grades != nil{
            aCoder.encode(grades, forKey: "grades")
        }
        if EduInfoCountry != nil{
            aCoder.encode(EduInfoCountry, forKey: "EduInfoCountry")
        }
        if EduInfoDegree != nil{
            aCoder.encode(EduInfoDegree, forKey: "EduInfoDegree")
        }
        if EduInfoInstitution != nil{
            aCoder.encode(EduInfoInstitution, forKey: "EduInfoInstitution")
        }
        
        if cellHeight != nil{
            aCoder.encode(cellHeight, forKey: "cellHeight")
        }
        if specialitilty != nil{
            aCoder.encode(specialitilty, forKey: "specialitilty")
        }
        if specialitilty_id != nil{
            aCoder.encode(specialitilty_id, forKey: "specialitilty_id")
        }
        

    }

}


class languageItemFromAPi : NSObject, NSCoding, Mappable{
    var id : Int?
    var language : skillObj22?

    var languageStrength : skillObj22?


    class func newInstance(map: Map) -> Mappable?{
        return languageItemFromAPi()
    }
    override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
    {
        id <- map["id"]
        language <- map["language"]
        languageStrength <- map["languageStrength"]
        
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        id = (aDecoder.decodeObject(forKey: "id") as? Int)!
        language = (aDecoder.decodeObject(forKey: "language") as? skillObj22)!
        languageStrength = (aDecoder.decodeObject(forKey: "languageStrength") as? skillObj22)!

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        
        if language != nil{
            aCoder.encode(language, forKey: "language")
        }
        
        if languageStrength != nil{
            aCoder.encode(languageStrength, forKey: "languageStrength")
        }
        

    }

}


