//
//	Experience.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


public final class Experience: NSObject, NSCoding, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let jobTitle = "job_title"
        static let companyName = "company_name"
        static let yearsOfExperience = "years_of_experience"
        
        static let month_of_experience = "month_of_experience"
        static let noticePeriod = "noticePeriod"
        static let cityName = "cityName"
        static let CurrentlyWorking = "CurrentlyWorking"
        static let country = "country"
        static let worksector = "worksector"
        static let jobtype = "jobtype"
        static let country_id = "country_id"
        static let worksector_id = "worksector_id"
        static let jobtype_id = "jobtype_id"
        static let city_id = "city_id"
        
        static let cellHeight = "cellHeight"
        static let Duties = "Duties"
        static let fromDate = "fromDate"
        static let toDate = "toDate"
        static let job_category_id = "job_category_id"
        static let job_category = "job_category"
    }
    
    // MARK: Properties
    public var jobTitle: String = ""
    public var companyName: String = ""
    public var yearsOfExperience: Int?
    var month_of_experience : Int?
    var noticePeriod:String?
    var cityName:String?
    var city_id: Int?
    var CurrentlyWorking:Int?
    var country:workSector?
    var worksector:defaultItem?
    var jobtype:defaultItem?
    var country_id:Int?
    var worksector_id:Int?
    var jobtype_id:Int?
    var cellHeight:Double?
    var Duties:String?
    var fromDate:String?
    var toDate:String?
    var job_category_id: Int?
    var job_category:jobCatItem?
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    init(jobTitle: String, companyName:String, yearsOfExperience:Int, month_of_experience:Int,noticePeriod:String,cityName:String,CurrentlyWorking:Int,country:workSector,worksector:defaultItem, jobtype:defaultItem, country_id: Int, worksector_id: Int, jobtype_id: Int, city_id: Int , cellHeight:Double, Duties:String?, fromDate:String?,toDate:String?, job_category_id: Int? ) {
        self.jobTitle = jobTitle
        self.companyName = companyName
        self.yearsOfExperience = yearsOfExperience
        self.month_of_experience = month_of_experience
        self.noticePeriod = noticePeriod
        self.cityName = cityName
        self.CurrentlyWorking = CurrentlyWorking
        self.country = country
        self.worksector = worksector
        self.jobtype = jobtype
        self.country_id = country_id
        self.worksector_id = worksector_id
        self.jobtype_id = jobtype_id
        self.city_id = city_id
        self.cellHeight = cellHeight
        self.Duties = Duties
        self.fromDate = fromDate
        self.toDate = toDate
        self.job_category_id = job_category_id
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        jobTitle <- map[SerializationKeys.jobTitle]
        companyName <- map[SerializationKeys.companyName]
        yearsOfExperience <- map[SerializationKeys.yearsOfExperience]
        month_of_experience <- map[SerializationKeys.month_of_experience]
        noticePeriod <- map[SerializationKeys.noticePeriod]
        cityName <- map[SerializationKeys.cityName]
        CurrentlyWorking <- map[SerializationKeys.CurrentlyWorking]
        country <- map[SerializationKeys.country]
        worksector <- map[SerializationKeys.worksector]
        jobtype <- map[SerializationKeys.jobtype]
        country_id <- map[SerializationKeys.country_id]
        worksector_id <- map[SerializationKeys.worksector_id]
        jobtype_id <- map[SerializationKeys.jobtype_id]
        city_id <- map[SerializationKeys.city_id]
        cellHeight <- map[SerializationKeys.cellHeight]
        Duties <- map[SerializationKeys.Duties]
        fromDate <- map[SerializationKeys.fromDate]
        toDate <- map[SerializationKeys.toDate]
        job_category_id <- map[SerializationKeys.job_category_id]
        job_category <- map[SerializationKeys.job_category]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.jobTitle] = jobTitle
        dictionary[SerializationKeys.companyName] = companyName
        if let value = yearsOfExperience { dictionary[SerializationKeys.yearsOfExperience] = value }
        
        if let value = cellHeight { dictionary[SerializationKeys.cellHeight] = value }
        
        dictionary[SerializationKeys.month_of_experience] = month_of_experience
        dictionary[SerializationKeys.noticePeriod] = noticePeriod
        dictionary[SerializationKeys.cityName] = cityName
        dictionary[SerializationKeys.country] = country
        dictionary[SerializationKeys.CurrentlyWorking] = CurrentlyWorking
        dictionary[SerializationKeys.worksector] = worksector
        dictionary[SerializationKeys.jobtype] = jobtype
        dictionary[SerializationKeys.country_id] = country_id
        dictionary[SerializationKeys.worksector_id] = worksector_id
        dictionary[SerializationKeys.jobtype_id] = jobtype_id
        dictionary[SerializationKeys.city_id] = city_id
        dictionary[SerializationKeys.Duties] = Duties
        dictionary[SerializationKeys.fromDate] = fromDate
        dictionary[SerializationKeys.toDate] = toDate
        dictionary[SerializationKeys.job_category_id] = job_category_id
        dictionary[SerializationKeys.job_category] = job_category
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.jobTitle = (aDecoder.decodeObject(forKey: SerializationKeys.jobTitle) as? String)!
        self.companyName = (aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String)!
        self.yearsOfExperience = aDecoder.decodeObject(forKey: SerializationKeys.yearsOfExperience) as? Int
        self.month_of_experience = aDecoder.decodeObject(forKey: SerializationKeys.month_of_experience) as? Int
        self.noticePeriod = aDecoder.decodeObject(forKey: SerializationKeys.noticePeriod) as? String
        self.cityName = aDecoder.decodeObject(forKey: SerializationKeys.cityName) as? String
        self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as? workSector
        self.CurrentlyWorking = aDecoder.decodeObject(forKey: SerializationKeys.CurrentlyWorking) as? Int
        self.worksector = aDecoder.decodeObject(forKey: SerializationKeys.worksector) as? defaultItem
        self.jobtype = aDecoder.decodeObject(forKey: SerializationKeys.jobtype) as? defaultItem
        self.country_id = aDecoder.decodeObject(forKey: SerializationKeys.country_id) as? Int
        self.worksector_id = aDecoder.decodeObject(forKey: SerializationKeys.worksector_id) as? Int
        self.jobtype_id = aDecoder.decodeObject(forKey: SerializationKeys.jobtype_id) as? Int
        self.city_id = aDecoder.decodeObject(forKey: SerializationKeys.city_id) as? Int
        self.cellHeight = aDecoder.decodeObject(forKey: SerializationKeys.cellHeight) as? Double
        self.Duties = aDecoder.decodeObject(forKey: SerializationKeys.Duties) as? String
        self.fromDate = aDecoder.decodeObject(forKey: SerializationKeys.fromDate) as? String
        self.toDate = aDecoder.decodeObject(forKey: SerializationKeys.toDate) as? String
        self.job_category_id = aDecoder.decodeObject(forKey: SerializationKeys.job_category_id) as? Int
        self.job_category = aDecoder.decodeObject(forKey: SerializationKeys.job_category) as? jobCatItem
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(jobTitle, forKey: SerializationKeys.jobTitle)
        aCoder.encode(companyName, forKey: SerializationKeys.companyName)
        aCoder.encode(yearsOfExperience, forKey: SerializationKeys.yearsOfExperience)
        
        aCoder.encode(month_of_experience, forKey: SerializationKeys.month_of_experience)
        aCoder.encode(noticePeriod, forKey: SerializationKeys.noticePeriod)
        aCoder.encode(cityName, forKey: SerializationKeys.cityName)
        aCoder.encode(country, forKey: SerializationKeys.country)
        aCoder.encode(CurrentlyWorking, forKey: SerializationKeys.CurrentlyWorking)
        aCoder.encode(worksector, forKey: SerializationKeys.worksector)
        aCoder.encode(jobtype, forKey: SerializationKeys.jobtype)
        aCoder.encode(country_id, forKey: SerializationKeys.country_id)
        aCoder.encode(worksector_id, forKey: SerializationKeys.worksector_id)
        aCoder.encode(jobtype_id, forKey: SerializationKeys.jobtype_id)
        aCoder.encode(city_id, forKey: SerializationKeys.city_id)
        aCoder.encode(cellHeight, forKey: SerializationKeys.cellHeight)
        aCoder.encode(Duties, forKey: SerializationKeys.Duties)
        aCoder.encode(fromDate, forKey: SerializationKeys.fromDate)
        aCoder.encode(toDate, forKey: SerializationKeys.toDate)
        aCoder.encode(job_category_id, forKey: SerializationKeys.job_category_id)
        aCoder.encode(job_category, forKey: SerializationKeys.job_category)
    }
    
}
public final class Experience2: NSObject, NSCoding, Mappable {
    public init?(map: Map) {
        
    }
    
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let jobTitle = "job_title"
        static let companyName = "company_name"
        static let yearsOfExperience = "years_of_experience"
        static let job_category_id = "job_category_id"
        static let month_of_experience = "month_of_experience"
        static let noticePeriod = "noticePeriod"
        static let cityName = "cityName"
        static let CurrentlyWorking = "CurrentlyWorking"
        static let country_id = "country_id"
        static let worksector_id = "worksector_id"
        static let jobtype_id = "jobtype_id"
        static let city_id = "city_id"
        static let Duties = "Duties"
        static let fromDate = "fromDate"
        static let toDate = "toDate"
    }
    
    // MARK: Properties
    public var jobTitle: String = ""
    public var companyName: String = ""
    public var yearsOfExperience: Int?
    var month_of_experience : Int?
    var noticePeriod:String?
    var cityName:String?
    var CurrentlyWorking:Int?
    var country_id:Int?
    var worksector_id:Int?
    var jobtype_id:Int?
    var job_category_id: Int?
    var city_id:Int?
    var Duties:String?
    var fromDate:String?
    var toDate:String?
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    init(jobTitle: String, companyName:String, yearsOfExperience:Int, month_of_experience:Int,noticePeriod:String,cityName:String,CurrentlyWorking:Int,country_id:Int?,worksector_id:Int, jobtype_id:Int, city_id: Int, Duties:String?, fromDate:String?,toDate:String?, job_category_id: Int?) {
        self.jobTitle = jobTitle
        self.companyName = companyName
        self.yearsOfExperience = yearsOfExperience
        self.month_of_experience = month_of_experience
        self.noticePeriod = noticePeriod
        self.cityName = cityName
        self.CurrentlyWorking = CurrentlyWorking
        self.country_id = country_id
        self.worksector_id = worksector_id
        self.jobtype_id = jobtype_id
        self.city_id = city_id
        self.Duties = Duties
        self.fromDate = fromDate
        self.toDate = toDate
        self.job_category_id = job_category_id
        
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        jobTitle <- map[SerializationKeys.jobTitle]
        companyName <- map[SerializationKeys.companyName]
        yearsOfExperience <- map[SerializationKeys.yearsOfExperience]
        month_of_experience <- map[SerializationKeys.month_of_experience]
        noticePeriod <- map[SerializationKeys.noticePeriod]
        cityName <- map[SerializationKeys.cityName]
        CurrentlyWorking <- map[SerializationKeys.CurrentlyWorking]
        country_id <- map[SerializationKeys.country_id]
        worksector_id <- map[SerializationKeys.worksector_id]
        jobtype_id <- map[SerializationKeys.jobtype_id]
        city_id <- map[SerializationKeys.city_id]
        Duties <- map[SerializationKeys.Duties]
        fromDate <- map[SerializationKeys.fromDate]
        toDate <- map[SerializationKeys.toDate]
        job_category_id <- map[SerializationKeys.job_category_id]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.jobTitle] = jobTitle
        dictionary[SerializationKeys.companyName] = companyName
        if let value = yearsOfExperience { dictionary[SerializationKeys.yearsOfExperience] = value }
        dictionary[SerializationKeys.month_of_experience] = month_of_experience
        dictionary[SerializationKeys.noticePeriod] = noticePeriod
        dictionary[SerializationKeys.cityName] = cityName
        dictionary[SerializationKeys.country_id] = country_id
        dictionary[SerializationKeys.CurrentlyWorking] = CurrentlyWorking
        dictionary[SerializationKeys.worksector_id] = worksector_id
        dictionary[SerializationKeys.jobtype_id] = jobtype_id
        dictionary[SerializationKeys.city_id] = city_id
        dictionary[SerializationKeys.Duties] = Duties
        dictionary[SerializationKeys.fromDate] = fromDate
        dictionary[SerializationKeys.toDate] = toDate
        dictionary[SerializationKeys.job_category_id] = job_category_id
        
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.jobTitle = (aDecoder.decodeObject(forKey: SerializationKeys.jobTitle) as? String)!
        self.companyName = (aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String)!
        self.yearsOfExperience = aDecoder.decodeObject(forKey: SerializationKeys.yearsOfExperience) as? Int
        self.month_of_experience = aDecoder.decodeObject(forKey: SerializationKeys.month_of_experience) as? Int
        self.noticePeriod = aDecoder.decodeObject(forKey: SerializationKeys.noticePeriod) as? String
        self.cityName = aDecoder.decodeObject(forKey: SerializationKeys.cityName) as? String
        self.country_id = aDecoder.decodeObject(forKey: SerializationKeys.country_id) as? Int
        self.CurrentlyWorking = aDecoder.decodeObject(forKey: SerializationKeys.CurrentlyWorking) as? Int
        self.worksector_id = aDecoder.decodeObject(forKey: SerializationKeys.worksector_id) as? Int
        self.jobtype_id = aDecoder.decodeObject(forKey: SerializationKeys.jobtype_id) as? Int
        self.city_id = aDecoder.decodeObject(forKey: SerializationKeys.city_id) as? Int
        self.Duties = aDecoder.decodeObject(forKey: SerializationKeys.Duties) as? String
        self.fromDate = aDecoder.decodeObject(forKey: SerializationKeys.fromDate) as? String
        self.toDate = aDecoder.decodeObject(forKey: SerializationKeys.toDate) as? String
        self.job_category_id = aDecoder.decodeObject(forKey: SerializationKeys.job_category_id) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(jobTitle, forKey: SerializationKeys.jobTitle)
        aCoder.encode(companyName, forKey: SerializationKeys.companyName)
        aCoder.encode(yearsOfExperience, forKey: SerializationKeys.yearsOfExperience)
        aCoder.encode(month_of_experience, forKey: SerializationKeys.month_of_experience)
        aCoder.encode(noticePeriod, forKey: SerializationKeys.noticePeriod)
        aCoder.encode(cityName, forKey: SerializationKeys.cityName)
        aCoder.encode(country_id, forKey: SerializationKeys.country_id)
        aCoder.encode(CurrentlyWorking, forKey: SerializationKeys.CurrentlyWorking)
        aCoder.encode(worksector_id, forKey: SerializationKeys.worksector_id)
        aCoder.encode(jobtype_id, forKey: SerializationKeys.jobtype_id)
        aCoder.encode(city_id, forKey: SerializationKeys.city_id)
        aCoder.encode(Duties, forKey: SerializationKeys.Duties)
        aCoder.encode(fromDate, forKey: SerializationKeys.fromDate)
        aCoder.encode(toDate, forKey: SerializationKeys.toDate)
        aCoder.encode(job_category_id, forKey: SerializationKeys.job_category_id)
    }
    
}
