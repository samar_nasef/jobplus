//
//	Payload.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Payload : NSObject, NSCoding, Mappable{

	var type : Int?


	class func newInstance(map: Map) -> Mappable?{
		return Payload()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		type <- map["type"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         type = aDecoder.decodeObject(forKey: "type") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}