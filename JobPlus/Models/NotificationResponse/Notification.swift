//
//	Notification.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Notification : NSObject, NSCoding, Mappable{

	var body : AnyObject?
	var createdAt : String?
	var id : Int?
	var payload : Payload?
	var title : String?


	class func newInstance(map: Map) -> Mappable?{
		return Notification()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		body <- map["body"]
		createdAt <- map["created_at"]
		id <- map["id"]
		payload <- map["payload"]
		title <- map["title"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         body = aDecoder.decodeObject(forKey: "body") as? AnyObject
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         payload = aDecoder.decodeObject(forKey: "payload") as? Payload
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if body != nil{
			aCoder.encode(body, forKey: "body")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if payload != nil{
			aCoder.encode(payload, forKey: "payload")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}