//
//  NotificationsCountRes.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/16/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation

struct NotificationsCountRes: Codable {
    let notifications_count: Int
    let NotificationStatus: Int
    let showOffers: Int?
    let showNewsArticles: Int?
    private enum CodingKeys : String, CodingKey {
        case notifications_count = "notifications count"
        case NotificationStatus = "NotificationStatus"
        case showOffers = "showOffers"
        case showNewsArticles = "showNewsArticles"
    }
}
struct NotificationStatusRes: Decodable{
    let status: Bool?
}
