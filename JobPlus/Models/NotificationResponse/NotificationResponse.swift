//
//	NotificationResponse.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper

struct NotificaationsListResponse: Decodable {
    let current_page: Int
    var data: [NotificationItem]?
    let first_page_url: String?
    let from: Int?
    let last_page: Int?
    let per_page: Int?
    let total: Int?
    
}

struct NotificationItem : Decodable{
    let id: Int
    let title: String?
    let bodyEn: String
    let bodyAr: String?
    let created_at: String
    let job_id: Int?
    
}
class NotificationResponse : NSObject, NSCoding, Mappable{
    
    var hasMorePages : Bool?
    var message : String?
    var notifications : [Notification]?
    var status : Bool?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return NotificationResponse()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        hasMorePages <- map["hasMorePages"]
        message <- map["message"]
        notifications <- map["notifications"]
        status <- map["status"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        hasMorePages = aDecoder.decodeObject(forKey: "hasMorePages") as? Bool
        message = aDecoder.decodeObject(forKey: "message") as? String
        notifications = aDecoder.decodeObject(forKey: "notifications") as? [Notification]
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if hasMorePages != nil{
            aCoder.encode(hasMorePages, forKey: "hasMorePages")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if notifications != nil{
            aCoder.encode(notifications, forKey: "notifications")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}
