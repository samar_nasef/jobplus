//
//  ProfileImgRes.swift
//  JobPlus
//
//  Created by elsaid yousif on 9/15/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation
import ObjectMapper

class editProfileImageResponse : NSObject, NSCoding, Mappable{

    var message : String?
    var error : String?
    var status : Bool?
    var userData : User?
    var errors : ErrorResponse?


    class func newInstance(map: Map) -> Mappable?{
        return editProfileImageResponse()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        error <- map["error"]
        message <- map["message"]
        status <- map["status"]
        userData <- map["userData"]
        errors <- map["errors"]
        
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Bool
         userData = aDecoder.decodeObject(forKey: "userData") as? User
         errors = aDecoder.decodeObject(forKey: "errors") as? ErrorResponse

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if userData != nil{
            aCoder.encode(userData, forKey: "userData")
        }
        if errors != nil{
            aCoder.encode(errors, forKey: "errors")
        }

    }

}
