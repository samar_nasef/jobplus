//
//	Data.swift
//
//	Create by passant on 6/1/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class City : NSObject, NSCoding, Mappable{

	var cityAr : String?
	var cityEn : String?
	var id : Int?


	class func newInstance(map: Map) -> Mappable?{
		return City()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		cityAr <- map["cityAr"]
		cityEn <- map["cityEn"]
		id <- map["id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cityAr = aDecoder.decodeObject(forKey: "cityAr") as? String
         cityEn = aDecoder.decodeObject(forKey: "cityEn") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cityAr != nil{
			aCoder.encode(cityAr, forKey: "cityAr")
		}
		if cityEn != nil{
			aCoder.encode(cityEn, forKey: "cityEn")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}

	}

}

extension City {
   
    var localizeName: String? {
        if L102Language.currentAppleLanguage() == "ar" {
            return self.cityAr ?? self.cityEn
        }else{
            return self.cityEn ?? self.cityAr
        }
    }
}
