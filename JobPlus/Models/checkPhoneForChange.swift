//
//  checkPhoneForChange.swift
//  JobPlus
//
//  Created by elsaid yousif on 1/18/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation

struct checkPhoneForChangeRes : Decodable {
    let message: String?
    let status: Bool?
    let errors: err?
}
struct err: Decodable{
    let mobile: String?
    let mobileVerificationCode: String?
}
