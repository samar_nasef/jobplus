//
//	CatgoriesResponse.swift
//
//	Create by apple on 29/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class CatgoriesResponse : NSObject, NSCoding, Mappable{

	var catgoery : [Catgoery]?


	class func newInstance(map: Map) -> Mappable?{
		return CatgoriesResponse()
	}
	required init?(map: Map){}
    override init(){}

	func mapping(map: Map)
	{
		catgoery <- map["data"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         catgoery = aDecoder.decodeObject(forKey: "catgoery") as? [Catgoery]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if catgoery != nil{
			aCoder.encode(catgoery, forKey: "catgoery")
		}

	}

}
