//
//	Catgoery.swift
//
//	Create by apple on 29/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Catgoery : NSObject, NSCoding, Mappable{

	var id : Int?
	var imagePath : String?
	var name : String?
    var numberOfJobs : AnyObject?
    var nameAr : AnyObject?

	class func newInstance(map: Map) -> Mappable?{
		return Catgoery()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		id <- map["id"]
		imagePath <- map["imagePath"]
		name <- map["name"]
        numberOfJobs <- map["jobsCount"]
        nameAr <- map["nameAr"]
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
        imagePath = aDecoder.decodeObject(forKey: "imagePath") as?  String
         name = aDecoder.decodeObject(forKey: "name") as? String
        numberOfJobs = aDecoder.decodeObject(forKey: "jobsCount") as AnyObject?
        nameAr = aDecoder.decodeObject(forKey: "nameAr") as AnyObject?
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if imagePath != nil{
			aCoder.encode(imagePath, forKey: "imagePath")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
        if numberOfJobs != nil{
            aCoder.encode(numberOfJobs, forKey: "jobsCount")
        }
        if nameAr != nil{
            aCoder.encode(nameAr, forKey: "nameAr")
        }

	}

}
