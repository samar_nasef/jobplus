//
//  AddFeedBackModel.swift
//  JobPlus
//
//  Created by elsaid yousif on 01/02/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation

struct AddFeedBackModel: Decodable {
    let status: Bool?
    let message: String?
}
