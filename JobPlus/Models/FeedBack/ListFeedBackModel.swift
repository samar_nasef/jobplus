//
//  ListFeedBackModel.swift
//  JobPlus
//
//  Created by elsaid yousif on 01/02/2023.
//  Copyright © 2023 elsaid yousif. All rights reserved.
//

import Foundation

struct ListFeedBackModel: Decodable{
    let status: Bool?
    let feedback: [feedBackItem]?
    
}

struct feedBackItem: Decodable{
    let rate: String?
    let message: String?
    let created_at: String?
}
