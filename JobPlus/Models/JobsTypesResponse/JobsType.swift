//
//	JobsType.swift
//
//	Create by apple on 2/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class JobsType : NSObject, NSCoding, Mappable{

	var color : String?
	var id : Int?
	var jobsCount : Int?
	var name : String?


	class func newInstance(map: Map) -> Mappable?{
		return JobsType()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		color <- map["color"]
		id <- map["id"]
		jobsCount <- map["jobsCount"]
		name <- map["name"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         color = aDecoder.decodeObject(forKey: "color") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         jobsCount = aDecoder.decodeObject(forKey: "jobsCount") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if color != nil{
			aCoder.encode(color, forKey: "color")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if jobsCount != nil{
			aCoder.encode(jobsCount, forKey: "jobsCount")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}

	}

}