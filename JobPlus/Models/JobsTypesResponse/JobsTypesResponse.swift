//
//	JobsResponse.swift
//
//	Create by apple on 2/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class JobsTypesResponse : NSObject, NSCoding, Mappable{

	var jobsTypes : [JobsType]?


	class func newInstance(map: Map) -> Mappable?{
		return JobsTypesResponse()
	}
	required init?(map: Map){}
    override init(){}

	func mapping(map: Map)
	{
		jobsTypes <- map["data"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         jobsTypes = aDecoder.decodeObject(forKey: "data") as? [JobsType]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if jobsTypes != nil{
			aCoder.encode(jobsTypes, forKey: "data")
		}

	}

}
