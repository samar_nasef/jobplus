//
//  Company.swift
//  JobPlus
//
//  Created by elsaid yousif on 13/04/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation

struct companiesRes: Decodable {
    let status: Bool?
    let companies: [companyItem]?
}

struct companyItem: Decodable {
    let id: Int?
    let name: String?
}

struct workSectorsRes: Decodable {
    let status: Bool?
    let workSectors: [workSectorItem]?
}

struct workSectorItem: Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
}

struct disabilityRes: Decodable {
    let status: Bool?
    let disabilitytype: [DisabilityTypeItem]?
}
