//
//  ContactUsModel.swift
//  JobPlus
//
//  Created by elsaid yousif on 18/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import Foundation
import ObjectMapper

class ContactUsRes : NSObject, NSCoding, Mappable{

    var status: Bool?
    var contactPhone : String?
    var contactEmail : String?


    class func newInstance(map: Map) -> Mappable?{
        return ContactUsRes()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        status <- map["status"]
        contactPhone <- map["contactPhone"]
        contactEmail <- map["contactEmail"]
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        contactPhone = aDecoder.decodeObject(forKey: "contactPhone") as? String
        contactEmail = aDecoder.decodeObject(forKey: "contactEmail") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if contactPhone != nil{
            aCoder.encode(contactPhone, forKey: "contactPhone")
        }
        if contactEmail != nil{
            aCoder.encode(contactEmail, forKey: "contactEmail")
        }
    }

}
