//
//  InvoicesModel.swift
//  JobPlus
//
//  Created by elsaid yousif on 19/01/2022.
//  Copyright © 2022 elsaid yousif. All rights reserved.
//

import Foundation

struct InvoicesModel: Decodable {
    let status: Bool?
    let Envoices: [InvoiceItem]?
}

struct InvoiceItem: Decodable{
    let PackageName: String?
    let InvoiceNumber: Int?
    let created_at: String?
    let end_subscribtation_date: String?
    let Price: String?
    let PaymentMethod: String?
    let Vat: String?
    let TotalPrice: String?
    let JobPlusLogo: String?
    let JobPlusOfficialNameEn: String?
    let JobPlusOfficialNameAr: String?
    let JobPlusAddressEn: String?
    let JobPlusAddressAr: String?
    let TaxIdNumber: String?
    let Username: String?
    let Email: String?
    let qrCodeData: String?
    let VatValue: Float?
    let pdfFilePath: String?
}
