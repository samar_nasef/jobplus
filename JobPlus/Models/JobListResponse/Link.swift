//
//	Link.swift
//
//	Create by apple on 6/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Link : NSObject, NSCoding, Mappable{

	var first : String?
	var last : String?
	var next : AnyObject?
	var prev : AnyObject?


	class func newInstance(map: Map) -> Mappable?{
		return Link()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		first <- map["first"]
		last <- map["last"]
		next <- map["next"]
		prev <- map["prev"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         first = aDecoder.decodeObject(forKey: "first") as? String
         last = aDecoder.decodeObject(forKey: "last") as? String
         next = aDecoder.decodeObject(forKey: "next") as? AnyObject
         prev = aDecoder.decodeObject(forKey: "prev") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if first != nil{
			aCoder.encode(first, forKey: "first")
		}
		if last != nil{
			aCoder.encode(last, forKey: "last")
		}
		if next != nil{
			aCoder.encode(next, forKey: "next")
		}
		if prev != nil{
			aCoder.encode(prev, forKey: "prev")
		}

	}

}