//
//	JobListResponse.swift
//
//	Create by apple on 6/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


struct JobListResponse : Decodable{
    let current_page: Int
    var data: [JobItem]?
    let userDeleted: Int?
    let first_page_url: String?
    let from: Int?
    let last_page: Int?
    let per_page: Int?
    let total: Int?
    let subscribedUser: Bool?
    var DisplayAdAfter:Int?
    var NotSubscribedMeassage:String?
}
struct JobItem : Decodable{
    let id: Int
    let title: String?
    let companyName: String
    let imagePath: String?
    let city: String
    let job_type_id: Int?
    let job_type_name: String?
    let job_type_color: String?
    let created_at: String?
    var appliedForJob: Bool?
    let expireDate: String?
    let job_status: Int?
    var jobExpired: Bool? = false
    let workTimeText: String?
    let DisabilityEnabled: Int?
    
}
class adItem {
    var id: Int = 0
    var title: String = ""
    
    init(id:Int,title: String) {
           
        self.id = id
        self.title = title
    }
}
class jobItem2ForAds {
    var id: Int = 0
    var title: String = ""
    var companyName: String = ""
    var imagePath: String = ""
    var city: String = ""
    var job_type_id: Int = 0
    var job_type_name: String = ""
    var job_type_color: String = ""
    var created_at: String = ""
    var appliedForJob: Bool = false
    var expireDate: String = ""
    var job_status: Int = 0
    var jobExpired: Bool?
    var workTimeText: String = ""
    var DisabilityEnabled = 0
    
    
    init(id:Int,title: String,companyName: String,imagePath: String, city: String,job_type_id: Int,job_type_name: String, job_type_color: String,created_at: String,appliedForJob: Bool,expireDate: String,job_status: Int,jobExpired: Bool,workTimeText: String, DisabilityEnabled: Int) {
           
        self.id = id
        self.title = title
        self.companyName = companyName
        self.imagePath = imagePath
        self.city = city
        self.job_type_id = job_type_id
        self.job_type_name = job_type_name
        self.job_type_color = job_type_color
        self.created_at = created_at
        self.appliedForJob = appliedForJob
        self.expireDate = expireDate
        self.job_status = job_status
        self.jobExpired = jobExpired
        self.workTimeText = workTimeText
        self.DisabilityEnabled = DisabilityEnabled
       }
}
struct cancelApplyRes: Decodable {
    let status: Bool?
    let jobData: [jobDetail]?
}

struct increaseClicksRes: Decodable{
    let status: String?
}
