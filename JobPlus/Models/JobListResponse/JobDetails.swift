//
//	JobDetails.swift
//
//	Create by passant on 3/1/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper

struct jobDetailRes : Decodable {
    let job: [jobDetail]?
    let numberOfAppliedJobsThisMonth:Int?
    let FullScreenAdAfter:Int?
}

struct jobDetail : Decodable {
    let id: Int
    let created_at: String?
    let companyName: String?
    let adminActive: Int?
    let expireDate: String?
    let city: String?
    let title: String
    let description: String?
    let requirements: [String]?
    //let jobQuestionsCount: Int?
    let job_status: Int?
    let imagePath: String?
    let job_type_name: String?
    let job_type_color: String?
    let appliedForJob: Bool?
    let job_salary: Int?
    let SalaryText: String?
    let gender: Int?
    let questionsCount: Int?
    let questions: [question]?
    let subscribedUser: Bool?
    let candidateNumbers: Int?
    let appliedForJobDate: String?
    let workTimeText: String?
    let ExternalApply: Int?
    let ExternalApplyLink: String?
    let DisabilityEnabled: Int?
    let userJobValidatiy: Float?
    let Qualifications: String?
    let specialityName: String?
    let experienceName: String?
    let ageFrom: Int?
    let ageTo: Int?
    let JobLanguages: [languageItem]?
}

struct languageItem: Decodable{
    let id: Int?
    let nameAr: String?
    let nameEn: String?
}

struct question: Decodable {
    let nameAr: String?
}

struct ReqItem: Decodable {
    let reqItem : String?
}


////////////// old
class JobDetails : NSObject, NSCoding, Mappable{

	var job : Job?
    var errors : ErrorResponse?

	class func newInstance(map: Map) -> Mappable?{
		return JobDetails()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		job <- map["job"]
		errors <- map["errors"]
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         job = aDecoder.decodeObject(forKey: "job") as? Job
        errors = aDecoder.decodeObject(forKey: "errors") as? ErrorResponse

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if job != nil{
			aCoder.encode(job, forKey: "job")
		}
        if errors != nil{
            aCoder.encode(errors, forKey: "errors")
        }

	}

}
