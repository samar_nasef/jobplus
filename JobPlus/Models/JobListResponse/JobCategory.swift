//
//    JobCategory.swift
//
//    Create by apple on 6/8/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper


class JobCategory : NSObject, NSCoding, Mappable{
    
    var id : Int?
    var imagePath : String?
    var jobsCount : Int?
    var name:String = "name"
    
    class func newInstance(map: Map) -> Mappable?{
        return JobCategory()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        imagePath <- map["imagePath"]
        jobsCount <- map["jobsCount"]
        name <- map["name"]
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        imagePath = aDecoder.decodeObject(forKey: "imagePath") as? String
        jobsCount = aDecoder.decodeObject(forKey: "jobsCount") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imagePath != nil{
            aCoder.encode(imagePath, forKey: "imagePath")
        }
        if jobsCount != nil{
            aCoder.encode(jobsCount, forKey: "jobsCount")
        }
        
    }
    
}
