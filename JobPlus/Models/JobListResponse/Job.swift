//
//	Data.swift
//
//	Create by apple on 6/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Job : NSObject, NSCoding, Mappable{

	var address : String?
	var appliersCount : Int?
	var descriptionField : String?
	var id : Int?
	var jobCategoryId : Int?
	var jobTypeId : Int?
	var requirements : [String]?
    var requirementsString = ""
    var requirementStringAttributedString:NSAttributedString?
	var title : String?
    var jobCategory : JobCategory?
    var jobType : JobType?
    var jobQuestionsCount :Int?
    
    var adminActive : Int?
    var appliedForJob : Bool?
    var company : Company?
    var companyActive : Int?
    var job : Job?
    var createdAt : String?
    var questionsAnswers : [QuestionsAnswer]?
    var timestamp :String?

    
    
    
	class func newInstance(map: Map) -> Mappable?{
		return Job()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
        company <- map["company"]
		address <- map["address"]
		appliersCount <- map["appliersCount"]
		descriptionField <- map["description"]
		id <- map["id"]
		jobCategoryId <- map["job_category_id"]
		jobTypeId <- map["job_type_id"]
		requirements <- map["requirements"]
        createdAt <- map["created_at"]
        timestamp <- map["timestamp"]
//        if requirements != nil{
//        for requirement in requirements! {
//             if MoLocalization.currentAppleLanguage() == MoLocalization.Language.ENGLISH {
//            requirementsString =  requirementsString + "•  " + requirement + "\n\n"
//             }else {
//                requirementsString =  requirementsString + "\n\n" + requirement + "  •"
//            }
//        }
//        }
       // requirementStringAttributedString = highlight(word: "•", in: requirementsString, with: UIColor.spiroDiscoBall)
		title <- map["title"]
        jobCategory <- map["job_category"]
        jobType <- map["job_type"]
        jobQuestionsCount <- map["jobQuestionsCount"]
        
        adminActive <- map["adminActive"]
        appliedForJob <- map["appliedForJob"]
        companyActive <- map["companyActive"]
        job <- map["job"]
        questionsAnswers <- map["questions_answers"]
        
	}
    func highlight(word: String, in str: String, with color: UIColor) -> NSAttributedString {
        let titleParagraphStyle = NSMutableParagraphStyle()
        if L102Language.currentAppleLanguage() == "ar" {
            titleParagraphStyle.alignment = .right
        }else {
            titleParagraphStyle.alignment = .left
        }
        let attributedString = NSMutableAttributedString(string: str,
                                                         attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0),
                                                                      NSAttributedString.Key.paragraphStyle:titleParagraphStyle])
        let highlightAttributes = [NSAttributedString.Key.font:FontHelper.defaultRegularFontWithSize(size: 25), NSAttributedString.Key.foregroundColor: UIColor.spiroDiscoBall]
        
        let nsstr = str as NSString
        var searchRange = NSMakeRange(0, nsstr.length)
        
        while true {
            let foundRange = nsstr.range(of: word, options: [], range: searchRange)
            if foundRange.location == NSNotFound {
                break
            }
            
            attributedString.setAttributes(highlightAttributes, range: foundRange)
            
            let newLocation = foundRange.location + foundRange.length
            let newLength = nsstr.length - newLocation
            searchRange = NSMakeRange(newLocation, newLength)
        }
        
        return attributedString
    }
    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         appliersCount = aDecoder.decodeObject(forKey: "appliersCount") as? Int
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         jobCategoryId = aDecoder.decodeObject(forKey: "job_category_id") as? Int
         jobTypeId = aDecoder.decodeObject(forKey: "job_type_id") as? Int
         requirements = aDecoder.decodeObject(forKey: "requirements") as? [String]
         title = aDecoder.decodeObject(forKey: "title") as? String
        
        adminActive = aDecoder.decodeObject(forKey: "adminActive") as? Int
        appliedForJob = aDecoder.decodeObject(forKey: "appliedForJob") as? Bool
        companyActive = aDecoder.decodeObject(forKey: "companyActive") as? Int
    
        job = aDecoder.decodeObject(forKey: "job") as? Job

        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        questionsAnswers = aDecoder.decodeObject(forKey: "questionsAnswers") as? [QuestionsAnswer]
        jobQuestionsCount = aDecoder.decodeObject(forKey: "jobQuestionsCount") as? Int
        timestamp = aDecoder.decodeObject(forKey: "timestamp") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if appliersCount != nil{
			aCoder.encode(appliersCount, forKey: "appliersCount")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if jobCategoryId != nil{
			aCoder.encode(jobCategoryId, forKey: "job_category_id")
		}
		if jobTypeId != nil{
			aCoder.encode(jobTypeId, forKey: "job_type_id")
		}
		if requirements != nil{
			aCoder.encode(requirements, forKey: "requirements")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

       
        if adminActive != nil{
            aCoder.encode(adminActive, forKey: "adminActive")
        }
        if appliedForJob != nil{
            aCoder.encode(appliedForJob, forKey: "appliedForJob")
        }
        if companyActive != nil{
            aCoder.encode(companyActive, forKey: "companyActive")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
      
        if job != nil{
            aCoder.encode(job, forKey: "job")
        }
        
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if questionsAnswers != nil{
            aCoder.encode(questionsAnswers, forKey: "questionsAnswers")
        }
        if jobQuestionsCount != nil{
            aCoder.encode(jobQuestionsCount, forKey: "jobQuestionsCount")
        }
        if timestamp != nil{
            aCoder.encode(timestamp, forKey: "timestamp")
        }
	}

}
