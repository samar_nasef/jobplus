//
//  arabClicks.swift
//  JobPlus
//
//  Created by samar nasef on 02/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
struct categoriesAndMarketsRes: Decodable {
    let status:Bool?
    let categories:[arabClickCategoryItem]?
    let Markets:[arabClickMarketsItem]?
}
struct arabClickCategoryItem:Decodable {
    let id: Int?
    let name:String?
    let arabclickcategoryID:Int?
    let status:Int?
    let IsDefault: Int?
    
}
struct arabClickMarketsItem:Decodable {
    let id: Int?
    let name:String?
    let arabclickcountryID:Int?
    let status:Int?
    
}
struct arabClicksOffersRes: Decodable {
    let status:Bool?
    let Offers:[arabClickOfferItem]?
    
}
struct arabClickOfferItem:Decodable {
    let id: Int?
    var name:String?
    let arabclickOfferID:Int?
    let arabclickcategoryID:Int?
    let description:String?
    let terms_and_conditions:String?
    let preview_url:String?
    let status:Int?
    let arabclickcountryID:Int?
    let arabclickImage:String?
    let arabclickPromoCode:String?

    
}
