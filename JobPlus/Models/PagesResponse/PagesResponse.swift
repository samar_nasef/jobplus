//
//	PagesResponse.swift
//
//	Create by passant on 30/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class PagesResponse : NSObject, NSCoding, Mappable{

	var page : Page?


	class func newInstance(map: Map) -> Mappable?{
		return PagesResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		page <- map["page"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         page = aDecoder.decodeObject(forKey: "page") as? Page

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if page != nil{
			aCoder.encode(page, forKey: "page")
		}

	}

}