//
//  DegreesTimesModel.swift
//  JobPlus
//
//  Created by elsaid yousif on 9/7/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation

struct DegreesTimesApiRes: Decodable {
    let certificates: [certificate]?
    let worktimes: [worktims]?
    let countries: [countryItem]?
}
struct NewCertificateApiRes: Decodable {
    let PreferedContact: [PreferredContactItem]?
    let softSkills : [softSkillItem]?
    let techSkills : [techSkillItem]?
    let languages : [LangItem]?
    let languageStrength : [languageStrengthItem]?
    let WorkSectors : [workSectorItem]?
    let cvTemplates : [cvTempleteItem]?
    let Institutions : [institutionItem]?
    let certificates : [certificatesItem]?
    let JobTypes : [workSectorItem]?
    let countries : [countryItem]?
    let DisabilityType : [DisabilityTypeItem]?
    let specialities : [specialityItem]?
    let experiences : [experienceItem]?
    let Job_categories : [Job_categoriesItem]?
}
struct Job_categoriesItem:Decodable{
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    var selected: Bool?
}
struct specialityItem:Decodable{
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    var selected: Bool?
}
struct experienceItem:Decodable{
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    var selected: Bool?
}

struct DisabilityTypeItem:Decodable{
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    var selected: Bool?
}

struct countriesRes: Decodable {
    let data: [countryItem]?
}
struct certificatesItem:Decodable{
    let id: Int?
    let certificateEn: String?
    let certificateAr: String?
    let certificateActive: Int?
    let RequireMajor: Int?
    let RequireDate: Int?
    let RequireGrades: Int?

}

struct softSkillItem:Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
//    let skillType: Int?
  
}

struct languageStrengthItem:Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    let Active: Int?
  
}
struct institutionItem:Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    let Active: Int?
  
}
struct cvTempleteItem:Decodable {
    let id: Int?
    let cvName:String?
    let imageEn: String?
    let imageAr: String?
    let Active: Int?
  
}
struct LangItem:Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    let Active: Int?
  
}
struct techSkillItem:Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
//    let skillType: Int?
  
}
struct countryItem: Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    let code: String?
    let cities: [cityItem]?
}
struct cityItem: Decodable{
    let id: Int?
    let cityEn: String?
    let cityAr: String?
}
struct PreferredContactItem: Decodable {
    let id: Int?
    let nameEn: String?
    let nameAr: String?
    
}
struct certificate: Decodable{
    let id: Int?
    let certificateAr: String?
    let certificateEn: String?
}
struct workTimeRes: Decodable {
    let status: Bool?
    let worktimes: [worktims]?
}
struct worktims: Decodable{
    let id: Int?
    let worktimeAr: String?
    let worktimeEn: String?
}

struct specialitiesRes: Decodable {
    let status: Bool?
    let specialities: [specialityItem]?
}
struct experiencesRes: Decodable {
    let status: Bool?
    let experiences: [experienceItem]?
}
