//
//	RootClass.swift
//
//	Create by passant on 20/5/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class GeneralResponse : NSObject, NSCoding, Mappable{

	var message : String?
	var status : String?
    var errors : ErrorResponse?
    var user : User?

	class func newInstance(map: Map) -> Mappable?{
		return GeneralResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
       
		message <- map["message"]
		status <- map["status"]
        errors <- map["errors"]
        user <- map["user"]	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         errors = aDecoder.decodeObject(forKey: "errors") as? ErrorResponse
         user = aDecoder.decodeObject(forKey: "user") as? User
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
        
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if errors != nil{
            aCoder.encode(errors, forKey: "errors")
        }
        if user != nil{
            aCoder.encode(user, forKey: "user")
        }
	}

}
