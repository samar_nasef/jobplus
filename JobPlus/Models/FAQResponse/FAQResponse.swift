//
//	FAQResponse.swift
//
//	Create by passant on 6/1/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FAQResponse : NSObject, NSCoding, Mappable{

	var data : [QuestionsAnswer]?
	var links : Link?
	var meta : Meta?
    var errors : ErrorResponse?
    var message : String?


	class func newInstance(map: Map) -> Mappable?{
		return FAQResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		data <- map["data"]
		links <- map["links"]
		meta <- map["meta"]
		errors <- map["errors"]
        message <- map["message"]
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObject(forKey: "data") as? [QuestionsAnswer]
         links = aDecoder.decodeObject(forKey: "links") as? Link
         meta = aDecoder.decodeObject(forKey: "meta") as? Meta
        errors = aDecoder.decodeObject(forKey: "errors") as? ErrorResponse
        message = aDecoder.decodeObject(forKey: "message") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if links != nil{
			aCoder.encode(links, forKey: "links")
		}
		if meta != nil{
			aCoder.encode(meta, forKey: "meta")
		}
        if errors != nil{
            aCoder.encode(errors, forKey: "errors")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }

	}

}
