//
//	Data.swift
//
//	Create by passant on 5/2/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class CountryRes : NSObject, NSCoding, Mappable{

	var id : Int?
	var nameAr : String?
	var nameEn : String?
    var isoCode: String?


	class func newInstance(map: Map) -> Mappable?{
		return CountryRes()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		id <- map["id"]
		nameAr <- map["nameAr"]
		nameEn <- map["nameEn"]
        isoCode <- map["isoCode"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         nameAr = aDecoder.decodeObject(forKey: "nameAr") as? String
         nameEn = aDecoder.decodeObject(forKey: "nameEn") as? String
        isoCode = aDecoder.decodeObject(forKey: "isoCode") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if nameAr != nil{
			aCoder.encode(nameAr, forKey: "nameAr")
		}
		if nameEn != nil{
			aCoder.encode(nameEn, forKey: "nameEn")
		}
        if isoCode != nil{
            aCoder.encode(nameEn, forKey: "isoCode")
        }

	}

}

extension CountryRes {
    
    var localizeName: String? {
        if L102Language.currentAppleLanguage() == "ar" {
            return self.nameAr ?? self.nameEn
        }else{
            return self.nameEn ?? self.nameAr
        }
    }
}
