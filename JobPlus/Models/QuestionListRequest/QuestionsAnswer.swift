//
//	QuestionsAnswer.swift
//
//	Create by passant on 28/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class QuestionsAnswer : NSObject, NSCoding, Mappable{
 
    var id :Int?
	var answer : String?
	var question : String?
    var name : String?


	class func newInstance(map: Map) -> Mappable?{
		return QuestionsAnswer()
	}
	required init?(map: Map){}
	 override init(){}

	func mapping(map: Map)
	{
		answer <- map["answer"]
		question <- map["question"]
        name <- map["name"]
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         answer = aDecoder.decodeObject(forKey: "answer") as? String
         question = aDecoder.decodeObject(forKey: "question") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if answer != nil{
			aCoder.encode(answer, forKey: "answer")
		}
		if question != nil{
			aCoder.encode(question, forKey: "question")
		}
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }

	}
    
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        dictionary["answer"] = answer
        dictionary["question"] = question
        dictionary["name"] = name
        return dictionary
    }

}
