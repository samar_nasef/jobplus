//
//	QuestionListResponse.swift
//
//	Create by passant on 28/8/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class QuestionListRequest : NSObject, NSCoding, Mappable{

	var questionsAnswers : [QuestionsAnswer]?


	class func newInstance(map: Map) -> Mappable?{
		return QuestionListRequest()
	}
	required init?(map: Map){}
    override init(){}

	func mapping(map: Map)
	{
		questionsAnswers <- map["questionsAnswers"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         questionsAnswers = aDecoder.decodeObject(forKey: "questionsAnswers") as? [QuestionsAnswer]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if questionsAnswers != nil{
			aCoder.encode(questionsAnswers, forKey: " ")
		}

	}
    
  

}
