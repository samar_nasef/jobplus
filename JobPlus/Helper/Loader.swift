//
//  Loader.swift
//  Tania
//
//  Created by mohamed-shaat on 9/18/17.
//  Copyright © 2017 ibtikar. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class Loader: NSObject {
    static var loaderColor = UIColor.orangePeel
   static var animationFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100))
    let animationFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100))
    //1A927D
    static var  animationView = NVActivityIndicatorView(frame: animationFrame, type: NVActivityIndicatorType.ballScaleMultiple, color:loaderColor, padding: 10)
    static var moiaProgress : UIImageView = {
        
        let loaderImgView = UIImageView (frame: CGRect (origin: CGPoint.zero, size: CGSize (width: 100, height: 100)))
        loaderImgView.image = #imageLiteral(resourceName: "splash_logo")
        
        return loaderImgView
    }()
    
  
    
    static var heartbeatingAnimation:CAAnimationGroup = {
        
        let beatAnimation = CASpringAnimation(keyPath: "transform.scale")
        
        beatAnimation.duration = 0.5
        
        beatAnimation.fromValue = 1.0
        beatAnimation.toValue = 1.2
        
        beatAnimation.autoreverses = true
        
        beatAnimation.initialVelocity = 0.5
        beatAnimation.damping = 0.8
        
        
        let opacityAnimation = CASpringAnimation(keyPath: "opacity")
        
        opacityAnimation.duration = 0.5
        
        opacityAnimation.fromValue = 1.0
        opacityAnimation.toValue = 0.90
        
        opacityAnimation.autoreverses = true
        
        opacityAnimation.initialVelocity = 0.5
        opacityAnimation.damping = 0.8
        
        let heartbeating = CAAnimationGroup()
        
        heartbeating.duration = 1.5
        heartbeating.repeatCount = 1000
        
        heartbeating.animations = [beatAnimation , opacityAnimation]
        
        return heartbeating
    }()
    static func showProgress(){
        if #available(iOS 13.0, *) {
            let scene = UIApplication.shared.connectedScenes.first
            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                let mainwindow = sd.window
             animationView = NVActivityIndicatorView(frame: animationFrame, type: NVActivityIndicatorType.ballScaleMultiple, color:loaderColor, padding: 10)
                animationView.center = mainwindow!.center
                mainwindow!.addSubview(animationView)
             animationView.startAnimating()
            }
        } else {
        let window: UIView = ((UIApplication.shared.delegate?.window)!)!
        animationView = NVActivityIndicatorView(frame: animationFrame, type: NVActivityIndicatorType.ballScaleMultiple, color:loaderColor, padding: 10)
        animationView.center = window.center
        window.addSubview(animationView)
        animationView.startAnimating()
        }
    }
    
    static func hidePrgoress(){
        
           animationView.stopAnimating()
            animationView.removeFromSuperview()
       
        
    }
 

}


extension UIView
{
    func hideAnimated() {
        
        self.hideAnimated(completion: nil)
    }
    
    func hideAnimated( completion: (() -> Void)? ){
        
        self.alpha = 1
        self.isHidden = false
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.alpha = 0
            
        }) { (_) in
            
            self.alpha = 1
            self.isHidden = true
            
            completion?()
        }
    }
    
    func showupAnimated() {
        
        self.alpha = 0
        self.isHidden = false
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.alpha = 1
        })
    }
    
}
