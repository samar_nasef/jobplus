//
//  TextFieldPadding.swift
//  ecommerce
//
//  Created by elsaid yousif on 7/11/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit

class PaddedTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 10, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 10, width: bounds.width, height: bounds.height)
    }
    
}
