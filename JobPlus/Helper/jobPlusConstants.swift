//
//  Constants.swift
//  Najeez
//
//  Created by passant on 5/7/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
// custom font Helper
struct FontHelper {
    static func defaultRegularFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueLTArabic-Roman", size: size)!
    }
    static func defaultMediumFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueLTArabic-Roman", size: size)!
    }
    static func defaultBoldFontWithSize(size: CGFloat) -> UIFont {
        return  UIFont(name: "HelveticaNeueLTArabic-Bold", size: size)!
    }
//    static func defaultArabicBeinFontWithSize(size: CGFloat) -> UIFont {
//        return UIFont(name: "beINNormal", size: size)!
//    }
//
    static func defaultArabicRegularFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueLTArabic-Roman", size: size)!
    }
    static func defaultArabicBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeueLTArabic-Bold", size: size)!
    }
}

struct  KeysAndValues {
    static let KEY_GoogleMap = "AIzaSyB_ajUPT7GA8dpz19zwIPcmUJ34Uoh3TRE"
    static let KEY_GooglePlaces = "AIzaSyD1FCL3QS7qf4LNFTvunN9BM8-vRtwBE-U"
  //  static let KEYZENDESKKEY = "5RkcMTOSDHeuWFoidy5Q9VQ4HiNeDp60" // account najeez
    static let KEYZENDESKKEY = "5htELMj386OJfhMVKptPXmR6aFFf3GR1" // account shaat
    static let KEY_ApiName = "X-Api-Key"
    static let KEY_ApiValue="55axvd80c2jxp31y"
    static let KEY_LANG="Accept-Language"
    static let KEY_Authorization="Authorization"
    static let PLATFORM_IOS     = 2;
    static let TYPE_CLIENT = "client"
    static let TYPE_AGENT = "agent"
    static let TYPE_PROGRAMMING = "programming"
    static let TYPE_MAINTENANCE = "maintenance"
    static let ORDERASC = "ASC"
    static let ORDERDESC = "DESC"
    static let BannerViewAdUnitID = "ca-app-pub-3940256099942544/2934735716"
    static let InterstitialAdUnitID = "ca-app-pub-6076183060289610/1275006030"
}


struct WebService {
    
    static let URLDASHBOARDDOMAINen = "https://www.jobplus.biz/parttime/public/en"
    static let URLDASHBOARDDOMAINar = "https://www.jobplus.biz/parttime/public/ar"
    static let URLCOMPANYSIGNUPDOMAIN = "/company/signup"
    static let URLCOMPANYSIGNINDOMAIN = "/login"
    static let newsUrlEn = "https://www.jobplus.biz/parttime/public/en/news" // parttimetest
    static let newsUrlAr = "https://www.jobplus.biz/parttime/public/ar/news"
    static let discounstURLEn = "https://www.jobplus.biz/parttime/public/en/discounts"
    static let discountsURLAr = "https://www.jobplus.biz/z/public/ar/discounts"
    static let URLBASE = "https://www.jobplus.biz/parttime/public/api/v1/"
    //static let URLBASE = "https://www.jobplus.biz/parttime/public/api/v1/"
    static let URLLOGIN = "users/login-check"
    static let URLFORGOTPASSWORD = "users/forgot-password"
    static let URLFORGOTPASSWORDTOKEN = "users/change-password"
    static let URLUSERREGISTER = "users/create-user"
    static let URLEDITFIREBASETOKEN = "users/devices"
    static let URLVERIFICATION = "users/verify-mobile"
    static let URLLOGOUT = "users/devices/"
    static let URLDeleteAcount = "users/destroy"
    static let captchaGetCode = "users/captcha_code"
    static let URLCHANGEPASSWORD = "users/edit-password"
    static let URLRESENDVERIFICATIONCODE = "users/resend-mobile-verification"
    static let URLEDITPROFILE = "users/edit-account"
    static let URLEditProfileImg = "users/profileImageUpload"
    static let URLGETABOUTUSPAGE = "pages/about"
    static let URLPRIVACYPOLICYPAGE = "pages/privacy-policy"
    //////////////////JOBS//////////////////////
    static let URLJOBSCATGORIES = "jobs/FavouriteView"
    static let URLJOBSTYPES = "jobs/types"
    static let URLJOBS = "jobs"
    static let URLGETJOBDETAILS = "jobs/detail"
    static let URLRECOMMENDEDJOBS = "jobs/recommendedJobs"
    static let URLJOBNEWEST = "jobs/newlyAddedJobs"
    //--/////// Degress & Times /////////
    static let DegreedTimesURL = "certificates"
    //////////////////QUESTIONS/////////////////
    static let URLQUESTIONSLIST = "jobs/questions"
    //////////////////applied jobs //////////////
    static let URLGETAPPLIEDJOBS = "users/my-applied-jobs"
    // ----- get user info ////////////////////
    static let URLGETUSERINFO = "users/details"
    //------ notification list/////////////////
    static let URLGETUSERNOTIFICATIONS = "users/notifications/getAllUserNotifications"
    //------ notification list/////////////////
    static let URLGETGeneralNOTIFICATIONS = "users/notifications/getAllUserGeneralNotifications"
    //------ notification list/////////////////
    static let URLGETJobsNOTIFICATIONS = "users/notifications/getAllUserJobNotifications"
    //------ read notifications/////////////////
    static let URLREADUSERNOTIFICATIONS = "users/notifications/changeNotificationStatus"
    //------ notifications count/////////////////
    static let URLGETUSERNOTIFICATIONSCount = "users/notifications/getCountOfUserNotifications"
    //--------preferences////////////////////
    static let URLADDPREFERENCE = "users/edit-cats-preference"
    //-------FAQ////////////////////////////
    static let URLGETFAQ = "faq"
    //-------ChechPhoneToChange//////////////////////////
    static let CHECKPHONETOCHANGE = "users/check-mobile"
    //-------VERIFYPHONEAFTERCHANGE//////////////////////
    static let VERIFYPHONEFTERCHANGE = "users/verify-mobile-after-change"
    
    
    static let URLGETPackages = "users/packages"
    static let URLCreatePayPage = "users/create_pay_page"
    static let iapTransactionURL = "users/packages/InAppPurchasePayment"
    static let homeCatsURL = "jobs/HomeCategories"
    static let comapniesDataURL = "compainesData"
    static let workSectorsURL = "workSectors"
    static let workTimesURL = "worktimes"
    static let countriesURL = "countries"
    static let disabilityTypesURL = "disabilitytype"
    static let getJobsBySectorIDURL = "jobs/JobsByCategoryID"
    static let addCategoryToFavURL = "users/addCategoryToFavurites"
    static let removeCategoryToFavURL = "users/removeCategoryFromFavurites"
    static let editAccountURL = "users/edit-account-Data"
    static let hyperPayURL = "users/hyperPaypayment"
    static let certificatesNewURL = "certificatesNew"
    static let allMessagesURL = "users/allMessages"
    static let MessageDetailsURL = "users/MessageDetails?id="
    static let AddNewMessageOrReplayURL = "users/AddNewMessageOrReplay"
    static let categoriesAndMarketsURL = "arabclicks/categoriesAndMarkets"
    static let arabClicksOfferURL = "arabclicks/offers"
    static let userNewPackagesUrl = "users/packages/UserNewPackage"
    static let CancelAutoSubscribeURL = "users/packages/CancelAutoSubscribe"
    static let ActivateAutoSubscribeURL = "users/packages/ActivateAutoSubscribe"
    static let DeleteUserProfileImage = "users/DeleteUserProfile"
    static let ContactInfoURL = "contact-us"
    static let NotificationStatusURL = "users/NotificationStatus"
    static let InvoicesURL = "users/Envoices"
    static let increaseClicks = "increaseClicks"
    static let VisitCount = "Visit-count"
    static let storeVersion = "store/version"
    static let specialities = "specialities"
    static let experiences = "experiences"
    static let listFeedbackURL = "feedbacks/list"
    static let addFeedBackURL = "feedbacks/add"
}

extension NSNotification.Name {
    
    static let afterUserLogin = NSNotification.Name("afterUserLogin")
}
