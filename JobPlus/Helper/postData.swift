//
//  postData.swift
//  farawla
//
//  Created by Elsaid Yousif on 2/15/18.
//  Copyright © 2018 Elsaid Yousif. All rights reserved.
//

import UIKit

class ApiService
{
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    static func getPostSecString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + ":\(value)")
        }
        return data.map { String($0) }.joined(separator: ",")
    }
    static func callPost(url:URL, params:[String:Any], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            print(jsonData)
            //let theJSONText = String(data: jsonData,
                              //       encoding: .utf8)
           // let encryptedPostString = userStatus.shared.encryptData(data: theJSONText!)
          //  let EncryptedObject = ["data": encryptedPostString]
           // let finalEncryptedObject = self.getPostString(params: params)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            var result:(message:String, data:Data?) = (message: "Fail", data: nil)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
//                if let returnedData = String(data: data!, encoding: .utf8) {
//                   print(returnedData)
//                }
                if(error != nil)
                {
                    result.message = "Fail Error not null : \(error.debugDescription)"
                }
                else
                {
                    result.message = "Success"
                    result.data = data
                }
                
                finish(result)
            }
            task.resume()
        }
     catch {
            print(error)
        }
        //let postString = self.getPostSecString(params: params)
        
    }
}
