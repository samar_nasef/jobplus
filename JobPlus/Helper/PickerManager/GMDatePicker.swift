//
//  GMDatePicker.swift
//  GMPicker
//
//  Created by Gabor Csontos on 8/3/16.
//  Copyright © 2016 GabeMajorszki. All rights reserved.
//

import UIKit

protocol GMDatePickerDelegate: class {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date)
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker)
    
}

class GMDatePicker: UIView {
    
    // MARK: - Config
    struct Config {
        
        fileprivate let contentHeight: CGFloat = 250
        fileprivate let bouncingOffset: CGFloat = 20
        
        var startDate: Date?
        var minDate:Date?
        var maxDate: Date?

        var confirmButtonTitle = "done".localized()
        var cancelButtonTitle = "CancelPicker".localized()
        var buttonFontSize:CGFloat = 16
        
        var headerHeight: CGFloat = 44
        
        var animationDuration: TimeInterval = 0.5
        
        var contentBackgroundColor: UIColor = UIColor.white
        var headerBackgroundColor: UIColor = UIColor.greenBlue
        var confirmButtonColor: UIColor = UIColor.white
        var cancelButtonColor: UIColor = UIColor.white
        
        var overlayBackgroundColor: UIColor = UIColor.black.withAlphaComponent(0.6)
        
    }
    
    var config = Config()
    
    weak var delegate: GMDatePickerDelegate?
    
    // MARK: - Variables
    var datePicker = UIDatePicker()
   
    var confirmButton = UIButton()
    var cancelButton = UIButton()
    var headerView = UIView()
    var backgroundView = UIView()
    var headerViewHeightConstraint: NSLayoutConstraint!
    
    var bottomConstraint: NSLayoutConstraint!
    var overlayButton: UIButton!
    
    
    
    // MARK: - ButtonTouched
    @objc func confirmButtonDidTapped(_ sender: AnyObject) {
        
        config.startDate = datePicker.date
        dismiss()
        delegate?.gmDatePicker(self, didSelect: datePicker.date)
        
    }
    @objc func cancelButtonDidTapped(_ sender: AnyObject) {
        dismiss()
        delegate?.gmDatePickerDidCancelSelection(self)
    }
    
    
    
    
    // MARK: - Private
    fileprivate func setup(_ parentVC: UIViewController) {
        
        
        // Loading configuration
        datePicker.datePickerMode = .date
        if let startDate = config.startDate {
            datePicker.date = startDate
        }
        if let minDate = config.minDate{
            datePicker.minimumDate = minDate
        }
        if let maxDate = config.maxDate{
            datePicker.maximumDate = maxDate
        }
        
        datePicker.locale = Locale(identifier: L102Language.currentAppleLanguage())
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }

        // Loading configuration
        confirmButton.setTitle(config.confirmButtonTitle, for: UIControl.State())
        cancelButton.setTitle(config.cancelButtonTitle, for: UIControl.State())
        
        confirmButton.setTitleColor(config.confirmButtonColor, for: UIControl.State())
        cancelButton.setTitleColor(config.cancelButtonColor, for: UIControl.State())
        headerView.backgroundColor = config.headerBackgroundColor
        backgroundView.backgroundColor = config.contentBackgroundColor
        
        // Overlay view constraints setup
        
        overlayButton = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        overlayButton.backgroundColor = config.overlayBackgroundColor
        overlayButton.alpha = 0
        
        overlayButton.addTarget(self, action: #selector(cancelButtonDidTapped(_:)), for: .touchUpInside)
        
        if !overlayButton.isDescendant(of: parentVC.view) { parentVC.view.addSubview(overlayButton)}
        
        overlayButton.translatesAutoresizingMaskIntoConstraints = false
        
        parentVC.view.addConstraints([
            NSLayoutConstraint(item: overlayButton, attribute: .bottom, relatedBy: .equal, toItem: parentVC.view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: overlayButton, attribute: .top, relatedBy: .equal, toItem: parentVC.view, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: overlayButton, attribute: .leading, relatedBy: .equal, toItem: parentVC.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: overlayButton, attribute: .trailing, relatedBy: .equal, toItem: parentVC.view, attribute: .trailing, multiplier: 1, constant: 0)
            ]
        )
        
        // Setup picker constraints
        
        frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: config.contentHeight + config.headerHeight)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        bottomConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: parentVC.view, attribute: .bottom, multiplier: 1, constant: 0)
        
        if !isDescendant(of: parentVC.view) { parentVC.view.addSubview(self) }
        
        parentVC.view.addConstraints([
            bottomConstraint,
            NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: parentVC.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: parentVC.view, attribute: .trailing, multiplier: 1, constant: 0)
            ]
        )
        addConstraint(
            NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: frame.height)
        )
        
        
        if !headerView.isDescendant(of: self) { addSubview(headerView)}
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: config.headerHeight).isActive = true
        headerView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true

        if !confirmButton.isDescendant(of: headerView) { headerView.addSubview(confirmButton)}
     
        confirmButton.translatesAutoresizingMaskIntoConstraints = false
        headerView.trailingAnchor.constraint(equalTo: confirmButton.trailingAnchor, constant: 12).isActive = true
        confirmButton.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        confirmButton.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
//        confirmButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        confirmButton.addTarget(self, action: #selector(confirmButtonDidTapped), for: .touchUpInside)
        confirmButton.titleLabel?.font = UIFont.systemFont(ofSize: config.buttonFontSize)
        confirmButton.setTitleColor(.white, for: .normal)
        
        if !cancelButton.isDescendant(of: headerView) { headerView.addSubview(cancelButton)}
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 12).isActive = true
        cancelButton.topAnchor.constraint(equalTo: headerView.topAnchor).isActive = true
        cancelButton.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
//        cancelButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        cancelButton.addTarget(self, action: #selector(cancelButtonDidTapped), for: .touchUpInside)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: config.buttonFontSize)
        cancelButton.setTitleColor(.white, for: .normal)

        if !backgroundView.isDescendant(of: self) { addSubview(backgroundView)}
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        backgroundView.heightAnchor.constraint(equalToConstant: config.contentHeight).isActive = true
        backgroundView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        
        if !datePicker.isDescendant(of: backgroundView) { backgroundView.addSubview(datePicker)}
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor).isActive = true
        datePicker.heightAnchor.constraint(equalTo: backgroundView.heightAnchor).isActive = true
        datePicker.widthAnchor.constraint(equalTo: backgroundView.widthAnchor).isActive = true
        
        move(goUp: false)
        
    }
    fileprivate func move(goUp: Bool) {
        bottomConstraint.constant = goUp ? config.bouncingOffset : config.contentHeight + config.headerHeight
    }
    
    // MARK: - Public
    func show(inVC parentVC: UIViewController, completion: (() -> ())? = nil) {
        
        setup(parentVC)
        move(goUp: true)
        
        UIView.animate(
            withDuration: config.animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseIn, animations: {
                
                parentVC.view.layoutIfNeeded()
                self.overlayButton.alpha = 1
                
            }, completion: { (finished) in
                completion?()
            }
        )
        
    }
    func dismiss(_ completion: (() -> ())? = nil) {
        
        move(goUp: false)
        
        UIView.animate(
            withDuration: config.animationDuration, animations: {
                
                self.layoutIfNeeded()
                self.overlayButton.alpha = 0
                
            }, completion: { (finished) in
                completion?()
                self.removeFromSuperview()
                self.overlayButton.removeFromSuperview()
            }
        )
        
    }
}
