//
//  String+Timepiece.swift
//  Timepiece
//
//  Created by Naoto Kaneko on 2015/03/01.
//  Copyright (c) 2015年 Naoto Kaneko. All rights reserved.
//

import Foundation

extension String {
    // MARK - Parse into NSDate
    
    func dateFromFormat(_ format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            formatter.locale = Locale(identifier: "ar_EG")
        }
        else{
            formatter.locale = Locale(identifier: "en_US")
        }
        return formatter.date(from: self)
    }
}
