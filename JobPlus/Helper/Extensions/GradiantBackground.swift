//
//  GradiantBackground.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/25/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit


@IBDesignable
public class GradientView: UIView {
    private func createGradient() -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.transform = CATransform3DMakeRotation(.pi / 2, 0, 0, 1)
        gradient.frame = bounds
        layer.insertSublayer(gradient, at: 0)
        return gradient
    }

    private var gradient: CAGradientLayer?

    @IBInspectable
    public var color1: UIColor? {
        didSet {
            updateColors()
        }
    }

    @IBInspectable
    public var color2: UIColor? {
        didSet {
            updateColors()
        }
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        gradient = createGradient()
        updateColors()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        gradient = createGradient()
        updateColors()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        gradient?.frame = bounds
    }

    private func updateColors() {
        guard
            let color1 = color1,
            let color2 = color2
        else {
            return
        }

        gradient?.colors = [color1.cgColor, color2.cgColor]
    }
}
extension UIView {
    func insertHorizontalGradient() -> GradientView {
        let gradientView = GradientView(frame: bounds)
        if L102Language.isRTL {
        gradientView.color1 = .white
        gradientView.color2 =  UIColor(hexString: "#ededf1")
        }
        else{
            gradientView.color1 = UIColor(hexString: "#ededf1")
            gradientView.color2 = .white
        }
        gradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.insertSubview(gradientView, at: 0)
        return gradientView
    }
    
        func layerGradient() {
            let layer : CAGradientLayer = CAGradientLayer()
            layer.frame.size = self.frame.size
            layer.frame.origin = CGPoint(x: 0.0,y: 0.0)
    
            let color0 = UIColor(hexString: "#D7DBDD").cgColor
            let color1 = UIColor(hexString: "#D7DBDD").cgColor
            let color2 = UIColor(hexString: "#E5E7E9").cgColor
            let color3 = UIColor(hexString: "#E5E7E9").cgColor
            let color4 = UIColor(hexString: "#FFFFFF").cgColor
            let color5 = UIColor(hexString: "#FFFFFF").cgColor
    
            layer.colors = [color0,color1,color2,color3,color4,color5]
            layer.cornerRadius = 55.5
            self.layer.insertSublayer(layer, at: 0)
        }
    func customLayerGradient(color1: String, color2: String, cornerRadius: CGFloat? = 0.0) {
        let layer: CAGradientLayer = CAGradientLayer()
        self.backgroundColor = .clear
        layer.frame.size = self.frame.size
        layer.frame.origin = CGPoint(x: 0.0, y: 0.0)
        let color_one = UIColor(hexString: color1).cgColor
        let color_two = UIColor(hexString: color2).cgColor
        layer.colors = [color_one, color_two]
        layer.cornerRadius = cornerRadius ?? 0.0
        self.layer.insertSublayer(layer, at: 0)
    }
    func customLayerGradient(size: CGSize, color1: String, color2: String, cornerRadius: CGFloat? = 0.0) {
        let layer: CAGradientLayer = CAGradientLayer()
        self.backgroundColor = .clear
        layer.frame.size = size
        
        layer.frame.origin = CGPoint(x: 0.0, y: 0.0)
        let color_one = UIColor(hexString: color1).cgColor
        let color_two = UIColor(hexString: color2).cgColor
        layer.colors = [color_one, color_two]
        layer.cornerRadius = cornerRadius ?? 0.0
        self.layer.insertSublayer(layer, at: 0)
    }
}
