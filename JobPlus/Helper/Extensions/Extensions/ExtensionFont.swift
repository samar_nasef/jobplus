//
//  ExtensionFont.swift
//  PartTime
//
//  Created by passant on 10/14/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import UIKit
extension UIFont {
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
}
