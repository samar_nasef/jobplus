//
//  ExtensionUiViewController.swift
//  Zumra
//
//  Created by Mohamed Shaat on 3/13/18.
//  Copyright © 2018 10degree. All rights reserved.
//

import Foundation
import SwiftMessages
extension UIViewController {
    func pushViewController(viewControllerName:String , storyBoardName : String ,animated:Bool?=true) {
        let storyboard: UIStoryboard = UIStoryboard(name: storyBoardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
        navigationController?.pushViewController(viewController, animated: animated!)
    }
    func presentViewController(viewControllerName:String , storyBoardName : String) {
        let storyboard: UIStoryboard = UIStoryboard(name: storyBoardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
        self.present(viewController, animated: true, completion: nil)
    }
    func showErrorMessage(message:String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.error)
        view.bodyLabel?.font = UIFont(name: "Cairo-SemiBold", size: 15)
        // Add a drop shadow.
        //view.configureDropShadow()
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = [""].randomElement()!
        view.configureContent(title: " ", body: message, iconText: iconText)
    
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: .normal)
        config.dimMode = .blur(style: .dark, alpha: 0.5, interactive: true)
        // Show the message.
        
        SwiftMessages.show(config: config, view: view)
    }
    func showErrorMessageLong(message:String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.error)
        view.bodyLabel?.font = UIFont(name: "Cairo-SemiBold", size: 15)
        // Add a drop shadow.
        //view.configureDropShadow()
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = [""].randomElement()!
        view.configureContent(title: " ", body: message, iconText: iconText)
    
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.duration = .seconds(seconds: 20)
        config.presentationContext = .window(windowLevel: .normal)
        config.dimMode = .blur(style: .dark, alpha: 0.5, interactive: true)
        // Show the message.
        
        SwiftMessages.show(config: config, view: view)
    }
    func showSucessMessage(message:String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        
        // Add a drop shadow.
        //view.configureDropShadow()
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        view.bodyLabel?.font = UIFont(name: "Cairo-SemiBold", size: 15)
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = [""].randomElement()!
        
        view.configureContent(title: " ", body: message, iconText: iconText)
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: .normal)
        config.dimMode = .blur(style: .dark, alpha: 0.5, interactive: true)
        // Show the message.
        SwiftMessages.show(config: config, view: view)
    }
    func startFromRoot() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Authorization", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
    }
    func setRoot(storyBoard:String,viewController:String){
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: storyBoard, bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: viewController)
    }
    func clearDataAndStart() {
        UserManager.sharedInstance.subscribedUser = false
        UserDefaults.standard.removeObject(forKey: "pfname")
        UserDefaults.standard.removeObject(forKey: "plname")
        UserDefaults.standard.removeObject(forKey: "pemail")
        UserDefaults.standard.removeObject(forKey: "pCountryCode")
        UserDefaults.standard.removeObject(forKey: "pCountry")
        UserDefaults.standard.removeObject(forKey: "pCity")
        UserDefaults.standard.removeObject(forKey: "pAddress")
        UserDefaults.standard.removeObject(forKey: "pPostalCode")
        UserManager.sharedInstance.removeUser()
        if #available(iOS 13.0, *) {
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            let mainwindow = sd.window
             let stry = UIStoryboard(name: "Authorizations", bundle: nil)
         mainwindow?.rootViewController = stry.instantiateInitialViewController()
            }
        }
        else{
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Authorizations", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
        }
    }
    
//    func setNavigationBarItem() {
//        self.addLeftBarButtonWithImage(#imageLiteral(resourceName: "ic_side_menu"))
//        self.addRightBarButtonWithImage(#imageLiteral(resourceName: "ic_bell"))
//        self.slideMenuController()?.removeLeftGestures()
//        self.slideMenuController()?.removeRightGestures()
//        self.slideMenuController()?.addLeftGestures()
//        self.slideMenuController()?.addRightGestures()
//    }
    
//    func addBellRightButton(){
//
//        let button = UIButton(type: .custom)
//        button.setImage(#imageLiteral(resourceName: "ic_bell"), for: .normal)
//        if let notificationEnabled = UserDefaults.standard.value(forKey: "NOTIFICATIONENABLED") as? Bool {
//            if notificationEnabled {
//                button.setImage(#imageLiteral(resourceName: "ic_bell"), for: .normal)
//            }else {
//                button.setImage(#imageLiteral(resourceName: "ic_bell_off"), for: .normal)
//            }
//        }
//        button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
//        button.addTarget(self, action: #selector(didTapBell), for: UIControl.Event.touchUpInside)
//        let barButton = UIBarButtonItem(customView: button)
//
//        navigationItem.rightBarButtonItem = barButton
//
//    }
    
//    @objc func didTapBell(sender: AnyObject){
//        print("didTapBell")
//        //go to home
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        if let notificationEnabled = UserDefaults.standard.value(forKey: "NOTIFICATIONENABLED") as? Bool {
//            if notificationEnabled {
//                UIApplication.shared.unregisterForRemoteNotifications()
//                UserDefaults.standard.set(false, forKey:"NOTIFICATIONENABLED")
//                sender.setImage(#imageLiteral(resourceName: "ic_bell_off"), for: .normal)
//            }else {
//                appDelegate.registerPushNotifications(application: UIApplication.shared)
//                UserDefaults.standard.set(true, forKey:"NOTIFICATIONENABLED")
//                 sender.setImage(#imageLiteral(resourceName: "ic_bell"), for: .normal)
//            }
//        }
//
//    }
    func openUrl(url:URL){
        if UIApplication.shared.canOpenURL(url) {
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
    }
//    func removeNavigationBarItem() {
//        self.navigationItem.leftBarButtonItem = nil
//        self.navigationItem.rightBarButtonItem = nil
//        self.slideMenuController()?.removeLeftGestures()
//        self.slideMenuController()?.removeRightGestures()
//    }
    
 
}

