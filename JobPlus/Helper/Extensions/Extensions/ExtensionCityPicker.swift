//
//  ExtensionCityPicker.swift
//  NajeezAgent
//
//  Created by passant on 5/10/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import UIKit
extension UIPickerView {
    func addCustomPicker(_ pickerArray:NSArray ,_ sender: UIButton ,_ pickerTitle:String , _ initialSelection:Int ,completion: @escaping (_ selectedintex:Int) ->()){
        let customPicker = ActionSheetMultipleStringPicker(title: pickerTitle.localized(), rows: [
            pickerArray
            ], initialSelection: [initialSelection], doneBlock: {
                picker, indexes, values in
                if let selectedCityIndex = indexes as NSArray?{
                    if let index = selectedCityIndex[0] as? Int {
                        completion(index)
                    }
                }
                
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender  )
        customPicker?.toolbarBackgroundColor = UIColor.frenchBlue
        customPicker?.toolbarButtonsColor = UIColor.white
        customPicker?.borderWidth = 0
        customPicker?.titleTextAttributes = [NSAttributedString.Key.font : FontHelper.defaultBoldFontWithSize(size: 17), NSAttributedString.Key.foregroundColor : UIColor.white] as [NSAttributedString.Key : Any]
        customPicker?.setDoneButton(UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: nil))
        customPicker?.setCancelButton(UIBarButtonItem(title: "CancelPicker".localized(), style: .plain, target: self, action: nil))
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
                
        
        customPicker?.pickerTextAttributes = [NSAttributedString.Key.font : FontHelper.defaultBoldFontWithSize(size: 17), NSAttributedString.Key.foregroundColor : UIColor.black , NSAttributedString.Key.paragraphStyle : paragraph ]
        
        customPicker?.show()
    }
}

