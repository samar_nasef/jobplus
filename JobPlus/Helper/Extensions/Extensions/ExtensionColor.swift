import UIKit
extension UIColor {
    
    class var frenchBlue: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 175.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }
    
    class var isabelline: UIColor {
        return UIColor(red: 241.0 / 255.0, green: 240.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    
    class var lapisLazuli: UIColor {
        return UIColor(red: 31.0 / 255.0, green: 94.0 / 255.0, blue: 141.0 / 255.0, alpha: 1.0)
    }
    
    class var lapisLazuli1: UIColor {
        return UIColor(red: 33.0 / 255.0, green: 112.0 / 255.0, blue: 171.0 / 255.0, alpha: 1.0)
    }
    
    class var unitedNationsBlue: UIColor {
        return UIColor(red: 80.0 / 255.0, green: 151.0 / 255.0, blue: 227.0 / 255.0, alpha: 1.0)
    }
    
    class var darkPink: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 80.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    }
    
    class var white: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    
    class var darkGray: UIColor {
        return UIColor(red: 165.0 / 255.0, green: 170.0 / 255.0, blue: 173.0 / 255.0, alpha: 1.0)
    }
    
    class var darkGray1: UIColor {
        return UIColor(red: 168.0 / 255.0, green: 168.0 / 255.0, blue: 168.0 / 255.0, alpha: 1.0)
    }
    
    class var darkJungleGreen: UIColor {
        return UIColor(red: 29.0 / 255.0, green: 29.0 / 255.0, blue: 29.0 / 255.0, alpha: 1.0)
    }
    
    class var steelBlue: UIColor {
        return UIColor(red: 58.0 / 255.0, green: 128.0 / 255.0, blue: 180.0 / 255.0, alpha: 1.0)
    }
    
    class var whiteSmoke: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 247.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
    }
    
    class var ghostWhite: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 249.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    }
    
    class var liver: UIColor {
        return UIColor(red: 81.0 / 255.0, green: 81.0 / 255.0, blue: 81.0 / 255.0, alpha: 1.0)
    }
    
    class var spiroDiscoBall: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 179.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    
    class var gainsboro: UIColor {
        return UIColor(red: 210.0 / 255.0, green: 221.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)
    }
    
    class var gainsboro1: UIColor {
        return UIColor(red: 214.0 / 255.0, green: 219.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }
    
    class var orangeYellow: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 212.0 / 255.0, blue: 98.0 / 255.0, alpha: 1.0)
    }
    
    class var terraCotta: UIColor {
        return UIColor(red: 229.0 / 255.0, green: 99.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
    }
    
    class var mediumSpringBud: UIColor {
        return UIColor(red: 184.0 / 255.0, green: 233.0 / 255.0, blue: 134.0 / 255.0, alpha: 1.0)
    }
    
    class var darkSlateGray: UIColor {
        return UIColor(red: 47.0 / 255.0, green: 72.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
    }
    
    class var darkSlateGray1: UIColor {
        return UIColor(red: 58.0 / 255.0, green: 85.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
    }
    
    class var manatee: UIColor {
        return UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
    }
    
    class var manatee1: UIColor {
        return UIColor(red: 151.0 / 255.0, green: 151.0 / 255.0, blue: 151.0 / 255.0, alpha: 1.0)
    }
    
    class var munsell: UIColor {
        return UIColor(red: 240.0 / 255.0, green: 242.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
    }
    
    class var munsell1: UIColor {
        return UIColor(red: 241.0 / 255.0, green: 243.0 / 255.0, blue: 246.0 / 255.0, alpha: 1.0)
    }
    
    class var charcoal: UIColor {
        return UIColor(red: 50.0 / 255.0, green: 54.0 / 255.0, blue: 67.0 / 255.0, alpha: 1.0)
    }
    
    class var black: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    class var saffron: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 184.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
    }
    
    class var slateGray: UIColor {
        return UIColor(red: 100.0 / 255.0, green: 121.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    }
    
    class var outerSpace: UIColor {
        return UIColor(red: 74.0 / 255.0, green: 74.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0)
    }
    
    class var silver: UIColor {
        return UIColor(red: 198.0 / 255.0, green: 198.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0)
    }
    
    class var glitter: UIColor {
        return UIColor(red: 231.0 / 255.0, green: 236.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)
    }
    
    class var greenBlue: UIColor {
        return UIColor(red: 0, green: 175, blue: 223, alpha: 1.0)
    }
    
    class var trolleyGrey: UIColor {
        return UIColor(red: 122.0 / 255.0, green: 122.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    }
    
    class var davyGrey: UIColor {
        return UIColor(red: 84.0 / 255.0, green: 84.0 / 255.0, blue: 90.0 / 255.0, alpha: 1.0)
    }
    
    class var orangePeel: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 160.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    class var mediumTurquoise: UIColor {
        return UIColor(red: 80.0 / 255.0, green: 227.0 / 255.0, blue: 194.0 / 255.0, alpha: 1.0)
    }
    
    class var lightGray: UIColor {
        return UIColor(red: 201.0 / 255.0, green: 212.0 / 255.0, blue: 215.0 / 255.0, alpha: 1.0)
    }
    
    class var lightGray1: UIColor {
        return UIColor(red: 209.0 / 255.0, green: 209.0 / 255.0, blue: 209.0 / 255.0, alpha: 1.0)
    }
    
    class var lightGray2: UIColor {
        return UIColor(red: 213.0 / 255.0, green: 214.0 / 255.0, blue: 219.0 / 255.0, alpha: 1.0)
    }
    
    class var lightGray3: UIColor {
        return UIColor(red: 214.0 / 255.0, green: 214.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0)
    }
    
    class var platinum: UIColor {
        return UIColor(red: 226.0 / 255.0, green: 234.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    
    class var platinum1: UIColor {
        return UIColor(red: 217.0 / 255.0, green: 228.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    
    class var platinum2: UIColor {
        return UIColor(red: 230.0 / 255.0, green: 230.0 / 255.0, blue: 230.0 / 255.0, alpha: 1.0)
    }
    
    class var platinum3: UIColor {
        return UIColor(red: 220.0 / 255.0, green: 227.0 / 255.0, blue: 234.0 / 255.0, alpha: 1.0)
    }
    
    class var platinum4: UIColor {
        return UIColor(red: 225.0 / 255.0, green: 230.0 / 255.0, blue: 233.0 / 255.0, alpha: 1.0)
    }
    
    class var timberwolf: UIColor {
        return UIColor(red: 216.0 / 255.0, green: 216.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
    }
    
    class var yellowGreen: UIColor {
        return UIColor(red: 126.0 / 255.0, green: 211.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
    }
    
    class var dimGray: UIColor {
        return UIColor(red: 108.0 / 255.0, green: 110.0 / 255.0, blue: 110.0 / 255.0, alpha: 1.0)
    }
    
    class var stilDeGrainYellow: UIColor {
        return UIColor(red: 246.0 / 255.0, green: 195.0 / 255.0, blue: 88.0 / 255.0, alpha: 1.0)
    }
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
