//
//  Date+Extenstions.swift
//  PartTime
//
//  Created by mohamed shaat on 7/18/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import UIKit

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatter.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        return dateFormatter.string(from: self)
    }
    func dateToTimeStamp() -> Int {
        
        let timeStamp = Int((self.timeIntervalSince1970)) + TimeZone.current.secondsFromGMT()
        print("TimeZone.current.secondsFromGMT() : \(TimeZone.current.secondsFromGMT())")
        return timeStamp
    }
}
