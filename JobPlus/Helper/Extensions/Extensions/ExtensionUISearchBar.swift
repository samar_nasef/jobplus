//
//  ExtensionUISearchBar.swift
//  Zumra
//
//  Created by Mohamed Shaat on 4/14/18.
//  Copyright © 2018 10degree. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    func change(textFont : UIFont?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            
            if let textField = view as? UITextField {
                textField.font = textFont
            }
        }
    } }
