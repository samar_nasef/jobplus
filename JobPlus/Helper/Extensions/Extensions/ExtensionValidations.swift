//
//  ExtensionValidations.swift
//  Zumra
//
//  Created by Mohamed Shaat on 3/19/18.
//  Copyright © 2018 10degree. All rights reserved.
//
import libPhoneNumber_iOS
import Foundation
extension String  {
    var isBlank: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    var isSaudiNumber:Bool{
    let saudiNumberRegEx = "(05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})"
    let saudiNumberTest  = NSPredicate(format:"SELF MATCHES %@", saudiNumberRegEx)
    return saudiNumberTest.evaluate(with: self)
    }
    func isValidPhoneNumber(code:String)->Bool {
        if(self.isBlank){
            return false
        }
        let phoneUtil = NBPhoneNumberUtil()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(withPhoneCarrierRegion: code+self)
            if(phoneUtil.isValidNumber(phoneNumber)==false){
              
                return false
            }

             print(phoneUtil.getRegionCode(for: phoneNumber))
            return true
        }
        catch _ as NSError {
           return false
        }
    }
    var isPasswordMoreThaneightCharacters:Bool {
        
        if self.count > 8 {
            return true
        }else {
            return false
        }
    }
    var isNameMoreThanTwoCharacters:Bool {
        if self.count > 2 {
            return true
        }else {
            return false
        }
    }
    
    
    func RemoveWhiteSpace() -> String
    {
        let replaced = self.trimmingCharacters(in: NSCharacterSet.whitespaces)
        return replaced
    }
    func containsOnlyEnglishLetters() -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") //add whatever characters you find English
        var amountOfEnglishChars = 0
        var amountOfNonEnglishChars = 0
        
        let char = self.unicodeScalars.first
        if characterset.contains(char!) {
                amountOfEnglishChars += 1
            }
            else {
                amountOfNonEnglishChars += 1
            }
        //}
        
        if amountOfEnglishChars > amountOfNonEnglishChars {
            return true
        }else {
            return false
        }
        
        //nameLabel.textAlignment = amountOfEnglishChars > amountOfNonEnglishChars ? .left : .right
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.width)
    }
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func toDate(withFormat format: String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = format
        dateFormatter.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatter.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        let theDate = dateFormatter.date(from: self)
        return theDate
    }
//    public var replacedArabicDigitsWithEnglish: String {
//        var str = self
//        let map = ["٠": "0",
//                   "١": "1",
//                   "٢": "2",
//                   "٣": "3",
//                   "٤": "4",
//                   "٥": "5",
//                   "٦": "6",
//                   "٧": "7",
//                   "٨": "8",
//                   "٩": "9"]
//        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
//        return str
//    }
}
