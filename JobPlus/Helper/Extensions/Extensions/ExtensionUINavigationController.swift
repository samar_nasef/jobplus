//
//  ExtensionUINavigationController.swift
//  Zumra
//
//  Created by Mohamed Shaat on 3/13/18.
//  Copyright © 2018 10degree. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationController {
    
    public func presentTransparentNavigationBar() {
        navigationBar.barTintColor = .clear
        navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        setNavigationBarHidden(false, animated:true)
    }
    
    public func hideTransparentNavigationBar() {
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
        navigationBar.isTranslucent = UINavigationBar.appearance().isTranslucent
        navigationBar.shadowImage =  UIImage()
        navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    public func setNavigationBarTintColor(barTintColor:UIColor) {
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
        navigationBar.isTranslucent = UINavigationBar.appearance().isTranslucent
        navigationBar.shadowImage =  UIImage()
        navigationBar.barTintColor = barTintColor
    }
    public func setNavigationBarTitleAndColor(){
        let attributes = [NSAttributedString.Key.font : FontHelper.defaultArabicBoldFontWithSize(size: 17), NSAttributedString.Key.foregroundColor : UIColor.trolleyGrey] as [NSAttributedString.Key : Any]
        
        self.navigationBar.tintColor = UIColor.saffron
        self.navigationBar.titleTextAttributes = attributes
    }
    public func setNavigationTintColor(tintColor:UIColor){
        let attributes = [NSAttributedString.Key.font : FontHelper.defaultArabicBoldFontWithSize(size: 17), NSAttributedString.Key.foregroundColor : UIColor.trolleyGrey] as [NSAttributedString.Key : Any]
        
        self.navigationBar.tintColor = tintColor
        self.navigationBar.titleTextAttributes = attributes
    }
    public func setNavigationBarTitleColor(color:UIColor,fontSize:Int){
        let attributes = [NSAttributedString.Key.font : FontHelper.defaultArabicBoldFontWithSize(size: CGFloat(fontSize)), NSAttributedString.Key.foregroundColor :color] as [NSAttributedString.Key : Any]
        self.navigationBar.titleTextAttributes = attributes
         UIApplication.shared.statusBarStyle = .lightContent
    }
    func removeNavigationBarLineColor()
    {
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setupNavigationControllerOfPartTime(){
        self.setNavigationTintColor(tintColor: UIColor.saffron)
        self.setNavigationBarTintColor(barTintColor: UIColor.frenchBlue)
        self.setNavigationBarTitleColor(color:UIColor.white, fontSize: 20)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.backgroundColor =  UIColor.frenchBlue
    }
    
    
    public func hideTransparentNavigationBarForTeacherDetails() {
        navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
        navigationBar.isTranslucent = true 
        navigationBar.shadowImage =  UIImage()
        // navigationBar.barTintColor = UIColor.clear
    }
}
