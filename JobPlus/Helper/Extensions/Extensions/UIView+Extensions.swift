import UIKit
import Foundation

// MARK: - Designable Extension

@IBDesignable
public extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}


// MARK: - Properties

public extension UIView {
    
    /// Size of view.
    var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.width = newValue.width
            self.height = newValue.height
        }
    }
    
    /// Width of view.
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    /// Height of view.
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

extension UIView {
    
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview.flatMap { $0.superview(of: T.self) }
    }
    
    class func getAllSubviews<T: UIView>(view: UIView) -> [T] {
        return view.subviews.flatMap { subView -> [T] in
            var result = getAllSubviews(view: subView) as [T]
            if let view = subView as? T {
                result.append(view)
            }
            return result
        }
    }
    
    func getAllSubviews<T: UIView>() -> [T] {
        return UIView.getAllSubviews(view: self) as [T]
    }
    
    func changeFont() {
        if L102Language.currentAppleLanguage() == "ar" {
        let allLabels = self.getAllSubviews() as [UILabel] 
            for label in allLabels {
                if label.font.isBold {
                    label.font = FontHelper.defaultArabicBoldFontWithSize(size: (label.font?.pointSize)! )
                }else {
                     label.font = FontHelper.defaultArabicRegularFontWithSize(size: (label.font?.pointSize)! )
                }
                
               
            }
       
         let allTextFields = self.getAllSubviews() as [UITextField]
            for textField in allTextFields {
                if (textField.font?.isBold)! {
                    textField.font = FontHelper.defaultArabicBoldFontWithSize(size: (textField.font?.pointSize)! )
                }else {
                    textField.font = FontHelper.defaultArabicRegularFontWithSize(size: (textField.font?.pointSize)! )
                }
            }
        
         let allButtons = self.getAllSubviews() as [UIButton]
            for button in allButtons {
                if (button.titleLabel?.font.isBold)! {
                    button.titleLabel?.font = FontHelper.defaultArabicBoldFontWithSize(size: (button.titleLabel?.font.pointSize)! )
                }else {
                    button.titleLabel?.font = FontHelper.defaultArabicRegularFontWithSize(size: (button.titleLabel?.font.pointSize)! )
                }
            }
//        }
        
        }else {
            
        }
    }
}


// MARK: - Methods

public extension UIView {
    
    typealias Configuration = (UIView) -> Swift.Void
    
    func config(configurate: Configuration?) {
        configurate?(self)
    }
    
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.silver.cgColor
        layer.shadowOpacity = 1.5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 10
        
        
    }
    
}

extension UIView {
    
    func searchVisualEffectsSubview() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let found = subview.searchVisualEffectsSubview() {
                    return found
                }
            }
        }
        return nil
    }
    
    /// This is the function to get subViews of a view of a particular type
    /// https://stackoverflow.com/a/45297466/5321670
    func subViews<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T{
                all.append(aView)
            }
        }
        return all
    }
    
    
    /// This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T
    /// https://stackoverflow.com/a/45297466/5321670
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
}
