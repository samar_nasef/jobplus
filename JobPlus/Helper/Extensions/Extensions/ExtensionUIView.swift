//
//  ViewRecursion.swift
//  Swift_iOS-Localize-Constraint
//  Created by mohamed-shaat on 2/5/17.
//  Copyright © 2017 shaat. All rights reserved.
//
//////////////
import Foundation
import UIKit
//extension UIViewController {
//    func handleLanguage() {
//            if(MoLocalization.getCurrentLanguage() == MoLocalization.Language.ARABIC) {
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            UINavigationBar .appearance().semanticContentAttribute = .forceRightToLeft
//            } else {
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            UINavigationBar .appearance().semanticContentAttribute =  .forceLeftToRight
//            }
//            let userdef = UserDefaults.standard
//            userdef.set([MoLocalization.getCurrentLanguage()], forKey: "AppleLanguages")
//            userdef.synchronize()
//    }
//}
extension  UILabel {
    @IBInspectable var localizationKey : String {
    set {
        self.text = newValue.localized()
        }
     get {
        return self.text!;
        }
    }
    
}

extension  UITextField
{
    @IBInspectable var localizationKey : String {
     set {
          self.placeholder=newValue.localized()
         }
     get {
          return self.placeholder!;
         }
    }
}

extension  UITextView {
    @IBInspectable var localizationKey : String {
        set {
            self.text=newValue.localized()
        }
        get {
            return self.text!;
        }
    }
}

extension  UIButton {
    @IBInspectable var localizationKey : String {
        set {
            setTitle(newValue.localized(),for: .normal)
        }
        get {
            return (self.titleLabel?.text)!;
            
        }
    }
}

extension UIImage {
    public func flippedImage() -> UIImage?{
        if let _cgImag = self.cgImage {
            let flippedimg = UIImage(cgImage: _cgImag, scale:self.scale , orientation: UIImage.Orientation.upMirrored)
            return flippedimg
        }
        return nil
    }
    
  
}

extension UIImageView {
    @IBInspectable var flipInarabic: Bool  {
        set {
            if(L102Language.currentAppleLanguage() == "ar" && newValue == true) {
                let rotatedImage = self.image?.flippedImage()
                self.image = rotatedImage
            }
        }
        get {
            return self.flipInarabic

        }
    }
    @IBInspectable var flipIninglish: Bool  {
        set {
            if(L102Language.currentAppleLanguage() == "en" && newValue == true) {
                let rotatedImage = self.image?.flippedImage()
                self.image = rotatedImage
            }
        }
        get {
            return self.flipIninglish

        }
    }
    
    @IBInspectable var arabicImage:UIImage {
        set {
            if(L102Language.currentAppleLanguage() == "ar") {
                self.image = newValue
            }
        }
        get {
            return self.image!
            
        }
    }
}


