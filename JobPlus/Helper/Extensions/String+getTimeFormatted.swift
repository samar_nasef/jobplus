//  String+getTimeFormatted.swift
//  Created by aya on 2/20/18.

import UIKit


extension String {
    func getTimeFormatted() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "DDDD dd, HH:mm"
        dateFormatterGet.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatterGet.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatterGet.locale = Locale(identifier: "en_US")
        }
        if let orderDateObject = dateFormatterGet.date(from: self) {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "DDDD dd, HH:mm" // "a" prints "pm" or "am"
            let hourString = formatter.string(from: orderDateObject) // "12 AM"
            return hourString
        }
        else{
            return ""
        }
    }
    
    func getFormattedDate() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatterGet.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatterGet.locale = Locale(identifier: "en_US")
        }
        if let estimatedDate = dateFormatterGet.date(from: self) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy"
            if L102Language.isRTL {
                dateFormatter.locale = Locale(identifier: "ar_EG")
            }
            else{
                dateFormatter.locale = Locale(identifier: "en_US")
            }
            let estimatedDateString = dateFormatter.string(from: estimatedDate)
            return estimatedDateString
        }
        else {
            return ""
        }
    }
    func getFormattedDateTime() -> String {
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        dateFormatterGet.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatterGet.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatterGet.locale = Locale(identifier: "en_US")
        }
        if let estimatedDate = dateFormatterGet.date(from: self + "Z") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy, hh:mm a"
            if L102Language.isRTL {
                dateFormatter.locale = Locale(identifier: "ar_EG")
            }
            else{
                dateFormatter.locale = Locale(identifier: "en_US")
            }
            let estimatedDateString = dateFormatter.string(from: estimatedDate)
            return estimatedDateString
        }
        else {
            return ""
        }
    }
    
    func checkExiration() -> Bool {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        dateFormatterGet.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatterGet.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatterGet.locale = Locale(identifier: "en_US")
        }
        if let epirydDate = dateFormatterGet.date(from: self + " 00:00:00Z") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let currentDate = Date()
            print("currentDate \(currentDate) epirydDate \(epirydDate)" )
            if currentDate >= epirydDate {
                return true
            }
            else{
                return false
            }
        }
        else{
            return true
        }
    }
    func checkExirationWithTime() -> Bool {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = NSTimeZone.local
        if L102Language.isRTL {
            dateFormatterGet.locale = Locale(identifier: "ar_EG")
        }
        else{
            dateFormatterGet.locale = Locale(identifier: "en_US")
        }
        if let epirydDate = dateFormatterGet.date(from: self) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let currentDate = Date()
            print("currentDate \(currentDate) epirydDate \(epirydDate)" )
            if currentDate > epirydDate {
                return true
            }
            else{
                return false
            }
        }
        else{
            return true
        }
    }
}


