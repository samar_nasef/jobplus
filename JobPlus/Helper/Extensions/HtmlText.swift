//
//  HtmlText.swift
//  ecommerce
//
//  Created by elsaid yousif on 7/3/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
//
//class HtmlText: NSObject {
//
//}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

