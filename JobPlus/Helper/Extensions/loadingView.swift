//
//  loadingView.swift
//  ecommerce
//
//  Created by elsaid yousif on 5/20/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let loaderColor = UIColor.orangePeel
        let animationFrame = CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100))
        let animationView = NVActivityIndicatorView(frame: animationFrame, type: NVActivityIndicatorType.ballScaleMultiple, color:loaderColor, padding: 10)
        DispatchQueue.main.async {
            animationView.center = onView.center
            onView.addSubview(animationView)
        }
        animationView.startAnimating()
        return animationView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
