//
//  AppDelegate.swift
//  JobPlus
//
//  Created by elsaid yousif on 11/24/19.
//  Copyright © 2019 elsaid yousif. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import SwiftMessages
import SwiftyStoreKit
//import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    //var mainViewController:CatgoriesViewController?
    var notificationEnabled : Bool = true
    var launchURL :URL? = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Thread.sleep(forTimeInterval: 2.0)
        if L102Language.currentAppleLanguage() == "ar" {
            L102Language.setAppleLAnguageTo(lang: "ar-SA")
        }
        
        L102Localizer.DoTheMagic()
        FirebaseApp.configure()
        self.registerPushNotifications(application: application)
        self.enableIQkeyboard()
        if #available(iOS 13.0, *) {
        }
        else{
            checkIfLogin()
        }
        
        let userActDic = launchOptions?[UIApplication.LaunchOptionsKey.userActivityDictionary] as? [String: Any]
        if let auserActivity: NSUserActivity = userActDic?["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity{
            launchURL = auserActivity.webpageURL
        }
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
                for purchase in purchases {
                    switch purchase.transaction.transactionState {
                    case .purchased, .restored:
                        if purchase.needsFinishTransaction {
                            // Deliver content from server, then:
                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                        }
                        // Unlock content
                    case .failed, .purchasing, .deferred:
                        break // do nothing
                    }
                }
            }
        
        //GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "7e4a822cb5833e3b250ca15bf85ca589" ]
        //GADMobileAds.sharedInstance().start(completionHandler: nil)
        return true
    }
        
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let url = self.launchURL {
            self.launchURL = nil
            DispatchQueue.main.asyncAfter(deadline: .now()+2.0, execute: {
                // wait to initalize notifications
                let dynamicLinks = DynamicLinks.dynamicLinks()
                NSLog("handling \(dynamicLinks)")
                dynamicLinks.handleUniversalLink(url) { (dynamiclink, error) in
                    if let link = dynamiclink?.url {
                        NSLog("proceessing \(dynamicLinks) \(link)")
                    }
                }
            })
        }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingUrl = userActivity.webpageURL {
            print(incomingUrl)
            let urlComponents = (String(incomingUrl.absoluteString)).split(separator: "/")
            print(urlComponents)
//            if urlComponents[1] == "jobplus.page.link" {
//                let lastComponent = (urlComponents[urlComponents.count - 1]).split(separator: "=")
//                let id = lastComponent[1]
//                print(id)
//                UserManager.sharedInstance.openDynamicLinkJobID = Int(id) ?? -1
//                UserManager.sharedInstance.openDynamicLink = true
//                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                let mainViewController = mainStoryBoard.instantiateInitialViewController()
//                self.window?.rootViewController = mainViewController
//            }
        }
      let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
        guard error == nil else {
            print("Error handling dynamic link \(String(describing: error?.localizedDescription))")
            return
        }
        if dynamiclink != nil {
            self.handleIncomingDynamicLink(dynamiclink!)
        }
      }

      return handled
    }
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink){
        guard let incomingUrl = dynamicLink.url else {
            print("no dynamic url")
            return
        }
        print("recieved dynamicURL \(incomingUrl.absoluteURL)")
        let urlComponents = (String(incomingUrl.absoluteString)).split(separator: "/")
        print(urlComponents)
        if urlComponents[1].contains("www.jobplus.biz") || urlComponents[1].contains("jobplus.page.link") {
            let lastComponent = (urlComponents[urlComponents.count - 1]).split(separator: "=")
            let id = lastComponent[1]
            print(id)
            UserManager.sharedInstance.openDynamicLinkJobID = Int(id) ?? -1
            UserManager.sharedInstance.openDynamicLink = true
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = mainStoryBoard.instantiateInitialViewController()
            self.window?.rootViewController = mainViewController
        }
    }
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      return application(app, open: url,
                         sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                         annotation: "")
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        
        print(dynamicLink)
        print(url)
        return true
      }
      return false
    }
    
    
    func enableIQkeyboard(){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    func checkIfLogin() {
        // rootnav
        if(UserManager.sharedInstance.isLogined()) {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = storyBoard.instantiateInitialViewController()
            self.window?.rootViewController = mainViewController
        }
        else{
            let introDone = UserDefaults.standard.integer(forKey: "introDone")
            if introDone == 1 {
                let storyBoard = UIStoryboard(name: "Authorizations", bundle: nil)
                let mainViewController = storyBoard.instantiateInitialViewController() //.instantiateViewController(withIdentifier: "rootnav")
                self.window?.rootViewController = mainViewController
            }
            else{
                window = UIWindow()
                window?.makeKeyAndVisible()
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal
                let swipeviewcontroller = SwipeViewController(collectionViewLayout: layout)
                self.window?.rootViewController = swipeviewcontroller
            }
        }
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

extension AppDelegate : UNUserNotificationCenterDelegate ,MessagingDelegate{
    func registerPushNotifications(application:UIApplication){
        // notification Registration
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    
    
    // Receive displayed notifications for iOS 10 devices.
    //    @available(iOS 10.0, *)
    //    func userNotificationCenter(_ center: UNUserNotificationCenter,
    //                                willPresent notification: UNNotification,
    //                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //        AudioServicesPlaySystemSound(SystemSoundID(1000))
    //        let userInfo = notification.request.content.userInfo
    //        // Print full message.
    //        print(userInfo)
    //        completionHandler([.alert, .badge, .sound])
    //    }
    //
    
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey:"FIREBASE_TOKEN")
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    //    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
    //        print("Received data message: \(remoteMessage.appData)")
    //    }
    // [END ios_10_data_message]

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        let user_info = response.notification.request.content.userInfo
        
            if UserManager.sharedInstance.getUser() != nil {
                if let jobId = user_info["jobId"] as? String, jobId != "0" {
                    if #available(iOS 13.0, *) {
                        UserManager.sharedInstance.openDynamicLinkJobID = Int(jobId) ?? -1
                        UserManager.sharedInstance.openDynamicLink = true
                        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                            let scene = UIApplication.shared.connectedScenes.first
                            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                let mainwindow = sd.window
                                let nav = mainwindow?.rootViewController as! UINavigationController
                                nav.pushViewController(viewController, animated: true)
                            }
                        }
                    }
                    else{
                        UserManager.sharedInstance.openDynamicLinkJobID = Int(jobId) ?? -1
                        UserManager.sharedInstance.openDynamicLink = true
                        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                            let nav = self.window?.rootViewController as! UINavigationController
                            nav.show(viewController, sender: nil)
                        }
                        
                    }
                    
                }
                else if let TypeID = user_info["type"] as? String {
                    if Int(TypeID) == -10 {
                        if let jobId = user_info["jobId"] as? String, jobId != "0" {
                            if Int(jobId) == UserManager.sharedInstance.getUser()?.id {
                                self.showErrorMessage(message: "Sorry, your account has been suspended, you can contact our support team".localized())
                                self.clearDataAndStart()
                            }
                        }
                    }
                    else if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                        viewController.notification_type = Int(TypeID)!
                        if #available(iOS 13.0, *) {
                            let scene = UIApplication.shared.connectedScenes.first
                            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                let mainwindow = sd.window
                                let nav = mainwindow?.rootViewController as! UINavigationController
                                nav.pushViewController(viewController, animated: true)
                            }
                        }
                        else{
                            let nav = self.window?.rootViewController as! UINavigationController
                            nav.show(viewController, sender: nil)
                        }
                    }
                    
                }
                else{
                    if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                        viewController.notification_type = 0
                        if #available(iOS 13.0, *) {
                            let scene = UIApplication.shared.connectedScenes.first
                            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                let mainwindow = sd.window
                                let nav = mainwindow?.rootViewController as! UINavigationController
                                nav.pushViewController(viewController, animated: true)
                            }
                        }
                        else{
                            let nav = self.window?.rootViewController as! UINavigationController
                            nav.show(viewController, sender: nil)
                        }
                    }
                }
            }
        
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // Print full message.
        print("notiiii \(userInfo)")
        var NotificationTitle: String? = ""
        var NotificationMessage: String? = ""
        if let aps = userInfo[AnyHashable("aps")] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? String {
                    NotificationMessage = body
                    print(body)
                }
                if let title = alert["title"] as? String {
                    NotificationTitle = title
                    print(title)
                }
            }
            else{
                if let body = aps["alert"] as? String {
                    NotificationMessage = body
                    print(body)
                }
            }
            if application.applicationState == UIApplication.State.inactive ||  application.applicationState == UIApplication.State.background  {


            }
            else  {
                if UserManager.sharedInstance.getUser() != nil {
                    if let TypeID = userInfo["type"] as? String {
                        if Int(TypeID) == -10 {
                            if let jobId = userInfo["jobId"] as? String, jobId != "0" {
                                if Int(jobId) == UserManager.sharedInstance.getUser()?.id {
                                    self.showErrorMessage(message: "Sorry, your account has been suspended, you can contact our support team".localized())
                                    self.clearDataAndStart()
                                }
                            }
                        }
                        else if let jobId = userInfo["jobId"] as? String, jobId != "0" {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotiCount"), object: nil)
                            self.showNotificationMessage(title: NotificationTitle ?? "", message: NotificationMessage ?? "" , type: TypeID, jobId: jobId)
                        }
                        else{
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotiCount"), object: nil)
                            self.showNotificationMessage(title: NotificationTitle ?? "", message: NotificationMessage ?? "" , type: TypeID, jobId: nil)
                        }
                        
                    }
                    else{
                        if let jobId = userInfo["jobId"] as? String, jobId != "0" {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotiCount"), object: nil)
                            self.showNotificationMessage(title: NotificationTitle ?? "", message: NotificationMessage ?? "" , type: nil, jobId: jobId)
                        }
                        else{
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNotiCount"), object: nil)
                            self.showNotificationMessage(title: NotificationTitle ?? "", message: NotificationMessage ?? "" , type: nil, jobId: nil)
                        }
                    }
                }
            }
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func showNotificationMessage(title: String, message:String, type: String?, jobId: String? ) {
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.info)
        // Add a drop shadow.
        //view.configureDropShadow()
        if let type_id = Int(type ?? "1") {
            view.button?.isHidden = false
            view.button?.setTitle("View".localized(), for: .normal)
            view.buttonTapHandler = { _ in
                if UserManager.sharedInstance.getUser() != nil {
                    //                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openServerNotification"), object: nil, userInfo: ["type": type_id])
                    if let jobId = jobId {
                        if #available(iOS 13.0, *) {
                            UserManager.sharedInstance.openDynamicLinkJobID = Int(jobId) ?? -1
                            UserManager.sharedInstance.openDynamicLink = true
                            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                                let scene = UIApplication.shared.connectedScenes.first
                                if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                    let mainwindow = sd.window
                                    let nav = mainwindow?.rootViewController as! UINavigationController
                                    nav.pushViewController(viewController, animated: true)
                                }
                            }
                        }
                        else{
                            UserManager.sharedInstance.openDynamicLinkJobID = Int(jobId) ?? -1
                            UserManager.sharedInstance.openDynamicLink = true
                            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as? JobDetailsViewController {
                                let nav = self.window?.rootViewController as! UINavigationController
                                nav.show(viewController, sender: nil)
                            }
                        }
                    }
                    else if let viewController = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
                        viewController.notification_type = type_id
                        if #available(iOS 13.0, *) {
                            let scene = UIApplication.shared.connectedScenes.first
                            if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                                let mainwindow = sd.window
                                let nav = mainwindow?.rootViewController as! UINavigationController
                                nav.popToRootViewController(animated: false)
                                nav.pushViewController(viewController, animated: true)
                                SwiftMessages.hide()
                            }
                        }
                        else{
                            let nav = self.window?.rootViewController as! UINavigationController
                            nav.popToRootViewController(animated: false)
                            nav.show(viewController, sender: nil)
                            SwiftMessages.hide()
                        }
                        
                    }
                }
            }
        }
        else{
            view.button?.isHidden = true
        }
        let icon_img = UIImage(named: "jpLogo_splash")
        view.bodyLabel?.font = UIFont(name: "Cairo-SemiBold", size: 14)
        view.titleLabel?.font = UIFont(name: "Cairo-SemiBold", size: 15)
        view.configureIcon(withSize: CGSize(width: 60, height: 40))
        view.iconImageView?.contentMode = .scaleAspectFit
        view.configureContent(title: title, body: message, iconImage: icon_img!)
        var config = SwiftMessages.Config()
        config.duration = .seconds(seconds: 5)
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: .normal)
        config.dimMode = .blur(style: .dark, alpha: 0.5, interactive: true)
        // Show the message.
        SwiftMessages.show(config: config, view: view)
    }
    
    func showErrorMessage(message:String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.error)
        view.bodyLabel?.font = UIFont(name: "Cairo-SemiBold", size: 15)
        // Add a drop shadow.
        //view.configureDropShadow()
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = [""].randomElement()!
        view.configureContent(title: " ", body: message, iconText: iconText)
    
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: .normal)
        config.dimMode = .blur(style: .dark, alpha: 0.5, interactive: true)
        // Show the message.
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func clearDataAndStart() {
        UserManager.sharedInstance.subscribedUser = false
        UserDefaults.standard.removeObject(forKey: "pfname")
        UserDefaults.standard.removeObject(forKey: "plname")
        UserDefaults.standard.removeObject(forKey: "pemail")
        UserDefaults.standard.removeObject(forKey: "pCountryCode")
        UserDefaults.standard.removeObject(forKey: "pCountry")
        UserDefaults.standard.removeObject(forKey: "pCity")
        UserDefaults.standard.removeObject(forKey: "pAddress")
        UserDefaults.standard.removeObject(forKey: "pPostalCode")
        UserManager.sharedInstance.removeUser()
        if #available(iOS 13.0, *) {
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
            let mainwindow = sd.window
             let stry = UIStoryboard(name: "Authorizations", bundle: nil)
         mainwindow?.rootViewController = stry.instantiateInitialViewController()
            }
        }
        else{
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let stry = UIStoryboard(name: "Authorizations", bundle: nil)
        rootviewcontroller.rootViewController = stry.instantiateInitialViewController()
        }
    }
    
}
