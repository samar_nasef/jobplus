//
//  subscriptionsApi.swift
//  JobPlus
//
//  Created by elsaid yousif on 3/2/20.
//  Copyright © 2020 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
//get-pages data


enum subscriptionsApi {
    case getPackagesData
    case createPayPage(paymentInfo:PaymentObj)
    case addTransactionIAP(transaction_id: String, packageID: Int, paymentBrand: Int)
    case getHyperPayURL(package_id: Int, amount: String, payment_brand: Int)
    case UserNewPackage
    case CancelAutoSubscribe
    case ActivateAutoSubscribe
    case Invoices
}

extension subscriptionsApi:TargetType  , AccessTokenAuthorizable{
    var authorizationType: AuthorizationType {
        switch self {
        case .getPackagesData:
            return .bearer
            
        case .createPayPage(_):
            return .bearer
            
        case .addTransactionIAP(_,_,_):
            return .bearer
        case .getHyperPayURL(_,_,_):
            return .bearer
        case .UserNewPackage:
            return .bearer
        case .CancelAutoSubscribe:
            return .bearer
        case .ActivateAutoSubscribe:
            return .bearer
        case .Invoices:
            return .bearer
        }
    }
    var baseURL : URL {
        return URL(string:WebService.URLBASE)!
    }
    var path: String {
        switch self {
        case .getPackagesData:
            return WebService.URLGETPackages
            
        case .createPayPage(_):
            return WebService.URLCreatePayPage
            
        case .addTransactionIAP(_,_,_):
            return WebService.iapTransactionURL
        case .getHyperPayURL(_,_,_):
            return WebService.hyperPayURL
        case .UserNewPackage:
            return WebService.userNewPackagesUrl
        case .CancelAutoSubscribe:
            return WebService.CancelAutoSubscribeURL
        case .ActivateAutoSubscribe:
            return WebService.ActivateAutoSubscribeURL
        case .Invoices:
            return WebService.InvoicesURL
        }
    }
    var method : Moya.Method {
        switch self {
        case .getPackagesData:
            return .get
            
        case .createPayPage(_):
            return .post
        case .addTransactionIAP(_):
            return .post
        case .getHyperPayURL(_,_,_):
            return .post
        case .UserNewPackage:
            return .get
        case .CancelAutoSubscribe:
            return .get
            
        case .ActivateAutoSubscribe:
            return .get
        case .Invoices:
            return .get
        }
    }
    var task : Task {
        switch self {
        case .getPackagesData:
            return .requestPlain
        case .createPayPage(let payinfo):
            return .requestParameters(parameters: ["cc_first_name" :payinfo.cc_first_name,"cc_last_name":payinfo.cc_last_name,  "cc_phone_number":payinfo.cc_phone_number,"phone_number" :payinfo.phone_number,"email":payinfo.email,"products_per_title" :payinfo.products_per_title,"unit_price":payinfo.unit_price,"quantity" :payinfo.quantity,"other_charges":payinfo.other_charges,"amount" :payinfo.amount,"discount":payinfo.discount,"currency" :payinfo.currency,"reference_no":payinfo.reference_no,"ip_customer" :payinfo.ip_customer,"billing_address":payinfo.billing_address,"state" :payinfo.state,"city":payinfo.city,"postal_code" :payinfo.postal_code,"country":payinfo.country,"shipping_first_name" :payinfo.shipping_first_name,"shipping_last_name":payinfo.shipping_last_name,"address_shipping" :payinfo.address_shipping,"city_shipping":payinfo.city_shipping,"state_shipping" :payinfo.state_shipping,"postal_code_shipping":payinfo.postal_code_shipping,"country_shipping" :payinfo.country_shipping,"msg_lang":payinfo.msg_lang], encoding: JSONEncoding.default )
       
        case .addTransactionIAP(let transaction_id, let packageID, let paymentBrand):
            return .requestParameters(parameters: ["transaction_id": transaction_id, "user_id": UserManager.sharedInstance.getUser()?.id ?? 0, "package_id": packageID, "paymentBrand": paymentBrand ], encoding: JSONEncoding.default)
        case .getHyperPayURL(package_id: let package_id, amount: let amount, let payment_brand):
            return .requestParameters(parameters: ["package_id": package_id, "user_id": UserManager.sharedInstance.getUser()?.id ?? 0, "amount": amount, "payment_brand": payment_brand ], encoding: JSONEncoding.default)
        case .UserNewPackage:
            return .requestParameters(parameters: ["NewModule":1], encoding: URLEncoding.queryString)
        case .CancelAutoSubscribe:
            return .requestParameters(parameters: ["NewModule":1], encoding: URLEncoding.queryString)
        case .ActivateAutoSubscribe:
            return .requestParameters(parameters: ["NewModule":1], encoding: URLEncoding.queryString)
        case .Invoices:
            return .requestPlain
        }
    }
    
    var headers : [String : String]? {
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json" , "Accept" :"application/json" , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data()
        
    }  // We just need to return something here to fully implement the protocol
    
}
