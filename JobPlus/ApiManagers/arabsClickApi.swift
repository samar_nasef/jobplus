//
//  arabsClickApi.swift
//  JobPlus
//
//  Created by samar nasef on 02/08/2021.
//  Copyright © 2021 elsaid yousif. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
enum arabsClickApi {
    case getCategoriesAndMarkets
    case getArabClicksOffers(countryID:Int,categoryID:Int)
//    case MessageDetails(id:Int?)
//    case AddNewMessageOrReplay(messageText:String?,RecevierID:Int?,ParentID:Int?)
    }
extension arabsClickApi:TargetType , AccessTokenAuthorizable{
    
    var authorizationType: AuthorizationType {
        switch self {
       
        case .getCategoriesAndMarkets:
            return .bearer
        case .getArabClicksOffers(_,_):
            return .bearer
//        case .AddNewMessageOrReplay(_,_,_):
//            return .bearer
//        case .MessageDetails(_):
//            return .bearer
       
        }
        
    }
    
    var baseURL : URL {
        return URL(string:WebService.URLBASE)!
    }
    
    var path: String {
        switch self {
        
        case .getCategoriesAndMarkets:
            return  WebService.categoriesAndMarketsURL
        case .getArabClicksOffers(_,_):
            return WebService.arabClicksOfferURL
//            return "\(WebService.arabClicksOfferURL)=\(countryID)&categoryID=\(categoryID)"
//        case .AddNewMessageOrReplay(_,_,_):
//            return  WebService.AddNewMessageOrReplayURL
//        case .MessageDetails(let id):
//            return  "\(WebService.MessageDetailsURL) \(id ?? 0)"
            
       
//        case .applyToJob(_,let jobId):
//            return "jobs/\(jobId)/apply"
       
        }
    }
    var method : Moya.Method {
        switch self {
        case .getCategoriesAndMarkets:
            return .get
        case .getArabClicksOffers(_,_):
            return .get
//        case .MessageDetails(_):
//            return .get
//        case .AddNewMessageOrReplay(_,_,_):
//            return .post
       
        }
    }
    var task : Task {
        switch self {
        case .getCategoriesAndMarkets:
            return .requestPlain
        case .getArabClicksOffers(let countryID,let categoryID):
            return .requestParameters(parameters: ["countryID" : countryID, "categoryID": categoryID ], encoding: URLEncoding.queryString)
           
//        case .AddNewMessageOrReplay(let messageText, let RecevierID, let ParentID ):
//            var parametrs = [String:Any]()
//            if let messageText = messageText {
//                parametrs ["messageText"] = messageText
//            }
//            if let RecevierID = RecevierID {
//                parametrs ["RecevierID"] = RecevierID
//            }
//            if let ParentID = ParentID {
//                parametrs ["ParentID"] = ParentID
//            }
//
//            return .requestParameters(parameters: parametrs, encoding: URLEncoding.default)
//        case .MessageDetails(_):
//            return .requestPlain
//
        }
    }
    
    var headers : [String : String]? {
        
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json" , "Accept" :"application/json" , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data() }  // We just need to return something here to fully implement the protocol
    
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
