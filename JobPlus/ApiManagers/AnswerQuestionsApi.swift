//
//  AnswerQuestionsApi.swift
//  PartTime
//
//  Created by passant on 8/28/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import Moya

enum AnswerQuestionsApi {
    case questionsList(jobId:Int)
    
}
extension AnswerQuestionsApi:TargetType , AccessTokenAuthorizable{
    
    var authorizationType: AuthorizationType {
        switch self {
      
        case .questionsList(_):
            return .bearer
        }
    }
    
    var baseURL : URL {
        return URL(string:WebService.URLBASE)!
    }
    var path: String {
        switch self {
        case .questionsList(_):
            return  WebService.URLQUESTIONSLIST
       
        }
    }
    var method : Moya.Method {
        switch self {
        case .questionsList(_):
            return .get
      
        }
    }
    var task : Task {
        switch self {
        case .questionsList(let jobId):
             return .requestParameters(parameters: ["job_id":jobId], encoding: URLEncoding.queryString)
   
        }
        
    }
    
    var headers : [String : String]? {
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json", "Accept" :"application/json"  , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data() }  // We just need to return something here to fully implement the protocol
    
}
