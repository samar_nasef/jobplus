//
//  NotificationsApi.swift
//  Najeez
//
//  Created by passant on 6/24/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//


import Foundation
import Moya
//list of notifications:


enum NotificationsApi {
    case getNotifications(page:Int)
    case getGeneralNotifications(page:Int)
    case getJobsNotifications(page:Int)
    case readNotications
    case getNotificationsCount
    case NotificationStatus(status: Int)
    
}

extension NotificationsApi:TargetType , AccessTokenAuthorizable{
    var authorizationType: AuthorizationType {
        switch self {
        case .getNotifications(_):
            return .bearer
        case .getGeneralNotifications(_):
             return .bearer
        case .getJobsNotifications(_):
             return .bearer
        case .readNotications:
    return .bearer
        case .getNotificationsCount:
            return .bearer
        case .NotificationStatus:
            return .bearer
            
        }
    }
    
    
    var baseURL : URL {
        return URL(string:WebService.URLBASE)!
    }
    var path: String {
        switch self {
        case .getNotifications(_):
            return  WebService.URLGETUSERNOTIFICATIONS
        case .getGeneralNotifications(_):
            return  WebService.URLGETGeneralNOTIFICATIONS
        case .getJobsNotifications(_):
            return  WebService.URLGETJobsNOTIFICATIONS
        case .readNotications:
            return WebService.URLREADUSERNOTIFICATIONS
        case .getNotificationsCount:
            return WebService.URLGETUSERNOTIFICATIONSCount
        case .NotificationStatus:
            return WebService.NotificationStatusURL
            
        }
    }
    var method : Moya.Method {
        switch self {
        case .getNotifications(_) :
            return .get
        case .getGeneralNotifications(_) :
            return .get
        case .getJobsNotifications(_) :
            return .get
        case .readNotications:
            return .post
        case .getNotificationsCount:
            return .get
        case .NotificationStatus(_):
            return .post
        }
    }
    var task : Task {
        switch self {
        case .getNotifications(let page):
            return .requestParameters(parameters: ["page":page , "identifier" : (UIDevice.current.identifierForVendor?.uuidString)!], encoding: URLEncoding.queryString)
        case .getGeneralNotifications(let page):
            return .requestParameters(parameters: ["page":page , "identifier" : (UIDevice.current.identifierForVendor?.uuidString)! ,"model":"iphone"], encoding: URLEncoding.queryString)
        case .getJobsNotifications(let page):
            return .requestParameters(parameters: ["page":page , "identifier" : (UIDevice.current.identifierForVendor?.uuidString)! ,"model":"iphone"], encoding: URLEncoding.queryString)
        case .readNotications:
            return .requestPlain
        case .getNotificationsCount:
            return .requestPlain
        case .NotificationStatus(status: let status):
            return .requestParameters(parameters: ["NotificationStatus":status ], encoding: JSONEncoding.default)
        }
    }
    
    var headers : [String : String]? {
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json", "Accept" :"application/json"  , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data()
        
    }  // We just need to return something here to fully implement the protocol
    
}
