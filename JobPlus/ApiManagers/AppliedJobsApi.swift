//
//  AppliedJobsApi.swift
//  PartTime
//
//  Created by passant on 8/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import Foundation
import Moya

enum AppliedJobsApi {
 
    case getAppliedJobs(page:Int)
}
extension AppliedJobsApi:TargetType , AccessTokenAuthorizable{
    
    var authorizationType: AuthorizationType {
        switch self {
    
        case .getAppliedJobs(_):
            return .bearer
        }
        
    }
    
    var baseURL : URL {
        
            return URL(string:WebService.URLBASE)!
    }
    var path: String {
        switch self {
       
        case .getAppliedJobs(_):
            return WebService.URLGETAPPLIEDJOBS
        }
    }
    var method : Moya.Method {
        switch self {
     
        case .getAppliedJobs(_):
            return .get
        }
    }
    var task : Task {
        switch self {
      
        case .getAppliedJobs(let page):
            return .requestParameters(parameters: ["page":page], encoding: URLEncoding.default)
        }
        
    }
    
    var headers : [String : String]? {
        
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json", "Accept" :"application/json"  , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data() }  // We just need to return something here to fully implement the protocol
    
}
