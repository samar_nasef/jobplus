//
//  AuthorizationApi.swift
//  Najeez
//
//  Created by passant on 5/8/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
//login
//add firebasetoken
//register
//forget password
//verification code
//resend verification code
//logout

enum AuthorizationApi {
    case loginRequest(mobile:String , password:String)
    case forgetPasswordRequest(mobile:String)
    case forgetPasswordTokenRequest(phone:String,password:String,forgotPasswordToken:String)
    case registerRequest(name:String , LastName: String, email:String , mobile:String , password:String , gender:String? , birthDate:Int?, mobileApplicationLocale:String,image:String?, DeviceGuid: String, CaptchaCode: String)
    case logoutUserRequest(firebaseToken:String)
    case deleteAccount
    case verficationCodeRequest(verificationCode:Int)
    case addFireBaseToken(deviceFirebaseToken:String)
    case editProfileimg(image:String?)
    case resendVerificationCodeRequest(secondPhone: String)
    case editProfileRequest(name:String ,
        email:String ,
        mobile:String ,
        gender:String? ,
        birthDate:Int?,
        mobileApplicationLocale:String,
        image:String? ,
        userCountryId:Int ,
        educationCountryId:Int ,
        educationName:String ,
        educationCity:String ,
        educationDegreeKey:String ,
        educationOtherDegree:String ,
        educationMajor:String ,
        experienceObject:ExperienceObjectToPost,
        cv_extension: String,
        cv: String,
        certificate_id: Int,
        worktime_id: Int,
        City_Residence: String)
    case editnewProfileRequest(data:User)
//    name:String ,
//        email:String ,
//        mobile:String ,
//        gender:String? ,
//        birthDate:Int?,
//        mobileApplicationLocale:String,
//        image:String? ,
//        userCountryId:Int ,
//        educationCountryId:Int ,
//        educationName:String ,
//        educationCity:String ,
//        educationDegreeKey:String ,
//        educationOtherDegree:String ,
//        educationMajor:String ,
//        experienceObject:ExperienceObjectToPost,
//        cv_extension: String,
//        cv: String,
//        certificate_id: Int,
//        worktime_id: Int,
//      City_Residence: String
    
    case changePasswordrequest(oldPassword:String , newPassword:String)
    case editProfileLanguage(mobileApplicationLocale:String)
    case deleteUserProfileImg
    case getUserInfo
    case checkChangePhone(mobile:String)
    case verifyMobileAfterChange(mobile: String, mobileVerificationCode: Int)
    case getCaptchCode(uuid: String)
}
extension AuthorizationApi:TargetType , AccessTokenAuthorizable{
    
    var authorizationType: AuthorizationType {
        switch self {
            
        case .loginRequest(_,_ ):
            return .none
            
        case .forgetPasswordRequest(_):
            return .none
        case .forgetPasswordTokenRequest(_,_,_):
            return .none
        case .registerRequest(_ ,_ ,_,_,_,_,_,_ ,_,_ ,_):
            return .none
            
        case .logoutUserRequest(_):
            return .bearer
        case .verficationCodeRequest(_):
            return .bearer
            
        case .addFireBaseToken:
            return .bearer
            
        case .resendVerificationCodeRequest(_):
            return .bearer
        case .editProfileimg(_):
            return .bearer
        case .editProfileRequest(_ , _ , _ , _ , _, _,_ ,_,_,_,_,_,_,_,_,_,_,_,_,_):
            return .bearer
        case .editnewProfileRequest(_):
            return .bearer
            
        case .changePasswordrequest(_ , _):
            return .bearer
        case .editProfileLanguage(_):
            return .bearer
        case .deleteUserProfileImg:
            return .bearer
        case .getUserInfo:
            return .bearer
        
        case .checkChangePhone(_):
             return .bearer
        case .verifyMobileAfterChange(_ , _):
            return .bearer
        case .deleteAccount:
            return .bearer
        case .getCaptchCode(_):
            return .none
        }
    }
    
    var baseURL : URL {
        return URL(string:WebService.URLBASE)!
    }
    var path: String {
        switch self {
        case .loginRequest(_,_):
            return  WebService.URLLOGIN
            
        case .forgetPasswordRequest(_):
            return  WebService.URLFORGOTPASSWORD
        case .forgetPasswordTokenRequest(_,_,_):
            return  WebService.URLFORGOTPASSWORDTOKEN
        case .registerRequest(_ ,_ ,_,_,_,_,_,_,_,_ ,_):
            return WebService.URLUSERREGISTER
        case .logoutUserRequest(_):
            return WebService.URLLOGOUT + "\((UIDevice.current.identifierForVendor?.uuidString)!)"
        case .verficationCodeRequest(_):
            return WebService.URLVERIFICATION
        case .addFireBaseToken(_):
            return WebService.URLEDITFIREBASETOKEN
            
        case .resendVerificationCodeRequest(_):
            return WebService.URLRESENDVERIFICATIONCODE
        case .editProfileRequest(_ , _ , _ , _ , _, _,_,_,_,_,_,_,_,_,_,_,_,_,_,_):
            return WebService.URLEDITPROFILE
            
        case .editnewProfileRequest(_):
            return WebService.editAccountURL
        case .changePasswordrequest(_ , _):
            return WebService.URLCHANGEPASSWORD
        case .editProfileimg(_):
            return WebService.URLEditProfileImg
        case .editProfileLanguage(_):
            return WebService.URLEDITPROFILE
            
        case .getUserInfo:
            return WebService.URLGETUSERINFO
        case .deleteUserProfileImg:
            return WebService.DeleteUserProfileImage
        case .checkChangePhone(_):
            return WebService.CHECKPHONETOCHANGE
        case .verifyMobileAfterChange(_ , _):
            return WebService.VERIFYPHONEFTERCHANGE
        case .deleteAccount:
            return WebService.URLDeleteAcount
        case .getCaptchCode:
            return WebService.captchaGetCode
        }
    }
    var method : Moya.Method {
        switch self {
        case .loginRequest , .forgetPasswordRequest :
            return .post
        case .registerRequest:
            return .post
        case .logoutUserRequest:
            return .post
        case .verficationCodeRequest:
            return .post
        case .addFireBaseToken :
            return .post
        case .resendVerificationCodeRequest(_):
            return .post
        case .forgetPasswordTokenRequest(_, _, _):
            return .post
        case .editProfileimg(_):
            return .post
        case .editProfileRequest(_ , _ , _ , _ , _, _,_,_,_,_,_,_,_,_,_,_,_,_,_,_):
            return .post
        case .editnewProfileRequest(_):
            return .post
        case .changePasswordrequest(_, _):
            return .post
        case .editProfileLanguage(_):
            return .post
        case .deleteUserProfileImg:
            return .get
        case .getUserInfo:
            return .get
        case .checkChangePhone(_):
            return .post
        case .verifyMobileAfterChange(_ , _):
            return .post
        
        case .deleteAccount:
            return .get
        case .getCaptchCode(_):
            return .post
        }
    }
    var task : Task {
        switch self {
        case .loginRequest(let mobile,let password ):
            return .requestParameters(parameters: ["mobile":mobile,"password":password], encoding: JSONEncoding.default)
            
        case .forgetPasswordRequest(let mobile):
            return .requestParameters(parameters: ["mobile":mobile], encoding: JSONEncoding.default)
            
        case .registerRequest(let name ,let LastName, let email , let mobile , let password , let gender ,let  birthDate, let mobileApplicationLocale,let image, let device_id, let captcha_code):
            var registerParameters =  ["name":name,"LastName":LastName,"email":email  ,"mobile":mobile ,"password":password,"gender":gender ?? "" , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate ?? "", "device_id": device_id, "code": captcha_code] as [String : Any]
            if image != nil {
                registerParameters =  ["name":name,"LastName":LastName,"email":email  ,"mobile":mobile ,"password":password,"gender":gender! , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate!,"image" : image!,  "device_id": device_id, "code": captcha_code] as [String : Any]
            }
            return .requestParameters(parameters:registerParameters, encoding: JSONEncoding.default)
        case .logoutUserRequest(_):
            return .requestPlain
            
        case .verficationCodeRequest(let verificationCode):
            return .requestParameters(parameters: ["mobileVerificationCode":verificationCode], encoding: JSONEncoding.default)
        case .addFireBaseToken(let deviceFirebaseToken):
            return .requestParameters(parameters: ["firebaseToken":deviceFirebaseToken,"identifier":(UIDevice.current.identifierForVendor?.uuidString)! ,"model":"iphone"], encoding: JSONEncoding.default)
        case .resendVerificationCodeRequest(let secondPhone):
            return .requestParameters(parameters: ["secondPhone":secondPhone], encoding: JSONEncoding.default)
        case .forgetPasswordTokenRequest(let phone, let password, let forgotPasswordToken):
            return .requestParameters(parameters: ["mobile":phone,"password":password,"forgotPasswordToken":forgotPasswordToken], encoding: JSONEncoding.default)
        case .editProfileimg(let image):
            let Parameters =  ["image": image]
            return .requestParameters(parameters:Parameters as [String : Any], encoding: JSONEncoding.default)
        case .editProfileRequest(let name, let email, let mobile, let gender, let birthDate, let mobileApplicationLocale, let image  , let userCountryId , let educationCountryId ,  let educationName ,let educationCity ,let educationDegreeKey , let educationOtherDegree , let educationMajor , let experienceObject, let cv_extension, let cv, let certificate_id, let worktime_id, let City_Residence):
            var registerParameters =  ["name":name,"email":email  ,"mobile":mobile ,"gender":gender ?? "" , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate ?? "" , "country_id" : userCountryId , "educInstitution":educationName , "educ_country_id":educationCountryId , "educCity":educationCity , "educDegree":educationDegreeKey , "educDegreeTxt":educationOtherDegree , "educMajor":educationMajor, "certificate_id" : certificate_id, "worktime_id": worktime_id, "City_Residence": City_Residence ] as [String : Any]
            if !cv.isEmpty {
                registerParameters =  ["name":name,"email":email  ,"mobile":mobile ,"gender":gender ?? "" , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate ?? "" , "country_id" : userCountryId , "educInstitution":educationName , "educ_country_id":educationCountryId , "educCity":educationCity , "educDegree":educationDegreeKey , "educDegreeTxt":educationOtherDegree , "educMajor":educationMajor, "cv_extension": cv_extension, "cv": cv , "certificate_id" : certificate_id, "worktime_id": worktime_id, "City_Residence": City_Residence ] as [String : Any]
            }
            if image != nil {
                registerParameters =  ["name":name,"email":email  ,"mobile":mobile ,"gender":gender! , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate!,"image" : image! , "country_id" : userCountryId , "educInstitution":educationName , "educ_country_id":educationCountryId , "educCity":educationCity , "educDegree":educationDegreeKey , "educDegreeTxt":educationOtherDegree , "educMajor":educationMajor , "certificate_id" : certificate_id, "worktime_id": worktime_id, "City_Residence": City_Residence] as [String : Any]
                if !cv.isEmpty {
                    registerParameters =  ["name":name,"email":email  ,"mobile":mobile ,"gender":gender! , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate!,"image" : image! , "country_id" : userCountryId , "educInstitution":educationName , "educ_country_id":educationCountryId , "educCity":educationCity , "educDegree":educationDegreeKey , "educDegreeTxt":educationOtherDegree , "educMajor":educationMajor, "cv_extension": cv_extension, "cv": cv, "certificate_id" : certificate_id, "worktime_id": worktime_id, "City_Residence": City_Residence ] as [String : Any]
                }
            }
            
            let objectAsDict:[String : AnyObject] = Mapper<ExperienceObjectToPost>().toJSON(experienceObject) as [String : AnyObject]
                print("objectAsDict = \(objectAsDict)")
            
            var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
            for exp in experienceObject.experiences {
                arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
            }
                registerParameters["experiences"] = arrOfExpDic
            
            return .requestParameters(parameters:registerParameters, encoding: JSONEncoding.default)
            
            
        case .editnewProfileRequest(let data):
            var registerParameters =  ["name":data.name ?? "" ,"CityID": data.CityID, "LastName":data.LastName,"SectionID":data.SectionID,"Nationality":data.Nationality,"userBio":data.userBio,"PreferedWork":data.PreferedWork,"DrivingLicense":data.DrivingLicense,"gender":data.gender,"birthDate":data.birthDate2,"email":data.email,"country_id":data.countryId,"City_Residence_Name":data.City_Residence,"PreferedContactID":data.PreferedContactID, "Disability": data.Disability, "DisabilityType": data.DisabilityType,"experienceid":data.experienceid] as [String : Any]
            print("parms: \(registerParameters)")
//            if !cv.isEmpty {
//                registerParameters =  ["name":data.name , "LastName":data.LastName ] as [String : Any]
//            }
//            if image != nil {
//                registerParameters =  ["name":name,"email":email  ,"mobile":mobile ,"gender":gender! , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate!,"image" : image! , "country_id" : userCountryId , "educInstitution":educationName , "educ_country_id":educationCountryId , "educCity":educationCity , "educDegree":educationDegreeKey , "educDegreeTxt":educationOtherDegree , "educMajor":educationMajor , "certificate_id" : certificate_id, "worktime_id": worktime_id, "City_Residence": City_Residence] as [String : Any]
//                if !cv.isEmpty {
//                    registerParameters =  ["name":name,"email":email  ,"mobile":mobile ,"gender":gender! , "mobileApplicationLocale" :mobileApplicationLocale ,"birthDate" : birthDate!,"image" : image! , "country_id" : userCountryId , "educInstitution":educationName , "educ_country_id":educationCountryId , "educCity":educationCity , "educDegree":educationDegreeKey , "educDegreeTxt":educationOtherDegree , "educMajor":educationMajor, "cv_extension": cv_extension, "cv": cv, "certificate_id" : certificate_id, "worktime_id": worktime_id, "City_Residence": City_Residence ] as [String : Any]
//                }
//            }
            
            if data.SectionID == 4 {
                let objectAsDict:[String : AnyObject] = Mapper<eduactionalObjectToPost>().toJSON(data.EducationalInformation2!) as [String : AnyObject]
                    print("objectAsDict = \(objectAsDict)")
                
                var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
                for exp in data.EducationalInformation2!.EducationalInformation {
                    arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                }
                    registerParameters["EducationalInformation"] = arrOfExpDic
            }
            
            if data.SectionID == 6 {
                if let softSkills = data.SoftSkills {
                    let objectAsDict:[String : AnyObject] = Mapper<SkillObjectToPost>().toJSON(data.SoftSkills!) as [String : AnyObject]
                        print("objectAsDict = \(objectAsDict)")
                    
                    var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
                    for exp in data.SoftSkills!.SoftSkills {
                        arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                    }
                    print(arrOfExpDic)
                        registerParameters["SoftSkills"] = arrOfExpDic
                }
                else{
                    registerParameters["SoftSkills"] = []
                }
                
            }
            
            if data.SectionID == 7 {
                let objectAsDict:[String : AnyObject] = Mapper<SkillObjectToPost>().toJSON(data.SoftSkills!) as [String : AnyObject]
                    print("objectAsDict = \(objectAsDict)")
                
                var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
                for exp in data.SoftSkills!.SoftSkills {
                    arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                }
                print(arrOfExpDic)
                registerParameters["TechnicalSkills"] = arrOfExpDic
            }
            
            if data.SectionID == 8 {
                let objectAsDict:[String : AnyObject] = Mapper<certificateObjectToPost>().toJSON(data.userCertificates2!) as [String : AnyObject]
                    print("objectAsDict = \(objectAsDict)")
                
                var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
                for exp in data.userCertificates2!.userCertificates {
                    arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                }
                    registerParameters["userCertificates"] = arrOfExpDic
            }
            
            if data.SectionID == 9 {
                let objectAsDict:[String : AnyObject] = Mapper<languageObjectToPost>().toJSON(data.userLanguages2!) as [String : AnyObject]
                    print("objectAsDict = \(objectAsDict)")
                
                var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
                for exp in data.userLanguages2!.userLanguages {
                    arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                }
                    registerParameters["userLanguages"] = arrOfExpDic
            }
            if data.SectionID == 10 {
                registerParameters["cv"] = data.cv
                registerParameters["cv_extension"] = data.cv_extension
            }
            if data.SectionID == 11 {
                registerParameters["cv_id"] = data.cv_id
                registerParameters["cv_lang"] = L102Language.isRTL ? "ar" : "en"
            }
            
            if data.SectionID == 5 {
                let objectAsDict:[String : AnyObject] = Mapper<ExperienceObjectToPost>().toJSON(data.experiences2!) as [String : AnyObject]
                    print("objectAsDict = \(objectAsDict)")
                
                var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
                for exp in data.experiences2!.experiences {
                    arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                }
                    registerParameters["experiences"] = arrOfExpDic
            }
            
            return .requestParameters(parameters:registerParameters, encoding: JSONEncoding.default)

            
            
        case .changePasswordrequest(let oldPassword, let newPassword):
            return .requestParameters(parameters: ["oldPassword":oldPassword,"password":newPassword,], encoding:JSONEncoding.default)
        case .editProfileLanguage(let mobileApplicationLocale):
            return .requestParameters(parameters: ["mobileApplicationLocale":mobileApplicationLocale], encoding:JSONEncoding.default)
        case .deleteUserProfileImg:
            return .requestPlain
        case .getUserInfo:
            return .requestPlain
        
        case .checkChangePhone(let mobile):
            
            return .requestParameters(parameters: ["mobile":mobile], encoding: JSONEncoding.default)
            
        case .verifyMobileAfterChange(let mobile, let mobileVerificationCode):
        return .requestParameters(parameters: ["mobile":mobile,"mobileVerificationCode":mobileVerificationCode], encoding: JSONEncoding.default)
        
        case .deleteAccount:
            return .requestPlain
        case .getCaptchCode(uuid: let uuid):
            return .requestParameters(parameters: ["device_id":uuid], encoding: JSONEncoding.default)
        }
    }
    
    var headers : [String : String]? {
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json", "Accept" :"application/json"  , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data() }  // We just need to return something here to fully implement the protocol
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
