//
//  SettingsApi.swift
//  Najeez
//
//  Created by passant on 6/3/18.
//  Copyright © 2018 tendegrees. All rights reserved.
//

import Foundation
import Moya
//get-pages data


enum SettingsApi {
    case getPagesData(slug:String)
    case addPreferedCategoires(categoiresIds:[Int], jobTypes: [Int], sectors: [Int], times: [Int], companies: [Int], cities: [Int])
    case getFAQData(page:Int)
    case getContact
}

extension SettingsApi:TargetType  , AccessTokenAuthorizable{
    var authorizationType: AuthorizationType {
        switch self {
        case .getPagesData(_):
            return .none
            
        case .addPreferedCategoires(_,_,_,_,_,_):
            return .bearer
            
        case .getFAQData(_):
            return .none
            
        case .getContact:
            return .none
        }
    }
    var baseURL : URL {
        return URL(string:WebService.URLBASE)!
    }
    var path: String {
        switch self {
        case .getPagesData(let slug):
            if slug == SettingPage.About.rawValue {
                return  WebService.URLGETABOUTUSPAGE
            }else {
                return WebService.URLPRIVACYPOLICYPAGE
            }
        case .addPreferedCategoires(_,_,_,_,_,_):
            return WebService.URLADDPREFERENCE
        case .getFAQData(_):
            return WebService.URLGETFAQ
        case .getContact:
            return WebService.ContactInfoURL
        }
    }
    var method : Moya.Method {
        switch self {
        case .getPagesData:
            return .get
        case .addPreferedCategoires:
            return .post
        case .getFAQData:
            return .get
        case .getContact:
            return .get
        }
    }
    var task : Task {
        switch self {
        case .getPagesData(_):
            return .requestPlain
        case .addPreferedCategoires(let categoiresIds, let jobTypes, let sectors, let times, let companies, let cities):
            return .requestParameters(parameters: ["cats" :categoiresIds, "worksectors": sectors, "worktimes": times, "jobtypes": jobTypes, "companies": companies, "cities": cities], encoding: JSONEncoding.default )
        case .getFAQData(let page):
            return .requestParameters(parameters: ["page" :page], encoding: URLEncoding.default )
        case .getContact:
            return .requestPlain
        }
    }
    
    var headers : [String : String]? {
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json"  , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage(),"Accept" :"application/json"]
    }
    var sampleData: Data { return Data()
        
    }  // We just need to return something here to fully implement the protocol
    
}
