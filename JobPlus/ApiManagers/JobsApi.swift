//
//  JobsApi.swift
//  PartTime
//
//  Created by mohamed shaat on 7/29/18.
//  Copyright © 2018 mohamed shaat. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
enum JobsApi {
    case catgories
    case types
    case DegreesTimes
    case certificatesNew
    case jobs(title:String?,jobTypeID:Int?,page:Int?,jobCityID:Int? , order:String, worktime_id: Int?, WorkSectorID: Int?, companyID: Int?, disabilityID: Int?, experienceID: Int?, specialityID: Int?, genderID: Int?, ageFrom: Int?, ageTo: Int?)
    case jobsNext(nextLink:String)
    case applyToJob(questionsAnswerObject:QuestionListRequest , jobId:Int)
    case getJobDetails(jobId:Int)
    case cancelApply(jobId:Int)
    case getRecommendedJobs(page:Int?)
    case getNewestJobs(page:Int?)
    case getHomeCategories
    case getCompaniesData
    case getWorkSectors
    case getWorkTimes
    case getCountries
    case getDisabilitities
    case getJobsBySectorID(ID: Int, page: Int)
    case addCategoryToFav(catId: Int)
    case removeCategoryToFav(catId: Int)
    case increaseClicks(jobId:Int)
    case addVisit
    case storeVersion
    case getSpecialities
    case getExperoences
    case listFeedbacks
    case addFeedback(rate: Float, message: String)
}
extension JobsApi:TargetType , AccessTokenAuthorizable{
    
    var authorizationType: AuthorizationType {
        switch self {
        case .catgories:
            return .none
        case .types:
            return .none
        case .jobs(_,_,_,_,_,_,_,_,_,_,_,_,_,_):
            if UserManager.sharedInstance.getUser() != nil {
                //return .bearer
            }
            return .none
        case .jobsNext(_):
            return .bearer
        case .applyToJob(_,_):
            return .bearer
        case .getJobDetails(_):
            return .bearer
        case .getRecommendedJobs(_):
            return .bearer
        case .getNewestJobs(_):
            return .none
        case .DegreesTimes:
            return .none
        case .certificatesNew:
            return .bearer
        case .getHomeCategories:
            return .bearer
        case .getCompaniesData:
            return .none
        case .getWorkSectors:
            return .none
        case .getWorkTimes:
            return .none
        case .getCountries:
            return .none
        case .getJobsBySectorID(_,_):
            return .bearer
        case .addCategoryToFav(_):
            return .bearer
        case .removeCategoryToFav(_):
            return .bearer
        case .cancelApply(_):
            return .bearer
        case .getDisabilitities:
            return .none
        case .increaseClicks(_):
            return .bearer
        case .addVisit:
            return .bearer
        case .storeVersion:
            return .none
        case .getSpecialities:
            return .none
        case .getExperoences:
            return .none
        case .listFeedbacks:
            return .bearer
        case .addFeedback(_,_):
            return .bearer
        }
        
    }
    
    var baseURL : URL {
        switch self {
        case .jobsNext(let nextLink):
            return URL(string:nextLink)!
        default:
            return URL(string:WebService.URLBASE)!
        }
    }
    var path: String {
        switch self {
        case .catgories:
            return  WebService.URLJOBSCATGORIES
        case .types:
            return  WebService.URLJOBSTYPES
        case .jobs(_, _, _,_,_,_,_,_,_,_,_,_,_,_):
            return WebService.URLJOBS
        case .jobsNext(_):
            return ""
        case .applyToJob(_,let jobId):
            return "jobs/\(jobId)/apply"
        case .getJobDetails(_):
            return WebService.URLGETJOBDETAILS
        case .getRecommendedJobs:
                   return WebService.URLRECOMMENDEDJOBS
        case .getNewestJobs:
                   return WebService.URLJOBNEWEST
        case .DegreesTimes:
            return WebService.DegreedTimesURL
            
        case .certificatesNew:
            return WebService.certificatesNewURL
            
        case .getHomeCategories:
            return WebService.homeCatsURL
        case .getCompaniesData:
            return WebService.comapniesDataURL
        case .getWorkSectors:
            return WebService.workSectorsURL
        case .getWorkTimes:
            return WebService.workTimesURL
        case .getCountries:
            return WebService.countriesURL
        case .getJobsBySectorID(_,_):
            return WebService.getJobsBySectorIDURL
        case .addCategoryToFav(_):
            return WebService.addCategoryToFavURL
        case .removeCategoryToFav(_):
            return WebService.removeCategoryToFavURL
        case .cancelApply(jobId: let jobId):
            return "jobs/\(jobId)/cancelapply"
        case .getDisabilitities:
            return WebService.disabilityTypesURL
        case .increaseClicks(jobId: let jobId):
            return "jobs/\(jobId)/increaseClicks"
        case .addVisit:
            return WebService.VisitCount
        case .storeVersion:
            return WebService.storeVersion
        case .getSpecialities:
            return WebService.specialities
        case .getExperoences:
            return WebService.experiences
        case .listFeedbacks:
            return WebService.listFeedbackURL
        case .addFeedback(_,_):
            return WebService.addFeedBackURL
        }
    }
    var method : Moya.Method {
        switch self {
        case .catgories:
            return .get
        case .types:
            return .get
        case .jobs(_,_,_,_,_,_,_,_,_,_,_,_,_,_):
            return .get
        case .jobsNext(_):
            return .get
        case .applyToJob(_,_):
            return .post
        case .getJobDetails(_):
            return .get
        case .getRecommendedJobs(_):
                   return .get
        case .getNewestJobs(_):
                  return .get
        case .DegreesTimes:
            return .get
            
        case .certificatesNew:
            return .get
        case .getHomeCategories:
            return .get
            
        case .getCompaniesData:
            return .get
        case .getWorkSectors:
            return .get
        case .getWorkTimes:
            return .get
        case .getCountries:
            return .get
        case .getJobsBySectorID(_,_):
            return .get
        case .addCategoryToFav(_):
            return .post
        case .removeCategoryToFav(_):
            return .post
        case .cancelApply(_):
            return .post
        case .getDisabilitities:
            return .get
        case .increaseClicks(_):
            return .post
        case .addVisit:
            return .post
        case .storeVersion:
            return .get
        case .getSpecialities:
            return .get
        case .getExperoences:
            return .get
        case .listFeedbacks:
            return .get
        case .addFeedback(_,_):
            return .post
        }
    }
    var task : Task {
        switch self {
        case .catgories:
            return .requestPlain
        case .types:
            return .requestPlain
        case .jobs(let title, let jobTypeID, let page , let jobCityID, let order, let worktime_id, let WorkSectorID, let companyID, let disabilityID, let experienceID, let specialityID, let genderID,let ageFrom, let ageTo):
            var parametrs = [String:Any]()
            if let title = title {
                parametrs ["title"] = title
            }
            if let jobTypeID = jobTypeID {
                parametrs ["job_type_id"] = jobTypeID
            }
            if let page = page {
                parametrs ["page"] = page
            }
            if let jobCityID = jobCityID {
                parametrs ["city_id"] = jobCityID
            }
            if let WorkSectorID = WorkSectorID {
                parametrs["WorkSectorID"] = WorkSectorID
            }
            if let companyID = companyID {
                parametrs["companyID"] = companyID
            }
            if let disabilityID = disabilityID {
                parametrs["DisabilityID"] = disabilityID
            }
            if let experienceID = experienceID, experienceID != -1 {
                parametrs["experienceid"] = experienceID
            }
            if let specialityID = specialityID, specialityID != -1 {
                parametrs["specialityid"] = specialityID
            }
            if let genderID = genderID {
                parametrs["gender"] = genderID
            }
            if let ageFrom = ageFrom {
                parametrs["ageFrom"] = ageFrom
            }
            if let ageTo = ageTo {
                parametrs["ageTo"] = ageTo
            }
            parametrs["worktime_id"] = worktime_id
            parametrs["order"] = order
            parametrs ["NewModule"] = 1
            parametrs ["user_id"] = UserManager.sharedInstance.getUser()?.id ?? 0
            return .requestParameters(parameters: parametrs, encoding: URLEncoding.default)
        case .jobsNext(_):
            return .requestPlain
        case .applyToJob(let questionsAnswerObject, _):
            let objectAsDict = Mapper<QuestionListRequest>().toJSON(questionsAnswerObject)
            print(objectAsDict)
           
            var registerParameters =  ["NewModule": "1", "NewVersion":1 ] as [String : Any]
            var arrOfExpDic :[[String:AnyObject]] = [[String:AnyObject]]()
            if let x = questionsAnswerObject.questionsAnswers{
                for exp in  x {
                    arrOfExpDic.append(exp.dictionaryRepresentation() as [String : AnyObject] )
                }
            }
            
            registerParameters["questionsAnswers"] = arrOfExpDic
            
             
            return .requestParameters(parameters: registerParameters, encoding: JSONEncoding.default )
            
        case .getJobDetails(let jobId):
            return .requestParameters(parameters: ["job_id" : jobId, "user_id": UserManager.sharedInstance.getUser()?.id ?? 0, "NewModule": "1" ], encoding: URLEncoding.queryString)
        case .getRecommendedJobs(let page):
            var parametrs = [String:Any]()
            if let page = page {
                parametrs ["page"] = page
            }
             parametrs ["user_id"] = UserManager.sharedInstance.getUser()?.id ?? 0
             parametrs ["NewModule"] = 1
            return .requestParameters(parameters: parametrs, encoding: URLEncoding.default)
        case .getNewestJobs(let page):
            var parametrs = [String:Any]()
            if let page = page {
                parametrs ["page"] = page
            }
             parametrs ["user_id"] = UserManager.sharedInstance.getUser()?.id ?? 0
             parametrs ["NewModule"] = 1
            return .requestParameters(parameters: parametrs, encoding: URLEncoding.default)
        case .DegreesTimes:
            return .requestPlain
        case .certificatesNew:
            return .requestParameters(parameters: ["NewModule":1], encoding: URLEncoding.queryString)
//            return .requestPlain
            
        case .getHomeCategories:
            var parametrs = [String:Any]()
            parametrs ["user_id"] = UserManager.sharedInstance.getUser()?.id ?? 0
            parametrs ["NewModule"] = 1
//            return .requestParameters(parameters: [ "user_id": UserManager.sharedInstance.getUser()?.id ?? 0 ], encoding: URLEncoding.queryString)
        
            return .requestParameters(parameters: parametrs, encoding: URLEncoding.queryString)
            
        case .getCompaniesData:
            return .requestPlain
        case .getWorkSectors:
            return .requestPlain
        case .getWorkTimes:
            return .requestPlain
        case .getCountries:
            return .requestPlain
        case .getJobsBySectorID(ID: let ID, page: let page):
            return .requestParameters(parameters: [ "user_id": UserManager.sharedInstance.getUser()?.id ?? 0, "cat_id" : ID, "page": page, "NewModule":1 ], encoding: URLEncoding.queryString)
        case .addCategoryToFav(catId: let catId):
            return .requestParameters(parameters: ["catId": catId], encoding: JSONEncoding.default)
        case .removeCategoryToFav(catId: let catId):
            return .requestParameters(parameters: ["catId": catId], encoding: JSONEncoding.default)
        case .cancelApply(_):
            return .requestParameters(parameters: ["NewModule":1], encoding: URLEncoding.queryString)
//            return .requestPlain
        case .getDisabilitities:
            return .requestPlain
        case .increaseClicks(_):
            return .requestParameters(parameters: ["NewModule":1], encoding: URLEncoding.queryString)
        case .addVisit:
            return .requestPlain
        case .storeVersion:
            return .requestPlain
        case .getSpecialities:
            return .requestPlain
        case .getExperoences:
            return .requestPlain
        case .listFeedbacks:
            return .requestPlain
        case .addFeedback(rate: let rate, message: let message):
            var Parameters =  ["rate": rate, "message": message ] as [String : Any]
            return .requestParameters(parameters: Parameters, encoding: JSONEncoding.default )
        }
    }
    
    var headers : [String : String]? {
        
        return ["Content-type": "application/json" , "Accept-Encoding" : "application/json" , "Accept" :"application/json" , KeysAndValues.KEY_ApiName: KeysAndValues.KEY_ApiValue , KeysAndValues.KEY_LANG:L102Language.currentAppleLanguage()]
    }
    var sampleData: Data { return Data() }  // We just need to return something here to fully implement the protocol
    
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
